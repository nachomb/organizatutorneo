select e.nombre,sum(partidosJugados),sum(partidosGanados), sum(partidosEmpatados),
sum(partidosPerdidos),sum(golesAFavor) as golesAFavor,sum(golesEnContra) as golesEnContra,golesAFavor - golesEnContra
,sum(puntos) as puntos
from posicion_torneo pt,equipo e
where pt.equipo_fk = e.id_equipo
group by equipo_fk
order by puntos desc;
