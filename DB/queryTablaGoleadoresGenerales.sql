select concat_ws (' ',j.nombre,j.apellido),e.nombre,goles
from posicion_goleador ps, jugador j, equipo e
where ps.jugador_fk = j.id_jugador
and j.equipo_fk = e.id_equipo
and ps.torneo_fk in (45,48)
and j.activo = true
order by goles desc;
