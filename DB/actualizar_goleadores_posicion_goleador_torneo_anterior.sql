-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.49-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema picadito
--

CREATE DATABASE IF NOT EXISTS picadito;
USE picadito;

--
-- Definition of table `accion`
--

DROP TABLE IF EXISTS `accion`;
CREATE TABLE `accion` (
  `ID_ACCION` bigint(20) NOT NULL AUTO_INCREMENT,
  `ENTIDAD` varchar(255) NOT NULL,
  `MENU_ENTRADA` varchar(255) NOT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `TIPO_ENTIDAD` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ACCION`),
  KEY `FK72BAB7E7FE022F9C` (`TIPO_ENTIDAD`),
  CONSTRAINT `FK72BAB7E7FE022F9C` FOREIGN KEY (`TIPO_ENTIDAD`) REFERENCES `tipo_entidad` (`ID_TIPO_ENTIDAD`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accion`
--

/*!40000 ALTER TABLE `accion` DISABLE KEYS */;
INSERT INTO `accion` (`ID_ACCION`,`ENTIDAD`,`MENU_ENTRADA`,`NOMBRE`,`TIPO_ENTIDAD`) VALUES 
 (171,'/Usuario*','Usuario_list','Usuarios',1),
 (172,'/Perfil*','Perfil_list','Perfiles',1),
 (173,'/Jugador*','Jugador_list','Jugadores',1),
 (174,'/Equipo*','Equipo_list','Equipos',1),
 (175,'/Torneo*','Torneo_list','Torneos',1),
 (176,'/Grupo*','Grupo_list','Grupos',1),
 (177,'/PartidoAmistoso*','PartidoAmistoso_list','PartidoAmistoso',1),
 (178,'/MiUsuario*','MiUsuario_miUsuarioListado','Mi Usuario',1),
 (179,'/MiJugador*','MiJugador_miJugadorListado','Mi Jugador',1),
 (180,'/MisEquipos*','MisEquipos','Mis Equipos',1),
 (181,'/MisTorneos*','MisTorneos','Mis Torneos',1),
 (182,'/MisGrupos*','MisGrupos','Mis Grupos',1),
 (183,'/MisPartidos*','MisPartidos','Mis Partidos',1),
 (184,'/MisInvitaciones*','MisInvitaciones','Mis Invitaciones',1);
/*!40000 ALTER TABLE `accion` ENABLE KEYS */;


--
-- Definition of table `alineacion`
--

DROP TABLE IF EXISTS `alineacion`;
CREATE TABLE `alineacion` (
  `ID_ALINEACION` bigint(20) NOT NULL AUTO_INCREMENT,
  `PARTIDO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ALINEACION`),
  KEY `FK41C471F193621169` (`PARTIDO`),
  KEY `FK24DA85F193621169` (`PARTIDO`),
  CONSTRAINT `FK24DA85F193621169` FOREIGN KEY (`PARTIDO`) REFERENCES `partido` (`ID_PARTIDO`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alineacion`
--

/*!40000 ALTER TABLE `alineacion` DISABLE KEYS */;
INSERT INTO `alineacion` (`ID_ALINEACION`,`PARTIDO`) VALUES 
 (1,1379),
 (2,1379),
 (3,1380),
 (4,1380),
 (5,1381),
 (6,1381),
 (7,1382),
 (8,1382),
 (59,1383),
 (60,1383),
 (19,1384),
 (20,1384),
 (21,1385),
 (22,1385),
 (17,1386),
 (18,1386),
 (61,1387),
 (62,1387),
 (37,1388),
 (38,1388),
 (39,1389),
 (40,1389),
 (41,1390),
 (42,1390),
 (43,1391),
 (44,1391),
 (45,1392),
 (46,1392),
 (47,1393),
 (48,1393),
 (49,1394),
 (50,1394),
 (67,1395),
 (68,1395),
 (69,1396),
 (70,1396),
 (75,1398),
 (76,1398),
 (81,1399),
 (82,1399),
 (77,1400),
 (78,1400),
 (79,1401),
 (80,1401),
 (83,1402),
 (84,1402),
 (9,1407),
 (10,1407),
 (11,1408),
 (12,1408),
 (13,1409),
 (14,1409),
 (15,1410),
 (16,1410),
 (29,1411),
 (30,1411),
 (27,1412),
 (28,1412),
 (23,1413),
 (24,1413),
 (25,1414),
 (26,1414),
 (35,1415),
 (36,1415),
 (31,1416),
 (32,1416),
 (33,1417),
 (34,1417),
 (51,1419),
 (52,1419),
 (53,1420),
 (54,1420),
 (55,1421),
 (56,1421),
 (57,1422),
 (58,1422),
 (73,1423),
 (74,1423),
 (65,1424),
 (66,1424),
 (71,1425),
 (72,1425),
 (63,1426),
 (64,1426),
 (87,1427),
 (88,1427),
 (89,1428),
 (90,1428),
 (91,1429),
 (92,1429),
 (85,1430),
 (86,1430),
 (95,1435),
 (96,1435),
 (93,1465),
 (94,1465);
/*!40000 ALTER TABLE `alineacion` ENABLE KEYS */;


--
-- Definition of table `alineacion_amistoso`
--

DROP TABLE IF EXISTS `alineacion_amistoso`;
CREATE TABLE `alineacion_amistoso` (
  `ID_ALINEACION_AMISTOSO` bigint(20) NOT NULL AUTO_INCREMENT,
  `PARTIDO_AMISTOSO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ALINEACION_AMISTOSO`),
  KEY `FKAEEA5BFBEF4CB320` (`PARTIDO_AMISTOSO`),
  CONSTRAINT `FKAEEA5BFBEF4CB320` FOREIGN KEY (`PARTIDO_AMISTOSO`) REFERENCES `partido_amistoso` (`ID_PARTIDO_AMISTOSO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alineacion_amistoso`
--

/*!40000 ALTER TABLE `alineacion_amistoso` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_amistoso` ENABLE KEYS */;


--
-- Definition of table `alineacion_amistoso_titulares`
--

DROP TABLE IF EXISTS `alineacion_amistoso_titulares`;
CREATE TABLE `alineacion_amistoso_titulares` (
  `ID_ALINEACION_AMISTOSO` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) DEFAULT NULL,
  `NOMBRE_JUGADOR` varchar(255) DEFAULT NULL,
  KEY `FKB80031B1FD04914E` (`ID_ALINEACION_AMISTOSO`),
  KEY `FKB80031B14FC1E07` (`ID_JUGADOR`),
  CONSTRAINT `FKB80031B14FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FKB80031B1FD04914E` FOREIGN KEY (`ID_ALINEACION_AMISTOSO`) REFERENCES `alineacion_amistoso` (`ID_ALINEACION_AMISTOSO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alineacion_amistoso_titulares`
--

/*!40000 ALTER TABLE `alineacion_amistoso_titulares` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_amistoso_titulares` ENABLE KEYS */;


--
-- Definition of table `alineacion_suplentes`
--

DROP TABLE IF EXISTS `alineacion_suplentes`;
CREATE TABLE `alineacion_suplentes` (
  `ID_ALINEACION` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) NOT NULL,
  KEY `FK3FC038CD7DEBF27F` (`ID_ALINEACION`),
  KEY `FK3FC038CD4FC1E07` (`ID_JUGADOR`),
  CONSTRAINT `FK3FC038CD4FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK3FC038CD7DEBF27F` FOREIGN KEY (`ID_ALINEACION`) REFERENCES `alineacion` (`ID_ALINEACION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alineacion_suplentes`
--

/*!40000 ALTER TABLE `alineacion_suplentes` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_suplentes` ENABLE KEYS */;


--
-- Definition of table `alineacion_titulares`
--

DROP TABLE IF EXISTS `alineacion_titulares`;
CREATE TABLE `alineacion_titulares` (
  `ID_ALINEACION` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) NOT NULL,
  KEY `FKD8C7CC277DEBF27F` (`ID_ALINEACION`),
  KEY `FKD8C7CC274FC1E07` (`ID_JUGADOR`),
  CONSTRAINT `FKD8C7CC274FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FKD8C7CC277DEBF27F` FOREIGN KEY (`ID_ALINEACION`) REFERENCES `alineacion` (`ID_ALINEACION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alineacion_titulares`
--

/*!40000 ALTER TABLE `alineacion_titulares` DISABLE KEYS */;
INSERT INTO `alineacion_titulares` (`ID_ALINEACION`,`ID_JUGADOR`) VALUES 
 (5,276),
 (5,278),
 (5,279),
 (5,280),
 (5,282),
 (5,284),
 (5,285),
 (6,334),
 (6,335),
 (6,336),
 (6,337),
 (6,339),
 (9,313),
 (9,315),
 (9,316),
 (9,317),
 (9,318),
 (9,319),
 (9,531),
 (9,542),
 (10,410),
 (10,411),
 (10,412),
 (10,413),
 (10,415),
 (10,416),
 (10,419),
 (10,543),
 (11,380),
 (11,381),
 (11,382),
 (11,385),
 (11,386),
 (11,387),
 (11,544),
 (12,388),
 (12,389),
 (12,390),
 (12,391),
 (12,392),
 (12,393),
 (12,394),
 (13,421),
 (13,422),
 (13,423),
 (13,424),
 (13,425),
 (13,427),
 (13,428),
 (13,429),
 (13,430),
 (14,325),
 (14,326),
 (14,327),
 (14,330),
 (14,331),
 (14,332),
 (14,545),
 (14,546),
 (1,357),
 (1,358),
 (1,359),
 (1,360),
 (1,361),
 (1,362),
 (1,367),
 (1,532),
 (1,533),
 (2,343),
 (2,344),
 (2,346),
 (2,347),
 (2,348),
 (2,350),
 (2,353),
 (3,286),
 (3,288),
 (3,289),
 (3,294),
 (3,534),
 (3,535),
 (3,536),
 (4,302),
 (4,304),
 (4,305),
 (4,306),
 (4,307),
 (4,308),
 (4,309),
 (4,537),
 (4,538),
 (7,402),
 (7,403),
 (7,405),
 (7,406),
 (7,408),
 (7,409),
 (7,540),
 (7,541),
 (8,296),
 (8,297),
 (8,298),
 (8,299),
 (8,300),
 (8,301),
 (8,539),
 (15,368),
 (15,369),
 (15,370),
 (15,371),
 (15,372),
 (15,375),
 (15,377),
 (15,378),
 (15,379),
 (16,395),
 (16,396),
 (16,397),
 (16,398),
 (16,399),
 (16,400),
 (16,547),
 (17,334),
 (17,335),
 (17,336),
 (17,337),
 (17,338),
 (17,339),
 (18,296),
 (18,297),
 (18,298),
 (18,300),
 (18,301),
 (18,353),
 (18,539),
 (18,552),
 (18,553),
 (19,357),
 (19,358),
 (19,359),
 (19,360),
 (19,367),
 (19,533),
 (19,556),
 (20,276),
 (20,277),
 (20,278),
 (20,284),
 (20,285),
 (23,388),
 (23,389),
 (23,390),
 (23,391),
 (23,392),
 (23,393),
 (23,394),
 (24,368),
 (24,369),
 (24,370),
 (24,371),
 (24,372),
 (24,375),
 (24,377),
 (24,378),
 (24,379),
 (25,325),
 (25,326),
 (25,327),
 (25,331),
 (25,332),
 (25,546),
 (26,395),
 (26,396),
 (26,397),
 (26,398),
 (26,399),
 (26,400),
 (26,547),
 (27,313),
 (27,315),
 (27,316),
 (27,317),
 (27,318),
 (27,319),
 (27,542),
 (27,550),
 (28,421),
 (28,424),
 (28,425),
 (28,428),
 (28,429),
 (28,559),
 (28,560),
 (29,410),
 (29,411),
 (29,413),
 (29,414),
 (29,415),
 (29,416),
 (29,419),
 (29,543),
 (29,561),
 (30,380),
 (30,381),
 (30,382),
 (30,385),
 (30,386),
 (30,387),
 (30,544),
 (21,302),
 (21,304),
 (21,305),
 (21,306),
 (21,307),
 (21,537),
 (21,554),
 (21,555),
 (22,401),
 (22,403),
 (22,405),
 (22,407),
 (22,408),
 (22,409),
 (22,540),
 (22,558),
 (31,368),
 (31,369),
 (31,370),
 (31,371),
 (31,372),
 (31,375),
 (31,377),
 (31,378),
 (31,379),
 (31,563),
 (32,380),
 (32,381),
 (32,382),
 (32,385),
 (32,386),
 (32,387),
 (32,544),
 (33,395),
 (33,396),
 (33,397),
 (33,398),
 (33,400),
 (34,315),
 (34,316),
 (34,317),
 (34,318),
 (34,319),
 (34,542),
 (35,421),
 (35,422),
 (35,424),
 (35,428),
 (35,429),
 (35,430),
 (36,411),
 (36,412),
 (36,413),
 (36,414),
 (36,416),
 (36,419),
 (36,543),
 (37,403),
 (37,405),
 (37,406),
 (37,408),
 (37,540),
 (37,541),
 (37,558),
 (38,286),
 (38,287),
 (38,288),
 (38,289),
 (38,294),
 (38,295),
 (38,534),
 (38,535),
 (38,536),
 (39,297),
 (39,300),
 (39,353),
 (39,539),
 (39,552),
 (39,553),
 (40,357),
 (40,358),
 (40,359),
 (40,360),
 (40,362),
 (40,533),
 (41,334),
 (41,336),
 (41,337),
 (41,338),
 (41,339),
 (41,565),
 (42,302),
 (42,304),
 (42,306),
 (42,307),
 (42,308),
 (42,537),
 (42,554),
 (42,555),
 (43,568),
 (43,569),
 (43,570),
 (43,571),
 (43,572),
 (43,573),
 (43,574),
 (43,575),
 (43,576),
 (43,577),
 (44,401),
 (44,405),
 (44,408),
 (44,409),
 (44,540),
 (44,580),
 (45,276),
 (45,278),
 (45,279),
 (45,281),
 (45,284),
 (46,296),
 (46,297),
 (46,300),
 (46,301),
 (46,353),
 (46,552),
 (46,553),
 (47,286),
 (47,287),
 (47,288),
 (47,289),
 (47,290),
 (47,295),
 (47,535),
 (48,334),
 (48,336),
 (48,337),
 (48,338),
 (48,339),
 (48,341),
 (49,357),
 (49,358),
 (49,360),
 (49,362),
 (49,532),
 (49,533),
 (49,564),
 (50,302),
 (50,304),
 (50,305),
 (50,306),
 (50,307),
 (50,308),
 (50,537),
 (50,554),
 (50,555),
 (50,566),
 (51,410),
 (51,411),
 (51,412),
 (51,413),
 (51,414),
 (51,419),
 (51,578),
 (51,579),
 (52,368),
 (52,369),
 (52,370),
 (52,371),
 (52,372),
 (52,375),
 (52,377),
 (52,378),
 (52,379),
 (52,563),
 (53,421),
 (53,422),
 (53,423),
 (53,424),
 (53,425),
 (53,428),
 (53,429),
 (53,430),
 (54,395),
 (54,396),
 (54,397),
 (54,398),
 (54,399),
 (54,400),
 (55,380),
 (55,381),
 (55,382),
 (55,385),
 (55,386),
 (55,387),
 (55,544),
 (56,321),
 (56,322),
 (56,323),
 (56,325),
 (56,327),
 (56,330),
 (56,331),
 (56,332),
 (56,546),
 (57,315),
 (57,316),
 (57,317),
 (57,318),
 (57,319),
 (57,542),
 (58,388),
 (58,389),
 (58,390),
 (58,392),
 (58,393),
 (58,394),
 (59,568),
 (59,569),
 (59,570),
 (59,571),
 (59,573),
 (59,574),
 (59,575),
 (59,583),
 (60,286),
 (60,287),
 (60,288),
 (60,289),
 (60,290),
 (60,295),
 (60,535),
 (69,334),
 (69,336),
 (69,337),
 (69,585),
 (69,586),
 (70,401),
 (70,403),
 (70,405),
 (70,408),
 (70,409),
 (70,540),
 (70,558),
 (70,580),
 (61,276),
 (61,278),
 (61,280),
 (61,281),
 (61,282),
 (61,284),
 (61,584),
 (62,568),
 (62,569),
 (62,570),
 (62,571),
 (62,573),
 (62,575),
 (62,576),
 (71,388),
 (71,389),
 (71,390),
 (71,392),
 (71,393),
 (72,421),
 (72,422),
 (72,423),
 (72,424),
 (72,428),
 (72,429),
 (72,430),
 (67,296),
 (67,297),
 (67,301),
 (67,552),
 (67,587),
 (68,568),
 (68,569),
 (68,570),
 (68,571),
 (68,573),
 (68,575),
 (68,583),
 (63,315),
 (63,316),
 (63,317),
 (63,318),
 (63,319),
 (63,542),
 (64,380),
 (64,381),
 (64,385),
 (64,386),
 (64,387),
 (64,544),
 (64,588),
 (65,321),
 (65,322),
 (65,323),
 (65,325),
 (65,326),
 (65,327),
 (65,330),
 (65,331),
 (65,332),
 (66,368),
 (66,369),
 (66,370),
 (66,371),
 (66,372),
 (66,375),
 (66,377),
 (66,378),
 (66,379),
 (66,563),
 (73,395),
 (73,396),
 (73,397),
 (73,398),
 (73,399),
 (73,400),
 (73,547),
 (74,410),
 (74,411),
 (74,412),
 (74,413),
 (74,414),
 (74,419),
 (74,578),
 (75,357),
 (75,358),
 (75,359),
 (75,360),
 (75,361),
 (75,362),
 (75,533),
 (76,286),
 (76,287),
 (76,288),
 (76,289),
 (76,290),
 (76,295),
 (76,535),
 (77,296),
 (77,297),
 (77,299),
 (77,300),
 (77,301),
 (77,539),
 (77,552),
 (77,553),
 (78,302),
 (78,304),
 (78,305),
 (78,307),
 (78,537),
 (78,554),
 (78,566),
 (79,401),
 (79,402),
 (79,403),
 (79,405),
 (79,408),
 (79,409),
 (79,580),
 (80,357),
 (80,358),
 (80,359),
 (80,360),
 (80,362),
 (80,363),
 (80,564),
 (81,568),
 (81,569),
 (81,570),
 (81,571),
 (81,575),
 (81,576),
 (81,583),
 (82,334),
 (82,335),
 (82,336),
 (82,337),
 (82,339),
 (82,565),
 (82,585),
 (83,276),
 (83,278),
 (83,282),
 (83,584),
 (83,589),
 (84,286),
 (84,287),
 (84,288),
 (84,289),
 (84,290),
 (84,295),
 (84,535),
 (84,590),
 (85,423),
 (85,424),
 (85,425),
 (85,428),
 (85,429),
 (85,430),
 (85,595),
 (86,380),
 (86,381),
 (86,382),
 (86,385),
 (86,386),
 (86,387),
 (86,544),
 (89,397),
 (89,398),
 (89,399),
 (89,400),
 (89,592),
 (89,593),
 (89,594),
 (90,389),
 (90,390),
 (90,392),
 (90,393),
 (90,394),
 (90,591),
 (91,368),
 (91,369),
 (91,370),
 (91,371),
 (91,372),
 (91,375),
 (91,377),
 (91,378),
 (91,379),
 (91,563),
 (92,313),
 (92,315),
 (92,317),
 (92,319),
 (92,542),
 (87,410),
 (87,412),
 (87,413),
 (87,414),
 (87,419),
 (87,543),
 (88,321),
 (88,322),
 (88,323),
 (88,326),
 (88,327),
 (88,330),
 (88,331),
 (88,332),
 (93,424),
 (95,554),
 (96,334);
/*!40000 ALTER TABLE `alineacion_titulares` ENABLE KEYS */;


--
-- Definition of table `dummy`
--

DROP TABLE IF EXISTS `dummy`;
CREATE TABLE `dummy` (
  `ID_DUMMY` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID_DUMMY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dummy`
--

/*!40000 ALTER TABLE `dummy` DISABLE KEYS */;
/*!40000 ALTER TABLE `dummy` ENABLE KEYS */;


--
-- Definition of table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
CREATE TABLE `equipo` (
  `ID_EQUIPO` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVO` bit(1) DEFAULT NULL,
  `NOMBRE` varchar(255) DEFAULT NULL,
  `RUTA_IMG_ESCUDO` varchar(255) DEFAULT NULL,
  `IMAGEN_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_EQUIPO`),
  UNIQUE KEY `NOMBRE` (`NOMBRE`),
  KEY `FK7A5B923F8CDC29FD` (`IMAGEN_FK`),
  CONSTRAINT `FK7A5B923F8CDC29FD` FOREIGN KEY (`IMAGEN_FK`) REFERENCES `imagen` (`ID_IMAGEN`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipo`
--

/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
INSERT INTO `equipo` (`ID_EQUIPO`,`ACTIVO`,`NOMBRE`,`RUTA_IMG_ESCUDO`,`IMAGEN_FK`) VALUES 
 (38,0x01,'Banco Provincia',NULL,NULL),
 (39,0x01,'CA Rober Forever',NULL,NULL),
 (40,0x01,'Caño la Liga FC',NULL,NULL),
 (41,0x01,'CK y Deportivo',NULL,NULL),
 (42,0x01,'Deportivo Colonos',NULL,NULL),
 (43,0x01,'El Team',NULL,NULL),
 (44,0x01,'Furia Rosa',NULL,NULL),
 (45,0x01,'HDL',NULL,NULL),
 (46,0x01,'ISDP FC',NULL,NULL),
 (47,0x01,'Juanas de Arco FC',NULL,NULL),
 (48,0x01,'La Liga-Moss FC',NULL,NULL),
 (49,0x01,'Pesutis FC',NULL,NULL),
 (50,0x01,'Santa Terracita',NULL,NULL),
 (51,0x01,'SITAS',NULL,NULL),
 (52,0x01,'Suricatas',NULL,NULL),
 (53,0x01,'Tirate un Caño',NULL,NULL),
 (54,0x01,'West Open',NULL,NULL);
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;


--
-- Definition of table `fecha`
--

DROP TABLE IF EXISTS `fecha`;
CREATE TABLE `fecha` (
  `ID_FECHA` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `idaYVuelta` bit(1) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `TORNEO_FK` bigint(20) DEFAULT NULL,
  `isVuelta` bit(1) DEFAULT NULL,
  `EQUIPO_LIBRE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_FECHA`),
  KEY `FK3FACF5D5DB4EB3D` (`TORNEO_FK`),
  KEY `FK3FACF5DFCD88930` (`EQUIPO_LIBRE`),
  CONSTRAINT `FK3FACF5D5DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`),
  CONSTRAINT `FK3FACF5DFCD88930` FOREIGN KEY (`EQUIPO_LIBRE`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=357 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fecha`
--

/*!40000 ALTER TABLE `fecha` DISABLE KEYS */;
INSERT INTO `fecha` (`ID_FECHA`,`activo`,`idaYVuelta`,`nombre`,`numero`,`TORNEO_FK`,`isVuelta`,`EQUIPO_LIBRE`) VALUES 
 (322,0x01,NULL,'Fecha 1',1,43,0x00,NULL),
 (323,0x01,NULL,'Fecha 2',2,43,0x00,NULL),
 (324,0x01,NULL,'Fecha 3',3,43,0x00,NULL),
 (325,0x01,NULL,'Fecha 4',4,43,0x00,NULL),
 (326,0x01,NULL,'Fecha 5',5,43,0x00,NULL),
 (327,0x01,NULL,'Fecha 6',6,43,0x00,NULL),
 (328,0x01,NULL,'Fecha 7',7,43,0x00,NULL),
 (329,0x01,NULL,'Fecha 1',1,44,0x00,NULL),
 (330,0x01,NULL,'Fecha 2',2,44,0x00,NULL),
 (331,0x01,NULL,'Fecha 3',3,44,0x00,NULL),
 (332,0x01,NULL,'Fecha 4',4,44,0x00,NULL),
 (333,0x01,NULL,'Fecha 5',5,44,0x00,NULL),
 (334,0x01,NULL,'Fecha 6',6,44,0x00,NULL),
 (335,0x01,NULL,'Fecha 7',7,44,0x00,NULL),
 (336,0x01,NULL,'Fecha 1',1,45,0x00,NULL),
 (337,0x01,NULL,'Fecha 2',2,45,0x00,NULL),
 (338,0x01,NULL,'Fecha 3',3,45,0x00,NULL),
 (339,0x01,NULL,'Fecha 4',4,45,0x00,NULL),
 (340,0x01,NULL,'Fecha 5',5,45,0x00,NULL),
 (341,0x01,NULL,'Fecha 6',6,45,0x00,NULL),
 (342,0x01,NULL,'Fecha 7',7,45,0x00,NULL),
 (343,0x01,NULL,'Fecha 1',1,46,0x00,NULL),
 (344,0x01,NULL,'Fecha 2',2,46,0x00,NULL),
 (345,0x01,NULL,'Fecha 3',3,46,0x00,NULL),
 (346,0x01,NULL,'Fecha 4',4,46,0x00,NULL),
 (347,0x01,NULL,'Fecha 5',5,46,0x00,NULL),
 (348,0x01,NULL,'Fecha 6',6,46,0x00,NULL),
 (349,0x01,NULL,'Fecha 7',7,46,0x00,NULL),
 (350,0x01,NULL,'Fecha 8',8,46,0x00,NULL),
 (351,0x01,NULL,'Fecha 9',9,46,0x00,NULL),
 (352,0x01,NULL,'Fecha 10',10,46,0x00,NULL),
 (353,0x01,NULL,'Fecha 11',11,46,0x00,NULL),
 (354,0x01,NULL,'Fecha 12',12,46,0x00,NULL),
 (355,0x01,NULL,'Fecha 13',13,46,0x00,NULL),
 (356,0x01,NULL,'Fecha 14',14,46,0x00,NULL);
/*!40000 ALTER TABLE `fecha` ENABLE KEYS */;


--
-- Definition of table `gol`
--

DROP TABLE IF EXISTS `gol`;
CREATE TABLE `gol` (
  `ID_GOL` bigint(20) NOT NULL AUTO_INCREMENT,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_GOL`),
  KEY `FK11464C42F976B` (`PARTIDO_FK`),
  KEY `FK1146418383FCB` (`JUGADOR_FK`),
  CONSTRAINT `FK1146418383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK11464C42F976B` FOREIGN KEY (`PARTIDO_FK`) REFERENCES `partido` (`ID_PARTIDO`)
) ENGINE=InnoDB AUTO_INCREMENT=449 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gol`
--

/*!40000 ALTER TABLE `gol` DISABLE KEYS */;
INSERT INTO `gol` (`ID_GOL`,`JUGADOR_FK`,`PARTIDO_FK`) VALUES 
 (26,282,1381),
 (27,282,1381),
 (28,284,1381),
 (29,334,1381),
 (30,334,1381),
 (31,335,1381),
 (32,337,1381),
 (33,318,1407),
 (34,531,1407),
 (35,531,1407),
 (36,531,1407),
 (37,315,1407),
 (38,315,1407),
 (39,313,1407),
 (40,542,1407),
 (41,542,1407),
 (42,319,1407),
 (43,319,1407),
 (44,319,1407),
 (45,385,1408),
 (46,385,1408),
 (47,386,1408),
 (48,386,1408),
 (49,387,1408),
 (50,388,1408),
 (51,391,1408),
 (52,389,1408),
 (53,394,1408),
 (54,422,1409),
 (55,422,1409),
 (56,421,1409),
 (57,424,1409),
 (58,424,1409),
 (59,424,1409),
 (60,424,1409),
 (61,424,1409),
 (62,545,1409),
 (71,534,1380),
 (72,535,1380),
 (73,535,1380),
 (74,548,1380),
 (75,537,1380),
 (76,537,1380),
 (77,537,1380),
 (78,537,1380),
 (79,302,1380),
 (80,538,1380),
 (81,538,1380),
 (82,549,1380),
 (83,408,1382),
 (84,402,1382),
 (85,402,1382),
 (86,297,1382),
 (87,296,1382),
 (88,539,1382),
 (89,371,1410),
 (90,371,1410),
 (91,371,1410),
 (92,368,1410),
 (93,379,1410),
 (94,379,1410),
 (95,397,1410),
 (96,397,1410),
 (97,533,1379),
 (98,533,1379),
 (99,334,1386),
 (100,334,1386),
 (101,334,1386),
 (102,335,1386),
 (103,335,1386),
 (104,297,1386),
 (105,300,1386),
 (106,552,1386),
 (107,552,1386),
 (108,552,1386),
 (109,357,1384),
 (110,357,1384),
 (111,357,1384),
 (112,533,1384),
 (113,533,1384),
 (114,533,1384),
 (115,533,1384),
 (116,533,1384),
 (117,533,1384),
 (118,533,1384),
 (119,285,1384),
 (120,278,1384),
 (133,389,1413),
 (134,388,1413),
 (135,368,1413),
 (136,379,1413),
 (137,379,1413),
 (138,332,1414),
 (139,332,1414),
 (140,397,1414),
 (141,397,1414),
 (142,397,1414),
 (143,400,1414),
 (144,547,1414),
 (145,313,1412),
 (146,542,1412),
 (147,550,1412),
 (148,319,1412),
 (149,429,1412),
 (150,424,1412),
 (151,559,1412),
 (152,559,1412),
 (153,560,1412),
 (154,560,1412),
 (155,560,1412),
 (156,560,1412),
 (170,561,1411),
 (171,543,1411),
 (172,385,1411),
 (173,385,1411),
 (174,382,1411),
 (175,387,1411),
 (176,387,1411),
 (177,304,1385),
 (178,304,1385),
 (179,537,1385),
 (180,554,1385),
 (181,554,1385),
 (182,554,1385),
 (183,554,1385),
 (184,554,1385),
 (185,555,1385),
 (186,555,1385),
 (187,408,1385),
 (188,408,1385),
 (196,385,1416),
 (197,386,1416),
 (198,386,1416),
 (199,562,1416),
 (200,397,1417),
 (201,397,1417),
 (202,400,1417),
 (203,400,1417),
 (204,319,1417),
 (205,542,1417),
 (206,421,1415),
 (207,422,1415),
 (208,422,1415),
 (209,422,1415),
 (210,424,1415),
 (211,424,1415),
 (212,424,1415),
 (213,430,1415),
 (214,430,1415),
 (215,414,1415),
 (216,414,1415),
 (217,406,1388),
 (218,406,1388),
 (219,408,1388),
 (220,535,1388),
 (221,535,1388),
 (222,535,1388),
 (223,295,1388),
 (224,353,1389),
 (225,353,1389),
 (226,552,1389),
 (227,533,1389),
 (228,533,1389),
 (229,533,1389),
 (230,533,1389),
 (231,304,1390),
 (232,304,1390),
 (233,304,1390),
 (234,537,1390),
 (235,537,1390),
 (236,554,1390),
 (237,554,1390),
 (238,554,1390),
 (239,554,1390),
 (240,554,1390),
 (241,554,1390),
 (242,554,1390),
 (243,555,1390),
 (244,555,1390),
 (245,555,1390),
 (246,555,1390),
 (247,555,1390),
 (248,555,1390),
 (249,568,1391),
 (250,569,1391),
 (251,569,1391),
 (252,569,1391),
 (253,569,1391),
 (254,570,1391),
 (255,571,1391),
 (256,405,1391),
 (257,408,1391),
 (258,408,1391),
 (259,408,1391),
 (260,408,1391),
 (261,408,1391),
 (262,297,1392),
 (263,300,1392),
 (264,300,1392),
 (265,300,1392),
 (266,353,1392),
 (267,353,1392),
 (268,353,1392),
 (269,552,1392),
 (270,552,1392),
 (271,552,1392),
 (272,552,1392),
 (273,287,1393),
 (274,295,1393),
 (275,295,1393),
 (276,535,1393),
 (277,535,1393),
 (278,535,1393),
 (279,535,1393),
 (280,535,1393),
 (281,535,1393),
 (282,337,1393),
 (283,533,1394),
 (284,533,1394),
 (285,305,1394),
 (286,537,1394),
 (287,554,1394),
 (288,554,1394),
 (289,554,1394),
 (290,555,1394),
 (291,555,1394),
 (292,368,1419),
 (293,368,1419),
 (294,371,1419),
 (295,379,1419),
 (296,379,1419),
 (297,379,1419),
 (298,422,1420),
 (299,424,1420),
 (300,424,1420),
 (301,424,1420),
 (302,430,1420),
 (303,430,1420),
 (304,397,1420),
 (305,397,1420),
 (306,382,1421),
 (307,385,1421),
 (308,385,1421),
 (309,386,1421),
 (310,386,1421),
 (311,387,1421),
 (312,318,1422),
 (313,319,1422),
 (314,542,1422),
 (315,542,1422),
 (321,569,1383),
 (322,569,1383),
 (323,569,1383),
 (324,571,1383),
 (325,295,1383),
 (351,334,1396),
 (352,334,1396),
 (353,337,1396),
 (354,401,1396),
 (355,405,1396),
 (356,405,1396),
 (357,405,1396),
 (358,408,1396),
 (359,568,1387),
 (360,569,1387),
 (361,569,1387),
 (362,388,1425),
 (363,422,1425),
 (364,424,1425),
 (374,552,1395),
 (375,569,1395),
 (376,569,1395),
 (377,542,1426),
 (378,542,1426),
 (379,542,1426),
 (380,380,1426),
 (381,385,1426),
 (382,387,1426),
 (383,588,1426),
 (384,332,1424),
 (385,332,1424),
 (386,371,1424),
 (387,371,1424),
 (388,396,1423),
 (389,400,1423),
 (390,412,1423),
 (391,414,1423),
 (392,357,1398),
 (393,533,1398),
 (394,533,1398),
 (395,533,1398),
 (396,533,1398),
 (397,552,1400),
 (398,304,1400),
 (399,554,1400),
 (400,408,1401),
 (401,408,1401),
 (402,408,1401),
 (403,402,1401),
 (404,358,1401),
 (405,358,1401),
 (406,358,1401),
 (407,363,1401),
 (408,569,1399),
 (409,569,1399),
 (410,571,1399),
 (411,575,1399),
 (412,575,1399),
 (413,584,1402),
 (414,535,1402),
 (415,535,1402),
 (416,535,1402),
 (417,535,1402),
 (418,295,1402),
 (419,430,1430),
 (420,430,1430),
 (421,595,1430),
 (422,595,1430),
 (423,595,1430),
 (424,386,1430),
 (425,387,1430),
 (426,387,1430),
 (428,397,1428),
 (429,398,1428),
 (430,400,1428),
 (431,389,1428),
 (432,394,1428),
 (433,591,1428),
 (434,379,1429),
 (435,379,1429),
 (436,319,1429),
 (437,313,1429),
 (438,542,1429),
 (439,332,1427),
 (440,424,1465),
 (441,424,1465),
 (442,554,1435),
 (443,554,1435),
 (444,554,1435),
 (445,554,1435),
 (446,334,1435),
 (447,334,1435),
 (448,334,1435);
/*!40000 ALTER TABLE `gol` ENABLE KEYS */;


--
-- Definition of table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
CREATE TABLE `grupo` (
  `ID_GRUPO` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `JUGADOR_CREADOR_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_GRUPO`),
  KEY `FK40F144940F0ADCE` (`JUGADOR_CREADOR_FK`),
  CONSTRAINT `FK40F144940F0ADCE` FOREIGN KEY (`JUGADOR_CREADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupo`
--

/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;


--
-- Definition of table `grupo_jugador`
--

DROP TABLE IF EXISTS `grupo_jugador`;
CREATE TABLE `grupo_jugador` (
  `ID_GRUPO` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) NOT NULL,
  KEY `FK916E89AC4FC1E07` (`ID_JUGADOR`),
  KEY `FK916E89AC9892C415` (`ID_GRUPO`),
  CONSTRAINT `FK916E89AC4FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK916E89AC9892C415` FOREIGN KEY (`ID_GRUPO`) REFERENCES `grupo` (`ID_GRUPO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupo_jugador`
--

/*!40000 ALTER TABLE `grupo_jugador` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo_jugador` ENABLE KEYS */;


--
-- Definition of table `imagen`
--

DROP TABLE IF EXISTS `imagen`;
CREATE TABLE `imagen` (
  `ID_IMAGEN` bigint(20) NOT NULL AUTO_INCREMENT,
  `imgSerialiazda` blob,
  PRIMARY KEY (`ID_IMAGEN`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imagen`
--

/*!40000 ALTER TABLE `imagen` DISABLE KEYS */;
INSERT INTO `imagen` (`ID_IMAGEN`,`imgSerialiazda`) VALUES 
 (1,NULL),
 (2,0x47494638396119000F00B30900CCB27FEDDB99FFFFEB663300996600CCCCCC99999999CCFF66666600000000000000000000000000000000000000000021F90401000009002C0000000019000F00400461302149E74C386B3D85F91F728CE4B5611DF809E2911CE64905744D23459E03C480E52884704894C952AAD66B14DB20418240B177021AAFBC82B3C2BD6E8D456F4740262B4BD7719945428313EB728B2986C7A33820CF970112FF423A197B560911003B);
/*!40000 ALTER TABLE `imagen` ENABLE KEYS */;


--
-- Definition of table `invitacion`
--

DROP TABLE IF EXISTS `invitacion`;
CREATE TABLE `invitacion` (
  `ID_INVITACION` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado` varchar(255) DEFAULT NULL,
  `mensaje` varchar(255) DEFAULT NULL,
  `ID_JUGADOR` bigint(20) DEFAULT NULL,
  `ID_PARTIDO_AMISTOSO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_INVITACION`),
  KEY `FK6421A94A4FC1E07` (`ID_JUGADOR`),
  KEY `FK6421A94A23481C4` (`ID_PARTIDO_AMISTOSO`),
  CONSTRAINT `FK6421A94A23481C4` FOREIGN KEY (`ID_PARTIDO_AMISTOSO`) REFERENCES `partido_amistoso` (`ID_PARTIDO_AMISTOSO`),
  CONSTRAINT `FK6421A94A4FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitacion`
--

/*!40000 ALTER TABLE `invitacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `invitacion` ENABLE KEYS */;


--
-- Definition of table `item_menu`
--

DROP TABLE IF EXISTS `item_menu`;
CREATE TABLE `item_menu` (
  `ID_ITEM_MENU` bigint(20) NOT NULL AUTO_INCREMENT,
  `FECHA_CREACION` datetime DEFAULT NULL,
  `FECHA_ULTIMA_MODIFICACION` datetime DEFAULT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `ORDEN` bigint(20) DEFAULT NULL,
  `TITULO` varchar(255) DEFAULT NULL,
  `ACCION` bigint(20) DEFAULT NULL,
  `USUARIO_CREACION` bigint(20) DEFAULT NULL,
  `USUARIO_ULTIMA_MODIFICACION` bigint(20) DEFAULT NULL,
  `ITEM_MENU_PADRE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ITEM_MENU`),
  KEY `FK2838470B21D8C9D8` (`USUARIO_CREACION`),
  KEY `FK2838470BF5191012` (`USUARIO_ULTIMA_MODIFICACION`),
  KEY `FK2838470B163E2E85` (`ACCION`),
  KEY `FK2838470B60B8561D` (`ITEM_MENU_PADRE`),
  CONSTRAINT `FK2838470B163E2E85` FOREIGN KEY (`ACCION`) REFERENCES `accion` (`ID_ACCION`),
  CONSTRAINT `FK2838470B21D8C9D8` FOREIGN KEY (`USUARIO_CREACION`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FK2838470B60B8561D` FOREIGN KEY (`ITEM_MENU_PADRE`) REFERENCES `item_menu` (`ID_ITEM_MENU`),
  CONSTRAINT `FK2838470BF5191012` FOREIGN KEY (`USUARIO_ULTIMA_MODIFICACION`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_menu`
--

/*!40000 ALTER TABLE `item_menu` DISABLE KEYS */;
INSERT INTO `item_menu` (`ID_ITEM_MENU`,`FECHA_CREACION`,`FECHA_ULTIMA_MODIFICACION`,`DESCRIPCION`,`ORDEN`,`TITULO`,`ACCION`,`USUARIO_CREACION`,`USUARIO_ULTIMA_MODIFICACION`,`ITEM_MENU_PADRE`) VALUES 
 (171,NULL,NULL,NULL,NULL,'Usuarios',171,NULL,NULL,NULL),
 (172,NULL,NULL,NULL,NULL,'Perfiles',172,NULL,NULL,NULL),
 (173,NULL,NULL,NULL,NULL,'Jugadores',173,NULL,NULL,NULL),
 (174,NULL,NULL,NULL,NULL,'Equipos',174,NULL,NULL,NULL),
 (175,NULL,NULL,NULL,NULL,'Torneos',175,NULL,NULL,NULL),
 (176,NULL,NULL,NULL,NULL,'Grupos',176,NULL,NULL,NULL),
 (177,NULL,NULL,NULL,NULL,'Partidos',177,NULL,NULL,NULL),
 (178,NULL,NULL,NULL,NULL,'Usuario',178,NULL,NULL,NULL),
 (179,NULL,NULL,NULL,NULL,'Jugador',179,NULL,NULL,NULL),
 (180,NULL,NULL,NULL,NULL,'Equipos',180,NULL,NULL,NULL),
 (181,NULL,NULL,NULL,NULL,'Torneos',181,NULL,NULL,NULL),
 (182,NULL,NULL,NULL,NULL,'Grupos',182,NULL,NULL,NULL),
 (183,NULL,NULL,NULL,NULL,'Partidos',183,NULL,NULL,NULL),
 (184,NULL,NULL,NULL,NULL,'Invitaciones',184,NULL,NULL,NULL);
/*!40000 ALTER TABLE `item_menu` ENABLE KEYS */;


--
-- Definition of table `jugador`
--

DROP TABLE IF EXISTS `jugador`;
CREATE TABLE `jugador` (
  `ID_JUGADOR` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `apodo` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `EQUIPO_FK` bigint(20) DEFAULT NULL,
  `USUARIO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_JUGADOR`),
  KEY `FKDFA027A2FABF067D` (`EQUIPO_FK`),
  KEY `FKDFA027A2C3F3AACD` (`USUARIO_FK`),
  CONSTRAINT `FKDFA027A2C3F3AACD` FOREIGN KEY (`USUARIO_FK`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FKDFA027A2FABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=596 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jugador`
--

/*!40000 ALTER TABLE `jugador` DISABLE KEYS */;
INSERT INTO `jugador` (`ID_JUGADOR`,`activo`,`apellido`,`apodo`,`nombre`,`EQUIPO_FK`,`USUARIO_FK`) VALUES 
 (276,0x01,'Decuzzi',NULL,'Jennifer',38,2),
 (277,0x01,'Alfonso',NULL,'Guadalupe',38,NULL),
 (278,0x01,'Risso Patron',NULL,'Belen',38,NULL),
 (279,0x01,'Albarracin',NULL,'Victoria',38,NULL),
 (280,0x01,'Albarracin',NULL,'Carolina',38,NULL),
 (281,0x01,'Gonzalez',NULL,'Macarena',38,NULL),
 (282,0x01,'Fontana',NULL,'Antonella',38,NULL),
 (283,0x01,'Roucco',NULL,'Mariana',38,NULL),
 (284,0x01,'Fiorino',NULL,'Ailen',38,NULL),
 (285,0x01,'Quiroga',NULL,'Micaela',38,NULL),
 (286,0x01,'Rial',NULL,'Solange',39,NULL),
 (287,0x01,'Avila',NULL,'Cintia',39,NULL),
 (288,0x01,'Breuer',NULL,'Jesica',39,NULL),
 (289,0x01,'d´Imperio',NULL,'Rosario',39,NULL),
 (290,0x01,'Rocio',NULL,'Ana',39,NULL),
 (291,0x01,'Albanese',NULL,'Camila',39,NULL),
 (292,0x01,'Del Puerto',NULL,'Angeles',39,NULL),
 (293,0x01,'Kovasevic',NULL,'Sol',39,NULL),
 (294,0x01,'Almandoz',NULL,'Ailen',39,NULL),
 (295,0x01,'Gonzalez',NULL,'Mariela',39,NULL),
 (296,0x01,'Guglielmo',NULL,'Paula',40,NULL),
 (297,0x01,'Santa Cruz',NULL,'Agustina',40,NULL),
 (298,0x01,'Bleuler',NULL,'Florencia',40,NULL),
 (299,0x01,'Castro',NULL,'Jesica',40,NULL),
 (300,0x01,'Benegas',NULL,'Valeria',40,NULL),
 (301,0x01,'Rosello',NULL,'Agostina',40,NULL),
 (302,0x01,'Pardo',NULL,'Karen',41,NULL),
 (303,0x01,'Sohns',NULL,'Jesica',NULL,NULL),
 (304,0x01,'Ortiz',NULL,'Analia',41,NULL),
 (305,0x01,'Alegre',NULL,'Natalia',41,NULL),
 (306,0x01,'Bususcovich',NULL,'Andrea',41,NULL),
 (307,0x01,'Codevilla',NULL,'Claudia',41,NULL),
 (308,0x01,'Garcia',NULL,'Natividad',41,NULL),
 (309,0x01,'Mattaliano',NULL,'Paola',41,NULL),
 (310,0x01,'Cannan',NULL,'Cecilia',41,NULL),
 (311,0x01,'Oliva',NULL,'Estela Maris',41,NULL),
 (312,0x01,'Sanchez',NULL,'Laura',41,NULL),
 (313,0x01,'Giudice',NULL,'Antonella',42,NULL),
 (314,0x01,'Dias de Leon',NULL,'Julieta',42,NULL),
 (315,0x01,'Sanchez',NULL,'Agustina',42,NULL),
 (316,0x01,'Viera',NULL,'Sofia',42,NULL),
 (317,0x01,'Lares',NULL,'Magali',42,NULL),
 (318,0x01,'Alvear',NULL,'Aldana',42,NULL),
 (319,0x01,'Davidoff',NULL,'Carolina',42,NULL),
 (320,0x01,'Cordova',NULL,'Barbara',42,NULL),
 (321,0x01,'Pillani',NULL,'Fiorella',43,NULL),
 (322,0x01,'Pillani',NULL,'Antonella',43,NULL),
 (323,0x01,'Cintolo',NULL,'Carolina',43,NULL),
 (324,0x01,'Linares',NULL,'Sofia',43,NULL),
 (325,0x01,'Crespo',NULL,'Carla',43,NULL),
 (326,0x01,'Leanza',NULL,'Maria Eugenia',43,NULL),
 (327,0x01,'Policarpi',NULL,'Florencia',43,NULL),
 (328,0x01,'Garcia',NULL,'Lucia',43,NULL),
 (329,0x01,'Patrone',NULL,'Stefania',43,NULL),
 (330,0x01,'Cepeda',NULL,'Lucia',43,NULL),
 (331,0x01,'Sulzberger',NULL,'Melanie',43,NULL),
 (332,0x01,'Uicich',NULL,'Belén',43,NULL),
 (333,0x01,'Yagueddu',NULL,'Maria Florencia',43,NULL),
 (334,0x01,'Ischia',NULL,'Nina',44,NULL),
 (335,0x01,'Parra',NULL,'Macarena',44,NULL),
 (336,0x01,'Torre',NULL,'Victoria',44,NULL),
 (337,0x01,'Gramajo',NULL,'Fany',44,NULL),
 (338,0x01,'Aversa',NULL,'Giuliana',44,NULL),
 (339,0x01,'Grondona',NULL,'Julia',44,NULL),
 (340,0x01,'De Luca',NULL,'Flavia',44,NULL),
 (341,0x01,'Varela',NULL,'Agustina',44,NULL),
 (342,0x01,'Civitillo',NULL,'Mariela',44,NULL),
 (343,0x01,'De Simone',NULL,'Lucia',45,NULL),
 (344,0x01,'Banegas',NULL,'Anabella',45,NULL),
 (345,0x01,'Ceruzzi',NULL,'Rocio',45,NULL),
 (346,0x01,'Clauzure',NULL,'Yamila',45,NULL),
 (347,0x01,'Cortez',NULL,'Ayelen',45,NULL),
 (348,0x01,'Echeverria',NULL,'Roberta',45,NULL),
 (349,0x01,'Fernandez',NULL,'Nadia',45,NULL),
 (350,0x01,'Julia',NULL,'Lucia',45,NULL),
 (351,0x01,'Lagos',NULL,'Ayelen',45,NULL),
 (352,0x01,'Laviña',NULL,'Mariana',45,NULL),
 (353,0x01,'López',NULL,'M. Soledad',40,NULL),
 (354,0x01,'Otero Rey',NULL,'Melisa',45,NULL),
 (355,0x01,'Sammartino',NULL,'Mariana',45,NULL),
 (356,0x01,'Teisseire',NULL,'Carolina',45,NULL),
 (357,0x01,'Cavalieri',NULL,'Micaela',46,NULL),
 (358,0x01,'Cavalieri',NULL,'Maylen',46,NULL),
 (359,0x01,'Herrera',NULL,'Priscila',46,NULL),
 (360,0x01,'Berta',NULL,'Sheila',46,NULL),
 (361,0x01,'Premio',NULL,'Romina',46,NULL),
 (362,0x01,'Agazzi',NULL,'Paula',46,NULL),
 (363,0x01,'Rivero',NULL,'Gladys',46,NULL),
 (364,0x01,'Nahir',NULL,'Cardoso',46,NULL),
 (365,0x01,'Pattacini',NULL,'Rocio',46,NULL),
 (366,0x01,'Pattacini',NULL,'Sol',46,NULL),
 (367,0x01,'Galeano',NULL,'Giselle',46,NULL),
 (368,0x01,'Zanellato',NULL,'Ivana',47,NULL),
 (369,0x01,'Parareda',NULL,'María Agustina',47,NULL),
 (370,0x01,'Zanellato',NULL,'Nadina',47,NULL),
 (371,0x01,'Mazzocchi',NULL,'María Laura',47,NULL),
 (372,0x01,'Adalian',NULL,'Priscila',47,NULL),
 (373,0x01,'Lema',NULL,'María Belen',47,NULL),
 (374,0x01,'de la Vega',NULL,'Florencia',NULL,NULL),
 (375,0x01,'Gonzalez Moss',NULL,'Emilia',47,3),
 (376,0x01,'Rinesi',NULL,'Pamela',47,NULL),
 (377,0x01,'Zamparo',NULL,'Daniela',47,NULL),
 (378,0x01,'Foglino',NULL,'Macarena',47,NULL),
 (379,0x01,'Merlos',NULL,'Maria Sol',47,NULL),
 (380,0x01,'Borsato',NULL,'Maria Agustina',48,NULL),
 (381,0x01,'Cosentino',NULL,'Nerina',48,NULL),
 (382,0x01,'Venditti',NULL,'Carolina',48,NULL),
 (383,0x01,'Cuitiño',NULL,'Nadia',NULL,NULL),
 (384,0x01,'Merlo',NULL,'Aldana',NULL,NULL),
 (385,0x01,'Borsato',NULL,'Maria Mercedes',48,NULL),
 (386,0x01,'Lemme',NULL,'Milagros',48,NULL),
 (387,0x01,'Della Villa',NULL,'Constanza',48,NULL),
 (388,0x01,'Nieto',NULL,'Tamara',49,NULL),
 (389,0x01,'Aguirre',NULL,'Estefania',49,NULL),
 (390,0x01,'Nieto',NULL,'Micaela',49,NULL),
 (391,0x01,'Nieto',NULL,'Yanina',49,NULL),
 (392,0x01,'Pizzagalli',NULL,'Johanna',49,NULL),
 (393,0x01,'Saccone',NULL,'Ana Carolina',49,NULL),
 (394,0x01,'Lacolla',NULL,'Carla',49,NULL),
 (395,0x01,'Belardita',NULL,'Paula',50,NULL),
 (396,0x01,'Claut',NULL,'Natalí',50,NULL),
 (397,0x01,'Buontempo',NULL,'Magali',50,NULL),
 (398,0x01,'Albini',NULL,'Belen',50,NULL),
 (399,0x01,'Ramirez',NULL,'Ivana',50,NULL),
 (400,0x01,'Alagastino',NULL,'Gisele',50,NULL),
 (401,0x01,'Duarte',NULL,'Estefania',51,NULL),
 (402,0x01,'Roldan',NULL,'Romina',51,NULL),
 (403,0x01,'Pepe',NULL,'Juana',51,NULL),
 (404,0x01,'Palacio',NULL,'Ivanna',51,NULL),
 (405,0x01,'Avendaño',NULL,'Luz',51,NULL),
 (406,0x01,'Azevedo',NULL,'Loana',51,NULL),
 (407,0x01,'Rodriguez',NULL,'Cintia',51,NULL),
 (408,0x01,'Forti',NULL,'Camila',51,NULL),
 (409,0x01,'Massabie',NULL,'Macarena',51,NULL),
 (410,0x01,'Lobo',NULL,'Ximena',52,NULL),
 (411,0x01,'Corvera Pose',NULL,'Victoria',52,NULL),
 (412,0x01,'Galasso',NULL,'Alina',52,NULL),
 (413,0x01,'Medina',NULL,'Maira',52,NULL),
 (414,0x01,'Liern',NULL,'Micaela',52,NULL),
 (415,0x01,'Betti',NULL,'Soledad',52,NULL),
 (416,0x01,'Betti',NULL,'Paula',52,NULL),
 (417,0x01,'Avila',NULL,'Mariel',52,NULL),
 (418,0x01,'Covatta',NULL,'Valeria',52,NULL),
 (419,0x01,'Andriani',NULL,'Silvina',52,NULL),
 (420,0x01,'Frascarelli',NULL,'Antonela',53,NULL),
 (421,0x01,'Imparato',NULL,'Giuliana',53,NULL),
 (422,0x01,'Imparato',NULL,'Eugenia',53,NULL),
 (423,0x01,'Paolillo',NULL,'Vanina',53,NULL),
 (424,0x01,'Indart',NULL,'Paloma',53,NULL),
 (425,0x01,'Rascon',NULL,'Bárbara',53,NULL),
 (426,0x01,'López Pumarega',NULL,'Sofía',53,NULL),
 (427,0x01,'Pros',NULL,'Julieta',53,NULL),
 (428,0x01,'Rodríguez',NULL,'Romina',53,NULL),
 (429,0x01,'Balboa',NULL,'Martina',53,NULL),
 (430,0x01,'Grau',NULL,'Victoria',53,NULL),
 (431,0x01,'Piazzi',NULL,'Luciana',53,NULL),
 (531,0x01,'Rodriguez','','Vanesa Fernanda',42,NULL),
 (532,0x01,'Arroyo','','Jesica',46,NULL),
 (533,0x01,'Billoni Ahumada','','Malen',46,NULL),
 (534,0x01,'Franco','','Aylen',39,NULL),
 (535,0x01,'Gonzalez','','Aldana',39,NULL),
 (536,0x01,'Sede','','Ana',39,NULL),
 (537,0x01,'Cordoba','','Alejandra',41,NULL),
 (538,0x01,'Pivero','','Michelle',41,NULL),
 (539,0x01,'Quintana','','Yamila',40,NULL),
 (540,0x01,'Avendaño','','Camila',51,NULL),
 (541,0x01,'Rodriguez','','Macarena',51,NULL),
 (542,0x01,'Carrasco','','María Florencia',42,NULL),
 (543,0x01,'Galasso','','Nerea',52,NULL),
 (544,0x01,'Dal Borgo','','Vanesa',48,NULL),
 (545,0x01,'Salgado','','Sofia',43,NULL),
 (546,0x01,'Ulloa','','Leonela',43,NULL),
 (547,0x01,'Rojas','','Karina',50,NULL),
 (548,0x00,'contra','','en',39,NULL),
 (549,0x00,'contra','','en',41,NULL),
 (550,0x01,'Villarruel','','Mariana Cecilia',42,NULL),
 (552,0x01,'de Tagliaferro','','Gisele',40,NULL),
 (553,0x01,'Galarreta','','Juliana',40,NULL),
 (554,0x01,'Nicoleta','','Maria',41,NULL),
 (555,0x01,'Esteche','','Soledad',41,NULL),
 (556,0x01,'Segovini','','Tamara',46,NULL),
 (558,0x01,'Meloni','','Giuliana',51,NULL),
 (559,0x01,'Vazquez','','Constanza',53,NULL),
 (560,0x00,'Taker','','Caterina',53,NULL),
 (561,0x00,'contra','','en',52,NULL),
 (562,0x00,'contra','','en',48,NULL),
 (563,0x01,'de la Vega','','María Florencia',47,NULL),
 (564,0x01,'Terranova','','Daiana',46,NULL),
 (565,0x01,'Albacete','','Agustina',44,NULL),
 (566,0x01,'Lopez','','Cristina',41,NULL),
 (567,0x01,'Bergman',NULL,'Ignacio',NULL,4),
 (568,0x01,'Wolfsteiner','','Marisa',54,NULL),
 (569,0x01,'Joncic','','Cinthya',54,NULL),
 (570,0x01,'Caballero Bordón','','María Belén',54,NULL),
 (571,0x01,'Pedernera','','Romina',54,NULL),
 (572,0x01,'Porro','','Cristina',54,NULL),
 (573,0x01,'Quiroga','','Noemí',54,NULL),
 (574,0x01,'Neira','','Ivanna',54,NULL),
 (575,0x01,'Romero','','Stella Maris',54,NULL),
 (576,0x01,'Esmoris','','Yamila',54,NULL),
 (577,0x01,'Langone','','Adriana',54,NULL),
 (578,0x01,'Mairotti','','Florencia',52,NULL),
 (579,0x01,'Merlo','','Aldana',52,NULL),
 (580,0x01,'Mariscotti','','Florencia',51,NULL),
 (581,0x01,'Perez','','Melany',38,NULL),
 (582,0x01,'Perez','','Denise',38,NULL),
 (583,0x01,'Tammaro','','Magalí',54,NULL),
 (584,0x01,'Lancetti','','Jimena',38,NULL),
 (585,0x01,'Heineken','','Mariana',44,NULL),
 (586,0x01,'Rossi','','Maria José',44,NULL),
 (587,0x01,'Compagnucci','','Merlina',40,NULL),
 (588,0x01,'Chaldu','','Melina',48,NULL),
 (589,0x01,'Nuñez','','Agustina',38,NULL),
 (590,0x01,'d´Imperio','','Victoria',39,NULL),
 (591,0x01,'Zaniratto','','Florencia',49,NULL),
 (592,0x01,'Schuarte','','Agostina',50,NULL),
 (593,0x01,'Sala','','Noelia',50,NULL),
 (594,0x01,'Coria','','Inés',50,NULL),
 (595,0x01,'Ferrero','','Paula',53,NULL);
/*!40000 ALTER TABLE `jugador` ENABLE KEYS */;


--
-- Definition of table `jugador_temp`
--

DROP TABLE IF EXISTS `jugador_temp`;
CREATE TABLE `jugador_temp` (
  `equipo` char(100) DEFAULT NULL,
  `apellido` char(100) DEFAULT NULL,
  `nombre` char(100) DEFAULT NULL,
  `mail` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jugador_temp`
--

/*!40000 ALTER TABLE `jugador_temp` DISABLE KEYS */;
INSERT INTO `jugador_temp` (`equipo`,`apellido`,`nombre`,`mail`) VALUES 
 ('Banco Provincia','Decuzzi','Jennifer','jenny_Dq_17@hotmail.com'),
 ('Banco Provincia','Alfonso','Guadalupe','guadamar@hotmail.com'),
 ('Banco Provincia','Risso Patron','Belen','Belu_risso@hotmail.com'),
 ('Banco Provincia','Albarracin','Victoria','victoria.s.albarracin@hotmail.com'),
 ('Banco Provincia','Albarracin','Carolina','carolina.l.albarracin@hotmail.com'),
 ('Banco Provincia','Gonzalez','Macarena','maca.gonzalez11@hotmail.com'),
 ('Banco Provincia','Fontana','Antonella','antofontis.g@hotmail.com'),
 ('Banco Provincia','Roucco','Mariana',''),
 ('Banco Provincia','Fiorino','Ailen','ailu.fiorino@hotmail.com'),
 ('Banco Provincia','Quiroga','Micaela','miku_q@hotmail.com'),
 ('CA Rober Forever','Rial','Solange','solange_rial@hotmail.com'),
 ('CA Rober Forever','Avila','Cintia','avilacin@hotmail.com'),
 ('CA Rober Forever','Breuer','Jesica','jesicabreuer86@hotmail.com'),
 ('CA Rober Forever','d´Imperio','Rosario','rosariodimperio@hotmail.com'),
 ('CA Rober Forever','Rocio','Ana','ana.rocio@live.com.ar'),
 ('CA Rober Forever','Albanese','Camila','camiladeaguero@hotmail.com'),
 ('CA Rober Forever','Del Puerto','Angeles','angeles.delpuerto@hotmail.com'),
 ('CA Rober Forever','Kovasevic','Sol','solkovasevic@hotmail.com'),
 ('CA Rober Forever','Almandoz','Ailen',''),
 ('CA Rober Forever','Gonzalez','Mariela',''),
 ('Caño la Liga FC','Guglielmo','Paula','Paula.guglielmo@hotmail.com'),
 ('Caño la Liga FC','Santa Cruz','Agustina','Agus-santacruz@hotmail.com'),
 ('Caño la Liga FC','Bleuler','Florencia','Flo._09@hotmail.com'),
 ('Caño la Liga FC','Castro','Jesica','lajeessii@hotmail.com'),
 ('Caño la Liga FC','Benegas','Valeria','vale_02@hotmail.com'),
 ('Caño la Liga FC','Rosello','Agostina','aaagos@hotmail.com'),
 ('Club Kar y Deportivo','Pardo','Karen','karenpardo76@hotmail.com'),
 ('Club Kar y Deportivo','Sohns','Jesica','ginat_hot@hotmail.com'),
 ('Club Kar y Deportivo','Ortiz','Analia','any_1422@hotmail.com'),
 ('Club Kar y Deportivo','Alegre','Natalia',''),
 ('Club Kar y Deportivo','Bususcovich','Andrea','yamefui09@hotmail.com'),
 ('Club Kar y Deportivo','Codevilla','Claudia','k.aiu69@hotmail.com'),
 ('Club Kar y Deportivo','Garcia','Natividad','natividadtequiero@hotmail.com'),
 ('Club Kar y Deportivo','Mattaliano','Paola','fernandamattaliano@hotmial.com'),
 ('Club Kar y Deportivo','Cannan','Cecilia',''),
 ('Club Kar y Deportivo','Oliva','Estela Maris',''),
 ('Club Kar y Deportivo','Sanchez','Laura','lauvi05@hotmail.com'),
 ('Deportivo Colonos','Giudice','Antonella','an.giudice@hotmail.com'),
 ('Deportivo Colonos','Dias de Leon','Julieta','julietadiasdeleon@hotmail.com'),
 ('Deportivo Colonos','Sanchez','Agustina','agus_9015@hotmail.com'),
 ('Deportivo Colonos','Viera','Sofia','ro69zn@hotmail.com'),
 ('Deportivo Colonos','Lares','Magali','loca2702@hotmail.com'),
 ('Deportivo Colonos','Alvear','Aldana','alvearaldana@hotmail.com'),
 ('Deportivo Colonos','Davidoff','Carolina','carochiss_14@hotmail.com'),
 ('Deportivo Colonos','Cordova','Barbara','baredith86@hotmail.com'),
 ('El Team','Pillani','Fiorella','fiorella.pillani@hotmail.com'),
 ('El Team','Pillani','Antonella','anti26@hotmail.com'),
 ('El Team','Cintolo','Carolina','cintolo.carolina@hotmail.com'),
 ('El Team','Linares','Sofia','linares.sofia@hotmail.com'),
 ('El Team','Crespo','Carla','crespo.carr@hotmail.com'),
 ('El Team','Leanza','Maria Eugenia','eugee.leanza@hotmail.com'),
 ('El Team','Policarpi','Florencia','florenciaa._@hotmail.com'),
 ('El Team','Garcia','Lucia','lulaia_@hotmail.com'),
 ('El Team','Patrone','Stefania','stefi_patrone@hotmail.com'),
 ('El Team','Cepeda','Lucia','lu-cepada@hotmail.com'),
 ('El Team','Sulzberger','Melanie','melisulz@hotmail.com'),
 ('El Team','Uicich','Belén','belu_uicich@hotmail.com'),
 ('El Team','Yagueddu','Maria Florencia','flor_yague@hotmail.com'),
 ('Furia Rosa','Ischia','Nina','nina_ischia17@hotmail.com'),
 ('Furia Rosa','Parra','Macarena','maca_parra@hotmail.com'),
 ('Furia Rosa','Torre','Victoria','mvt17_11@hotmail.com'),
 ('Furia Rosa','Gramajo','Fany','funny.gra@hotmail.com'),
 ('Furia Rosa','Aversa','Giuliana','giuli.aversa@hotmail.com'),
 ('Furia Rosa','Grondona','Julia','majugrondona@hotmail.com'),
 ('Furia Rosa','De Luca','Flavia','flavi_287@hotmail.com'),
 ('Furia Rosa','Varela','Agustina','varelaagustina@hotmail.com'),
 ('Furia Rosa','Civitillo','Mariela','marielacivitillo@hotmail.com'),
 ('HDL','De Simone','Lucia','lunadesi@hotmail.com'),
 ('HDL','Banegas','Anabella','yeimi_bc_12@hotmail.com'),
 ('HDL','Ceruzzi','Rocio','ro10@hotmail.com'),
 ('HDL','Clauzure','Yamila','yamy_vpc_88@hotmail.com'),
 ('HDL','Cortez','Ayelen','ayelore_23rei@hotmail.com'),
 ('HDL','Echeverria','Roberta','robycon_2003@hotmail.com'),
 ('HDL','Fernandez','Nadia','elglobodesanjusto@hotmail.com'),
 ('HDL','Julia','Lucia','lucia.julia.r@hotmail.com'),
 ('HDL','Lagos','Ayelen','aye10_nqn@hotmail.com'),
 ('HDL','Laviña','Mariana','mari16_larubia22@hotmail.com'),
 ('HDL','Lòpez','M. Soledad','solelopez13@hotmail.com'),
 ('HDL','Otero Rey','Melisa','m.e.l.10@hotmail.com'),
 ('HDL','Sammartino','Mariana','living-the-vida-loka@hotmail.com'),
 ('HDL','Teisseire','Carolina','karito_palermo_87@hotmail.com'),
 ('I See Dead People FC','Cavalieri','Micaela','micaela.cavalieri@hotmail.com'),
 ('I See Dead People FC','Cavalieri','Maylen','may_gallinita@hotmail.com'),
 ('I See Dead People FC','Herrera','Priscila','pricila_128@hotmail.com'),
 ('I See Dead People FC','Berta','Sheila','shey_x7uk@hotmail.com'),
 ('I See Dead People FC','Premio','Romina','romina_premio@hotmail.com'),
 ('I See Dead People FC','Agazzi','Paula','paulaagazzi@hotmail.com'),
 ('I See Dead People FC','Rivero','Gladys',''),
 ('I See Dead People FC','Nahir','Cardoso',''),
 ('I See Dead People FC','Pattacini','Rocio','roochii_26@hotmail.com'),
 ('I See Dead People FC','Pattacini','Sol',''),
 ('I See Dead People FC','Galeano','Giselle',''),
 ('Juanas de Arco FC','Zanellato','Ivana','i_zanellato@hotmail.com'),
 ('Juanas de Arco FC','Parareda','María Agustina','magu.08@hotmail.com'),
 ('Juanas de Arco FC','Zanellato','Nadina','naddu.06@hotmail.com'),
 ('Juanas de Arco FC','Mazzocchi','María Laura','marialaura.88@hotmail.com'),
 ('Juanas de Arco FC','Adalian','Priscila','prichuu.13@hotmail.com'),
 ('Juanas de Arco FC','Lema','María Belen','be_lenn@live.com.ar'),
 ('Juanas de Arco FC','de la Vega','Florencia','flor.67@hotmail.com'),
 ('Juanas de Arco FC','Gonzalez Moss','Emilia','emigm_182@hotmail.com'),
 ('Juanas de Arco FC','Rinesi','Pamela','pame_more@hotmail.com'),
 ('Juanas de Arco FC','Zamparo','Daniela','danielazamparo@hotmail.com'),
 ('Juanas de Arco FC','Foglino','Macarena','macaa_55@hotmail.com'),
 ('Juanas de Arco FC','Merlos','Maria Sol','ssol._@hotmail.com'),
 ('La Liga-Moss FC','Borsato','Maria Agustina','magus_borsato@hotmail.com'),
 ('La Liga-Moss FC','Cosentino','Nerina','neru_cosentino@hotmail.com'),
 ('La Liga-Moss FC','Venditti','Carolina','caro.venditti@hotmail.com'),
 ('La Liga-Moss FC','Cuitiño','Nadia','ncuitinio@hotmail.com'),
 ('La Liga-Moss FC','Merlo','Aldana','aldy.89@hotmail.com'),
 ('La Liga-Moss FC','Borsato','Maria Mercedes','mechiborsato@hotmail.com'),
 ('La Liga-Moss FC','Lemme','Milagros','mililemme@hotmail.com'),
 ('La Liga-Moss FC','Della Villa','Constanza','coty_d7@hotmail.com'),
 ('Pesutis FC','Nieto','Tamara','atamarita@hotmail.com'),
 ('Pesutis FC','Aguirre','Estefania','estefiiaguirre@hotmail.com'),
 ('Pesutis FC','Nieto','Micaela','mica_nieto@hotmail.com'),
 ('Pesutis FC','Nieto','Yanina','yani_nieto@hotmail.com'),
 ('Pesutis FC','Pizzagalli','Johanna','johapizzagalli@hotmail.com'),
 ('Pesutis FC','Saccone','Ana Carolina','carosaccone@hotmail.com'),
 ('Pesutis FC','Lacolla','Carla','carlita_laco@hotmail.com'),
 ('Santa Terracita','Belardita','Paula','Paulabelardita@hotmail.com'),
 ('Santa Terracita','Claut','Natalí','natyclaut@hotmail.com'),
 ('Santa Terracita','Buontempo','Magali','magali_buontempo14@hotmail.com'),
 ('Santa Terracita','Albini','Belen','mbalbini@gmail.com'),
 ('Santa Terracita','Ramirez','Ivana','ivanaramirez@hotmail.com'),
 ('Santa Terracita','Alagastino','Gisele','gisele_alagastino@hotmail.com'),
 ('SITAS','Duarte','Estefania','estefy_65@hotmail.com'),
 ('SITAS','Roldan','Romina','romina._roldan@hotmail.com'),
 ('SITAS','Pepe','Juana','campanita.vip@hotmail.com'),
 ('SITAS','Palacio','Ivanna','yo_la_grosa_93@hotmail.com'),
 ('SITAS','Avendaño','Luz','lucecitta@hotmail.com'),
 ('SITAS','Azevedo','Loana','lolaa.azevedo@hotmail.com'),
 ('SITAS','Rodriguez','Cintia','cintia.1611@hotmail.com'),
 ('SITAS','Forti','Camila','camii.forti@hotmail.com'),
 ('SITAS','Massabie','Macarena','macarenamassabie@hotmail.com'),
 ('Suricatas','Lobo','Ximena','ximena.lobo@hotmail.com'),
 ('Suricatas','Corvera Pose','Victoria','corver.rr@hotmail.com'),
 ('Suricatas','Galasso','Alina','alina_glas@hotmail.com'),
 ('Suricatas','Medina','Maira','may77_mg@hotmail.com'),
 ('Suricatas','Liern','Micaela','mica.liern@hotmail.com '),
 ('Suricatas','Betti','Soledad','soolee._@hotmail.com'),
 ('Suricatas','Betti','Paula','ppauu_@hotmail.com'),
 ('Suricatas','Avila','Mariel','maruavila@live.com.ar'),
 ('Suricatas','Covatta','Valeria','alita.vcb@hotmail.com'),
 ('Suricatas','Andriani','Silvina','silviandriani@hotmaill.com'),
 ('Tirate un Caño','Frascarelli','Antonela','anto.frasca@hotmail.com'),
 ('Tirate un Caño','Imparato','Giuliana','giuli.imparato@hotmail.com'),
 ('Tirate un Caño','Imparato','Eugenia','euge_rizi@hotmail.com'),
 ('Tirate un Caño','Paolillo','Vanina','vanina.sp@hotmail.com'),
 ('Tirate un Caño','Indart','Paloma','palo.indart@hotmail.com'),
 ('Tirate un Caño','Rascon','Bárbara','barbie_rascon@hotmail.com'),
 ('Tirate un Caño','López Pumarega','Sofía','sofylp89@hotmail.com'),
 ('Tirate un Caño','Pros','Julieta','julieta.pr@hotmail.com'),
 ('Tirate un Caño','Rodríguez','Romina','romi_lahormiga05@hotmail.com'),
 ('Tirate un Caño','Balboa','Martina','marti_balboa@hotmail.com'),
 ('Tirate un Caño','Grau','Victoria','victoriaa.3@hotmail.com'),
 ('Tirate un Caño','Piazzi','Luciana','');
/*!40000 ALTER TABLE `jugador_temp` ENABLE KEYS */;


--
-- Definition of table `partido`
--

DROP TABLE IF EXISTS `partido`;
CREATE TABLE `partido` (
  `ID_PARTIDO` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `golesLocal` int(11) DEFAULT NULL,
  `golesVisitante` int(11) DEFAULT NULL,
  `jugado` bit(1) DEFAULT NULL,
  `EQUIPO_LOCAL_FK` bigint(20) DEFAULT NULL,
  `EQUIPO_VISITANTE_FK` bigint(20) DEFAULT NULL,
  `fecha_ID_FECHA` bigint(20) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `fechaPartido` datetime DEFAULT NULL,
  `ALINEACION_LOCAL` bigint(20) DEFAULT NULL,
  `ALINEACION_VISITANTE` bigint(20) DEFAULT NULL,
  `ID_FECHA` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_PARTIDO`),
  KEY `FKFB8855C1BC4EC0BF` (`fecha_ID_FECHA`),
  KEY `FKFB8855C1D1BD3B53` (`EQUIPO_VISITANTE_FK`),
  KEY `FKFB8855C191634FD1` (`EQUIPO_LOCAL_FK`),
  KEY `FKFB8855C1B2E6CD45` (`ALINEACION_VISITANTE`),
  KEY `FKFB8855C1EE33A287` (`ALINEACION_LOCAL`),
  KEY `FKFB8855C1986A3A3D` (`ID_FECHA`),
  KEY `FKD0BCC9E1B2E6CD45` (`ALINEACION_VISITANTE`),
  KEY `FKD0BCC9E1D1BD3B53` (`EQUIPO_VISITANTE_FK`),
  KEY `FKD0BCC9E1986A3A3D` (`ID_FECHA`),
  KEY `FKD0BCC9E191634FD1` (`EQUIPO_LOCAL_FK`),
  KEY `FKD0BCC9E1EE33A287` (`ALINEACION_LOCAL`),
  CONSTRAINT `FKD0BCC9E191634FD1` FOREIGN KEY (`EQUIPO_LOCAL_FK`) REFERENCES `equipo` (`ID_EQUIPO`),
  CONSTRAINT `FKD0BCC9E1986A3A3D` FOREIGN KEY (`ID_FECHA`) REFERENCES `fecha` (`ID_FECHA`),
  CONSTRAINT `FKD0BCC9E1B2E6CD45` FOREIGN KEY (`ALINEACION_VISITANTE`) REFERENCES `alineacion` (`ID_ALINEACION`),
  CONSTRAINT `FKD0BCC9E1D1BD3B53` FOREIGN KEY (`EQUIPO_VISITANTE_FK`) REFERENCES `equipo` (`ID_EQUIPO`),
  CONSTRAINT `FKD0BCC9E1EE33A287` FOREIGN KEY (`ALINEACION_LOCAL`) REFERENCES `alineacion` (`ID_ALINEACION`)
) ENGINE=InnoDB AUTO_INCREMENT=1519 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partido`
--

/*!40000 ALTER TABLE `partido` DISABLE KEYS */;
INSERT INTO `partido` (`ID_PARTIDO`,`activo`,`golesLocal`,`golesVisitante`,`jugado`,`EQUIPO_LOCAL_FK`,`EQUIPO_VISITANTE_FK`,`fecha_ID_FECHA`,`descripcion`,`fechaPartido`,`ALINEACION_LOCAL`,`ALINEACION_VISITANTE`,`ID_FECHA`) VALUES 
 (1379,0x01,5,0,0x01,46,45,NULL,'',NULL,1,2,322),
 (1380,0x01,4,8,0x01,39,41,NULL,'',NULL,3,4,322),
 (1381,0x01,3,4,0x01,38,44,NULL,'',NULL,5,6,322),
 (1382,0x01,3,3,0x01,51,40,NULL,'',NULL,7,8,322),
 (1383,0x01,4,1,0x01,54,39,NULL,'',NULL,59,60,323),
 (1384,0x01,10,2,0x01,46,38,NULL,'',NULL,19,20,323),
 (1385,0x01,10,2,0x01,41,51,NULL,'',NULL,21,22,323),
 (1386,0x01,5,5,0x01,44,40,NULL,'',NULL,17,18,323),
 (1387,0x01,0,3,0x01,38,54,NULL,'',NULL,61,62,324),
 (1388,0x01,3,4,0x01,51,39,NULL,'',NULL,37,38,324),
 (1389,0x01,3,4,0x01,40,46,NULL,'',NULL,39,40,324),
 (1390,0x01,0,18,0x01,44,41,NULL,'',NULL,41,42,324),
 (1391,0x01,7,6,0x01,54,51,NULL,'',NULL,43,44,325),
 (1392,0x01,0,11,0x01,38,40,NULL,'',NULL,45,46,325),
 (1393,0x01,9,1,0x01,39,44,NULL,'',NULL,47,48,325),
 (1394,0x01,2,7,0x01,46,41,NULL,'',NULL,49,50,325),
 (1395,0x01,1,2,0x01,40,54,NULL,'',NULL,67,68,326),
 (1396,0x01,3,5,0x01,44,51,NULL,'',NULL,69,70,326),
 (1397,0x01,5,0,0x01,41,38,NULL,NULL,NULL,NULL,NULL,326),
 (1398,0x01,5,0,0x01,46,39,NULL,'',NULL,75,76,326),
 (1399,0x01,5,0,0x01,54,44,NULL,'',NULL,81,82,327),
 (1400,0x01,1,2,0x01,40,41,NULL,'',NULL,77,78,327),
 (1401,0x01,4,4,0x01,51,46,NULL,'',NULL,79,80,327),
 (1402,0x01,1,5,0x01,38,39,NULL,'',NULL,83,84,327),
 (1403,0x01,0,0,0x00,41,54,NULL,NULL,NULL,NULL,NULL,328),
 (1404,0x01,0,0,0x00,46,44,NULL,NULL,NULL,NULL,NULL,328),
 (1405,0x01,0,0,0x00,39,40,NULL,NULL,NULL,NULL,NULL,328),
 (1406,0x01,0,0,0x00,38,51,NULL,NULL,NULL,NULL,NULL,328),
 (1407,0x01,12,0,0x01,42,52,NULL,'',NULL,9,10,329),
 (1408,0x01,5,4,0x01,48,49,NULL,'',NULL,11,12,329),
 (1409,0x01,8,1,0x01,53,43,NULL,'',NULL,13,14,329),
 (1410,0x01,6,2,0x01,47,50,NULL,'',NULL,15,16,329),
 (1411,0x01,2,5,0x01,52,48,NULL,'',NULL,29,30,330),
 (1412,0x01,4,8,0x01,42,53,NULL,'',NULL,27,28,330),
 (1413,0x01,2,3,0x01,49,47,NULL,'',NULL,23,24,330),
 (1414,0x01,2,5,0x01,43,50,NULL,'',NULL,25,26,330),
 (1415,0x01,9,2,0x01,53,52,NULL,'',NULL,35,36,331),
 (1416,0x01,0,4,0x01,47,48,NULL,'',NULL,31,32,331),
 (1417,0x01,4,2,0x01,50,42,NULL,'',NULL,33,34,331),
 (1418,0x01,5,0,0x01,43,49,NULL,NULL,NULL,NULL,NULL,331),
 (1419,0x01,0,6,0x01,52,47,NULL,'',NULL,51,52,332),
 (1420,0x01,6,2,0x01,53,50,NULL,'',NULL,53,54,332),
 (1421,0x01,6,0,0x01,48,43,NULL,'',NULL,55,56,332),
 (1422,0x01,4,0,0x01,42,49,NULL,'',NULL,57,58,332),
 (1423,0x01,2,2,0x01,50,52,NULL,'',NULL,73,74,333),
 (1424,0x01,2,2,0x01,43,47,NULL,'',NULL,65,66,333),
 (1425,0x01,1,2,0x01,49,53,NULL,'',NULL,71,72,333),
 (1426,0x01,3,4,0x01,42,48,NULL,'',NULL,63,64,333),
 (1427,0x01,0,1,0x01,52,43,NULL,'',NULL,87,88,334),
 (1428,0x01,3,3,0x01,50,49,NULL,'',NULL,89,90,334),
 (1429,0x01,2,3,0x01,47,42,NULL,'',NULL,91,92,334),
 (1430,0x01,5,3,0x01,53,48,NULL,'',NULL,85,86,334),
 (1431,0x01,0,0,0x00,49,52,NULL,NULL,NULL,NULL,NULL,335),
 (1432,0x01,0,0,0x00,42,43,NULL,NULL,NULL,NULL,NULL,335),
 (1433,0x01,0,0,0x00,48,50,NULL,NULL,NULL,NULL,NULL,335),
 (1434,0x01,0,0,0x00,53,47,NULL,NULL,NULL,NULL,NULL,335),
 (1435,0x01,4,3,0x01,41,44,NULL,'',NULL,95,96,336),
 (1436,0x01,0,0,0x00,39,43,NULL,NULL,NULL,NULL,NULL,336),
 (1437,0x01,0,0,0x00,38,42,NULL,NULL,NULL,NULL,NULL,336),
 (1438,0x01,0,0,0x00,45,40,NULL,NULL,NULL,NULL,NULL,336),
 (1439,0x01,0,0,0x00,44,39,NULL,NULL,NULL,NULL,NULL,337),
 (1440,0x01,0,0,0x00,41,38,NULL,NULL,NULL,NULL,NULL,337),
 (1441,0x01,0,0,0x00,43,45,NULL,NULL,NULL,NULL,NULL,337),
 (1442,0x01,0,0,0x00,42,40,NULL,NULL,NULL,NULL,NULL,337),
 (1443,0x01,0,0,0x00,38,44,NULL,NULL,NULL,NULL,NULL,338),
 (1444,0x01,0,0,0x00,45,39,NULL,NULL,NULL,NULL,NULL,338),
 (1445,0x01,0,0,0x00,40,41,NULL,NULL,NULL,NULL,NULL,338),
 (1446,0x01,0,0,0x00,42,43,NULL,NULL,NULL,NULL,NULL,338),
 (1447,0x01,0,0,0x00,44,45,NULL,NULL,NULL,NULL,NULL,339),
 (1448,0x01,0,0,0x00,38,40,NULL,NULL,NULL,NULL,NULL,339),
 (1449,0x01,0,0,0x00,39,42,NULL,NULL,NULL,NULL,NULL,339),
 (1450,0x01,0,0,0x00,41,43,NULL,NULL,NULL,NULL,NULL,339),
 (1451,0x01,0,0,0x00,40,44,NULL,NULL,NULL,NULL,NULL,340),
 (1452,0x01,0,0,0x00,42,45,NULL,NULL,NULL,NULL,NULL,340),
 (1453,0x01,0,0,0x00,43,38,NULL,NULL,NULL,NULL,NULL,340),
 (1454,0x01,0,0,0x00,41,39,NULL,NULL,NULL,NULL,NULL,340),
 (1455,0x01,0,0,0x00,44,42,NULL,NULL,NULL,NULL,NULL,341),
 (1456,0x01,0,0,0x00,40,43,NULL,NULL,NULL,NULL,NULL,341),
 (1457,0x01,0,0,0x00,45,41,NULL,NULL,NULL,NULL,NULL,341),
 (1458,0x01,0,0,0x00,38,39,NULL,NULL,NULL,NULL,NULL,341),
 (1459,0x01,0,0,0x00,43,44,NULL,NULL,NULL,NULL,NULL,342),
 (1460,0x01,0,0,0x00,41,42,NULL,NULL,NULL,NULL,NULL,342),
 (1461,0x01,0,0,0x00,39,40,NULL,NULL,NULL,NULL,NULL,342),
 (1462,0x01,0,0,0x00,38,45,NULL,NULL,NULL,NULL,NULL,342),
 (1463,0x01,0,0,0x00,47,54,NULL,NULL,NULL,NULL,NULL,343),
 (1464,0x01,0,0,0x00,50,48,NULL,NULL,NULL,NULL,NULL,343),
 (1465,0x01,2,0,0x01,53,52,NULL,'',NULL,93,94,343),
 (1466,0x01,0,0,0x00,49,51,NULL,NULL,NULL,NULL,NULL,343),
 (1467,0x01,0,0,0x00,54,50,NULL,NULL,NULL,NULL,NULL,344),
 (1468,0x01,0,0,0x00,47,53,NULL,NULL,NULL,NULL,NULL,344),
 (1469,0x01,0,0,0x00,48,49,NULL,NULL,NULL,NULL,NULL,344),
 (1470,0x01,0,0,0x00,52,51,NULL,NULL,NULL,NULL,NULL,344),
 (1471,0x01,0,0,0x00,53,54,NULL,NULL,NULL,NULL,NULL,345),
 (1472,0x01,0,0,0x00,49,50,NULL,NULL,NULL,NULL,NULL,345),
 (1473,0x01,0,0,0x00,51,47,NULL,NULL,NULL,NULL,NULL,345),
 (1474,0x01,0,0,0x00,52,48,NULL,NULL,NULL,NULL,NULL,345),
 (1475,0x01,0,0,0x00,54,49,NULL,NULL,NULL,NULL,NULL,346),
 (1476,0x01,0,0,0x00,53,51,NULL,NULL,NULL,NULL,NULL,346),
 (1477,0x01,0,0,0x00,50,52,NULL,NULL,NULL,NULL,NULL,346),
 (1478,0x01,0,0,0x00,47,48,NULL,NULL,NULL,NULL,NULL,346),
 (1479,0x01,0,0,0x00,51,54,NULL,NULL,NULL,NULL,NULL,347),
 (1480,0x01,0,0,0x00,52,49,NULL,NULL,NULL,NULL,NULL,347),
 (1481,0x01,0,0,0x00,48,53,NULL,NULL,NULL,NULL,NULL,347),
 (1482,0x01,0,0,0x00,47,50,NULL,NULL,NULL,NULL,NULL,347),
 (1483,0x01,0,0,0x00,54,52,NULL,NULL,NULL,NULL,NULL,348),
 (1484,0x01,0,0,0x00,51,48,NULL,NULL,NULL,NULL,NULL,348),
 (1485,0x01,0,0,0x00,49,47,NULL,NULL,NULL,NULL,NULL,348),
 (1486,0x01,0,0,0x00,53,50,NULL,NULL,NULL,NULL,NULL,348),
 (1487,0x01,0,0,0x00,48,54,NULL,NULL,NULL,NULL,NULL,349),
 (1488,0x01,0,0,0x00,47,52,NULL,NULL,NULL,NULL,NULL,349),
 (1489,0x01,0,0,0x00,50,51,NULL,NULL,NULL,NULL,NULL,349),
 (1490,0x01,0,0,0x00,53,49,NULL,NULL,NULL,NULL,NULL,349),
 (1491,0x01,0,0,0x00,54,47,NULL,NULL,NULL,NULL,NULL,350),
 (1492,0x01,0,0,0x00,48,50,NULL,NULL,NULL,NULL,NULL,350),
 (1493,0x01,0,0,0x00,52,53,NULL,NULL,NULL,NULL,NULL,350),
 (1494,0x01,0,0,0x00,51,49,NULL,NULL,NULL,NULL,NULL,350),
 (1495,0x01,0,0,0x00,50,54,NULL,NULL,NULL,NULL,NULL,351),
 (1496,0x01,0,0,0x00,53,47,NULL,NULL,NULL,NULL,NULL,351),
 (1497,0x01,0,0,0x00,49,48,NULL,NULL,NULL,NULL,NULL,351),
 (1498,0x01,0,0,0x00,51,52,NULL,NULL,NULL,NULL,NULL,351),
 (1499,0x01,0,0,0x00,54,53,NULL,NULL,NULL,NULL,NULL,352),
 (1500,0x01,0,0,0x00,50,49,NULL,NULL,NULL,NULL,NULL,352),
 (1501,0x01,0,0,0x00,47,51,NULL,NULL,NULL,NULL,NULL,352),
 (1502,0x01,0,0,0x00,48,52,NULL,NULL,NULL,NULL,NULL,352),
 (1503,0x01,0,0,0x00,49,54,NULL,NULL,NULL,NULL,NULL,353),
 (1504,0x01,0,0,0x00,51,53,NULL,NULL,NULL,NULL,NULL,353),
 (1505,0x01,0,0,0x00,52,50,NULL,NULL,NULL,NULL,NULL,353),
 (1506,0x01,0,0,0x00,48,47,NULL,NULL,NULL,NULL,NULL,353),
 (1507,0x01,0,0,0x00,54,51,NULL,NULL,NULL,NULL,NULL,354),
 (1508,0x01,0,0,0x00,49,52,NULL,NULL,NULL,NULL,NULL,354),
 (1509,0x01,0,0,0x00,53,48,NULL,NULL,NULL,NULL,NULL,354),
 (1510,0x01,0,0,0x00,50,47,NULL,NULL,NULL,NULL,NULL,354),
 (1511,0x01,0,0,0x00,52,54,NULL,NULL,NULL,NULL,NULL,355),
 (1512,0x01,0,0,0x00,48,51,NULL,NULL,NULL,NULL,NULL,355),
 (1513,0x01,0,0,0x00,47,49,NULL,NULL,NULL,NULL,NULL,355),
 (1514,0x01,0,0,0x00,50,53,NULL,NULL,NULL,NULL,NULL,355),
 (1515,0x01,0,0,0x00,54,48,NULL,NULL,NULL,NULL,NULL,356),
 (1516,0x01,0,0,0x00,52,47,NULL,NULL,NULL,NULL,NULL,356),
 (1517,0x01,0,0,0x00,51,50,NULL,NULL,NULL,NULL,NULL,356),
 (1518,0x01,0,0,0x00,49,53,NULL,NULL,NULL,NULL,NULL,356);
/*!40000 ALTER TABLE `partido` ENABLE KEYS */;


--
-- Definition of table `partido_amistoso`
--

DROP TABLE IF EXISTS `partido_amistoso`;
CREATE TABLE `partido_amistoso` (
  `ID_PARTIDO_AMISTOSO` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantParticipantes` int(11) DEFAULT NULL,
  `lugar` varchar(255) DEFAULT NULL,
  `mensaje` varchar(255) DEFAULT NULL,
  `modo` varchar(255) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `ID_GRUPO` bigint(20) DEFAULT NULL,
  `USUARIO_CREADOR_FK` bigint(20) DEFAULT NULL,
  `ALINEACION_EQUIPOA` bigint(20) DEFAULT NULL,
  `ALINEACION_EQUIPOB` bigint(20) DEFAULT NULL,
  `GOLES_EQUIPO_A` bigint(20) DEFAULT NULL,
  `GOLES_EQUIPO_B` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_PARTIDO_AMISTOSO`),
  KEY `FKDB81C22B9892C415` (`ID_GRUPO`),
  KEY `FKDB81C22BA313C4D0` (`USUARIO_CREADOR_FK`),
  KEY `FKDB81C22B7C64902B` (`ALINEACION_EQUIPOA`),
  KEY `FKDB81C22B7C64902C` (`ALINEACION_EQUIPOB`),
  CONSTRAINT `FKDB81C22B7C64902B` FOREIGN KEY (`ALINEACION_EQUIPOA`) REFERENCES `alineacion_amistoso` (`ID_ALINEACION_AMISTOSO`),
  CONSTRAINT `FKDB81C22B7C64902C` FOREIGN KEY (`ALINEACION_EQUIPOB`) REFERENCES `alineacion_amistoso` (`ID_ALINEACION_AMISTOSO`),
  CONSTRAINT `FKDB81C22B9892C415` FOREIGN KEY (`ID_GRUPO`) REFERENCES `grupo` (`ID_GRUPO`),
  CONSTRAINT `FKDB81C22BA313C4D0` FOREIGN KEY (`USUARIO_CREADOR_FK`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partido_amistoso`
--

/*!40000 ALTER TABLE `partido_amistoso` DISABLE KEYS */;
/*!40000 ALTER TABLE `partido_amistoso` ENABLE KEYS */;


--
-- Definition of table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
CREATE TABLE `perfil` (
  `ID_PERFIL` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVO` bit(1) DEFAULT NULL,
  `FECHA_CREACION` datetime DEFAULT NULL,
  `FECHA_ULTIMA_MODIFICACION` datetime DEFAULT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `USUARIO_CREACION` bigint(20) DEFAULT NULL,
  `USUARIO_ULTIMA_MODIFICACION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_PERFIL`),
  KEY `FK8C765DCC21D8C9D8` (`USUARIO_CREACION`),
  KEY `FK8C765DCCF5191012` (`USUARIO_ULTIMA_MODIFICACION`),
  CONSTRAINT `FK8C765DCC21D8C9D8` FOREIGN KEY (`USUARIO_CREACION`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FK8C765DCCF5191012` FOREIGN KEY (`USUARIO_ULTIMA_MODIFICACION`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perfil`
--

/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` (`ID_PERFIL`,`ACTIVO`,`FECHA_CREACION`,`FECHA_ULTIMA_MODIFICACION`,`DESCRIPCION`,`NOMBRE`,`USUARIO_CREACION`,`USUARIO_ULTIMA_MODIFICACION`) VALUES 
 (41,0x01,NULL,NULL,NULL,'PADMIN',NULL,NULL),
 (42,0x01,NULL,NULL,NULL,'PUSER',NULL,NULL);
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;


--
-- Definition of table `perfil_accion`
--

DROP TABLE IF EXISTS `perfil_accion`;
CREATE TABLE `perfil_accion` (
  `ID_PERFIL` bigint(20) NOT NULL,
  `ID_ACCION` bigint(20) NOT NULL,
  KEY `FK4BB179BA1BB53A29` (`ID_ACCION`),
  KEY `FK4BB179BA4F2C85F3` (`ID_PERFIL`),
  CONSTRAINT `FK4BB179BA1BB53A29` FOREIGN KEY (`ID_ACCION`) REFERENCES `accion` (`ID_ACCION`),
  CONSTRAINT `FK4BB179BA4F2C85F3` FOREIGN KEY (`ID_PERFIL`) REFERENCES `perfil` (`ID_PERFIL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perfil_accion`
--

/*!40000 ALTER TABLE `perfil_accion` DISABLE KEYS */;
INSERT INTO `perfil_accion` (`ID_PERFIL`,`ID_ACCION`) VALUES 
 (41,171),
 (41,172),
 (41,173),
 (41,174),
 (41,175),
 (41,176),
 (41,177),
 (42,178),
 (42,179),
 (42,180),
 (42,181),
 (42,182),
 (42,183),
 (42,184);
/*!40000 ALTER TABLE `perfil_accion` ENABLE KEYS */;


--
-- Definition of table `perfil_usuario`
--

DROP TABLE IF EXISTS `perfil_usuario`;
CREATE TABLE `perfil_usuario` (
  `ID_PERFIL` bigint(20) NOT NULL,
  `ID_USUARIO` bigint(20) NOT NULL,
  KEY `FK68C1F79B4F2C85F3` (`ID_PERFIL`),
  KEY `FK68C1F79BD77A7F61` (`ID_USUARIO`),
  CONSTRAINT `FK68C1F79B4F2C85F3` FOREIGN KEY (`ID_PERFIL`) REFERENCES `perfil` (`ID_PERFIL`),
  CONSTRAINT `FK68C1F79BD77A7F61` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perfil_usuario`
--

/*!40000 ALTER TABLE `perfil_usuario` DISABLE KEYS */;
INSERT INTO `perfil_usuario` (`ID_PERFIL`,`ID_USUARIO`) VALUES 
 (42,2),
 (42,3),
 (41,1),
 (42,4);
/*!40000 ALTER TABLE `perfil_usuario` ENABLE KEYS */;


--
-- Definition of table `posicion_goleador`
--

DROP TABLE IF EXISTS `posicion_goleador`;
CREATE TABLE `posicion_goleador` (
  `ID_POSICIONGOLEADOR` bigint(20) NOT NULL AUTO_INCREMENT,
  `GOLES` int(11) DEFAULT NULL,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `TORNEO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_POSICIONGOLEADOR`),
  UNIQUE KEY `JUGADOR_FK` (`JUGADOR_FK`,`TORNEO_FK`),
  KEY `FK84336CEC18383FCB` (`JUGADOR_FK`),
  KEY `FK84336CEC5DB4EB3D` (`TORNEO_FK`),
  CONSTRAINT `FK84336CEC18383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK84336CEC5DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`)
) ENGINE=InnoDB AUTO_INCREMENT=1625 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posicion_goleador`
--

/*!40000 ALTER TABLE `posicion_goleador` DISABLE KEYS */;
INSERT INTO `posicion_goleador` (`ID_POSICIONGOLEADOR`,`GOLES`,`JUGADOR_FK`,`TORNEO_FK`) VALUES 
 (1014,19,533,43),
 (1015,16,554,43),
 (1016,15,535,43),
 (1017,13,569,43),
 (1018,13,408,43),
 (1019,10,552,43),
 (1020,10,555,43),
 (1021,8,537,43),
 (1022,7,334,43),
 (1023,6,304,43),
 (1024,5,295,43),
 (1025,5,353,43),
 (1026,4,357,43),
 (1027,4,405,43),
 (1028,4,300,43),
 (1029,3,358,43),
 (1030,3,571,43),
 (1031,3,335,43),
 (1032,3,337,43),
 (1033,3,402,43),
 (1034,3,297,43),
 (1035,2,538,43),
 (1036,2,568,43),
 (1037,2,282,43),
 (1038,2,406,43),
 (1039,2,575,43),
 (1040,1,302,43),
 (1041,1,539,43),
 (1042,1,363,43),
 (1043,1,584,43),
 (1044,1,284,43),
 (1045,1,570,43),
 (1046,1,285,43),
 (1047,1,278,43),
 (1048,1,287,43),
 (1049,1,534,43),
 (1050,1,305,43),
 (1051,1,401,43),
 (1052,1,296,43),
 (1267,13,424,44),
 (1268,10,542,44),
 (1269,10,397,44),
 (1270,9,379,44),
 (1271,8,385,44),
 (1272,7,319,44),
 (1273,7,422,44),
 (1274,7,386,44),
 (1275,7,387,44),
 (1276,6,430,44),
 (1277,6,371,44),
 (1278,5,332,44),
 (1279,5,400,44),
 (1280,4,368,44),
 (1281,3,313,44),
 (1282,3,389,44),
 (1283,3,595,44),
 (1284,3,414,44),
 (1285,3,531,44),
 (1286,3,388,44),
 (1287,2,315,44),
 (1288,2,394,44),
 (1289,2,421,44),
 (1290,2,382,44),
 (1291,2,318,44),
 (1292,2,559,44),
 (1293,1,391,44),
 (1294,1,588,44),
 (1295,1,398,44),
 (1296,1,547,44),
 (1297,1,591,44),
 (1298,1,543,44),
 (1299,1,550,44),
 (1300,1,396,44),
 (1301,1,545,44),
 (1302,1,429,44),
 (1303,1,412,44),
 (1304,1,380,44),
 (1520,15,424,46),
 (1521,4,368,46),
 (1522,6,371,46),
 (1523,9,379,46),
 (1524,1,380,46),
 (1525,2,382,46),
 (1526,8,385,46),
 (1527,7,386,46),
 (1528,7,387,46),
 (1529,3,388,46),
 (1530,3,389,46),
 (1531,1,391,46),
 (1532,2,394,46),
 (1533,1,396,46),
 (1534,10,397,46),
 (1535,1,398,46),
 (1536,5,400,46),
 (1537,1,401,46),
 (1538,3,402,46),
 (1539,4,405,46),
 (1540,2,406,46),
 (1541,13,408,46),
 (1542,1,412,46),
 (1543,3,414,46),
 (1544,2,421,46),
 (1545,7,422,46),
 (1546,1,429,46),
 (1547,6,430,46),
 (1548,1,543,46),
 (1549,1,547,46),
 (1550,2,559,46),
 (1551,2,568,46),
 (1552,13,569,46),
 (1553,1,570,46),
 (1554,3,571,46),
 (1555,2,575,46),
 (1556,1,588,46),
 (1557,1,591,46),
 (1558,3,595,46),
 (1559,20,554,45),
 (1560,10,334,45),
 (1562,1,278,45),
 (1563,2,282,45),
 (1564,1,284,45),
 (1565,1,285,45),
 (1566,1,287,45),
 (1567,5,295,45),
 (1568,1,296,45),
 (1569,3,297,45),
 (1570,4,300,45),
 (1571,1,302,45),
 (1572,6,304,45),
 (1573,1,305,45),
 (1574,3,313,45),
 (1575,2,315,45),
 (1576,2,318,45),
 (1577,7,319,45),
 (1578,5,332,45),
 (1579,3,335,45),
 (1580,3,337,45),
 (1581,5,353,45),
 (1582,3,531,45),
 (1583,1,534,45),
 (1584,15,535,45),
 (1585,8,537,45),
 (1586,2,538,45),
 (1587,1,539,45),
 (1588,10,542,45),
 (1589,1,545,45),
 (1590,1,550,45),
 (1591,10,552,45),
 (1592,10,555,45),
 (1593,1,584,45);
/*!40000 ALTER TABLE `posicion_goleador` ENABLE KEYS */;


--
-- Definition of table `posicion_torneo`
--

DROP TABLE IF EXISTS `posicion_torneo`;
CREATE TABLE `posicion_torneo` (
  `ID_POSICION` bigint(20) NOT NULL AUTO_INCREMENT,
  `amarillas` int(11) DEFAULT NULL,
  `golesAFavor` int(11) DEFAULT NULL,
  `golesEnContra` int(11) DEFAULT NULL,
  `partidosEmpatados` int(11) DEFAULT NULL,
  `partidosGanados` int(11) DEFAULT NULL,
  `partidosJugados` int(11) DEFAULT NULL,
  `partidosPerdidos` int(11) DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `rojas` int(11) DEFAULT NULL,
  `EQUIPO_FK` bigint(20) DEFAULT NULL,
  `TORNEO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_POSICION`),
  KEY `FK8BF26DE6FABF067D` (`EQUIPO_FK`),
  KEY `FK8BF26DE65DB4EB3D` (`TORNEO_FK`),
  CONSTRAINT `FK8BF26DE65DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`),
  CONSTRAINT `FK8BF26DE6FABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posicion_torneo`
--

/*!40000 ALTER TABLE `posicion_torneo` DISABLE KEYS */;
INSERT INTO `posicion_torneo` (`ID_POSICION`,`amarillas`,`golesAFavor`,`golesEnContra`,`partidosEmpatados`,`partidosGanados`,`partidosJugados`,`partidosPerdidos`,`puntos`,`rojas`,`EQUIPO_FK`,`TORNEO_FK`) VALUES 
 (146,0,0,5,0,0,1,1,0,0,45,43),
 (147,0,50,9,0,6,6,0,18,0,41,43),
 (148,0,13,45,1,1,6,4,4,0,44,43),
 (149,0,24,16,2,1,6,3,5,0,40,43),
 (150,0,23,31,2,1,6,3,5,0,51,43),
 (151,0,6,38,0,0,6,6,0,0,38,43),
 (152,4,23,22,0,3,6,3,9,0,39,43),
 (153,0,30,16,1,4,6,1,13,0,46,43),
 (154,0,6,35,1,0,6,5,1,0,52,44),
 (155,0,10,22,1,0,6,5,1,0,49,44),
 (156,0,11,21,1,2,6,3,7,0,43,44),
 (157,0,18,21,2,2,6,2,8,0,50,44),
 (158,1,19,13,1,3,6,2,10,0,47,44),
 (159,0,38,13,0,6,6,0,18,0,53,44),
 (160,0,27,14,0,5,6,1,15,0,48,44),
 (161,1,28,18,0,3,6,3,9,0,42,44),
 (162,NULL,21,8,0,5,5,0,15,0,54,43),
 (163,0,3,4,0,0,1,1,0,0,44,45),
 (164,0,0,0,0,0,0,0,0,0,43,45),
 (165,0,0,0,0,0,0,0,0,0,42,45),
 (166,0,0,0,0,0,0,0,0,0,40,45),
 (167,0,0,0,0,0,0,0,0,0,45,45),
 (168,0,0,0,0,0,0,0,0,0,38,45),
 (169,0,0,0,0,0,0,0,0,0,39,45),
 (170,0,4,3,0,1,1,0,3,0,41,45),
 (171,0,0,0,0,0,0,0,0,0,54,46),
 (172,0,0,0,0,0,0,0,0,0,48,46),
 (173,0,0,0,0,0,0,0,0,0,52,46),
 (174,0,0,0,0,0,0,0,0,0,51,46),
 (175,0,0,0,0,0,0,0,0,0,49,46),
 (176,0,0,0,0,0,0,0,0,0,53,46),
 (177,0,0,0,0,0,0,0,0,0,50,46),
 (178,0,0,0,0,0,0,0,0,0,47,46);
/*!40000 ALTER TABLE `posicion_torneo` ENABLE KEYS */;


--
-- Definition of table `puntaje_partido`
--

DROP TABLE IF EXISTS `puntaje_partido`;
CREATE TABLE `puntaje_partido` (
  `ID_PUNTAJEPARTIDO` bigint(20) NOT NULL AUTO_INCREMENT,
  `puntaje` int(11) DEFAULT NULL,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_FK` bigint(20) DEFAULT NULL,
  `figura` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ID_PUNTAJEPARTIDO`),
  KEY `FK2F416253C42F976B` (`PARTIDO_FK`),
  KEY `FK2F41625318383FCB` (`JUGADOR_FK`),
  KEY `FKF0842A93C42F976B` (`PARTIDO_FK`),
  CONSTRAINT `FK2F41625318383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FKF0842A93C42F976B` FOREIGN KEY (`PARTIDO_FK`) REFERENCES `partido` (`ID_PARTIDO`)
) ENGINE=InnoDB AUTO_INCREMENT=668 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `puntaje_partido`
--

/*!40000 ALTER TABLE `puntaje_partido` DISABLE KEYS */;
INSERT INTO `puntaje_partido` (`ID_PUNTAJEPARTIDO`,`puntaje`,`JUGADOR_FK`,`PARTIDO_FK`,`figura`) VALUES 
 (1,0,357,1379,0x00),
 (2,0,358,1379,0x00),
 (3,0,359,1379,0x00),
 (4,0,360,1379,0x00),
 (5,0,361,1379,0x00),
 (6,0,362,1379,0x00),
 (7,0,367,1379,0x00),
 (8,0,532,1379,0x00),
 (9,0,533,1379,0x00),
 (10,0,343,1379,0x00),
 (11,0,344,1379,0x00),
 (12,0,346,1379,0x00),
 (13,0,347,1379,0x00),
 (14,0,348,1379,0x01),
 (15,0,350,1379,0x00),
 (16,0,353,1379,0x00),
 (17,0,286,1380,0x00),
 (18,0,288,1380,0x00),
 (19,0,289,1380,0x00),
 (20,0,294,1380,0x00),
 (21,0,534,1380,0x00),
 (22,0,535,1380,0x00),
 (23,0,536,1380,0x00),
 (24,0,302,1380,0x00),
 (25,0,304,1380,0x00),
 (26,0,305,1380,0x00),
 (27,0,306,1380,0x00),
 (28,0,307,1380,0x00),
 (29,0,308,1380,0x00),
 (30,0,309,1380,0x00),
 (31,0,537,1380,0x01),
 (32,0,538,1380,0x00),
 (33,0,276,1381,0x00),
 (34,0,278,1381,0x00),
 (35,0,279,1381,0x00),
 (36,0,280,1381,0x00),
 (37,0,282,1381,0x00),
 (38,0,284,1381,0x00),
 (39,0,285,1381,0x00),
 (40,0,334,1381,0x01),
 (41,0,335,1381,0x00),
 (42,0,336,1381,0x00),
 (43,0,337,1381,0x00),
 (44,0,339,1381,0x00),
 (45,0,402,1382,0x00),
 (46,0,403,1382,0x00),
 (47,0,405,1382,0x00),
 (48,0,406,1382,0x00),
 (49,0,408,1382,0x01),
 (50,0,409,1382,0x00),
 (51,0,540,1382,0x00),
 (52,0,541,1382,0x00),
 (53,0,296,1382,0x00),
 (54,0,297,1382,0x00),
 (55,0,298,1382,0x00),
 (56,0,299,1382,0x00),
 (57,0,300,1382,0x00),
 (58,0,301,1382,0x00),
 (59,0,539,1382,0x00),
 (60,0,313,1407,0x00),
 (61,0,315,1407,0x00),
 (62,0,316,1407,0x00),
 (63,0,317,1407,0x00),
 (64,0,318,1407,0x00),
 (65,0,319,1407,0x00),
 (66,0,531,1407,0x01),
 (67,0,542,1407,0x00),
 (68,0,410,1407,0x00),
 (69,0,411,1407,0x00),
 (70,0,412,1407,0x00),
 (71,0,413,1407,0x00),
 (72,0,415,1407,0x00),
 (73,0,416,1407,0x00),
 (74,0,419,1407,0x00),
 (75,0,543,1407,0x00),
 (76,0,380,1408,0x00),
 (77,0,381,1408,0x00),
 (78,0,382,1408,0x00),
 (79,0,385,1408,0x00),
 (80,0,386,1408,0x00),
 (81,0,387,1408,0x01),
 (82,0,544,1408,0x00),
 (83,0,388,1408,0x00),
 (84,0,389,1408,0x00),
 (85,0,390,1408,0x00),
 (86,0,391,1408,0x00),
 (87,0,392,1408,0x00),
 (88,0,393,1408,0x00),
 (89,0,394,1408,0x00),
 (90,0,421,1409,0x00),
 (91,0,422,1409,0x00),
 (92,0,423,1409,0x00),
 (93,0,424,1409,0x01),
 (94,0,425,1409,0x00),
 (95,0,427,1409,0x00),
 (96,0,428,1409,0x00),
 (97,0,429,1409,0x00),
 (98,0,430,1409,0x00),
 (99,0,325,1409,0x00),
 (100,0,326,1409,0x00),
 (101,0,327,1409,0x00),
 (102,0,330,1409,0x00),
 (103,0,331,1409,0x00),
 (104,0,332,1409,0x00),
 (105,0,545,1409,0x00),
 (106,0,546,1409,0x00),
 (107,0,368,1410,0x00),
 (108,0,369,1410,0x00),
 (109,0,370,1410,0x00),
 (110,0,371,1410,0x00),
 (111,0,372,1410,0x00),
 (112,0,377,1410,0x00),
 (113,0,378,1410,0x00),
 (114,0,379,1410,0x01),
 (115,0,395,1410,0x00),
 (116,0,396,1410,0x00),
 (117,0,397,1410,0x00),
 (118,0,398,1410,0x00),
 (119,0,399,1410,0x00),
 (120,0,400,1410,0x00),
 (121,0,547,1410,0x00),
 (122,0,375,1410,0x00),
 (123,0,334,1386,0x00),
 (124,0,335,1386,0x01),
 (125,0,336,1386,0x00),
 (126,0,337,1386,0x00),
 (127,0,338,1386,0x00),
 (128,0,339,1386,0x00),
 (129,0,296,1386,0x00),
 (130,0,297,1386,0x00),
 (131,0,298,1386,0x00),
 (132,0,300,1386,0x00),
 (133,0,301,1386,0x00),
 (134,0,353,1386,0x00),
 (135,0,539,1386,0x00),
 (136,0,552,1386,0x00),
 (137,0,553,1386,0x00),
 (138,0,357,1384,0x00),
 (139,0,358,1384,0x00),
 (140,0,359,1384,0x00),
 (141,0,360,1384,0x00),
 (142,0,367,1384,0x00),
 (143,0,533,1384,0x01),
 (144,0,556,1384,0x00),
 (145,0,276,1384,0x00),
 (146,0,277,1384,0x00),
 (147,0,278,1384,0x00),
 (148,0,284,1384,0x00),
 (149,0,285,1384,0x00),
 (150,0,302,1385,0x00),
 (151,0,304,1385,0x00),
 (152,0,305,1385,0x00),
 (153,0,306,1385,0x00),
 (154,0,307,1385,0x00),
 (155,0,537,1385,0x00),
 (156,0,554,1385,0x01),
 (157,0,555,1385,0x00),
 (158,0,401,1385,0x00),
 (159,0,403,1385,0x00),
 (160,0,405,1385,0x00),
 (161,0,407,1385,0x00),
 (162,0,408,1385,0x00),
 (163,0,409,1385,0x00),
 (164,0,540,1385,0x00),
 (165,0,558,1385,0x00),
 (166,0,388,1413,0x00),
 (167,0,389,1413,0x00),
 (168,0,390,1413,0x00),
 (169,0,391,1413,0x00),
 (170,0,392,1413,0x00),
 (171,0,393,1413,0x00),
 (172,0,394,1413,0x00),
 (173,0,368,1413,0x00),
 (174,0,369,1413,0x00),
 (175,0,370,1413,0x00),
 (176,0,371,1413,0x00),
 (177,0,372,1413,0x00),
 (178,0,375,1413,0x00),
 (179,0,377,1413,0x00),
 (180,0,378,1413,0x00),
 (181,0,379,1413,0x01),
 (182,0,325,1414,0x00),
 (183,0,326,1414,0x00),
 (184,0,327,1414,0x00),
 (185,0,331,1414,0x00),
 (186,0,332,1414,0x00),
 (187,0,546,1414,0x00),
 (188,0,395,1414,0x00),
 (189,0,396,1414,0x00),
 (190,0,397,1414,0x01),
 (191,0,398,1414,0x00),
 (192,0,399,1414,0x00),
 (193,0,400,1414,0x00),
 (194,0,547,1414,0x00),
 (195,0,313,1412,0x00),
 (196,0,315,1412,0x00),
 (197,0,316,1412,0x00),
 (198,0,317,1412,0x00),
 (199,0,318,1412,0x00),
 (200,0,319,1412,0x00),
 (201,0,542,1412,0x00),
 (202,0,550,1412,0x00),
 (203,0,421,1412,0x00),
 (204,0,424,1412,0x00),
 (205,0,425,1412,0x00),
 (206,0,428,1412,0x00),
 (207,0,429,1412,0x00),
 (208,0,559,1412,0x00),
 (209,0,560,1412,0x01),
 (210,0,410,1411,0x00),
 (211,0,411,1411,0x00),
 (212,0,413,1411,0x00),
 (213,0,414,1411,0x00),
 (214,0,415,1411,0x00),
 (215,0,416,1411,0x00),
 (216,0,419,1411,0x00),
 (217,0,543,1411,0x00),
 (218,0,380,1411,0x00),
 (219,0,381,1411,0x00),
 (220,0,382,1411,0x00),
 (221,0,385,1411,0x00),
 (222,0,386,1411,0x00),
 (223,0,387,1411,0x01),
 (224,0,544,1411,0x00),
 (225,0,561,1411,0x00),
 (226,0,368,1416,0x00),
 (227,0,369,1416,0x00),
 (228,0,370,1416,0x00),
 (229,0,371,1416,0x00),
 (230,0,372,1416,0x00),
 (231,0,375,1416,0x00),
 (232,0,377,1416,0x00),
 (233,0,378,1416,0x00),
 (234,0,379,1416,0x00),
 (235,0,380,1416,0x00),
 (236,0,381,1416,0x00),
 (237,0,382,1416,0x00),
 (238,0,385,1416,0x00),
 (239,0,386,1416,0x01),
 (240,0,387,1416,0x00),
 (241,0,544,1416,0x00),
 (242,0,563,1416,0x00),
 (243,0,395,1417,0x00),
 (244,0,396,1417,0x00),
 (245,0,397,1417,0x01),
 (246,0,398,1417,0x00),
 (247,0,400,1417,0x00),
 (248,0,315,1417,0x00),
 (249,0,316,1417,0x00),
 (250,0,317,1417,0x00),
 (251,0,318,1417,0x00),
 (252,0,319,1417,0x00),
 (253,0,542,1417,0x00),
 (254,0,421,1415,0x00),
 (255,0,422,1415,0x01),
 (256,0,424,1415,0x00),
 (257,0,428,1415,0x00),
 (258,0,429,1415,0x00),
 (259,0,430,1415,0x00),
 (260,0,411,1415,0x00),
 (261,0,412,1415,0x00),
 (262,0,413,1415,0x00),
 (263,0,414,1415,0x00),
 (264,0,416,1415,0x00),
 (265,0,419,1415,0x00),
 (266,0,543,1415,0x00),
 (267,0,403,1388,0x00),
 (268,0,405,1388,0x00),
 (269,0,406,1388,0x00),
 (270,0,408,1388,0x00),
 (271,0,540,1388,0x00),
 (272,0,541,1388,0x00),
 (273,0,558,1388,0x00),
 (274,0,286,1388,0x00),
 (275,0,287,1388,0x00),
 (276,0,288,1388,0x00),
 (277,0,289,1388,0x00),
 (278,0,294,1388,0x00),
 (279,0,295,1388,0x00),
 (280,0,534,1388,0x00),
 (281,0,535,1388,0x01),
 (282,0,536,1388,0x00),
 (283,0,297,1389,0x00),
 (284,0,300,1389,0x00),
 (285,0,353,1389,0x00),
 (286,0,539,1389,0x00),
 (287,0,552,1389,0x00),
 (288,0,553,1389,0x00),
 (289,0,357,1389,0x00),
 (290,0,358,1389,0x00),
 (291,0,359,1389,0x00),
 (292,0,360,1389,0x00),
 (293,0,362,1389,0x00),
 (294,0,533,1389,0x01),
 (295,0,334,1390,0x00),
 (296,0,336,1390,0x00),
 (297,0,337,1390,0x00),
 (298,0,338,1390,0x00),
 (299,0,339,1390,0x00),
 (300,0,565,1390,0x00),
 (301,0,302,1390,0x00),
 (302,0,304,1390,0x00),
 (303,0,306,1390,0x00),
 (304,0,307,1390,0x00),
 (305,0,308,1390,0x00),
 (306,0,537,1390,0x00),
 (307,0,554,1390,0x00),
 (308,0,555,1390,0x01),
 (309,0,568,1391,0x00),
 (310,0,569,1391,0x00),
 (311,0,570,1391,0x00),
 (312,0,571,1391,0x00),
 (313,0,572,1391,0x00),
 (314,0,573,1391,0x00),
 (315,0,574,1391,0x00),
 (316,0,575,1391,0x00),
 (317,0,576,1391,0x00),
 (318,0,577,1391,0x00),
 (319,0,401,1391,0x01),
 (320,0,405,1391,0x00),
 (321,0,408,1391,0x00),
 (322,0,409,1391,0x00),
 (323,0,540,1391,0x00),
 (324,0,580,1391,0x00),
 (325,0,276,1392,0x00),
 (326,0,278,1392,0x00),
 (327,0,279,1392,0x00),
 (328,0,281,1392,0x00),
 (329,0,284,1392,0x00),
 (330,0,296,1392,0x00),
 (331,0,297,1392,0x00),
 (332,0,300,1392,0x00),
 (333,0,301,1392,0x00),
 (334,0,353,1392,0x01),
 (335,0,552,1392,0x00),
 (336,0,553,1392,0x00),
 (337,0,286,1393,0x00),
 (338,0,287,1393,0x00),
 (339,0,288,1393,0x00),
 (340,0,289,1393,0x00),
 (341,0,290,1393,0x00),
 (342,0,295,1393,0x00),
 (343,0,535,1393,0x01),
 (344,0,334,1393,0x00),
 (345,0,336,1393,0x00),
 (346,0,337,1393,0x00),
 (347,0,338,1393,0x00),
 (348,0,339,1393,0x00),
 (349,0,341,1393,0x00),
 (350,0,357,1394,0x00),
 (351,0,358,1394,0x00),
 (352,0,360,1394,0x00),
 (353,0,362,1394,0x00),
 (354,0,532,1394,0x00),
 (355,0,533,1394,0x00),
 (356,0,564,1394,0x00),
 (357,0,302,1394,0x00),
 (358,0,304,1394,0x00),
 (359,0,305,1394,0x00),
 (360,0,306,1394,0x00),
 (361,0,307,1394,0x00),
 (362,0,308,1394,0x00),
 (363,0,537,1394,0x00),
 (364,0,554,1394,0x01),
 (365,0,555,1394,0x00),
 (366,0,566,1394,0x00),
 (367,0,410,1419,0x00),
 (368,0,411,1419,0x00),
 (369,0,412,1419,0x00),
 (370,0,413,1419,0x00),
 (371,0,414,1419,0x00),
 (372,0,419,1419,0x00),
 (373,0,578,1419,0x00),
 (374,0,579,1419,0x00),
 (375,0,368,1419,0x00),
 (376,0,369,1419,0x00),
 (377,0,370,1419,0x00),
 (378,0,371,1419,0x00),
 (379,0,372,1419,0x00),
 (380,0,375,1419,0x00),
 (381,0,377,1419,0x00),
 (382,0,378,1419,0x00),
 (383,0,379,1419,0x01),
 (384,0,563,1419,0x00),
 (385,0,421,1420,0x00),
 (386,0,422,1420,0x00),
 (387,0,423,1420,0x00),
 (388,0,424,1420,0x00),
 (389,0,425,1420,0x00),
 (390,0,428,1420,0x00),
 (391,0,429,1420,0x00),
 (392,0,430,1420,0x01),
 (393,0,395,1420,0x00),
 (394,0,396,1420,0x00),
 (395,0,397,1420,0x00),
 (396,0,398,1420,0x00),
 (397,0,399,1420,0x00),
 (398,0,400,1420,0x00),
 (399,0,380,1421,0x00),
 (400,0,381,1421,0x00),
 (401,0,382,1421,0x00),
 (402,0,385,1421,0x00),
 (403,0,386,1421,0x01),
 (404,0,387,1421,0x00),
 (405,0,544,1421,0x00),
 (406,0,321,1421,0x00),
 (407,0,322,1421,0x00),
 (408,0,323,1421,0x00),
 (409,0,325,1421,0x00),
 (410,0,327,1421,0x00),
 (411,0,330,1421,0x00),
 (412,0,331,1421,0x00),
 (413,0,332,1421,0x00),
 (414,0,546,1421,0x00),
 (415,0,315,1422,0x00),
 (416,0,316,1422,0x00),
 (417,0,317,1422,0x00),
 (418,0,318,1422,0x00),
 (419,0,319,1422,0x00),
 (420,0,542,1422,0x01),
 (421,0,388,1422,0x00),
 (422,0,389,1422,0x00),
 (423,0,390,1422,0x00),
 (424,0,392,1422,0x00),
 (425,0,393,1422,0x00),
 (426,0,394,1422,0x00),
 (427,0,568,1383,0x00),
 (428,0,569,1383,0x00),
 (429,0,570,1383,0x00),
 (430,0,571,1383,0x01),
 (431,0,573,1383,0x00),
 (432,0,574,1383,0x00),
 (433,0,575,1383,0x00),
 (434,0,286,1383,0x00),
 (435,0,287,1383,0x00),
 (436,0,288,1383,0x00),
 (437,0,289,1383,0x00),
 (438,0,290,1383,0x00),
 (439,0,295,1383,0x00),
 (440,0,535,1383,0x00),
 (441,0,583,1383,0x00),
 (442,0,276,1387,0x00),
 (443,0,278,1387,0x00),
 (444,0,280,1387,0x00),
 (445,0,281,1387,0x00),
 (446,0,282,1387,0x00),
 (447,0,284,1387,0x00),
 (448,0,584,1387,0x00),
 (449,0,568,1387,0x00),
 (450,0,569,1387,0x01),
 (451,0,570,1387,0x00),
 (452,0,571,1387,0x00),
 (453,0,573,1387,0x00),
 (454,0,575,1387,0x00),
 (455,0,576,1387,0x00),
 (456,0,315,1426,0x00),
 (457,0,316,1426,0x00),
 (458,0,317,1426,0x00),
 (459,0,318,1426,0x00),
 (460,0,319,1426,0x01),
 (461,0,542,1426,0x00),
 (462,0,380,1426,0x00),
 (463,0,381,1426,0x00),
 (464,0,385,1426,0x00),
 (465,0,386,1426,0x00),
 (466,0,387,1426,0x00),
 (467,0,544,1426,0x00),
 (468,0,321,1424,0x00),
 (469,0,322,1424,0x00),
 (470,0,323,1424,0x00),
 (471,0,325,1424,0x00),
 (472,0,327,1424,0x00),
 (473,0,330,1424,0x00),
 (474,0,331,1424,0x00),
 (475,0,332,1424,0x01),
 (476,0,368,1424,0x00),
 (477,0,369,1424,0x00),
 (478,0,370,1424,0x00),
 (479,0,371,1424,0x00),
 (480,0,372,1424,0x00),
 (481,0,375,1424,0x00),
 (482,0,377,1424,0x00),
 (483,0,378,1424,0x00),
 (484,0,379,1424,0x00),
 (485,0,563,1424,0x00),
 (486,0,296,1395,0x00),
 (487,0,297,1395,0x00),
 (488,0,301,1395,0x00),
 (489,0,552,1395,0x00),
 (490,0,568,1395,0x00),
 (491,0,569,1395,0x00),
 (492,0,570,1395,0x00),
 (493,0,571,1395,0x01),
 (494,0,573,1395,0x00),
 (495,0,575,1395,0x00),
 (496,0,583,1395,0x00),
 (497,0,334,1396,0x00),
 (498,0,336,1396,0x00),
 (499,0,337,1396,0x00),
 (500,0,401,1396,0x00),
 (501,0,403,1396,0x00),
 (502,0,405,1396,0x01),
 (503,0,408,1396,0x00),
 (504,0,409,1396,0x00),
 (505,0,540,1396,0x00),
 (506,0,558,1396,0x00),
 (507,0,580,1396,0x00),
 (508,0,585,1396,0x00),
 (509,0,586,1396,0x00),
 (510,0,388,1425,0x00),
 (511,0,389,1425,0x00),
 (512,0,390,1425,0x01),
 (513,0,392,1425,0x00),
 (514,0,393,1425,0x00),
 (515,0,421,1425,0x00),
 (516,0,422,1425,0x00),
 (517,0,423,1425,0x00),
 (518,0,424,1425,0x00),
 (519,0,428,1425,0x00),
 (520,0,429,1425,0x00),
 (521,0,430,1425,0x00),
 (522,0,395,1423,0x00),
 (523,0,396,1423,0x00),
 (524,0,397,1423,0x00),
 (525,0,398,1423,0x00),
 (526,0,399,1423,0x00),
 (527,0,400,1423,0x00),
 (528,0,547,1423,0x00),
 (529,0,410,1423,0x00),
 (530,0,411,1423,0x00),
 (531,0,412,1423,0x00),
 (532,0,413,1423,0x00),
 (533,0,414,1423,0x01),
 (534,0,419,1423,0x00),
 (535,0,578,1423,0x00),
 (536,0,357,1398,0x00),
 (537,0,358,1398,0x00),
 (538,0,359,1398,0x00),
 (539,0,360,1398,0x00),
 (540,0,362,1398,0x00),
 (541,0,533,1398,0x01),
 (542,0,286,1398,0x00),
 (543,0,287,1398,0x00),
 (544,0,288,1398,0x00),
 (545,0,289,1398,0x00),
 (546,0,290,1398,0x00),
 (547,0,295,1398,0x00),
 (548,0,535,1398,0x00),
 (549,0,587,1395,0x00),
 (550,0,588,1426,0x00),
 (551,0,326,1424,0x00),
 (552,0,361,1398,0x00),
 (553,0,296,1400,0x00),
 (554,0,297,1400,0x00),
 (555,0,299,1400,0x00),
 (556,0,300,1400,0x00),
 (557,0,301,1400,0x00),
 (558,0,539,1400,0x00),
 (559,0,552,1400,0x00),
 (560,0,553,1400,0x00),
 (561,0,302,1400,0x00),
 (562,0,304,1400,0x00),
 (563,0,305,1400,0x00),
 (564,0,307,1400,0x01),
 (565,0,537,1400,0x00),
 (566,0,554,1400,0x00),
 (567,0,566,1400,0x00),
 (568,0,401,1401,0x00),
 (569,0,402,1401,0x00),
 (570,0,403,1401,0x00),
 (571,0,405,1401,0x00),
 (572,0,408,1401,0x01),
 (573,0,409,1401,0x00),
 (574,0,580,1401,0x00),
 (575,0,357,1401,0x00),
 (576,0,358,1401,0x00),
 (577,0,359,1401,0x00),
 (578,0,360,1401,0x00),
 (579,0,362,1401,0x00),
 (580,0,363,1401,0x00),
 (581,0,564,1401,0x00),
 (582,0,568,1399,0x00),
 (583,0,569,1399,0x01),
 (584,0,570,1399,0x00),
 (585,0,571,1399,0x00),
 (586,0,575,1399,0x00),
 (587,0,576,1399,0x00),
 (588,0,583,1399,0x00),
 (589,0,334,1399,0x00),
 (590,0,335,1399,0x00),
 (591,0,336,1399,0x00),
 (592,0,337,1399,0x00),
 (593,0,339,1399,0x00),
 (594,0,565,1399,0x00),
 (595,0,585,1399,0x00),
 (596,0,276,1402,0x00),
 (597,0,278,1402,0x00),
 (598,0,282,1402,0x00),
 (599,0,584,1402,0x00),
 (600,0,589,1402,0x00),
 (601,0,286,1402,0x00),
 (602,0,287,1402,0x00),
 (603,0,288,1402,0x00),
 (604,0,289,1402,0x00),
 (605,0,290,1402,0x00),
 (606,0,295,1402,0x00),
 (607,0,535,1402,0x01),
 (608,0,590,1402,0x00),
 (609,0,423,1430,0x00),
 (610,0,424,1430,0x00),
 (611,0,425,1430,0x00),
 (612,0,428,1430,0x00),
 (613,0,429,1430,0x00),
 (614,0,430,1430,0x00),
 (615,0,595,1430,0x01),
 (616,0,380,1430,0x00),
 (617,0,381,1430,0x00),
 (618,0,382,1430,0x00),
 (619,0,385,1430,0x00),
 (620,0,386,1430,0x00),
 (621,0,387,1430,0x00),
 (622,0,544,1430,0x00),
 (623,0,410,1427,0x00),
 (624,0,412,1427,0x00),
 (625,0,413,1427,0x00),
 (626,0,419,1427,0x00),
 (627,0,543,1427,0x00),
 (628,0,321,1427,0x01),
 (629,0,322,1427,0x00),
 (630,0,323,1427,0x00),
 (631,0,326,1427,0x00),
 (632,0,327,1427,0x00),
 (633,0,330,1427,0x00),
 (634,0,331,1427,0x00),
 (635,0,332,1427,0x00),
 (636,0,397,1428,0x00),
 (637,0,398,1428,0x00),
 (638,0,399,1428,0x00),
 (639,0,400,1428,0x00),
 (640,0,592,1428,0x00),
 (641,0,593,1428,0x00),
 (642,0,594,1428,0x00),
 (643,0,389,1428,0x00),
 (644,0,390,1428,0x00),
 (645,0,392,1428,0x00),
 (646,0,393,1428,0x00),
 (647,0,394,1428,0x01),
 (648,0,591,1428,0x00),
 (649,0,368,1429,0x00),
 (650,0,369,1429,0x00),
 (651,0,370,1429,0x00),
 (652,0,371,1429,0x00),
 (653,0,372,1429,0x00),
 (654,0,375,1429,0x00),
 (655,0,377,1429,0x00),
 (656,0,378,1429,0x00),
 (657,0,379,1429,0x00),
 (658,0,563,1429,0x00),
 (659,0,313,1429,0x00),
 (660,0,315,1429,0x00),
 (661,0,317,1429,0x00),
 (662,0,319,1429,0x01),
 (663,0,542,1429,0x00),
 (664,0,414,1427,0x00),
 (665,0,424,1465,0x01),
 (666,0,554,1435,0x01),
 (667,0,334,1435,0x00);
/*!40000 ALTER TABLE `puntaje_partido` ENABLE KEYS */;


--
-- Definition of table `sancion`
--

DROP TABLE IF EXISTS `sancion`;
CREATE TABLE `sancion` (
  `ID_SANCION` bigint(20) NOT NULL AUTO_INCREMENT,
  `CUMPLIDA` bit(1) DEFAULT NULL,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_INICIO_FK` bigint(20) DEFAULT NULL,
  `TIPO_SANCION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_SANCION`),
  KEY `FK99FAF085C127A392` (`TIPO_SANCION`),
  KEY `FK99FAF085C1295389` (`PARTIDO_INICIO_FK`),
  KEY `FK99FAF08518383FCB` (`JUGADOR_FK`),
  KEY `FK6F2F64A5C127A392` (`TIPO_SANCION`),
  KEY `FK6F2F64A5C1295389` (`PARTIDO_INICIO_FK`),
  KEY `FK6F2F64A518383FCB` (`JUGADOR_FK`),
  CONSTRAINT `FK6F2F64A518383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK6F2F64A5C127A392` FOREIGN KEY (`TIPO_SANCION`) REFERENCES `tipo_sancion` (`ID_TIPO_SANCION`),
  CONSTRAINT `FK6F2F64A5C1295389` FOREIGN KEY (`PARTIDO_INICIO_FK`) REFERENCES `partido` (`ID_PARTIDO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sancion`
--

/*!40000 ALTER TABLE `sancion` DISABLE KEYS */;
/*!40000 ALTER TABLE `sancion` ENABLE KEYS */;


--
-- Definition of table `sancion_partido`
--

DROP TABLE IF EXISTS `sancion_partido`;
CREATE TABLE `sancion_partido` (
  `ID_SANCION` bigint(20) NOT NULL,
  `ID_PARTIDO` bigint(20) NOT NULL,
  UNIQUE KEY `ID_PARTIDO` (`ID_PARTIDO`),
  KEY `FKB7B098073CCC7A45` (`ID_PARTIDO`),
  KEY `FKB7B0980779B1AFCD` (`ID_SANCION`),
  CONSTRAINT `FKB7B098073CCC7A45` FOREIGN KEY (`ID_PARTIDO`) REFERENCES `partido` (`ID_PARTIDO`),
  CONSTRAINT `FKB7B0980779B1AFCD` FOREIGN KEY (`ID_SANCION`) REFERENCES `sancion` (`ID_SANCION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sancion_partido`
--

/*!40000 ALTER TABLE `sancion_partido` DISABLE KEYS */;
/*!40000 ALTER TABLE `sancion_partido` ENABLE KEYS */;


--
-- Definition of table `tarjeta`
--

DROP TABLE IF EXISTS `tarjeta`;
CREATE TABLE `tarjeta` (
  `DTYPE` varchar(31) NOT NULL,
  `ID_TARJETA` bigint(20) NOT NULL AUTO_INCREMENT,
  `TIPO` varchar(255) DEFAULT NULL,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_TARJETA`),
  KEY `FKCF1CA9EDC42F976B` (`PARTIDO_FK`),
  KEY `FKCF1CA9ED18383FCB` (`JUGADOR_FK`),
  CONSTRAINT `FKCF1CA9ED18383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FKCF1CA9EDC42F976B` FOREIGN KEY (`PARTIDO_FK`) REFERENCES `partido` (`ID_PARTIDO`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tarjeta`
--

/*!40000 ALTER TABLE `tarjeta` DISABLE KEYS */;
INSERT INTO `tarjeta` (`DTYPE`,`ID_TARJETA`,`TIPO`,`JUGADOR_FK`,`PARTIDO_FK`) VALUES 
 ('TarjetaAmarilla',4,'AMARILLA',305,1380),
 ('TarjetaAmarilla',5,'AMARILLA',402,1382),
 ('TarjetaAmarilla',6,'AMARILLA',299,1382),
 ('TarjetaAmarilla',8,'AMARILLA',368,1413),
 ('TarjetaAmarilla',9,'AMARILLA',315,1412),
 ('TarjetaAmarilla',10,'AMARILLA',302,1385),
 ('TarjetaAmarilla',11,'AMARILLA',289,1388),
 ('TarjetaAmarilla',12,'AMARILLA',288,1388),
 ('TarjetaAmarilla',13,'AMARILLA',533,1389),
 ('TarjetaAmarilla',14,'AMARILLA',358,1389),
 ('TarjetaAmarilla',15,'AMARILLA',397,1420),
 ('TarjetaAmarilla',18,'AMARILLA',569,1387),
 ('TarjetaAmarilla',19,'AMARILLA',571,1387),
 ('TarjetaAmarilla',21,'AMARILLA',397,1423),
 ('TarjetaAmarilla',22,'AMARILLA',286,1398),
 ('TarjetaAmarilla',23,'AMARILLA',537,1400),
 ('TarjetaAmarilla',24,'AMARILLA',358,1401),
 ('TarjetaAmarilla',25,'AMARILLA',363,1401),
 ('TarjetaAmarilla',26,'AMARILLA',288,1402),
 ('TarjetaAmarilla',27,'AMARILLA',382,1430),
 ('TarjetaAmarilla',30,'AMARILLA',321,1427),
 ('TarjetaAmarilla',31,'AMARILLA',332,1427);
/*!40000 ALTER TABLE `tarjeta` ENABLE KEYS */;


--
-- Definition of table `tipo_entidad`
--

DROP TABLE IF EXISTS `tipo_entidad`;
CREATE TABLE `tipo_entidad` (
  `ID_TIPO_ENTIDAD` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(255) NOT NULL,
  PRIMARY KEY (`ID_TIPO_ENTIDAD`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_entidad`
--

/*!40000 ALTER TABLE `tipo_entidad` DISABLE KEYS */;
INSERT INTO `tipo_entidad` (`ID_TIPO_ENTIDAD`,`NOMBRE`) VALUES 
 (1,'page');
/*!40000 ALTER TABLE `tipo_entidad` ENABLE KEYS */;


--
-- Definition of table `tipo_sancion`
--

DROP TABLE IF EXISTS `tipo_sancion`;
CREATE TABLE `tipo_sancion` (
  `ID_TIPO_SANCION` bigint(20) NOT NULL AUTO_INCREMENT,
  `CANT_AMARILLAS` int(11) DEFAULT NULL,
  `CANT_ROJAS` int(11) DEFAULT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `DURACION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_TIPO_SANCION`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_sancion`
--

/*!40000 ALTER TABLE `tipo_sancion` DISABLE KEYS */;
INSERT INTO `tipo_sancion` (`ID_TIPO_SANCION`,`CANT_AMARILLAS`,`CANT_ROJAS`,`DESCRIPCION`,`DURACION`) VALUES 
 (1,2,0,'Doble amarilla',1),
 (2,1,1,'Roja por segunda amarilla',1),
 (3,2,1,'Roja por segunda amarilla (mal especificado)',1),
 (4,0,1,'Roja directa',2);
/*!40000 ALTER TABLE `tipo_sancion` ENABLE KEYS */;


--
-- Definition of table `torneo`
--

DROP TABLE IF EXISTS `torneo`;
CREATE TABLE `torneo` (
  `ID_TORNEO` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `cantidadFechas` int(11) DEFAULT NULL,
  `fechaActual` int(11) DEFAULT NULL,
  `idaYVuelta` bit(1) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `tipoTorneo` varchar(255) DEFAULT NULL,
  `jugadoresPorEquipo` int(11) DEFAULT NULL,
  `cantPuntosEmpate` int(11) DEFAULT NULL,
  `cantPuntosGanador` int(11) DEFAULT NULL,
  `cantPuntosPerdedor` int(11) DEFAULT NULL,
  `cantFechasSancionAmarilla` int(11) DEFAULT NULL,
  `cantFechasSancionRoja` int(11) DEFAULT NULL,
  `cantTarjAmarillasParaSancion` int(11) DEFAULT NULL,
  `cantTarjRojasParaSancion` int(11) DEFAULT NULL,
  `USUARIO_CREADOR_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_TORNEO`),
  KEY `FK93D6C8E1A313C4D0` (`USUARIO_CREADOR_FK`),
  CONSTRAINT `FK93D6C8E1A313C4D0` FOREIGN KEY (`USUARIO_CREADOR_FK`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `torneo`
--

/*!40000 ALTER TABLE `torneo` DISABLE KEYS */;
INSERT INTO `torneo` (`ID_TORNEO`,`activo`,`cantidadFechas`,`fechaActual`,`idaYVuelta`,`nombre`,`tipoTorneo`,`jugadoresPorEquipo`,`cantPuntosEmpate`,`cantPuntosGanador`,`cantPuntosPerdedor`,`cantFechasSancionAmarilla`,`cantFechasSancionRoja`,`cantTarjAmarillasParaSancion`,`cantTarjRojasParaSancion`,`USUARIO_CREADOR_FK`) VALUES 
 (43,0x01,7,7,0x00,'Nivelacion Zona 1','Todos contra Todos',5,1,3,0,1,1,3,1,NULL),
 (44,0x01,7,7,0x00,'Nivelacion Zona 2','Todos contra Todos',5,1,3,0,1,1,3,1,NULL),
 (45,0x01,7,1,0x00,'probando1','Todos contra Todos',5,1,3,0,1,1,5,1,NULL),
 (46,0x01,14,1,0x01,'probando2','Todos contra Todos',5,1,3,0,1,1,5,1,NULL);
/*!40000 ALTER TABLE `torneo` ENABLE KEYS */;


--
-- Definition of table `torneo_anterior`
--

DROP TABLE IF EXISTS `torneo_anterior`;
CREATE TABLE `torneo_anterior` (
  `torneo` bigint(20) DEFAULT NULL,
  `torneo_anterior` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `torneo_anterior`
--

/*!40000 ALTER TABLE `torneo_anterior` DISABLE KEYS */;
INSERT INTO `torneo_anterior` (`torneo`,`torneo_anterior`) VALUES 
 (45,43),
 (45,44),
 (46,43),
 (46,44);
/*!40000 ALTER TABLE `torneo_anterior` ENABLE KEYS */;


--
-- Definition of table `torneo_equipo`
--

DROP TABLE IF EXISTS `torneo_equipo`;
CREATE TABLE `torneo_equipo` (
  `TORNEO_FK` bigint(20) NOT NULL,
  `EQUIPO_FK` bigint(20) NOT NULL,
  KEY `FKE32C375D5DB4EB3D` (`TORNEO_FK`),
  KEY `FKE32C375DFABF067D` (`EQUIPO_FK`),
  CONSTRAINT `FKE32C375D5DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`),
  CONSTRAINT `FKE32C375DFABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `torneo_equipo`
--

/*!40000 ALTER TABLE `torneo_equipo` DISABLE KEYS */;
INSERT INTO `torneo_equipo` (`TORNEO_FK`,`EQUIPO_FK`) VALUES 
 (43,45),
 (43,41),
 (43,44),
 (43,40),
 (43,51),
 (43,38),
 (43,39),
 (43,46),
 (44,52),
 (44,49),
 (44,43),
 (44,50),
 (44,47),
 (44,53),
 (44,48),
 (44,42),
 (43,54),
 (45,44),
 (45,43),
 (45,42),
 (45,40),
 (45,45),
 (45,38),
 (45,39),
 (45,41),
 (46,54),
 (46,48),
 (46,52),
 (46,51),
 (46,49),
 (46,53),
 (46,50),
 (46,47);
/*!40000 ALTER TABLE `torneo_equipo` ENABLE KEYS */;


--
-- Definition of table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `ID_USUARIO` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVO` bit(1) DEFAULT NULL,
  `FECHA_CREACION` datetime DEFAULT NULL,
  `FECHA_ULTIMA_MODIFICACION` datetime DEFAULT NULL,
  `CLAVE` varchar(255) NOT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `USUARIO_CREACION` bigint(20) DEFAULT NULL,
  `USUARIO_ULTIMA_MODIFICACION` bigint(20) DEFAULT NULL,
  `TEL_CELULAR` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_USUARIO`),
  KEY `FK22E07F0E21D8C9D8` (`USUARIO_CREACION`),
  KEY `FK22E07F0EF5191012` (`USUARIO_ULTIMA_MODIFICACION`),
  CONSTRAINT `FK22E07F0E21D8C9D8` FOREIGN KEY (`USUARIO_CREACION`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FK22E07F0EF5191012` FOREIGN KEY (`USUARIO_ULTIMA_MODIFICACION`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario`
--

/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`ID_USUARIO`,`ACTIVO`,`FECHA_CREACION`,`FECHA_ULTIMA_MODIFICACION`,`CLAVE`,`NOMBRE`,`USUARIO_CREACION`,`USUARIO_ULTIMA_MODIFICACION`,`TEL_CELULAR`) VALUES 
 (1,0x01,NULL,NULL,'0cc175b9c0f1b6a831c399e269772661','a',NULL,NULL,''),
 (2,0x01,NULL,NULL,'eae479109863f5ae51ee7181f25b2650','zona1',NULL,NULL,''),
 (3,0x01,NULL,NULL,'0141a9a2363dfe0ec69478b02bd0656d','zona2',NULL,NULL,''),
 (4,0x01,NULL,NULL,'e3e5adec9deb6cfb8abe11c7a5e48d52','nacho_bergman@hotmail.com',NULL,NULL,NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;


--
-- Definition of procedure `actualizar_goleadores`
--

DROP PROCEDURE IF EXISTS `actualizar_goleadores`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_goleadores`(IN id_torneo BIGINT(20))
BEGIN

delete from posicion_goleador
where torneo_fk = id_torneo;

insert into posicion_goleador(goles,jugador_fk,torneo_fk)
select count(*) as cantGoles,g.jugador_fk,id_torneo
from gol g
inner join jugador j on j.id_jugador = g.jugador_fk
inner join partido p on p.id_partido = g.partido_fk
inner join fecha f on f.id_fecha = p.id_fecha
inner join torneo t on t.id_torneo = f.torneo_fk
where j.activo = true
and t.id_torneo = id_torneo
group by g.jugador_fk,id_torneo
order by cantGoles desc;


update posicion_goleador pg,
(
select sum(goles) as cantGoles,pg2.jugador_fk as jugId
from posicion_goleador pg2
where pg2.torneo_fk in (select torneo_anterior from torneo_anterior where torneo = id_torneo)
group by jugId
) tornAnt
set goles = goles + tornAnt.cantGoles
where pg.jugador_fk = tornAnt.jugId and pg.torneo_fk = id_torneo;

insert ignore into posicion_goleador(goles,jugador_fk,torneo_fk)
select sum(goles) as cantGoles,jugador_fk,id_torneo
from posicion_goleador pg2
inner join jugador j on j.id_jugador = pg2.jugador_fk
where pg2.torneo_fk in (select torneo_anterior from torneo_anterior where torneo = id_torneo)
and j.equipo_fk in (select equipo_fk from torneo_equipo where torneo_fk = id_torneo)
group by jugador_fk,id_torneo;


END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `actualizar_posiciones`
--

DROP PROCEDURE IF EXISTS `actualizar_posiciones`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_posiciones`(IN id_torneo BIGINT(20))
BEGIN
  declare cant_puntos_ganador int;
  declare cant_puntos_empate int;
  declare cant_puntos_perdedor int;

  select cantPuntosGanador from torneo t where t.id_torneo = id_torneo into cant_puntos_ganador;
  select cantPuntosEmpate from torneo t where t.id_torneo = id_torneo into cant_puntos_empate;
  select cantPuntosPerdedor from torneo t where t.id_torneo = id_torneo into cant_puntos_perdedor;

update posicion_torneo pos_tor
set pos_tor.puntos = 0,
pos_tor.partidosGanados = 0,
pos_tor.partidosEmpatados = 0,
pos_tor.partidosPerdidos = 0,
pos_tor.partidosJugados = 0,
pos_tor.golesAFavor = 0,
pos_tor.golesEnContra = 0
where pos_tor.torneo_fk = id_torneo;

update posicion_torneo pos_tor,
(SELECT equipo_local_fk as equipo,
SUM(CASE WHEN golesLocal > golesVisitante THEN cant_puntos_ganador when golesLocal = golesVisitante then cant_puntos_empate ELSE cant_puntos_perdedor END) as puntos,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as perdidos,
COUNT(*) as jugados,
SUM(golesLocal) as golesFavor,
SUM(golesVisitante) as golesContra
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = id_torneo and jugado = true and p.activo = true
GROUP BY equipo_local_fk) ploc
set pos_tor.puntos = ploc.puntos,
pos_tor.partidosGanados = ploc.ganados,
pos_tor.partidosEmpatados = ploc.empatados,
pos_tor.partidosPerdidos = ploc.perdidos,
pos_tor.partidosJugados = jugados,
pos_tor.golesAFavor = golesFavor,
pos_tor.golesEnContra = golesContra
where pos_tor.equipo_fk = ploc.equipo and pos_tor.torneo_fk = id_torneo;

update posicion_torneo pos_tor,
(SELECT equipo_visitante_fk as equipo,
SUM(CASE WHEN golesLocal < golesVisitante THEN cant_puntos_ganador when golesLocal = golesVisitante then cant_puntos_empate ELSE cant_puntos_perdedor END) as puntos,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as perdidos,
COUNT(*) as jugados,
SUM(golesVisitante) as golesFavor,
SUM(golesLocal) as golesContra
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = id_torneo and jugado = true and p.activo = true
GROUP BY equipo_visitante_fk) pvis
set pos_tor.puntos = pos_tor.puntos + pvis.puntos,
pos_tor.partidosGanados = pos_tor.partidosGanados + pvis.ganados,
pos_tor.partidosEmpatados = pos_tor.partidosEmpatados + pvis.empatados,
pos_tor.partidosPerdidos = pos_tor.partidosPerdidos + pvis.perdidos,
pos_tor.partidosJugados = pos_tor.partidosJugados + jugados,
pos_tor.golesAFavor = pos_tor.golesAFavor + golesFavor,
pos_tor.golesEnContra = pos_tor.golesEnContra + golesContra
where pos_tor.equipo_fk = pvis.equipo and pos_tor.torneo_fk = id_torneo;



END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
