SELECT e.id_equipo,e.nombre,p.puntos,p.partidosJugados,p.partidosGanados,p.partidosEmpatados,p.partidosPerdidos,golesAFavor,golesEnContra
FROM posicion_torneo p
inner join equipo e on p.equipo_fk = e.id_equipo
where torneo_fk = 4
order by puntos desc;

call actualizar_posiciones(4);

SELECT * FROM fecha f where f.torneo_fk = 4;

SELECT * FROM partido p;


SELECT equipo_local_fk as equipo,
SUM(CASE WHEN golesLocal > golesVisitante THEN 3 when golesLocal = golesVisitante then 1 ELSE 0 END) as puntos,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as perdidos
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = 4 and jugado = true and p.activo = true
GROUP BY equipo_local_fk;
SELECT equipo_visitante_fk as equipo,
SUM(CASE WHEN golesLocal < golesVisitante THEN 3 when golesLocal = golesVisitante then 1 ELSE 0 END) as puntos,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as perdidos
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = 4 and jugado = true and p.activo = true
GROUP BY equipo_visitante_fk;

update posicion_torneo pos_tor
set pos_tor.puntos = 0,
pos_tor.partidosGanados = 0,
pos_tor.partidosEmpatados = 0,
pos_tor.partidosPerdidos = 0;

update posicion_torneo pos_tor,
(SELECT equipo_local_fk as equipo,
SUM(CASE WHEN golesLocal > golesVisitante THEN 3 when golesLocal = golesVisitante then 1 ELSE 0 END) as puntos,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as perdidos
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = 4 and jugado = true and p.activo = true
GROUP BY equipo_local_fk) ploc
set pos_tor.puntos = ploc.puntos,
pos_tor.partidosGanados = ploc.ganados,
pos_tor.partidosEmpatados = ploc.empatados,
pos_tor.partidosPerdidos = ploc.perdidos
where pos_tor.equipo_fk = ploc.equipo and pos_tor.torneo_fk = 4;

update posicion_torneo pos_tor,
(SELECT equipo_visitante_fk as equipo,
SUM(CASE WHEN golesLocal < golesVisitante THEN 3 when golesLocal = golesVisitante then 1 ELSE 0 END) as puntos,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as perdidos
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = 4 and jugado = true and p.activo = true
GROUP BY equipo_visitante_fk) pvis
set pos_tor.puntos = pos_tor.puntos + pvis.puntos,
pos_tor.partidosGanados = pos_tor.puntos + pvis.ganados,
pos_tor.partidosEmpatados = pos_tor.puntos + pvis.empatados,
pos_tor.partidosPerdidos = pos_tor.puntos + pvis.perdidos
where pos_tor.equipo_fk = pvis.equipo and pos_tor.torneo_fk = 4;





(SELECT equipo_visitante_fk as equipo,
SUM(CASE WHEN golesLocal < golesVisitante THEN 3 when golesLocal = golesVisitante then 1 ELSE 0 END) as puntos,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as perdidos
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = 22 and jugado = true and p.activo = true
GROUP BY equipo_visitante_fk) pvis
where ploc.equipo = pvis.equipo) pos_calc
set pos_tor.puntos = pos_calc.puntos,
pos_tor.partidosGanados = pos_calc.ganados,
pos_tor.partidosEmpatados = pos_calc.empatados,
pos_tor.partidosPerdidos = pos_calc.perdidos
where pos_tor.equipo_fk = pos_calc.equipo and pos_tor.torneo_fk = 22;

select ploc.equipo,
ploc.puntos + pvis.puntos as puntos,
ploc.ganados + pvis.ganados as ganados,
ploc.empatados + pvis.empatados as empatados,
ploc.perdidos + pvis.perdidos as perdidos

FROM;
SELECT equipo_local_fk,
SUM(CASE WHEN golesLocal > golesVisitante THEN 3 when golesLocal = golesVisitante then 1 ELSE 0 END) as puntos,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as perdidos
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = 4 and jugado = true and p.activo = true
GROUP BY equipo_local_fk
union
SELECT equipo_visitante_fk,
SUM(CASE WHEN golesLocal < golesVisitante THEN 3 when golesLocal = golesVisitante then 1 ELSE 0 END) as puntos,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as perdidos
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = 4 and jugado = true and p.activo = true
GROUP BY equipo_visitante_fk;

SELECT equipo_local_fk as equipo_local,
SUM(CASE WHEN golesLocal > golesVisitante THEN 3 when golesLocal = golesVisitante then 1 ELSE 0 END) as puntos,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as perdidos,
equipo_visitante_fk as equipo_visitante,
SUM(CASE WHEN golesLocal < golesVisitante THEN 3 when golesLocal = golesVisitante then 1 ELSE 0 END) as puntos,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as perdidos
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = 22 and jugado = true and p.activo = true
GROUP BY equipo_local_fk,equipo_visitante_fk;

SELECT equipo_visitante_fk as equipo,
SUM(CASE WHEN golesLocal < golesVisitante THEN 3 when golesLocal = golesVisitante then 1 ELSE 0 END) as puntos,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as perdidos
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = 4 and jugado = true and p.activo = true
GROUP BY equipo_visitante_fk