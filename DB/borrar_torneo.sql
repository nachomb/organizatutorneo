DELIMITER $$

DROP PROCEDURE IF EXISTS `borrar_torneo` $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_torneo`(IN idTorneo BIGINT(20))
BEGIN

delete from partido where id_fecha in(
select id_fecha from fecha where torneo_fk = idTorneo
);

delete from fecha where torneo_fk = idTorneo;

delete from torneo_equipo where torneo_fk = idTorneo;

delete from posicion_torneo where torneo_fk = idTorneo;

delete from torneo where id_torneo = idTorneo;

END $$

DELIMITER ;