select * from jugador;

drop table jugador_temp;

create table jugador_temp(
equipo char(100),
apellido char(100),
nombre char(100),
mail char(100));

LOAD DATA INFILE 'C:\\B.csv'
INTO TABLE jugador_temp
FIELDS TERMINATED BY ';';

select * from jugador_temp;

update jugador_temp
set mail = SUBSTRING(mail, 1, CHAR_LENGTH(mail) - 1 );

insert into jugador(apellido,nombre,activo) select apellido,nombre,true from jugador_temp;

insert into equipo(nombre,activo) select distinct equipo,true from jugador_temp;

update jugador j,jugador_temp jt
set j.equipo_fk = (select id_equipo from equipo e where e.nombre = jt.equipo)
where j.nombre = jt.nombre and j.apellido = jt.apellido;






