-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.49-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema picadito
--

CREATE DATABASE IF NOT EXISTS picadito;
USE picadito;

--
-- Definition of table `accion`
--

DROP TABLE IF EXISTS `accion`;
CREATE TABLE `accion` (
  `ID_ACCION` bigint(20) NOT NULL AUTO_INCREMENT,
  `ENTIDAD` varchar(255) NOT NULL,
  `MENU_ENTRADA` varchar(255) NOT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `TIPO_ENTIDAD` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ACCION`),
  KEY `FK72BAB7E7FE022F9C` (`TIPO_ENTIDAD`),
  CONSTRAINT `FK72BAB7E7FE022F9C` FOREIGN KEY (`TIPO_ENTIDAD`) REFERENCES `tipo_entidad` (`ID_TIPO_ENTIDAD`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accion`
--

/*!40000 ALTER TABLE `accion` DISABLE KEYS */;
INSERT INTO `accion` (`ID_ACCION`,`ENTIDAD`,`MENU_ENTRADA`,`NOMBRE`,`TIPO_ENTIDAD`) VALUES 
 (171,'/Usuario*','Usuario_list','Usuarios',1),
 (172,'/Perfil*','Perfil_list','Perfiles',1),
 (173,'/Jugador*','Jugador_list','Jugadores',1),
 (174,'/Equipo*','Equipo_list','Equipos',1),
 (175,'/Torneo*','Torneo_list','Torneos',1),
 (176,'/Grupo*','Grupo_list','Grupos',1),
 (177,'/PartidoAmistoso*','PartidoAmistoso_list','PartidoAmistoso',1),
 (178,'/MiUsuario*','MiUsuario_miUsuarioListado','Mi Usuario',1),
 (179,'/MiJugador*','MiJugador_miJugadorListado','Mi Jugador',1),
 (180,'/MisEquipos*','MisEquipos','Mis Equipos',1),
 (181,'/MisTorneos*','MisTorneos','Mis Torneos',1),
 (182,'/MisGrupos*','MisGrupos','Mis Grupos',1),
 (183,'/MisPartidos*','MisPartidos','Mis Partidos',1),
 (184,'/MisInvitaciones*','MisInvitaciones','Mis Invitaciones',1);
/*!40000 ALTER TABLE `accion` ENABLE KEYS */;


--
-- Definition of table `alineacion`
--

DROP TABLE IF EXISTS `alineacion`;
CREATE TABLE `alineacion` (
  `ID_ALINEACION` bigint(20) NOT NULL AUTO_INCREMENT,
  `PARTIDO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ALINEACION`),
  KEY `FK41C471F193621169` (`PARTIDO`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alineacion`
--

/*!40000 ALTER TABLE `alineacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion` ENABLE KEYS */;


--
-- Definition of table `alineacion_amistoso`
--

DROP TABLE IF EXISTS `alineacion_amistoso`;
CREATE TABLE `alineacion_amistoso` (
  `ID_ALINEACION_AMISTOSO` bigint(20) NOT NULL AUTO_INCREMENT,
  `PARTIDO_AMISTOSO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ALINEACION_AMISTOSO`),
  KEY `FKAEEA5BFBEF4CB320` (`PARTIDO_AMISTOSO`),
  CONSTRAINT `FKAEEA5BFBEF4CB320` FOREIGN KEY (`PARTIDO_AMISTOSO`) REFERENCES `partido_amistoso` (`ID_PARTIDO_AMISTOSO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alineacion_amistoso`
--

/*!40000 ALTER TABLE `alineacion_amistoso` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_amistoso` ENABLE KEYS */;


--
-- Definition of table `alineacion_amistoso_titulares`
--

DROP TABLE IF EXISTS `alineacion_amistoso_titulares`;
CREATE TABLE `alineacion_amistoso_titulares` (
  `ID_ALINEACION_AMISTOSO` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) DEFAULT NULL,
  `NOMBRE_JUGADOR` varchar(255) DEFAULT NULL,
  KEY `FKB80031B1FD04914E` (`ID_ALINEACION_AMISTOSO`),
  CONSTRAINT `FKB80031B1FD04914E` FOREIGN KEY (`ID_ALINEACION_AMISTOSO`) REFERENCES `alineacion_amistoso` (`ID_ALINEACION_AMISTOSO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alineacion_amistoso_titulares`
--

/*!40000 ALTER TABLE `alineacion_amistoso_titulares` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_amistoso_titulares` ENABLE KEYS */;


--
-- Definition of table `alineacion_suplentes`
--

DROP TABLE IF EXISTS `alineacion_suplentes`;
CREATE TABLE `alineacion_suplentes` (
  `ID_ALINEACION` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) NOT NULL,
  KEY `FK3FC038CD7DEBF27F` (`ID_ALINEACION`),
  KEY `FK3FC038CD4FC1E07` (`ID_JUGADOR`),
  CONSTRAINT `FK3FC038CD4FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK3FC038CD7DEBF27F` FOREIGN KEY (`ID_ALINEACION`) REFERENCES `alineacion` (`ID_ALINEACION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alineacion_suplentes`
--

/*!40000 ALTER TABLE `alineacion_suplentes` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_suplentes` ENABLE KEYS */;


--
-- Definition of table `alineacion_titulares`
--

DROP TABLE IF EXISTS `alineacion_titulares`;
CREATE TABLE `alineacion_titulares` (
  `ID_ALINEACION` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) NOT NULL,
  KEY `FKD8C7CC277DEBF27F` (`ID_ALINEACION`),
  KEY `FKD8C7CC274FC1E07` (`ID_JUGADOR`),
  CONSTRAINT `FKD8C7CC274FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FKD8C7CC277DEBF27F` FOREIGN KEY (`ID_ALINEACION`) REFERENCES `alineacion` (`ID_ALINEACION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alineacion_titulares`
--

/*!40000 ALTER TABLE `alineacion_titulares` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_titulares` ENABLE KEYS */;


--
-- Definition of table `dummy`
--

DROP TABLE IF EXISTS `dummy`;
CREATE TABLE `dummy` (
  `ID_DUMMY` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID_DUMMY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dummy`
--

/*!40000 ALTER TABLE `dummy` DISABLE KEYS */;
/*!40000 ALTER TABLE `dummy` ENABLE KEYS */;


--
-- Definition of table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
CREATE TABLE `equipo` (
  `ID_EQUIPO` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVO` bit(1) DEFAULT NULL,
  `NOMBRE` varchar(255) DEFAULT NULL,
  `RUTA_IMG_ESCUDO` varchar(255) DEFAULT NULL,
  `IMAGEN_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_EQUIPO`),
  UNIQUE KEY `NOMBRE` (`NOMBRE`),
  KEY `FK7A5B923F8CDC29FD` (`IMAGEN_FK`),
  CONSTRAINT `FK7A5B923F8CDC29FD` FOREIGN KEY (`IMAGEN_FK`) REFERENCES `imagen` (`ID_IMAGEN`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipo`
--

/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
INSERT INTO `equipo` (`ID_EQUIPO`,`ACTIVO`,`NOMBRE`,`RUTA_IMG_ESCUDO`,`IMAGEN_FK`) VALUES 
 (38,0x01,'Banco Provincia',NULL,NULL),
 (39,0x01,'CA Rober Forever',NULL,NULL),
 (40,0x01,'Caño la Liga FC',NULL,NULL),
 (41,0x01,'Club Kar y Deportivo',NULL,NULL),
 (42,0x01,'Deportivo Colonos',NULL,NULL),
 (43,0x01,'El Team',NULL,NULL),
 (44,0x01,'Furia Rosa',NULL,NULL),
 (45,0x01,'HDL',NULL,NULL),
 (46,0x01,'I See Dead People FC',NULL,NULL),
 (47,0x01,'Juanas de Arco FC',NULL,NULL),
 (48,0x01,'La Liga-Moss FC',NULL,NULL),
 (49,0x01,'Pesutis FC',NULL,NULL),
 (50,0x01,'Santa Terracita',NULL,NULL),
 (51,0x01,'SITAS',NULL,NULL),
 (52,0x01,'Suricatas',NULL,NULL),
 (53,0x01,'Tirate un Caño',NULL,NULL);
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;


--
-- Definition of table `fecha`
--

DROP TABLE IF EXISTS `fecha`;
CREATE TABLE `fecha` (
  `ID_FECHA` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `idaYVuelta` bit(1) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `TORNEO_FK` bigint(20) DEFAULT NULL,
  `isVuelta` bit(1) DEFAULT NULL,
  `EQUIPO_LIBRE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_FECHA`),
  KEY `FK3FACF5D5DB4EB3D` (`TORNEO_FK`),
  KEY `FK3FACF5DFCD88930` (`EQUIPO_LIBRE`),
  CONSTRAINT `FK3FACF5D5DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`),
  CONSTRAINT `FK3FACF5DFCD88930` FOREIGN KEY (`EQUIPO_LIBRE`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=336 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fecha`
--

/*!40000 ALTER TABLE `fecha` DISABLE KEYS */;
INSERT INTO `fecha` (`ID_FECHA`,`activo`,`idaYVuelta`,`nombre`,`numero`,`TORNEO_FK`,`isVuelta`,`EQUIPO_LIBRE`) VALUES 
 (322,0x01,NULL,'Fecha 1',1,43,0x00,NULL),
 (323,0x01,NULL,'Fecha 2',2,43,0x00,NULL),
 (324,0x01,NULL,'Fecha 3',3,43,0x00,NULL),
 (325,0x01,NULL,'Fecha 4',4,43,0x00,NULL),
 (326,0x01,NULL,'Fecha 5',5,43,0x00,NULL),
 (327,0x01,NULL,'Fecha 6',6,43,0x00,NULL),
 (328,0x01,NULL,'Fecha 7',7,43,0x00,NULL),
 (329,0x01,NULL,'Fecha 1',1,44,0x00,NULL),
 (330,0x01,NULL,'Fecha 2',2,44,0x00,NULL),
 (331,0x01,NULL,'Fecha 3',3,44,0x00,NULL),
 (332,0x01,NULL,'Fecha 4',4,44,0x00,NULL),
 (333,0x01,NULL,'Fecha 5',5,44,0x00,NULL),
 (334,0x01,NULL,'Fecha 6',6,44,0x00,NULL),
 (335,0x01,NULL,'Fecha 7',7,44,0x00,NULL);
/*!40000 ALTER TABLE `fecha` ENABLE KEYS */;


--
-- Definition of table `gol`
--

DROP TABLE IF EXISTS `gol`;
CREATE TABLE `gol` (
  `ID_GOL` bigint(20) NOT NULL AUTO_INCREMENT,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_GOL`),
  KEY `FK11464C42F976B` (`PARTIDO_FK`),
  KEY `FK1146418383FCB` (`JUGADOR_FK`),
  CONSTRAINT `FK1146418383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK11464C42F976B` FOREIGN KEY (`PARTIDO_FK`) REFERENCES `partido` (`ID_PARTIDO`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gol`
--

/*!40000 ALTER TABLE `gol` DISABLE KEYS */;
/*!40000 ALTER TABLE `gol` ENABLE KEYS */;


--
-- Definition of table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
CREATE TABLE `grupo` (
  `ID_GRUPO` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `JUGADOR_CREADOR_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_GRUPO`),
  KEY `FK40F144940F0ADCE` (`JUGADOR_CREADOR_FK`),
  CONSTRAINT `FK40F144940F0ADCE` FOREIGN KEY (`JUGADOR_CREADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupo`
--

/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;


--
-- Definition of table `grupo_jugador`
--

DROP TABLE IF EXISTS `grupo_jugador`;
CREATE TABLE `grupo_jugador` (
  `ID_GRUPO` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) NOT NULL,
  KEY `FK916E89AC4FC1E07` (`ID_JUGADOR`),
  KEY `FK916E89AC9892C415` (`ID_GRUPO`),
  CONSTRAINT `FK916E89AC4FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK916E89AC9892C415` FOREIGN KEY (`ID_GRUPO`) REFERENCES `grupo` (`ID_GRUPO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupo_jugador`
--

/*!40000 ALTER TABLE `grupo_jugador` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo_jugador` ENABLE KEYS */;


--
-- Definition of table `imagen`
--

DROP TABLE IF EXISTS `imagen`;
CREATE TABLE `imagen` (
  `ID_IMAGEN` bigint(20) NOT NULL AUTO_INCREMENT,
  `imgSerialiazda` blob,
  PRIMARY KEY (`ID_IMAGEN`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imagen`
--

/*!40000 ALTER TABLE `imagen` DISABLE KEYS */;
INSERT INTO `imagen` (`ID_IMAGEN`,`imgSerialiazda`) VALUES 
 (1,NULL),
 (2,0x47494638396119000F00B30900CCB27FEDDB99FFFFEB663300996600CCCCCC99999999CCFF66666600000000000000000000000000000000000000000021F90401000009002C0000000019000F00400461302149E74C386B3D85F91F728CE4B5611DF809E2911CE64905744D23459E03C480E52884704894C952AAD66B14DB20418240B177021AAFBC82B3C2BD6E8D456F4740262B4BD7719945428313EB728B2986C7A33820CF970112FF423A197B560911003B);
/*!40000 ALTER TABLE `imagen` ENABLE KEYS */;


--
-- Definition of table `invitacion`
--

DROP TABLE IF EXISTS `invitacion`;
CREATE TABLE `invitacion` (
  `ID_INVITACION` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado` varchar(255) DEFAULT NULL,
  `mensaje` varchar(255) DEFAULT NULL,
  `ID_JUGADOR` bigint(20) DEFAULT NULL,
  `ID_PARTIDO_AMISTOSO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_INVITACION`),
  KEY `FK6421A94A4FC1E07` (`ID_JUGADOR`),
  KEY `FK6421A94A23481C4` (`ID_PARTIDO_AMISTOSO`),
  CONSTRAINT `FK6421A94A23481C4` FOREIGN KEY (`ID_PARTIDO_AMISTOSO`) REFERENCES `partido_amistoso` (`ID_PARTIDO_AMISTOSO`),
  CONSTRAINT `FK6421A94A4FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitacion`
--

/*!40000 ALTER TABLE `invitacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `invitacion` ENABLE KEYS */;


--
-- Definition of table `item_menu`
--

DROP TABLE IF EXISTS `item_menu`;
CREATE TABLE `item_menu` (
  `ID_ITEM_MENU` bigint(20) NOT NULL AUTO_INCREMENT,
  `FECHA_CREACION` datetime DEFAULT NULL,
  `FECHA_ULTIMA_MODIFICACION` datetime DEFAULT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `ORDEN` bigint(20) DEFAULT NULL,
  `TITULO` varchar(255) DEFAULT NULL,
  `ACCION` bigint(20) DEFAULT NULL,
  `USUARIO_CREACION` bigint(20) DEFAULT NULL,
  `USUARIO_ULTIMA_MODIFICACION` bigint(20) DEFAULT NULL,
  `ITEM_MENU_PADRE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ITEM_MENU`),
  KEY `FK2838470B21D8C9D8` (`USUARIO_CREACION`),
  KEY `FK2838470BF5191012` (`USUARIO_ULTIMA_MODIFICACION`),
  KEY `FK2838470B163E2E85` (`ACCION`),
  KEY `FK2838470B60B8561D` (`ITEM_MENU_PADRE`),
  CONSTRAINT `FK2838470B163E2E85` FOREIGN KEY (`ACCION`) REFERENCES `accion` (`ID_ACCION`),
  CONSTRAINT `FK2838470B21D8C9D8` FOREIGN KEY (`USUARIO_CREACION`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FK2838470B60B8561D` FOREIGN KEY (`ITEM_MENU_PADRE`) REFERENCES `item_menu` (`ID_ITEM_MENU`),
  CONSTRAINT `FK2838470BF5191012` FOREIGN KEY (`USUARIO_ULTIMA_MODIFICACION`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_menu`
--

/*!40000 ALTER TABLE `item_menu` DISABLE KEYS */;
INSERT INTO `item_menu` (`ID_ITEM_MENU`,`FECHA_CREACION`,`FECHA_ULTIMA_MODIFICACION`,`DESCRIPCION`,`ORDEN`,`TITULO`,`ACCION`,`USUARIO_CREACION`,`USUARIO_ULTIMA_MODIFICACION`,`ITEM_MENU_PADRE`) VALUES 
 (171,NULL,NULL,NULL,NULL,'Usuarios',171,NULL,NULL,NULL),
 (172,NULL,NULL,NULL,NULL,'Perfiles',172,NULL,NULL,NULL),
 (173,NULL,NULL,NULL,NULL,'Jugadores',173,NULL,NULL,NULL),
 (174,NULL,NULL,NULL,NULL,'Equipos',174,NULL,NULL,NULL),
 (175,NULL,NULL,NULL,NULL,'Torneos',175,NULL,NULL,NULL),
 (176,NULL,NULL,NULL,NULL,'Grupos',176,NULL,NULL,NULL),
 (177,NULL,NULL,NULL,NULL,'Partidos',177,NULL,NULL,NULL),
 (178,NULL,NULL,NULL,NULL,'Usuario',178,NULL,NULL,NULL),
 (179,NULL,NULL,NULL,NULL,'Jugador',179,NULL,NULL,NULL),
 (180,NULL,NULL,NULL,NULL,'Equipos',180,NULL,NULL,NULL),
 (181,NULL,NULL,NULL,NULL,'Torneos',181,NULL,NULL,NULL),
 (182,NULL,NULL,NULL,NULL,'Grupos',182,NULL,NULL,NULL),
 (183,NULL,NULL,NULL,NULL,'Partidos',183,NULL,NULL,NULL),
 (184,NULL,NULL,NULL,NULL,'Invitaciones',184,NULL,NULL,NULL);
/*!40000 ALTER TABLE `item_menu` ENABLE KEYS */;


--
-- Definition of table `jugador`
--

DROP TABLE IF EXISTS `jugador`;
CREATE TABLE `jugador` (
  `ID_JUGADOR` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `apodo` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `EQUIPO_FK` bigint(20) DEFAULT NULL,
  `USUARIO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_JUGADOR`),
  KEY `FKDFA027A2FABF067D` (`EQUIPO_FK`),
  KEY `FKDFA027A2C3F3AACD` (`USUARIO_FK`),
  CONSTRAINT `FKDFA027A2C3F3AACD` FOREIGN KEY (`USUARIO_FK`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FKDFA027A2FABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=532 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jugador`
--

/*!40000 ALTER TABLE `jugador` DISABLE KEYS */;
INSERT INTO `jugador` (`ID_JUGADOR`,`activo`,`apellido`,`apodo`,`nombre`,`EQUIPO_FK`,`USUARIO_FK`) VALUES 
 (276,0x01,'Decuzzi',NULL,'Jennifer',38,NULL),
 (277,0x01,'Alfonso',NULL,'Guadalupe',38,NULL),
 (278,0x01,'Risso Patron',NULL,'Belen',38,NULL),
 (279,0x01,'Albarracin',NULL,'Victoria',38,NULL),
 (280,0x01,'Albarracin',NULL,'Carolina',38,NULL),
 (281,0x01,'Gonzalez',NULL,'Macarena',38,NULL),
 (282,0x01,'Fontana',NULL,'Antonella',38,NULL),
 (283,0x01,'Roucco',NULL,'Mariana',38,NULL),
 (284,0x01,'Fiorino',NULL,'Ailen',38,NULL),
 (285,0x01,'Quiroga',NULL,'Micaela',38,NULL),
 (286,0x01,'Rial',NULL,'Solange',39,NULL),
 (287,0x01,'Avila',NULL,'Cintia',39,NULL),
 (288,0x01,'Breuer',NULL,'Jesica',39,NULL),
 (289,0x01,'d´Imperio',NULL,'Rosario',39,NULL),
 (290,0x01,'Rocio',NULL,'Ana',39,NULL),
 (291,0x01,'Albanese',NULL,'Camila',39,NULL),
 (292,0x01,'Del Puerto',NULL,'Angeles',39,NULL),
 (293,0x01,'Kovasevic',NULL,'Sol',39,NULL),
 (294,0x01,'Almandoz',NULL,'Ailen',39,NULL),
 (295,0x01,'Gonzalez',NULL,'Mariela',39,NULL),
 (296,0x01,'Guglielmo',NULL,'Paula',40,NULL),
 (297,0x01,'Santa Cruz',NULL,'Agustina',40,NULL),
 (298,0x01,'Bleuler',NULL,'Florencia',40,NULL),
 (299,0x01,'Castro',NULL,'Jesica',40,NULL),
 (300,0x01,'Benegas',NULL,'Valeria',40,NULL),
 (301,0x01,'Rosello',NULL,'Agostina',40,NULL),
 (302,0x01,'Pardo',NULL,'Karen',41,NULL),
 (303,0x01,'Sohns',NULL,'Jesica',41,NULL),
 (304,0x01,'Ortiz',NULL,'Analia',41,NULL),
 (305,0x01,'Alegre',NULL,'Natalia',41,NULL),
 (306,0x01,'Bususcovich',NULL,'Andrea',41,NULL),
 (307,0x01,'Codevilla',NULL,'Claudia',41,NULL),
 (308,0x01,'Garcia',NULL,'Natividad',41,NULL),
 (309,0x01,'Mattaliano',NULL,'Paola',41,NULL),
 (310,0x01,'Cannan',NULL,'Cecilia',41,NULL),
 (311,0x01,'Oliva',NULL,'Estela Maris',41,NULL),
 (312,0x01,'Sanchez',NULL,'Laura',41,NULL),
 (313,0x01,'Giudice',NULL,'Antonella',42,NULL),
 (314,0x01,'Dias de Leon',NULL,'Julieta',42,NULL),
 (315,0x01,'Sanchez',NULL,'Agustina',42,NULL),
 (316,0x01,'Viera',NULL,'Sofia',42,NULL),
 (317,0x01,'Lares',NULL,'Magali',42,NULL),
 (318,0x01,'Alvear',NULL,'Aldana',42,NULL),
 (319,0x01,'Davidoff',NULL,'Carolina',42,NULL),
 (320,0x01,'Cordova',NULL,'Barbara',42,NULL),
 (321,0x01,'Pillani',NULL,'Fiorella',43,NULL),
 (322,0x01,'Pillani',NULL,'Antonella',43,NULL),
 (323,0x01,'Cintolo',NULL,'Carolina',43,NULL),
 (324,0x01,'Linares',NULL,'Sofia',43,NULL),
 (325,0x01,'Crespo',NULL,'Carla',43,NULL),
 (326,0x01,'Leanza',NULL,'Maria Eugenia',43,NULL),
 (327,0x01,'Policarpi',NULL,'Florencia',43,NULL),
 (328,0x01,'Garcia',NULL,'Lucia',43,NULL),
 (329,0x01,'Patrone',NULL,'Stefania',43,NULL),
 (330,0x01,'Cepeda',NULL,'Lucia',43,NULL),
 (331,0x01,'Sulzberger',NULL,'Melanie',43,NULL),
 (332,0x01,'Uicich',NULL,'Belén',43,NULL),
 (333,0x01,'Yagueddu',NULL,'Maria Florencia',43,NULL),
 (334,0x01,'Ischia',NULL,'Nina',44,NULL),
 (335,0x01,'Parra',NULL,'Macarena',44,NULL),
 (336,0x01,'Torre',NULL,'Victoria',44,NULL),
 (337,0x01,'Gramajo',NULL,'Fany',44,NULL),
 (338,0x01,'Aversa',NULL,'Giuliana',44,NULL),
 (339,0x01,'Grondona',NULL,'Julia',44,NULL),
 (340,0x01,'De Luca',NULL,'Flavia',44,NULL),
 (341,0x01,'Varela',NULL,'Agustina',44,NULL),
 (342,0x01,'Civitillo',NULL,'Mariela',44,NULL),
 (343,0x01,'De Simone',NULL,'Lucia',45,NULL),
 (344,0x01,'Banegas',NULL,'Anabella',45,NULL),
 (345,0x01,'Ceruzzi',NULL,'Rocio',45,NULL),
 (346,0x01,'Clauzure',NULL,'Yamila',45,NULL),
 (347,0x01,'Cortez',NULL,'Ayelen',45,NULL),
 (348,0x01,'Echeverria',NULL,'Roberta',45,NULL),
 (349,0x01,'Fernandez',NULL,'Nadia',45,NULL),
 (350,0x01,'Julia',NULL,'Lucia',45,NULL),
 (351,0x01,'Lagos',NULL,'Ayelen',45,NULL),
 (352,0x01,'Laviña',NULL,'Mariana',45,NULL),
 (353,0x01,'Lòpez',NULL,'M. Soledad',45,NULL),
 (354,0x01,'Otero Rey',NULL,'Melisa',45,NULL),
 (355,0x01,'Sammartino',NULL,'Mariana',45,NULL),
 (356,0x01,'Teisseire',NULL,'Carolina',45,NULL),
 (357,0x01,'Cavalieri',NULL,'Micaela',46,NULL),
 (358,0x01,'Cavalieri',NULL,'Maylen',46,NULL),
 (359,0x01,'Herrera',NULL,'Priscila',46,NULL),
 (360,0x01,'Berta',NULL,'Sheila',46,NULL),
 (361,0x01,'Premio',NULL,'Romina',46,NULL),
 (362,0x01,'Agazzi',NULL,'Paula',46,NULL),
 (363,0x01,'Rivero',NULL,'Gladys',46,NULL),
 (364,0x01,'Nahir',NULL,'Cardoso',46,NULL),
 (365,0x01,'Pattacini',NULL,'Rocio',46,NULL),
 (366,0x01,'Pattacini',NULL,'Sol',46,NULL),
 (367,0x01,'Galeano',NULL,'Giselle',46,NULL),
 (368,0x01,'Zanellato',NULL,'Ivana',47,NULL),
 (369,0x01,'Parareda',NULL,'María Agustina',47,NULL),
 (370,0x01,'Zanellato',NULL,'Nadina',47,NULL),
 (371,0x01,'Mazzocchi',NULL,'María Laura',47,NULL),
 (372,0x01,'Adalian',NULL,'Priscila',47,NULL),
 (373,0x01,'Lema',NULL,'María Belen',47,NULL),
 (374,0x01,'de la Vega',NULL,'Florencia',47,NULL),
 (375,0x01,'Gonzalez Moss',NULL,'Emilia',47,NULL),
 (376,0x01,'Rinesi',NULL,'Pamela',47,NULL),
 (377,0x01,'Zamparo',NULL,'Daniela',47,NULL),
 (378,0x01,'Foglino',NULL,'Macarena',47,NULL),
 (379,0x01,'Merlos',NULL,'Maria Sol',47,NULL),
 (380,0x01,'Borsato',NULL,'Maria Agustina',48,NULL),
 (381,0x01,'Cosentino',NULL,'Nerina',48,NULL),
 (382,0x01,'Venditti',NULL,'Carolina',48,NULL),
 (383,0x01,'Cuitiño',NULL,'Nadia',48,NULL),
 (384,0x01,'Merlo',NULL,'Aldana',48,NULL),
 (385,0x01,'Borsato',NULL,'Maria Mercedes',48,NULL),
 (386,0x01,'Lemme',NULL,'Milagros',48,NULL),
 (387,0x01,'Della Villa',NULL,'Constanza',48,NULL),
 (388,0x01,'Nieto',NULL,'Tamara',49,NULL),
 (389,0x01,'Aguirre',NULL,'Estefania',49,NULL),
 (390,0x01,'Nieto',NULL,'Micaela',49,NULL),
 (391,0x01,'Nieto',NULL,'Yanina',49,NULL),
 (392,0x01,'Pizzagalli',NULL,'Johanna',49,NULL),
 (393,0x01,'Saccone',NULL,'Ana Carolina',49,NULL),
 (394,0x01,'Lacolla',NULL,'Carla',49,NULL),
 (395,0x01,'Belardita',NULL,'Paula',50,NULL),
 (396,0x01,'Claut',NULL,'Natalí',50,NULL),
 (397,0x01,'Buontempo',NULL,'Magali',50,NULL),
 (398,0x01,'Albini',NULL,'Belen',50,NULL),
 (399,0x01,'Ramirez',NULL,'Ivana',50,NULL),
 (400,0x01,'Alagastino',NULL,'Gisele',50,NULL),
 (401,0x01,'Duarte',NULL,'Estefania',51,NULL),
 (402,0x01,'Roldan',NULL,'Romina',51,NULL),
 (403,0x01,'Pepe',NULL,'Juana',51,NULL),
 (404,0x01,'Palacio',NULL,'Ivanna',51,NULL),
 (405,0x01,'Avendaño',NULL,'Luz',51,NULL),
 (406,0x01,'Azevedo',NULL,'Loana',51,NULL),
 (407,0x01,'Rodriguez',NULL,'Cintia',51,NULL),
 (408,0x01,'Forti',NULL,'Camila',51,NULL),
 (409,0x01,'Massabie',NULL,'Macarena',51,NULL),
 (410,0x01,'Lobo',NULL,'Ximena',52,NULL),
 (411,0x01,'Corvera Pose',NULL,'Victoria',52,NULL),
 (412,0x01,'Galasso',NULL,'Alina',52,NULL),
 (413,0x01,'Medina',NULL,'Maira',52,NULL),
 (414,0x01,'Liern',NULL,'Micaela',52,NULL),
 (415,0x01,'Betti',NULL,'Soledad',52,NULL),
 (416,0x01,'Betti',NULL,'Paula',52,NULL),
 (417,0x01,'Avila',NULL,'Mariel',52,NULL),
 (418,0x01,'Covatta',NULL,'Valeria',52,NULL),
 (419,0x01,'Andriani',NULL,'Silvina',52,NULL),
 (420,0x01,'Frascarelli',NULL,'Antonela',53,NULL),
 (421,0x01,'Imparato',NULL,'Giuliana',53,NULL),
 (422,0x01,'Imparato',NULL,'Eugenia',53,NULL),
 (423,0x01,'Paolillo',NULL,'Vanina',53,NULL),
 (424,0x01,'Indart',NULL,'Paloma',53,NULL),
 (425,0x01,'Rascon',NULL,'Bárbara',53,NULL),
 (426,0x01,'López Pumarega',NULL,'Sofía',53,NULL),
 (427,0x01,'Pros',NULL,'Julieta',53,NULL),
 (428,0x01,'Rodríguez',NULL,'Romina',53,NULL),
 (429,0x01,'Balboa',NULL,'Martina',53,NULL),
 (430,0x01,'Grau',NULL,'Victoria',53,NULL),
 (431,0x01,'Piazzi',NULL,'Luciana',53,NULL),
 (531,0x01,'Rodriguez','','Vanesa Fernanda',42,NULL);
/*!40000 ALTER TABLE `jugador` ENABLE KEYS */;


--
-- Definition of table `jugador_temp`
--

DROP TABLE IF EXISTS `jugador_temp`;
CREATE TABLE `jugador_temp` (
  `equipo` char(100) DEFAULT NULL,
  `apellido` char(100) DEFAULT NULL,
  `nombre` char(100) DEFAULT NULL,
  `mail` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jugador_temp`
--

/*!40000 ALTER TABLE `jugador_temp` DISABLE KEYS */;
INSERT INTO `jugador_temp` (`equipo`,`apellido`,`nombre`,`mail`) VALUES 
 ('Banco Provincia','Decuzzi','Jennifer','jenny_Dq_17@hotmail.com'),
 ('Banco Provincia','Alfonso','Guadalupe','guadamar@hotmail.com'),
 ('Banco Provincia','Risso Patron','Belen','Belu_risso@hotmail.com'),
 ('Banco Provincia','Albarracin','Victoria','victoria.s.albarracin@hotmail.com'),
 ('Banco Provincia','Albarracin','Carolina','carolina.l.albarracin@hotmail.com'),
 ('Banco Provincia','Gonzalez','Macarena','maca.gonzalez11@hotmail.com'),
 ('Banco Provincia','Fontana','Antonella','antofontis.g@hotmail.com'),
 ('Banco Provincia','Roucco','Mariana',''),
 ('Banco Provincia','Fiorino','Ailen','ailu.fiorino@hotmail.com'),
 ('Banco Provincia','Quiroga','Micaela','miku_q@hotmail.com'),
 ('CA Rober Forever','Rial','Solange','solange_rial@hotmail.com'),
 ('CA Rober Forever','Avila','Cintia','avilacin@hotmail.com'),
 ('CA Rober Forever','Breuer','Jesica','jesicabreuer86@hotmail.com'),
 ('CA Rober Forever','d´Imperio','Rosario','rosariodimperio@hotmail.com'),
 ('CA Rober Forever','Rocio','Ana','ana.rocio@live.com.ar'),
 ('CA Rober Forever','Albanese','Camila','camiladeaguero@hotmail.com'),
 ('CA Rober Forever','Del Puerto','Angeles','angeles.delpuerto@hotmail.com'),
 ('CA Rober Forever','Kovasevic','Sol','solkovasevic@hotmail.com'),
 ('CA Rober Forever','Almandoz','Ailen',''),
 ('CA Rober Forever','Gonzalez','Mariela',''),
 ('Caño la Liga FC','Guglielmo','Paula','Paula.guglielmo@hotmail.com'),
 ('Caño la Liga FC','Santa Cruz','Agustina','Agus-santacruz@hotmail.com'),
 ('Caño la Liga FC','Bleuler','Florencia','Flo._09@hotmail.com'),
 ('Caño la Liga FC','Castro','Jesica','lajeessii@hotmail.com'),
 ('Caño la Liga FC','Benegas','Valeria','vale_02@hotmail.com'),
 ('Caño la Liga FC','Rosello','Agostina','aaagos@hotmail.com'),
 ('Club Kar y Deportivo','Pardo','Karen','karenpardo76@hotmail.com'),
 ('Club Kar y Deportivo','Sohns','Jesica','ginat_hot@hotmail.com'),
 ('Club Kar y Deportivo','Ortiz','Analia','any_1422@hotmail.com'),
 ('Club Kar y Deportivo','Alegre','Natalia',''),
 ('Club Kar y Deportivo','Bususcovich','Andrea','yamefui09@hotmail.com'),
 ('Club Kar y Deportivo','Codevilla','Claudia','k.aiu69@hotmail.com'),
 ('Club Kar y Deportivo','Garcia','Natividad','natividadtequiero@hotmail.com'),
 ('Club Kar y Deportivo','Mattaliano','Paola','fernandamattaliano@hotmial.com'),
 ('Club Kar y Deportivo','Cannan','Cecilia',''),
 ('Club Kar y Deportivo','Oliva','Estela Maris',''),
 ('Club Kar y Deportivo','Sanchez','Laura','lauvi05@hotmail.com'),
 ('Deportivo Colonos','Giudice','Antonella','an.giudice@hotmail.com'),
 ('Deportivo Colonos','Dias de Leon','Julieta','julietadiasdeleon@hotmail.com'),
 ('Deportivo Colonos','Sanchez','Agustina','agus_9015@hotmail.com'),
 ('Deportivo Colonos','Viera','Sofia','ro69zn@hotmail.com'),
 ('Deportivo Colonos','Lares','Magali','loca2702@hotmail.com'),
 ('Deportivo Colonos','Alvear','Aldana','alvearaldana@hotmail.com'),
 ('Deportivo Colonos','Davidoff','Carolina','carochiss_14@hotmail.com'),
 ('Deportivo Colonos','Cordova','Barbara','baredith86@hotmail.com'),
 ('El Team','Pillani','Fiorella','fiorella.pillani@hotmail.com'),
 ('El Team','Pillani','Antonella','anti26@hotmail.com'),
 ('El Team','Cintolo','Carolina','cintolo.carolina@hotmail.com'),
 ('El Team','Linares','Sofia','linares.sofia@hotmail.com'),
 ('El Team','Crespo','Carla','crespo.carr@hotmail.com'),
 ('El Team','Leanza','Maria Eugenia','eugee.leanza@hotmail.com'),
 ('El Team','Policarpi','Florencia','florenciaa._@hotmail.com'),
 ('El Team','Garcia','Lucia','lulaia_@hotmail.com'),
 ('El Team','Patrone','Stefania','stefi_patrone@hotmail.com'),
 ('El Team','Cepeda','Lucia','lu-cepada@hotmail.com'),
 ('El Team','Sulzberger','Melanie','melisulz@hotmail.com'),
 ('El Team','Uicich','Belén','belu_uicich@hotmail.com'),
 ('El Team','Yagueddu','Maria Florencia','flor_yague@hotmail.com'),
 ('Furia Rosa','Ischia','Nina','nina_ischia17@hotmail.com'),
 ('Furia Rosa','Parra','Macarena','maca_parra@hotmail.com'),
 ('Furia Rosa','Torre','Victoria','mvt17_11@hotmail.com'),
 ('Furia Rosa','Gramajo','Fany','funny.gra@hotmail.com'),
 ('Furia Rosa','Aversa','Giuliana','giuli.aversa@hotmail.com'),
 ('Furia Rosa','Grondona','Julia','majugrondona@hotmail.com'),
 ('Furia Rosa','De Luca','Flavia','flavi_287@hotmail.com'),
 ('Furia Rosa','Varela','Agustina','varelaagustina@hotmail.com'),
 ('Furia Rosa','Civitillo','Mariela','marielacivitillo@hotmail.com'),
 ('HDL','De Simone','Lucia','lunadesi@hotmail.com'),
 ('HDL','Banegas','Anabella','yeimi_bc_12@hotmail.com'),
 ('HDL','Ceruzzi','Rocio','ro10@hotmail.com'),
 ('HDL','Clauzure','Yamila','yamy_vpc_88@hotmail.com'),
 ('HDL','Cortez','Ayelen','ayelore_23rei@hotmail.com'),
 ('HDL','Echeverria','Roberta','robycon_2003@hotmail.com'),
 ('HDL','Fernandez','Nadia','elglobodesanjusto@hotmail.com'),
 ('HDL','Julia','Lucia','lucia.julia.r@hotmail.com'),
 ('HDL','Lagos','Ayelen','aye10_nqn@hotmail.com'),
 ('HDL','Laviña','Mariana','mari16_larubia22@hotmail.com'),
 ('HDL','Lòpez','M. Soledad','solelopez13@hotmail.com'),
 ('HDL','Otero Rey','Melisa','m.e.l.10@hotmail.com'),
 ('HDL','Sammartino','Mariana','living-the-vida-loka@hotmail.com'),
 ('HDL','Teisseire','Carolina','karito_palermo_87@hotmail.com'),
 ('I See Dead People FC','Cavalieri','Micaela','micaela.cavalieri@hotmail.com'),
 ('I See Dead People FC','Cavalieri','Maylen','may_gallinita@hotmail.com'),
 ('I See Dead People FC','Herrera','Priscila','pricila_128@hotmail.com'),
 ('I See Dead People FC','Berta','Sheila','shey_x7uk@hotmail.com'),
 ('I See Dead People FC','Premio','Romina','romina_premio@hotmail.com'),
 ('I See Dead People FC','Agazzi','Paula','paulaagazzi@hotmail.com'),
 ('I See Dead People FC','Rivero','Gladys',''),
 ('I See Dead People FC','Nahir','Cardoso',''),
 ('I See Dead People FC','Pattacini','Rocio','roochii_26@hotmail.com'),
 ('I See Dead People FC','Pattacini','Sol',''),
 ('I See Dead People FC','Galeano','Giselle',''),
 ('Juanas de Arco FC','Zanellato','Ivana','i_zanellato@hotmail.com'),
 ('Juanas de Arco FC','Parareda','María Agustina','magu.08@hotmail.com'),
 ('Juanas de Arco FC','Zanellato','Nadina','naddu.06@hotmail.com'),
 ('Juanas de Arco FC','Mazzocchi','María Laura','marialaura.88@hotmail.com'),
 ('Juanas de Arco FC','Adalian','Priscila','prichuu.13@hotmail.com'),
 ('Juanas de Arco FC','Lema','María Belen','be_lenn@live.com.ar'),
 ('Juanas de Arco FC','de la Vega','Florencia','flor.67@hotmail.com'),
 ('Juanas de Arco FC','Gonzalez Moss','Emilia','emigm_182@hotmail.com'),
 ('Juanas de Arco FC','Rinesi','Pamela','pame_more@hotmail.com'),
 ('Juanas de Arco FC','Zamparo','Daniela','danielazamparo@hotmail.com'),
 ('Juanas de Arco FC','Foglino','Macarena','macaa_55@hotmail.com'),
 ('Juanas de Arco FC','Merlos','Maria Sol','ssol._@hotmail.com'),
 ('La Liga-Moss FC','Borsato','Maria Agustina','magus_borsato@hotmail.com'),
 ('La Liga-Moss FC','Cosentino','Nerina','neru_cosentino@hotmail.com'),
 ('La Liga-Moss FC','Venditti','Carolina','caro.venditti@hotmail.com'),
 ('La Liga-Moss FC','Cuitiño','Nadia','ncuitinio@hotmail.com'),
 ('La Liga-Moss FC','Merlo','Aldana','aldy.89@hotmail.com'),
 ('La Liga-Moss FC','Borsato','Maria Mercedes','mechiborsato@hotmail.com'),
 ('La Liga-Moss FC','Lemme','Milagros','mililemme@hotmail.com'),
 ('La Liga-Moss FC','Della Villa','Constanza','coty_d7@hotmail.com'),
 ('Pesutis FC','Nieto','Tamara','atamarita@hotmail.com'),
 ('Pesutis FC','Aguirre','Estefania','estefiiaguirre@hotmail.com'),
 ('Pesutis FC','Nieto','Micaela','mica_nieto@hotmail.com'),
 ('Pesutis FC','Nieto','Yanina','yani_nieto@hotmail.com'),
 ('Pesutis FC','Pizzagalli','Johanna','johapizzagalli@hotmail.com'),
 ('Pesutis FC','Saccone','Ana Carolina','carosaccone@hotmail.com'),
 ('Pesutis FC','Lacolla','Carla','carlita_laco@hotmail.com'),
 ('Santa Terracita','Belardita','Paula','Paulabelardita@hotmail.com'),
 ('Santa Terracita','Claut','Natalí','natyclaut@hotmail.com'),
 ('Santa Terracita','Buontempo','Magali','magali_buontempo14@hotmail.com'),
 ('Santa Terracita','Albini','Belen','mbalbini@gmail.com'),
 ('Santa Terracita','Ramirez','Ivana','ivanaramirez@hotmail.com'),
 ('Santa Terracita','Alagastino','Gisele','gisele_alagastino@hotmail.com'),
 ('SITAS','Duarte','Estefania','estefy_65@hotmail.com'),
 ('SITAS','Roldan','Romina','romina._roldan@hotmail.com'),
 ('SITAS','Pepe','Juana','campanita.vip@hotmail.com'),
 ('SITAS','Palacio','Ivanna','yo_la_grosa_93@hotmail.com'),
 ('SITAS','Avendaño','Luz','lucecitta@hotmail.com'),
 ('SITAS','Azevedo','Loana','lolaa.azevedo@hotmail.com'),
 ('SITAS','Rodriguez','Cintia','cintia.1611@hotmail.com'),
 ('SITAS','Forti','Camila','camii.forti@hotmail.com'),
 ('SITAS','Massabie','Macarena','macarenamassabie@hotmail.com'),
 ('Suricatas','Lobo','Ximena','ximena.lobo@hotmail.com'),
 ('Suricatas','Corvera Pose','Victoria','corver.rr@hotmail.com'),
 ('Suricatas','Galasso','Alina','alina_glas@hotmail.com'),
 ('Suricatas','Medina','Maira','may77_mg@hotmail.com'),
 ('Suricatas','Liern','Micaela','mica.liern@hotmail.com '),
 ('Suricatas','Betti','Soledad','soolee._@hotmail.com'),
 ('Suricatas','Betti','Paula','ppauu_@hotmail.com'),
 ('Suricatas','Avila','Mariel','maruavila@live.com.ar'),
 ('Suricatas','Covatta','Valeria','alita.vcb@hotmail.com'),
 ('Suricatas','Andriani','Silvina','silviandriani@hotmaill.com'),
 ('Tirate un Caño','Frascarelli','Antonela','anto.frasca@hotmail.com'),
 ('Tirate un Caño','Imparato','Giuliana','giuli.imparato@hotmail.com'),
 ('Tirate un Caño','Imparato','Eugenia','euge_rizi@hotmail.com'),
 ('Tirate un Caño','Paolillo','Vanina','vanina.sp@hotmail.com'),
 ('Tirate un Caño','Indart','Paloma','palo.indart@hotmail.com'),
 ('Tirate un Caño','Rascon','Bárbara','barbie_rascon@hotmail.com'),
 ('Tirate un Caño','López Pumarega','Sofía','sofylp89@hotmail.com'),
 ('Tirate un Caño','Pros','Julieta','julieta.pr@hotmail.com'),
 ('Tirate un Caño','Rodríguez','Romina','romi_lahormiga05@hotmail.com'),
 ('Tirate un Caño','Balboa','Martina','marti_balboa@hotmail.com'),
 ('Tirate un Caño','Grau','Victoria','victoriaa.3@hotmail.com'),
 ('Tirate un Caño','Piazzi','Luciana','');
/*!40000 ALTER TABLE `jugador_temp` ENABLE KEYS */;


--
-- Definition of table `partido`
--

DROP TABLE IF EXISTS `partido`;
CREATE TABLE `partido` (
  `ID_PARTIDO` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `golesLocal` int(11) DEFAULT NULL,
  `golesVisitante` int(11) DEFAULT NULL,
  `jugado` bit(1) DEFAULT NULL,
  `EQUIPO_LOCAL_FK` bigint(20) DEFAULT NULL,
  `EQUIPO_VISITANTE_FK` bigint(20) DEFAULT NULL,
  `fecha_ID_FECHA` bigint(20) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `fechaPartido` datetime DEFAULT NULL,
  `ALINEACION_LOCAL` bigint(20) DEFAULT NULL,
  `ALINEACION_VISITANTE` bigint(20) DEFAULT NULL,
  `ID_FECHA` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_PARTIDO`),
  KEY `FKFB8855C1BC4EC0BF` (`fecha_ID_FECHA`),
  KEY `FKFB8855C1D1BD3B53` (`EQUIPO_VISITANTE_FK`),
  KEY `FKFB8855C191634FD1` (`EQUIPO_LOCAL_FK`),
  KEY `FKFB8855C1B2E6CD45` (`ALINEACION_VISITANTE`),
  KEY `FKFB8855C1EE33A287` (`ALINEACION_LOCAL`),
  KEY `FKFB8855C1986A3A3D` (`ID_FECHA`)
) ENGINE=InnoDB AUTO_INCREMENT=1435 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partido`
--

/*!40000 ALTER TABLE `partido` DISABLE KEYS */;
INSERT INTO `partido` (`ID_PARTIDO`,`activo`,`golesLocal`,`golesVisitante`,`jugado`,`EQUIPO_LOCAL_FK`,`EQUIPO_VISITANTE_FK`,`fecha_ID_FECHA`,`descripcion`,`fechaPartido`,`ALINEACION_LOCAL`,`ALINEACION_VISITANTE`,`ID_FECHA`) VALUES 
 (1379,0x01,0,0,0x00,46,45,NULL,NULL,NULL,NULL,NULL,322),
 (1380,0x01,0,0,0x00,39,41,NULL,NULL,NULL,NULL,NULL,322),
 (1381,0x01,0,0,0x00,38,44,NULL,NULL,NULL,NULL,NULL,322),
 (1382,0x01,0,0,0x00,51,40,NULL,NULL,NULL,NULL,NULL,322),
 (1383,0x01,0,0,0x00,45,39,NULL,NULL,NULL,NULL,NULL,323),
 (1384,0x01,0,0,0x00,46,38,NULL,NULL,NULL,NULL,NULL,323),
 (1385,0x01,0,0,0x00,41,51,NULL,NULL,NULL,NULL,NULL,323),
 (1386,0x01,0,0,0x00,44,40,NULL,NULL,NULL,NULL,NULL,323),
 (1387,0x01,0,0,0x00,38,45,NULL,NULL,NULL,NULL,NULL,324),
 (1388,0x01,0,0,0x00,51,39,NULL,NULL,NULL,NULL,NULL,324),
 (1389,0x01,0,0,0x00,40,46,NULL,NULL,NULL,NULL,NULL,324),
 (1390,0x01,0,0,0x00,44,41,NULL,NULL,NULL,NULL,NULL,324),
 (1391,0x01,0,0,0x00,45,51,NULL,NULL,NULL,NULL,NULL,325),
 (1392,0x01,0,0,0x00,38,40,NULL,NULL,NULL,NULL,NULL,325),
 (1393,0x01,0,0,0x00,39,44,NULL,NULL,NULL,NULL,NULL,325),
 (1394,0x01,0,0,0x00,46,41,NULL,NULL,NULL,NULL,NULL,325),
 (1395,0x01,0,0,0x00,40,45,NULL,NULL,NULL,NULL,NULL,326),
 (1396,0x01,0,0,0x00,44,51,NULL,NULL,NULL,NULL,NULL,326),
 (1397,0x01,0,0,0x00,41,38,NULL,NULL,NULL,NULL,NULL,326),
 (1398,0x01,0,0,0x00,46,39,NULL,NULL,NULL,NULL,NULL,326),
 (1399,0x01,0,0,0x00,45,44,NULL,NULL,NULL,NULL,NULL,327),
 (1400,0x01,0,0,0x00,40,41,NULL,NULL,NULL,NULL,NULL,327),
 (1401,0x01,0,0,0x00,51,46,NULL,NULL,NULL,NULL,NULL,327),
 (1402,0x01,0,0,0x00,38,39,NULL,NULL,NULL,NULL,NULL,327),
 (1403,0x01,0,0,0x00,41,45,NULL,NULL,NULL,NULL,NULL,328),
 (1404,0x01,0,0,0x00,46,44,NULL,NULL,NULL,NULL,NULL,328),
 (1405,0x01,0,0,0x00,39,40,NULL,NULL,NULL,NULL,NULL,328),
 (1406,0x01,0,0,0x00,38,51,NULL,NULL,NULL,NULL,NULL,328),
 (1407,0x01,0,0,0x00,42,52,NULL,NULL,NULL,NULL,NULL,329),
 (1408,0x01,0,0,0x00,48,49,NULL,NULL,NULL,NULL,NULL,329),
 (1409,0x01,0,0,0x00,53,43,NULL,NULL,NULL,NULL,NULL,329),
 (1410,0x01,0,0,0x00,47,50,NULL,NULL,NULL,NULL,NULL,329),
 (1411,0x01,0,0,0x00,52,48,NULL,NULL,NULL,NULL,NULL,330),
 (1412,0x01,0,0,0x00,42,53,NULL,NULL,NULL,NULL,NULL,330),
 (1413,0x01,0,0,0x00,49,47,NULL,NULL,NULL,NULL,NULL,330),
 (1414,0x01,0,0,0x00,43,50,NULL,NULL,NULL,NULL,NULL,330),
 (1415,0x01,0,0,0x00,53,52,NULL,NULL,NULL,NULL,NULL,331),
 (1416,0x01,0,0,0x00,47,48,NULL,NULL,NULL,NULL,NULL,331),
 (1417,0x01,0,0,0x00,50,42,NULL,NULL,NULL,NULL,NULL,331),
 (1418,0x01,0,0,0x00,43,49,NULL,NULL,NULL,NULL,NULL,331),
 (1419,0x01,0,0,0x00,52,47,NULL,NULL,NULL,NULL,NULL,332),
 (1420,0x01,0,0,0x00,53,50,NULL,NULL,NULL,NULL,NULL,332),
 (1421,0x01,0,0,0x00,48,43,NULL,NULL,NULL,NULL,NULL,332),
 (1422,0x01,0,0,0x00,42,49,NULL,NULL,NULL,NULL,NULL,332),
 (1423,0x01,0,0,0x00,50,52,NULL,NULL,NULL,NULL,NULL,333),
 (1424,0x01,0,0,0x00,43,47,NULL,NULL,NULL,NULL,NULL,333),
 (1425,0x01,0,0,0x00,49,53,NULL,NULL,NULL,NULL,NULL,333),
 (1426,0x01,0,0,0x00,42,48,NULL,NULL,NULL,NULL,NULL,333),
 (1427,0x01,0,0,0x00,52,43,NULL,NULL,NULL,NULL,NULL,334),
 (1428,0x01,0,0,0x00,50,49,NULL,NULL,NULL,NULL,NULL,334),
 (1429,0x01,0,0,0x00,47,42,NULL,NULL,NULL,NULL,NULL,334),
 (1430,0x01,0,0,0x00,53,48,NULL,NULL,NULL,NULL,NULL,334),
 (1431,0x01,0,0,0x00,49,52,NULL,NULL,NULL,NULL,NULL,335),
 (1432,0x01,0,0,0x00,42,43,NULL,NULL,NULL,NULL,NULL,335),
 (1433,0x01,0,0,0x00,48,50,NULL,NULL,NULL,NULL,NULL,335),
 (1434,0x01,0,0,0x00,53,47,NULL,NULL,NULL,NULL,NULL,335);
/*!40000 ALTER TABLE `partido` ENABLE KEYS */;


--
-- Definition of table `partido_amistoso`
--

DROP TABLE IF EXISTS `partido_amistoso`;
CREATE TABLE `partido_amistoso` (
  `ID_PARTIDO_AMISTOSO` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantParticipantes` int(11) DEFAULT NULL,
  `lugar` varchar(255) DEFAULT NULL,
  `mensaje` varchar(255) DEFAULT NULL,
  `modo` varchar(255) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `ID_GRUPO` bigint(20) DEFAULT NULL,
  `USUARIO_CREADOR_FK` bigint(20) DEFAULT NULL,
  `ALINEACION_EQUIPOA` bigint(20) DEFAULT NULL,
  `ALINEACION_EQUIPOB` bigint(20) DEFAULT NULL,
  `GOLES_EQUIPO_A` bigint(20) DEFAULT NULL,
  `GOLES_EQUIPO_B` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_PARTIDO_AMISTOSO`),
  KEY `FKDB81C22B9892C415` (`ID_GRUPO`),
  KEY `FKDB81C22BA313C4D0` (`USUARIO_CREADOR_FK`),
  KEY `FKDB81C22B7C64902B` (`ALINEACION_EQUIPOA`),
  KEY `FKDB81C22B7C64902C` (`ALINEACION_EQUIPOB`),
  CONSTRAINT `FKDB81C22B7C64902B` FOREIGN KEY (`ALINEACION_EQUIPOA`) REFERENCES `alineacion_amistoso` (`ID_ALINEACION_AMISTOSO`),
  CONSTRAINT `FKDB81C22B7C64902C` FOREIGN KEY (`ALINEACION_EQUIPOB`) REFERENCES `alineacion_amistoso` (`ID_ALINEACION_AMISTOSO`),
  CONSTRAINT `FKDB81C22B9892C415` FOREIGN KEY (`ID_GRUPO`) REFERENCES `grupo` (`ID_GRUPO`),
  CONSTRAINT `FKDB81C22BA313C4D0` FOREIGN KEY (`USUARIO_CREADOR_FK`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partido_amistoso`
--

/*!40000 ALTER TABLE `partido_amistoso` DISABLE KEYS */;
/*!40000 ALTER TABLE `partido_amistoso` ENABLE KEYS */;


--
-- Definition of table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
CREATE TABLE `perfil` (
  `ID_PERFIL` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVO` bit(1) DEFAULT NULL,
  `FECHA_CREACION` datetime DEFAULT NULL,
  `FECHA_ULTIMA_MODIFICACION` datetime DEFAULT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `USUARIO_CREACION` bigint(20) DEFAULT NULL,
  `USUARIO_ULTIMA_MODIFICACION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_PERFIL`),
  KEY `FK8C765DCC21D8C9D8` (`USUARIO_CREACION`),
  KEY `FK8C765DCCF5191012` (`USUARIO_ULTIMA_MODIFICACION`),
  CONSTRAINT `FK8C765DCC21D8C9D8` FOREIGN KEY (`USUARIO_CREACION`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FK8C765DCCF5191012` FOREIGN KEY (`USUARIO_ULTIMA_MODIFICACION`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perfil`
--

/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` (`ID_PERFIL`,`ACTIVO`,`FECHA_CREACION`,`FECHA_ULTIMA_MODIFICACION`,`DESCRIPCION`,`NOMBRE`,`USUARIO_CREACION`,`USUARIO_ULTIMA_MODIFICACION`) VALUES 
 (41,0x01,NULL,NULL,NULL,'PADMIN',NULL,NULL),
 (42,0x01,NULL,NULL,NULL,'PUSER',NULL,NULL);
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;


--
-- Definition of table `perfil_accion`
--

DROP TABLE IF EXISTS `perfil_accion`;
CREATE TABLE `perfil_accion` (
  `ID_PERFIL` bigint(20) NOT NULL,
  `ID_ACCION` bigint(20) NOT NULL,
  KEY `FK4BB179BA1BB53A29` (`ID_ACCION`),
  KEY `FK4BB179BA4F2C85F3` (`ID_PERFIL`),
  CONSTRAINT `FK4BB179BA1BB53A29` FOREIGN KEY (`ID_ACCION`) REFERENCES `accion` (`ID_ACCION`),
  CONSTRAINT `FK4BB179BA4F2C85F3` FOREIGN KEY (`ID_PERFIL`) REFERENCES `perfil` (`ID_PERFIL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perfil_accion`
--

/*!40000 ALTER TABLE `perfil_accion` DISABLE KEYS */;
INSERT INTO `perfil_accion` (`ID_PERFIL`,`ID_ACCION`) VALUES 
 (41,171),
 (41,172),
 (41,173),
 (41,174),
 (41,175),
 (41,176),
 (41,177),
 (42,178),
 (42,179),
 (42,180),
 (42,181),
 (42,182),
 (42,183),
 (42,184);
/*!40000 ALTER TABLE `perfil_accion` ENABLE KEYS */;


--
-- Definition of table `perfil_usuario`
--

DROP TABLE IF EXISTS `perfil_usuario`;
CREATE TABLE `perfil_usuario` (
  `ID_PERFIL` bigint(20) NOT NULL,
  `ID_USUARIO` bigint(20) NOT NULL,
  KEY `FK68C1F79B4F2C85F3` (`ID_PERFIL`),
  KEY `FK68C1F79BD77A7F61` (`ID_USUARIO`),
  CONSTRAINT `FK68C1F79B4F2C85F3` FOREIGN KEY (`ID_PERFIL`) REFERENCES `perfil` (`ID_PERFIL`),
  CONSTRAINT `FK68C1F79BD77A7F61` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perfil_usuario`
--

/*!40000 ALTER TABLE `perfil_usuario` DISABLE KEYS */;
INSERT INTO `perfil_usuario` (`ID_PERFIL`,`ID_USUARIO`) VALUES 
 (41,1),
 (42,1);
/*!40000 ALTER TABLE `perfil_usuario` ENABLE KEYS */;


--
-- Definition of table `posicion_torneo`
--

DROP TABLE IF EXISTS `posicion_torneo`;
CREATE TABLE `posicion_torneo` (
  `ID_POSICION` bigint(20) NOT NULL AUTO_INCREMENT,
  `amarillas` int(11) DEFAULT NULL,
  `golesAFavor` int(11) DEFAULT NULL,
  `golesEnContra` int(11) DEFAULT NULL,
  `partidosEmpatados` int(11) DEFAULT NULL,
  `partidosGanados` int(11) DEFAULT NULL,
  `partidosJugados` int(11) DEFAULT NULL,
  `partidosPerdidos` int(11) DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `rojas` int(11) DEFAULT NULL,
  `EQUIPO_FK` bigint(20) DEFAULT NULL,
  `TORNEO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_POSICION`),
  KEY `FK8BF26DE6FABF067D` (`EQUIPO_FK`),
  KEY `FK8BF26DE65DB4EB3D` (`TORNEO_FK`),
  CONSTRAINT `FK8BF26DE65DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`),
  CONSTRAINT `FK8BF26DE6FABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posicion_torneo`
--

/*!40000 ALTER TABLE `posicion_torneo` DISABLE KEYS */;
INSERT INTO `posicion_torneo` (`ID_POSICION`,`amarillas`,`golesAFavor`,`golesEnContra`,`partidosEmpatados`,`partidosGanados`,`partidosJugados`,`partidosPerdidos`,`puntos`,`rojas`,`EQUIPO_FK`,`TORNEO_FK`) VALUES 
 (146,0,0,0,0,0,0,0,0,0,45,43),
 (147,0,0,0,0,0,0,0,0,0,41,43),
 (148,0,0,0,0,0,0,0,0,0,44,43),
 (149,0,0,0,0,0,0,0,0,0,40,43),
 (150,0,0,0,0,0,0,0,0,0,51,43),
 (151,0,0,0,0,0,0,0,0,0,38,43),
 (152,0,0,0,0,0,0,0,0,0,39,43),
 (153,0,0,0,0,0,0,0,0,0,46,43),
 (154,0,0,0,0,0,0,0,0,0,52,44),
 (155,0,0,0,0,0,0,0,0,0,49,44),
 (156,0,0,0,0,0,0,0,0,0,43,44),
 (157,0,0,0,0,0,0,0,0,0,50,44),
 (158,0,0,0,0,0,0,0,0,0,47,44),
 (159,0,0,0,0,0,0,0,0,0,53,44),
 (160,0,0,0,0,0,0,0,0,0,48,44),
 (161,0,0,0,0,0,0,0,0,0,42,44);
/*!40000 ALTER TABLE `posicion_torneo` ENABLE KEYS */;


--
-- Definition of table `puntaje_partido`
--

DROP TABLE IF EXISTS `puntaje_partido`;
CREATE TABLE `puntaje_partido` (
  `ID_PUNTAJEPARTIDO` bigint(20) NOT NULL AUTO_INCREMENT,
  `puntaje` int(11) DEFAULT NULL,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_FK` bigint(20) DEFAULT NULL,
  `figura` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ID_PUNTAJEPARTIDO`),
  KEY `FK2F416253C42F976B` (`PARTIDO_FK`),
  KEY `FK2F41625318383FCB` (`JUGADOR_FK`),
  CONSTRAINT `FK2F41625318383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`)
) ENGINE=InnoDB AUTO_INCREMENT=282 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `puntaje_partido`
--

/*!40000 ALTER TABLE `puntaje_partido` DISABLE KEYS */;
/*!40000 ALTER TABLE `puntaje_partido` ENABLE KEYS */;


--
-- Definition of table `sancion`
--

DROP TABLE IF EXISTS `sancion`;
CREATE TABLE `sancion` (
  `ID_SANCION` bigint(20) NOT NULL AUTO_INCREMENT,
  `CUMPLIDA` bit(1) DEFAULT NULL,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_INICIO_FK` bigint(20) DEFAULT NULL,
  `TIPO_SANCION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_SANCION`),
  KEY `FK99FAF085C127A392` (`TIPO_SANCION`),
  KEY `FK99FAF085C1295389` (`PARTIDO_INICIO_FK`),
  KEY `FK99FAF08518383FCB` (`JUGADOR_FK`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sancion`
--

/*!40000 ALTER TABLE `sancion` DISABLE KEYS */;
INSERT INTO `sancion` (`ID_SANCION`,`CUMPLIDA`,`JUGADOR_FK`,`PARTIDO_INICIO_FK`,`TIPO_SANCION`) VALUES 
 (1,0x00,202,1328,4),
 (2,0x00,203,1328,4);
/*!40000 ALTER TABLE `sancion` ENABLE KEYS */;


--
-- Definition of table `sancion_partido`
--

DROP TABLE IF EXISTS `sancion_partido`;
CREATE TABLE `sancion_partido` (
  `ID_SANCION` bigint(20) NOT NULL,
  `ID_PARTIDO` bigint(20) NOT NULL,
  UNIQUE KEY `ID_PARTIDO` (`ID_PARTIDO`),
  KEY `FKB7B098073CCC7A45` (`ID_PARTIDO`),
  KEY `FKB7B0980779B1AFCD` (`ID_SANCION`),
  CONSTRAINT `FKB7B098073CCC7A45` FOREIGN KEY (`ID_PARTIDO`) REFERENCES `partido` (`ID_PARTIDO`),
  CONSTRAINT `FKB7B0980779B1AFCD` FOREIGN KEY (`ID_SANCION`) REFERENCES `sancion` (`ID_SANCION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sancion_partido`
--

/*!40000 ALTER TABLE `sancion_partido` DISABLE KEYS */;
/*!40000 ALTER TABLE `sancion_partido` ENABLE KEYS */;


--
-- Definition of table `tarjeta`
--

DROP TABLE IF EXISTS `tarjeta`;
CREATE TABLE `tarjeta` (
  `DTYPE` varchar(31) NOT NULL,
  `ID_TARJETA` bigint(20) NOT NULL AUTO_INCREMENT,
  `TIPO` varchar(255) DEFAULT NULL,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_TARJETA`),
  KEY `FKCF1CA9EDC42F976B` (`PARTIDO_FK`),
  KEY `FKCF1CA9ED18383FCB` (`JUGADOR_FK`),
  CONSTRAINT `FKCF1CA9ED18383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FKCF1CA9EDC42F976B` FOREIGN KEY (`PARTIDO_FK`) REFERENCES `partido` (`ID_PARTIDO`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tarjeta`
--

/*!40000 ALTER TABLE `tarjeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarjeta` ENABLE KEYS */;


--
-- Definition of table `tipo_entidad`
--

DROP TABLE IF EXISTS `tipo_entidad`;
CREATE TABLE `tipo_entidad` (
  `ID_TIPO_ENTIDAD` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(255) NOT NULL,
  PRIMARY KEY (`ID_TIPO_ENTIDAD`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_entidad`
--

/*!40000 ALTER TABLE `tipo_entidad` DISABLE KEYS */;
INSERT INTO `tipo_entidad` (`ID_TIPO_ENTIDAD`,`NOMBRE`) VALUES 
 (1,'ACCION');
/*!40000 ALTER TABLE `tipo_entidad` ENABLE KEYS */;


--
-- Definition of table `tipo_sancion`
--

DROP TABLE IF EXISTS `tipo_sancion`;
CREATE TABLE `tipo_sancion` (
  `ID_TIPO_SANCION` bigint(20) NOT NULL AUTO_INCREMENT,
  `CANT_AMARILLAS` int(11) DEFAULT NULL,
  `CANT_ROJAS` int(11) DEFAULT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `DURACION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_TIPO_SANCION`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_sancion`
--

/*!40000 ALTER TABLE `tipo_sancion` DISABLE KEYS */;
INSERT INTO `tipo_sancion` (`ID_TIPO_SANCION`,`CANT_AMARILLAS`,`CANT_ROJAS`,`DESCRIPCION`,`DURACION`) VALUES 
 (1,2,0,'Doble amarilla',1),
 (2,1,1,'Roja por segunda amarilla',1),
 (3,2,1,'Roja por segunda amarilla (mal especificado)',1),
 (4,0,1,'Roja directa',2);
/*!40000 ALTER TABLE `tipo_sancion` ENABLE KEYS */;


--
-- Definition of table `torneo`
--

DROP TABLE IF EXISTS `torneo`;
CREATE TABLE `torneo` (
  `ID_TORNEO` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `cantidadFechas` int(11) DEFAULT NULL,
  `fechaActual` int(11) DEFAULT NULL,
  `idaYVuelta` bit(1) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `tipoTorneo` varchar(255) DEFAULT NULL,
  `jugadoresPorEquipo` int(11) DEFAULT NULL,
  `cantPuntosEmpate` int(11) DEFAULT NULL,
  `cantPuntosGanador` int(11) DEFAULT NULL,
  `cantPuntosPerdedor` int(11) DEFAULT NULL,
  `cantFechasSancionAmarilla` int(11) DEFAULT NULL,
  `cantFechasSancionRoja` int(11) DEFAULT NULL,
  `cantTarjAmarillasParaSancion` int(11) DEFAULT NULL,
  `cantTarjRojasParaSancion` int(11) DEFAULT NULL,
  `USUARIO_CREADOR_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_TORNEO`),
  KEY `FK93D6C8E1A313C4D0` (`USUARIO_CREADOR_FK`),
  CONSTRAINT `FK93D6C8E1A313C4D0` FOREIGN KEY (`USUARIO_CREADOR_FK`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `torneo`
--

/*!40000 ALTER TABLE `torneo` DISABLE KEYS */;
INSERT INTO `torneo` (`ID_TORNEO`,`activo`,`cantidadFechas`,`fechaActual`,`idaYVuelta`,`nombre`,`tipoTorneo`,`jugadoresPorEquipo`,`cantPuntosEmpate`,`cantPuntosGanador`,`cantPuntosPerdedor`,`cantFechasSancionAmarilla`,`cantFechasSancionRoja`,`cantTarjAmarillasParaSancion`,`cantTarjRojasParaSancion`,`USUARIO_CREADOR_FK`) VALUES 
 (43,0x01,7,1,0x00,'Nivelacion Zona 1','Todos contra Todos',5,1,3,0,1,1,3,1,NULL),
 (44,0x01,7,1,0x00,'Nivelacion Zona 2','Todos contra Todos',5,1,3,0,1,1,3,1,NULL);
/*!40000 ALTER TABLE `torneo` ENABLE KEYS */;


--
-- Definition of table `torneo_equipo`
--

DROP TABLE IF EXISTS `torneo_equipo`;
CREATE TABLE `torneo_equipo` (
  `TORNEO_FK` bigint(20) NOT NULL,
  `EQUIPO_FK` bigint(20) NOT NULL,
  KEY `FKE32C375D5DB4EB3D` (`TORNEO_FK`),
  KEY `FKE32C375DFABF067D` (`EQUIPO_FK`),
  CONSTRAINT `FKE32C375D5DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`),
  CONSTRAINT `FKE32C375DFABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `torneo_equipo`
--

/*!40000 ALTER TABLE `torneo_equipo` DISABLE KEYS */;
INSERT INTO `torneo_equipo` (`TORNEO_FK`,`EQUIPO_FK`) VALUES 
 (43,45),
 (43,41),
 (43,44),
 (43,40),
 (43,51),
 (43,38),
 (43,39),
 (43,46),
 (44,52),
 (44,49),
 (44,43),
 (44,50),
 (44,47),
 (44,53),
 (44,48),
 (44,42);
/*!40000 ALTER TABLE `torneo_equipo` ENABLE KEYS */;


--
-- Definition of table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `ID_USUARIO` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVO` bit(1) DEFAULT NULL,
  `FECHA_CREACION` datetime DEFAULT NULL,
  `FECHA_ULTIMA_MODIFICACION` datetime DEFAULT NULL,
  `CLAVE` varchar(255) NOT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `USUARIO_CREACION` bigint(20) DEFAULT NULL,
  `USUARIO_ULTIMA_MODIFICACION` bigint(20) DEFAULT NULL,
  `TEL_CELULAR` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_USUARIO`),
  KEY `FK22E07F0E21D8C9D8` (`USUARIO_CREACION`),
  KEY `FK22E07F0EF5191012` (`USUARIO_ULTIMA_MODIFICACION`),
  CONSTRAINT `FK22E07F0E21D8C9D8` FOREIGN KEY (`USUARIO_CREACION`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FK22E07F0EF5191012` FOREIGN KEY (`USUARIO_ULTIMA_MODIFICACION`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario`
--

/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`ID_USUARIO`,`ACTIVO`,`FECHA_CREACION`,`FECHA_ULTIMA_MODIFICACION`,`CLAVE`,`NOMBRE`,`USUARIO_CREACION`,`USUARIO_ULTIMA_MODIFICACION`,`TEL_CELULAR`) VALUES 
 (1,0x01,NULL,NULL,'0cc175b9c0f1b6a831c399e269772661','a',NULL,NULL,NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;


--
-- Definition of procedure `actualizar_posiciones`
--

DROP PROCEDURE IF EXISTS `actualizar_posiciones`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_posiciones`(IN id_torneo BIGINT(20))
BEGIN
  declare cant_puntos_ganador int;
  declare cant_puntos_empate int;
  declare cant_puntos_perdedor int;

  select cantPuntosGanador from torneo t where t.id_torneo = id_torneo into cant_puntos_ganador;
  select cantPuntosEmpate from torneo t where t.id_torneo = id_torneo into cant_puntos_empate;
  select cantPuntosPerdedor from torneo t where t.id_torneo = id_torneo into cant_puntos_perdedor;

update posicion_torneo pos_tor
set pos_tor.puntos = 0,
pos_tor.partidosGanados = 0,
pos_tor.partidosEmpatados = 0,
pos_tor.partidosPerdidos = 0,
pos_tor.partidosJugados = 0,
pos_tor.golesAFavor = 0,
pos_tor.golesEnContra = 0
where pos_tor.torneo_fk = id_torneo;

update posicion_torneo pos_tor,
(SELECT equipo_local_fk as equipo,
SUM(CASE WHEN golesLocal > golesVisitante THEN cant_puntos_ganador when golesLocal = golesVisitante then cant_puntos_empate ELSE cant_puntos_perdedor END) as puntos,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as perdidos,
COUNT(*) as jugados,
SUM(golesLocal) as golesFavor,
SUM(golesVisitante) as golesContra
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = id_torneo and jugado = true and p.activo = true
GROUP BY equipo_local_fk) ploc
set pos_tor.puntos = ploc.puntos,
pos_tor.partidosGanados = ploc.ganados,
pos_tor.partidosEmpatados = ploc.empatados,
pos_tor.partidosPerdidos = ploc.perdidos,
pos_tor.partidosJugados = jugados,
pos_tor.golesAFavor = golesFavor,
pos_tor.golesEnContra = golesContra
where pos_tor.equipo_fk = ploc.equipo and pos_tor.torneo_fk = id_torneo;

update posicion_torneo pos_tor,
(SELECT equipo_visitante_fk as equipo,
SUM(CASE WHEN golesLocal < golesVisitante THEN cant_puntos_ganador when golesLocal = golesVisitante then cant_puntos_empate ELSE cant_puntos_perdedor END) as puntos,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as perdidos,
COUNT(*) as jugados,
SUM(golesVisitante) as golesFavor,
SUM(golesLocal) as golesContra
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = id_torneo and jugado = true and p.activo = true
GROUP BY equipo_visitante_fk) pvis
set pos_tor.puntos = pos_tor.puntos + pvis.puntos,
pos_tor.partidosGanados = pos_tor.partidosGanados + pvis.ganados,
pos_tor.partidosEmpatados = pos_tor.partidosEmpatados + pvis.empatados,
pos_tor.partidosPerdidos = pos_tor.partidosPerdidos + pvis.perdidos,
pos_tor.partidosJugados = pos_tor.partidosJugados + jugados,
pos_tor.golesAFavor = pos_tor.golesAFavor + golesFavor,
pos_tor.golesEnContra = pos_tor.golesEnContra + golesContra
where pos_tor.equipo_fk = pvis.equipo and pos_tor.torneo_fk = id_torneo;



END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
