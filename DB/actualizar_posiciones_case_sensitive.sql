﻿drop procedure if exists actualizar_posiciones;
DELIMITER //
CREATE PROCEDURE actualizar_posiciones(IN ID_TORNEO BIGINT(20))
BEGIN
  declare cant_puntos_ganador int;
  declare cant_puntos_empate int;
  declare cant_puntos_perdedor int;

  select cantPuntosGanador from TORNEO t where t.ID_TORNEO = ID_TORNEO into cant_puntos_ganador;
  select cantPuntosEmpate from TORNEO t where t.ID_TORNEO = ID_TORNEO into cant_puntos_empate;
  select cantPuntosPerdedor from TORNEO t where t.ID_TORNEO = ID_TORNEO into cant_puntos_perdedor;

update POSICION_TORNEO pos_tor
set pos_tor.puntos = 0,
pos_tor.partidosGanados = 0,
pos_tor.partidosEmpatados = 0,
pos_tor.partidosPerdidos = 0,
pos_tor.partidosJugados = 0,
pos_tor.golesAFavor = 0,
pos_tor.golesEnContra = 0
where pos_tor.TORNEO_FK = ID_TORNEO;

update POSICION_TORNEO pos_tor,
(SELECT equipo_local_fk as equipo,
SUM(CASE WHEN golesLocal > golesVisitante THEN cant_puntos_ganador when golesLocal = golesVisitante then cant_puntos_empate ELSE cant_puntos_perdedor END) as puntos,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as perdidos,
COUNT(*) as jugados,
SUM(golesLocal) as golesFavor,
SUM(golesVisitante) as golesContra
FROM PARTIDO p
INNER JOIN FECHA f on f.ID_FECHA = p.ID_FECHA
WHERE f.TORNEO_FK = ID_TORNEO and jugado = true and p.activo = true
GROUP BY equipo_local_fk) ploc
set pos_tor.puntos = ploc.puntos,
pos_tor.partidosGanados = ploc.ganados,
pos_tor.partidosEmpatados = ploc.empatados,
pos_tor.partidosPerdidos = ploc.perdidos,
pos_tor.partidosJugados = jugados,
pos_tor.golesAFavor = golesFavor,
pos_tor.golesEnContra = golesContra
where pos_tor.equipo_fk = ploc.equipo and pos_tor.TORNEO_FK = ID_TORNEO;

update POSICION_TORNEO pos_tor,
(SELECT equipo_visitante_fk as equipo,
SUM(CASE WHEN golesLocal < golesVisitante THEN cant_puntos_ganador when golesLocal = golesVisitante then cant_puntos_empate ELSE cant_puntos_perdedor END) as puntos,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as perdidos,
COUNT(*) as jugados,
SUM(golesVisitante) as golesFavor,
SUM(golesLocal) as golesContra
FROM PARTIDO p
INNER JOIN FECHA f on f.ID_FECHA = p.ID_FECHA
WHERE f.TORNEO_FK = ID_TORNEO and jugado = true and p.activo = true
GROUP BY equipo_visitante_fk) pvis
set pos_tor.puntos = pos_tor.puntos + pvis.puntos,
pos_tor.partidosGanados = pos_tor.partidosGanados + pvis.ganados,
pos_tor.partidosEmpatados = pos_tor.partidosEmpatados + pvis.empatados,
pos_tor.partidosPerdidos = pos_tor.partidosPerdidos + pvis.perdidos,
pos_tor.partidosJugados = pos_tor.partidosJugados + jugados,
pos_tor.golesAFavor = pos_tor.golesAFavor + golesFavor,
pos_tor.golesEnContra = pos_tor.golesEnContra + golesContra
where pos_tor.equipo_fk = pvis.equipo and pos_tor.TORNEO_FK = ID_TORNEO;



END //
DELIMITER ;
