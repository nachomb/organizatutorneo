﻿DELETE FROM perfil_accion;
DELETE FROM perfil_usuario;
DELETE FROM perfil;
DELETE FROM item_menu;
DELETE FROM accion;
DELETE FROM tipo_entidad;
DELETE FROM tipo_sancion;


/*
PUESTA EN PRODUCCION
*/
INSERT INTO tipo_entidad (ID_tipo_entidad, NOMBRE)
VALUES (1, 'page');

/*DESARROLLO
INSERT INTO TIPO_ENTIDAD (ID_TIPO_ENTIDAD, NOMBRE)
VALUES (1, 'ACCION');*/


/**
 *  acciones generales para administrador
 * */
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/Usuario*','Usuario_list','Usuarios',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/perfil*','Perfil_list','perfiles',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/Jugador*','Jugador_list','Jugadores',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/Equipo*','Equipo_list','Equipos',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/Torneo*','Torneo_list','Torneos',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/Publicidad*','Publicidad_list','Publicidad',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/ProgramacionPartidos*','ProgramacionPartidos','ProgramacionPartidos',1);
/*insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/Grupo*','Grupo_list','Grupos',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/PartidoAmistoso*','PartidoAmistoso_list','PartidoAmistoso',1);*/
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/Exporter*','Exporter_estadisticas','Exporter',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/Resumen*','Resumen_list','Resumen',1);
/*insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/Partido_input*','Partido_input','Partido',1);*/
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/Sede*','Sede_list','Sedes',1);


/**
 * Items de menu generales para administrador
 */
insert into item_menu (titulo,accion)
values ('Usuarios',(select ID_accion from accion where NOMBRE = 'Usuarios'));
insert into item_menu (titulo,accion)
values ('perfiles',(select ID_accion from accion where NOMBRE = 'perfiles'));
insert into item_menu (titulo,accion)
values ('Jugadores',(select ID_accion from accion where NOMBRE = 'Jugadores'));
insert into item_menu (titulo,accion)
values ('Equipos',(select ID_accion from accion where NOMBRE = 'Equipos'));
insert into item_menu (titulo,accion)
values ('Torneos',(select ID_accion from accion where NOMBRE = 'Torneos'));
insert into item_menu (titulo,accion)
values ('Publicidad',(select ID_accion from accion where NOMBRE = 'Publicidad'));
insert into item_menu (titulo,accion)
values ('Horarios',(select ID_accion from accion where NOMBRE = 'ProgramacionPartidos'));
insert into item_menu (titulo,accion)
values ('Resumenes',(select ID_accion from accion where NOMBRE = 'Resumen'));
insert into item_menu (titulo,accion)
values ('Sedes',(select ID_accion from accion where NOMBRE = 'Sedes'));
/*insert into item_menu (titulo,accion)
values ('Grupos',(select ID_accion from accion where NOMBRE = 'Grupos'));
insert into item_menu (titulo,accion)
values ('Partidos',(select ID_accion from accion where NOMBRE = 'PartidoAmistoso'));*/


/*
 * perfil de administrador
 */
insert into perfil (ACTIVO,NOMBRE)
values (true,'PADMIN');

/**
 * Se asigna perfil de administrador al usuario con id 1
 */
insert into perfil_usuario (ID_perfil,ID_USUARIO)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),1);

/**
 * acciones asignadas al perfil de administrador
 */
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_accion from accion where NOMBRE = 'Usuarios'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_accion from accion where NOMBRE = 'perfiles'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_accion from accion where NOMBRE = 'Jugadores'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_accion from accion where NOMBRE = 'Equipos'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_accion from accion where NOMBRE = 'Torneos'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_accion from accion where NOMBRE = 'Publicidad'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_accion from accion where NOMBRE = 'ProgramacionPartidos'));
/*insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_accion from accion where NOMBRE = 'Grupos'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_accion from accion where NOMBRE = 'PartidoAmistoso'));*/
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_accion from accion where NOMBRE = 'Exporter'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_accion from accion where NOMBRE = 'Resumen'));
/*insert into perfil_accion (ID_PERFIL,ID_ACCION)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_ACCION from accion where NOMBRE = 'Partido'));*/
insert into perfil_accion (ID_PERFIL,ID_ACCION)
values ((select ID_perfil from perfil where NOMBRE = 'PADMIN'),(select ID_ACCION from accion where NOMBRE = 'Sedes'));

/**
 * Usuario "comun"
 **/

/**
 *  acciones para usuario comun
 * */
/* insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/MiUsuario*','MiUsuario_miUsuarioListado','Mi Usuario',1); */
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/MiJugador*','MiJugador_miJugadorListado','Mi Jugador',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/MisEquipos*','MisEquipos','Mis Equipos',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/MisTorneos*','MisTorneos','Mis Torneos',1);
/* insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/MisGrupos*','MisGrupos','Mis Grupos',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/MisPartidos*','MisPartidos_listAmistoso','Mis Partidos',1);
insert into accion (ENTIDAD,MENU_ENTRADA,NOMBRE,tipo_entidad)
values ('/MisInvitaciones*','MisInvitaciones','Mis Invitaciones',1); */

/**
 * Items de menu para usuario comun
 */
/* insert into item_menu (titulo,accion)
values ('Usuario',(select ID_accion from accion where NOMBRE = 'Mi Usuario')); */
insert into item_menu (titulo,accion)
values ('Jugadores',(select ID_accion from accion where NOMBRE = 'Mi Jugador'));
insert into item_menu (titulo,accion)
values ('Equipos',(select ID_accion from accion where NOMBRE = 'Mis Equipos'));
insert into item_menu (titulo,accion)
values ('Torneos',(select ID_accion from accion where NOMBRE = 'Mis Torneos'));
/*insert into item_menu (titulo,accion)
 values ('Grupos',(select ID_accion from accion where NOMBRE = 'Mis Grupos'));
insert into item_menu (titulo,accion)
values ('Partidos',(select ID_accion from accion where NOMBRE = 'Mis Partidos'));
insert into item_menu (titulo,accion)
values ('Invitaciones',(select ID_accion from accion where NOMBRE = 'Mis Invitaciones')); */

/*
 * perfil de usuario comun
 */

insert into perfil (ACTIVO,NOMBRE)
values (true,'PUSER');

/**
 * acciones asignadas al perfil de usuario comun
 */
/* insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'Mi Usuario')); */
/* insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'Mi Jugador'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'Mis Equipos'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'Mis Torneos')); */
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'Jugadores'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'Equipos'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'Torneos'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'ProgramacionPartidos'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'Sedes'));
/*insert into perfil_accion (ID_perfil,ID_accion)
 values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'Mis Grupos'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'Mis Partidos'));
insert into perfil_accion (ID_perfil,ID_accion)
values ((select ID_perfil from perfil where NOMBRE = 'PUSER'),(select ID_accion from accion where NOMBRE = 'Mis Invitaciones')); */


INSERT INTO usuario
(`ACTIVO`,`CLAVE`,`NOMBRE`)
VALUES(true,'0cc175b9c0f1b6a831c399e269772661','puser@gmail.com');
/**
 * Todos los usuarios son PUSER salvo el admin
*/
insert into perfil_usuario (ID_perfil,ID_USUARIO)
select P.ID_perfil, U.ID_USUARIO from perfil P, usuario U 
where P.NOMBRE = 'PUSER' AND U.NOMBRE <> 'nachomb@gmail.com'; 

/**
 * Tipos de sanciones
 */
INSERT INTO tipo_sancion VALUES(1,2,0,'Doble amarilla',1);
INSERT INTO tipo_sancion VALUES(2,1,1,'Roja por segunda amarilla',1);
INSERT INTO tipo_sancion VALUES(3,2,1,'Roja por segunda amarilla (mal especificado)',1);
INSERT INTO tipo_sancion VALUES(4,0,1,'Roja directa',1);
