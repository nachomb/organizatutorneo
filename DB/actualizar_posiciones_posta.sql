-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 30-12-2013 a las 13:36:10
-- Versión del servidor: 5.1.41
-- Versión de PHP: 5.3.2-1ubuntu4.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=root@localhost PROCEDURE actualizar_goleadores(IN id_torneo BIGINT(20))
BEGIN

delete from posicion_goleador
where torneo_fk = id_torneo;

insert into posicion_goleador(goles,jugador_fk,torneo_fk,equipo_fk)
select count(*) as cantGoles,g.jugador_fk,id_torneo,g.equipo_fk
from gol g
inner join jugador j on j.id_jugador = g.jugador_fk
inner join partido p on p.id_partido = g.partido_fk
inner join fecha f on f.id_fecha = p.id_fecha
inner join torneo t on t.id_torneo = f.torneo_fk
where j.activo = true
and t.id_torneo = id_torneo
group by g.jugador_fk,id_torneo,g.equipo_fk
order by cantGoles desc;


update posicion_goleador pg,
(
select sum(goles) as cantGoles,pg2.jugador_fk as jugId
from posicion_goleador pg2
where pg2.torneo_fk in (select torneo_anterior from torneo_anterior where torneo = id_torneo)
group by jugId
) tornAnt
set goles = goles + tornAnt.cantGoles
where pg.jugador_fk = tornAnt.jugId and pg.torneo_fk = id_torneo;

insert ignore into posicion_goleador(goles,jugador_fk,torneo_fk,equipo_fk)
select sum(goles) as cantGoles,jugador_fk,id_torneo,pg2.equipo_fk
from posicion_goleador pg2
inner join jugador j on j.id_jugador = pg2.jugador_fk
where pg2.torneo_fk in (select torneo_anterior from torneo_anterior where torneo = id_torneo)
and pg2.equipo_fk in (select equipo_fk from torneo_equipo where torneo_fk = id_torneo)
group by jugador_fk,id_torneo,j.equipo_fk;


END$$

CREATE DEFINER=root@localhost PROCEDURE actualizar_posiciones(IN id_torneo BIGINT(20))
BEGIN
  declare cant_puntos_ganador int;
  declare cant_puntos_empate int;
  declare cant_puntos_perdedor int;

  select cantPuntosGanador from torneo t where t.id_torneo = id_torneo into cant_puntos_ganador;
  select cantPuntosEmpate from torneo t where t.id_torneo = id_torneo into cant_puntos_empate;
  select cantPuntosPerdedor from torneo t where t.id_torneo = id_torneo into cant_puntos_perdedor;

update posicion_torneo pos_tor
set pos_tor.puntos = 0,
pos_tor.partidosGanados = 0,
pos_tor.partidosEmpatados = 0,
pos_tor.partidosPerdidos = 0,
pos_tor.partidosJugados = 0,
pos_tor.golesAFavor = 0,
pos_tor.golesEnContra = 0
where pos_tor.torneo_fk = id_torneo;

update posicion_torneo pos_tor,
(SELECT equipo_local_fk as equipo,
SUM(CASE WHEN golesLocal > golesVisitante THEN cant_puntos_ganador when golesLocal = golesVisitante then cant_puntos_empate ELSE cant_puntos_perdedor END) as puntos,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as perdidos,
COUNT(*) as jugados,
SUM(golesLocal) as golesFavor,
SUM(golesVisitante) as golesContra
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = id_torneo and jugado = true and p.activo = true
GROUP BY equipo_local_fk) ploc
set pos_tor.puntos = ploc.puntos,
pos_tor.partidosGanados = ploc.ganados,
pos_tor.partidosEmpatados = ploc.empatados,
pos_tor.partidosPerdidos = ploc.perdidos,
pos_tor.partidosJugados = jugados,
pos_tor.golesAFavor = golesFavor,
pos_tor.golesEnContra = golesContra
where pos_tor.equipo_fk = ploc.equipo and pos_tor.torneo_fk = id_torneo;

update posicion_torneo pos_tor,
(SELECT equipo_visitante_fk as equipo,
SUM(CASE WHEN golesLocal < golesVisitante THEN cant_puntos_ganador when golesLocal = golesVisitante then cant_puntos_empate ELSE cant_puntos_perdedor END) as puntos,
SUM(CASE WHEN golesLocal < golesVisitante THEN 1 ELSE 0 END) as ganados,
SUM(CASE WHEN golesLocal = golesVisitante THEN 1 ELSE 0 END) as empatados,
SUM(CASE WHEN golesLocal > golesVisitante THEN 1 ELSE 0 END) as perdidos,
COUNT(*) as jugados,
SUM(golesVisitante) as golesFavor,
SUM(golesLocal) as golesContra
FROM partido p
INNER JOIN fecha f on f.id_fecha = p.id_fecha
WHERE f.torneo_fk = id_torneo and jugado = true and p.activo = true
GROUP BY equipo_visitante_fk) pvis
set pos_tor.puntos = pos_tor.puntos + pvis.puntos,
pos_tor.partidosGanados = pos_tor.partidosGanados + pvis.ganados,
pos_tor.partidosEmpatados = pos_tor.partidosEmpatados + pvis.empatados,
pos_tor.partidosPerdidos = pos_tor.partidosPerdidos + pvis.perdidos,
pos_tor.partidosJugados = pos_tor.partidosJugados + jugados,
pos_tor.golesAFavor = pos_tor.golesAFavor + golesFavor,
pos_tor.golesEnContra = pos_tor.golesEnContra + golesContra
where pos_tor.equipo_fk = pvis.equipo and pos_tor.torneo_fk = id_torneo;



END$$

CREATE DEFINER=root@localhost PROCEDURE actualizar_tarjetas(IN id_torneo BIGINT(20))
BEGIN

delete from cantidad_tarjetas
where torneo_fk = id_torneo;

/*inserto los jugadores con tarjetas amarillas*/
insert into cantidad_tarjetas(tarjetas_amarillas,jugador_fk, torneo_fk, equipo_fk)
  select count(*) as cantAmarillas, jugador_fk, id_torneo, t.equipo_fk
  FROM tarjeta t
    inner join jugador j on j.id_jugador = t.jugador_fk
    inner join partido p on p.id_partido = t.partido_fk
    inner join fecha f on f.id_fecha = p.id_fecha
  where f.torneo_fk = id_torneo
  and TIPO = 'AMARILLA'
  GROUP BY t.`JUGADOR_FK`,id_torneo,t.equipo_fk
  order by cantAmarillas desc;

/*actualizo los jugadores que tienen tarjetas roja*/
update cantidad_tarjetas ct,
  (select count(*) as cant,j.id_jugador as id_jugador from tarjeta t
  inner join jugador j on j.id_jugador = t.jugador_fk
    inner join partido p on p.id_partido = t.partido_fk
    inner join fecha f on f.id_fecha = p.id_fecha
  where f.torneo_fk = id_torneo
  and TIPO = 'ROJA'
  GROUP BY id_jugador
  ) tarjetasRojas
set ct.tarjetas_rojas = tarjetasRojas.cant
where ct.jugador_fk = tarjetasRojas.id_jugador and torneo_fk = id_torneo;

/*inserto los jugadores con tarjetas rojas SOLAMENTE*/
insert ignore into cantidad_tarjetas(tarjetas_rojas,jugador_fk,torneo_fk, equipo_fk)
  select count(*) as cantRojas,jugador_fk, id_torneo, t.equipo_fk
  from tarjeta t
    inner join jugador j on j.id_jugador = t.jugador_fk
    inner join partido p on p.id_partido = t.partido_fk
    inner join fecha f on f.id_fecha = p.id_fecha
  where f.torneo_fk = id_torneo
  and TIPO = 'ROJA'
  GROUP BY jugador_fk,id_torneo,t.equipo_fk
order by cantRojas desc;

/*actualizo las tarjetas amarillas y rojas si este torneo depende de torneos anteriores*/
update cantidad_tarjetas ct,
(
  select sum(tarjetas_amarillas) as cantAmarillas, sum(tarjetas_rojas) as cantRojas, ct2.jugador_fk as jugId
  from cantidad_tarjetas ct2
  where ct2.torneo_fk in (select torneo_anterior from torneo_anterior where torneo = id_torneo)
  group by jugId
) tornAnt
set tarjetas_amarillas = tarjetas_amarillas + tornAnt.cantAmarillas,
tarjetas_rojas = tarjetas_rojas + tornAnt.cantRojas
where ct.jugador_fk = tornAnt.jugId and ct.torneo_fk = id_torneo;

/*inserto las tarjetas de torneos anteriores*/
insert ignore into cantidad_tarjetas(tarjetas_amarillas,tarjetas_rojas,jugador_fk,torneo_fk, equipo_fk)
  select sum(tarjetas_amarillas) as cantAmarillas, sum(tarjetas_rojas) as cantRojas, jugador_fk, id_torneo, ct2.equipo_fk
  from cantidad_tarjetas ct2
  inner join jugador j on j.id_jugador = ct2.jugador_fk
  where ct2.torneo_fk in (select torneo_anterior from torneo_anterior where torneo = id_torneo)
  and j.equipo_fk in (select equipo_fk from torneo_equipo where torneo_fk = id_torneo)
  group by jugador_fk, id_torneo, ct2.equipo_fk;


END$$

CREATE DEFINER=root@localhost PROCEDURE borrar_torneo(IN idTorneo BIGINT(20))
BEGIN

delete from partido where id_fecha in(
select id_fecha from fecha where torneo_fk = idTorneo
);

delete from fecha where torneo_fk = idTorneo;

delete from torneo_equipo where torneo_fk = idTorneo;

delete from posicion_torneo where torneo_fk = idTorneo;

delete from torneo where id_torneo = idTorneo;

END$$

DELIMITER ;
