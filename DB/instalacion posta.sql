CREATE DATABASE  IF NOT EXISTS `picadito` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `picadito`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: picadito
-- ------------------------------------------------------
-- Server version	5.5.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `perfil_usuario`
--

DROP TABLE IF EXISTS `perfil_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil_usuario` (
  `ID_PERFIL` bigint(20) NOT NULL,
  `ID_USUARIO` bigint(20) NOT NULL,
  KEY `FK68C1F79B4F2C85F3` (`ID_PERFIL`),
  KEY `FK68C1F79BD77A7F61` (`ID_USUARIO`),
  CONSTRAINT `FK68C1F79B4F2C85F3` FOREIGN KEY (`ID_PERFIL`) REFERENCES `perfil` (`ID_PERFIL`),
  CONSTRAINT `FK68C1F79BD77A7F61` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil_usuario`
--

LOCK TABLES `perfil_usuario` WRITE;
/*!40000 ALTER TABLE `perfil_usuario` DISABLE KEYS */;
INSERT INTO `perfil_usuario` VALUES (57,1),(58,2),(58,3),(58,4),(58,5);
/*!40000 ALTER TABLE `perfil_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `torneo_equipo`
--

DROP TABLE IF EXISTS `torneo_equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `torneo_equipo` (
  `TORNEO_FK` bigint(20) NOT NULL,
  `EQUIPO_FK` bigint(20) NOT NULL,
  KEY `FKE32C375D5DB4EB3D` (`TORNEO_FK`),
  KEY `FKE32C375DFABF067D` (`EQUIPO_FK`),
  CONSTRAINT `FKE32C375D5DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`),
  CONSTRAINT `FKE32C375DFABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `torneo_equipo`
--

LOCK TABLES `torneo_equipo` WRITE;
/*!40000 ALTER TABLE `torneo_equipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `torneo_equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagen`
--

DROP TABLE IF EXISTS `imagen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagen` (
  `ID_IMAGEN` bigint(20) NOT NULL AUTO_INCREMENT,
  `imgSerialiazda` blob,
  PRIMARY KEY (`ID_IMAGEN`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagen`
--

LOCK TABLES `imagen` WRITE;
/*!40000 ALTER TABLE `imagen` DISABLE KEYS */;
/*!40000 ALTER TABLE `imagen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicidad`
--

DROP TABLE IF EXISTS `publicidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publicidad` (
  `ID_PUBLICIDAD` bigint(20) NOT NULL AUTO_INCREMENT,
  `imagenSlider` varchar(255) DEFAULT NULL,
  `imagenVertical` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `activoSlider` bit(1) DEFAULT NULL,
  `activoVertical` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ID_PUBLICIDAD`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicidad`
--

LOCK TABLES `publicidad` WRITE;
/*!40000 ALTER TABLE `publicidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `publicidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `torneo`
--

DROP TABLE IF EXISTS `torneo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `torneo` (
  `ID_TORNEO` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `cantidadFechas` int(11) DEFAULT NULL,
  `fechaActual` int(11) DEFAULT NULL,
  `idaYVuelta` bit(1) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `tipoTorneo` varchar(255) DEFAULT NULL,
  `jugadoresPorEquipo` int(11) DEFAULT NULL,
  `cantPuntosEmpate` int(11) DEFAULT NULL,
  `cantPuntosGanador` int(11) DEFAULT NULL,
  `cantPuntosPerdedor` int(11) DEFAULT NULL,
  `cantFechasSancionAmarilla` int(11) DEFAULT NULL,
  `cantFechasSancionRoja` int(11) DEFAULT NULL,
  `cantTarjAmarillasParaSancion` int(11) DEFAULT NULL,
  `cantTarjRojasParaSancion` int(11) DEFAULT NULL,
  `USUARIO_CREADOR_FK` bigint(20) DEFAULT NULL,
  `rutaImagen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_TORNEO`),
  KEY `FK93D6C8E1A313C4D0` (`USUARIO_CREADOR_FK`),
  CONSTRAINT `FK93D6C8E1A313C4D0` FOREIGN KEY (`USUARIO_CREADOR_FK`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `torneo`
--

LOCK TABLES `torneo` WRITE;
/*!40000 ALTER TABLE `torneo` DISABLE KEYS */;
/*!40000 ALTER TABLE `torneo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alineacion_amistoso_titulares`
--

DROP TABLE IF EXISTS `alineacion_amistoso_titulares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alineacion_amistoso_titulares` (
  `ID_ALINEACION_AMISTOSO` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) DEFAULT NULL,
  `NOMBRE_JUGADOR` varchar(255) DEFAULT NULL,
  KEY `FKB80031B1FD04914E` (`ID_ALINEACION_AMISTOSO`),
  KEY `FKB80031B14FC1E07` (`ID_JUGADOR`),
  CONSTRAINT `FKB80031B14FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FKB80031B1FD04914E` FOREIGN KEY (`ID_ALINEACION_AMISTOSO`) REFERENCES `alineacion_amistoso` (`ID_ALINEACION_AMISTOSO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alineacion_amistoso_titulares`
--

LOCK TABLES `alineacion_amistoso_titulares` WRITE;
/*!40000 ALTER TABLE `alineacion_amistoso_titulares` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_amistoso_titulares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posicion_goleador`
--

DROP TABLE IF EXISTS `posicion_goleador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posicion_goleador` (
  `ID_POSICIONGOLEADOR` bigint(20) NOT NULL AUTO_INCREMENT,
  `GOLES` int(11) DEFAULT NULL,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `TORNEO_FK` bigint(20) DEFAULT NULL,
  `EQUIPO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_POSICIONGOLEADOR`),
  UNIQUE KEY `JUGADOR_FK` (`JUGADOR_FK`,`TORNEO_FK`),
  KEY `FK84336CEC18383FCB` (`JUGADOR_FK`),
  KEY `FK84336CEC5DB4EB3D` (`TORNEO_FK`),
  KEY `FK84336CECFABF067D` (`EQUIPO_FK`),
  CONSTRAINT `FK84336CEC18383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK84336CEC5DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`),
  CONSTRAINT `FK84336CECFABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=13157 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posicion_goleador`
--

LOCK TABLES `posicion_goleador` WRITE;
/*!40000 ALTER TABLE `posicion_goleador` DISABLE KEYS */;
/*!40000 ALTER TABLE `posicion_goleador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarjeta`
--

DROP TABLE IF EXISTS `tarjeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarjeta` (
  `DTYPE` varchar(31) NOT NULL,
  `ID_TARJETA` bigint(20) NOT NULL AUTO_INCREMENT,
  `TIPO` varchar(255) DEFAULT NULL,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_FK` bigint(20) DEFAULT NULL,
  `EQUIPO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_TARJETA`),
  KEY `FKCF1CA9EDC42F976B` (`PARTIDO_FK`),
  KEY `FKCF1CA9ED18383FCB` (`JUGADOR_FK`),
  KEY `FKA4511E0DFABF067D` (`EQUIPO_FK`),
  CONSTRAINT `FKA4511E0DFABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`),
  CONSTRAINT `FKCF1CA9ED18383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FKCF1CA9EDC42F976B` FOREIGN KEY (`PARTIDO_FK`) REFERENCES `partido` (`ID_PARTIDO`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarjeta`
--

LOCK TABLES `tarjeta` WRITE;
/*!40000 ALTER TABLE `tarjeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarjeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `torneo_anterior`
--

DROP TABLE IF EXISTS `torneo_anterior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `torneo_anterior` (
  `torneo` bigint(20) DEFAULT NULL,
  `torneo_anterior` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `torneo_anterior`
--

LOCK TABLES `torneo_anterior` WRITE;
/*!40000 ALTER TABLE `torneo_anterior` DISABLE KEYS */;
/*!40000 ALTER TABLE `torneo_anterior` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cantidad_tarjetas`
--

DROP TABLE IF EXISTS `cantidad_tarjetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cantidad_tarjetas` (
  `ID_CANTIDADTARJETAS` bigint(20) NOT NULL AUTO_INCREMENT,
  `TARJETAS_AMARILLAS` int(11) DEFAULT '0',
  `TARJETAS_ROJAS` int(11) DEFAULT '0',
  `JUGADOR_FK` bigint(20) NOT NULL DEFAULT '0',
  `TORNEO_FK` bigint(20) NOT NULL DEFAULT '0',
  `EQUIPO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_CANTIDADTARJETAS`) USING BTREE,
  UNIQUE KEY `JUGADOR_TORNEO` (`JUGADOR_FK`,`TORNEO_FK`),
  KEY `FK366EF48318383FCB` (`JUGADOR_FK`),
  KEY `FK366EF4835DB4EB3D` (`TORNEO_FK`),
  KEY `FK366EF483FABF067D` (`EQUIPO_FK`),
  CONSTRAINT `FK366EF48318383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK366EF4835DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`),
  CONSTRAINT `FK366EF483FABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=2785 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cantidad_tarjetas`
--

LOCK TABLES `cantidad_tarjetas` WRITE;
/*!40000 ALTER TABLE `cantidad_tarjetas` DISABLE KEYS */;
/*!40000 ALTER TABLE `cantidad_tarjetas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gol`
--

DROP TABLE IF EXISTS `gol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gol` (
  `ID_GOL` bigint(20) NOT NULL AUTO_INCREMENT,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_FK` bigint(20) DEFAULT NULL,
  `EQUIPO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_GOL`),
  KEY `FK11464C42F976B` (`PARTIDO_FK`),
  KEY `FK1146418383FCB` (`JUGADOR_FK`),
  KEY `FK19084FABF067D` (`EQUIPO_FK`),
  CONSTRAINT `FK1146418383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK11464C42F976B` FOREIGN KEY (`PARTIDO_FK`) REFERENCES `partido` (`ID_PARTIDO`),
  CONSTRAINT `FK19084FABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=3203 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gol`
--

LOCK TABLES `gol` WRITE;
/*!40000 ALTER TABLE `gol` DISABLE KEYS */;
/*!40000 ALTER TABLE `gol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fecha`
--

DROP TABLE IF EXISTS `fecha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fecha` (
  `ID_FECHA` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `idaYVuelta` bit(1) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `TORNEO_FK` bigint(20) DEFAULT NULL,
  `isVuelta` bit(1) DEFAULT NULL,
  `EQUIPO_LIBRE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_FECHA`),
  KEY `FK3FACF5D5DB4EB3D` (`TORNEO_FK`),
  KEY `FK3FACF5DFCD88930` (`EQUIPO_LIBRE`),
  CONSTRAINT `FK3FACF5D5DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`),
  CONSTRAINT `FK3FACF5DFCD88930` FOREIGN KEY (`EQUIPO_LIBRE`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=583 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fecha`
--

LOCK TABLES `fecha` WRITE;
/*!40000 ALTER TABLE `fecha` DISABLE KEYS */;
/*!40000 ALTER TABLE `fecha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jugador_temp`
--

DROP TABLE IF EXISTS `jugador_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jugador_temp` (
  `equipo` char(100) DEFAULT NULL,
  `apellido` char(100) DEFAULT NULL,
  `nombre` char(100) DEFAULT NULL,
  `mail` char(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jugador_temp`
--

LOCK TABLES `jugador_temp` WRITE;
/*!40000 ALTER TABLE `jugador_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `jugador_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alineacion_titulares`
--

DROP TABLE IF EXISTS `alineacion_titulares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alineacion_titulares` (
  `ID_ALINEACION` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) NOT NULL,
  KEY `FKD8C7CC277DEBF27F` (`ID_ALINEACION`),
  KEY `FKD8C7CC274FC1E07` (`ID_JUGADOR`),
  CONSTRAINT `FKD8C7CC274FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FKD8C7CC277DEBF27F` FOREIGN KEY (`ID_ALINEACION`) REFERENCES `alineacion` (`ID_ALINEACION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alineacion_titulares`
--

LOCK TABLES `alineacion_titulares` WRITE;
/*!40000 ALTER TABLE `alineacion_titulares` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_titulares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil` (
  `ID_PERFIL` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVO` bit(1) DEFAULT NULL,
  `FECHA_CREACION` datetime DEFAULT NULL,
  `FECHA_ULTIMA_MODIFICACION` datetime DEFAULT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `USUARIO_CREACION` bigint(20) DEFAULT NULL,
  `USUARIO_ULTIMA_MODIFICACION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_PERFIL`),
  KEY `FK8C765DCC21D8C9D8` (`USUARIO_CREACION`),
  KEY `FK8C765DCCF5191012` (`USUARIO_ULTIMA_MODIFICACION`),
  CONSTRAINT `FK8C765DCC21D8C9D8` FOREIGN KEY (`USUARIO_CREACION`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FK8C765DCCF5191012` FOREIGN KEY (`USUARIO_ULTIMA_MODIFICACION`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil`
--

LOCK TABLES `perfil` WRITE;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` VALUES (57,'',NULL,NULL,NULL,'PADMIN',NULL,NULL),(58,'',NULL,NULL,NULL,'PUSER',NULL,NULL);
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alineacion_amistoso`
--

DROP TABLE IF EXISTS `alineacion_amistoso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alineacion_amistoso` (
  `ID_ALINEACION_AMISTOSO` bigint(20) NOT NULL AUTO_INCREMENT,
  `PARTIDO_AMISTOSO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ALINEACION_AMISTOSO`),
  KEY `FKAEEA5BFBEF4CB320` (`PARTIDO_AMISTOSO`),
  CONSTRAINT `FKAEEA5BFBEF4CB320` FOREIGN KEY (`PARTIDO_AMISTOSO`) REFERENCES `partido_amistoso` (`ID_PARTIDO_AMISTOSO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alineacion_amistoso`
--

LOCK TABLES `alineacion_amistoso` WRITE;
/*!40000 ALTER TABLE `alineacion_amistoso` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_amistoso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `puntaje_partido`
--

DROP TABLE IF EXISTS `puntaje_partido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `puntaje_partido` (
  `ID_PUNTAJEPARTIDO` bigint(20) NOT NULL AUTO_INCREMENT,
  `puntaje` int(11) DEFAULT NULL,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_FK` bigint(20) DEFAULT NULL,
  `figura` bit(1) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_PUNTAJEPARTIDO`),
  KEY `FK2F416253C42F976B` (`PARTIDO_FK`),
  KEY `FK2F41625318383FCB` (`JUGADOR_FK`),
  KEY `FKF0842A93C42F976B` (`PARTIDO_FK`),
  CONSTRAINT `FK2F41625318383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FKF0842A93C42F976B` FOREIGN KEY (`PARTIDO_FK`) REFERENCES `partido` (`ID_PARTIDO`)
) ENGINE=InnoDB AUTO_INCREMENT=5625 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `puntaje_partido`
--

LOCK TABLES `puntaje_partido` WRITE;
/*!40000 ALTER TABLE `puntaje_partido` DISABLE KEYS */;
/*!40000 ALTER TABLE `puntaje_partido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jugador`
--

DROP TABLE IF EXISTS `jugador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jugador` (
  `ID_JUGADOR` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `apodo` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `EQUIPO_FK` bigint(20) DEFAULT NULL,
  `USUARIO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_JUGADOR`),
  UNIQUE KEY `nombre` (`nombre`,`apellido`,`EQUIPO_FK`),
  KEY `FKDFA027A2FABF067D` (`EQUIPO_FK`),
  KEY `FKDFA027A2C3F3AACD` (`USUARIO_FK`),
  CONSTRAINT `FKDFA027A2C3F3AACD` FOREIGN KEY (`USUARIO_FK`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FKDFA027A2FABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=1314 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jugador`
--

LOCK TABLES `jugador` WRITE;
/*!40000 ALTER TABLE `jugador` DISABLE KEYS */;
INSERT INTO `jugador` VALUES (1313,'','Alesso',NULL,'Florencia',88,NULL);
/*!40000 ALTER TABLE `jugador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo_jugador`
--

DROP TABLE IF EXISTS `grupo_jugador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo_jugador` (
  `ID_GRUPO` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) NOT NULL,
  KEY `FK916E89AC4FC1E07` (`ID_JUGADOR`),
  KEY `FK916E89AC9892C415` (`ID_GRUPO`),
  CONSTRAINT `FK916E89AC4FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK916E89AC9892C415` FOREIGN KEY (`ID_GRUPO`) REFERENCES `grupo` (`ID_GRUPO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo_jugador`
--

LOCK TABLES `grupo_jugador` WRITE;
/*!40000 ALTER TABLE `grupo_jugador` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo_jugador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alineacion_suplentes`
--

DROP TABLE IF EXISTS `alineacion_suplentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alineacion_suplentes` (
  `ID_ALINEACION` bigint(20) NOT NULL,
  `ID_JUGADOR` bigint(20) NOT NULL,
  KEY `FK3FC038CD7DEBF27F` (`ID_ALINEACION`),
  KEY `FK3FC038CD4FC1E07` (`ID_JUGADOR`),
  CONSTRAINT `FK3FC038CD4FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK3FC038CD7DEBF27F` FOREIGN KEY (`ID_ALINEACION`) REFERENCES `alineacion` (`ID_ALINEACION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alineacion_suplentes`
--

LOCK TABLES `alineacion_suplentes` WRITE;
/*!40000 ALTER TABLE `alineacion_suplentes` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_suplentes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dummy`
--

DROP TABLE IF EXISTS `dummy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dummy` (
  `ID_DUMMY` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID_DUMMY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dummy`
--

LOCK TABLES `dummy` WRITE;
/*!40000 ALTER TABLE `dummy` DISABLE KEYS */;
/*!40000 ALTER TABLE `dummy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partido`
--

DROP TABLE IF EXISTS `partido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partido` (
  `ID_PARTIDO` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `golesLocal` int(11) DEFAULT NULL,
  `golesVisitante` int(11) DEFAULT NULL,
  `jugado` bit(1) DEFAULT NULL,
  `EQUIPO_LOCAL_FK` bigint(20) DEFAULT NULL,
  `EQUIPO_VISITANTE_FK` bigint(20) DEFAULT NULL,
  `fecha_ID_FECHA` bigint(20) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `fechaPartido` datetime DEFAULT NULL,
  `ALINEACION_LOCAL` bigint(20) DEFAULT NULL,
  `ALINEACION_VISITANTE` bigint(20) DEFAULT NULL,
  `ID_FECHA` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_PARTIDO`),
  KEY `FKFB8855C1BC4EC0BF` (`fecha_ID_FECHA`),
  KEY `FKFB8855C1D1BD3B53` (`EQUIPO_VISITANTE_FK`),
  KEY `FKFB8855C191634FD1` (`EQUIPO_LOCAL_FK`),
  KEY `FKFB8855C1B2E6CD45` (`ALINEACION_VISITANTE`),
  KEY `FKFB8855C1EE33A287` (`ALINEACION_LOCAL`),
  KEY `FKFB8855C1986A3A3D` (`ID_FECHA`),
  KEY `FKD0BCC9E1B2E6CD45` (`ALINEACION_VISITANTE`),
  KEY `FKD0BCC9E1D1BD3B53` (`EQUIPO_VISITANTE_FK`),
  KEY `FKD0BCC9E1986A3A3D` (`ID_FECHA`),
  KEY `FKD0BCC9E191634FD1` (`EQUIPO_LOCAL_FK`),
  KEY `FKD0BCC9E1EE33A287` (`ALINEACION_LOCAL`),
  CONSTRAINT `FKD0BCC9E191634FD1` FOREIGN KEY (`EQUIPO_LOCAL_FK`) REFERENCES `equipo` (`ID_EQUIPO`),
  CONSTRAINT `FKD0BCC9E1986A3A3D` FOREIGN KEY (`ID_FECHA`) REFERENCES `fecha` (`ID_FECHA`),
  CONSTRAINT `FKD0BCC9E1B2E6CD45` FOREIGN KEY (`ALINEACION_VISITANTE`) REFERENCES `alineacion` (`ID_ALINEACION`),
  CONSTRAINT `FKD0BCC9E1D1BD3B53` FOREIGN KEY (`EQUIPO_VISITANTE_FK`) REFERENCES `equipo` (`ID_EQUIPO`),
  CONSTRAINT `FKD0BCC9E1EE33A287` FOREIGN KEY (`ALINEACION_LOCAL`) REFERENCES `alineacion` (`ID_ALINEACION`)
) ENGINE=InnoDB AUTO_INCREMENT=2367 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partido`
--

LOCK TABLES `partido` WRITE;
/*!40000 ALTER TABLE `partido` DISABLE KEYS */;
/*!40000 ALTER TABLE `partido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumen`
--

DROP TABLE IF EXISTS `resumen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumen` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activo` bit(1) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `tapa` varchar(255) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumen`
--

LOCK TABLES `resumen` WRITE;
/*!40000 ALTER TABLE `resumen` DISABLE KEYS */;
/*!40000 ALTER TABLE `resumen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invitacion`
--

DROP TABLE IF EXISTS `invitacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invitacion` (
  `ID_INVITACION` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado` varchar(255) DEFAULT NULL,
  `mensaje` varchar(255) DEFAULT NULL,
  `ID_JUGADOR` bigint(20) DEFAULT NULL,
  `ID_PARTIDO_AMISTOSO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_INVITACION`),
  KEY `FK6421A94A4FC1E07` (`ID_JUGADOR`),
  KEY `FK6421A94A23481C4` (`ID_PARTIDO_AMISTOSO`),
  CONSTRAINT `FK6421A94A23481C4` FOREIGN KEY (`ID_PARTIDO_AMISTOSO`) REFERENCES `partido_amistoso` (`ID_PARTIDO_AMISTOSO`),
  CONSTRAINT `FK6421A94A4FC1E07` FOREIGN KEY (`ID_JUGADOR`) REFERENCES `jugador` (`ID_JUGADOR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invitacion`
--

LOCK TABLES `invitacion` WRITE;
/*!40000 ALTER TABLE `invitacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `invitacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_entidad`
--

DROP TABLE IF EXISTS `tipo_entidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_entidad` (
  `ID_TIPO_ENTIDAD` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(255) NOT NULL,
  PRIMARY KEY (`ID_TIPO_ENTIDAD`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_entidad`
--

LOCK TABLES `tipo_entidad` WRITE;
/*!40000 ALTER TABLE `tipo_entidad` DISABLE KEYS */;
INSERT INTO `tipo_entidad` VALUES (1,'page');
/*!40000 ALTER TABLE `tipo_entidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sancion_partido`
--

DROP TABLE IF EXISTS `sancion_partido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sancion_partido` (
  `ID_SANCION` bigint(20) NOT NULL,
  `ID_PARTIDO` bigint(20) NOT NULL,
  UNIQUE KEY `ID_PARTIDO` (`ID_PARTIDO`),
  KEY `FKB7B0980779B1AFCD` (`ID_SANCION`),
  CONSTRAINT `FKB7B098073CCC7A45` FOREIGN KEY (`ID_PARTIDO`) REFERENCES `partido` (`ID_PARTIDO`),
  CONSTRAINT `FKB7B0980779B1AFCD` FOREIGN KEY (`ID_SANCION`) REFERENCES `sancion` (`ID_SANCION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sancion_partido`
--

LOCK TABLES `sancion_partido` WRITE;
/*!40000 ALTER TABLE `sancion_partido` DISABLE KEYS */;
/*!40000 ALTER TABLE `sancion_partido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `ID_GRUPO` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `JUGADOR_CREADOR_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_GRUPO`),
  KEY `FK40F144940F0ADCE` (`JUGADOR_CREADOR_FK`),
  CONSTRAINT `FK40F144940F0ADCE` FOREIGN KEY (`JUGADOR_CREADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partido_amistoso`
--

DROP TABLE IF EXISTS `partido_amistoso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partido_amistoso` (
  `ID_PARTIDO_AMISTOSO` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantParticipantes` int(11) DEFAULT NULL,
  `lugar` varchar(255) DEFAULT NULL,
  `mensaje` varchar(255) DEFAULT NULL,
  `modo` varchar(255) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `ID_GRUPO` bigint(20) DEFAULT NULL,
  `USUARIO_CREADOR_FK` bigint(20) DEFAULT NULL,
  `ALINEACION_EQUIPOA` bigint(20) DEFAULT NULL,
  `ALINEACION_EQUIPOB` bigint(20) DEFAULT NULL,
  `GOLES_EQUIPO_A` bigint(20) DEFAULT NULL,
  `GOLES_EQUIPO_B` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_PARTIDO_AMISTOSO`),
  KEY `FKDB81C22B9892C415` (`ID_GRUPO`),
  KEY `FKDB81C22BA313C4D0` (`USUARIO_CREADOR_FK`),
  KEY `FKDB81C22B7C64902B` (`ALINEACION_EQUIPOA`),
  KEY `FKDB81C22B7C64902C` (`ALINEACION_EQUIPOB`),
  CONSTRAINT `FKDB81C22B7C64902B` FOREIGN KEY (`ALINEACION_EQUIPOA`) REFERENCES `alineacion_amistoso` (`ID_ALINEACION_AMISTOSO`),
  CONSTRAINT `FKDB81C22B7C64902C` FOREIGN KEY (`ALINEACION_EQUIPOB`) REFERENCES `alineacion_amistoso` (`ID_ALINEACION_AMISTOSO`),
  CONSTRAINT `FKDB81C22B9892C415` FOREIGN KEY (`ID_GRUPO`) REFERENCES `grupo` (`ID_GRUPO`),
  CONSTRAINT `FKDB81C22BA313C4D0` FOREIGN KEY (`USUARIO_CREADOR_FK`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partido_amistoso`
--

LOCK TABLES `partido_amistoso` WRITE;
/*!40000 ALTER TABLE `partido_amistoso` DISABLE KEYS */;
/*!40000 ALTER TABLE `partido_amistoso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil_accion`
--

DROP TABLE IF EXISTS `perfil_accion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil_accion` (
  `ID_PERFIL` bigint(20) NOT NULL,
  `ID_ACCION` bigint(20) NOT NULL,
  KEY `FK4BB179BA1BB53A29` (`ID_ACCION`),
  KEY `FK4BB179BA4F2C85F3` (`ID_PERFIL`),
  CONSTRAINT `FK4BB179BA1BB53A29` FOREIGN KEY (`ID_ACCION`) REFERENCES `accion` (`ID_ACCION`),
  CONSTRAINT `FK4BB179BA4F2C85F3` FOREIGN KEY (`ID_PERFIL`) REFERENCES `perfil` (`ID_PERFIL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil_accion`
--

LOCK TABLES `perfil_accion` WRITE;
/*!40000 ALTER TABLE `perfil_accion` DISABLE KEYS */;
INSERT INTO `perfil_accion` VALUES (57,309),(57,310),(57,311),(57,312),(57,313),(57,314),(57,315),(57,316),(57,317),(57,318),(57,319),(57,320),(58,321),(58,322),(58,323),(58,324),(58,325),(58,326),(58,327),(58,320);
/*!40000 ALTER TABLE `perfil_accion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_menu`
--

DROP TABLE IF EXISTS `item_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_menu` (
  `ID_ITEM_MENU` bigint(20) NOT NULL AUTO_INCREMENT,
  `FECHA_CREACION` datetime DEFAULT NULL,
  `FECHA_ULTIMA_MODIFICACION` datetime DEFAULT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `ORDEN` bigint(20) DEFAULT NULL,
  `TITULO` varchar(255) DEFAULT NULL,
  `ACCION` bigint(20) DEFAULT NULL,
  `USUARIO_CREACION` bigint(20) DEFAULT NULL,
  `USUARIO_ULTIMA_MODIFICACION` bigint(20) DEFAULT NULL,
  `ITEM_MENU_PADRE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ITEM_MENU`),
  KEY `FK2838470B21D8C9D8` (`USUARIO_CREACION`),
  KEY `FK2838470BF5191012` (`USUARIO_ULTIMA_MODIFICACION`),
  KEY `FK2838470B163E2E85` (`ACCION`),
  KEY `FK2838470B60B8561D` (`ITEM_MENU_PADRE`),
  CONSTRAINT `FK2838470B163E2E85` FOREIGN KEY (`ACCION`) REFERENCES `accion` (`ID_ACCION`),
  CONSTRAINT `FK2838470B21D8C9D8` FOREIGN KEY (`USUARIO_CREACION`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FK2838470B60B8561D` FOREIGN KEY (`ITEM_MENU_PADRE`) REFERENCES `item_menu` (`ID_ITEM_MENU`),
  CONSTRAINT `FK2838470BF5191012` FOREIGN KEY (`USUARIO_ULTIMA_MODIFICACION`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=300 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_menu`
--

LOCK TABLES `item_menu` WRITE;
/*!40000 ALTER TABLE `item_menu` DISABLE KEYS */;
INSERT INTO `item_menu` VALUES (283,NULL,NULL,NULL,NULL,'Usuarios',309,NULL,NULL,NULL),(284,NULL,NULL,NULL,NULL,'Perfiles',310,NULL,NULL,NULL),(285,NULL,NULL,NULL,NULL,'Jugadores',311,NULL,NULL,NULL),(286,NULL,NULL,NULL,NULL,'Equipos',312,NULL,NULL,NULL),(287,NULL,NULL,NULL,NULL,'Torneos',313,NULL,NULL,NULL),(288,NULL,NULL,NULL,NULL,'Publicidad',314,NULL,NULL,NULL),(289,NULL,NULL,NULL,NULL,'Horarios',315,NULL,NULL,NULL),(290,NULL,NULL,NULL,NULL,'Resumenes',319,NULL,NULL,NULL),(291,NULL,NULL,NULL,NULL,'Grupos',316,NULL,NULL,NULL),(292,NULL,NULL,NULL,NULL,'Partidos',317,NULL,NULL,NULL),(293,NULL,NULL,NULL,NULL,'Usuario',321,NULL,NULL,NULL),(294,NULL,NULL,NULL,NULL,'Jugador',322,NULL,NULL,NULL),(295,NULL,NULL,NULL,NULL,'Equipos',323,NULL,NULL,NULL),(296,NULL,NULL,NULL,NULL,'Torneos',324,NULL,NULL,NULL),(297,NULL,NULL,NULL,NULL,'Grupos',325,NULL,NULL,NULL),(298,NULL,NULL,NULL,NULL,'Partidos',326,NULL,NULL,NULL),(299,NULL,NULL,NULL,NULL,'Invitaciones',327,NULL,NULL,NULL);
/*!40000 ALTER TABLE `item_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accion`
--

DROP TABLE IF EXISTS `accion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accion` (
  `ID_ACCION` bigint(20) NOT NULL AUTO_INCREMENT,
  `ENTIDAD` varchar(255) NOT NULL,
  `MENU_ENTRADA` varchar(255) NOT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `TIPO_ENTIDAD` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ACCION`),
  KEY `FK72BAB7E7FE022F9C` (`TIPO_ENTIDAD`),
  CONSTRAINT `FK72BAB7E7FE022F9C` FOREIGN KEY (`TIPO_ENTIDAD`) REFERENCES `tipo_entidad` (`ID_TIPO_ENTIDAD`)
) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accion`
--

LOCK TABLES `accion` WRITE;
/*!40000 ALTER TABLE `accion` DISABLE KEYS */;
INSERT INTO `accion` VALUES (309,'/Usuario*','Usuario_list','Usuarios',1),(310,'/Perfil*','Perfil_list','Perfiles',1),(311,'/Jugador*','Jugador_list','Jugadores',1),(312,'/Equipo*','Equipo_list','Equipos',1),(313,'/Torneo*','Torneo_list','Torneos',1),(314,'/Publicidad*','Publicidad_list','Publicidad',1),(315,'/ProgramacionPartidos*','ProgramacionPartidos','ProgramacionPartidos',1),(316,'/Grupo*','Grupo_list','Grupos',1),(317,'/PartidoAmistoso*','PartidoAmistoso_list','PartidoAmistoso',1),(318,'/Exporter*','Exporter_estadisticas','Exporter',1),(319,'/Resumen*','Resumen_list','Resumen',1),(320,'/MostrarJugadores*','MostrarJugadores','MostrarJugadores',1),(321,'/MiUsuario*','MiUsuario_miUsuarioListado','Mi Usuario',1),(322,'/MiJugador*','MiJugador_miJugadorListado','Mi Jugador',1),(323,'/MisEquipos*','MisEquipos','Mis Equipos',1),(324,'/MisTorneos*','MisTorneos','Mis Torneos',1),(325,'/MisGrupos*','MisGrupos_listAmistoso','Mis Grupos',1),(326,'/MisPartidos*','MisPartidos_listAmistoso','Mis Partidos',1),(327,'/MisInvitaciones*','MisInvitaciones','Mis Invitaciones',1);
/*!40000 ALTER TABLE `accion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `ID_USUARIO` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVO` bit(1) DEFAULT NULL,
  `FECHA_CREACION` datetime DEFAULT NULL,
  `FECHA_ULTIMA_MODIFICACION` datetime DEFAULT NULL,
  `CLAVE` varchar(255) NOT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `USUARIO_CREACION` bigint(20) DEFAULT NULL,
  `USUARIO_ULTIMA_MODIFICACION` bigint(20) DEFAULT NULL,
  `TEL_CELULAR` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_USUARIO`),
  KEY `FK22E07F0E21D8C9D8` (`USUARIO_CREACION`),
  KEY `FK22E07F0EF5191012` (`USUARIO_ULTIMA_MODIFICACION`),
  CONSTRAINT `FK22E07F0E21D8C9D8` FOREIGN KEY (`USUARIO_CREACION`) REFERENCES `usuario` (`ID_USUARIO`),
  CONSTRAINT `FK22E07F0EF5191012` FOREIGN KEY (`USUARIO_ULTIMA_MODIFICACION`) REFERENCES `usuario` (`ID_USUARIO`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'',NULL,NULL,'535794bc42fbddc0a47505bee0f9a405','ligafutboleras@gmail.com',NULL,NULL,''),(2,'',NULL,NULL,'eae479109863f5ae51ee7181f25b2650','zona1',NULL,NULL,''),(3,'',NULL,NULL,'0141a9a2363dfe0ec69478b02bd0656d','zona2',NULL,NULL,''),(4,'',NULL,NULL,'a962b6dfa20a044e254bd95fcc63987b','nachomb@gmail.com',NULL,NULL,''),(5,'',NULL,NULL,'14145616539b222a8715417a3c8fcd25','nico',NULL,NULL,'');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_sancion`
--

DROP TABLE IF EXISTS `tipo_sancion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_sancion` (
  `ID_TIPO_SANCION` bigint(20) NOT NULL AUTO_INCREMENT,
  `CANT_AMARILLAS` int(11) DEFAULT NULL,
  `CANT_ROJAS` int(11) DEFAULT NULL,
  `DESCRIPCION` varchar(255) DEFAULT NULL,
  `DURACION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_TIPO_SANCION`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_sancion`
--

LOCK TABLES `tipo_sancion` WRITE;
/*!40000 ALTER TABLE `tipo_sancion` DISABLE KEYS */;
INSERT INTO `tipo_sancion` VALUES (1,2,0,'Doble amarilla',1),(2,1,1,'Roja por segunda amarilla',1),(3,2,1,'Roja por segunda amarilla (mal especificado)',1),(4,0,1,'Roja directa',1);
/*!40000 ALTER TABLE `tipo_sancion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posicion_torneo`
--

DROP TABLE IF EXISTS `posicion_torneo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posicion_torneo` (
  `ID_POSICION` bigint(20) NOT NULL AUTO_INCREMENT,
  `amarillas` int(11) DEFAULT NULL,
  `golesAFavor` int(11) DEFAULT NULL,
  `golesEnContra` int(11) DEFAULT NULL,
  `partidosEmpatados` int(11) DEFAULT NULL,
  `partidosGanados` int(11) DEFAULT NULL,
  `partidosJugados` int(11) DEFAULT NULL,
  `partidosPerdidos` int(11) DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `rojas` int(11) DEFAULT NULL,
  `EQUIPO_FK` bigint(20) DEFAULT NULL,
  `TORNEO_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_POSICION`),
  KEY `FK8BF26DE6FABF067D` (`EQUIPO_FK`),
  KEY `FK8BF26DE65DB4EB3D` (`TORNEO_FK`),
  CONSTRAINT `FK8BF26DE65DB4EB3D` FOREIGN KEY (`TORNEO_FK`) REFERENCES `torneo` (`ID_TORNEO`),
  CONSTRAINT `FK8BF26DE6FABF067D` FOREIGN KEY (`EQUIPO_FK`) REFERENCES `equipo` (`ID_EQUIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=390 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posicion_torneo`
--

LOCK TABLES `posicion_torneo` WRITE;
/*!40000 ALTER TABLE `posicion_torneo` DISABLE KEYS */;
/*!40000 ALTER TABLE `posicion_torneo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipo` (
  `ID_EQUIPO` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVO` bit(1) DEFAULT NULL,
  `NOMBRE` varchar(255) DEFAULT NULL,
  `RUTA_IMG_ESCUDO` varchar(255) DEFAULT NULL,
  `IMAGEN_FK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_EQUIPO`),
  UNIQUE KEY `NOMBRE` (`NOMBRE`),
  KEY `FK7A5B923F8CDC29FD` (`IMAGEN_FK`),
  CONSTRAINT `FK7A5B923F8CDC29FD` FOREIGN KEY (`IMAGEN_FK`) REFERENCES `imagen` (`ID_IMAGEN`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipo`
--

LOCK TABLES `equipo` WRITE;
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
INSERT INTO `equipo` VALUES (88,'','A.C. Ital Club',NULL,NULL);
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alineacion`
--

DROP TABLE IF EXISTS `alineacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alineacion` (
  `ID_ALINEACION` bigint(20) NOT NULL AUTO_INCREMENT,
  `PARTIDO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_ALINEACION`),
  KEY `FK41C471F193621169` (`PARTIDO`),
  KEY `FK24DA85F193621169` (`PARTIDO`),
  CONSTRAINT `FK24DA85F193621169` FOREIGN KEY (`PARTIDO`) REFERENCES `partido` (`ID_PARTIDO`)
) ENGINE=InnoDB AUTO_INCREMENT=827 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alineacion`
--

LOCK TABLES `alineacion` WRITE;
/*!40000 ALTER TABLE `alineacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sancion`
--

DROP TABLE IF EXISTS `sancion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sancion` (
  `ID_SANCION` bigint(20) NOT NULL AUTO_INCREMENT,
  `CUMPLIDA` bit(1) DEFAULT NULL,
  `JUGADOR_FK` bigint(20) DEFAULT NULL,
  `PARTIDO_INICIO_FK` bigint(20) DEFAULT NULL,
  `TIPO_SANCION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_SANCION`),
  KEY `FK99FAF085C127A392` (`TIPO_SANCION`),
  KEY `FK99FAF085C1295389` (`PARTIDO_INICIO_FK`),
  KEY `FK99FAF08518383FCB` (`JUGADOR_FK`),
  KEY `FK6F2F64A5C127A392` (`TIPO_SANCION`),
  KEY `FK6F2F64A5C1295389` (`PARTIDO_INICIO_FK`),
  KEY `FK6F2F64A518383FCB` (`JUGADOR_FK`),
  CONSTRAINT `FK6F2F64A518383FCB` FOREIGN KEY (`JUGADOR_FK`) REFERENCES `jugador` (`ID_JUGADOR`),
  CONSTRAINT `FK6F2F64A5C127A392` FOREIGN KEY (`TIPO_SANCION`) REFERENCES `tipo_sancion` (`ID_TIPO_SANCION`),
  CONSTRAINT `FK6F2F64A5C1295389` FOREIGN KEY (`PARTIDO_INICIO_FK`) REFERENCES `partido` (`ID_PARTIDO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sancion`
--

LOCK TABLES `sancion` WRITE;
/*!40000 ALTER TABLE `sancion` DISABLE KEYS */;
/*!40000 ALTER TABLE `sancion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-25 16:41:51
