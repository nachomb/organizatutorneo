﻿

drop procedure if exists actualizar_goleadores;
DELIMITER //
CREATE PROCEDURE actualizar_goleadores(IN id_torneo BIGINT(20))
BEGIN

delete from posicion_goleador
where torneo_fk = id_torneo;

insert into posicion_goleador(goles,jugador_fk,torneo_fk,equipo_fk)
select count(*) as cantGoles,g.jugador_fk,id_torneo,g.equipo_fk
from gol g
inner join jugador j on j.id_jugador = g.jugador_fk
inner join partido p on p.id_partido = g.partido_fk
inner join fecha f on f.id_fecha = p.id_fecha
inner join torneo t on t.id_torneo = f.torneo_fk
where j.activo = true
and t.id_torneo = id_torneo
group by g.jugador_fk,id_torneo,j.equipo_fk
order by cantGoles desc;


update posicion_goleador pg,
(
select sum(goles) as cantGoles,pg2.jugador_fk as jugId
from posicion_goleador pg2
where pg2.torneo_fk in (select torneo_anterior_fk from torneo_anterior where torneo = id_torneo)
group by jugId
) tornAnt
set goles = goles + tornAnt.cantGoles
where pg.jugador_fk = tornAnt.jugId and pg.torneo_fk = id_torneo;

insert ignore into posicion_goleador(goles,jugador_fk,torneo_fk,equipo_fk)
select sum(goles) as cantGoles,jugador_fk,id_torneo,j.equipo_fk
from posicion_goleador pg2
inner join jugador j on j.id_jugador = pg2.jugador_fk
where pg2.torneo_fk in (select torneo_anterior_fk from torneo_anterior where torneo = id_torneo)
and j.equipo_fk in (select equipo_fk from torneo_equipo where torneo_fk = id_torneo)
group by jugador_fk,id_torneo,j.equipo_fk;


END //
DELIMITER ;