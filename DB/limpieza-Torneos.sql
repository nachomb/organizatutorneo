DELETE FROM TARJETA;
DELETE FROM GOL;
DELETE FROM ALINEACION_titulares;
DELETE FROM ALINEACION_suplentes;
-- Eliminar constraints (entre alineacion y partido)
DELETE FROM ALINEACION;
-- Eliminar constraints (entre alineacion y partido)
DELETE FROM PARTIDO;
DELETE FROM FECHA;
DELETE FROM TORNEO_EQUIPO;
DELETE FROM POSICION_TORNEO;
DELETE FROM TORNEO;

/* Para Regenerar el esquema, borrar a mano
 las siguientes tablas: ( en el script no 
 va por las constraints)
DROP TABLE PARTIDO;
DROP TABLE FECHA;
DROP TABLE TORNEO_EQUIPO;
DROP TABLE POSICION_TORNEO;
DROP TABLE TORNEO;*/