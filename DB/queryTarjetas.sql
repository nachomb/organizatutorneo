﻿SELECT count(*) as tarjetas,t.tipo, j.nombre,j.apellido, e.nombre
FROM tarjeta t
inner join jugador j on j.id_jugador = t.jugador_fk
inner join equipo e on e.id_equipo = j.equipo_fk
where e.id_equipo in(select equipo_fk from torneo_equipo where torneo_fk = 45)
GROUP BY t.`JUGADOR_FK`,tipo
order by tarjetas desc;

drop procedure if exists actualizar_tarjetas;
DELIMITER //
CREATE PROCEDURE actualizar_tarjetas(IN id_torneo BIGINT(20))
BEGIN

delete from cantidad_tarjetas
where torneo_fk = id_torneo;

insert into cantidad_tarjetas(tarjetas_amarillas,jugador_fk, torneo_fk)
select count(*) as cantAmarillas, jugador_fk, id_torneo
FROM tarjeta t
  inner join jugador j on j.id_jugador = t.jugador_fk
  inner join equipo e on e.id_equipo = j.equipo_fk
where e.id_equipo in(
      select equipo_fk from torneo_equipo where torneo_fk = id_torneo
       )
and TIPO = 'AMARILLA'
GROUP BY t.`JUGADOR_FK`
order by cantAmarillas desc;

SELECT count(*) as tarjetas,t.tipo, j.nombre,j.apellido, e.nombre
FROM tarjeta t
inner join jugador j on j.id_jugador = t.jugador_fk
inner join equipo e on e.id_equipo = j.equipo_fk
where e.id_equipo in(select equipo_fk from torneo_equipo where torneo_fk = 45)
GROUP BY t.`JUGADOR_FK`,tipo
order by tarjetas desc;

insert into posicion_goleador(goles,jugador_fk,torneo_fk)
select count(*) as cantGoles,g.jugador_fk,id_torneo
from gol g
inner join jugador j on j.id_jugador = g.jugador_fk
inner join partido p on p.id_partido = g.partido_fk
inner join fecha f on f.id_fecha = p.id_fecha
inner join torneo t on t.id_torneo = f.torneo_fk
where j.activo = true
and t.id_torneo = id_torneo
group by g.jugador_fk,id_torneo
order by cantGoles desc;


update posicion_goleador pg,
(
select sum(goles) as cantGoles,pg2.jugador_fk as jugId
from posicion_goleador pg2
where pg2.torneo_fk in (select torneo_anterior from torneo_anterior where torneo = id_torneo)
group by jugId
) tornAnt
set goles = goles + tornAnt.cantGoles
where pg.jugador_fk = tornAnt.jugId and pg.torneo_fk = id_torneo;

insert ignore into posicion_goleador(goles,jugador_fk,torneo_fk)
select sum(goles) as cantGoles,jugador_fk,id_torneo
from posicion_goleador pg2
inner join jugador j on j.id_jugador = pg2.jugador_fk
where pg2.torneo_fk in (select torneo_anterior from torneo_anterior where torneo = id_torneo)
and j.equipo_fk in (select equipo_fk from torneo_equipo where torneo_fk = id_torneo)
group by jugador_fk,id_torneo;


END //
DELIMITER ;