﻿DELIMITER $$

DROP PROCEDURE IF EXISTS `actualizar_tarjetas` $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizar_tarjetas`(IN id_torneo BIGINT(20))
BEGIN

delete from cantidad_tarjetas
where torneo_fk = id_torneo;

/*inserto los jugadores con tarjetas amarillas*/
insert into cantidad_tarjetas(tarjetas_amarillas,jugador_fk, torneo_fk, equipo_fk)
  select count(*) as cantAmarillas, jugador_fk, id_torneo, t.equipo_fk
  FROM tarjeta t
    inner join jugador j on j.id_jugador = t.jugador_fk
    inner join partido p on p.id_partido = t.partido_fk
    inner join fecha f on f.id_fecha = p.id_fecha
  where f.torneo_fk = id_torneo
  and TIPO = 'AMARILLA'
  GROUP BY t.`JUGADOR_FK`,id_torneo,t.equipo_fk
  order by cantAmarillas desc;

/*actualizo los jugadores que tienen tarjetas roja*/
update cantidad_tarjetas ct,
  (select count(*) as cant,j.id_jugador as id_jugador from tarjeta t
  inner join jugador j on j.id_jugador = t.jugador_fk
    inner join partido p on p.id_partido = t.partido_fk
    inner join fecha f on f.id_fecha = p.id_fecha
  where f.torneo_fk = id_torneo
  and TIPO = 'ROJA'
  GROUP BY id_jugador
  ) tarjetasRojas
set ct.tarjetas_rojas = tarjetasRojas.cant
where ct.jugador_fk = tarjetasRojas.id_jugador and torneo_fk = id_torneo;

/*inserto los jugadores con tarjetas rojas SOLAMENTE*/
insert ignore into cantidad_tarjetas(tarjetas_rojas,jugador_fk,torneo_fk, equipo_fk)
  select count(*) as cantRojas,jugador_fk, id_torneo, t.equipo_fk
  from tarjeta t
    inner join jugador j on j.id_jugador = t.jugador_fk
    inner join partido p on p.id_partido = t.partido_fk
    inner join fecha f on f.id_fecha = p.id_fecha
  where f.torneo_fk = id_torneo
  and TIPO = 'ROJA'
  GROUP BY jugador_fk,id_torneo,t.equipo_fk
order by cantRojas desc;

/*actualizo las tarjetas amarillas y rojas si este torneo depende de torneos anteriores*/
update cantidad_tarjetas ct,
(
  select sum(tarjetas_amarillas) as cantAmarillas, sum(tarjetas_rojas) as cantRojas, ct2.jugador_fk as jugId
  from cantidad_tarjetas ct2
  where ct2.torneo_fk in (select torneo_anterior_fk from torneo_anterior where torneo = id_torneo)
  group by jugId
) tornAnt
set tarjetas_amarillas = tarjetas_amarillas + tornAnt.cantAmarillas,
tarjetas_rojas = tarjetas_rojas + tornAnt.cantRojas
where ct.jugador_fk = tornAnt.jugId and ct.torneo_fk = id_torneo;

/*inserto las tarjetas de torneos anteriores*/
insert ignore into cantidad_tarjetas(tarjetas_amarillas,tarjetas_rojas,jugador_fk,torneo_fk, equipo_fk)
  select sum(tarjetas_amarillas) as cantAmarillas, sum(tarjetas_rojas) as cantRojas, jugador_fk, id_torneo, ct2.equipo_fk
  from cantidad_tarjetas ct2
  inner join jugador j on j.id_jugador = ct2.jugador_fk
  where ct2.torneo_fk in (select torneo_anterior_fk from torneo_anterior where torneo = id_torneo)
  and j.equipo_fk in (select equipo_fk from torneo_equipo where torneo_fk = id_torneo)
  group by jugador_fk, id_torneo, ct2.equipo_fk;


END $$

DELIMITER ;