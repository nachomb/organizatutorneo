package ar.com.noxit.picadito.sms;

import java.io.IOException;

import org.smslib.AGateway.Protocols;
import org.smslib.GatewayException;
import org.smslib.Library;
import org.smslib.SMSLibException;
import org.smslib.Service;
import org.smslib.Settings;
import org.smslib.TimeoutException;
import org.smslib.modem.SerialModemGateway;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ar.com.noxit.picadito.sms.reader.Reader;
import ar.com.noxit.picadito.sms.writer.Writer;
import ar.com.noxit.servicios.IInvitacionService;

public class App {

    private IInvitacionService invitacionService;

    private static final String DEFAULT_APPLICATION_CONTEXT_XML_FILE = "applicationContext.xml";
    private static final String DEFAULT_SIMPIN = "0000";
    private static final int DEFAULT_BAUD_RATE = 921600;
    private static final String DEFAULT_MODEM_MODEL = "V180";
    private static final String DEFAULT_MODEM_VENDOR = "Motorola";
    private static final String DEFAULT_SERIAL_PORT = "COM6";
    // private static final String DEFAULT_SERIAL_PORT = "/dev/ttyS0";
    private static final String DEFAULT_MODEM_ID = "modem.com6";
    private static final String APPLICATION = "aplicacion";

    public static void main(String[] args) {
        settingsforLinux();

        ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext(
                DEFAULT_APPLICATION_CONTEXT_XML_FILE);
        App aplicacion = (App) contexto.getBean(APPLICATION, App.class);
        aplicacion.ejecutar();

    }

    public void ejecutar() {
        SerialModemGateway modem = createSerialModemGateway();
        // Se crean los hilos deshabilitados
        Reader reader = new Reader();
        reader.setInvitacionService(invitacionService);
        Writer writer = new Writer(modem);
        writer.setInvitacionService(invitacionService);
        try {
            Service.getInstance().addGateway(modem);
            Service.getInstance().startService();
            reader.enable();
            writer.enable();
        } catch (TimeoutException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (GatewayException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SMSLibException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                writer.cancel();
                reader.cancel();
                Service.getInstance().stopService();
            } catch (TimeoutException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (GatewayException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SMSLibException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    public static void settingsforLinux() {
        /** -Dsmslib.serial.polling=true -Dsmslib.serial.noflush=true */
        /** Para recordar que se puede setear por aca */
        Service service = Service.getInstance();
        Settings settings = service.getSettings();
        settings.SERIAL_NOFLUSH = true;
        settings.SERIAL_POLLING = true;
    }

    public static void printLibraryInfo() {
        System.out.println("Serial gsm modem Reading and Receiving.");
        System.out.println(Library.getLibraryDescription());
        System.out.println("Version: " + Library.getLibraryVersion());
    }

    public static SerialModemGateway createSerialModemGateway() {
        SerialModemGateway gateway = new SerialModemGateway(DEFAULT_MODEM_ID, DEFAULT_SERIAL_PORT, DEFAULT_BAUD_RATE,
                DEFAULT_MODEM_VENDOR, DEFAULT_MODEM_MODEL);
        gateway.setProtocol(Protocols.PDU);
        gateway.setInbound(true);
        gateway.setOutbound(true);
        gateway.setSimPin(DEFAULT_SIMPIN);
        return gateway;
    }

    public void setInvitacionService(IInvitacionService invitacionService) {
        this.invitacionService = invitacionService;
    }

    public IInvitacionService getInvitacionService() {
        return invitacionService;
    }
}
