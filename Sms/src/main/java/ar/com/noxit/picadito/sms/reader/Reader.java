package ar.com.noxit.picadito.sms.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.AGateway;
import org.smslib.AGateway.GatewayStatuses;
import org.smslib.ICallNotification;
import org.smslib.IGatewayStatusNotification;
import org.smslib.IInboundMessageNotification;
import org.smslib.IOrphanedMessageNotification;
import org.smslib.InboundMessage;
import org.smslib.Message.MessageTypes;
import org.smslib.Service;
import org.smslib.threading.AServiceThread;

import ar.com.noxit.picadito.dominio.entidades.Invitacion;
import ar.com.noxit.servicios.IInvitacionService;

public class Reader extends AServiceThread implements IInboundMessageNotification, ICallNotification,
        IGatewayStatusNotification, IOrphanedMessageNotification {

    private static Logger logger = LoggerFactory.getLogger(Reader.class);

    private static final String NOMBRE = "Lector";
    private static final int DELAY = 500;
    private static final int INITIAL_DELAY = 500;

    private static final String ESPACIO = " ";
    private static final String RESPUESTA_SI = "si";
    private static final String RESPUESTA_NO = "no";

    private IInvitacionService invitacionService;

    private List<InboundMessage> mensajes = Collections.synchronizedList(new ArrayList<InboundMessage>());

    public Reader() {
        super(NOMBRE, DELAY, INITIAL_DELAY, false);
        init();
    }

    public Reader(Boolean activo) {
        super(NOMBRE, DELAY, INITIAL_DELAY, activo);
        init();
    }

    public void init() {
        Service.getInstance().setInboundMessageNotification(this);
        Service.getInstance().setCallNotification(this);
        Service.getInstance().setGatewayStatusNotification(this);
        Service.getInstance().setOrphanedMessageNotification(this);
    }

    // Thread Process
    @Override
    public void process() throws Exception {
        // List<InboundMessage> msgList = new ArrayList<InboundMessage>();
        // Service.getInstance().readMessages(msgList, MessageClasses.ALL);

        synchronized (mensajes) {
            if (!mensajes.isEmpty()) {
                logger.debug("Comienzo del proceso de mensajes de entrada.");
                Iterator<InboundMessage> itMensajes = mensajes.iterator();
                while (itMensajes.hasNext()) {
                    InboundMessage sms = itMensajes.next();
                    // Hacer algo con le mensaje
                    logger.debug(sms.toString());
                    procesarMensaje(sms);
                    itMensajes.remove();
                }
            } else {
                logger.debug("No hay mensajes a recibir.");
            }
            Thread.sleep(DELAY);
        }
    }

    private void procesarMensaje(InboundMessage sms) {
        String numero = sms.getSmscNumber();
        String mensaje = sms.getText().trim().toUpperCase();
        String respuesta = "";
        Long idPartido = null;
        try {
            int primerEspacio = mensaje.indexOf(ESPACIO);
            idPartido = Long.valueOf(mensaje.substring(0, primerEspacio));
            respuesta = mensaje.substring(primerEspacio, mensaje.indexOf(ESPACIO, primerEspacio));
            Invitacion invitacion = invitacionService.getInvitacionByIdPartidoYNumero(numero, idPartido);
            if (invitacion == null)
                return;
            if (RESPUESTA_SI.equals(respuesta)) {
                invitacionService.cambiarEstadoAConfirmado(invitacion);
            } else if (RESPUESTA_NO.equals(respuesta)) {
                invitacionService.cambiarEstadoACancelado(invitacion);
            }
        } catch (NumberFormatException ex) {
            idPartido = null;
        }

    }

    // Inbound message process
    @Override
    public void process(AGateway gateway, MessageTypes msgType, InboundMessage msg) {
        if (msgType == MessageTypes.INBOUND) {
            logger.debug(">>> Mensaje de entrada del Gateway: " + gateway.getGatewayId());

            synchronized (mensajes) {
                mensajes.add(msg);
            }

        } else if (msgType == MessageTypes.STATUSREPORT) {
            logger.debug(">>> Reporte detectado del Gateway: " + gateway.getGatewayId());
            logger.debug(msg.toString());
        }

    }

    // In call process
    @Override
    public void process(AGateway gateway, String callerId) {
        logger.debug(">>> Llamada entrante del Gateway: " + gateway.getGatewayId() + " : " + callerId);
    }

    // Inbound status process
    @Override
    public void process(AGateway gateway, GatewayStatuses oldStatus, GatewayStatuses newStatus) {
        logger.debug(">>> Gateway Status change for " + gateway.getGatewayId() + ", OLD: " + oldStatus + " -> NEW: "
                + newStatus);
    }

    // Orphan inbound message process
    @Override
    public boolean process(AGateway gateway, InboundMessage msg) {
        logger.debug(">>> Mensaje huerfano del Gateway: " + gateway.getGatewayId());
        synchronized (mensajes) {
            mensajes.add(msg);
        }

        return false;
    }

    public void setInvitacionService(IInvitacionService invitacionService) {
        this.invitacionService = invitacionService;
    }

    public IInvitacionService getInvitacionService() {
        return invitacionService;
    }

}
