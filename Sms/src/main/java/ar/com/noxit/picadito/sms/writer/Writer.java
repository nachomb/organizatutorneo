package ar.com.noxit.picadito.sms.writer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smslib.AGateway;
import org.smslib.IOutboundMessageNotification;
import org.smslib.OutboundMessage;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;
import org.smslib.threading.AServiceThread;

import ar.com.noxit.picadito.dominio.entidades.Invitacion;
import ar.com.noxit.servicios.IInvitacionService;

public class Writer extends AServiceThread implements IOutboundMessageNotification {

    private static Logger logger = LoggerFactory.getLogger(Writer.class);

    private static final String NOMBRE = "Escritor";
    private static final int DELAY = 500;
    private static final int INITIAL_DELAY = 500;
    private SerialModemGateway modem;
    private IInvitacionService invitacionService;

    public Writer(SerialModemGateway modem) {
        super(NOMBRE, DELAY, INITIAL_DELAY, false);
        this.modem = modem;
        init();
    }

    public Writer(SerialModemGateway modem, Boolean activo) {
        super(NOMBRE, DELAY, INITIAL_DELAY, activo);
        this.modem = modem;
        init();
    }

    private void init() {
        Service.getInstance().setOutboundMessageNotification(this);
    }

    @Override
    public void process() throws Exception {
        // Obtener mensajes a enviar desde la base
        List<Invitacion> invitaciones = invitacionService.getAllInvitacionesAEnviar();
        if (!invitaciones.isEmpty()) {
            logger.debug("Comienzo el encolado de mensajes a enviar.");
            // Enviar mensaje asincronicamente.
            Iterator<Invitacion> itInvitaciones = invitaciones.iterator();
            while (itInvitaciones.hasNext()) {
                Invitacion invitacion = itInvitaciones.next();
                String telefonoDestino = invitacion.getTelefonoDestino();
                if (telefonoDestino == null || telefonoDestino.isEmpty()) {
                    invitacionService.cambiarEstadoACancelado(invitacion);
                } else {
                    OutboundMessage msg = new OutboundMessage(telefonoDestino, invitacion.getMensaje());
                    invitacionService.cambiarEstadoAEnviado(invitacion);
                    Service.getInstance().queueMessage(msg, modem.getGatewayId());
                }
                itInvitaciones.remove();
            }
        } else {
            logger.debug("No hay mensajes a enviar.");
        }
        Thread.sleep(DELAY);
    }

    @Override
    public void process(AGateway gateway, OutboundMessage msg) {
        logger.debug("Outbound handler called from Gateway: " + gateway.getGatewayId());
        logger.debug(msg.toString());
    }

    public void setInvitacionService(IInvitacionService invitacionService) {
        this.invitacionService = invitacionService;
    }

    public IInvitacionService getInvitacionService() {
        return invitacionService;
    }

}
