package ar.com.noxit.picadito.dominio.entidades;

import javax.persistence.Entity;

@Entity
public class TarjetaAmarilla extends Tarjeta {

    /**
     * Serial Version UID autogenerado
     */
    private static final long serialVersionUID = -2265794979150020106L;

    public TarjetaAmarilla() {
        super(Tarjeta.TIPO_AMARILLA);
    }

    public TarjetaAmarilla(Jugador jugador, Partido partido, Equipo equipo) {
        super(jugador, partido, Tarjeta.TIPO_AMARILLA, equipo);
    }

}
