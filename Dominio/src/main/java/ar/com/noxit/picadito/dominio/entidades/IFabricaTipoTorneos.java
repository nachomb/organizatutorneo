package ar.com.noxit.picadito.dominio.entidades;

public interface IFabricaTipoTorneos {

	public TipoTorneo getTipoTorneo(String nombre);
}
