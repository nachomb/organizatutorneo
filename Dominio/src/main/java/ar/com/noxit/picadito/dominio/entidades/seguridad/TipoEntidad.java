package ar.com.noxit.picadito.dominio.entidades.seguridad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

/**
 * Entidad que representa un tipo de entidad de una accion
 * 
 */
@Entity
@Table(name = "tipo_entidad")
public class TipoEntidad extends EntidadGenerica {

    // Identificador
    @Id
    @Column(name = "ID_TIPO_ENTIDAD")
    @GeneratedValue
    private Long id;

    // Nombre del tipo de entidad
    @Column(name = "NOMBRE", nullable = false)
    private String nombre;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return this.nombre;
    }

}
