package ar.com.noxit.picadito.dominio.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "sancion")
public class Sancion extends EntidadGenerica {

    /**
     * Serial Version UID autogenerado
     */
    private static final long serialVersionUID = 1566062433683147888L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SANCION")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FECHA_INICIO_FK")
    private Fecha fechaInicio;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TORNEO_FK")
    private Torneo torneo;
    
    @Column
    private Integer cantidadFechas;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "JUGADOR_FK")
    private Jugador jugadorSancionado;

    @Column(name = "CUMPLIDA")
    private Boolean cumplida;
    
    
    /*@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_SANCION")
    private TipoSancion tipo;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(joinColumns = @JoinColumn(name = "ID_SANCION", referencedColumnName = "ID_SANCION"), inverseJoinColumns = @JoinColumn(name = "ID_PARTIDO", referencedColumnName = "ID_PARTIDO"))
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private List<Partido> partidosAplicados;*/

    public Fecha getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Fecha fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Integer getCantidadFechas() {
		return cantidadFechas;
	}

	public void setCantidadFechas(Integer cantidadFechas) {
		this.cantidadFechas = cantidadFechas;
	}


    public Sancion() {
        cumplida = false;
    }

   /* public Sancion(Partido partidoInicio, Jugador jugadorSancionado, TipoSancion tipo) {
        this.partidoInicio = partidoInicio;
        this.jugadorSancionado = jugadorSancionado;
        this.tipo = tipo;
        cumplida = false;
    }

    public void agregarPartidoSancionCumplida(Partido partido) {
        if (this.partidosAplicados == null)
            this.partidosAplicados = new ArrayList<Partido>();
        if (tipo.getDuracionSancion().intValue() == partidosAplicados.size() && !partidosAplicados.contains(partido)) {
            cumplida = true;
        } else {
            if (!this.partidosAplicados.contains(partido))
                this.partidosAplicados.add(partido);
        }
    }*/

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setJugadorSancionado(Jugador jugadorSancionado) {
        this.jugadorSancionado = jugadorSancionado;
    }

    public Jugador getJugadorSancionado() {
        return jugadorSancionado;
    }

    public void setCumplida(Boolean cumplida) {
        this.cumplida = cumplida;
    }

    public Boolean getCumplida() {
        return cumplida;
    }

    public Torneo getTorneo() {
		return torneo;
	}

	public void setTorneo(Torneo torneo) {
		this.torneo = torneo;
	}

	public void cumplirSancion() {
        cumplida = true;
    }
    
}
