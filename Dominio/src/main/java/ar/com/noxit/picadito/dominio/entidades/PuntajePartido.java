package ar.com.noxit.picadito.dominio.entidades;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "puntaje_partido")
public class PuntajePartido extends EntidadGenerica {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 6153815109137643019L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PUNTAJEPARTIDO")
    private Long id;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "PARTIDO_FK")
    private Partido partido;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "JUGADOR_FK")
    private Jugador jugador;
    
	@Column(name = "puntaje")
    private Integer puntaje;
	
	@Column(name = "figura")
	private Boolean figura;
	
    @Column(name = "numero")
    private Integer numero;

    public PuntajePartido(Jugador jugador, Partido partido, int puntaje, Boolean figura, int numero) {
		this.jugador = jugador;
		this.partido = partido;
		this.puntaje = puntaje;
		this.figura = figura;
		this.numero = numero;
	}
	
    public Boolean getFigura() {
		return figura;
	}

	public void setFigura(Boolean figura) {
		this.figura = figura;
	}


	public PuntajePartido() {
	}

	public Partido getPartido() {
		return partido;
	}

	public void setPartido(Partido partido) {
		this.partido = partido;
	}

	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	public Integer getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(Integer puntaje) {
		this.puntaje = puntaje;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
		
	}
    
	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

}
