package ar.com.noxit.picadito.dominio.entidades.generico;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.IPersistible;

public interface IEntidad extends IPersistible<Long> {

    public boolean equals(IEntidad entidad);

}
