package ar.com.noxit.picadito.dominio.entidades;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

@Entity
@Table(name = "jugador")
public class Jugador extends EntidadGenerica {

    /**
     * Serial Version UID autogenerado
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_JUGADOR")
    private Long id;

    @Column
    private String nombre;

    @Column
    private String apellido;

    @Column
    private String apodo;
    
    @Column
    private String dni;

    @Column
    private Boolean activo;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USUARIO_FK")
    private Usuario usuario;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "EQUIPO_FK")
    private Equipo equipo;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "goleador")
    private List<Gol> goles;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    @JoinTable(name = "grupo_jugador", joinColumns = { @JoinColumn(name = "ID_JUGADOR") }, inverseJoinColumns = { @JoinColumn(name = "ID_GRUPO", referencedColumnName = "ID_GRUPO") })
    private Collection<Grupo> grupos;
    
    @OneToOne
    @JoinColumn(name = "USUARIO_CREADOR_FK")
    private Usuario creador;

    @Transient
    private String convocado;

    @Transient
    private Integer puntajePartido;
    
    @Transient
    private Integer numero;
    
    @Transient
    private Boolean esFigura;
    
    @Transient
    private Integer cantGolesPartido;
    
    @Transient
    private Integer cantAmarillasPartido = 0;
    
    @Transient
    private Integer cantRojasPartido = 0;

    public Integer getCantGolesPartido() {
		return cantGolesPartido;
	}

	public void setCantGolesPartido(Integer cantGolesPartido) {
		this.cantGolesPartido = cantGolesPartido;
	}

	public Integer getCantAmarillasPartido() {
		return cantAmarillasPartido;
	}

	public void setCantAmarillasPartido(Integer cantAmarillasPartido) {
		this.cantAmarillasPartido = cantAmarillasPartido;
	}

	public Integer getCantRojasPartido() {
		return cantRojasPartido;
	}

	public void setCantRojasPartido(Integer cantRojasPartido) {
		this.cantRojasPartido = cantRojasPartido;
	}

	public Boolean getEsFigura() {
		return esFigura;
	}

	public void setEsFigura(Boolean esFigura) {
		this.esFigura = esFigura;
	}

	public Integer getPuntajePartido() {
        return puntajePartido;
    }

    public void setPuntajePartido(Integer puntajePartido) {
        this.puntajePartido = puntajePartido;
    }

    public String getConvocado() {
        return convocado;
    }

    public void setConvocado(String convocado) {
        this.convocado = convocado;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "jugadorSancionado")
    private List<Sancion> sanciones;

    public Jugador() {
        setActivo(true);
    }

    public String getApodo() {
        return apodo;
    }

    public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public String toString() {
        //String ap = ("".equals(apodo) || apodo == null) ? "" : "(" + getApodo() + ")";
        return getApellido()+ ", " + getNombre()  ;
    }

    public void setGoles(List<Gol> goles) {
        this.goles = goles;
    }

    public List<Gol> getGoles() {
        return goles;
    }

    public void setGrupos(Collection<Grupo> grupos) {
        this.grupos = grupos;
    }

    public Collection<Grupo> getGrupos() {
        return grupos;
    }

    public void setSanciones(List<Sancion> sanciones) {
        this.sanciones = sanciones;
    }

    public List<Sancion> getSanciones() {
        return sanciones;
    }

    public void agregarSancion(Sancion sancion) {
        if (this.sanciones == null)
            this.sanciones = new ArrayList<Sancion>();
        this.sanciones.add(sancion);
    }

    public boolean getEsTitular() {
        return "T".equals(convocado);
    }

    public boolean getEsSuplente() {
        return "S".equals(convocado);
    }

    public boolean getEstaSuspendido() {
        for (Sancion sancion : this.sanciones) {
            if (!sancion.getCumplida())
                return true;
        }
        return false;
    }

	public void incrementarAmarilla() {
		++this.cantAmarillasPartido;
		
	}

	public void incrementarRoja() {
		++this.cantRojasPartido;
		
	}
	
	public Integer getCantTarjetasTotal(){
		return this.cantAmarillasPartido + this.cantRojasPartido;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getNombreYApellido(){
		StringBuilder builder = new StringBuilder();
		builder.append(this.nombre);
		builder.append(" ");
		builder.append(this.getApellido());
		return builder.toString();
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}
	
}
