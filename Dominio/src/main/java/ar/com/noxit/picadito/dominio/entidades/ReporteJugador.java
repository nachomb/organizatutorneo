package ar.com.noxit.picadito.dominio.entidades;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ReporteJugador {

	private Jugador jugador;
	private Integer cantGoles;
	private Integer cantPartidos;
	private Float golesPromedio;
	private Float puntajePromedio;
	private Integer cantAmarillas;
	private Integer cantRojas;
	private Integer cantFigura;
	private NumberFormat formateador = new DecimalFormat("###,##");
	
	public ReporteJugador(PosicionGoleador posGol) {
		this.jugador = posGol.getJugador();
		this.cantGoles = posGol.getGoles();
	}
	
	public ReporteJugador() {
	}
	
	public Integer getCantFigura() {
		return cantFigura;
	}

	public void setCantFigura(Integer cantFigura) {
		this.cantFigura = cantFigura;
	}
	
	public Jugador getJugador() {
		return jugador;
	}
	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}
	public Integer getCantGoles() {
		return cantGoles;
	}
	public void setCantGoles(Integer cantGoles) {
		this.cantGoles = cantGoles;
	}
	public Integer getCantPartidos() {
		return cantPartidos;
	}
	public void setCantPartidos(Integer cantPartidos) {
		this.cantPartidos = cantPartidos;
	}
	public Float getGolesPromedio() {
		return golesPromedio;
	}
	
	public void setGolesPromedio(Float golesPromedio) {
		this.golesPromedio = golesPromedio;
	}
	public Float getPuntajePromedio() {
		return puntajePromedio;
	}
	public void setPuntajePromedio(Float puntajePromedio) {
		this.puntajePromedio = puntajePromedio;
	}
	public Integer getCantAmarillas() {
		return cantAmarillas;
	}
	public void setCantAmarillas(Integer cantAmarillas) {
		this.cantAmarillas = cantAmarillas;
	}
	public Integer getCantRojas() {
		return cantRojas;
	}
	public void setCantRojas(Integer cantRojas) {
		this.cantRojas = cantRojas;
	}
}
