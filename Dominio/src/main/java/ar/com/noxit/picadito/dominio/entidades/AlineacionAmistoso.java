package ar.com.noxit.picadito.dominio.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "alineacion_amistoso")
public class AlineacionAmistoso extends EntidadGenerica {

    /**
     * Serial Version UID autogenerado
     */
    private static final long serialVersionUID = 5196880247898491699L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ALINEACION_AMISTOSO")
    private Long id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "PARTIDO_AMISTOSO")
    private PartidoAmistoso partidoAmistoso;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH },fetch = FetchType.LAZY)
    @JoinTable(name = "alineacion_amistoso_titulares", 
            joinColumns = { @JoinColumn(name = "ID_ALINEACION_AMISTOSO") }, 
            inverseJoinColumns = { @JoinColumn(name = "ID_JUGADOR", referencedColumnName = "ID_JUGADOR") })
    private List<Jugador> titulares = new ArrayList<Jugador>();

    public AlineacionAmistoso() {
        titulares = new ArrayList<Jugador>();
    }

    public AlineacionAmistoso(PartidoAmistoso partidoAmistoso) {
        this.partidoAmistoso = partidoAmistoso;
        titulares = new ArrayList<Jugador>();
    }

    public AlineacionAmistoso(PartidoAmistoso partidoAmistoso, List<Jugador> equipo) {
        this.partidoAmistoso = partidoAmistoso;
        titulares = crearListado(equipo);
    }

    private List<Jugador> crearListado(List<Jugador> equipo) {
        return equipo;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setPartidoAmistoso(PartidoAmistoso partidoAmistoso) {
        this.partidoAmistoso = partidoAmistoso;
    }

    public PartidoAmistoso getPartidoAmistoso() {
        return partidoAmistoso;
    }

    public void setTitulares(List<Jugador> titulares) {
        this.titulares = titulares;
    }

    public List<Jugador> getTitulares() {
        return titulares;
    }

    public void recalcularAlineacion(List<Jugador> equipo) {
        if (titulares == null)
            titulares = crearListado(equipo);
        else {
            titulares.clear();
            titulares.addAll(crearListado(equipo));
        }
    }

}
