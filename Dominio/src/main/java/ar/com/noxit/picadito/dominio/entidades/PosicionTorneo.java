package ar.com.noxit.picadito.dominio.entidades;

import java.text.DecimalFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "posicion_torneo")
public class PosicionTorneo extends EntidadGenerica {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final DecimalFormat formateador = new DecimalFormat("###");

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_POSICION")
	private Long id;

	@Column
	private Integer partidosJugados;
	@Column
	private Integer partidosGanados;
	@Column
	private Integer partidosEmpatados;
	@Column
	private Integer partidosPerdidos;
	@Column
	private Integer puntos;
	@Column
	private Integer golesAFavor;
	@Column
	private Integer golesEnContra;
	@Column
	private Integer amarillas;
	@Column
	private Integer rojas;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "EQUIPO_FK")
	private Equipo equipo;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "TORNEO_FK")
	private Torneo torneo;

	@Transient
	private Integer posicion;
	
	public Integer getPosicion() {
		return posicion;
	}


	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}


	public PosicionTorneo(Torneo torneo, Equipo equipo) {
		this.torneo = torneo;
		this.equipo = equipo;
		partidosJugados = 0;
		partidosGanados = 0;
		partidosEmpatados = 0;
		partidosPerdidos = 0;
		puntos = 0;
		golesAFavor = 0;
		golesEnContra = 0;
		amarillas = 0;
		rojas = 0;
	}
	

	public PosicionTorneo() {
		super();
	}



	public Integer getPartidosJugados() {
		return partidosJugados;
	}

	public void setPartidosJugados(Integer partidosJugados) {
		this.partidosJugados = partidosJugados;
	}

	public Integer getPartidosGanados() {
		return partidosGanados;
	}

	public void setPartidosGanados(Integer partidosGanados) {
		this.partidosGanados = partidosGanados;
	}

	public Integer getPartidosEmpatados() {
		return partidosEmpatados;
	}

	public void setPartidosEmpatados(Integer partidosEmpatados) {
		this.partidosEmpatados = partidosEmpatados;
	}

	public Integer getPartidosPerdidos() {
		return partidosPerdidos;
	}

	public void setPartidosPerdidos(Integer partidosPerdidos) {
		this.partidosPerdidos = partidosPerdidos;
	}

	public Integer getPuntos() {
		return puntos;
	}

	public void setPuntos(Integer puntos) {
		this.puntos = puntos;
	}

	public Integer getGolesAFavor() {
		return golesAFavor;
	}

	public void setGolesAFavor(Integer golesAFavor) {
		this.golesAFavor = golesAFavor;
	}

	public Integer getGolesEnContra() {
		return golesEnContra;
	}

	public void setGolesEnContra(Integer golesEnContra) {
		this.golesEnContra = golesEnContra;
	}

	public Integer getAmarillas() {
		return amarillas;
	}

	public void setAmarillas(Integer amarillas) {
		this.amarillas = amarillas;
	}

	public Integer getRojas() {
		return rojas;
	}

	public void setRojas(Integer rojas) {
		this.rojas = rojas;
	}

	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;

	}

	public void setTorneo(Torneo torneo) {
		this.torneo = torneo;
	}

	public Torneo getTorneo() {
		return torneo;
	}

	public void incrementarGanados() {
		++this.partidosGanados;
		
	}

	public void incrementarPerdidos() {
		++this.partidosPerdidos;
		
	}

	public void incrementarEmpatados() {
		++this.partidosEmpatados;
	}

	public Integer getDiferencia(){
		return getGolesAFavor() - getGolesEnContra();
	}

	public Integer getPorcentajePuntos(){
		if(getPartidosJugados().equals(0))
			return 0;
		double puntosPorPartido = getPuntos().floatValue() / getPartidosJugados();
		double porcentajePuntos = (puntosPorPartido*100)/torneo.getCantPuntosGanador().floatValue();
		return Integer.valueOf(formateador.format(porcentajePuntos));
	}
	
}
