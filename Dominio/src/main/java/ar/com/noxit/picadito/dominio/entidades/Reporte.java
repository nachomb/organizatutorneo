package ar.com.noxit.picadito.dominio.entidades;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Reporte {

	private List<Equipo> equiposMasGolesFavor;
	private List<Equipo> equiposMenosGolesFavor;
	private List<Equipo> equiposMenosGolesEnContra;
	private List<Equipo> equiposMasGolesEnContra;
	private List<Equipo> equiposFairPlay;
	private List<Equipo> equiposMasSucios;
	private List<Equipo> equiposPorcentaje;
	
	private final static int NIVEL_DETALLE = 2;
	
	
	public List<Equipo> getEquiposMasGolesFavor() {
		return equiposMasGolesFavor;
	}
	public void setEquiposMasGolesFavor(List<Equipo> equiposMasGolesFavor) {
		this.equiposMasGolesFavor = equiposMasGolesFavor;
	}

	public List<Equipo> getEquiposMenosGolesFavor() {
		return equiposMenosGolesFavor;
	}
	public void setEquiposMenosGolesFavor(List<Equipo> equiposMenosGolesFavor) {
		this.equiposMenosGolesFavor = equiposMenosGolesFavor;
	}
	public List<Equipo> getEquiposMenosGolesEnContra() {
		return equiposMenosGolesEnContra;
	}
	public void setEquiposMenosGolesEnContra(List<Equipo> equiposMenosGolesEnContra) {
		this.equiposMenosGolesEnContra = equiposMenosGolesEnContra;
	}
	public List<Equipo> getEquiposMasGolesEnContra() {
		return equiposMasGolesEnContra;
	}
	public void setEquiposMasGolesEnContra(List<Equipo> equiposMasGolesEnContra) {
		this.equiposMasGolesEnContra = equiposMasGolesEnContra;
	}
	public List<Equipo> getEquiposFairPlay() {
		return equiposFairPlay;
	}
	public void setEquiposFairPlay(List<Equipo> equiposFairPlay) {
		this.equiposFairPlay = equiposFairPlay;
	}
	public List<Equipo> getEquiposMasSucios() {
		return equiposMasSucios;
	}
	public void setEquiposMasSucios(List<Equipo> equiposMasSucios) {
		this.equiposMasSucios = equiposMasSucios;
	}
	public List<Equipo> getEquiposPorcentaje() {
		return equiposPorcentaje;
	}
	public void setEquiposPorcentaje(List<Equipo> equiposPorcentaje) {
		this.equiposPorcentaje = equiposPorcentaje;
	}

	public void calcular(List<Equipo> equipos) {
		Collections.sort(equipos, new Comparator<Equipo>(){

			@Override
			public int compare(Equipo e1, Equipo e2) {
				return e2.getDatoReporte().getGolesAFavor().compareTo(e1.getDatoReporte().getGolesAFavor());
			}
			
		});
		List<Equipo> equiposOrdenadosGolesFavor = new ArrayList<Equipo>(equipos);
		setEquiposMasGolesFavor(new ArrayList<Equipo>(equiposOrdenadosGolesFavor.subList(0, NIVEL_DETALLE)));
		Collections.reverse(equiposOrdenadosGolesFavor);
		setEquiposMenosGolesFavor(equiposOrdenadosGolesFavor.subList(0, NIVEL_DETALLE));
		Collections.sort(equipos, new Comparator<Equipo>(){

			@Override
			public int compare(Equipo e1, Equipo e2) {
				return e2.getDatoReporte().getGolesEnContra().compareTo(e1.getDatoReporte().getGolesEnContra());
			}
			
		});
		List<Equipo> equiposOrdenadosGolesContra = new ArrayList<Equipo>(equipos);
		setEquiposMasGolesEnContra(new ArrayList<Equipo>(equiposOrdenadosGolesContra.subList(0, NIVEL_DETALLE)));
		Collections.reverse(equiposOrdenadosGolesContra);
		setEquiposMenosGolesEnContra(equiposOrdenadosGolesContra.subList(0, NIVEL_DETALLE));
	}
	
	
}
