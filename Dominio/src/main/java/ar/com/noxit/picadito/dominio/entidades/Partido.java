package ar.com.noxit.picadito.dominio.entidades;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.StringEscapeUtils;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "partido")
public class Partido extends EntidadGenerica {

    /**
     * Serial version autogenerado
     */
    private static final long serialVersionUID = 219207948366114966L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PARTIDO")
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_FECHA")
    private Fecha fecha;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "EQUIPO_LOCAL_FK")
    private Equipo equipoLocal;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "EQUIPO_VISITANTE_FK")
    private Equipo equipoVisitante;

    @Column
    private Integer golesLocal;

    @Column
    private Integer golesVisitante;

    @Column
    private Boolean activo;

    @Column
    private Boolean jugado;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPartido;

	@Column
    private String descripcion;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "ALINEACION_LOCAL")
    private Alineacion alineacionLocal;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "ALINEACION_VISITANTE")
    private Alineacion alineacionVisitante;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "partido")
    private List<Gol> goles;
    
    @OneToMany(mappedBy="partido",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    private List<Tarjeta> tarjetas;
    
    public Partido() {
        this.setActivo(true);
        this.setJugado(false);
        setGolesLocal(0);
        setGolesVisitante(0);
    }

    public Partido(Equipo local, Equipo visitante) {
        this.setActivo(true);
        this.setJugado(false);
        this.equipoLocal = local;
        this.equipoVisitante = visitante;
        setGolesLocal(0);
        setGolesVisitante(0);
    }

    public Partido(Equipo local, Equipo visitante, Fecha fecha) {
        this.setActivo(true);
        this.setJugado(false);
        this.equipoLocal = local;
        this.equipoVisitante = visitante;
        this.fecha = fecha;
        setGolesLocal(0);
        setGolesVisitante(0);
    }

    public void asignarAlineacionesDefault(Equipo equipoLocal, Equipo equipoVisitante, Fecha fecha) {
        if (this.alineacionLocal == null)
            alineacionLocal = new Alineacion(this, equipoLocal, fecha);
        else
            alineacionLocal.recalcularAlineacion(equipoLocal, fecha);
        if (this.alineacionVisitante == null)
            alineacionVisitante = new Alineacion(this, equipoVisitante, fecha);
        else
            alineacionVisitante.recalcularAlineacion(equipoVisitante, fecha);
    }

    public void asignarAlineaciones(List<Jugador> titularesLocal, List<Jugador> suplentesLocal,
            List<Jugador> titularesVisitante, List<Jugador> suplentesVisitante) {
        if (this.alineacionLocal == null)
            alineacionLocal = new Alineacion(this, titularesLocal, suplentesLocal);
        else
            alineacionLocal.recalcularAlineacion(titularesLocal, suplentesLocal);
        if (this.alineacionVisitante == null)
            alineacionVisitante = new Alineacion(this, titularesVisitante, suplentesVisitante);
        else
            alineacionVisitante.recalcularAlineacion(titularesVisitante, suplentesVisitante);
    }

    public void jugarPartido(Integer golesLocal, Integer golesVisitante) {
        setJugado(true);
        setGolesLocal(golesLocal);
        setGolesVisitante(golesVisitante);
    }

    public Equipo ganadorPartido() {
        if (golesLocal.intValue() > golesVisitante.intValue()) {
            return equipoLocal;
        } else if (golesLocal.intValue() < golesVisitante.intValue()) {
            return equipoVisitante;
        } else {
        	Torneo torneo = getFecha().getTorneo();
        	return torneo.getMejorPosicionado(equipoLocal, equipoVisitante);
        }
    }

    public Integer getDiferenciaGol() {
        return Math.abs(golesLocal.intValue() - golesVisitante.intValue());
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Equipo getEquipoLocal() {
        return equipoLocal;
    }

    public void setEquipoLocal(Equipo equipoLocal) {
        this.equipoLocal = equipoLocal;
    }

    public Equipo getEquipoVisitante() {
        return equipoVisitante;
    }

    public void setEquipoVisitante(Equipo equipoVisitante) {
        this.equipoVisitante = equipoVisitante;
    }

    public Integer getGolesLocal() {
        return golesLocal;
    }

    public void setGolesLocal(Integer golesLocal) {
        this.golesLocal = golesLocal;
    }

    public Integer getGolesVisitante() {
        return golesVisitante;
    }

    public void setGolesVisitante(Integer golesVisitante) {
        this.golesVisitante = golesVisitante;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setJugado(Boolean jugado) {
        this.jugado = jugado;
    }

    public Boolean getJugado() {
        return jugado;
    }

    public void setFecha(Fecha fecha) {
        this.fecha = fecha;
    }

    public Fecha getFecha() {
        return fecha;
    }

    @Override
    public String toString() {
        String str = "";
        if (equipoLocal != null && equipoVisitante != null) {
        	StringBuilder stringBuilder = new StringBuilder();
        	StringEscapeUtils.escapeHtml(equipoLocal.getNombre());
        	if(jugado){
				stringBuilder.append(equipoLocal.getNombre());
				stringBuilder.append(" ");
				stringBuilder.append(golesLocal);
				stringBuilder.append(" - ");
				stringBuilder.append(golesVisitante);
				stringBuilder.append(" ");
				stringBuilder.append(equipoVisitante.getNombre());
				stringBuilder.append(" Final");
        	}else{
				stringBuilder.append(equipoLocal.getNombre());
				stringBuilder.append(" vs ");
				stringBuilder.append(equipoVisitante.getNombre());
				stringBuilder.append(" ");
				if(fechaPartido != null){
					stringBuilder.append(Utils.dateToString(fechaPartido));
				}
				stringBuilder.append(" ");
				if(descripcion != null){
					stringBuilder.append(descripcion);
				}
				//stringBuilder.append(" - No Jugado");
        	}
        	str = stringBuilder.toString();
        }
        return str;
    }

    public void setFechaPartido(Date fechaPartido) {
        this.fechaPartido = fechaPartido;
    }

    public Date getFechaPartido() {
        return fechaPartido;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setAlineacionLocal(Alineacion alineacionLocal) {
        this.alineacionLocal = alineacionLocal;
    }

    public Alineacion getAlineacionLocal() {
        return alineacionLocal;
    }

    public void setAlineacionVisitante(Alineacion alineacionVisitante) {
        this.alineacionVisitante = alineacionVisitante;
    }

    public Alineacion getAlineacionVisitante() {
        return alineacionVisitante;
    }

    public Equipo getGanador() {
        if (getGolesLocal() > getGolesVisitante())
            return getEquipoLocal();
        else if (getGolesLocal() < getGolesVisitante())
            return getEquipoVisitante();
        return null;

    }

    public Equipo getPerdedor() {
        if (getGolesLocal() < getGolesVisitante())
            return getEquipoLocal();
        else if (getGolesLocal() > getGolesVisitante())
            return getEquipoVisitante();
        return null;
    }

    public void setGoles(List<Gol> goles) {
        this.goles = goles;
    }

    public List<Gol> getGoles() {
        return goles;
    }

    public void setTarjetas(List<Tarjeta> tarjetas) {
        this.tarjetas = tarjetas;
    }

    public List<Tarjeta> getTarjetas() {
        return tarjetas;
    }
    
    @Transient
    public Date getFechaPresentacion() {
    	Date fechaPresentacion = Utils.addMinutes(fechaPartido, -15);
		return fechaPresentacion;
	}

}
