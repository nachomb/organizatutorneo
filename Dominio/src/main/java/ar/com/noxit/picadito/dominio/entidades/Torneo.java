package ar.com.noxit.picadito.dominio.entidades;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.log4j.Logger;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

@Entity
@Table(name = "torneo")
public class Torneo extends EntidadGenerica {

	public static final int DEFAULT_CANT_JUGADORES_X_EQUIPO = 5;
	private static final int DEFAULT_CANT_PUNTOS_GANADOR = 3;
	private static final int DEFAULT_CANT_PUNTOS_EMPATE = 1;
	private static final int DEFAULT_CANT_PUNTOS_PERDEDOR = 0;

	/**
	 * Serial version autogenerado
	 */
	private static final long serialVersionUID = -5793467576381764058L;
	private static final Integer DEFAULT_CANT_FECHAS_SANCION_AMARILLA = 1;
	private static final Integer DEFAULT_CANT_FECHAS_SANCION_ROJA = 1;
	private static final Integer DEFAULT_CANT_TARJ_AMA_SANCION = 5;
	private static final Integer DEFAULT_CANT_TARJ_ROJ_SANCION = 1;
	
	private static Logger logger = Logger.getLogger(Torneo.class);

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_TORNEO")
	private Long id;

	@Column
	private String nombre;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "torneo")
	private List<Fecha> fechas;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "torneo")
	private List<PosicionTorneo> posiciones;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "torneo_equipo", joinColumns = { @JoinColumn(name = "TORNEO_FK", referencedColumnName = "ID_TORNEO") }, inverseJoinColumns = { @JoinColumn(name = "EQUIPO_FK", referencedColumnName = "ID_EQUIPO") })
	private List<Equipo> equipos;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "torneo_anterior", joinColumns = { @JoinColumn(name = "torneo", referencedColumnName = "ID_TORNEO") }, inverseJoinColumns = { @JoinColumn(name = "torneo_anterior_fk", referencedColumnName = "ID_TORNEO") })
	private List<Torneo> torneosAnteriores;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "torneo")
	private List<PosicionGoleador> tablaGoleadores;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "torneo")
	private List<CantidadTarjetas> jugadoresTarjetas;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "torneo")
	private List<Sancion> sanciones;

	@Column
	private Boolean idaYVuelta;

	@Column
	private Boolean activo;

	@Column
	private Integer cantidadFechas;

	@Column
	private Integer fechaActual;

	@Column
	private String tipoTorneo;

	@Column
	private Integer jugadoresPorEquipo = DEFAULT_CANT_JUGADORES_X_EQUIPO;
	@Column
	private Integer cantPuntosGanador = DEFAULT_CANT_PUNTOS_GANADOR;
	@Column
	private Integer cantPuntosEmpate = DEFAULT_CANT_PUNTOS_EMPATE;
	@Column
	private Integer cantPuntosPerdedor = DEFAULT_CANT_PUNTOS_PERDEDOR;
	@Column
	private Integer cantFechasSancionAmarilla = DEFAULT_CANT_FECHAS_SANCION_AMARILLA;
	@Column
	private Integer cantFechasSancionRoja = DEFAULT_CANT_FECHAS_SANCION_ROJA;
	@Column
	private Integer cantTarjAmarillasParaSancion = DEFAULT_CANT_TARJ_AMA_SANCION;
	@Column
	private Integer cantTarjRojasParaSancion = DEFAULT_CANT_TARJ_ROJ_SANCION;
    @OneToOne
    @JoinColumn(name = "USUARIO_CREADOR_FK")
    private Usuario creador;

	@Transient
	private static final IFabricaTipoTorneos fabricaTipoTorneos = new FabricaTipoTorneos();
	@Transient
	private Integer maxGoles;
	
	@Column(nullable = true)
	private String rutaImagen;
	
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "SEDE_FK")
    private Sede sede;

	public Torneo() {
		setActivo(true);
		setIdaYVuelta(false);
		this.setEquipos(new ArrayList<Equipo>());
		this.setFechas(new ArrayList<Fecha>());
		setCantidadFechas(0);
		setFechaActual(1);
	}

	public Torneo(List<Equipo> equipos) {
		setActivo(true);
		setIdaYVuelta(false);
		this.setFechas(new ArrayList<Fecha>());
		this.setEquipos(equipos);
		calcularCantidadFechas();
		setFechaActual(1);
	}

	public void calcularFecha() {
		TipoTorneo tipoTorneoObj = fabricaTipoTorneos
				.getTipoTorneo(getTipoTorneo());
		tipoTorneoObj.calcularFechas(this);
	}

	public void calcularSiguienteFecha() {
		// TODO: sacar esto e inyectarlo con spring
		TipoTorneo tipoTorneoObj = fabricaTipoTorneos
				.getTipoTorneo(getTipoTorneo());
		tipoTorneoObj.calcularSiguienteFecha(this);
	}

	public boolean agregarEquipo(Equipo equipo) {
		calcularCantidadFechas();
		return this.equipos.add(equipo);
	}

	public boolean eliminarEquipo(Equipo equipo) {
		calcularCantidadFechas();
		return this.equipos.remove(equipo);
	}

	private void calcularCantidadFechas() {
		// TODO: sacar esto e inyectarlo con spring
		if (getTipoTorneo() != null && !getTipoTorneo().isEmpty()) {
			TipoTorneo tipoTorneoObj = fabricaTipoTorneos
					.getTipoTorneo(getTipoTorneo());
			int cant = tipoTorneoObj.calcularCantidadFechas(this);
			setCantidadFechas(cant);
		}
	}

	public void inicializarPosiciones() {
		// TODO: sacar esto e inyectarlo con spring
		TipoTorneo tipo = fabricaTipoTorneos.getTipoTorneo(getTipoTorneo());
		tipo.inicializarPosiciones(this);
	}

	public void acutalizarPosiciones() {
		TipoTorneo tipo = fabricaTipoTorneos.getTipoTorneo(getTipoTorneo());
		tipo.actualizarPosiciones(this);
	}

	public boolean estaIniciado() {
		return !this.getFechas().isEmpty();
	}

	public void calcularTablaGoleadores() {
		tablaGoleadores = new ArrayList<PosicionGoleador>();
		for (Equipo equipo : equipos) {
			for (Jugador jugador : equipo.getJugadores()) {
				PosicionGoleador pos = new PosicionGoleador(this, jugador);
				pos.calcularGolesJugador();
				if (pos.hizoAlgunGolEnTorneo() && jugador.getActivo()) {
					tablaGoleadores.add(pos);
				}
			}
		}
	}
	
	public void calcularTablaGoleadores(Equipo eq){
		tablaGoleadores = new ArrayList<PosicionGoleador>();
		for (Equipo equipo : equipos) {
			if(equipo.equals(eq)){
				for (Jugador jugador : equipo.getJugadores()) {
					PosicionGoleador pos = new PosicionGoleador(this, jugador);
					pos.calcularGolesJugador();
					if (pos.hizoAlgunGolEnTorneo()) {
						tablaGoleadores.add(pos);
					}
				}
				break;
			}
		}
		
	}
	
	public String getCantGoles(Jugador jugador, int numeroFecha) {
		PosicionGoleador pos = new PosicionGoleador(this, jugador);
		return pos.calcularGolesPorFecha(numeroFecha).toString();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Fecha> getFechas() {
		return fechas;
	}

	public void setFechas(List<Fecha> fechas) {
		this.fechas = fechas;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public void setEquipos(List<Equipo> equipos) {
		this.equipos = equipos;
		calcularCantidadFechas();
	}

	public List<Equipo> getEquipos() {
		return equipos;
	}

	public void setIdaYVuelta(Boolean idaYVuelta) {
		this.idaYVuelta = idaYVuelta;
	}

	public Boolean getIdaYVuelta() {
		return idaYVuelta;
	}

	public void setCantidadFechas(Integer cantidadFechas) {
		this.cantidadFechas = cantidadFechas;
	}

	public Integer getCantidadFechas() {
		return cantidadFechas;
	}

	public void setFechaActual(Integer fechaActual) {
		this.fechaActual = fechaActual;
	}

	public Integer getFechaActual() {
		return fechaActual;
	}

	public void setTipoTorneo(String tipoTorneo) {
		this.tipoTorneo = tipoTorneo;
	}

	public String getTipoTorneo() {
		return tipoTorneo;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}
	
	public void incrementarFechaActual() {
		++fechaActual;

	}

	public Integer getCantPuntosGanador() {
		if (cantPuntosGanador == null)
			return DEFAULT_CANT_PUNTOS_GANADOR;
		return cantPuntosGanador;
	}

	public void setCantPuntosGanador(Integer cantPuntosGanador) {
		if (cantPuntosGanador == null)
			this.cantPuntosGanador = DEFAULT_CANT_PUNTOS_GANADOR;
		else
			this.cantPuntosGanador = cantPuntosGanador;
	}

	public Integer getCantPuntosEmpate() {
		if (cantPuntosEmpate == null)
			return DEFAULT_CANT_PUNTOS_EMPATE;
		return cantPuntosEmpate;
	}

	public void setCantPuntosEmpate(Integer cantPuntosEmpate) {
		if (cantPuntosEmpate == null)
			this.cantPuntosEmpate = DEFAULT_CANT_PUNTOS_EMPATE;
		else
			this.cantPuntosEmpate = cantPuntosEmpate;
	}

	public Integer getCantPuntosPerdedor() {
		if (cantPuntosPerdedor == null)
			return DEFAULT_CANT_PUNTOS_PERDEDOR;
		return cantPuntosPerdedor;
	}

	public void setCantPuntosPerdedor(Integer cantPuntosPerdedor) {
		if (cantPuntosPerdedor == null)
			this.cantPuntosPerdedor = DEFAULT_CANT_PUNTOS_PERDEDOR;
		else
			this.cantPuntosPerdedor = cantPuntosPerdedor;
	}

	public Integer getCantFechasSancionAmarilla() {
		if (cantFechasSancionAmarilla == null)
			return DEFAULT_CANT_FECHAS_SANCION_AMARILLA;
		return cantFechasSancionAmarilla;
	}

	public void setCantFechasSancionAmarilla(Integer cantFechasSancionAmarilla) {
		if (cantFechasSancionAmarilla == null)
			this.cantFechasSancionAmarilla = DEFAULT_CANT_FECHAS_SANCION_AMARILLA;
		else
			this.cantFechasSancionAmarilla = cantFechasSancionAmarilla;
	}

	public Integer getCantFechasSancionRoja() {
		if (cantFechasSancionRoja == null)
			return DEFAULT_CANT_FECHAS_SANCION_ROJA;
		return cantFechasSancionRoja;
	}

	public void setCantFechasSancionRoja(Integer cantFechasSancionRoja) {
		if (cantFechasSancionRoja == null)
			this.cantFechasSancionRoja = DEFAULT_CANT_FECHAS_SANCION_ROJA;
		else
			this.cantFechasSancionRoja = cantFechasSancionRoja;
	}

	public Integer getCantTarjAmarillasParaSancion() {
		if (cantTarjAmarillasParaSancion == null)
			return DEFAULT_CANT_TARJ_AMA_SANCION;
		return cantTarjAmarillasParaSancion;
	}

	public void setCantTarjAmarillasParaSancion(
			Integer cantTarjAmarillasParaSancion) {
		if (cantTarjAmarillasParaSancion == null)
			this.cantTarjAmarillasParaSancion = DEFAULT_CANT_TARJ_AMA_SANCION;
		else
			this.cantTarjAmarillasParaSancion = cantTarjAmarillasParaSancion;
	}

	public Integer getCantTarjRojasParaSancion() {
		if (cantTarjRojasParaSancion == null)
			return DEFAULT_CANT_TARJ_ROJ_SANCION;
		return cantTarjRojasParaSancion;
	}

	public void setCantTarjRojasParaSancion(Integer cantTarjRojasParaSancion) {
		if (cantTarjRojasParaSancion == null)
			this.cantTarjRojasParaSancion = DEFAULT_CANT_TARJ_ROJ_SANCION;
		else
			this.cantTarjRojasParaSancion = cantTarjRojasParaSancion;
	}

	/**
	 * Se mantiene el valor default (5) en caso de que no se ingrese nada.
	 * 
	 * @param jugadoresPorEquipo
	 */
	public void setJugadoresPorEquipo(Integer jugadoresPorEquipo) {
		if (jugadoresPorEquipo == null)
			this.jugadoresPorEquipo = DEFAULT_CANT_JUGADORES_X_EQUIPO;
		else
			this.jugadoresPorEquipo = jugadoresPorEquipo;
	}

	/**
	 * Se Mantiene valor default (5) si no tiene nada asignado
	 * 
	 * @return cantidad de jugadores por equipo (default en caso nulo)
	 */
	public Integer getJugadoresPorEquipo() {
		if (jugadoresPorEquipo == null)
			return DEFAULT_CANT_JUGADORES_X_EQUIPO;
		else
			return jugadoresPorEquipo;
	}

	public void setPosiciones(List<PosicionTorneo> posiciones) {
		this.posiciones = posiciones;
	}

	public List<PosicionTorneo> getPosiciones() {
		// TODO: cambiar esto por un implements comparable
		Collections.sort(posiciones, new Comparator<PosicionTorneo>() {
			@Override
			public int compare(PosicionTorneo o1, PosicionTorneo o2) {
				int con = o2.getPuntos().compareTo(o1.getPuntos());
				if (con == 0){
					con = o2.getDiferencia().compareTo(o1.getDiferencia());
					if(con == 0){
						con = o2.getGolesAFavor().compareTo(o1.getGolesAFavor());
					}
				}
				return con;
			}
		});

		return posiciones;
	}

	public Fecha obtenerFechaActual() {
		return fechas.get(getFechaActual() - 1);
	}

	public void setTablaGoleadores(List<PosicionGoleador> tablaGoleadores) {
		this.tablaGoleadores = tablaGoleadores;
	}

	public List<PosicionGoleador> getTablaGoleadores() {
		// TODO: cambiar esto por un implements comparable
		Collections.sort(tablaGoleadores, new Comparator<PosicionGoleador>() {
			@Override
			public int compare(PosicionGoleador o1, PosicionGoleador o2) {
				int goles = o2.getGoles().compareTo(o1.getGoles());
				if(goles == 0){
					return o1.getJugador().toString().compareTo(o2.getJugador().toString());
				}
				return goles;
			}
		});
		return tablaGoleadores;
	}
	
	public Integer getPosicionEquipo(Equipo equipo){
		Integer pos = 1;
		for(PosicionTorneo posTor: getPosiciones()){
			if(posTor.getEquipo().equals(equipo)){
				break;
			}
			++pos;
			
		}
		return pos;
		
	}
	
	public Integer getPosicionEquipoTorneosAnteriores(Equipo equipo){
    	for(Torneo t: torneosAnteriores){
    		if(t.getEquipos().contains(equipo)){
    			return t.getPosicionEquipo(equipo);
    		}
    	}
    	return Integer.MAX_VALUE;
	}
	
	public Equipo getMejorPosicionado(Equipo equipo1, Equipo equipo2){
		
		Integer posicionEquipo1 = this.getPosicionEquipoTorneosAnteriores(equipo1);
		Integer posicionEquipo2 = this.getPosicionEquipoTorneosAnteriores(equipo2);
		logger.info(String.format("Equipo 1: %s. Posicion: %s. Equipo 2: %s .Posicion: %s.", equipo1, posicionEquipo1, equipo2, posicionEquipo2));
		if(posicionEquipo1 < posicionEquipo2){
			logger.info(String.format("Pasa Equipo 1: %s.", equipo1));
			return equipo1;
		}else{
			logger.info(String.format("Pasa Equipo 2: %s.", equipo2));
			return equipo2;
		}
		
	}
	
	public boolean isTodosContraTodos() {
		return "Todos contra Todos".equals(getTipoTorneo());
	}
	
	public Integer getMaxGoles(){
		return this.maxGoles;
	}

	public void calcularMaxGoles() {
		List<PosicionGoleador> goleadores = getTablaGoleadores();
		PosicionGoleador posicionGoleador = goleadores.get(0);
		this.maxGoles = posicionGoleador.getGoles();
		
	}
	
	public Integer getFechaActualMenosUno(){
		return this.fechaActual - 1;
	}

	/*public void setJugadoresTarjetas(List<Jugador> jugadoresTarjetas) {
		this.jugadoresTarjetas = jugadoresTarjetas;
	}
	
	public List<Jugador> getJugadoresTarjetas() {
		// TODO: cambiar esto por un implements comparable
		Collections.sort(jugadoresTarjetas, new Comparator<Jugador>() {
			@Override
			public int compare(Jugador o1, Jugador o2) {
				int goles = o2.getCantTarjetasTotal().compareTo(o1.getCantTarjetasTotal());
				if(goles == 0){
					return o1.toString().compareTo(o2.toString());
				}
				return goles;
			}
		});
		return jugadoresTarjetas;
	}*/
	
	public List<CantidadTarjetas> getJugadoresTarjetas() {
		return jugadoresTarjetas;
	}

	public void setJugadoresTarjetas(List<CantidadTarjetas> jugadoresTarjetas) {
		this.jugadoresTarjetas = jugadoresTarjetas;
	}
	
	public List<Sancion> getSanciones() {
		return sanciones;
	}

	public void setSanciones(List<Sancion> sanciones) {
		this.sanciones = sanciones;
	}
	
	@Override
	public String toString(){
		return nombre;
	}
	
	public String getRutaImagen() {
		return rutaImagen;
	}

	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}

	public List<Torneo> getTorneosAnteriores() {
		return torneosAnteriores;
	}

	public void setTorneosAnteriores(List<Torneo> torneosAnteriores) {
		this.torneosAnteriores = torneosAnteriores;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

}
