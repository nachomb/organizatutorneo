package ar.com.noxit.picadito.dominio.entidades.seguridad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

/**
 * Entidad que representa acciones que se asocian a los perfiles
 * 
 */
@Entity
@Table(name = "accion")
public class Accion extends EntidadGenerica {

    // Identificador de la accion
    @Id
    @Column(name = "ID_ACCION")
    @GeneratedValue
    private Long id;

    // Nombre de la accion
    @Column(name = "NOMBRE", nullable = false)
    private String nombre;

    // Entidad de la accion. Por ejemplo, si es de tipo page, sera:
    // /UnaAction*
    // El wildcard permite que se aplique seguridad a todo lo que comience con /UnaAction
    // Ejemplo: /UnaAction_list.action o /UnaAction_add.action, etc.
    @Column(name = "ENTIDAD", nullable = false)
    private String entidad;

    // Accion que se ejecuta al hacer click en el item de menu
    @Column(name = "MENU_ENTRADA", nullable = false)
    private String menuEntrada;

    // Identificador del tipo de entidad
    @ManyToOne
    @JoinColumn(name = "TIPO_ENTIDAD")
    private TipoEntidad tipoEntidad;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String toString() {
        return this.nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getMenuEntrada() {
        return menuEntrada;
    }

    public void setMenuEntrada(String menuEntrada) {
        this.menuEntrada = menuEntrada;
    }

    public TipoEntidad getTipoEntidad() {
        return tipoEntidad;
    }

    public void setTipoEntidad(TipoEntidad tipoEntidad) {
        this.tipoEntidad = tipoEntidad;
    }

}
