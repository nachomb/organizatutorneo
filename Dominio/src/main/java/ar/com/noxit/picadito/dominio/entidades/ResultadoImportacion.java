package ar.com.noxit.picadito.dominio.entidades;

public class ResultadoImportacion {

	private Integer cantJugadoresCreados;
	private Integer cantEquiposCreados;
	
	public ResultadoImportacion(int cantEquiposCreados,
			int cantJugadoresCreados) {
		this.cantEquiposCreados = cantEquiposCreados;
		this.cantJugadoresCreados = cantJugadoresCreados;
	}
	public Integer getCantJugadoresCreados() {
		return cantJugadoresCreados;
	}
	public void setCantJugadoresCreados(Integer cantJugadoresCreados) {
		this.cantJugadoresCreados = cantJugadoresCreados;
	}
	public Integer getCantEquiposCreados() {
		return cantEquiposCreados;
	}
	public void setCantEquiposCreados(Integer cantEquiposCreados) {
		this.cantEquiposCreados = cantEquiposCreados;
	}
	
	
}
