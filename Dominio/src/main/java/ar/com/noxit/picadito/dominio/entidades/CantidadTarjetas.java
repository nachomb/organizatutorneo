package ar.com.noxit.picadito.dominio.entidades;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;


@Entity
@Table(name = "cantidad_tarjetas")
public class CantidadTarjetas extends EntidadGenerica {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6970040668596518824L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CANTIDADTARJETAS")
    private Long id;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TORNEO_FK")
    private Torneo torneo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "JUGADOR_FK")
    private Jugador jugador;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EQUIPO_FK")
    private Equipo equipo;

	@Column(name = "TARJETAS_AMARILLAS")
    private Integer cantTarjetasAmarillas;
	
	@Column(name = "TARJETAS_ROJAS")
	private Integer cantTarjetasRojas;
    
	public Equipo getEquipo() {
		return equipo;
	}
	
	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}
	
    public Integer getCantTarjetasAmarillas() {
		return cantTarjetasAmarillas;
	}

	public void setCantTarjetasAmarillas(Integer cantTarjetasAmarillas) {
		this.cantTarjetasAmarillas = cantTarjetasAmarillas;
	}

	public Integer getCantTarjetasRojas() {
		return cantTarjetasRojas;
	}

	public void setCantTarjetasRojas(Integer cantTarjetasRojas) {
		this.cantTarjetasRojas = cantTarjetasRojas;
	}
    
    public Torneo getTorneo() {
		return torneo;
	}

	public void setTorneo(Torneo torneo) {
		this.torneo = torneo;
	}

	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }
    
	public Integer getCantTarjetasTotal(){
		return this.cantTarjetasAmarillas + this.cantTarjetasRojas;
	}

	

}
