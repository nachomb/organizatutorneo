package ar.com.noxit.picadito.dominio.entidades.seguridad;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.GrantedAuthorityImpl;
import org.acegisecurity.userdetails.UserDetails;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "usuario")
public class Usuario extends EntidadGenerica implements UserDetails {

    private static final long serialVersionUID = -2484763868213252925L;
    private static final String PROFILE_PREFIX = "PROFILE_";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_USUARIO")
    private Long id;

    @Column(name = "NOMBRE", nullable = false)
    private String username;

    @Column(name = "CLAVE", nullable = false)
    private String password;
    
    @Column(name = "TEL_CELULAR", nullable = true)
    private String telefonoCelular;

    @Column(name = "ACTIVO", nullable = true)
    private boolean activo;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    @JoinTable(name = "perfil_usuario", joinColumns = { @JoinColumn(name = "ID_USUARIO") }, inverseJoinColumns = { @JoinColumn(name = "ID_PERFIL", referencedColumnName = "ID_PERFIL") })
    private Collection<Perfil> perfiles;

    @Embedded
    private Auditoria auditoria = new Auditoria();

    /**
     * Constructor
     */
    public Usuario() {
        this.username = new String();
        this.password = new String();
        this.activo = true;
        this.perfiles = new ArrayList<Perfil>();
    }

    /**
     * Constructor
     */
    public Usuario(String username, String password, boolean activo, Collection<Perfil> perfiles) {
        this.username = username;
        this.password = password;
        this.activo = activo;
        this.perfiles = new ArrayList<Perfil>(perfiles);
    }

    /**
     * Constructor
     */
    public Usuario(String username, String password) {
        this.username = username;
        this.password = password;
        this.activo = true;
        this.perfiles = new ArrayList<Perfil>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public boolean isActivo() {
        return this.getActivo().booleanValue();
    }

    public Collection<Perfil> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(Collection<Perfil> perfiles) {
        this.perfiles = perfiles;
    }

    public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	/**
     * Se agrega el perfil a la lista de perfiles del usuario si es que no existe
     * 
     * @param perfil
     *            : perfir a ser agregado al usuario
     */
    public void addPerfil(Perfil perfil) {
        if (!this.hasPerfil(perfil))
            perfiles.add(perfil);
    }

    /**
     * Elimina el perfil del usuario si es que ste lo contiene
     * 
     * @param perfil
     *            : perfil a ser eliminado
     */
    public void removePerfil(Perfil perfil) {
        if (this.hasPerfil(perfil))
            perfiles.remove(perfil);
    }

    /**
     * Identifica si un usuario contiene cierto perfil
     * 
     * @param perfil
     *            : perfil a ser buscado
     * 
     * @return retorna true si el usuario contiene el perfil. False en caso contrario.
     */
    public boolean hasPerfil(Perfil perfil) {
        if (perfiles.contains(perfil))
            return true;
        return false;
    }

    /**
     * Metodo para cargar todos los perfiles del usuario
     */
    @Transient
    public GrantedAuthority[] getAuthorities() {
        int profilesSize = this.getPerfiles().size();
        int i = 0;

        // Carga los perfiles del usuario
        GrantedAuthority[] authorities = new GrantedAuthorityImpl[profilesSize];

        Iterator<Perfil> iterator = this.getPerfiles().iterator();

        while (iterator.hasNext()) {
            Perfil profile = iterator.next();

            authorities[i] = new GrantedAuthorityImpl(PROFILE_PREFIX + profile.getNombre());

            i++;
        }

        return authorities;
    }

    public boolean isAccountNonExpired() {
        return this.isEnabled();
    }

    public boolean isAccountNonLocked() {
        return this.isEnabled();
    }

    public boolean isCredentialsNonExpired() {
        return this.isEnabled();
    }

    public String toString() {
        return this.username;
    }

    public void setAuditoria(Auditoria auditoria) {
        this.auditoria = auditoria;
    }

    public Auditoria getAuditoria() {
        return auditoria;
    }

    @Override
    public boolean isEnabled() {
        return this.isActivo();
    }

    public boolean isAnonimo() {
        return false;
    }

}