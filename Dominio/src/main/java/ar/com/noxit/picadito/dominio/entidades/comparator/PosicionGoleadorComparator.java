package ar.com.noxit.picadito.dominio.entidades.comparator;

import java.util.Comparator;

import ar.com.noxit.picadito.dominio.entidades.PosicionGoleador;

public class PosicionGoleadorComparator implements Comparator<PosicionGoleador>{

	@Override
	public int compare(PosicionGoleador pg1, PosicionGoleador pg2) {
		return pg2.getGoles().compareTo(pg1.getGoles());
	}


}
