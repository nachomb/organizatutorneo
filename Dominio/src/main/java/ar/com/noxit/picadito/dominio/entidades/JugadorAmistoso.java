package ar.com.noxit.picadito.dominio.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class JugadorAmistoso implements Serializable {

    /**
     * Serial Version UID autogenerado
     */
    private static final long serialVersionUID = -8468078297483766711L;

    @Column(name = "ID_JUGADOR")
    private Long id;

    @Column(name = "NOMBRE_JUGADOR")
    private String nombre;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

}