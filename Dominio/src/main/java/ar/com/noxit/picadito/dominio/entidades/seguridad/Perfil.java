package ar.com.noxit.picadito.dominio.entidades.seguridad;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

/**
 * Entidad que representa perfiles de usuarios
 * 
 */

@Entity
@Table(name = "perfil")
public class Perfil extends EntidadGenerica {

    // Identificador del perfil
    @Id
    @Column(name = "ID_PERFIL")
    @GeneratedValue
    private Long id;

    // Nombre del perfil.
    @Column(name = "NOMBRE", nullable = false)
    private String nombre;

    // Descripcion del perfil
    @Column(name = "DESCRIPCION")
    private String descripcion;

    // Acciones que puede realizar un perfil
    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    @JoinTable(name = "perfil_accion", joinColumns = { @JoinColumn(name = "ID_PERFIL", referencedColumnName = "ID_PERFIL") }, inverseJoinColumns = { @JoinColumn(name = "ID_ACCION", referencedColumnName = "ID_ACCION") })
    private Collection<Accion> acciones;

    // Usuarios asociados al perfil
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    @JoinTable(name = "perfil_usuario", joinColumns = { @JoinColumn(name = "ID_PERFIL") }, inverseJoinColumns = { @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO") })
    private Collection<Usuario> usuarios;

    @Column(name = "ACTIVO", nullable = true)
    private boolean activo;

    @Embedded
    private Auditoria auditoria = new Auditoria();

    /**
     * Constructor
     */
    public Perfil() {
        this.nombre = new String();
        this.acciones = new ArrayList<Accion>();
        this.activo = true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Collection<Accion> getAcciones() {
        return acciones;
    }

    public void setAcciones(Collection<Accion> acciones) {
        this.acciones = acciones;
    }

    public Collection<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Collection<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    /**
     * Se agrega la accin a la lista de acciones si es que no la contiene
     * 
     * @param accion
     *            : accin a ser agregada al perfil
     */
    public void addAccion(Accion accion) {
        if (!this.hasAccion(accion))
            acciones.add(accion);
    }

    /**
     * Se elimina la accin de la lista de acciones si es que existe
     * 
     * @param accion
     */
    public void removeAccion(Accion accion) {
        if (this.hasAccion(accion))
            acciones.remove(accion);
    }

    /**
     * Identifica si una accin es contenida por un perfil
     * 
     * @param accion
     *            : accin a ser buscada
     * @return retorna true si que la accin es contenida por el perfil. False en caso contrario
     */
    public boolean hasAccion(Accion accion) {
        if (acciones.contains(accion))
            return true;
        return false;
    }

    public String toString() {
        return this.nombre;
    }

    public void setAuditoria(Auditoria auditoria) {
        this.auditoria = auditoria;
    }

    public Auditoria getAuditoria() {
        return auditoria;
    }

}
