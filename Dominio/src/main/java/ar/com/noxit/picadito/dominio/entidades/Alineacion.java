package ar.com.noxit.picadito.dominio.entidades;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;


@Entity
@Table(name = "alineacion")
public class Alineacion extends EntidadGenerica {

    /**
     * Serial Version UID autogenerado
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ALINEACION")
    private Long id;

    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH }, fetch = FetchType.LAZY)
    @JoinColumn(name = "PARTIDO")
    private Partido partido;
    
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH },fetch = FetchType.LAZY)
    @JoinTable(name = "alineacion_titulares", joinColumns = { @JoinColumn(name = "ID_ALINEACION") }, inverseJoinColumns = { @JoinColumn(name = "ID_JUGADOR", referencedColumnName = "ID_JUGADOR") })
    private Collection<Jugador> titulares;
    
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH },fetch = FetchType.LAZY)
    @JoinTable(name = "alineacion_suplentes", joinColumns = { @JoinColumn(name = "ID_ALINEACION") }, inverseJoinColumns = { @JoinColumn(name = "ID_JUGADOR", referencedColumnName = "ID_JUGADOR") })
    private Collection<Jugador> suplentes;

    public Alineacion() {
        titulares = new ArrayList<Jugador>();
        suplentes = new ArrayList<Jugador>();
    }

    public Alineacion(Partido partido) {
        this.partido = partido;
        titulares = new ArrayList<Jugador>();
        suplentes = new ArrayList<Jugador>();
    }

    public Alineacion(Partido partido, Equipo equipo, Fecha fecha) {
        this.partido = partido;
        setAlineacion(equipo, fecha);
    }

    public Alineacion(Partido partido, List<Jugador> jugadoresTitulares, List<Jugador> jugadoresSuplentes) {
        this.partido = partido;
        this.titulares = jugadoresTitulares;
        this.suplentes = jugadoresSuplentes;
    }

    public void setAlineacion(Equipo equipo, Fecha fecha) {
        Torneo torneo = fecha.getTorneo();
        titulares = new ArrayList<Jugador>();
        suplentes = new ArrayList<Jugador>();
        if (equipo != null) {
            setAlineacionEquipo(equipo, torneo);
        }
    }

    private void setAlineacionEquipo(Equipo equipo, Torneo torneo) {
        List<Jugador> jugadoresTitulares = equipo.getJugadoresTitulares(torneo);
        List<Jugador> jugadoresSuplentes = equipo.getJugadoresSuplentes(torneo);
        for (Jugador jugador : jugadoresTitulares) {
            titulares.add(jugador);
        }
        for (Jugador jugador : jugadoresSuplentes) {
            suplentes.add(jugador);
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }

    public Partido getPartido() {
        return partido;
    }

    public void setTitulares(List<Jugador> titulares) {
        this.titulares = titulares;
    }

    public Collection<Jugador> getTitulares() {
        return titulares;
    }

    public void setSuplentes(List<Jugador> suplentes) {
        this.suplentes = suplentes;
    }

    public Collection<Jugador> getSuplentes() {
        return suplentes;
    }

    public void recalcularAlineacion(List<Jugador> titulares, List<Jugador> suplentes) {
        if (this.titulares == null)
            this.titulares = titulares;
        else {
            this.titulares.clear();
            this.titulares.addAll(titulares);
        }
        if (this.suplentes == null)
            this.suplentes = suplentes;
        else {
            this.suplentes.clear();
            this.suplentes.addAll(suplentes);
        }

    }

    public void recalcularAlineacion(Equipo equipo, Fecha fecha) {
        if (equipo != null) {
            if (titulares == null && suplentes == null) {
                setAlineacion(equipo, fecha);
            } else if (titulares == null) {
                titulares = new ArrayList<Jugador>();
                suplentes.clear();
                setAlineacionEquipo(equipo, fecha.getTorneo());
            } else {
                suplentes = new ArrayList<Jugador>();
                titulares.clear();
                setAlineacionEquipo(equipo, fecha.getTorneo());
            }
        }
    }

}
