package ar.com.noxit.picadito.dominio.entidades;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

@Entity
@Table(name = "equipo")
public class Equipo extends EntidadGenerica {

    /**
     * Serial Version UID autegenerado
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EQUIPO")
    private Long id;

    @Column(name = "NOMBRE", unique = true)
    private String nombre;

    @Column(name = "ACTIVO", nullable = true)
    private boolean activo;

    @OneToMany(mappedBy = "equipo", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<Jugador> jugadores;

    @Column(name = "RUTA_IMG_ESCUDO", nullable = true)
    private String rutaImgEscudo;
    
    @OneToOne
    @JoinColumn(name = "USUARIO_CREADOR_FK")
    private Usuario creador;

    @Transient
    private DatoReporte datoReporte;
    
    public DatoReporte getDatoReporte() {
		return datoReporte;
	}

	public void setDatoReporte(DatoReporte datoReporte) {
		this.datoReporte = datoReporte;
	}

	public Equipo() {
        setActivo(true);
    }

    public Equipo(String nombre) {
        setNombre(nombre);
    }

    @Override
    public String toString() {
        return getNombre();
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setJugadores(Collection<Jugador> jugadores) {
        this.jugadores = jugadores;
    }

    public Collection<Jugador> getJugadores() {
        return jugadores;
    }

    public void setRutaImgEscudo(String rutaImgEscudo) {
        this.rutaImgEscudo = rutaImgEscudo;
    }

    public String getRutaImgEscudo() {
        return rutaImgEscudo;
    }

    public boolean tieneCantidadMinimaJugadores(Integer jugadoresPorEquipo) {
        return (jugadores.size() >= jugadoresPorEquipo.intValue());
    }

    public List<Jugador> getJugadoresTitulares(Torneo torneo) {
        List<Jugador> titulares = new ArrayList<Jugador>();
        for (Jugador jug : jugadores) {
            if (esTitular(jug, torneo)) {
                titulares.add(jug);
            }
        }
        return titulares;
    }

    public List<Jugador> getJugadoresSuplentes(Torneo torneo) {
        List<Jugador> suplentes = new ArrayList<Jugador>();
        for (Jugador jug : jugadores) {
            if (!esTitular(jug, torneo)) {
                suplentes.add(jug);
            }
        }
        return suplentes;
    }

    private boolean esTitular(Jugador jugador, Torneo torneo) {
        int jugPorEq = torneo.getJugadoresPorEquipo().intValue();
        if (jugadores.size() < jugPorEq) {
            return false;
        } else {
            int i = 0;
            for (Jugador jug : jugadores) {
                if (jug.equals(jugador) && i < jugPorEq) {
                    return true;
                }
                ++i;
            }
            return false;
        }
    }

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

}
