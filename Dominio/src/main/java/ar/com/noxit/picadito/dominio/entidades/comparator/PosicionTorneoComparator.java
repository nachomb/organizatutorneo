package ar.com.noxit.picadito.dominio.entidades.comparator;

import java.util.Comparator;

import ar.com.noxit.picadito.dominio.entidades.PosicionTorneo;

public class PosicionTorneoComparator implements Comparator<PosicionTorneo> {

	@Override
	public int compare(PosicionTorneo p1, PosicionTorneo p2) {
		int compare = p2.getPuntos().compareTo(p1.getPuntos());
		if(compare == 0){
			compare = p2.getDiferencia().compareTo(p1.getDiferencia());
		}
		if(compare == 0){
			compare = p2.getGolesAFavor().compareTo(p1.getGolesAFavor());
		}
		return compare;
	}

}
