package ar.com.noxit.picadito.dominio.entidades;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Accion;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Auditoria;

@Entity
@Table(name = "item_menu")
public class ItemMenu extends EntidadGenerica {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_ITEM_MENU")
    @GeneratedValue
    private Long id;

    @Column(name = "TITULO")
    private String titulo;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @ManyToOne
    @JoinColumn(name = "ACCION")
    private Accion accion;

    @ManyToOne
    @JoinColumn(name = "ITEM_MENU_PADRE")
    private ItemMenu itemMenuPadre;

    @Column(name = "ORDEN")
    private Long orden;

    @Embedded
    private Auditoria auditoria;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setAccion(Accion accion) {
        this.accion = accion;
    }

    public Accion getAccion() {
        return accion;
    }

    public void setItemMenuPadre(ItemMenu itemMenuPadre) {
        this.itemMenuPadre = itemMenuPadre;
    }

    public ItemMenu getItemMenuPadre() {
        return itemMenuPadre;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setOrden(Long orden) {
        this.orden = orden;
    }

    public Long getOrden() {
        return orden;
    }

    public void setAuditoria(Auditoria auditoria) {
        this.auditoria = auditoria;
    }

    public Auditoria getAuditoria() {
        return auditoria;
    }

}
