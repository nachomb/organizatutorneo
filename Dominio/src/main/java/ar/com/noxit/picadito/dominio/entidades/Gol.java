package ar.com.noxit.picadito.dominio.entidades;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "gol")
public class Gol extends EntidadGenerica {

    /**
     * Serial version UID autogenerado
     */
    private static final long serialVersionUID = -1463662221221271407L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_GOL")
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "JUGADOR_FK")
    private Jugador goleador;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "PARTIDO_FK")
    private Partido partido;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "EQUIPO_FK")
    private Equipo equipo;

    public Gol() {
	}

	public Gol(Jugador goleador, Partido partido, Equipo equipo) {
        this.goleador = goleador;
        this.partido = partido;
        this.equipo = equipo;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setGoleador(Jugador goleador) {
        this.goleador = goleador;
    }

    public Jugador getGoleador() {
        return goleador;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }

    public Partido getPartido() {
        return partido;
    }

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	public Equipo getEquipo() {
		return equipo;
	}

}
