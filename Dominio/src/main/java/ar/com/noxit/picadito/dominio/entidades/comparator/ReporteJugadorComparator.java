package ar.com.noxit.picadito.dominio.entidades.comparator;

import java.util.Comparator;

import ar.com.noxit.picadito.dominio.entidades.ReporteJugador;

public class ReporteJugadorComparator implements Comparator<ReporteJugador> {

	@Override
	public int compare(ReporteJugador rp1, ReporteJugador rp2) {
		if(!rp1.getJugador().getActivo()){
			return 1;
		}else if(!rp2.getJugador().getActivo()){
			return -1;
		}
		return rp2.getCantPartidos().compareTo(rp1.getCantPartidos());
	}

}
