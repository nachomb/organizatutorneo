package ar.com.noxit.picadito.dominio.entidades;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "tarjeta")
public abstract class Tarjeta extends EntidadGenerica {

    /**
     * Serial version UID autogenerado
     */
    private static final long serialVersionUID = -1463662221221271407L;

    public static final String TIPO_AMARILLA = "AMARILLA";
    public static final String TIPO_ROJA = "ROJA";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TARJETA")
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "JUGADOR_FK")
    private Jugador jugador;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "PARTIDO_FK")
    private Partido partido;

    @Column(name = "TIPO")
    private String tipo;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "EQUIPO_FK")
    private Equipo equipo;
    
    public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	public Tarjeta() {
    }

    public Tarjeta(String tipo) {
        this.tipo = tipo;
    }

    public Tarjeta(Jugador jugador, Partido partido, Equipo equipo) {
        this.jugador = jugador;
        this.partido = partido;
        this.equipo = equipo;
    }

    public Tarjeta(Jugador jugador, Partido partido, String tipo, Equipo equipo) {
        this.jugador = jugador;
        this.partido = partido;
        this.tipo = tipo;
        this.equipo = equipo;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setJugador(Jugador goleador) {
        this.jugador = goleador;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }

    public Partido getPartido() {
        return partido;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

}
