package ar.com.noxit.picadito.dominio.entidades.generico.entities;

import java.io.Serializable;

public interface IPersistible<ID extends Serializable> extends Serializable {

    public ID getId();

    public void setId(ID id);

}
