package ar.com.noxit.picadito.dominio.entidades;

public interface TipoTorneo {
	
	public void inicializarPosiciones(Torneo torneo);

    public void calcularFechas(Torneo torneo);

    public void calcularSiguienteFecha(Torneo torneo);

    public int calcularCantidadFechas(Torneo torneo);

	public void actualizarPosiciones(Torneo torneo);

}
