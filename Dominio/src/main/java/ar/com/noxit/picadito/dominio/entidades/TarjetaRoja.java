package ar.com.noxit.picadito.dominio.entidades;

import javax.persistence.Entity;

@Entity
public class TarjetaRoja extends Tarjeta {

    /**
     * Serial Version UID autogenerado
     */
    private static final long serialVersionUID = -6652297884460874553L;

    public TarjetaRoja() {
        super(Tarjeta.TIPO_ROJA);
    }

    public TarjetaRoja(Jugador jugador, Partido partido, Equipo equipo) {
        super(jugador, partido, Tarjeta.TIPO_ROJA, equipo);
    }

}
