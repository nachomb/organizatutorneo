package ar.com.noxit.picadito.dominio.entidades;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "grupo")
public class Grupo extends EntidadGenerica {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1939009445105299562L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_GRUPO")
	private Long id;
    
	@Column
	private String nombre;
	
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    @JoinTable(name = "grupo_jugador", joinColumns = { @JoinColumn(name = "ID_GRUPO") }, inverseJoinColumns = { @JoinColumn(name = "ID_JUGADOR", referencedColumnName = "ID_JUGADOR") })
    private Collection<Jugador> jugadores;
    
    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "JUGADOR_CREADOR_FK")
    private Jugador creador;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setJugadores(Collection<Jugador> jugadores) {
		this.jugadores = jugadores;
	}

	public Collection<Jugador> getJugadores() {
		return jugadores;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	@Override
	public String toString() {
		return nombre;
	}

	public void setCreador(Jugador creador) {
		this.creador = creador;
	}

	public Jugador getCreador() {
		return creador;
	}


	
	
}
