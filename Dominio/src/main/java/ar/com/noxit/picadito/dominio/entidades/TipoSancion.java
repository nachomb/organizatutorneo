package ar.com.noxit.picadito.dominio.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "tipo_sancion")
public class TipoSancion extends EntidadGenerica {

    /**
     * Serial Version UID autogenerado
     */
    private static final long serialVersionUID = -2137000766644583839L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TIPO_SANCION")
    private Long id;

    @Column(name = "DURACION")
    private Long duracionSancion;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "CANT_AMARILLAS")
    private Integer cantidadAmarillas;

    @Column(name = "CANT_ROJAS")
    private Integer cantidadRojas;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getDuracionSancion() {
        return duracionSancion;
    }

    public void setDuracionSancion(Long duracionSancion) {
        this.duracionSancion = duracionSancion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setCantidadAmarillas(Integer cantidadAmarillas) {
        this.cantidadAmarillas = cantidadAmarillas;
    }

    public Integer getCantidadAmarillas() {
        return cantidadAmarillas;
    }

    public void setCantidadRojas(Integer cantidadRojas) {
        this.cantidadRojas = cantidadRojas;
    }

    public Integer getCantidadRojas() {
        return cantidadRojas;
    }

}
