package ar.com.noxit.picadito.dominio.entidades;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "fecha")
public class Fecha extends EntidadGenerica implements Comparable<Fecha> {

    class FechaEntry implements Entry<Equipo, Integer> {

        private Equipo key;
        private Integer value;

        public FechaEntry(Equipo key, Integer value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public Equipo getKey() {
            return key;
        }

        @Override
        public Integer getValue() {
            return value;
        }

        @Override
        public Integer setValue(Integer value) {
            Integer last = this.value;
            this.value = value;
            return last;
        }

    }

    /**
     * Serial version autogenerado
     */
    private static final long serialVersionUID = -1203607890941871024L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_FECHA")
    private Long id;

    @Column
    private String nombre;

    @Column
    private Integer numero;

    @Column
    private Boolean isVuelta;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "TORNEO_FK")
    private Torneo torneo;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "fecha")
    private List<Partido> partidos;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="EQUIPO_LIBRE")
    private Equipo equipoLibre;

    @Column
    private Boolean activo;

    public Fecha() {
        this.setActivo(true);
        this.setIsVuelta(false);
        partidos = new ArrayList<Partido>();
    }

    public Fecha(Integer numero) {
        this.numero = numero;
        this.setActivo(true);
        this.setIsVuelta(false);
        partidos = new ArrayList<Partido>();
    }

    public Fecha(Integer numero, Torneo torneo) {
        this.numero = numero;
        this.torneo = torneo;
        this.setIsVuelta(false);
        this.setActivo(true);
        partidos = new ArrayList<Partido>();
    }

    public Fecha(Integer numero, Torneo torneo, Boolean isVuelta) {
        this.numero = numero;
        this.torneo = torneo;
        this.setIsVuelta(isVuelta);
        this.setActivo(true);
        partidos = new ArrayList<Partido>();
    }

   /* public void calcularFecha(List<Equipo> equipos) {
        int cantEquipos = equipos.size();
        int cantidadPartidos = (int) Math.floor(cantEquipos / 2);
        for (int i = 0; i <= cantidadPartidos; i += 2) {
            Partido partido = new Partido(equipos.get(i), equipos.get(i + 1), this);
            partidos.add(partido);
        }

    }*/

    public boolean isJugada() {
        Iterator<Partido> it = partidos.iterator();
        boolean jugada = true;
        while (it.hasNext() && jugada) {
            jugada = jugada && it.next().getJugado();
        }
        return jugada;
    }

    public List<Equipo> obtenerGanadoresFecha() {
        List<Equipo> ganadores = new ArrayList<Equipo>();
        Iterator<Partido> it = partidos.iterator();
        while (it.hasNext()) {
            ganadores.add(it.next().ganadorPartido());
        }
        return ganadores;
    }

    public List<Entry<Equipo, Integer>> obtenerGanadoresYDiferenciaGolFecha() {
        List<Entry<Equipo, Integer>> ganadores = new ArrayList<Entry<Equipo, Integer>>();
        Iterator<Partido> it = partidos.iterator();
        while (it.hasNext()) {
            Partido next = it.next();
            ganadores.add(new FechaEntry(next.ganadorPartido(), next.getDiferenciaGol()));
        }
        return ganadores;
    }

    public List<Equipo> obtenerEquiposVuelta() {
        List<Equipo> equipos = new ArrayList<Equipo>();
        Iterator<Partido> it = partidos.iterator();
        while (it.hasNext()) {
            Partido part = it.next();
            equipos.add(part.getEquipoVisitante());
            equipos.add(part.getEquipoLocal());
        }
        return equipos;
    }

    @Override
    public int compareTo(Fecha otraFecha) {
        return this.getNumero().compareTo(otraFecha.getNumero());
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setPartidos(List<Partido> partidos) {
        this.partidos = partidos;
    }

    public List<Partido> getPartidos() {
        return partidos;
    }

    public void setTorneo(Torneo torneo) {
        this.torneo = torneo;
    }

    public Torneo getTorneo() {
        return torneo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setIsVuelta(Boolean isVuelta) {
        this.isVuelta = isVuelta;
    }

    public Boolean getIsVuelta() {
        return isVuelta;
    }
    
    public String toString(){
		String cadena = getNombre()+ "\n";
		
		for(Partido p:getPartidos()){
			cadena += p.toString() + "\n"; 
		}
		if(equipoLibre != null){
			cadena += "libre" + equipoLibre.getNombre();
		}
    	return cadena;
    	
    }

	public void setEquipoLibre(Equipo equipoLibre) {
		this.equipoLibre = equipoLibre;
	}

	public Equipo getEquipoLibre() {
		return equipoLibre;
	}
	
	public String getFechaYTorneo(){
		return nombre + " " + torneo.getNombre();
	}

}
