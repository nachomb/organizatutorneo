package ar.com.noxit.picadito.dominio.entidades.comparator;

import java.util.Comparator;

import ar.com.noxit.picadito.dominio.entidades.Partido;

public class PartidoComparator implements Comparator<Partido>{

	@Override
	public int compare(Partido p1, Partido p2) {
		if(p1.getFechaPartido() == null || p2.getFechaPartido() == null)
			return 0;
		
		return p1.getFechaPartido().compareTo(p2.getFechaPartido());
	}

}
