package ar.com.noxit.picadito.dominio.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name="resumen")
public class Resumen extends EntidadGenerica {

	private static final long serialVersionUID = -8131009500593158712L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
    @Column
	private String nombre;
	
    @Column
	private String link;
    
    @Column
    private String tapa;

	@Column
    private Boolean activo;
	
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
	
    
    public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getTapa() {
		return tapa;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setTapa(String tapa) {
		this.tapa = tapa;
	}
	
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

}
