package ar.com.noxit.picadito.dominio.entidades;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TodosContraTodos implements TipoTorneo {

	public void calcularFechas(Torneo torneo) {
		List<Fecha> fechas = new ArrayList<Fecha>();
		List<Equipo> equipos = torneo.getEquipos();
		boolean hayEquipoLibre = false;
		if (equipos.size() % 2 != 0) {
			equipos.add(null);
			hayEquipoLibre = true;
		}
		int cantEquipos = equipos.size();
		int cantFechas = cantEquipos - 1;
		int cantPartidosPorFecha = cantEquipos / 2;

		Collections.shuffle(equipos);

		for (int i = 0; i < cantFechas; ++i) {
			Fecha fechaActual = new Fecha(i + 1, torneo);
			fechaActual.setNombre("Fecha " + (i + 1));

			for (int j = 0; j < cantPartidosPorFecha; ++j) {
				Partido partido = new Partido(equipos.get(j),
						equipos.get(cantFechas - j), fechaActual);
				fechaActual.getPartidos().add(partido);
			}
			fechas.add(fechaActual);
			equipos.add(1, equipos.remove(cantEquipos - 1));
			if (i % 2 == 0)
				alternarEquipos(fechaActual);
		}

		if (torneo.getIdaYVuelta()) {
			for (int i = cantFechas; i < cantFechas * 2; ++i) {
				Fecha fechaActual = new Fecha(i + 1, torneo);
				fechaActual.setNombre("Fecha " + (i + 1));
				Fecha fechaIda = fechas.get(i - cantFechas);
				for (Partido p : fechaIda.getPartidos()) {
					Partido partido = new Partido(p.getEquipoVisitante(),
							p.getEquipoLocal(), fechaActual);
					fechaActual.getPartidos().add(partido);
				}
				fechas.add(fechaActual);
			}
		}

		if (hayEquipoLibre){
			corregirFechas(fechas);
			equipos.remove(null);
		}

		torneo.setFechas(fechas);
	}

	private void corregirFechas(List<Fecha> fechas) {
		Equipo equipoLibre;
		for (Fecha f : fechas) {
			List<Partido> partidos = f.getPartidos();
			equipoLibre = null;
			for (Partido p : partidos) {
				if (p.getEquipoLocal() == null) {
					equipoLibre = p.getEquipoVisitante();
				} else if (p.getEquipoVisitante() == null) {
					equipoLibre = p.getEquipoLocal();
				}
				if (equipoLibre != null) {
					partidos.remove(p);
					break;
				}
			}
			f.setEquipoLibre(equipoLibre);
		}

	}

	private void alternarEquipos(Fecha fecha) {
		for (Partido p : fecha.getPartidos()) {
			Equipo equipoLocal = p.getEquipoLocal();
			p.setEquipoLocal(p.getEquipoVisitante());
			p.setEquipoVisitante(equipoLocal);
		}
	}

	@Override
	public void calcularSiguienteFecha(Torneo torneo) {
		Fecha fechaActual = torneo.obtenerFechaActual();
		int size = torneo.getFechas().size();
		if(fechaActual.isJugada() && fechaActual.getNumero() < size)
			torneo.incrementarFechaActual();
	}

	@Override
	public int calcularCantidadFechas(Torneo torneo) {
		int cantEquipos = torneo.getEquipos().size();
		int cantFechas = cantEquipos - 1;
		cantFechas = torneo.getIdaYVuelta() ? cantFechas * 2 : cantFechas;
		return cantFechas;
	}

	@Override
	public void inicializarPosiciones(Torneo torneo) {
		List<PosicionTorneo> posic = new ArrayList<PosicionTorneo>();
		for (Equipo equipo : torneo.getEquipos()) {
			PosicionTorneo posicionTorneo = new PosicionTorneo(torneo, equipo);
			posic.add(posicionTorneo);

		}
		torneo.setPosiciones(posic);

	}

	@Override
	public void actualizarPosiciones(Torneo torneo) {
		Fecha fechaActual = torneo.obtenerFechaActual();
		List<Partido> partidos = fechaActual.getPartidos();
		for (Partido partido : partidos) {
			Equipo ganador = partido.getGanador();
			if(ganador != null){
				PosicionTorneo posicionTorneoGanador = buscarPosicion(torneo, ganador);
				posicionTorneoGanador.incrementarGanados();
				Equipo perdedor = partido.getPerdedor();
				PosicionTorneo posicionTorneoPerdedor = buscarPosicion(torneo, perdedor);
				posicionTorneoPerdedor.incrementarPerdidos();
			}else{
				/*empataron*/
				PosicionTorneo posicionTorneoLocal = buscarPosicion(torneo, partido.getEquipoLocal());
				PosicionTorneo posicionTorneoVisitante = buscarPosicion(torneo, partido.getEquipoVisitante());
				posicionTorneoLocal.incrementarEmpatados();
				posicionTorneoVisitante.incrementarEmpatados();
			}
		}

	}

	private PosicionTorneo buscarPosicion(Torneo torneo, Equipo equipo) {
		PosicionTorneo res = null;
		for(PosicionTorneo pos : torneo.getPosiciones()){
			if(pos.getEquipo().getId().equals(equipo.getId())){
				res = pos;
				break;
			}
		}
		return res;
		
	}
}
