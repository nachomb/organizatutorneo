package ar.com.noxit.picadito.dominio.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "publicidad")
public class Publicidad extends EntidadGenerica {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4555465670554263989L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PUBLICIDAD")
	Long id;
	
	@Column
	private String nombre;
	
	@Column
	private String imagenVertical;
	
	@Column
	private String imagenSlider;
	
	@Column
	private Boolean activoVertical;

	@Column
	private Boolean activoSlider;

	@Column
	private String link;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getImagenVertical() {
		return imagenVertical;
	}

	public void setImagenVertical(String imagenVertical) {
		this.imagenVertical = imagenVertical;
	}

	public String getImagenSlider() {
		return imagenSlider;
	}

	public void setImagenSlider(String imagenSlider) {
		this.imagenSlider = imagenSlider;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	
	public Boolean getActivoVertical() {
		return activoVertical;
	}

	public void setActivoVertical(Boolean activoVertical) {
		this.activoVertical = activoVertical;
	}
	
	public Boolean getActivoSlider() {
		return activoSlider;
	}

	public void setActivoSlider(Boolean activoSlider) {
		this.activoSlider = activoSlider;
	}

}
