package ar.com.noxit.picadito.dominio.entidades.seguridad;

public class UsuarioAnonimo extends Usuario {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static UsuarioAnonimo instanciaUnica = new UsuarioAnonimo();

    private UsuarioAnonimo() {
        super("Anónimo", "anonymousUser");
    }

    public static UsuarioAnonimo getInstancia() {
        return instanciaUnica;
    }

    @Override
    public boolean isAnonimo() {
        return true;
    }
}
