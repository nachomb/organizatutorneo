package ar.com.noxit.picadito.dominio.entidades.generico.entities;

import ar.com.noxit.picadito.dominio.entidades.generico.IEntidad;

public abstract class EntidadGenerica implements IEntidad {

    public boolean equals(IEntidad obj) {
        if (obj != null) {
            if (this.getId() == null || obj.getId() == null)
                return false;
            return this.getId().equals(obj.getId());
        }
        return false;
    }
}
