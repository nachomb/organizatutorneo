package ar.com.noxit.picadito.dominio.entidades;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "posicion_goleador")
public class PosicionGoleador extends EntidadGenerica {

    /**
     * Serial Version UID autogenerado
     */
    private static final long serialVersionUID = 2638854619614033000L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_POSICIONGOLEADOR")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TORNEO_FK")
    private Torneo torneo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "JUGADOR_FK")
    private Jugador jugador;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EQUIPO_FK")
    private Equipo equipo;

	@Column(name = "GOLES")
    private Integer goles;

	@Transient
	private Integer posicion;

	public PosicionGoleador() {
        goles = 0;
    }

    public PosicionGoleador(Torneo torneo, Jugador jugador) {
        this.torneo = torneo;
        this.jugador = jugador;
        this.goles = 0;
    }

    public void calcularGolesJugador() {
        List<Gol> gls = jugador.getGoles();
        for (final Gol gol : gls) {
            if (gol.getPartido().getFecha().getTorneo().equals(torneo)) {
                goles += 1;
            }
        }
    }
    
    public Integer calcularGolesPorFecha(int numeroFecha){
        List<Gol> gls = jugador.getGoles();
        for (final Gol gol : gls) {
            if (gol.getPartido().getFecha().getTorneo().equals(torneo) &&  gol.getPartido().getFecha().getNumero() <= numeroFecha) {
                goles += 1;
            }
        }
        return goles;
    }

    public boolean hizoAlgunGolEnTorneo() {
        return goles != null && !goles.equals(new Integer(0));
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public void setTorneo(Torneo torneo) {
        this.torneo = torneo;
    }

    public Torneo getTorneo() {
        return torneo;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setGoles(Integer goles) {
        this.goles = goles;
    }

    public Integer getGoles() {
        return goles;
    }

    public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}
	
	public Integer getPosicion() {
		return posicion;
	}
	
	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}

}
