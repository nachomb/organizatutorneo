package ar.com.noxit.picadito.dominio.entidades;


public class DatoReporte {

	private Integer golesAFavor;
    private Integer golesEnContra;
    private Integer tarjetasAmarillas;
    private Integer tarjetasRojas;
    private Integer porcentaje;
    
	public Integer getGolesAFavor() {
		return golesAFavor;
	}
	public void setGolesAFavor(Integer golesAFavor) {
		this.golesAFavor = golesAFavor;
	}
	public Integer getGolesEnContra() {
		return golesEnContra;
	}
	public void setGolesEnContra(Integer golesEnContra) {
		this.golesEnContra = golesEnContra;
	}
	public Integer getTarjetasAmarillas() {
		return tarjetasAmarillas;
	}
	public void setTarjetasAmarillas(Integer tarjetasAmarillas) {
		this.tarjetasAmarillas = tarjetasAmarillas;
	}
	public Integer getTarjetasRojas() {
		return tarjetasRojas;
	}
	public void setTarjetasRojas(Integer tarjetasRojas) {
		this.tarjetasRojas = tarjetasRojas;
	}
	public Integer getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Integer porcentaje) {
		this.porcentaje = porcentaje;
	}
}
