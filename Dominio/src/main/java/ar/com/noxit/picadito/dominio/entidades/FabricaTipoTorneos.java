package ar.com.noxit.picadito.dominio.entidades;

import java.util.HashMap;
import java.util.Map;

public class FabricaTipoTorneos implements IFabricaTipoTorneos{

	private Map<String,TipoTorneo> mapTipoTorneos;
	
	public FabricaTipoTorneos(){
		mapTipoTorneos = new HashMap<String,TipoTorneo>();
		mapTipoTorneos.put("Todos contra Todos" ,new TodosContraTodos());
		mapTipoTorneos.put("Playoffs" ,new Playoffs());
	}
	
	@Override
	public TipoTorneo getTipoTorneo(String nombre){
		return mapTipoTorneos.get(nombre);
	}
	
}
