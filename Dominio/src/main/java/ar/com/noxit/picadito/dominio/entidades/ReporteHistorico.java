package ar.com.noxit.picadito.dominio.entidades;

import java.util.List;

public class ReporteHistorico {

	private List<PosicionTorneo> posiciones;
	private List<ReporteJugador> reporteJugadores;
	private Equipo equipo;
	
	public Equipo getEquipo() {
		return equipo;
	}
	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}
	public List<ReporteJugador> getReporteJugadores() {
		return reporteJugadores;
	}
	public void setReporteJugadores(List<ReporteJugador> reporteJugadores) {
		this.reporteJugadores = reporteJugadores;
	}
	
	public List<PosicionTorneo> getPosiciones() {
		return posiciones;
	}
	public void setPosiciones(List<PosicionTorneo> posiciones) {
		this.posiciones = posiciones;
	}
	
}
