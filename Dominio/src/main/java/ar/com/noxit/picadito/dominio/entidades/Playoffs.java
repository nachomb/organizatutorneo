package ar.com.noxit.picadito.dominio.entidades;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import org.apache.log4j.Logger;

public class Playoffs implements TipoTorneo {

    private static final double BASE = 2;

    private static final String IDA = "Ida";

    private static final String VUELTA = "Vuelta";

    private static final String INSTANCIA_BASE = " vos de final";
    
    private static final String[] instancias = {"Final","Semifinales","Cuartos de final","Octavos de final"};
    
    private static Logger logger = Logger.getLogger(Playoffs.class);

    @Override
    public void calcularFechas(Torneo torneo) {

        Fecha fecha = new Fecha(torneo.getFechaActual(), torneo);
        int cantEquipos = torneo.getEquipos().size();
        calcularInstancia(cantEquipos);
        fecha.setNombre(torneo.getIdaYVuelta() ? IDA + " " + calcularInstancia(cantEquipos): calcularInstancia(cantEquipos));
        torneo.getFechas().add(fecha);
        Collections.shuffle(torneo.getEquipos(), new Random(Calendar.getInstance().getTimeInMillis()));
        calcularFecha(torneo.getEquipos(), fecha);

    }

    protected String calcularInstancia(int cantEquipos) {
    	int length = instancias.length;
		int posInsActual = (int) Math.round(Math.sqrt(cantEquipos)) - 1;
		if (posInsActual < length)
			return instancias[posInsActual];
		else
			return (cantEquipos/2) + INSTANCIA_BASE;
			
	}

	private void calcularFecha(List<Equipo> equipos, Fecha fecha) {
        int cantEquipos = equipos.size();
        int cantidadPartidos = (int) Math.floor(cantEquipos / 2);
        int equipoIndex = 0;
        for (int i = 0; i < cantidadPartidos; ++i) {
            Partido partido = new Partido(equipos.get(equipoIndex), equipos.get(equipoIndex + 1), fecha);
            fecha.getPartidos().add(partido);
            equipoIndex += 2;
        }

    }

    @Override
    public void calcularSiguienteFecha(Torneo torneo) {
        List<Fecha> fec = torneo.getFechas();

        if ((!fec.isEmpty() && torneo.getFechaActual() < torneo.getCantidadFechas())) {
            if (!torneo.getIdaYVuelta()) { // Calculo fechas simples
                doCalcularFecha(fec, torneo);
            } else if ((torneo.getFechaActual().intValue() % 2) != 0) { // En la ida verifico si calculo la vuelta
                doCalcularFechaVuelta(fec, torneo);
            } else {
                doCalcularFechaIda(fec, torneo);
            }
        }
    }

    private void doCalcularFechaVuelta(List<Fecha> fec, Torneo torneo) {
        Collections.sort(fec);
        Fecha fecha = fec.get(torneo.getFechaActual() - 1);
        if (fecha.isJugada()) {
            torneo.incrementarFechaActual();
            Fecha fechaAct = new Fecha(torneo.getFechaActual(), torneo, true);
            torneo.getFechas().add(fechaAct);
            calcularFecha(fecha.obtenerEquiposVuelta(), fechaAct);
            fechaAct.setNombre(VUELTA + " " + calcularInstancia(fechaAct.getPartidos().size() * 2) );
        }
    }

    private void doCalcularFechaIda(List<Fecha> fec, Torneo torneo) {
        Collections.sort(fec);
        int fechaActual = torneo.getFechaActual();
        Fecha fechaIda = fec.get(fechaActual - 2);
        Fecha fechaVuelta = fec.get(fechaActual - 1);
        if (fechaIda.isJugada() && fechaVuelta.isJugada()) {
            torneo.incrementarFechaActual();
            Fecha fechaAct = new Fecha(fechaActual, torneo);
            torneo.getFechas().add(fechaAct);
            List<Entry<Equipo, Integer>> ganadoresIda = fechaIda.obtenerGanadoresYDiferenciaGolFecha();
            List<Entry<Equipo, Integer>> ganadoresVuelta = fechaVuelta.obtenerGanadoresYDiferenciaGolFecha();
            List<Equipo> ganadores = calcularGanadores(ganadoresIda, ganadoresVuelta, torneo);
            calcularFecha(ganadores, fechaAct);
            fechaAct.setNombre(IDA + " " + calcularInstancia(fechaAct.getPartidos().size() * 2));
        }
    }

    private List<Equipo> calcularGanadores(List<Entry<Equipo, Integer>> ganadoresIda,
            List<Entry<Equipo, Integer>> ganadoresVuelta, Torneo torneo) {
        List<Equipo> ganadores = new ArrayList<Equipo>();
        Iterator<Entry<Equipo, Integer>> itIda = ganadoresIda.iterator();
        Iterator<Entry<Equipo, Integer>> itVue = ganadoresVuelta.iterator();
        while (itIda.hasNext() && itVue.hasNext()) {
            Entry<Equipo, Integer> ganIda = itIda.next();
            Entry<Equipo, Integer> ganVue = itVue.next();
            Equipo ganadorIda = ganIda.getKey();
			Equipo ganadorVuelta = ganVue.getKey();
			if (ganadorIda != null && ganadorVuelta != null) {
                if (ganIda.getValue().intValue() > ganVue.getValue().intValue()) {
                    ganadores.add(ganadorIda);
                } else if(ganIda.getValue().intValue() < ganVue.getValue().intValue()){
                    ganadores.add(ganadorVuelta);
                }else{
                	logger.info(String.format("Empate en goles, pasa el mejor posicionado en torneos anteriores. Equipo 1: %s. Equipo 2: %s ", ganadorIda, ganadorVuelta));
                	ganadores.add(torneo.getMejorPosicionado(ganadorIda, ganadorVuelta));
                }
            } else if (ganadorVuelta == null) {
                ganadores.add(ganadorIda);
            } else {
                ganadores.add(ganadorVuelta);
            }
        }
        return ganadores;
    }

	private void doCalcularFecha(List<Fecha> fec, Torneo torneo) {
        Collections.sort(fec);
        int fechaActual = torneo.getFechaActual();
        Fecha fecha = fec.get(fechaActual - 1);
        if (fecha.isJugada()) {
            torneo.incrementarFechaActual();
            Fecha fechaAct = new Fecha(fechaActual, torneo);
            torneo.getFechas().add(fechaAct);
            List<Equipo> ganadores = fecha.obtenerGanadoresFecha();
            calcularFecha(ganadores, fechaAct);
            fechaAct.setNombre(calcularInstancia(fechaAct.getPartidos().size() * 2));
        }
    }

    @Override
    public int calcularCantidadFechas(Torneo torneo) {
        int cant = (int) Math.ceil(Math.log((double) torneo.getEquipos().size()) / Math.log(BASE));
        cant = torneo.getIdaYVuelta() ? cant * 2 : cant;
        return cant;
    }

	@Override
	public void inicializarPosiciones(Torneo torneo) {		
	}

	@Override
	public void actualizarPosiciones(Torneo torneo) {		
	}

}
