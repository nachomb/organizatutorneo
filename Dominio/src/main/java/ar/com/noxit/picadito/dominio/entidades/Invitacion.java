package ar.com.noxit.picadito.dominio.entidades;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;

@Entity
@Table(name = "invitacion")
public class Invitacion extends EntidadGenerica {

    /**
	 * 
	 */
    private static final long serialVersionUID = 8707987641958108405L;
    public static final String ESTADO_PENDIENTE = "pendiente";
    public static final String ESTADO_ENVIADO = "enviado";
    public static final String ESTADO_CONFIRMADO = "confirmado";
    public static final String ESTADO_CANCELADO = "cancelado";
    public static final String ESTADO_EN_DUDA = "en duda";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_INVITACION")
    private Long id;

    @Column
    private String mensaje;

    @Column
    private String estado;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_JUGADOR")
    private Jugador jugador;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PARTIDO_AMISTOSO")
    private PartidoAmistoso partidoAmistoso;

    public Invitacion() {
    }

    public Invitacion(Jugador jugador, PartidoAmistoso partido) {
        this.jugador = jugador;
        this.partidoAmistoso = partido;
        this.estado = ESTADO_PENDIENTE;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public PartidoAmistoso getPartidoAmistoso() {
        return partidoAmistoso;
    }

    public void setPartidoAmistoso(PartidoAmistoso partidoAmistoso) {
        this.partidoAmistoso = partidoAmistoso;
    }

    public boolean estaConfirmado() {
        return ESTADO_CONFIRMADO.equals(this.estado);
    }

    public void cambiarEstadoAPendiente() {
        this.estado = ESTADO_PENDIENTE;
    }

    public void cambiarEstadoAConfirmado() {
        this.estado = ESTADO_CONFIRMADO;
    }

    public void cambiarEstadoACancelado() {
        this.estado = ESTADO_CANCELADO;
    }

    public void cambiarEstadoADuda() {
        this.estado = ESTADO_EN_DUDA;
    }

    public boolean estaCancelado() {
        return ESTADO_CANCELADO.equals(this.estado);
    }

    public boolean estaEnDuda() {
        return ESTADO_EN_DUDA.equals(this.estado);
    }

    public void cambiarEstadoAEnviado() {
        this.estado = ESTADO_ENVIADO;
    }

    public boolean estaEnviado() {
        return ESTADO_ENVIADO.equals(this.estado);
    }

    public String getTelefonoDestino() {
        return this.jugador.getUsuario().getTelefonoCelular();
    }

}