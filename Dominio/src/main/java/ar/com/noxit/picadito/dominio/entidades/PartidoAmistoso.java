package ar.com.noxit.picadito.dominio.entidades;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import ar.com.noxit.picadito.dominio.entidades.generico.entities.EntidadGenerica;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

@Entity
@Table(name = "partido_amistoso")
public class PartidoAmistoso extends EntidadGenerica {

    private static final long serialVersionUID = 5829453728796541307L;
    private static final int DEFAULT_CANT_PARTICIPANTES = 10;
    private static final long DEFAULT_GOLES_INICIALES = 0;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PARTIDO_AMISTOSO")
    private Long id;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column
    private String modo;

    @Column
    private Integer cantParticipantes;

    @Column
    private String lugar;

    @Column
    private String mensaje;

    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY, mappedBy = "partidoAmistoso")
    private Collection<Invitacion> invitaciones;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_GRUPO")
    private Grupo grupo;

    @OneToOne
    @JoinColumn(name = "USUARIO_CREADOR_FK")
    private Usuario creador;

    @Transient
    private int cantConfirmados = 0;

    @Transient
    private int cantCancelados = 0;

    @Transient
    private int cantEnDuda = 0;

    @Transient
    private int cantPendientes = 0;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ALINEACION_EQUIPOA")
    private AlineacionAmistoso alineacionEquipoA;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ALINEACION_EQUIPOB")
    private AlineacionAmistoso alineacionEquipoB;

    @Column(name = "GOLES_EQUIPO_A")
    private Long golesEquipoA = DEFAULT_GOLES_INICIALES;

    @Column(name = "GOLES_EQUIPO_B")
    private Long golesEquipoB = DEFAULT_GOLES_INICIALES;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getModo() {
        return modo;
    }

    public void setModo(String modo) {
        this.modo = modo;
    }

    public Integer getCantParticipantes() {
        if (cantParticipantes == null)
            return DEFAULT_CANT_PARTICIPANTES;

        return cantParticipantes;
    }

    public void setCantParticipantes(Integer cantParticipantes) {
        if (cantParticipantes == null)
            this.cantParticipantes = DEFAULT_CANT_PARTICIPANTES;
        else
            this.cantParticipantes = cantParticipantes;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Collection<Invitacion> getInvitaciones() {
        return invitaciones;
    }

    public void setInvitaciones(Collection<Invitacion> invitaciones) {
        this.invitaciones = invitaciones;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public int getCantConfirmados() {
        return cantConfirmados;
    }

    public void setCreador(Usuario creador) {
        this.creador = creador;
    }

    public Usuario getCreador() {
        return creador;
    }

    public void calcularTotales() {
        for (Invitacion inv : invitaciones) {
            if (inv.estaConfirmado())
                ++cantConfirmados;
            else if (inv.estaCancelado()) {
                ++cantCancelados;
            } else if (inv.estaEnDuda()) {
                ++cantEnDuda;
            } else {
                ++cantPendientes;
            }
        }
    }

    public int getCantCancelados() {
        return cantCancelados;
    }

    public int getCantEnDuda() {
        return cantEnDuda;
    }

    public int getCantPendientes() {
        return cantPendientes;
    }

    public List<Jugador> getParticipantes() {
        List<Jugador> participantes = new ArrayList<Jugador>();
        for (Invitacion invitacion : invitaciones) {
            if (invitacion.estaConfirmado() || invitacion.estaEnDuda()) {
                participantes.add(invitacion.getJugador());
            }
        }
        return participantes;
    }
    
    public void asignarAlineaciones(List<Jugador> equipoA, List<Jugador> equipoB) {
        if (equipoA != null && this.alineacionEquipoA == null)
            alineacionEquipoA = new AlineacionAmistoso(this, equipoA);
        else if (equipoA != null && this.alineacionEquipoA != null)
            alineacionEquipoA.recalcularAlineacion(equipoA);

        if (equipoB != null && alineacionEquipoB == null)
            alineacionEquipoB = new AlineacionAmistoso(this, equipoB);
        else if (equipoB != null && alineacionEquipoB != null)
            alineacionEquipoB.recalcularAlineacion(equipoB);
    }

    public void setAlineacionEquipoA(AlineacionAmistoso alineacionEquipoA) {
        this.alineacionEquipoA = alineacionEquipoA;
    }

    public AlineacionAmistoso getAlineacionEquipoA() {
        return alineacionEquipoA;
    }

    public void setAlineacionEquipoB(AlineacionAmistoso alineacionEquipoB) {
        this.alineacionEquipoB = alineacionEquipoB;
    }

    public AlineacionAmistoso getAlineacionEquipoB() {
        return alineacionEquipoB;
    }

    public void setGolesEquipoA(Long golesEquipoA) {
        this.golesEquipoA = golesEquipoA;
    }

    public Long getGolesEquipoA() {
        return golesEquipoA;
    }

    public void setGolesEquipoB(Long golesEquipoB) {
        this.golesEquipoB = golesEquipoB;
    }

    public Long getGolesEquipoB() {
        return golesEquipoB;
    }

}