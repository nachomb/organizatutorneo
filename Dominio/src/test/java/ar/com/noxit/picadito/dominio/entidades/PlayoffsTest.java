package ar.com.noxit.picadito.dominio.entidades;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PlayoffsTest {

	private Playoffs target;
	
	@Before
	public void setUp(){
		this.target = new Playoffs(); 
	}
	
	@Test
	public void calcularInstacia_ochoEquipos_cuartosDeFinal(){
		String instancia = this.target.calcularInstancia(8);
		assertEquals("Cuartos de final", instancia);
	}
	
	@Test
	public void calcularInstacia_dieciseisEquipos_octavosDeFinal(){
		String instancia = this.target.calcularInstancia(16);
		assertEquals("Octavos de final", instancia);
	}
	
	@Test
	public void calcularInstacia_cuatroEquipos_semiFinal(){
		String instancia = this.target.calcularInstancia(4);
		assertEquals("Semifinales", instancia);
	}
	
	@Test
	public void calcularInstacia_cuatroEquipos_final(){
		String instancia = this.target.calcularInstancia(2);
		assertEquals("Final", instancia);
	}
}
