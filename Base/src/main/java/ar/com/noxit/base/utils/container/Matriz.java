package ar.com.noxit.base.utils.container;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Matriz {

	private List<List<String>> matriz;
	
	public Matriz() {
		this.matriz = new ArrayList<List<String>>();
	}

	public List<String> getFila(int fila){
		return matriz.get(fila);
	}
	
	public void agregarFila(List<String> fila){
		matriz.add(fila);
	}

	public int getCantFilas(){
		return matriz.size();
	}
	
	public int getCantColumnas(){
		if(!matriz.isEmpty()){
			return matriz.get(0).size();
		}
		return 0;
	}
	
	public void agregarColumna(List<String> columna){
		if(getCantFilas() != columna.size()){
			for(int i = 0 ; i < columna.size() ; ++i){
				matriz.add(new ArrayList<String>());
			}
			
		}
		Iterator<String> it = columna.iterator();
		for(List<String> fila : matriz){
			fila.add(it.next());
		}
		
	}

	public void trasponer() {
		Matriz matrizTraspuesta = new Matriz();
		List<String> columna = null;
		for(int column = 0 ; column < getCantColumnas() ; ++column ){
			columna = new ArrayList<String>();
			for(int fila = 0 ; fila < getCantFilas() ; ++fila){
				List<String> filaJ = getFila(fila);
				columna.add(filaJ.get(column));
			}
			matrizTraspuesta.agregarFila(columna);
		}
		this.matriz = matrizTraspuesta.getMatriz();
	}
	
	private List<List<String>> getMatriz(){
		return this.matriz;
	}
	
}
