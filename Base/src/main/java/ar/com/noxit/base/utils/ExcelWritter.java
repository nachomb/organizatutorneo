package ar.com.noxit.base.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import ar.com.noxit.base.utils.container.Matriz;

public class ExcelWritter {

	private static final int columnaInicialLocales = 1;
	private static final int rowsBetweenResults = 14;
	private static final int rowNumberInicialJugadores = 4;
	private static final int columnaInicialVisitantes = 7;
	private static final int rowNumberInicialPosiciones = 4;
	private static final int rowsEntreTorneos = 16;
	private static final int rowNumberInicialGoles = 4;
	private static final int rowsEntreTablaGoles = 14;
	private static final int rowNumberInicialTarjetas = 3;
	private static final int columnasEntreTablaTarjetas = 5;
	private Workbook workbook;
	private int rowNumber;
	private int contPartidos;
	private int contTorneos;
	private int contTablaGoles;
	private int contTablaTarjetas;
	private static Logger logger = Logger.getLogger(ExcelWritter.class);
	private CellStyle estiloFiguraParNumero;
	private CellStyle estiloFiguraParNombre;
	private CellStyle estiloFiguraImparNumero;
	private CellStyle estiloFiguraImparNombre;

	private static final DecimalFormat formateador = new DecimalFormat("###");

	public void inicializar() {
		contPartidos = 0;
		contTorneos = 0;
		contTablaGoles = 0;
		Sheet sheet = workbook.getSheetAt(0);
		estiloFiguraParNumero = sheet.getRow(4).getCell(13).getCellStyle();
		estiloFiguraParNombre = sheet.getRow(4).getCell(14).getCellStyle();
		estiloFiguraImparNumero = sheet.getRow(5).getCell(13).getCellStyle();
		estiloFiguraImparNombre = sheet.getRow(5).getCell(14).getCellStyle();
	}

	public ExcelWritter(String nombreArchivo) throws IOException {
		InputStream inp = getClass().getResourceAsStream(nombreArchivo);
		try {
			workbook = WorkbookFactory.create(inp);
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		inp.close();
	}

	public void writeResults(ResultSet results) throws SQLException,
			IOException {

		Sheet sheet = workbook.getSheetAt(0);
		rowNumber = 1 + contPartidos * rowsBetweenResults;
		logger.info("Escribiendo Resultados: Fila " + rowNumber);
		while (results.next()) {
			Row row = sheet.getRow(rowNumber);
			row.getCell(1).setCellValue(results.getString("nombreLocal"));
			row.getCell(7).setCellValue(results.getString("nombreVisitante"));
			row = sheet.getRow(rowNumber + 1);
			row.getCell(1).setCellValue(results.getInt("golesLocal"));
			row.getCell(7).setCellValue(results.getInt("golesVisitante"));
			rowNumber += rowsBetweenResults;
		}
		logger.info("FIN Escribiendo Resultados");
	}

	public void writePlayersLocales(ResultSet players) throws SQLException {
		writePlayers(players, columnaInicialLocales);

	}

	public void writePlayersVisitantes(ResultSet players) throws SQLException {
		writePlayers(players, columnaInicialVisitantes);
		++contPartidos;
	}

	private void writePlayers(ResultSet players, int columnaInicial)
			throws SQLException {
		logger.info("Escribiendo Jugadores");
		Sheet sheet = workbook.getSheetAt(0);
		rowNumber = rowNumberInicialJugadores + contPartidos
				* rowsBetweenResults;

		while (players.next()) {
			logger.info("Escribiendo Jugador con ID: " + players.getInt(7));
			Row row = sheet.getRow(rowNumber);
			int cellNumber = columnaInicial;
			row.getCell(cellNumber++).setCellValue(players.getInt(1));
			row.getCell(cellNumber++).setCellValue(players.getString(2));
			row.getCell(cellNumber++).setCellValue(players.getInt(3));
			row.getCell(cellNumber++).setCellValue(players.getInt(4));
			row.getCell(cellNumber).setCellValue(players.getInt(5));

			if (players.getBoolean(6)) {
				logger.info("Es figura, seteando estilos");
				cellNumber = columnaInicial;
				if ((rowNumber % 2) == 0) {
					row.getCell(cellNumber++).setCellStyle(
							estiloFiguraParNumero);
					row.getCell(cellNumber++).setCellStyle(
							estiloFiguraParNombre);
					row.getCell(cellNumber++).setCellStyle(
							estiloFiguraParNumero);
					row.getCell(cellNumber++).setCellStyle(
							estiloFiguraParNumero);
					row.getCell(cellNumber).setCellStyle(estiloFiguraParNumero);
				} else {
					row.getCell(cellNumber++).setCellStyle(
							estiloFiguraImparNumero);
					row.getCell(cellNumber++).setCellStyle(
							estiloFiguraImparNombre);
					row.getCell(cellNumber++).setCellStyle(
							estiloFiguraImparNumero);
					row.getCell(cellNumber++).setCellStyle(
							estiloFiguraImparNumero);
					row.getCell(cellNumber).setCellStyle(
							estiloFiguraImparNumero);

				}
			}

			++rowNumber;
		}

		logger.info("FIN Escribiendo Jugadores");
	}

	public void writePosiciones(ResultSet posiciones) throws SQLException {
		logger.info("Escribiendo Posiciones");
		Sheet sheet = workbook.getSheetAt(1);
		rowNumber = rowNumberInicialPosiciones + contTorneos * rowsEntreTorneos;
		int pos = 1;
		while (posiciones.next()) {
			Row row = sheet.getRow(rowNumber);
			int cellNumber = 1;
			logger.info("Escribiendo Posicion: " + pos + ", del torneo: "
					+ contTorneos);
			row.getCell(cellNumber++).setCellValue(pos++);
			logger.info("Escribiendo Posicion: " + posiciones.getString(1));
			row.getCell(cellNumber++).setCellValue(posiciones.getString(1));
			logger.info("Escribiendo Posicion: " + posiciones.getInt(2));
			row.getCell(cellNumber++).setCellValue(posiciones.getInt(2));
			logger.info("Escribiendo Posicion: " + posiciones.getString(3));
			row.getCell(cellNumber++).setCellValue(posiciones.getInt(3));
			logger.info("Escribiendo Posicion: " + posiciones.getInt(4));
			row.getCell(cellNumber++).setCellValue(posiciones.getInt(4));
			logger.info("Escribiendo Posicion: " + posiciones.getInt(5));
			row.getCell(cellNumber++).setCellValue(posiciones.getInt(5));
			logger.info("Escribiendo Posicion: " + posiciones.getInt(6));
			row.getCell(cellNumber++).setCellValue(posiciones.getInt(6));
			logger.info("Escribiendo Posicion: " + posiciones.getInt(7));
			row.getCell(cellNumber++).setCellValue(posiciones.getInt(7));
			logger.info("Escribiendo Posicion: " + posiciones.getInt(8));
			row.getCell(cellNumber++).setCellValue(posiciones.getInt(8));
			logger.info("Escribiendo Posicion: " + posiciones.getInt(9));
			row.getCell(cellNumber++).setCellValue(posiciones.getInt(9));
			row.getCell(cellNumber++).setCellValue(
					getPorcentajePuntos(posiciones));
			++rowNumber;
		}
		++contTorneos;
		logger.info("FIN Escribiendo Posiciones");
	}

	public void escribirMatriz(Matriz matriz, int hoja, int filaInicial,
			int columnaInicial) {
		logger.info("Escribiendo Matriz en la hoja " + hoja + " fila "
				+ filaInicial + " columna " + columnaInicial);
		
		Sheet sheet = workbook.getSheetAt(hoja);
		int numFila = filaInicial;
		int numColumna = columnaInicial;
		
		for(int i = 0 ; i < matriz.getCantFilas() ; ++i){
			Row fila = sheet.getRow(numFila);
			List<String> valoresFila = matriz.getFila(i);
			numColumna = columnaInicial;
			for(String valor : valoresFila){
				logger.info("escribiendo valor " + valor + " en la fila " + numFila + " en la columna " + numColumna);
				fila.getCell(numColumna).setCellValue(valor);
				++numColumna;
			}
			++numFila;
		}
		logger.info("FIN Escribiendo Matriz");
	}

	private Integer getPorcentajePuntos(ResultSet posiciones)
			throws SQLException {
		if (posiciones.getInt("partidosJugados") == 0)
			return 0;
		double puntosPorPartido = posiciones.getFloat("puntos")
				/ posiciones.getFloat("partidosJugados");
		double porcentajePuntos = (puntosPorPartido * 100) / 3;
		return Integer.valueOf(formateador.format(porcentajePuntos));
	}

	public void writeGoleadores(ResultSet goleadores) throws SQLException {
		logger.info("Escribiendo Goleadores");
		Sheet sheet = workbook.getSheetAt(2);
		rowNumber = rowNumberInicialGoles + contTablaGoles
				* rowsEntreTablaGoles;
		int pos = 1;
		while (goleadores.next()) {
			Row row = sheet.getRow(rowNumber);
			int cellNumber = 1;
			logger.info(goleadores.getString(1));
			row.getCell(cellNumber++).setCellValue(pos++);
			row.getCell(cellNumber++).setCellValue(goleadores.getString(1));
			row.getCell(cellNumber++).setCellValue(goleadores.getString(2));
			row.getCell(cellNumber).setCellValue(goleadores.getInt(3));
			++rowNumber;
		}
		++contTablaGoles;
		logger.info("FIN Escribiendo Goleadores");

	}

	public void writeTarjetas(ResultSet tarjetas) throws SQLException {
		logger.info("Escribiendo Tarjetas");
		Sheet sheet = workbook.getSheetAt(3);
		rowNumber = rowNumberInicialTarjetas;
		while (tarjetas.next()) {
			Row row = sheet.getRow(rowNumber);
			int cellNumber = 1 + contTablaTarjetas * columnasEntreTablaTarjetas;
			row.getCell(cellNumber++).setCellValue(tarjetas.getString(1));
			row.getCell(cellNumber++).setCellValue(tarjetas.getString(2));
			row.getCell(cellNumber++).setCellValue(tarjetas.getInt(3));
			row.getCell(cellNumber).setCellValue(tarjetas.getInt(4));
			++rowNumber;
		}
		++contTablaTarjetas;
		logger.info("FIN Escribiendo Tarjetas");

	}

	public void writeCell(int rowNumber, int columnNumber, int sheetNumber,
			String valor) {
		logger.info("escribiendo fila " + rowNumber + " columna "
				+ columnNumber + " valor " + valor);
		Sheet sheet = workbook.getSheetAt(sheetNumber);
		Row row = sheet.getRow(rowNumber);
		Cell cell = row.getCell(columnNumber);
		cell.setCellValue(valor);
	}

	public void save(String nombre) throws IOException {
		logger.info("Escribiendo Archivo xlsx");
		//FileOutputStream fileOut = new FileOutputStream(nombre);
		File yourFile = new File(nombre);
		if(!yourFile.exists()) {
		    yourFile.createNewFile();
		} 
		FileOutputStream fileOut = new FileOutputStream(yourFile, false); 
		
		workbook.write(fileOut);
		fileOut.close();
	}

	public void setCellTypeNumeric(int rowInicial, int columnInicial, int rowFinal, int columnFinal, int sheetNumber) {
		Sheet sheet = workbook.getSheetAt(sheetNumber);
		for(int i = rowInicial ; i < rowFinal ; ++i){
			Row row = sheet.getRow(i);
			for(int j = columnInicial ; j < columnFinal ; ++j){
				Cell cell = row.getCell(j);
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
			}
		}
	}

}
