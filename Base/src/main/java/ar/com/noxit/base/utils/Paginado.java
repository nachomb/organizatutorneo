package ar.com.noxit.base.utils;

/**
 * Este objeto sera utilizado por todos los daos que necesiten enviar un resultado paginado. Todos los daos tienen como
 * atributo unPaginado y al momento de resolver una consulta devuelve el resultado segun lo indicado en la instancia de
 * este objeto. Ver el comentario sobre el metodo doList() en AbstractDAO
 * 
 * @author lucas
 * 
 */
public class Paginado {
    private Integer page;
    private Integer size;

    public Paginado() {
    }

    public Paginado(Paginado paginado) {
        this.page = new Integer(paginado.page);
        this.size = new Integer(paginado.size);
    }

    public Integer getPage() {
        return this.page;
    }

    public void setPage(Integer page) throws Exception {
        if (page < 1) {
            throw new Exception("el numero de pagina debe ser positivo");
        }
        this.page = page;
    }

    public Integer getSize() {
        return this.size;
    }

    public void setSize(Integer size) throws Exception {
        if (size < 1) {
            throw new Exception("el tamanio de la pagina debe ser mayor a 1");
        }
        this.size = size;
    }
}
