package ar.com.noxit.base.utils;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Mail {

    private String strSmtp;
    private String from;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setStrSmtp(String strSmtp) {
        this.strSmtp = strSmtp;
    }

    public String getStrSmtp() {
        return this.strSmtp;
    }

    public void sendMail(String recipients[], String subject, String message) throws MessagingException {
        Session session = getSession();

        // create a message
        Message msg = new MimeMessage(session);

        InternetAddress[] addressTo = getInternetAddresses(recipients, msg);
        
        msg.setRecipients(Message.RecipientType.TO, addressTo);
        // Optional : You can also set your custom headers in the Email if you
        // Want
        // msg.addHeader("MyHeaderName", "myHeaderValue");

        // Setting the Subject and Content Type
        msg.setSubject(subject);
        msg.setContent(message, "text/plain");

        // send message
        // Transport.send(msg);

        Transport tr = session.getTransport("smtps");
        tr.connect(this.strSmtp, this.from, this.password);
        tr.sendMessage(msg, msg.getAllRecipients());
        tr.close();
    }

	private InternetAddress[] getInternetAddresses(String[] recipients,
			Message msg) throws AddressException, MessagingException {
		// set the from and to address
        InternetAddress addressFrom = new InternetAddress(from);
        msg.setFrom(addressFrom);

        InternetAddress[] addressTo = new InternetAddress[recipients.length];
        for (int i = 0; i < recipients.length; i++) {
            addressTo[i] = new InternetAddress(recipients[i]);
        }
		return addressTo;
	}

	private Session getSession() {
		boolean debug = true;

        // Set the host smtp address
        Properties props = new Properties();
        props.put("mail.smtps.host", getStrSmtp());
        props.put("mail.smtps.auth", "true");
        // create some properties and get the default Session

        Session session = Session.getDefaultInstance(props);
        session.setDebug(debug);
		return session;
	}
    
	public void enviarMailUsuarioNuevo(String mail, String pass) {
		String subject = "Usuario y clave para ingresar a picadito.com.ar";

		String message = "Hola,";
		message += "\n Su usuario es: " + mail + ".";
		message += "\n Su clave es: " + pass + " .";
		message += "\n Ya puede ingresar al sistema.";

		String[] recipient = new String[1];
		recipient[0] = mail;

		try {
			this.sendMail(recipient, subject, message);

		} catch (Exception e) {
			System.out.println("Error in sending mail:" + e);
		}
	}
	
	public void enviarMailConAdjunto(String mail, String subject, String file){
		
		String message = "Este es un mail generado por el sistema.";

		String[] recipient = new String[1];
		recipient[0] = mail;

		try {
			this.sendMail(recipient, subject, message, file);

		} catch (Exception e) {
			System.out.println("Error in sending mail:" + e);
		}
		
	}

	private void sendMail(String[] recipients, String subject, String message,
			String file) throws MessagingException {
		
        Session session = getSession();

        // create a message
        Message msg = new MimeMessage(session);

        InternetAddress[] addressTo = getInternetAddresses(recipients, msg);
        
        msg.setRecipients(Message.RecipientType.TO, addressTo);

        // Setting the Subject and Content Type
        msg.setSubject(subject);
        //msg.setContent(message, "text/plain");
        
        // create and fill the first message part
        MimeBodyPart mbp1 = new MimeBodyPart();
        mbp1.setText(message);
        
        // create the second message part
        MimeBodyPart mbp2 = new MimeBodyPart();

        // attach the file to the message
        FileDataSource fds = new FileDataSource(file);
        mbp2.setDataHandler(new DataHandler(fds));
        mbp2.setFileName(fds.getName());

        // create the Multipart and add its parts to it
        Multipart mp = new MimeMultipart();
        mp.addBodyPart(mbp1);
        mp.addBodyPart(mbp2);
        
        msg.setContent(mp);
        
        Transport tr = session.getTransport("smtps");
        tr.connect(this.strSmtp, this.from, this.password);
        tr.sendMessage(msg, msg.getAllRecipients());
        tr.close();
		
	}

}