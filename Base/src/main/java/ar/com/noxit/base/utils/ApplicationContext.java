package ar.com.noxit.base.utils;

public class ApplicationContext {
    private static final Integer generalPageSize = 10;
    private static ThreadLocal<Paginado> paginado = new ThreadLocal<Paginado>();

    private void ApplicationContext() {
    }

    public static ThreadLocal<Paginado> getPaginado() {
        return paginado;
    }

    public static Integer getGeneralPageSize() {
        return generalPageSize;
    }
}