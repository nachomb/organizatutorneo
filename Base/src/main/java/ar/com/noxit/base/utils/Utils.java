package ar.com.noxit.base.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

public abstract class Utils {

	public static final String DATE_FORMAT = "yyyyMMddHHmmss";
	public static final String[] colores = { "FF0000", "006400", "F08080",
			"7CFC00", "20B2AA", "EE82EE", "FFFF00", "4B0082", "808080" };
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm");
	private static boolean produccion = true;
	private static Calendar calendar = new GregorianCalendar();
	private static final NumberFormat formatter = new DecimalFormat("#0.00");
	
	public static String formatDouble(double value){
		return formatter.format(value);
	}

	/**
	 * Ejemplos de uso: Date hoy = new Date();
	 * System.out.println(formatDateTime(hoy, "dd/MM/yyyy"));
	 * System.out.println(formatDateTime(hoy, "HHmmss"));
	 * System.out.println(formatDateTime(hoy, "yyyyMMdd"));
	 * System.out.println(formatDateTime(hoy, "hhmmss"));
	 * 
	 * Nota: Si date es null, devuelve null
	 * 
	 * @param date
	 * @param format
	 * @return
	 */
	public static String formatDateTime(Date date, String format) {
		if (date == null)
			return null;
		SimpleDateFormat dateformat = new SimpleDateFormat(format);
		StringBuilder sb = new StringBuilder(dateformat.format(date));
		return sb.toString();
	}

	/**
	 * 
	 * @param str
	 * @param beginIndex
	 * @param length
	 * @return
	 */
	public static String substringByLength(String str, int beginIndex,
			int length) {

		if (str == null)
			return null;

		if (beginIndex > str.length())
			beginIndex = str.length();

		int endIndex = beginIndex + length;
		if (endIndex > str.length())
			endIndex = str.length();

		return str.substring(beginIndex, endIndex).trim();
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	static public String formatEnglishMode(String pattern, double value) {
		NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH); // El
																			// Locale
																			// ENGLISH
																			// hace
																			// que
																			// el
																			// separador
																			// de
		// decimales sea el punto
		DecimalFormat df = (DecimalFormat) nf;
		df.applyPattern(pattern);
		return df.format(value);
	}

	public static String nullAsBlank(String str) {
		return (str == null) ? "" : str;
	}

	public static String generarNumeroOrden() {
		return new SimpleDateFormat(DATE_FORMAT).format(Calendar.getInstance()
				.getTime());
	}

	public static boolean isValidDateFormat(String strDate, String format) {
		if (strDate == null)
			return false;

		SimpleDateFormat dateFormat = new SimpleDateFormat(format);

		if (strDate.trim().length() != dateFormat.toPattern().length())
			return false;

		dateFormat.setLenient(false);

		try {
			dateFormat.parse(strDate);
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}

	public static Date getDate(String strFecha, String strHora,
			String fechaFormato, String horaFormato) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(fechaFormato + " "
				+ horaFormato);
		dateFormat.setLenient(false);
		return dateFormat.parse(strFecha + " " + strHora);
	}

	/**
	 * Obtiene un Long a partir de un string
	 * 
	 * @param string
	 * @return primer Long que encuentra en el string o null si no encuentra
	 */
	public static Long getLong(String str) {
		Pattern p = Pattern.compile("(\\d+)");
		Matcher m = p.matcher(str);
		if (m.find())
			return Long.valueOf(m.group(1));

		return null;
	}

	/**
	 * Copia archivos !
	 * 
	 * @param fromFile
	 * @param toFile
	 * @throws IOException
	 * @author mmilicich (fuente:
	 *         http://www.java2s.com/Code/Java/File-Input-Output
	 *         /CopyfilesusingJavaIOAPI.htm)
	 */
	public static void fileCopy(File fromFile, File toFile) throws IOException {

		if (!fromFile.exists())
			throw new IOException("FileCopy: " + "no such source file: "
					+ fromFile);
		if (!fromFile.isFile())
			throw new IOException("FileCopy: " + "can't copy directory: "
					+ fromFile);
		if (!fromFile.canRead())
			throw new IOException("FileCopy: " + "source file is unreadable: "
					+ fromFile);

		if (toFile.isDirectory())
			toFile = new File(toFile, fromFile.getName());

		if (toFile.exists()) {
			if (!toFile.canWrite())
				throw new IOException("FileCopy: "
						+ "destination file is unwriteable: " + toFile);
			System.out.print("Overwrite existing file " + toFile.getName()
					+ "? (Y/N): ");
			System.out.flush();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					System.in));
			String response = in.readLine();
			if (!response.equals("Y") && !response.equals("y"))
				throw new IOException("FileCopy: "
						+ "existing file was not overwritten.");
		} else {
			String parent = toFile.getParent();
			if (parent == null)
				parent = System.getProperty("user.dir");
			File dir = new File(parent);
			if (!dir.exists())
				throw new IOException("FileCopy: "
						+ "destination directory doesn't exist: " + parent);
			if (dir.isFile())
				throw new IOException("FileCopy: "
						+ "destination is not a directory: " + parent);
			if (!dir.canWrite())
				throw new IOException("FileCopy: "
						+ "destination directory is unwriteable: " + parent);
		}

		FileInputStream from = null;
		FileOutputStream to = null;
		try {
			from = new FileInputStream(fromFile);
			to = new FileOutputStream(toFile);
			byte[] buffer = new byte[4096];
			int bytesRead;

			while ((bytesRead = from.read(buffer)) != -1)
				to.write(buffer, 0, bytesRead); // write
		} finally {
			if (from != null)
				try {
					from.close();
				} catch (IOException e) {
					;
				}
			if (to != null)
				try {
					to.close();
				} catch (IOException e) {
					;
				}
		}
	}

	public static void toBeginningOfDay(Calendar fecha) {
		if (fecha != null) {
			fecha.set(Calendar.HOUR_OF_DAY, 0);
			fecha.set(Calendar.MINUTE, 0);
			fecha.set(Calendar.SECOND, 0);
			fecha.set(Calendar.MILLISECOND, 0);
		}
	}

	public static void toEndOfDay(Calendar fecha) {
		if (fecha != null) {
			fecha.set(Calendar.HOUR_OF_DAY, 23);
			fecha.set(Calendar.MINUTE, 59);
			fecha.set(Calendar.SECOND, 59);
			fecha.set(Calendar.MILLISECOND, 999);
		}
	}

	public static String NUMEROS = "0123456789";

	public static String MAYUSCULAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public static String MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";

	//
	public static String getPinNumber() {
		return getPassword(NUMEROS, 4);
	}

	public static String getPassword() {
		return getPassword(8);
	}

	public static String getPassword(int length) {
		return getPassword(NUMEROS + MAYUSCULAS + MINUSCULAS, length);
	}

	public static String getPassword(String key, int length) {
		String pswd = "";

		for (int i = 0; i < length; i++) {
			pswd += (key.charAt((int) (Math.random() * key.length())));
		}

		return pswd;
	}

	public static List<Long> stringToLongList(String idsStr) {
		List<Long> listIds = new ArrayList<Long>();
		if (idsStr != null && idsStr.length() > 0) {
			for (String id : idsStr.split(",")) {
				listIds.add(new Long(id.trim()));
			}
		}
		return listIds;
	}

	public static List<String> stringToStringList(String idsStr) {
		return new ArrayList<String>(Arrays.asList(idsStr.split(",")));
	}

	public static Map<Long, Long> stringToMap(String idsStr) {
		Map<Long, Long> map = new TreeMap<Long, Long>();

		if (idsStr != null && idsStr.length() > 0) {
			String[] splitted = idsStr.split(",");
			if (splitted.length % 2 != 0) {
				return null;
			}
			for (int i = 0; i < splitted.length; i += 2 ) {
				map.put(new Long(splitted[i]), new Long(splitted[i + 1]));
			}
		}
		return map;
	}

	public static String guadarImagen(File upload) throws IOException {

		if (upload != null) {
			// String sep = System.getProperty("file.separator");
			// String sep = "/";
			int rand = (int) (Math.random() * 100000);
			String fullFileName;
			String relativePath;
			if (produccion) {
				fullFileName = "/usr/share/tomcat6/webapps/ROOT/imagenes/"
						+ rand + ".jpg";
				relativePath = "/imagenes/" + rand + ".jpg";
			} else {
				fullFileName = "/repo/" + rand + ".jpg";
				relativePath = "/repo/" + rand + ".jpg";
			}
			File theFile = new File(fullFileName);
			FileUtils.copyFile(upload, theFile);
			return relativePath;

		}

		return null;
	}
	
	public static String guadarArchivo(File upload) throws IOException {

		if (upload != null) {
			String fullFileName;
			String relativePath;

			String fileName = upload.getName();
			System.out.println(fileName);
			fullFileName = "/repo/" + fileName;
			relativePath = "/repo/" + fileName;
			File theFile = new File(fullFileName);
			FileUtils.copyFile(upload, theFile);
			return relativePath;

		}

		return null;
	}

	public static Date stringToDate(String fechaYHora) throws ParseException {
		return dateFormat.parse(fechaYHora);
	}

	public static String dateToString(Date date) {
		return dateFormat.format(date);
	}

	public static int stringToInt(String string) {
		return Integer.parseInt(string);
	}

	public static Date addMinutes(Date date, int minutes) {
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, minutes);
		return calendar.getTime();
	}
}
