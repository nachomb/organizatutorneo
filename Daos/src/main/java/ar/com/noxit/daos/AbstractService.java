package ar.com.noxit.daos;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.dao.generico.IGenericDAO;

@Transactional
public class AbstractService<T> implements IGenericService<T> {

    protected IGenericDAO<T, Long> genericDAO;

    private static final Logger LOG = LoggerFactory.getLogger(AbstractService.class);

    @Transactional(readOnly = true)
    public Collection<T> get(T businessObject) {
        return genericDAO.get(businessObject);
    }

    @Transactional(readOnly = true)
    public Collection<T> getAll() {
        return genericDAO.getAll();
    }

    public Class<T> getEntity() {
        return genericDAO.getEntity();
    }

    @Transactional(readOnly = true)
    public T load(Long id) {
        return genericDAO.load(id);
    }

    @Transactional
    public T merge(T businessObject) {
        return genericDAO.merge(businessObject);
    }

    @Transactional
    public void persist(T businessObject) {
        genericDAO.persist(businessObject);
    }

    @Transactional
    public void remove(T businessObject) {
        genericDAO.remove(businessObject);
    }

    @Transactional
    public void update(T businessObject) {
        genericDAO.update(businessObject);
    }

    @Transactional(readOnly = true)
    public List<T> find(String query) {
        return genericDAO.find(query);
    }

    @Transactional(readOnly = true)
    public List<T> find(String query, Object[] values) {
        return genericDAO.find(query, values);
    }

    @Transactional
    public void saveOrUpdate(T businessObject) {
        this.genericDAO.saveOrUpdate(businessObject);
    }

    @Transactional(readOnly = true)
    public T findById(Long id) {
        return this.genericDAO.findById(id);
    }

    static public Logger getLogger() {
        return LOG;
    }

    @Transactional(readOnly = true)
    public List<T> getAllPaginado() {
        return this.genericDAO.getAllPaginado();
    }

    @Transactional(readOnly = true)
    public Integer getCountAll() {
        return this.genericDAO.getCountAll();
    }

}
