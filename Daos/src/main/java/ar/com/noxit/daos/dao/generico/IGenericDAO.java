package ar.com.noxit.daos.dao.generico;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface IGenericDAO<T, ID extends Serializable> {

    /**
     * Obtiene la entidad cuyo identificador es el indicado
     * 
     * @param id
     *            identificador unico de la entidad
     * @return retorna la entidad cuyo identificador es el indicado
     */
    @Transactional(readOnly = true)
    public T load(ID id);

    /**
     * Persiste la entidad indicada
     * 
     * @param businessObject
     *            entidad que se quiere persistir
     */
    @Transactional
    public void persist(T businessObject);

    /**
     * Actualiza la entidad indicada
     * 
     * @param businessObject
     *            entidad que se quiere actualizar
     */
    @Transactional
    public void update(T businessObject);

    /**
     * Persiste o actualiza la entidad indicada
     * 
     * @param businessObject
     *            entidad que se quiere persistir o actualizar
     * @return retorna la entidad con un nuevo identificador en caso de haber sido persistida
     */
    @Transactional
    public T merge(T businessObject);

    /**
     * Remueve la entidad indicada
     * 
     * @param businessObject
     *            entidad que se desea remover
     */
    @Transactional
    public void remove(T businessObject);

    /**
     * Obtiene una coleccion de entidades que se asocian al ejemplo
     * 
     * @param businessObject
     *            entidad ejemplo
     * @return retorna una coleccion de entidades
     */
    @Transactional(readOnly = true)
    public Collection<T> get(T businessObject);

    /**
     * Obtiene todas las entidades relacionadas a la clase
     * 
     * @param clazz
     *            clase de la entidad
     * @return coleccion de entidades
     */
    @Transactional(readOnly = true)
    public Collection<T> getAll();

    /**
     * Devuelve la clase de la entidad asociada
     * 
     * @return clase asociada
     */
    public Class<T> getEntity();

    /**
     * 
     * @param query
     * @return
     */
    @Transactional(readOnly = true)
    public List<T> find(String query);

    @Transactional(readOnly = true)
    public List<T> find(String query, Object[] values);

    /**
     * 
     * @param query
     * @param params
     * @return
     */
    public Collection<T> find(String query, Object param);

    /**
     * 
     * @param businessObject
     */
    @Transactional
    public void saveOrUpdate(T businessObject);

    /**
     * 
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    public T findById(ID id);

    @Transactional(readOnly = true)
    public List<T> getAllPaginado();

    @Transactional(readOnly = true)
    public Integer getCountAll();

}
