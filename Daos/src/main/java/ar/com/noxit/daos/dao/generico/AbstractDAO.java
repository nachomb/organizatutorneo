package ar.com.noxit.daos.dao.generico;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Projections;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.base.utils.ApplicationContext;
import ar.com.noxit.base.utils.Paginado;

@SuppressWarnings("unchecked")
@Transactional
public abstract class AbstractDAO<T> extends HibernateDaoSupport implements IGenericDAO<T, Long> {

    // Clase de la entidad asociada
    Class<T> persistClass;

    // Se utilizará el datasource para poder obtenerlo con el método getConnection() que se usaran en los reportes.
    // Se pone como atributo para que spring lo inyecte con el autowire.
    private BasicDataSource dataSource;

    public AbstractDAO() {
        this.persistClass = getEntity();
    }

    @Transactional(readOnly = true)
    public Collection<T> get(T object) throws DataAccessException {
        return this.getHibernateTemplate().findByExample(object);
    }

    @Transactional(readOnly = true)
    public T load(Long id) {
        return (T) this.getHibernateTemplate().load(getEntity(), id);
    }

    public T merge(T object) {
        return (T) this.getHibernateTemplate().merge(object);
    }

    @Transactional
    public void persist(T object) {
        this.getHibernateTemplate().persist(object);
    }

    public void remove(T object) {
        this.getHibernateTemplate().delete(object);

    }

    public void update(T object) {
        this.getHibernateTemplate().update(object);
    }

    @Transactional(readOnly = true)
    public Collection<T> getAll() {
        return this.getHibernateTemplate().find("from " + persistClass.getSimpleName() + " order by id");
    }

    @Transactional(readOnly = true)
    public Class<T> getEntity() {
        return (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Transactional(readOnly = true)
    public List<T> find(String query) {
        return this.getHibernateTemplate().find(query);
    }

    @Transactional(readOnly = true)
    public List<T> find(String query, Object[] values) {
        return this.getHibernateTemplate().find(query, values);
    }

    public void saveOrUpdate(T object) {
        this.getHibernateTemplate().saveOrUpdate(object);
    }

    @Transactional(readOnly = true)
    public T findById(Long id) {
        List<T> list = this.find("from " + this.getEntity().getSimpleName() + " where id=" + id);

        if (list.size() != 1)
            return null;

        return list.iterator().next();
    }

    public Collection<T> find(String query, Object param) {
        return this.getHibernateTemplate().find(query, param);
    }

    /**
     * Todos los metodos de los daos que por medio de un Query pretendan devolver una coleccion de objetos, deberian
     * llamar a este doList para que en el caso que se haya seteado en el thread local un paginado, entonces dicha
     * coleccion se devuelva paginada.
     * 
     * @author afillol;
     */
    public List doList(Query query) {
        Paginado paginado = this.getPaginado();

        if (paginado == null) {
            return query.list();
        }

        Integer desde = ((paginado.getPage() - 1) * paginado.getSize());

        return query.setFirstResult(desde).setMaxResults(paginado.getSize()).list();
    }

    /**
     * Todos los metodos de los daos que por medio de un Query pretendan devolver una coleccion de objetos, deberian
     * llamar a este doList para que en el caso que se haya seteado en el thread local un paginado, entonces dicha
     * coleccion se devuelva paginada.
     * 
     * @author afillol;
     */
    public List doList(Criteria criteria) {
        Paginado paginado = this.getPaginado();

        if (paginado == null) {
            return criteria.list();
        }

        Integer desde = ((paginado.getPage() - 1) * paginado.getSize());

        return criteria.setFirstResult(desde).setMaxResults(paginado.getSize()).list();
    }

    protected Integer getRowCount(Criteria criteria) {
        criteria.setProjection(Projections.rowCount());

        return ((Integer) criteria.list().get(0)).intValue();
    }

    protected Integer getRowCount(Query query) {
        ScrollableResults results = query.scroll();
        int size = (results.last() ? results.getRowNumber() + 1 : 0);
        return new Integer(size);
    }

    private Paginado getPaginado() {
        Paginado paginado = null;
        if (ApplicationContext.getPaginado().get() != null) {
            paginado = new Paginado(ApplicationContext.getPaginado().get());
            ApplicationContext.getPaginado().set(null);
        }
        return paginado;
    }

    @Transactional(readOnly = true)
    public List<T> getAllPaginado() {
        return (List<T>) this.doList(getQueryAll());
    }

    @Transactional(readOnly = true)
    public Integer getCountAll() {
        return getRowCount(getQueryAll());
    }

    protected Query getQueryAll() {
        Query query = this.getSession().createQuery("from " + this.getEntity().getSimpleName() + " order by id");
        return query;
    }

    public void setDataSource(BasicDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public BasicDataSource getDataSource() {
        return this.dataSource;
    }

}
