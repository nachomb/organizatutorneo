package ar.com.noxit.daos;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

public interface IGenericService<T> {

    @Transactional(readOnly = true)
    public Collection<T> get(T businessObject);

    @Transactional(readOnly = true)
    public T load(Long id);

    @Transactional
    public T merge(T businessObject);

    @Transactional
    public void persist(T businessObject);

    public void remove(T businessObject);

    @Transactional
    public void update(T businessObject);

    @Transactional(readOnly = true)
    public Collection<T> getAll();

    public Class<T> getEntity();

    @Transactional(readOnly = true)
    public List<T> find(String query);

    @Transactional
    public void saveOrUpdate(T businessObject);

    @Transactional(readOnly = true)
    public T findById(Long id);

    public Collection<T> getAllPaginado();

    @Transactional(readOnly = true)
    public Integer getCountAll();

}
