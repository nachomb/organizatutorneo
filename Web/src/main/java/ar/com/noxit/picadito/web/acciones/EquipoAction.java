package ar.com.noxit.picadito.web.acciones;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.seguridad.services.IUsuarioService;
import ar.com.noxit.servicios.IEquipoService;
import ar.com.noxit.servicios.IJugadorService;

import com.google.common.collect.Lists;
import com.opensymphony.xwork2.Preparable;

@SuppressWarnings("serial")
public class EquipoAction extends BaseActionModelDrivenPaginado<Equipo>
		implements Preparable {

	private boolean habilitarOKenabled = true;
	private IJugadorService jugadorService;
	private IUsuarioService usuarioService;
	private String jugadoresSeleccionados;
	private Collection<Jugador> jugadoresEquipo;
	private Collection<Jugador> jugadoresPosibles;

	private File upload; // The actual file
	private String uploadContentType;// The content type of the file
	private String uploadFileName;// The uploaded file name and path
	private String fileCaption;// The caption of the file entered by user.

	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadContentType() {
		return uploadContentType;
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public String getFileCaption() {
		return fileCaption;
	}

	public void setFileCaption(String fileCaption) {
		this.fileCaption = fileCaption;
	}

	public void setJugadoresEquipo(Collection<Jugador> jugadoresEquipo) {
		this.jugadoresEquipo = jugadoresEquipo;
	}

	public Collection<Jugador> getJugadoresEquipo() {
		return jugadoresEquipo;
	}

	public void setJugadoresPosibles(Collection<Jugador> jugadoresPosibles) {
		this.jugadoresPosibles = jugadoresPosibles;
	}

	public Collection<Jugador> getJugadoresPosibles() {
		return jugadoresPosibles;
	}

	public void setJugadoresSeleccionados(String jugadoresSeleccionados) {
		this.jugadoresSeleccionados = jugadoresSeleccionados;
	}

	public String getJugadoresSeleccionados() {
		return jugadoresSeleccionados;
	}

	public IJugadorService getJugadorService() {
		return jugadorService;
	}

	public void setJugadorService(IJugadorService jugadorService) {
		this.jugadorService = jugadorService;
	}

	public boolean isHabilitarOKenabled() {
		return habilitarOKenabled;
	}

	public void setHabilitarOKenabled(boolean habilitarOKenabled) {
		this.habilitarOKenabled = habilitarOKenabled;
	}

	public EquipoAction(IEquipoService equipoService,IUsuarioService usuarioService) {
		this.genericService = equipoService;
		this.usuarioService = usuarioService;
		setDisplaytagGridID("equipoGrid");
		this.setPageSize(20);
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		setJugadoresPosibles(jugadorService.getJugadoresByUsuarioCreadorSinEquipo(getSessionUser()));
		if (getRequestId() != 0) {
			this.entity = genericService.findById(Long.valueOf(getRequestId()));
			setJugadoresEquipo(this.entity.getJugadores());
		} else {
			this.entity = new Equipo();
		}

	}

	@Override
	public Equipo getModel() {
		return this.getEntity();
	}

	@Override
	public String remove() {
		// Eliminacion Logica SIEMPRE:
		this.entity.setActivo(false);
		genericService.update(this.entity);
		return list();
	}

	protected boolean tienePermiso() {
		Jugador jugador = getJugadorService().getjugadorByUsuario(getSessionUser());
		if (!this.entity.equals(jugador.getEquipo())) {
			return false;
		}
		return true;

	}

	@Override
	public void validate() {
		if (this.entity.getNombre().length() == 0) {
			addFieldError("nombre", "Nombre requerido.");
		}
		if (((IEquipoService) this.genericService).esEquipoDuplicado(entity)) {
			addFieldError("nombre", "El nombre del equipo ya existe.");
		}
	}

	@Override
	public String update() {
		this.fillEntity();
		this.updateJugadores();
		try {
			if(upload != null)
				this.entity.setRutaImgEscudo(Utils.guadarImagen(upload));
		} catch (IOException e) {
			addActionError(e.getMessage());
		}
		return super.update();
	}

	private void updateJugadores() {
		Collection<Jugador> jugadoresEq = this.entity.getJugadores();
		for (Jugador j : jugadoresEquipo) {
			if (!jugadoresEq.contains(j)) {
				j.setEquipo(null);
				jugadorService.update(j);
			}
		}
	}

	@Override
	public String save() {
		this.fillEntity();
		try {
			if(upload != null)
				this.entity.setRutaImgEscudo(Utils.guadarImagen(upload));
		} catch (IOException e) {
			addActionError(e.getMessage());
		}
		return super.save();
	}



	private void fillEntity() {
		Collection<Jugador> jugadoresCollection = getJugadoresFromString(getJugadoresSeleccionados());
		this.getEntity().setJugadores(jugadoresCollection);
		setEquipoAJugadores();
		Usuario usuario = usuarioService.findById(getSessionUser().getId());
		this.entity.setCreador(usuario);

	}

	private void setEquipoAJugadores() {
		for (Jugador j : this.entity.getJugadores()) {
			j.setEquipo(this.entity);
		}

	}

	private Collection<Jugador> getJugadoresFromString(String jugSelccionados) {
		Collection<Jugador> jugadoresColl = new ArrayList<Jugador>();
		List<Long> ids = Utils.stringToLongList(jugSelccionados);

		if (ids.size() > 0) {
			for (Long id : ids) {
				jugadoresColl.add(this.getJugadorService().findById(id));
			}
		}

		return jugadoresColl;
	}

	@SkipValidation
	public String misEquipos() {
		setActionMethod("MisEquipos");
		cargarMisEquipos();
		setReadOnly(true);
		return Constantes.LIST;
	}

	private void cargarMisEquipos() {
		Usuario usuario = getSessionUser();
		if (!usuario.isAnonimo()) {
			List<Equipo> equipos = getEquipoService().getEquiposByUsuario(
					usuario);
			this.setRowCount(equipos.size());
			this.setEntities(equipos);
		} else {
			this.setRowCount(0);
			this.setEntities(new ArrayList<Equipo>());
		}
	}

	private IEquipoService getEquipoService() {
		return (IEquipoService) this.genericService;
	}

	/*@Override
	public String list() {
		super.list();
		Usuario usuario = getSessionUser();
		for (Perfil perfil : usuario.getPerfiles()) {
			if ("PADMIN".equals(perfil.getNombre())) {
				return super.list();
			}
		}
		return "misEquipos";
	}*/
	
	
	@SkipValidation
	public String listAllActivos(){
        setReadOnly(true);
        this.setRowCount(this.genericService.getCountAll());
        this.setEntities(((IEquipoService)this.genericService).getAllActivos());
        this.setMappedRequest(Constantes.LIST);
        return Constantes.SUCCESS;
	}
	
	@Override
	public Collection<Equipo> getEntitiesToList() {
		Usuario usuario = getSessionUser();
		if(!usuario.isAnonimo() && "PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
			Collection<Equipo> equipos = this.getEquipoService().getEquiposByUsuarioCreador(usuario);
			if(equipos != null){
				setRowCount(equipos.size());
			}
			return equipos;
		}
		return Lists.newArrayList();
	}

	@Override
	@SkipValidation
	public String edit() {
		Usuario usuario = getSessionUser();
		if(!usuario.isAnonimo() && "PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
			Equipo equipo = genericService.findById(Long.valueOf(getRequestId()));
			if(usuario.equals(equipo.getCreador())){
				return super.edit();
			}
		}
		return ERROR;
	}

}
