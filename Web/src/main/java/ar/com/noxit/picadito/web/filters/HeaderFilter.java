package ar.com.noxit.picadito.web.filters;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class HeaderFilter implements Filter {

    private DateFormat dateFormat;

    public void init(FilterConfig filterConfig) throws ServletException {
        dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Calendar calendar = new GregorianCalendar();
        ((HttpServletResponse) response).setHeader("Last-Modified", dateFormat.format(calendar.getTime()));
        calendar.add(Calendar.MONTH, 1);
        String expiresDate = dateFormat.format(calendar.getTime());
		((HttpServletResponse) response).setHeader("Expires", expiresDate);
		((HttpServletResponse) response).setHeader("Cache-Control", "public");
        // Continue
        chain.doFilter(request, response);
    }

    public void destroy() {
        this.dateFormat = null;
    }

}