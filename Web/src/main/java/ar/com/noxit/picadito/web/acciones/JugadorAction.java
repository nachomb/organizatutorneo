package ar.com.noxit.picadito.web.acciones;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.base.utils.Mail;
import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.seguridad.services.IUsuarioService;
import ar.com.noxit.servicios.IEquipoService;
import ar.com.noxit.servicios.IJugadorService;

import com.google.common.collect.Lists;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.Validations;

@SuppressWarnings("serial")
public class JugadorAction extends BaseActionModelDrivenPaginado<Jugador>
		implements Preparable {

	private boolean habilitarOKenabled = true;
	private boolean crearUsuario;
	private String mail;
	private String telefonoCelular;
	private IUsuarioService usuarioService;
	private Mail mailService;
	private IEquipoService equipoService;
	private Collection<Equipo> equiposPosibles;
	private String nombreFiltro;
	private String apellidoFiltro;
	private String selectedEquipo;

	public String getApellidoFiltro() {
		return apellidoFiltro;
	}

	public void setApellidoFiltro(String apellidoFiltro) {
		this.apellidoFiltro = apellidoFiltro;
	}
	
	public String getNombreFiltro() {
		return nombreFiltro;
	}

	public void setNombreFiltro(String nombreFiltro) {
		this.nombreFiltro = nombreFiltro;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setEquiposPosibles(Collection<Equipo> equiposPosibles) {
		this.equiposPosibles = equiposPosibles;
	}

	public Collection<Equipo> getEquiposPosibles() {
		return equiposPosibles;
	}

	public void setEquipoService(IEquipoService equipoService) {
		this.equipoService = equipoService;
	}

	public IEquipoService getEquipoService() {
		return equipoService;
	}

	public Mail getMailService() {
		return mailService;
	}

	public void setMailService(Mail mailService) {
		this.mailService = mailService;
	}

	public boolean isHabilitarOKenabled() {
		return habilitarOKenabled;
	}

	public void setHabilitarOKenabled(boolean habilitarOKenabled) {
		this.habilitarOKenabled = habilitarOKenabled;
	}

	public IUsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(IUsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean isCrearUsuario() {
		return crearUsuario;
	}

	public void setCrearUsuario(boolean crearUsuario) {
		this.crearUsuario = crearUsuario;
	}

	public JugadorAction(IJugadorService jugadorService) {
		this.genericService = jugadorService;
		setDisplaytagGridID("jugadorGrid");
		this.setPageSize(20);
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		setEquiposPosibles(equipoService.getEquiposByUsuarioCreador(getSessionUser()));
		if (getRequestId() != 0) {
			this.entity = genericService.findById(Long.valueOf(getRequestId()));
			cargarForm();
		} else {
			this.entity = new Jugador();
		}
	}

	private void cargarForm() {
		Equipo equipo = this.entity.getEquipo();
		if (equipo != null) {
			setSelectedEquipo(equipo.getNombre());
		}
	}

	@Override
	public Jugador getModel() {
		return this.getEntity();
	}

	@Override
	public String save() {
		fillEntity();
		if (crearUsuario) {
			Usuario usuario = new Usuario();
			String pass = Utils.getPassword();
			usuario.setUsername(mail);
			usuario.setTelefonoCelular(telefonoCelular);
			usuarioService.encodeAndSetPassword(usuario, pass);
			usuarioService.saveOrUpdate(usuario);
			this.entity.setUsuario(usuario);
			mailService.enviarMailUsuarioNuevo(mail, pass);
		}
		return super.save();
	}

	@Override
	public String update() {
		fillEntity();
		return super.update();
	}

	private void fillEntity() {
		String equipo = getSelectedEquipo();
		Equipo eq = null;
		if (equipo != null && !equipo.trim().isEmpty()) {
			eq = equipoService.findById(Long.parseLong(equipo.trim()));
		}
		this.getEntity().setEquipo(eq);
		this.getEntity().setCreador(usuarioService.findById(getSessionUser().getId()));
	}

	public void validate() {

		if (crearUsuario && "".equals(mail)) {
			addFieldError("mail",
					"Si desea crear el usuario debe ingresar un mail.");
		}

		// No se debe repetir el nombre de usuario
		if (crearUsuario && usuarioService.isDuplicateUsername(mail)) {
			addFieldError("mail", "El mail ingresado ya existe.");
		}

		if (this.getEntity().getNombre() != null && this.getEntity().getNombre().length() == 0) {
			addFieldError("nombre", "Nombre requerido.");
		}

		if (this.getEntity().getApellido() != null && this.getEntity().getApellido().length() == 0) {
			addFieldError("apellido", "Apellido requerido.");
		}
	}

	@Override
	public String remove() {
		// Eliminacion Logica SIEMPRE:
		this.entity.setActivo(false);
		if (this.entity.getUsuario() != null) {
			this.entity.getUsuario().setActivo(false);
		}
		genericService.update(this.entity);
		return list();
	}

	public void setSelectedEquipo(String selectedEquipo) {
		this.selectedEquipo = selectedEquipo;
	}

	public String getSelectedEquipo() {
		return selectedEquipo;
	}

	@SkipValidation
	public String miJugadorListado() {
		setActionMethod("MiJugador_miJugadorDetalle");
		Jugador jugador = getJugadorService().getjugadorByUsuario(
				getSessionUser());
		if (jugador != null) {
			List<Jugador> jugadores = new ArrayList<Jugador>();
			jugadores.add(jugador);
			setEntities(jugadores);
			this.setRowCount(jugadores.size());
			return Constantes.LIST;
		} else {
			return "crearJugador";
		}
	}

	@SkipValidation
	public String miJugadorDetalle() {
		validar();
		setActionMethod("MiJugador_miJugadorListado");
		return Constantes.SUCCESS;
	}

	@Validations
	public String crearMiJugador() {
		setActionMethod("MiJugador");
		fillEntity();
		genericService.persist(entity);
		return Constantes.SUCCESS;
	}

	private IJugadorService getJugadorService() {
		return (IJugadorService) this.genericService;
	}

	@Override
	protected boolean tienePermiso() {
		Jugador jugador = getJugadorService().getjugadorByUsuario(
				getSessionUser());
		if (!this.entity.equals(jugador)) {
			return false;
		}
		return true;
	}

	@Override
	public Collection<Jugador> getEntitiesToList() {
		if(this.nombreFiltro == null && this.apellidoFiltro == null){
			Usuario usuario = getSessionUser();
			if(!usuario.isAnonimo() && "PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
				Collection<Jugador> jugadores = this.getJugadorService().getJugadoresByUsuarioCreador(usuario);
				if(jugadores != null){
					setRowCount(jugadores.size());
				}
				return jugadores;
			}
		}else{
			Jugador jugador = new Jugador();
			jugador.setNombre(nombreFiltro);
			jugador.setApellido(apellidoFiltro);
			jugador.setCreador(getSessionUser());
			Collection<Jugador> jugadores = getJugadorService().getJugadorLike(jugador);
			if(jugadores != null){
				setRowCount(jugadores.size());
			}
			return jugadores;
		}
		return Lists.newArrayList();
	}

	@Override
	@SkipValidation
	public String edit() {
		Usuario usuario = getSessionUser();
		if(!usuario.isAnonimo() && "PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
			Jugador jugador = genericService.findById(Long.valueOf(getRequestId()));
			if(usuario.equals(jugador.getCreador())){
				return super.edit();
			}
		}
		return ERROR;
	}	
	
}
