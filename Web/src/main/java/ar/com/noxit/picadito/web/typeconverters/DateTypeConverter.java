package ar.com.noxit.picadito.web.typeconverters;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.apache.struts2.util.StrutsTypeConverter;

public class DateTypeConverter extends StrutsTypeConverter {

    @Override
    public Object convertFromString(Map map, String[] strings, Class toClass) {
        if (strings.length != 1) {
            super.performFallbackConversion(map, strings, toClass);
        }

        String[] strDate = this.getStringDate(strings[0]);

        Date date = null;
        if (strDate != null) {
            Calendar c = Calendar.getInstance();

            int dia = Integer.valueOf(strDate[0]);
            int mes = Integer.valueOf(strDate[1]);
            int anio = Integer.valueOf(strDate[2]);
            int horas = 0;
            int minutos = 0;
            int segundos = 0;

            c.set(anio, mes - 1, dia, horas, minutos, segundos);
            date = c.getTime();
        }

        return date;
    }

    @Override
    public String convertToString(Map arg0, Object arg1) {
        return arg1.toString();
    }

    private String[] getStringDate(String strDate) {
        StringTokenizer strtok = new StringTokenizer(strDate, "/");

        String[] date = new String[3];
        try {
            date[0] = strtok.nextToken();
            date[1] = strtok.nextToken();
            date[2] = strtok.nextToken();
            return date;
        } catch (NoSuchElementException e) {
            return null;
        }
    }
}
