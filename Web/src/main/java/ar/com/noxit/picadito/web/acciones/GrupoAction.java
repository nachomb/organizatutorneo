package ar.com.noxit.picadito.web.acciones;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.picadito.dominio.entidades.Grupo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.servicios.IGrupoService;
import ar.com.noxit.servicios.IJugadorService;

import com.opensymphony.xwork2.Preparable;

public class GrupoAction extends BaseActionModelDrivenPaginado<Grupo> implements
		Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7237631543139979971L;
	private boolean habilitarOKenabled = true;
	private Collection<String> jugadoresSel = new ArrayList<String>();
	private IJugadorService jugadorService;

	public void setJugadorService(IJugadorService jugadorService) {
		this.jugadorService = jugadorService;
	}

	public IJugadorService getJugadorService() {
		return jugadorService;
	}

	public void setJugadoresSel(Collection<String> jugadoresSel) {
		this.jugadoresSel = jugadoresSel;
	}

	public Collection<String> getJugadoresSel() {
		return jugadoresSel;
	}

	public void setHabilitarOKenabled(boolean habilitarOKenabled) {
		this.habilitarOKenabled = habilitarOKenabled;
	}

	public boolean isHabilitarOKenabled() {
		return habilitarOKenabled;
	}

	public GrupoAction(IGrupoService grupoService) {
		this.genericService = grupoService;
		this.setDisplaytagGridID("grupoGrid");
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		if (getRequestId() != 0) {
			this.entity = genericService.findById(Long.valueOf(getRequestId()));
			validar();
			for (Jugador j : this.entity.getJugadores()) {
				jugadoresSel.add(j.getUsuario().getUsername());
			}
		} else {
			Jugador jugador = getJugadorService().getjugadorByUsuario(getSessionUser());
			this.entity = new Grupo();
			this.entity.setCreador(jugador);
		}

	}

	@Override
	public Grupo getModel() {
		return this.getEntity();
	}

	@Override
	public String save() {
		this.fillEntity();
		return super.save();
	}
	
	@Override
	public String list() {
		if(perfilAdmin())
			return super.list();
		return "misGrupos";
	}

	private boolean perfilAdmin() {
		Usuario usuario = getSessionUser();
		for(Perfil perfil : usuario.getPerfiles()){
			if("PADMIN".equals(perfil.getNombre())){
				return true;
			}
		}
		return false;
	}

	private void fillEntity() {
		Collection<Jugador> jugadores = new ArrayList<Jugador>();
		for (String nombre : jugadoresSel) {
			Jugador jugador = jugadorService.getjugadorByNombreUsuario(nombre);
			jugadores.add(jugador);
		}
		this.getEntity().setJugadores(jugadores);
		this.getEntity().setCreador(
				jugadorService.getjugadorByUsuario(getSessionUser()));
	}

	@Override
	public String update() {
		this.fillEntity();
		return super.update();
	}

	@Override
    public void validate() {
        if (this.entity.getNombre().length() == 0) {
            addFieldError("nombre", "Nombre requerido.");
        } else {
            Jugador jugador = this.getJugadorService().getjugadorByUsuario(getSessionUser());
            if (this.getGrupoService().existeGrupoParaJugador(jugador,this.entity)){
                addFieldError("nombre", "Ya existe grupo con el nombre ingresado para el usuario.");
            }
        }
        for (String nombre : jugadoresSel) {
            Jugador jugador = this.getJugadorService().getjugadorByNombreUsuario(nombre);
            if (jugador == null) {
                addActionError("El usuario " + nombre + " no existe o no tiene creado un jugador.");
            }
            if (nombre.trim().equalsIgnoreCase(this.getEntity().getCreador().getUsuario().getUsername())){
                addActionError("El usuario " + nombre + " es el creador del grupo.");
            }
        }
    }

	@SkipValidation
	public String misGrupos() {
		setActionMethod("MisGrupos");
		cargarMisGrupos();
		return Constantes.LIST;
	}

	private void cargarMisGrupos() {
		Usuario usuario = getSessionUser();
		if (!usuario.isAnonimo()) {
			List<Grupo> grupos = this.getGrupoService().getGruposByUsuario(usuario);
			this.setRowCount(grupos.size());
			this.setEntities(grupos);
		} else {
			this.setRowCount(0);
			this.setEntities(new ArrayList<Grupo>());
		}

	}

	protected boolean tienePermiso() {
		Jugador jugador = getJugadorService().getjugadorByUsuario(getSessionUser());
		if (!this.entity.getCreador().equals(jugador)) {
			return false;
		}
		return true;
		
	}

	private IGrupoService getGrupoService() {
		return (IGrupoService) this.genericService;
	}

	@SkipValidation
	public String listAmistoso() {
	    setActionMethod("MisGrupos_listAmistoso");
        cargarMisGrupos();
        return Constantes.LIST;
	}
	

	@SkipValidation
	public String addAmistoso() {
	    setActionMethod("MisGrupos_saveAmistoso");
        return Constantes.SUCCESS;
	}
	
	public String saveAmistoso() {
        this.fillEntity();
        genericService.persist(this.entity);
        return listAmistoso();
    }
	
	@SkipValidation
    public String editAmistoso() {
        setActionMethod("MisGrupos_updateAmistoso");
        return Constantes.SUCCESS;
    }
    
    public String updateAmistoso() {
        this.fillEntity();
        genericService.update(this.entity);
        return listAmistoso();
    }
    
    @SkipValidation
    public String destroyAmistoso() {
        setReadOnly(true);
        setActionMethod("MisGrupos_removeAmistoso");
        return Constantes.SUCCESS;
    }
    
    @SkipValidation
    public String removeAmistoso() {
        genericService.remove(this.entity);
        return listAmistoso();
    }
}
