package ar.com.noxit.picadito.web.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.lang.StringUtils;

import ar.com.noxit.picadito.dominio.entidades.ItemMenu;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.IEquipoService;
import ar.com.noxit.servicios.IItemMenuService;
import ar.com.noxit.servicios.IPublicidadService;
import ar.com.noxit.servicios.impl.ITorneoService;

public class MenuSession {
	
	private static MenuSession INSTANCE = new MenuSession();

	private MenuSession() {
	}

	public static MenuSession getInstance() {
		return INSTANCE;
	}

	public void refreshMenuSession(Map session, IItemMenuService itemMenuService) {
		session.put("itemsMenu", null);
		cargarMenuEnSesion(session, itemMenuService);
	}

	private void cargarMenuEnSesion(Map session,
			IItemMenuService itemMenuService) {
		Collection<Perfil> perfiles = this.getPerfiles();

		if (perfiles != null && perfiles.size() > 0) {
			// Obtengo los items menu de nivel
			Collection<ItemMenu> itemsMenu = itemMenuService
					.findAllItemsMenuByPadre(perfiles, null);

			// Pongo en sesion los items menu
			session.put("itemsMenu", itemsMenu);
		}
	}

	/**
	 * Devuelve los items menu del nivel 1
	 * 
	 * @param padres
	 * @return
	 */
	private Collection<ItemMenu> getItemsMenuNivel1(
			Collection<ItemMenu> padres, IItemMenuService itemMenuService) {
		Collection<ItemMenu> hijos = new ArrayList<ItemMenu>();

		Iterator<ItemMenu> it = padres.iterator();

		while (it.hasNext()) {
			Collection<ItemMenu> aux = itemMenuService.findAllItemsMenuByPadre(
					this.getPerfiles(), it.next());

			if (aux != null && !aux.isEmpty())
				hijos.addAll(aux);
		}

		return hijos;
	}

	/**
	 * Devuelve los perfiles de un usuario
	 * 
	 * @return
	 */
	private Collection<Perfil> getPerfiles() {
		return ((Usuario) (SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal())).getPerfiles();
	}

	public void refreshEquipos(Map session, IEquipoService equipoService) {
		session.put("equipos", equipoService.getAllActivos());
		
	}

	public void refreshTorneos(Map session, ITorneoService torneoService, String uid) {
		Long id = Long.valueOf(uid);
		Usuario usuario = new Usuario();
		usuario.setId(id);
		session.put("torneos", torneoService.getTorneosByUsuarioCreador(usuario));
		
	}

	public void refreshPublicidadesSlider(Map session, IPublicidadService publicidadService) {
		session.put("publicidadesSlider",publicidadService.getPublicidadesSliderActivas());
	}

	public void refreshPublicidadesVerticales(Map session,
			IPublicidadService publicidadService) {
		session.put("publicidadesVerticales",publicidadService.getPublicidadesVerticalesActivas());
		
	}
}
