package ar.com.noxit.picadito.web.acciones;

import java.io.File;
import java.io.IOException;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.picadito.dominio.entidades.Publicidad;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.servicios.IPublicidadService;

import com.opensymphony.xwork2.Preparable;

@SuppressWarnings("serial")
public class PublicidadAction extends BaseActionModelDrivenPaginado<Publicidad>
		implements Preparable {

	private boolean habilitarOKenabled = true;
	private File uploadImgVertical;
	private File uploadImgSlider;
	
	public File getUploadImgVertical() {
		return uploadImgVertical;
	}

	public void setUploadImgVertical(File uploadImgVertical) {
		this.uploadImgVertical = uploadImgVertical;
	}

	public File getUploadImgSlider() {
		return uploadImgSlider;
	}

	public void setUploadImgSlider(File uploadImgSlider) {
		this.uploadImgSlider = uploadImgSlider;
	}

	public boolean isHabilitarOKenabled() {
		return habilitarOKenabled;
	}

	public void setHabilitarOKenabled(boolean habilitarOKenabled) {
		this.habilitarOKenabled = habilitarOKenabled;
	}

	public PublicidadAction(IPublicidadService publicidadService){
        this.genericService = publicidadService;
        this.setDisplaytagGridID("publicidadGrid");
	}
	
	@Override
	public Publicidad getModel() {
		return this.getEntity();
	}

	@Override
	protected boolean tienePermiso() {
		return true;
	}
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		if (getRequestId() != 0) {
			this.entity = genericService.findById(Long.valueOf(getRequestId()));
		} else {
			this.entity = new Publicidad();
		}

	}
	
	@Override
	public String save() {
		guardarImagenes();
		return super.save();
	}
	
	@Override
	public String update() {
		guardarImagenes();
		return super.update();
	}

	private void guardarImagenes() {
		try {
			if(uploadImgVertical != null)
				this.entity.setImagenVertical(Utils.guadarImagen(uploadImgVertical));
			if(uploadImgSlider != null)
				this.entity.setImagenSlider(Utils.guadarImagen(uploadImgSlider));
		} catch (IOException e) {
			addActionError(e.getMessage());
		}
	}


}
