package ar.com.noxit.picadito.web.acciones;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.picadito.dominio.entidades.Grupo;
import ar.com.noxit.picadito.dominio.entidades.Invitacion;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.PartidoAmistoso;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.seguridad.services.IUsuarioService;
import ar.com.noxit.servicios.IGrupoService;
import ar.com.noxit.servicios.IInvitacionService;
import ar.com.noxit.servicios.IJugadorService;
import ar.com.noxit.servicios.IPartidoAmistosoService;

import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;

public class PartidoAmistosoAction extends BaseActionModelDrivenPaginado<PartidoAmistoso> implements Preparable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -483711541970257224L;
    private boolean habilitarOKenabled = true;
    private Collection<String> modos = new ArrayList<String>();
    private String fechaYHora;
    private String selectedGrupo;
    private Collection<Grupo> gruposPosibles;
    private IGrupoService grupoService;
    private IJugadorService jugadorService;
    private IUsuarioService usuarioService;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
    private static final String SEPARADOR_JUGADORES = ",";
    private Collection<String> jugadoresInvitados = new ArrayList<String>();
    private Collection<Jugador> jugadoresGrupo = null;
    private Jugador jugadorSession;
    private Collection<PartidoAmistoso> partidosJugados;
    private Integer partidosJugadosSize;
    private IInvitacionService invitacionService;

    private String selectedJugadoresEquipoA;
    private String selectedJugadoresEquipoB;
    private List<String> equipoANull = new ArrayList<String>();
    private List<String> equipoBNull = new ArrayList<String>();

    public IInvitacionService getInvitacionService() {
        return invitacionService;
    }

    public void setInvitacionService(IInvitacionService invitacionService) {
        this.invitacionService = invitacionService;
    }

    public Integer getPartidosJugadosSize() {
        return partidosJugadosSize;
    }

    public void setPartidosJugadosSize(Integer partidosJugadosSize) {
        this.partidosJugadosSize = partidosJugadosSize;
    }

    public Collection<PartidoAmistoso> getPartidosJugados() {
        return partidosJugados;
    }

    public void setPartidosJugados(Collection<PartidoAmistoso> partidosJugados) {
        this.partidosJugados = partidosJugados;
    }

    public void setJugadorSession(Jugador jugadorSession) {
        this.jugadorSession = jugadorSession;
    }

    public Jugador getJugadorSession() {
        return jugadorSession;
    }

    public void setUsuarioService(IUsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    public IUsuarioService getUsuarioService() {
        return usuarioService;
    }

    public void setJugadoresGrupo(Collection<Jugador> jugadoresGrupo) {
        this.jugadoresGrupo = jugadoresGrupo;
    }

    public Collection<Jugador> getJugadoresGrupo() {
        return jugadoresGrupo;
    }

    public void setJugadoresInvitados(Collection<String> jugadoresInvitados) {
        this.jugadoresInvitados = jugadoresInvitados;
    }

    public Collection<String> getJugadoresInvitados() {
        return jugadoresInvitados;
    }

    public void setJugadorService(IJugadorService jugadorService) {
        this.jugadorService = jugadorService;
    }

    public IJugadorService getJugadorService() {
        return jugadorService;
    }

    public void setGrupoService(IGrupoService grupoService) {
        this.grupoService = grupoService;
    }

    public IGrupoService getGrupoService() {
        return grupoService;
    }

    public void setGruposPosibles(Collection<Grupo> gruposPosibles) {
        this.gruposPosibles = gruposPosibles;
    }

    public Collection<Grupo> getGruposPosibles() {
        return gruposPosibles;
    }

    public void setSelectedGrupo(String selectedGrupo) {
        this.selectedGrupo = selectedGrupo;
    }

    public String getSelectedGrupo() {
        return selectedGrupo;
    }

    public void setFechaYHora(String fechaYHora) {
        this.fechaYHora = fechaYHora;
    }

    public String getFechaYHora() {
        return fechaYHora;
    }

    public void setModos(Collection<String> modos) {
        this.modos = modos;
    }

    public Collection<String> getModos() {
        return modos;
    }

    public void setHabilitarOKenabled(boolean habilitarOKenabled) {
        this.habilitarOKenabled = habilitarOKenabled;
    }

    public boolean isHabilitarOKenabled() {
        return habilitarOKenabled;
    }

    public PartidoAmistosoAction(IPartidoAmistosoService partidoAmistosoService) {
        this.genericService = partidoAmistosoService;
        this.setDisplaytagGridID("partidoAmistosoGrid");
    }

    @Override
    public void prepare() throws Exception {
        super.prepare();
        modos.add("Difusion");
        modos.add("Conservador");
        this.gruposPosibles = grupoService.getGruposByUsuario(getSessionUser());
        if (getRequestId() != 0) {
            this.entity = genericService.findById(Long.valueOf(getRequestId()));
            cargarForm();

        } else {
            this.entity = new PartidoAmistoso();
            this.entity.setCreador(getSessionUser());
        }
    }

    private void cargarForm() {
        setFechaYHora(dateFormat.format(this.entity.getFecha()));
        Grupo grupo = this.entity.getGrupo();
        if (grupo != null)
            setSelectedGrupo(grupo.getNombre());
    }

    @Override
    public PartidoAmistoso getModel() {
        return this.getEntity();
    }

    @Override
    public String save() {
        this.fillEntity();
        return super.save();
    }

    private void fillEntity() {
        try {
            Date fecha = dateFormat.parse(fechaYHora);
            this.getEntity().setFecha(fecha);
        } catch (ParseException ex) {
            ex.printStackTrace();

        }
        String grupo = getSelectedGrupo();
        Grupo gru = null;
        Usuario usuario = getSessionUser();
        Jugador jugador = getJugadorService().getjugadorByUsuario(usuario);
        if (grupo != null && !grupo.trim().isEmpty()) {
            gru = grupoService.getGrupoPorNombreYCreador(grupo.trim(), jugador);
        }
        this.getEntity().setGrupo(gru);

        Collection<Invitacion> invitaciones = new ArrayList<Invitacion>();
        if (getRequestId() == 0) {
            // si es nuevo creo las invitaciones
            invitaciones.add(new Invitacion(jugador, this.entity));

            for (String nombre : jugadoresInvitados) {
                Jugador j = this.getJugadorService().getjugadorByNombreUsuario(nombre);
                Invitacion invitacion = new Invitacion(j, this.entity);
                invitaciones.add(invitacion);
            }
        }
        this.getEntity().setInvitaciones(invitaciones);
        this.getEntity().setCreador(usuarioService.findById(usuario.getId()));
    }

    @Override
    public String update() {
        this.fillEntity();
        return super.update();
    }

    @Override
    public String list() {
        this.setMappedRequest(Constantes.LIST);
        this.setEntities(this.getPartidoAmistosoService().getAllPartidosNoJugados());
        this.setRowCount(this.getPartidoAmistosoService().getCountAllPartidosNoJugados());
        this.setPartidosJugados(this.getPartidoAmistosoService().getAllPartidosJugados());
        this.setPartidosJugadosSize(this.getPartidoAmistosoService().getCountAllPartidosJugados());
        return Constantes.LIST;
    }

    @Override
    public void validate() {
        if (this.getEntity().getLugar().length() == 0) {
            addFieldError("lugar", "Lugar requerido.");
        }
        if (this.getEntity().getModo() == null || this.getEntity().getModo().equals("1")) {
            addFieldError("modo", "Debe seleccionar un modo.");
        }

        try {
            dateFormat.parse(fechaYHora);
        } catch (ParseException ex) {
            addFieldError("fechaYHora", "Formato de la fecha invalida");
        }
        if (this.getEntity().getCantParticipantes() == null || this.getEntity().getCantParticipantes() == 0) {
            addFieldError("cantParticipantes", "Cantidad de participantes requerido.");
        }
    }

    private void cargarMisPartidoAmistosos() {
        Usuario usuario = getSessionUser();

        if (!usuario.isAnonimo()) {
            Jugador jugador = getJugadorService().getjugadorByUsuario(usuario);
            this.setEntities(this.getPartidoAmistosoService().getPartidosByUsuario(usuario, jugador));
            this.setRowCount(this.getPartidoAmistosoService().getCountPartidosByUsuario(usuario, jugador));
            this.setPartidosJugados(this.getPartidoAmistosoService().getPartidosJugadosByUsuario(usuario, jugador));
            this.setPartidosJugadosSize(this.getPartidoAmistosoService().getCountPartidosJugadosByUsuario(usuario,
                    jugador));
        } else {
            this.setRowCount(0);
            this.setEntities(new ArrayList<PartidoAmistoso>());
            this.setPartidosJugadosSize(0);
            this.setPartidosJugados(new ArrayList<PartidoAmistoso>());
        }

    }

    @SkipValidation
    public String mostrarJugadores() {
        if (!"".equals(this.selectedGrupo)) {
            Grupo grupo = getGrupoService().getGrupoPorNombreYCreador(this.selectedGrupo,
                    jugadorService.getjugadorByUsuario(getSessionUser()));
            this.setJugadoresGrupo(grupo.getJugadores());
        }
        return Constantes.SUCCESS;
    }

    public String invitacionesList() {
        this.setEntities(this.getEntitiesToList());
        this.setMappedRequest(Constantes.LIST);
        return Constantes.LIST;
    }

    private IPartidoAmistosoService getPartidoAmistosoService() {
        return (IPartidoAmistosoService) this.genericService;
    }

    @SkipValidation
    public void calcularTotales() {
        this.entity.calcularTotales();
    }

    @Override
    public String consulta() {
        String result = super.consulta();
        setJugadorSession(jugadorService.getjugadorByUsuario(getSessionUser()));
        setHabilitarOKenabled(false);
        return result;
    }

    @SkipValidation
    public String ficha() {
        setReadOnly(true);
        return Constantes.SUCCESS;
    }

    @Override
    protected boolean tienePermiso() {
        Usuario usuario = getSessionUser();
        if (!this.entity.getCreador().equals(usuario)) {
            return false;
        }
        return true;
    }

    @Override
    public String remove() {
        Collection<Invitacion> invitaciones = this.entity.getInvitaciones();
        for (Invitacion inv : invitaciones) {
            invitacionService.remove(inv);
        }
        return super.remove();
    }

    @Override
    public String input() {
        setMappedRequest("cargar");
        return Constantes.INPUT;
    }

    public String inputAmistoso() {
        setActionMethod("MisPartidos_cargar");
        return Constantes.INPUT;
    }

    @Validations(requiredFields = {
            @RequiredFieldValidator(type = ValidatorType.SIMPLE, fieldName = "golesEquipoA", message = "Debe ingresar los goles del equipo A."),
            @RequiredFieldValidator(type = ValidatorType.SIMPLE, fieldName = "golesEquipoB", message = "Debe ingresar los goles del equipo B.") })
    public String cargar() {
        cargarAlineaciones();
        return "cargado";
    }

    private void cargarAlineaciones() {
        List<Long> idsJugadoresEquipoA = obtenerIdsJugadores(getSelectedJugadoresEquipoA());
        List<Long> idsJugadoresEquipoB = obtenerIdsJugadores(getSelectedJugadoresEquipoB());
        getPartidoAmistosoService().cargarAlineaciones(getEntity(), idsJugadoresEquipoA, idsJugadoresEquipoB);
    }

    private List<Long> obtenerIdsJugadores(String titulares) {
        List<Long> ids = new ArrayList<Long>();
        if (titulares == null)
            return ids;
        for (String id : titulares.split(SEPARADOR_JUGADORES)) {
            try {
                Long valor = Long.valueOf(id.trim());
                ids.add(valor);
            } catch (NumberFormatException ex) {
                // Hacer Nada :P
            }
        }
        return ids;
    }

    @SkipValidation
    public String addAmistoso() {
        validar();
        setActionMethod("MisPartidos_saveAmistoso");
        return Constantes.SUCCESS;
    }

    public String saveAmistoso() {
        this.fillEntity();
        genericService.persist(this.entity);
        return listAmistoso();
    }

    @SkipValidation
    public String editAmistoso() {
        validar();
        setActionMethod("MisPartidos_updateAmistoso");
        return Constantes.SUCCESS;
    }

    public String updateAmistoso() {
        this.fillEntity();
        genericService.update(this.entity);
        return listAmistoso();
    }

    @SkipValidation
    public String destroyAmistoso() {
        validar();
        setReadOnly(true);
        setActionMethod("MisPartidos_removeAmistoso");
        return Constantes.SUCCESS;
    }

    public String removeAmistoso() {
        Collection<Invitacion> invitaciones = this.entity.getInvitaciones();
        for (Invitacion inv : invitaciones) {
            invitacionService.remove(inv);
        }
        genericService.remove(this.entity);
        return listAmistoso();
    }

    @SkipValidation
    public String listAmistoso() {
        setActionMethod("MisPartidos_listAmistoso");
        cargarMisPartidoAmistosos();
        return Constantes.LIST;
    }

    public void setSelectedJugadoresEquipoA(String selectedJugadoresEquipoA) {
        this.selectedJugadoresEquipoA = selectedJugadoresEquipoA;
    }

    public String getSelectedJugadoresEquipoA() {
        return selectedJugadoresEquipoA;
    }

    public void setSelectedJugadoresEquipoB(String selectedJugadoresEquipoB) {
        this.selectedJugadoresEquipoB = selectedJugadoresEquipoB;
    }

    public String getSelectedJugadoresEquipoB() {
        return selectedJugadoresEquipoB;
    }

    public void setEquipoANull(List<String> equipoANull) {
        this.equipoANull = equipoANull;
    }

    public List<String> getEquipoANull() {
        return equipoANull;
    }

    public void setEquipoBNull(List<String> equipoBNull) {
        this.equipoBNull = equipoBNull;
    }

    public List<String> getEquipoBNull() {
        return equipoBNull;
    }

}
