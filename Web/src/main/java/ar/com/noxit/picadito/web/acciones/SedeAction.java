package ar.com.noxit.picadito.web.acciones;

import java.util.Collection;

import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.picadito.dominio.entidades.Sede;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.seguridad.services.IUsuarioService;
import ar.com.noxit.servicios.ISedeService;

import com.google.common.collect.Lists;
import com.opensymphony.xwork2.Preparable;

@SuppressWarnings("serial")
public class SedeAction extends BaseActionModelDrivenPaginado<Sede>
		implements Preparable {

	private boolean habilitarOKenabled = true;
	private IUsuarioService usuarioService;

	public boolean isHabilitarOKenabled() {
		return habilitarOKenabled;
	}

	public void setHabilitarOKenabled(boolean habilitarOKenabled) {
		this.habilitarOKenabled = habilitarOKenabled;
	}

	public SedeAction(ISedeService sedeService,IUsuarioService usuarioService){
		this.genericService = sedeService;
		this.usuarioService = usuarioService;
		setDisplaytagGridID("sedeGrid");
	}
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		if (getRequestId() != 0) {
			this.entity = genericService.findById(Long.valueOf(getRequestId()));
		} else {
			this.entity = new Sede();
		}

	}
	
	@Override
	public String save() {
		this.fillEntity();
		return super.save();
	}
	
	private void fillEntity() {
		Usuario usuario = usuarioService.findById(getSessionUser().getId());
		this.entity.setCreador(usuario);
	}

	@Override
    public String remove() {
		try{
	        genericService.remove(this.entity);
	        return list();
		}catch(Exception exception){
			addActionError("Hay torneos que hacen referencia a esta sede.");
	        setReadOnly(true);
	        setMappedRequest(Constantes.REMOVE);
	        return Constantes.SUCCESS;
		}
    }
	
	@Override
	public Sede getModel() {
		return this.getEntity();
	}

	@Override
	protected boolean tienePermiso() {
		return true;
	}
	
	@Override
	public Collection<Sede> getEntitiesToList() {
		Usuario usuario = getSessionUser();
		if(!usuario.isAnonimo() && "PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
			Collection<Sede> sedes = ((ISedeService)this.genericService).getSedesByUsuarioCreador(usuario);
			if(sedes != null){
				setRowCount(sedes.size());
			}
			return sedes;
		}
		return Lists.newArrayList();
	}

	@Override
	@SkipValidation
	public String edit() {
		Usuario usuario = getSessionUser();
		if(!usuario.isAnonimo() && "PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
			Sede sede = genericService.findById(Long.valueOf(getRequestId()));
			if(usuario.equals(sede.getCreador())){
				return super.edit();
			}
		}
		return ERROR;
	}

}
