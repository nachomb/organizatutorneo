package ar.com.noxit.picadito.web.acciones;

import ar.com.noxit.picadito.dominio.entidades.Dummy;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;

import com.opensymphony.xwork2.Preparable;

// public class ItemMenuAction extends BaseActionModelDrivenPaginado<ItemMenu> implements Preparable {
public class ItemMenuAction extends BaseActionModelDrivenPaginado<Dummy> implements Preparable {

    @Override
    public Dummy getModel() {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	protected boolean tienePermiso() {
		return false;
	}

    // private static final Long MAX_ORDEN = new Long("99999999999");
    // private Collection<ItemMenu> itemsMenu;
    // private Collection<Action> actions;
    // private ActionService actionService;
    //
    // private Long selectedActionID;
    // private Long selectedItemMenuPadreID;
    //
    // private boolean habilitarOKenabled = true;
    //
    // public boolean isHabilitarOKenabled() {
    // return habilitarOKenabled;
    // }
    //
    // public void setHabilitarOK(boolean habilitarOKenabled) {
    // this.habilitarOKenabled = habilitarOKenabled;
    // }
    //
    // public void setSelectedActionID(Long selectedActionID) {
    // this.selectedActionID = selectedActionID;
    // }
    //
    // public Long getSelectedActionID() {
    // return selectedActionID;
    // }
    //
    // public void setSelectedItemMenuPadreID(Long selectedItemMenuPadreID) {
    // this.selectedItemMenuPadreID = selectedItemMenuPadreID;
    // }
    //
    // public Long getSelectedItemMenuPadreID() {
    // return selectedItemMenuPadreID;
    // }
    //
    // public ActionService getActionService() {
    // return actionService;
    // }
    //
    // public void setActionService(ActionService actionService) {
    // this.actionService = actionService;
    // }
    //
    // public Collection<Action> getActions() {
    // return actions;
    // }
    //
    // public void setActions(Collection<Action> actions) {
    // this.actions = actions;
    // }
    //
    // public Collection<ItemMenu> getItemsMenu() {
    // return itemsMenu;
    // }
    //
    // public void setItemsMenu(Collection<ItemMenu> itemsMenu) {
    // this.itemsMenu = itemsMenu;
    // }
    //
    // public ItemMenuAction(ItemMenuService itemMenuService) {
    // this.genericService = itemMenuService;
    // this.setDisplaytagGridID("menuGrid"); // Nombre de la grilla de DisplayTag asociada al Paginado
    // }
    //
    // private void cleanItemSession() {
    // Map session = ActionContext.getContext().getSession();
    // MenuSession.getInstance().refreshMenuSession(session, (ItemMenuService) genericService);
    // }
    //
    // @Override
    // public String update() {
    // this.fillEntity();
    // String retorno = super.update();
    // cleanItemSession();
    // return retorno;
    // };
    //
    // @Override
    // public String save() {
    // this.fillEntity();
    // String retorno = super.save();
    // cleanItemSession();
    // return retorno;
    // }
    //
    // @Override
    // public String remove() {
    // String retorno = super.remove();
    // cleanItemSession();
    // return retorno;
    // }
    //
    // @Override
    // public String destroy() {
    //
    // if (((ItemMenuService) (this.genericService)).findAllItemsMenuHijosByPadre(this.getEntity()).size() > 0) {
    // addActionError("No se puede eliminar este menú porque tiene hijos.");
    // setHabilitarOK(false);
    // return Constantes.INPUT;
    // } else {
    // String retorno = super.destroy();
    // return retorno;
    // }
    // }
    //
    // private void fillEntity() {
    // ItemMenu itemMenuPadre = null;
    // Action action = null;
    //
    // if (getSelectedItemMenuPadreID() != -1)
    // itemMenuPadre = genericService.findById(getSelectedItemMenuPadreID());
    //
    // if (getSelectedActionID() != -1)
    // action = actionService.findById(getSelectedActionID());
    //
    // this.getEntity().setItemMenuPadre(itemMenuPadre);
    // this.getEntity().setAction(action);
    // this.getEntity().setLastModificationUser(getSessionUser());
    // }
    //
    // public void prepare() throws Exception {
    // super.prepare();
    // this.itemsMenu = getItemMenuService().getAllItemMenuPadres();
    // this.actions = this.actionService.getAll();
    //
    // if (getRequestId() != 0) {
    // this.entity = genericService.findById(Long.valueOf(getRequestId()));
    // Action action = this.entity.getAction();
    // if (action != null)
    // setSelectedActionID(action.getId());
    //
    // if (this.entity.getItemMenuPadre() != null)
    // setSelectedItemMenuPadreID(this.entity.getItemMenuPadre().getId());
    // } else {
    // this.entity = new ItemMenu();
    // }
    // }
    //
    // @Validations
    // public ItemMenu getModel() {
    // return entity;
    // }
    //
    // public void validate() {
    //
    // // Trims de los Strings
    // this.getEntity().setTitulo(this.getEntity().getTitulo().trim());
    // this.getEntity().setDescripcion(this.getEntity().getDescripcion().trim());
    //
    // if (this.getEntity().getTitulo().length() == 0) {
    // addFieldError("titulo", "Titulo requerido.");
    // }
    //
    // if (this.getEntity().getTitulo().length() > 20) {
    // addFieldError("titulo", "El título debe ser menor a 20 caracteres.");
    // }
    //
    // if (this.getEntity().getOrden() == null) {
    // addFieldError("orden", "Orden requerido.");
    // } else if (this.getEntity().getOrden() > MAX_ORDEN || this.getEntity().getOrden() < 0) {
    // addFieldError("orden", "El orden debe ser menor a " + MAX_ORDEN + " y mayor o igual a 0.");
    // }
    //
    // if (this.getEntity().getDescripcion().length() > 100) {
    // addFieldError("descripcion", "Descripción debe ser menor a 100 caracteres.");
    // }
    //
    // if (this.getSelectedActionID() == -1) {
    // addFieldError("selectedActionID", "La acción es requerida.");
    // } else {
    // // Si hay accion, pero su tipo es distinto de item_menu, debe ser un item menu padre: no puede tener menu
    // // padre asociado
    // if (getSelectedItemMenuPadreID() != -1
    // && getActionService().findById(getSelectedActionID()).getEntityType().getName().equals("item_menu")) {
    // addFieldError("selectedItemMenuPadreID",
    // "La acción elegida corresponde a un item de menú principal, por lo que no puede poseer menú padre asignado. ");
    // } else if (getSelectedItemMenuPadreID() == -1
    // && !getActionService().findById(getSelectedActionID()).getEntityType().getName().equals("item_menu")) {
    // addFieldError("selectedItemMenuPadreID",
    // "La acción elegida no corresponde a un item de menú principal, por lo que debe poseer menú padre asignado. ");
    // }
    // }
    //
    // // No se debe repetir el nombre del profile:
    // if (((ItemMenuService) genericService).isDuplicateTitulo(this.getEntity())) {
    // addFieldError("titulo", "El título ingresado ya existe.");
    // }
    //
    // }
    //
    // private ItemMenuService getItemMenuService() {
    // return (ItemMenuService) this.genericService;
    // }

}
