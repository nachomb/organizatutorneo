package ar.com.noxit.picadito.web.acciones;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.picadito.dominio.entidades.Dummy;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.servicios.IEquipoService;
import ar.com.noxit.servicios.IExcelExporterService;
import ar.com.noxit.servicios.IPartidoService;
import ar.com.noxit.servicios.impl.ITorneoService;

import com.opensymphony.xwork2.Preparable;

public class ExporterAction extends
		BaseActionModelDrivenPaginado<Dummy> implements Preparable {

	private static final long serialVersionUID = 7730191806491790180L;
	private Collection<Torneo> torneosPosibles;
	private Collection<Equipo> equipos;
	private ITorneoService torneoService;
	private String torneosSeleccionados;
	private String fechasSeleccionadas;
	private String partidosSeleccionados;
	private IExcelExporterService excelExporterService; 
	private IPartidoService partidoService;
	private IEquipoService equipoService;
	private String mail;
	private String selectedEquipos;
	
	public String getSelectedEquipos() {
		return selectedEquipos;
	}

	public void setSelectedEquipos(String selectedEquipos) {
		this.selectedEquipos = selectedEquipos;
	}

	public Collection<Equipo> getEquipos() {
		return equipos;
	}

	public void setEquipos(Collection<Equipo> equipos) {
		this.equipos = equipos;
	}
	
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPartidosSeleccionados() {
		return partidosSeleccionados;
	}

	public void setPartidosSeleccionados(String partidosSeleccionados) {
		this.partidosSeleccionados = partidosSeleccionados;
	}

	public String getFechasSeleccionadas() {
		return fechasSeleccionadas;
	}

	public void setFechasSeleccionadas(String fechasSeleccionadas) {
		this.fechasSeleccionadas = fechasSeleccionadas;
	}

	public Collection<Torneo> getTorneosPosibles() {
		return torneosPosibles;
	}

	public void setTorneosPosibles(Collection<Torneo> torneosPosibles) {
		this.torneosPosibles = torneosPosibles;
	}
	
	public String getTorneosSeleccionados() {
		return torneosSeleccionados;
	}

	public void setTorneosSeleccionados(String torneosSeleccionados) {
		this.torneosSeleccionados = torneosSeleccionados;
	}

	public ExporterAction(ITorneoService torneoService, IExcelExporterService excelExporterService,
			IPartidoService partidoService,IEquipoService equipoService){
		this.torneoService = torneoService;
		this.excelExporterService = excelExporterService;
		this.partidoService = partidoService;
		this.equipoService = equipoService;
	}
	
	@Override
	public Dummy getModel() {
		return null;
	}

	@Override
	protected boolean tienePermiso() {
		return true;
	}

	@Override
	public void prepare() throws Exception {
		this.torneosPosibles = this.torneoService.findActivos();
		this.mail = getSessionUser().getUsername();
	}

	public String estadisticas() {
		setMappedRequest("exportarEstadisticas");
		return Constantes.SUCCESS;
	}
	
	public String planilla() {
		setMappedRequest("exportarPlanilla");
		return Constantes.SUCCESS;
	}
	
	public String estadisticasHistoricas(){
		setMappedRequest("exportarEstadisticasHistoricas");
		this.setEquipos(this.equipoService.getAllActivos());
		return "estHis";
	}
	
	
	@SkipValidation
	public String exportarEstadisticasHistoricas(){
		List<Long> idEquipos = Utils.stringToLongList(selectedEquipos);
		List<Equipo> equiposSeleccionados = new ArrayList<Equipo>();
		
		for(Long id : idEquipos){
			Equipo equipo = this.equipoService.findById(id);
			equiposSeleccionados.add(equipo);
		}
		
		try {
			String nombreArchivo = this.excelExporterService.exportarEstadisticasHistoricas(equiposSeleccionados);
			escribirArchivoEnResponse(nombreArchivo);
		} catch (Exception e) {
			e.printStackTrace();
			return Constantes.ERROR;
		}
		return null;
	}
	
	@SkipValidation
	public String exportarEstadisticas(){
		List<Long> idTorneos = Utils.stringToLongList(torneosSeleccionados);
		List<Long> idFechas = Utils.stringToLongList(fechasSeleccionadas);
		
		try {
			String nombreArchivo = this.excelExporterService.exportarEstadisticas(idTorneos, idFechas);
			escribirArchivoEnResponse(nombreArchivo);
		} catch (Exception e) {
			e.printStackTrace();
			return Constantes.ERROR;
		}
		return null;
	}
	
	@SkipValidation
	public String exportarPlanilla(){
		List<Long> idPartidos = Utils.stringToLongList(partidosSeleccionados);
		List<Partido> partidos = new ArrayList<Partido>();
		for(Long id : idPartidos){
			partidos.add(this.partidoService.findById(id));
		}
		try {
			String nombreArchivo = this.excelExporterService.exportarPlanilla(partidos);
			escribirArchivoEnResponse(nombreArchivo);
		    
		} catch (Exception e) {
			e.printStackTrace();
			return Constantes.ERROR;
		}
		return null;
	}

	private void escribirArchivoEnResponse(String nombreArchivo)
			throws FileNotFoundException, IOException {
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition","attachment;filename="+nombreArchivo);
		
		FileInputStream in = new FileInputStream(new File(nombreArchivo));
		ServletOutputStream out = response.getOutputStream();
		
		byte[] outputByte = new byte[4096];
		//copy binary content to output stream
		while(in.read(outputByte, 0, 4096) != -1){
			out.write(outputByte, 0, 4096);
		}
		in.close();
		out.flush();
		out.close();
	}

}
