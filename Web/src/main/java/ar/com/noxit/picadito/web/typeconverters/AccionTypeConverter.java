/**
 * 
 */
package ar.com.noxit.picadito.web.typeconverters;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

import ar.com.noxit.picadito.dominio.entidades.seguridad.Accion;
import ar.com.noxit.seguridad.dao.IAccionDAO;
import ar.com.noxit.seguridad.dao.impl.AccionDAO;

public class AccionTypeConverter extends StrutsTypeConverter {

    private IAccionDAO accionDAO;

    @Override
    public Object convertFromString(Map map, String[] strings, Class toClass) {
        if (strings.length != 1) {
            super.performFallbackConversion(map, strings, toClass);
        }

        Accion action = null;
        String nombre = strings[0];

        if ((nombre != null) && (nombre.length() > 0)) {
            action = this.getAccionDAO().getAccionByName(nombre);
        }

        return action;
    }

    @Override
    public String convertToString(Map map, Object accion) {
        if ((accion != null))
            return accion.toString();
        return null;
    }

    public IAccionDAO getAccionDAO() {
        return this.accionDAO;
    }

    public void setAccionDAO(IAccionDAO accionDAO) {
        this.accionDAO = accionDAO;
    }

}
