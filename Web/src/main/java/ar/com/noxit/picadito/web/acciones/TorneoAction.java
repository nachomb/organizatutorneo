package ar.com.noxit.picadito.web.acciones;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Fecha;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Reporte;
import ar.com.noxit.picadito.dominio.entidades.ReporteHistorico;
import ar.com.noxit.picadito.dominio.entidades.Sede;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.seguridad.services.IUsuarioService;
import ar.com.noxit.servicios.IEquipoService;
import ar.com.noxit.servicios.IJugadorService;
import ar.com.noxit.servicios.ISedeService;
import ar.com.noxit.servicios.impl.ITorneoService;

import com.google.common.collect.Lists;
import com.opensymphony.xwork2.Preparable;

@SuppressWarnings("serial")
public class TorneoAction extends BaseActionModelDrivenPaginado<Torneo>
		implements Preparable {

	private static final String SEPARADOR_IDS_EQUIPOS = ",";
	private static final double BASE = 2;
	private boolean habilitarOKenabled = true;
	private ITorneoService torneoService;
	private IEquipoService equipoService;
	private IJugadorService jugadorService;
	private ISedeService sedeService;
	private Collection<Equipo> equiposListado = new ArrayList<Equipo>();
	private Collection<String> tiposTorneo = new ArrayList<String>();
	private Collection<Torneo> torneos;
	private Collection<Sede> sedes;
	private String selectedTorneo;

	private String selectedEquipos;
	private String selectedJugadores;
	private String sedeSeleccionada;
	private String datosGrafico = "";
	private String leyendaJugadores = "";
	private String coloresPuntos = "";
	private Reporte reporte;
	private ReporteHistorico reporteHistorico;
	private String idEquipo;
	private File upload;
	private static Collection<Fecha> previsualizacionFechas = new ArrayList<Fecha>();
	private Boolean act;
	private String selectedTorneosAnteriores;
	private IUsuarioService usuarioService;
	private long uid;
	
	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public IUsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(IUsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public String getSedeSeleccionada() {
		return sedeSeleccionada;
	}

	public void setSedeSeleccionada(String sedeSeleccionada) {
		this.sedeSeleccionada = sedeSeleccionada;
	}

	public Collection<Sede> getSedes() {
		return sedes;
	}

	public void setSedes(Collection<Sede> sedes) {
		this.sedes = sedes;
	}

	public String getSelectedTorneosAnteriores() {
		return selectedTorneosAnteriores;
	}

	public void setSelectedTorneosAnteriores(String selectedTorneosAnteriores) {
		this.selectedTorneosAnteriores = selectedTorneosAnteriores;
	}

	public Boolean getAct() {
		return act;
	}

	public void setAct(Boolean act) {
		this.act = act;
	}

	public Collection<Fecha> getPrevisualizacionFechas() {
		return previsualizacionFechas;
	}

	public void setPrevisualizacionFechas(Collection<Fecha> previsualizacionFechas) {
		TorneoAction.previsualizacionFechas = previsualizacionFechas;
	}

	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getSelectedTorneo() {
		return selectedTorneo;
	}

	public void setSelectedTorneo(String selectedTorneo) {
		this.selectedTorneo = selectedTorneo;
	}

	public void setEquipoService(IEquipoService equipoService) {
		this.equipoService = equipoService;
	}

	public IEquipoService getEquipoService() {
		return equipoService;
	}

	public boolean isHabilitarOKenabled() {
		return habilitarOKenabled;
	}

	public void setHabilitarOKenabled(boolean habilitarOKenabled) {
		this.habilitarOKenabled = habilitarOKenabled;
	}

	public ITorneoService getTorneoService() {
		return torneoService;
	}

	public void setTorneoService(ITorneoService torneoService) {
		this.torneoService = torneoService;
	}

	public void setSelectedEquipos(String selectedEquipos) {
		this.selectedEquipos = selectedEquipos;
	}

	public String getSelectedEquipos() {
		return selectedEquipos;
	}

	public void setEquiposListado(Collection<Equipo> equiposListado) {
		this.equiposListado = equiposListado;
	}

	public Collection<Equipo> getEquiposListado() {
		return equiposListado;
	}

	public void setTiposTorneo(Collection<String> tiposTorneo) {
		this.tiposTorneo = tiposTorneo;
	}

	public Collection<String> getTiposTorneo() {
		return tiposTorneo;
	}

	public void setDatosGrafico(String datosGrafico) {
		this.datosGrafico = datosGrafico;
	}

	public String getDatosGrafico() {
		return datosGrafico;
	}
	
	public Collection<Torneo> getTorneos() {
		return torneos;
	}

	public void setTorneos(Collection<Torneo> torneos) {
		this.torneos = torneos;
	}
	
	public ReporteHistorico getReporteHistorico() {
		return reporteHistorico;
	}
	
	public void setReporteHistorico(ReporteHistorico reporteHistorico) {
		this.reporteHistorico = reporteHistorico;
	}
	
	public String getIdEquipo() {
		return idEquipo;
	}

	public void setIdEquipo(String idEquipo) {
		this.idEquipo = idEquipo;
	}

	public Reporte getReporte() {
		return reporte;
	}

	public void setReporte(Reporte reporte) {
		this.reporte = reporte;
	}

	public String getColoresPuntos() {
		return coloresPuntos;
	}

	public void setColoresPuntos(String coloresPuntos) {
		this.coloresPuntos = coloresPuntos;
	}

	public String getColoresLineas() {
		return coloresLineas;
	}

	public void setColoresLineas(String coloresLineas) {
		this.coloresLineas = coloresLineas;
	}

	private String coloresLineas = "";

	public String getLeyendaJugadores() {
		return leyendaJugadores;
	}

	public void setLeyendaJugadores(String leyendaJugadores) {
		this.leyendaJugadores = leyendaJugadores;
	}

	public IJugadorService getJugadorService() {
		return jugadorService;
	}

	public void setJugadorService(IJugadorService jugadorService) {
		this.jugadorService = jugadorService;
	}

	public String getSelectedJugadores() {
		return selectedJugadores;
	}

	public void setSelectedJugadores(String selectedJugadores) {
		this.selectedJugadores = selectedJugadores;
	}

	public TorneoAction(ITorneoService torneoService, ISedeService sedeService, IUsuarioService usuarioService) {
		this.genericService = torneoService;
		this.sedeService = sedeService;
		this.usuarioService = usuarioService;
		setDisplaytagGridID("torneoGrid");
	}

	@Override
	public void prepare() throws Exception {
		super.prepare();
		Usuario sessionUser = getSessionUser();
		if (getRequestId() != 0) {
			this.entity = genericService.findById(Long.valueOf(getRequestId()));
		} else if(!sessionUser.isAnonimo()){
			this.entity = new Torneo();
			tiposTorneo.add("Todos contra Todos");
			tiposTorneo.add("Playoffs");
			equiposListado = this.equipoService.getEquiposByUsuarioCreador(sessionUser);
			sedes = this.sedeService.getSedesByUsuarioCreador(sessionUser);
			uid = sessionUser.getId();
		}
		getActionMessages().clear();
	}

	@Override
	public Torneo getModel() {
		return this.getEntity();
	}

	@Override
	public String save() {
		fillEntity();
		//this.getEntity().setFechas(new ArrayList<Fecha>(previsualizacionFechas));
		//TODO: ver que onda con este comentario
		Usuario usuario = getSessionUser();
		this.entity.setCreador(usuarioService.findById(usuario.getId()));
		try {
			this.entity.setRutaImagen(Utils.guadarImagen(upload));
		} catch (IOException e) {
			addActionError(e.getMessage());
		}
		return super.save();
	}

	public String publicar(){
		fillEntity();
		this.getEntity().setFechas(new ArrayList<Fecha>(previsualizacionFechas));
		this.genericService.saveOrUpdate(this.getEntity());
		return list();
	}
	
	@Override
	public String update() {
		fillEntity();
		return super.update();
	}

	private void fillEntity() {
		String eqs = getSelectedEquipos();
		List<Equipo> lista = obtenerEquiposSeleccionados(eqs);
		getEntity().setEquipos(lista);
		List<Torneo> torneosAnteriores = obtenerTorneosSeleccionados();
		getEntity().setTorneosAnteriores(torneosAnteriores);
		Sede sede = this.sedeService.findById(Long.valueOf(this.sedeSeleccionada));
		getEntity().setSede(sede);
	}

	private List<Torneo> obtenerTorneosSeleccionados() {
		List<Long> idList = Utils.stringToLongList(selectedTorneosAnteriores);
		List<Torneo> torneosAnteriores = new ArrayList<Torneo>();
		for(Long id : idList){
			torneosAnteriores.add(this.torneoService.findById(id));
		}
		return torneosAnteriores;
	}

	private List<Equipo> obtenerEquiposSeleccionados(String eqs) {
		List<Equipo> lista = new ArrayList<Equipo>();
		if (eqs != null && !eqs.trim().isEmpty()) {
			String[] strs = eqs.split(SEPARADOR_IDS_EQUIPOS);
			for (String id : strs) {
				try {
					Equipo eq = equipoService.findById(Long.valueOf(id.trim()));
					lista.add(eq);
				} catch (NumberFormatException e) {
					// Do nothing
				}
			}
		}
		return lista;
	}

	public void validate() {

		if (this.getEntity().getNombre().length() == 0) {
			addFieldError("nombre", "Nombre requerido.");
		}

		List<Equipo> eqs = obtenerEquiposSeleccionados(getSelectedEquipos());
		if (eqs == null || eqs.isEmpty() || eqs.size() == 1) {
			addFieldError("selectedEquipos",
					"Debe seleccionar al menos dos equipos para el torneo.");
		} else {
			//validarCantidadJugadoresPorEquipo(eqs);
		}

		if (this.getEntity().getTipoTorneo() == null
				|| this.getEntity().getTipoTorneo().equals("1")) {
			addFieldError("tipoTorneo", "Debe seleccionar un tipo de torneo.");
		} else if (this.getEntity().getTipoTorneo().equals("Playoffs")) {
			int cantEq = eqs.size();
			double fechas = Math.ceil(Math.log((double) cantEq)
					/ Math.log(BASE));
			if (((int) Math.pow(BASE, fechas)) != cantEq) {
				addFieldError(
						"selectedEquipos",
						"Cantidad de equipos incorrecta. Debe seleccionar una cantidad potencia de dos de equipos para el torneo tipo playoff.");
			}
		}

		if (this.getEntity().getJugadoresPorEquipo() != null
				&& this.getEntity().getJugadoresPorEquipo() < Torneo.DEFAULT_CANT_JUGADORES_X_EQUIPO) {
			addFieldError(
					"jugadoresPorEquipo",
					"Cantidad de jugadores por equipo errónea. Debe ingresar un número mayor o igual a "
							+ Torneo.DEFAULT_CANT_JUGADORES_X_EQUIPO);

		}

	}

	private void validarCantidadJugadoresPorEquipo(List<Equipo> eqs) {
		for (Equipo equipo : eqs) {
			if (!equipo.tieneCantidadMinimaJugadores(this.getEntity()
					.getJugadoresPorEquipo())) {
				addFieldError(
						"selectedEquipos",
						"Equipo "
								+ equipo.getNombre()
								+ " no tiene la cantidad mínima de jugadores para el torneo.");
			}
		}
	}

	@Override
	public String edit() {
		super.edit();
		if (!getEntity().estaIniciado()) {
			return Constantes.SUCCESS;
		}
		return list();
	}

	@Override
	public String remove() {
		// Eliminacion Logica SIEMPRE:
		this.entity.setActivo(false);
		genericService.update(this.entity);
		return list();
	}

	@SkipValidation
	public String iniciar() {
		validar();
		setMappedRequest("list");
		if (!getEntity().estaIniciado()) {
			getTorneoService().iniciarTorneo(getEntity());
		}
		return Constantes.SHOW;
	}

	@SkipValidation
	public String detalle() {
		setActionMethod("ConsultaTorneo");
		setReadOnly(true);
		return Constantes.SHOW;
	}

	@SkipValidation
	public String misTorneos() {
		setReadOnly(true);
		setActionMethod("MisTorneos");
		cargarMisTorneos();
		return Constantes.LIST;
	}

	private void cargarMisTorneos() {
		Usuario usuario = getSessionUser();
		if (!usuario.isAnonimo()) {
			List<Torneo> torneos = getTorneoService().getTorneosByUsuario(
					usuario);
			this.setRowCount(torneos.size());
			this.setEntities(torneos);
		} else {
			this.setRowCount(0);
			this.setEntities(new ArrayList<Torneo>());
		}
	}

	@SkipValidation
	public String tablaGoleadores() {
		setMappedRequest("graficoGoles");
		//setEntity(getTorneoService().calcularTablaGoleadores(getEntity()));
		return "tablaGoles";
	}

	@SkipValidation
	public String estadisticas() {
		if (!getEntity().isTodosContraTodos())
			return tablaGoleadores();
		setMappedRequest("list");
		return "estad";
	}

	@SkipValidation
	public String fixture() {
		setMappedRequest("list");
		return "fixture";
	}

	@Override
	protected boolean tienePermiso() {
		if (!getSessionUser().equals(entity.getCreador())) {
			return false;
		}
		return true;
	}

	@SkipValidation
	public String graficoGoles() {
		// TODO: ver la manera de no tener que volver a calcular la tabla de
		// goleadores
		setEntity(getTorneoService().calcularTablaGoleadores(getEntity()));
		List<Long> list = Utils.stringToLongList(selectedJugadores);
		int i = 1;
		for (Long idJugador : list) {
			Jugador j = jugadorService.findById(idJugador);
			datosGrafico += "1|" + getTorneoService().getDatosGrafico(entity, j);
			leyendaJugadores += j.toString();
			//harcodeando
			coloresPuntos += "o," + Utils.colores[i-1] + "," + Integer.valueOf(i-1) + ",-1,6";
			coloresLineas += Utils.colores[i-1];
			if (i != list.size()) {
				datosGrafico += "|";
				leyendaJugadores += "|";
				coloresPuntos += "|";
				coloresLineas += ",";
			}
			++i;
		}
		return "graficoGoles";
	}
	
	@SkipValidation
	public String reporte(){
		reporte = getTorneoService().generarReporte(entity);
		return "reporte";
	}

	@SkipValidation
	public void calcularMaxGoles() {
		this.entity.calcularMaxGoles();
	}
	
	@SkipValidation
	public String reporteHistorico(){
		if(idEquipo == null){
			equiposListado = equipoService.getAll();
			setMappedRequest("reporteHistorico");
			return "reporteHistorico";
		}
		Equipo equipo = equipoService.findById(new Long(idEquipo.trim()));
		reporteHistorico = torneoService.getReporteHistorico(equipo);
		return "reporteHistorico";
	}
	
	@SkipValidation
	public String tablaTarjetas(){
		setEntity(getTorneoService().calcularTarjetas(getEntity()));
		return "tarjetas";
		
	}
	
	@SkipValidation
	public String tablaSanciones(){
		setEntity(getTorneoService().calcularSanciones(getEntity()));
		return "sanciones";
		
	}
	
	@SkipValidation
	public String actualizar(){
		getTorneoService().actualizarPosiciones(getEntity().getId());
		
		List<String> messages = new ArrayList<String>();
		messages.add("Se actualizaron correctamente las posiciones del torneo " + getEntity().getNombre());
		setActionMessages(messages);
		return list();
	}
	
	@SkipValidation
	public String estadisticasTorneosActivos(){
		if(act == null || act == true){
			torneos = torneoService.findActivos();
		}else{
			torneos = torneoService.findNoActivos();
		}
		setActionMethod("Estadisticas");
		return Constantes.SUCCESS;
	}
	
	@SkipValidation
	public String fixtureTorneosActivos(){
		if(act == null || act == true){
			torneos = torneoService.findActivos();
		}else{
			torneos = torneoService.findNoActivos();
		}
		setActionMethod("Fixture");
		return Constantes.SUCCESS;
	}
	
	@SkipValidation
	public String previsualizacion(){
		if("".equals(selectedEquipos) || selectedEquipos == null)
			return Constantes.SUCCESS;
		previsualizacionFechas.clear();
		this.fillEntity();
		this.getEntity().calcularFecha();
		setPrevisualizacionFechas(this.getEntity().getFechas());
		return Constantes.SUCCESS;
	}

	@Override
	public Collection<Torneo> getEntitiesToList() {
		Usuario usuario = getSessionUser();
		if(!usuario.isAnonimo()){
			if("PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
				return getTorneosAndSetRowCount(usuario);
			}
		}else{
			usuario = usuarioService.findById(uid);
			return getTorneosAndSetRowCount(usuario);
		}
		return Lists.newArrayList();
	}

	private Collection<Torneo> getTorneosAndSetRowCount(Usuario usuario) {
		Collection<Torneo> torneos = this.torneoService.getTorneosByUsuarioCreador(usuario);
		if(torneos != null){
			setRowCount(torneos.size());
		}
		return torneos;
	}
	
}
