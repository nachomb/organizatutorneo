package ar.com.noxit.picadito.web.acciones;

import ar.com.noxit.picadito.dominio.entidades.Dummy;
import ar.com.noxit.picadito.web.base.BaseActionModelDriven;

import com.opensymphony.xwork2.Preparable;

@SuppressWarnings("serial")
public class PrincipalAction extends BaseActionModelDriven<Dummy> implements Preparable{

	public PrincipalAction(){
	}
	
	@Override
	public void prepare() throws Exception {
		
	}

	@Override
	public Dummy getModel() {
		return null;
	}

	@Override
	protected boolean tienePermiso() {
		return true;
	}

}
