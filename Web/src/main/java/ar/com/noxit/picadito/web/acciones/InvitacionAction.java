package ar.com.noxit.picadito.web.acciones;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.picadito.dominio.entidades.Invitacion;
import ar.com.noxit.picadito.dominio.entidades.PartidoAmistoso;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.servicios.IInvitacionService;
import ar.com.noxit.servicios.IPartidoAmistosoService;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public class InvitacionAction extends BaseActionModelDrivenPaginado<Invitacion> implements
Preparable, ModelDriven<Invitacion>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -111944621875541095L;
	private IPartidoAmistosoService partidoAmistosoService;
	private long idPartidoAmistoso;
	private String oper;
	private String men;
	private String dummy;

	public String getDummy() {
		return dummy;
	}

	public void setDummy(String dummy) {
		this.dummy = dummy;
	}

	public void setMen(String men) {
		this.men = men;
	}

	public String getMen() {
		return men;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getOper() {
		return oper;
	}

	public void setIdPartidoAmistoso(long idPartidoAmistoso) {
		this.idPartidoAmistoso = idPartidoAmistoso;
	}

	public long getIdPartidoAmistoso() {
		return idPartidoAmistoso;
	}

	public void setPartidoAmistosoService(IPartidoAmistosoService partidoAmistosoService) {
		this.partidoAmistosoService = partidoAmistosoService;
	}

	public IPartidoAmistosoService getPartidoAmistosoService() {
		return partidoAmistosoService;
	}
	
	public InvitacionAction(IInvitacionService invitacionService){
		this.genericService = invitacionService;
		this.setDisplaytagGridID("invitacionGrid");
	}

	@Override
	public Invitacion getModel() {
		return this.entity;
	}
	
	@Override
	public Collection<Invitacion> getEntitiesToList() {
		PartidoAmistoso partidoAmistoso = partidoAmistosoService.findById(getRequestId());
		this.setRowCount(partidoAmistoso.getInvitaciones().size());
		return partidoAmistoso.getInvitaciones();
	}
	
	private IInvitacionService getInvitacionService(){
		return (IInvitacionService) this.genericService;
	}

	@SkipValidation
	public String misInvitaciones() {
		setActionMethod("Invitacion_editGrid");
		cargarMisInvitaciones();
		return Constantes.LIST;
	}

	private void cargarMisInvitaciones() {
		Usuario usuario = getSessionUser();
		if (!usuario.isAnonimo()) {
			List<Invitacion> invitaciones = this.getInvitacionService().findByUsuario(usuario);
			this.setRowCount(invitaciones.size());
			this.setEntities(invitaciones);
		} else {
			this.setRowCount(0);
			this.setEntities(new ArrayList<Invitacion>());
		}
	}
	
	public String confirmado(){
		Invitacion invitacion = getInvitacionService().findById(getRequestId());
		getInvitacionService().cambiarEstadoAConfirmado(invitacion);
		setIdPartidoAmistoso(invitacion.getPartidoAmistoso().getId());
		if (!invitacion.getPartidoAmistoso().getCreador()
				.equals(getSessionUser()))
			return "invitaciones";
		return Constantes.SUCCESS;
		
	}
	
	public String cancelado(){
		Invitacion invitacion = getInvitacionService().findById(getRequestId());
		getInvitacionService().cambiarEstadoACancelado(invitacion);
		setIdPartidoAmistoso(invitacion.getPartidoAmistoso().getId());
		if (!invitacion.getPartidoAmistoso().getCreador()
				.equals(getSessionUser()))
			return "invitaciones";
		return Constantes.SUCCESS;
	}
	
	public String duda(){
		Invitacion invitacion = getInvitacionService().findById(getRequestId());
		getInvitacionService().cambiarEstadoADuda(invitacion);
		setIdPartidoAmistoso(invitacion.getPartidoAmistoso().getId());
		if (!invitacion.getPartidoAmistoso().getCreador().equals(getSessionUser()))
			return "invitaciones";
		return Constantes.SUCCESS;
	}

	public String editGrid() {
		Invitacion invitacion = getInvitacionService().findById(getRequestId());
		invitacion.setMensaje(getMen());
		if("confirmado".equals(getOper())){
			invitacion.cambiarEstadoAConfirmado();
		}else if("cancelado".equals(getOper())){
			invitacion.cambiarEstadoACancelado();
		}else if("en duda".equals(getOper())){
			invitacion.cambiarEstadoADuda();
		}
		getInvitacionService().update(invitacion);
		if (getDummy() != null ){
			return "invitaciones";
		}else{
			setIdPartidoAmistoso(invitacion.getPartidoAmistoso().getId());
			return Constantes.SUCCESS;
		}
	}

	@Override
	protected boolean tienePermiso() {
		return false;
	}

}
