package ar.com.noxit.picadito.web.acciones;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.PuntajePartido;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDriven;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.servicios.IEquipoService;
import ar.com.noxit.servicios.IGolService;
import ar.com.noxit.servicios.IJugadorService;
import ar.com.noxit.servicios.IPartidoService;
import ar.com.noxit.servicios.IPuntajePartidoService;
import ar.com.noxit.servicios.ISancionService;
import ar.com.noxit.servicios.ITarjetaService;
import ar.com.noxit.servicios.impl.ITorneoService;

import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.Validations;

public class PartidoAction extends BaseActionModelDriven<Partido> implements Preparable {

    private static final String SEPARADOR_JUGADORES = ",";

    /**
     * Serial id autogenerado
     */
    private static final long serialVersionUID = 219896311757797L;
    
    private static Logger logger = Logger.getLogger(PartidoAction.class);

    private String selectedTitularesLocales;
    private String selectedTitularesVisitantes;
    private String selectedSuplentesLocales;
    private String selectedSuplentesVisitantes;
    private String puntajesLocales;
    private String puntajesVisitantes;
    private String figura;
    private String numerosLocales;
    private String numerosVisitantes;
    private String golesLocales;
    private String golesVisitantes;
    private String amarillasLocales;
    private String amarillasVisitantes;
    private String rojasLocales;
    private String rojasVisitantes;
	private String selectedEquipoLocal;
	private String selectedEquipoVisitante;
	
	private List<Jugador> jugadoresLocalesNoSancionados = new ArrayList<Jugador>();
    private List<Jugador> jugadoresVisitantesNoSancionados = new ArrayList<Jugador>();

    /* Services */
    private ITarjetaService tarjetaService;
    private ITorneoService torneoService;
    private IGolService golService;
    private ISancionService sancionService;
    private IPuntajePartidoService puntajePartidoService;
    private IJugadorService jugadorService;

	private String datosGrafico;
    private String leyendaGrafico;

    private String fechaYHora;
	private IEquipoService equipoService;

	public PartidoAction(IPartidoService partidoService, IGolService golService, ITorneoService torneoService,
            ISancionService sancionService, IEquipoService equipoService) {
        this.genericService = partidoService;
        this.golService = golService;
        this.torneoService = torneoService;
        this.sancionService = sancionService;
        this.equipoService = equipoService;
    }

    @Override
    public void prepare() throws Exception {
        if (getRequestId() != 0) {
            setEntity(getPartidoService().findById(getRequestId()));
            if (this.entity.getFechaPartido() != null) {
                setFechaYHora(Utils.dateFormat.format(this.entity.getFechaPartido()));
            }
            if (this.entity != null) {
                cargarAlineacionesForm(entity);
                this.selectedEquipoLocal = this.entity.getEquipoLocal().getNombre();
                this.selectedEquipoVisitante = this.entity.getEquipoVisitante().getNombre();
            }
        }
    }

    private void cargarAlineacionesForm(Partido partido) {
        Collection<Jugador> titularesLocales = (partido.getAlineacionLocal() != null) ? partido.getAlineacionLocal()
                .getTitulares() : new ArrayList<Jugador>();
        Collection<Jugador> suplentesLocales = (partido.getAlineacionLocal() != null) ? partido.getAlineacionLocal()
                .getSuplentes() : new ArrayList<Jugador>();
        Collection<Jugador> titularesVisitantes = (partido.getAlineacionVisitante() != null) ? partido
                .getAlineacionVisitante().getTitulares() : new ArrayList<Jugador>();
        Collection<Jugador> suplentesVisitantes = (partido.getAlineacionVisitante() != null) ? partido
                .getAlineacionVisitante().getSuplentes() : new ArrayList<Jugador>();

        for (Jugador j : partido.getEquipoLocal().getJugadores()) {
            PuntajePartido punt = puntajePartidoService.findByJugadorPartido(j, partido);
            if (punt != null){
            	j.setPuntajePartido(punt.getPuntaje());
            	j.setEsFigura(punt.getFigura());
            	j.setNumero(punt.getNumero());
            	//TODO:BORRAR
                j.setCantGolesPartido(golService.countGoles(j,partido).intValue());
                j.setCantAmarillasPartido(tarjetaService.countTarjetasAmarillas(j,partido).intValue());
                j.setCantRojasPartido(tarjetaService.countTarjetasRojas(j,partido).intValue());
                //fin borrar
            }
            if (titularesLocales.contains(j)) {
                j.setConvocado("T");
                
            } else if (suplentesLocales.contains(j)) {
                j.setConvocado("S");
            }
        }
        for (Jugador j : partido.getEquipoVisitante().getJugadores()) {
            PuntajePartido punt = puntajePartidoService.findByJugadorPartido(j, partido);
            if (punt != null){
                j.setPuntajePartido(punt.getPuntaje());
            	j.setEsFigura(punt.getFigura());
            	j.setNumero(punt.getNumero());
            	
            	//TODO:BORRAR
                j.setCantGolesPartido(golService.countGoles(j,partido).intValue());
                j.setCantAmarillasPartido(tarjetaService.countTarjetasAmarillas(j,partido).intValue());
                j.setCantRojasPartido(tarjetaService.countTarjetasRojas(j,partido).intValue());
                //fin borrar
                
            }
            if (titularesVisitantes.contains(j)) {
                j.setConvocado("T");
            } else if (suplentesVisitantes.contains(j)) {
                j.setConvocado("S");
            }
        }
    }

    @Override
    public String input() {
        setMappedRequest("cargar");
        //TODO: descomentar y corregir
        //verificarSanciones();
        return Constantes.SUCCESS;
    }

//    private void verificarSanciones() {
//        List<Sancion> sanciones = getSancionService().getSancionesNoCumplidas(this.entity.getEquipoLocal(),
//                this.entity.getEquipoVisitante());
//        for (Sancion sancion : sanciones) {
//            if (!sancion.getPartidoInicio().equals(entity)) {
//                sancion.agregarPartidoSancionCumplida(entity);
//                getSancionService().update(sancion);
//            }
//        }
//    }
    
	private boolean cambiarEquipos() {
		boolean cambioEquipo = false;
		if(!selectedEquipoLocal.equals(this.getEntity().getEquipoLocal().getNombre())){
			this.getEntity().setEquipoLocal(getEquipo(selectedEquipoLocal));
			cambioEquipo = true;
		}
		if(!selectedEquipoVisitante.equals(this.getEntity().getEquipoVisitante().getNombre())){
			this.getEntity().setEquipoVisitante(getEquipo(selectedEquipoVisitante));
			cambioEquipo = true;
		}
		return cambioEquipo;
	}

	private Equipo getEquipo(String equipo) {
		Equipo eq = null;
		if (equipo != null && !equipo.trim().isEmpty()) {
			eq = equipoService.getEquipoPorNombre(equipo.trim());
		}
		return eq;
	}

    @Validations
    public String cargar() {
    	if(cambiarEquipos()){
    		return "cargado";
    	}
    	
        cargarAlineaciones();
        cargarPuntajes();
        cargarGoleadores();
        cargarTarjetas();
        cargarFecha();
        //TODO: descomentar y corregir
        //cargarSanciones();
        getTorneoService().continuarTorneo(getEntity().getFecha().getTorneo());
        if(getEntity().getFecha().isJugada())
        	getTorneoService().actualizarPosiciones(getEntity().getFecha().getTorneo().getId());
        // getPartidoService().update(getEntity());
        return "cargado";
    }

    private void cargarPuntajes() {
    	logger.info("cargando puntajes...");
        List<Long> puntLocales = Utils.stringToLongList(puntajesLocales);
        List<Long> puntVisitantes = Utils.stringToLongList(puntajesVisitantes);
        Map<Long, Long> numLocales = Utils.stringToMap(numerosLocales);
        Map<Long, Long> numVisitantes = Utils.stringToMap(numerosVisitantes);
        Long idJugFigura = new Long(figura.trim());
        Jugador jugFigura = jugadorService.findById(idJugFigura);
        getPuntajePartidoService().generarPuntajes(numLocales, puntLocales, jugFigura, getEntity(), getEntity().getEquipoLocal(),
                getEntity().getAlineacionLocal());
        getPuntajePartidoService().generarPuntajes(numVisitantes, puntVisitantes, jugFigura, getEntity(), getEntity().getEquipoVisitante(),
                getEntity().getAlineacionVisitante());
    	logger.info("FIN cargando puntajes...");
    }

    private void cargarFecha() {
        try {
            if (fechaYHora != null && fechaYHora.trim().length() != 0) {
                Date fecha = Utils.dateFormat.parse(fechaYHora);
                this.getEntity().setFechaPartido(fecha);
            }
        } catch (ParseException ex) {
            // No hace nada
        }
    }

    private void cargarGoleadores() {
    	logger.info("cargando goleadores...");
        Map<Long, Long> goleadoresLocales = Utils.stringToMap(this.golesLocales);
        Map<Long, Long> goleadoresVisitantes = Utils.stringToMap(this.golesVisitantes);
        logger.info("eliminando goles...");
        getGolService().eliminarGoles(getEntity());
        
        getGolService().generarGoles(goleadoresLocales, getEntity());
        getGolService().generarGoles(goleadoresVisitantes, getEntity());
        logger.info("FIN cargando goleadores...");
    }

    private void cargarAlineaciones() {
    	logger.info("cargando alineaciones...");
        List<Long> idsTitularesLocales = obtenerIdsJugadores(getSelectedTitularesLocales());
        List<Long> idsTitularesVisitantes = obtenerIdsJugadores(getSelectedTitularesVisitantes());
        List<Long> idsSuplentesLocales = obtenerIdsJugadores(getSelectedSuplentesLocales());
        List<Long> idsSuplentesVisitantes = obtenerIdsJugadores(getSelectedSuplentesVisitantes());
        getPartidoService().cargarAlineaciones(getEntity(), idsTitularesLocales, idsTitularesVisitantes,
                idsSuplentesLocales, idsSuplentesVisitantes);
        logger.info("FIN cargando alineaciones...");
    }

    private void cargarTarjetas() {
    	logger.info("cargando tarjetas...");
        Map<Long, Long> tarjetasAmarillasLocales = Utils.stringToMap(this.amarillasLocales);
        Map<Long, Long> tarjetasAmarillasVisitantes = Utils.stringToMap(this.amarillasVisitantes);
        Map<Long, Long> tarjetasRojasLocales = Utils.stringToMap(this.rojasLocales);
        Map<Long, Long> tarjetasRojasVisitantes = Utils.stringToMap(this.rojasVisitantes);
        
        logger.info("eliminando tarjetas del partido: " + getEntity().getId());
        getTarjetaService().borrarAmarillas(getEntity());
        getTarjetaService().borrarRojas(getEntity());
        
        getTarjetaService().generarAmarillas(tarjetasAmarillasLocales, getEntity());
        getTarjetaService().generarAmarillas(tarjetasAmarillasVisitantes, getEntity());
        getTarjetaService().generarRojas(tarjetasRojasLocales, getEntity());
        getTarjetaService().generarRojas(tarjetasRojasVisitantes, getEntity());
    	logger.info("FIN cargando tarjetas...");
    }

    private void cargarSanciones() {
    	logger.info("cargando sanciones...");
        getSancionService().generarSanciones(getEntity());
        logger.info("FIN cargando sanciones...");
    }

    private List<Long> obtenerIdsJugadores(String titulares) {
        List<Long> ids = new ArrayList<Long>();
        for (String id : titulares.split(SEPARADOR_JUGADORES)) {
            try {
                Long valor = Long.valueOf(id.trim());
                ids.add(valor);
            } catch (NumberFormatException ex) {
                // Hacer Nada :P
            }
        }
        return ids;
    }

    @Override
    public void validate() {
        // comento validacion para pruebas, descomentar luego.
        validarFecha();
    }

    private void validarFecha() {
        try {
            if (fechaYHora != null && !fechaYHora.trim().isEmpty()) {
            	Utils.dateFormat.parse(fechaYHora);
            }
        } catch (ParseException ex) {
            addFieldError("fechaYHora", "Formato de la fecha inválida");
        }
    }

    @SkipValidation
    public String detalle() {
        setActionMethod("ConsultaTorneo");
        setReadOnly(true);
        return Constantes.SUCCESS;
    }

    @Override
    public String remove() {
        getEntity().setActivo(false);
        return super.update();
    }

    @Override
    public Partido getModel() {
        return this.entity;
    }

    @Override
    protected boolean tienePermiso() {
        return false;
    }

	@Override
	@SkipValidation
	public String edit() {
		Usuario usuario = getSessionUser();
		if(!usuario.isAnonimo() && "PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
			Partido partido = genericService.findById(Long.valueOf(getRequestId()));
			if(usuario.equals(partido.getFecha().getTorneo().getCreador())){
				return super.edit();
			}
		}
		return ERROR;
	}
    
    @SkipValidation
    public String historial() {
        List<String> datos = ((IPartidoService) this.genericService).getDatosGraficoHistorial(this.entity);
        datosGrafico = datos.get(0) + "," + datos.get(1) + "," + datos.get(2);
        leyendaGrafico = this.entity.getEquipoLocal().getNombre() + " ganó: " + datos.get(0) + "| Empataron: "
                + datos.get(1) + "|" + this.entity.getEquipoVisitante().getNombre() + " ganó: " + datos.get(2);
        return Constantes.SUCCESS;
    }
    
    public void setPartidoService(IPartidoService partidoService) {
        this.genericService = partidoService;
    }

    public IPartidoService getPartidoService() {
        return (IPartidoService) this.genericService;
    }

    public void setSelectedTitularesLocales(String selectedTitularesLocales) {
        this.selectedTitularesLocales = selectedTitularesLocales;
    }

    public String getSelectedTitularesLocales() {
        return selectedTitularesLocales;
    }

    public void setSelectedTitularesVisitantes(String selectedTitularesVisitantes) {
        this.selectedTitularesVisitantes = selectedTitularesVisitantes;
    }

    public String getSelectedTitularesVisitantes() {
        return selectedTitularesVisitantes;
    }

    public void setGolService(IGolService golService) {
        this.golService = golService;
    }

    public IGolService getGolService() {
        return golService;
    }

    public ITarjetaService getTarjetaService() {
        return tarjetaService;
    }

    public void setTarjetaService(ITarjetaService tarjetaService) {
        this.tarjetaService = tarjetaService;
    }

    public ITorneoService getTorneoService() {
        return this.torneoService;
    }

    public void setTorneoService(ITorneoService torneoService) {
        this.torneoService = torneoService;
    }

    public void setSancionService(ISancionService sancionService) {
        this.sancionService = sancionService;
    }

    public ISancionService getSancionService() {
        return sancionService;
    }

    public void setJugadoresLocalesNoSancionados(List<Jugador> jugadoresLocalesNoSancionados) {
        this.jugadoresLocalesNoSancionados = jugadoresLocalesNoSancionados;
    }

    public List<Jugador> getJugadoresLocalesNoSancionados() {
        return jugadoresLocalesNoSancionados;
    }

    public void setJugadoresVisitantesNoSancionados(List<Jugador> jugadoresVisitantesNoSancionados) {
        this.jugadoresVisitantesNoSancionados = jugadoresVisitantesNoSancionados;
    }

    public List<Jugador> getJugadoresVisitantesNoSancionados() {
        return jugadoresVisitantesNoSancionados;
    }

	public String getNumerosVisitantes() {
		return numerosVisitantes;
	}

	public void setNumerosVisitantes(String numerosVisitantes) {
		this.numerosVisitantes = numerosVisitantes;
	}

	public String getNumerosLocales() {
		return numerosLocales;
	}

	public void setNumerosLocales(String numerosLocales) {
		this.numerosLocales = numerosLocales;
	}
    
    public String getAmarillasLocales() {
    	return amarillasLocales;
    }
    
    public void setAmarillasLocales(String amarillasLocales) {
    	this.amarillasLocales = amarillasLocales;
    }
    
    public String getAmarillasVisitantes() {
    	return amarillasVisitantes;
    }
    
    public void setAmarillasVisitantes(String amarillasVisitantes) {
    	this.amarillasVisitantes = amarillasVisitantes;
    }
    
    public String getRojasLocales() {
    	return rojasLocales;
    }
    
    public void setRojasLocales(String rojasLocales) {
    	this.rojasLocales = rojasLocales;
    }
    
    public String getRojasVisitantes() {
    	return rojasVisitantes;
    }
    
    public void setRojasVisitantes(String rojasVisitantes) {
    	this.rojasVisitantes = rojasVisitantes;
    }
    
	public String getGolesLocales() {
		return golesLocales;
	}

	public void setGolesLocales(String golesLocales) {
		this.golesLocales = golesLocales;
	}

	public String getGolesVisitantes() {
		return golesVisitantes;
	}

	public void setGolesVisitantes(String golesVisitantes) {
		this.golesVisitantes = golesVisitantes;
	}
    
    public IJugadorService getJugadorService() {
    	return jugadorService;
    }
    
    public void setJugadorService(IJugadorService jugadorService) {
    	this.jugadorService = jugadorService;
    }
    public String getFigura() {
    	return figura;
    }
    
    public void setFigura(String figura) {
    	this.figura = figura;
    }
    public IPuntajePartidoService getPuntajePartidoService() {
        return puntajePartidoService;
    }

    public void setPuntajePartidoService(IPuntajePartidoService puntajePartidoService) {
        this.puntajePartidoService = puntajePartidoService;
    }

    public String getPuntajesLocales() {
        return puntajesLocales;
    }

    public void setPuntajesLocales(String puntajesLocales) {
        this.puntajesLocales = puntajesLocales;
    }

    public String getPuntajesVisitantes() {
        return puntajesVisitantes;
    }

    public void setPuntajesVisitantes(String puntajesVisitantes) {
        this.puntajesVisitantes = puntajesVisitantes;
    }

    @SkipValidation
    public String getFechaYHora() {
		return fechaYHora;
    }

    public void setFechaYHora(String fechaYHora) {
        this.fechaYHora = fechaYHora;
    }

    public String getLeyendaGrafico() {
        return leyendaGrafico;
    }

    public void setLeyendaGrafico(String leyendaGrafico) {
        this.leyendaGrafico = leyendaGrafico;
    }

    public String getDatosGrafico() {
        return datosGrafico;

    }

    public void setDatosGrafico(String datosGrafico) {
        this.datosGrafico = datosGrafico;
    }

    public String getSelectedSuplentesLocales() {
        return selectedSuplentesLocales;

    }

    public void setSelectedSuplentesLocales(String selectedSuplentesLocales) {
        this.selectedSuplentesLocales = selectedSuplentesLocales;
    }

    public String getSelectedSuplentesVisitantes() {
        return selectedSuplentesVisitantes;
    }

    public void setSelectedSuplentesVisitantes(String selectedSuplentesVisitantes) {
        this.selectedSuplentesVisitantes = selectedSuplentesVisitantes;
    }
    
	public String getSelectedEquipoLocal() {
		return selectedEquipoLocal;
	}

	public void setSelectedEquipoLocal(String selectedEquipoLocal) {
		this.selectedEquipoLocal = selectedEquipoLocal;
	}

	public String getSelectedEquipoVisitante() {
		return selectedEquipoVisitante;
	}

	public void setSelectedEquipoVisitante(String selectedEquipoVisitante) {
		this.selectedEquipoVisitante = selectedEquipoVisitante;
	}

}
