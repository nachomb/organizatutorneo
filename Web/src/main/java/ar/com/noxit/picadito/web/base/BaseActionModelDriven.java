package ar.com.noxit.picadito.web.base;

import org.apache.struts2.interceptor.validation.SkipValidation;


import com.opensymphony.xwork2.ModelDriven;

/**
 * Action generico para la elaboracion de CRUDs
 * 
 * @param <T>
 *            entidad asociada al CRUD
 * 
 */
public abstract class BaseActionModelDriven<T> extends BaseAction<T> implements ModelDriven<T> {

    private static final long serialVersionUID = 1L;

    /**
     * Mtodo utilizado por OGNL para manejo de entidades
     * 
     * @return retorna la entidad
     */
    @SkipValidation
    public abstract T getModel();

}
