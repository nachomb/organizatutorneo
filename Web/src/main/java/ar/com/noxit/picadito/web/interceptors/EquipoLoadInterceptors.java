package ar.com.noxit.picadito.web.interceptors;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import ar.com.noxit.picadito.web.utils.MenuSession;
import ar.com.noxit.servicios.IEquipoService;
import ar.com.noxit.servicios.IPublicidadService;
import ar.com.noxit.servicios.impl.ITorneoService;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class EquipoLoadInterceptors extends AbstractInterceptor implements
		Serializable {

	private static final long serialVersionUID = -3050842617173936191L;
	private IEquipoService equipoService;
	private ITorneoService torneoService;
	private IPublicidadService publicidadService;
	
	public void setPublicidadService(IPublicidadService publicidadService) {
		this.publicidadService = publicidadService;
	}

	public void setTorneoService(ITorneoService torneoService) {
		this.torneoService = torneoService;
	}

	public void setEquipoService(IEquipoService equipoService) {
		this.equipoService = equipoService;
	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
       // Usuario usuario = (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        //if (!usuario.isAnonimo()) {
            Map session = ActionContext.getContext().getSession();
//            if (session.get("equipos") == null) {
//                MenuSession.getInstance().refreshEquipos(session, equipoService);
//            }
            String[] uidArray = (String[])invocation.getInvocationContext().getParameters().get("uid");
            String uid = null;
            if(uidArray != null && uidArray.length == 1){
            	uid = uidArray[0];
            }
            String sessionUid = (String)session.get("uid");
			if (!StringUtils.equals(sessionUid, uid) && uid != null) {
				session.put("uid", uid);
				MenuSession.getInstance().refreshTorneos(session, torneoService, uid);
            }
            
//            if(session.get("publicidadesVerticales") == null){
//            	session.put("publicidadesVerticales",publicidadService.getPublicidadesVerticalesActivas());
//            }else{
//            	List publicidades = (List) session.get("publicidadesVerticales");
//            	if(publicidades != null){
//            		Collections.shuffle(publicidades);
//            		session.put("publicidadesVerticales",publicidades);
//            	}
//            }
//            
//            if(session.get("publicidadesSlider") == null){
//            	session.put("publicidadesSlider",publicidadService.getPublicidadesSliderActivas());
//            }else{
//            	List publicidades = (List) session.get("publicidadesSlider");
//            	if(publicidades != null){
//            		Collections.shuffle(publicidades);
//            		session.put("publicidadesSlider",publicidades);
//            	}
//            }
        //}

        return invocation.invoke();
	}

}
