package ar.com.noxit.picadito.web.base;

import java.util.Collection;
import java.util.Map;

import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.constantes.Constantes;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Action generico para la elaboracion de CRUDs
 * 
 * @param <T>
 *            tipo de entidad asociada al CRUD
 * 
 */
public abstract class BaseAction<T> extends ActionSupport implements SessionAware {

    protected IGenericService<T> genericService;

    protected long requestId = 0;

    private boolean readOnly = false;

    private String mappedRequest;

    protected T entity;

    protected Collection<T> entities;

    protected Map session;

	protected void validar() {
//		if(perfilAdmin())
//			return;
//		if(!tienePermiso()){
//        	throw new RuntimeException("Accion no permitida");
//		}
	}
	
	abstract protected boolean tienePermiso();

	private boolean perfilAdmin() {
		Usuario usuario = getSessionUser();
		for(Perfil perfil : usuario.getPerfiles()){
			if("PADMIN".equals(perfil.getNombre())){
				return true;
			}
		}
		return false;
	}
    /**
     * Mapea al formulario de la entidad en solo lectura
     * 
     * @return retorna condigo de exito
     */
    @SkipValidation
    public String show() {
        setReadOnly(true);
        setMappedRequest(Constantes.LIST);
        return Constantes.SUCCESS;
    }

    /**
     * Mapea al formulario de la entidad para ser insertada
     * 
     * @return retorna condigo de exito
     */
    @SkipValidation
    public String add() {
    	validar();
        setMappedRequest(Constantes.SAVE);
        return Constantes.SUCCESS;
    }

    /**
     * Realiza la insercion en la base de datos
     * 
     * @return retorna codigo de listado
     */
    public String save() {
        genericService.persist(this.entity);
        return list();
    }

    /**
     * Mapea al formulario de la entidad para ser editada
     * 
     * @return retorna codigo de exito
     */
    @SkipValidation
    public String edit() {
    	validar();
        setMappedRequest(Constantes.UPDATE);
        return Constantes.SUCCESS;
    }

    /**
     * Realiza la actualizacion en la base
     * 
     * @return retorna codigo de listado
     */
    public String update() {
        genericService.update(this.entity);
        return list();
    }

    /**
     * Mapea al formulario para eliminar una entidad
     * 
     * @return retorna codigo de exito
     */
    @SkipValidation
    public String destroy() {
    	validar();
        setReadOnly(true);
        setMappedRequest(Constantes.REMOVE);
        return Constantes.SUCCESS;
    }

    /**
     * Elimina la entidad seleccionada
     * 
     * @return retorna codigo de salida exitoso
     */
    @SkipValidation
    public String remove() {
        genericService.remove(this.entity);
        return list();
    }

    /**
     * Mapea al formulario que lista las entidades * Si se quiere filtrar la lista a mostrar se debe redefinir el mtodo
     * getEntitiesToList()
     * 
     * @return retorna codigo de listado
     */
    @SkipValidation
    public String list() {
        this.setEntities(this.getEntitiesToList());
        this.setMappedRequest(Constantes.LIST);
        return Constantes.LIST;
    }

    /**
     * Mapea el metodo de solo consulta activando el modo de solo lectura
     * 
     * @return codigo de listado
     */
    @SkipValidation
    public String listSoloConsulta() {
        setReadOnly(true);
        return list();
    }

    @SkipValidation
    public String consulta() {
        setReadOnly(true);
        setMappedRequest(Constantes.LIST_CONSULTA);
        return Constantes.SUCCESS;
    }

    @SkipValidation
    public String getActionClass() {
        return (String) getClass().getSimpleName().substring(0, getClass().getSimpleName().indexOf("Action"));
    }

    @SkipValidation
    public String getDestination() {
        return (String) getClass().getSimpleName().substring(0, getClass().getSimpleName().indexOf("Action"));
    }

    @SkipValidation
    public String getActionMethod() {
        return mappedRequest;
    }

    @SkipValidation
    public void setActionMethod(String method) {
        this.mappedRequest = method;
    }

    /**
     * Arma la accion teniendo en cuenta el proximo metodo a ejecutar
     * 
     * @param actionMethod
     */
    @SkipValidation
    public void setMappedRequest(String actionMethod) {
        this.mappedRequest = getActionClass() + "_" + actionMethod;
    }

    /**
     * Setea el formulario como solo lectura o lectura/escrotura
     * 
     * @param readOnly
     */
    @SkipValidation
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    @SkipValidation
    public long getRequestId() {
        return requestId;
    }

    @SkipValidation
    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    /**
     * Indica si el formulario es de solo lectura
     * 
     * @return retorna verdadero si es de solo lectura
     */
    @SkipValidation
    public boolean isReadOnly() {
        return readOnly;
    }

    @SkipValidation
    public Collection<T> getEntities() {
        return entities;
    }

    @SkipValidation
    public void setEntities(Collection<T> entities) {
        this.entities = entities;
    }

    @SkipValidation
    public T getEntity() {
        return entity;
    }

    @SkipValidation
    public void setEntity(T entity) {
        this.entity = entity;
    }

    /**
     * Mtodo que obtiene las entities a mostrar cuando se quieren listar. Redefinirlo en caso que se quiera aplicar algn
     * filtro a la lista
     * 
     * @return retorna una coleccin de las entities a mostrar
     */
    @SkipValidation
    public Collection<T> getEntitiesToList() {
        return this.genericService.getAll();
    }

    public Map getSession() {
        return session;
    }

    public void setSession(Map session) {
        this.session = session;
    }

    /**
     * Devuelve el User asociado a la session actual
     * 
     * @return
     */
    @SkipValidation
    protected Usuario getSessionUser() {
        return ((Usuario) (SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
    }

}
