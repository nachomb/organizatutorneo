package ar.com.noxit.picadito.web.acciones;

import java.util.Map;

import org.acegisecurity.context.SecurityContextHolder;

import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDriven;
import ar.com.noxit.seguridad.services.IUsuarioService;

import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.Validations;

@SuppressWarnings("serial")
public class ModificarClaveAction extends BaseActionModelDriven<Usuario> implements Preparable {

    private String claveVieja;
    private String claveNueva;
    private String claveNuevaRepetida;

    public ModificarClaveAction(IUsuarioService usuarioService) {
        this.genericService = usuarioService;
    }

    public String execute() throws Exception {
        return this.update();
    }

    @Override
    public String update() {
        this.fillEntity();
        return super.update();
    };

    private void fillEntity() {
        Long id = this.entity.getId();
        this.entity = genericService.findById(id);

        ((IUsuarioService) this.genericService).encodeAndSetPassword(this.getEntity(), claveNueva);
    }

    @Override
    public String save() {
        this.fillEntity();
        return super.save();
    }

    public void prepare() throws Exception {
        this.entity = ((Usuario) (SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
    }

    @Validations
    public Usuario getModel() {
        return entity;
    }

    public void validate() {

        String claveViejaCodificada = ((IUsuarioService) this.genericService).encodePassword(claveVieja);
        if (!this.getEntity().getPassword().equals(claveViejaCodificada)) {
            addFieldError("claveVieja", "Contraseña invalida.");
        }

        if (this.getClaveNueva().length() == 0) {
            addFieldError("claveNueva", "Contraseña nueva requerida.");
        }

        if (!this.getClaveNueva().equals(this.getClaveNuevaRepetida())) {
            addFieldError("claveNuevaRepetida", "Las contraseñas no coinciden.");
        }

        if (((IUsuarioService) this.genericService).encodePassword(this.getClaveNueva()).equals(claveViejaCodificada)) {
            addFieldError("claveNueva", "Las nueva contraseña no puede ser igual a la anterior .");
        }
    }

    public String getClaveVieja() {
        return claveVieja;
    }

    public void setClaveVieja(String claveVieja) {
        this.claveVieja = claveVieja;
    }

    public String getClaveNueva() {
        return claveNueva;
    }

    public void setClaveNueva(String claveNueva) {
        this.claveNueva = claveNueva;
    }

    public String getClaveNuevaRepetida() {
        return claveNuevaRepetida;
    }

    public void setClaveNuevaRepetida(String claveNuevaRepetida) {
        this.claveNuevaRepetida = claveNuevaRepetida;
    }

	@Override
	protected boolean tienePermiso() {
		if(!getSessionUser().equals(this.entity)){
			return false;
		}
		return true;
	}
	

}
