package ar.com.noxit.picadito.web.acciones;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.acegisecurity.AuthenticationException;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.providers.ProviderManager;
import org.acegisecurity.providers.UsernamePasswordAuthenticationToken;
import org.acegisecurity.providers.dao.DaoAuthenticationProvider;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.base.utils.Mail;
import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.seguridad.services.IPerfilService;
import ar.com.noxit.seguridad.services.IUsuarioService;
import ar.com.noxit.seguridad.services.impl.DetallesUsuarioService;
import ar.com.noxit.servicios.IJugadorService;

import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.User;

@SuppressWarnings("serial")
public class UsuarioAction extends BaseActionModelDrivenPaginado<Usuario> implements Preparable {

    private IPerfilService perfilService;
    private IJugadorService jugadorService;
    private Collection<Perfil> availableProfiles;
    private String selectedProfileIDs;

    private String claveSinEncriptar;
    private String claveSinEncriptarRepetida;

    private String nombreJugador;
    private String apellidoJugador;
    private String apodoJugador;

    private boolean habilitarOKenabled = true;
    private boolean crearJugador = false;

    private String code;
    private Mail mailService;
    private String email;

    private ProviderManager authenticationManager;

    private DetallesUsuarioService detallesUsuarioService;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Mail getMailService() {
        return mailService;
    }

    public void setMailService(Mail mailService) {
        this.mailService = mailService;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isCrearJugador() {
        return crearJugador;
    }

    public void setCrearJugador(boolean crearJugador) {
        this.crearJugador = crearJugador;
    }

    public String getNombreJugador() {
        return nombreJugador;
    }

    public void setNombreJugador(String nombreJugador) {
        this.nombreJugador = nombreJugador;
    }

    public String getApellidoJugador() {
        return apellidoJugador;
    }

    public void setApellidoJugador(String apellidoJugador) {
        this.apellidoJugador = apellidoJugador;
    }

    public String getApodoJugador() {
        return apodoJugador;
    }

    public void setApodoJugador(String apodoJugador) {
        this.apodoJugador = apodoJugador;
    }

    public UsuarioAction(IUsuarioService userService) {
        this.genericService = userService;
        this.setDisplaytagGridID("userGrid"); // Nombre de la grilla de DisplayTag asociada al Paginado
    }

    @Override
    public String destroy() {

        // Validamos antes de eliminar que no nos estemos "autodestruyendo"...
        Usuario esteUsuario = getSessionUser();

        if (this.entity.equals(esteUsuario)) {
            addActionError("No se puede eliminar/inactivar a sí mismo.");
            setHabilitarOKenabled(false);
            return Constantes.INPUT;
        }
        return super.destroy();
    }

    @Override
    public String update() {
        this.fillEntity();
        return super.update();
    }

    private void fillEntity() {
        Long id = this.entity.getId();
        if (id != null) {
            String usernameTemp = this.entity.getUsername();
            this.entity = genericService.findById(id);
            this.entity.setUsername(usernameTemp);
        }

        // Si hay una nueva password la codifico y la seteo
        if (claveSinEncriptar.length() > 0) {
            ((IUsuarioService) this.genericService).encodeAndSetPassword(this.getEntity(), claveSinEncriptar);
        }

        Collection<Perfil> profiles = getProfilesFromString(selectedProfileIDs);
        this.getEntity().setPerfiles(profiles);

    }

    private Collection<Perfil> getProfilesFromString(String selectedProfileIDs) {
        Collection<Perfil> profiles = null;
        List<Long> ids = Utils.stringToLongList(selectedProfileIDs);

        if (ids.size() > 0) {
            profiles = new ArrayList<Perfil>();
            for (Long id : ids) {
                profiles.add(this.getPerfilService().findById(id));
            }
        }

        return profiles;
    }

    @SkipValidation
    public String listSoloConsulta() {
        setReadOnly(true);
        return list();
    }

    @Override
    public String save() {
        this.fillEntity();
        return super.save();
    }

    public void prepare() throws Exception {
        super.prepare();
        availableProfiles = getPerfilService().getAllActivos();
        if (getRequestId() != 0) {
            this.entity = genericService.findById(Long.valueOf(getRequestId()));
        } else {
            this.entity = new Usuario();
        }
    }

    @Validations
    public Usuario getModel() {
        return entity;
    }

    public void validate() {

        // Trims de los Strings
        this.getEntity().setUsername(this.getEntity().getUsername().trim());

        if (this.getEntity().getUsername().length() == 0) {
            addFieldError("username", "Complete el campo Email");
        }else{

            final String EMAIL_PATTERN = 
            		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            Pattern p = Pattern.compile(EMAIL_PATTERN);
            Matcher m = p.matcher(this.getEntity().getUsername());
            if (!m.matches()) {
                addFieldError("username",
                        "El Email ingresado es inválido");
            }
        	
        }

        if(StringUtils.isBlank(claveSinEncriptar)){
        	addFieldError("claveSinEncriptar", "Complete el campo contraseña");
        	
        }else{
        	
        	Pattern pass = Pattern.compile("[a-zA-Z0-9!?$%^&*()_+={[}]:;@'~#|\\<,>.?/]*");
        	Matcher passMatcher = pass.matcher(claveSinEncriptar);
        	if (!passMatcher.matches()) {
        		addFieldError("claveSinEncriptar", "Contraseña invalida");
        	}
        	
        }
        

        if (Utils.stringToLongList(getSelectedProfileIDs()).size() == 0) {
            addFieldError("selectedProfileIDs", "Debe seleccionar al menos un perfil.");
        }

        // La password no puede tener caracteres en blanco:
        if (getClaveSinEncriptar().indexOf(' ') != -1) {
            addFieldError("claveSinEncriptar", "La contraseña no puede tener espacios.");
        }

        if (!getClaveSinEncriptar().equals(getClaveSinEncriptarRepetida())) {
            addFieldError("claveSinEncriptar", "Las contraseñas ingresadas son distintas.");
        }

        // No se debe repetir el nombre de usuario:
        if (((IUsuarioService) genericService).isDuplicateUsername(this.getEntity())) {
            addFieldError("username", "El Email ingresado ya existe.");
        }

        // Validamos que no nos estemos "autoinactivando"...
        Usuario esteUsuario = this.getSessionUser();

        if (esteUsuario != null && esteUsuario.equals(this.entity) && !this.entity.isEnabled()) {
            addFieldError("enabled", "No se puede eliminar/inactivar a sí mismo.");
        }

        if (crearJugador) {
            if (nombreJugador.length() == 0) {
                addFieldError("nombreJugador", "Nombre requerido.");
            }
            if (apellidoJugador.length() == 0) {
                addFieldError("apellidoJugador", "Apellido requerido.");
            }

        }

    }

    @Override
    public String remove() {
        // Eliminacion Logica SIEMPRE:
        this.getEntity().setActivo(false);
        // Usuario Ultima Modif:
        // this.entity.setUserLastUpdate(this.getSessionUser());

        genericService.update(this.entity);
        return list();
    }

    @SkipValidation
    public String crearUsuarioConJugador() {
        setActionMethod("GuardarUsuarioConJugador");
        return Constantes.SUCCESS;
    }

    public String guardarUsuarioConJugador() {
        this.entity.addPerfil(perfilService.findByName("PUSER"));
        if (claveSinEncriptar.length() > 0) {
            ((IUsuarioService) this.genericService).encodeAndSetPassword(this.getEntity(), claveSinEncriptar);
        }
        genericService.persist(this.entity);
        if (crearJugador) {
            Jugador jugador = new Jugador();
            jugador.setActivo(true);
            jugador.setNombre(nombreJugador);
            jugador.setApellido(apellidoJugador);
            jugador.setApodo(apodoJugador);
            jugador.setUsuario(this.entity);

            jugadorService.persist(jugador);

        }
        return Constantes.SUCCESS;
    }

    @SkipValidation
    public String miUsuarioListado() {
        setActionMethod("MiUsuario_miUsuarioDetalle");
        List<Usuario> listado = new ArrayList<Usuario>();
        listado.add(getSessionUser());
        setEntities(listado);
        this.setRowCount(listado.size());
        return Constantes.LIST;
    }

    @SkipValidation
    public String miUsuarioDetalle() {
        validar();
        setActionMethod("MiUsuario_miUsuarioListado");
        return Constantes.SUCCESS;
    }

    @SkipValidation
    public String facebookLogin() throws IOException {
        String token = null;
        final String callback = "http://localhost:8080/web/FacebookLogin.action";
        final String tokenUrl = "https://graph.facebook.com/oauth/access_token"
                + String.format("?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s", "182811701772156", callback,
                        "79febcb9466ca713213b8f11ff1cad24", this.code);

        URL graphFacebook = new URL(tokenUrl);
        URLConnection urlCon = graphFacebook.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(urlCon.getInputStream()));
        String inputLine = in.readLine();
        if (inputLine != null) {
            token = inputLine.substring(inputLine.indexOf("=") + 1, inputLine.indexOf("&"));
        }
        in.close();

        FacebookClient facebookClient = new DefaultFacebookClient(token);
        User user = facebookClient.fetchObject("me", User.class);
        this.email = user.getEmail();
        boolean existe = ((IUsuarioService) this.genericService).isDuplicateUsername(email);
        String clave = "";
        if (!existe) {
            crearUsuarioYJugador(user);
            clave = this.claveSinEncriptar;
        } else {
            Usuario usuario = ((IUsuarioService) this.genericService).getByUsername(this.email);
            clave = usuario.getPassword();
        }

        try {
            UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(this.email, clave);

            getSession().put("ACEGI_SECURITY_LAST_USERNAME", this.email);
            
            if (existe) {
                DaoAuthenticationProvider authDao = new DaoAuthenticationProvider();
                authDao.setUserDetailsService(getDetallesUsuarioService());

                SecurityContextHolder.getContext().setAuthentication(authDao.authenticate(authRequest));
            } else
                SecurityContextHolder.getContext().setAuthentication(
                        getAuthenticationManager().authenticate(authRequest));

            getSessionUser();
            return Constantes.SUCCESS;
        } catch (AuthenticationException ex) {
            return "failure";
        }

    }

    private void crearUsuarioYJugador(User user) {
        this.entity = new Usuario();
        this.entity.setUsername(email);
        this.claveSinEncriptar = Utils.getPassword();
        this.nombreJugador = user.getFirstName();
        this.apellidoJugador = user.getLastName();
        this.crearJugador = true;
        guardarUsuarioConJugador();
        mailService.enviarMailUsuarioNuevo(email, this.claveSinEncriptar);
    }

    public boolean isHabilitarOKenabled() {
        return habilitarOKenabled;
    }

    public void setHabilitarOKenabled(boolean habilitarOKenabled) {
        this.habilitarOKenabled = habilitarOKenabled;
    }

    public IPerfilService getProfileService() {
        return getPerfilService();
    }

    public void setProfileService(IPerfilService profileService) {
        this.setPerfilService(profileService);
    }

    public Collection<Perfil> getAvailableProfiles() {
        return availableProfiles;
    }

    public void setAvailableProfiles(Collection<Perfil> availableProfiles) {
        this.availableProfiles = availableProfiles;
    }

    public String getSelectedProfileIDs() {
        return selectedProfileIDs;
    }

    public void setSelectedProfileIDs(String selectedProfileIDs) {
        this.selectedProfileIDs = selectedProfileIDs;
    }

    public String getClaveSinEncriptar() {
        return claveSinEncriptar;
    }

    public void setClaveSinEncriptar(String claveSinEncriptar) {
        this.claveSinEncriptar = claveSinEncriptar;
    }

    public void setPerfilService(IPerfilService perfilService) {
        this.perfilService = perfilService;
    }

    public IPerfilService getPerfilService() {
        return perfilService;
    }

    public String getClaveSinEncriptarRepetida() {
        return claveSinEncriptarRepetida;
    }

    public void setClaveSinEncriptarRepetida(String claveSinEncriptarRepetida) {
        this.claveSinEncriptarRepetida = claveSinEncriptarRepetida;
    }

    public void setJugadorService(IJugadorService jugadorService) {
        this.jugadorService = jugadorService;
    }

    public IJugadorService getJugadorService() {
        return jugadorService;
    }

    @Override
    protected boolean tienePermiso() {
        if (!this.entity.equals(getSessionUser())) {
            return false;
        }
        return true;
    }

    public void setAuthenticationManager(ProviderManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public ProviderManager getAuthenticationManager() {
        return authenticationManager;
    }

    public void setDetallesUsuarioService(DetallesUsuarioService detallesUsuarioService) {
        this.detallesUsuarioService = detallesUsuarioService;
    }

    public DetallesUsuarioService getDetallesUsuarioService() {
        return detallesUsuarioService;
    }
}
