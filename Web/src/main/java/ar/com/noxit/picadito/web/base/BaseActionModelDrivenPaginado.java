package ar.com.noxit.picadito.web.base;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;


import ar.com.noxit.base.utils.ApplicationContext;
import ar.com.noxit.base.utils.Paginado;

import com.opensymphony.xwork2.Preparable;

/**
 * BaseActionModelDriven con Paginado de Displaytag
 * 
 */
public abstract class BaseActionModelDrivenPaginado<T> extends BaseActionModelDriven<T> implements ServletRequestAware,
        Preparable {

    private static final long serialVersionUID = 1L;

    private Integer rowCount = new Integer(0);
    private Integer pageSize = new Integer(10);
    private Integer initialPage = new Integer(1);
    private HttpServletRequest servletRequest;
    private String displaytagGridID;

    @Override
    public void prepare() throws Exception {

        this.preparePaginado();

    }

    protected void preparePaginado() throws Exception {

        // Configuramos la paginación de la grilla (solo si setearon el nombre de la grilla)
        if (displaytagGridID != null) {
            String pageNumber = this.getServletRequest().getParameter(
                    (new ParamEncoder(this.displaytagGridID).encodeParameterName(TableTagParameters.PARAMETER_PAGE)));
            Paginado paginado = new Paginado();
            paginado.setPage((pageNumber == null || pageNumber.equals("")) ? initialPage.intValue() : new Integer(
                    pageNumber).intValue());
            paginado.setSize(pageSize.intValue());
            ApplicationContext.getPaginado().set(paginado);
        }

    }

    /**
     * Por default se listaran todas las entidades.
     */
    @Override
    public Collection<T> getEntitiesToList() {
        this.setRowCount(this.genericService.getCountAll());
        return this.genericService.getAllPaginado();
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setDisplaytagGridID(String displaytagGridID) {
        this.displaytagGridID = displaytagGridID;
    }

    public String getDisplaytagGridID() {
        return displaytagGridID;
    }

    public HttpServletRequest getServletRequest() {
        return servletRequest;
    }

    public void setServletRequest(HttpServletRequest servletRequest) {
        this.servletRequest = servletRequest;
    }

    public void setInitialPage(Integer initialPage) {
        this.initialPage = initialPage;
    }

    public Integer getInitialPage() {
        return initialPage;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

}
