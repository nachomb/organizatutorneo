package ar.com.noxit.picadito.web.acciones;

import java.io.File;
import java.io.IOException;

import ar.com.noxit.picadito.dominio.entidades.Dummy;
import ar.com.noxit.picadito.dominio.entidades.ResultadoImportacion;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.servicios.IImporterService;

import com.opensymphony.xwork2.Preparable;

public class ImporterAction extends
		BaseActionModelDrivenPaginado<Dummy> implements Preparable {

	private static final long serialVersionUID = -4603448404995537192L;
	private File excel;
	private IImporterService importerService;
	private boolean habilitarOKenabled = true;
	private ResultadoImportacion resultadoImportacion;
	
	public ResultadoImportacion getResultadoImportacion() {
		return resultadoImportacion;
	}

	public void setResultadoImportacion(ResultadoImportacion resultadoImportacion) {
		this.resultadoImportacion = resultadoImportacion;
	}

	public boolean isHabilitarOKenabled() {
		return habilitarOKenabled;
	}

	public void setHabilitarOKenabled(boolean habilitarOKenabled) {
		this.habilitarOKenabled = habilitarOKenabled;
	}
	
	public File getExcel() {
		return excel;
	}

	public void setExcel(File excel) {
		this.excel = excel;
	}
	
	public IImporterService getImporterService() {
		return importerService;
	}

	public void setImporterService(IImporterService importerService) {
		this.importerService = importerService;
	}

	@Override
	public Dummy getModel() {
		return null;
	}

	@Override
	protected boolean tienePermiso() {
		return true;
	}
	
	public String jugadores(){
		setMappedRequest("importar");
		return Constantes.SUCCESS;
		
	}
	
	public String importar() throws IOException{
		this.resultadoImportacion = importerService.importarJugadoresYEquipos(excel);
		return "resultados";
	}

}
