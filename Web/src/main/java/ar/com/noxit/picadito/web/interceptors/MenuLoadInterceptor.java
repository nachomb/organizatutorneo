package ar.com.noxit.picadito.web.interceptors;

import java.io.Serializable;
import java.util.Map;

import org.acegisecurity.context.SecurityContextHolder;

import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.utils.MenuSession;
import ar.com.noxit.servicios.IItemMenuService;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class MenuLoadInterceptor extends AbstractInterceptor implements Serializable {

    private static final long serialVersionUID = 8801454386874508746L;

    private IItemMenuService itemMenuService;

    public void setItemMenuService(IItemMenuService itemMenuService) {
        this.itemMenuService = itemMenuService;
    }

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        //Usuario usuario = (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        //if (!usuario.isAnonimo()) {
            Map session = ActionContext.getContext().getSession();
            if (session.get("itemsMenu") == null) {
                MenuSession.getInstance().refreshMenuSession(session, itemMenuService);
            }
        //}

        return invocation.invoke();
    }
}
