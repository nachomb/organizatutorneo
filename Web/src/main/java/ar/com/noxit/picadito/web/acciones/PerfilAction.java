package ar.com.noxit.picadito.web.acciones;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.picadito.dominio.entidades.seguridad.Accion;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.seguridad.helpers.ContextHelper;
import ar.com.noxit.seguridad.services.IAccionService;
import ar.com.noxit.seguridad.services.IPerfilService;

import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.Validations;

@SuppressWarnings("serial")
public class PerfilAction extends BaseActionModelDrivenPaginado<Perfil> implements Preparable {

    private IAccionService accionService;

    private Collection<Accion> availableActions;

    private String selectedActionIDs;

    private boolean habilitarOKenabled = true;

    private String formTitulo = "";

    private String mensajeAdvertencia = "";

    private ContextHelper contextHelper;

    public ContextHelper getContextHelper() {
        return contextHelper;
    }

    public void setContextHelper(ContextHelper contextHelper) {
        this.contextHelper = contextHelper;
    }

    public PerfilAction(IPerfilService perfilService) {
        this.genericService = perfilService;
        this.setDisplaytagGridID("profileGrid"); // Nombre de la grilla de DisplayTag asociada al Paginado
    }

    @Override
    public String add() {
        formTitulo = "Nuevo Perfil";
        return super.add();
    }

    @Override
    public String edit() {
        formTitulo = "Editar Perfil";
        return super.edit();
    }

    @Override
    public String update() {
        this.fillEntity();
        this.contextHelper.reloadContext();
        return super.update();
    }

    private void fillEntity() {
        // Usuario Ultima Modif:
        // this.getEntity().setUserLastUpdate(this.getSessionUser());
        Collection<Accion> actions = getActionsFromString(selectedActionIDs);
        this.getEntity().setAcciones(actions);
    }

    private Collection<Accion> getActionsFromString(String selectedActionIDs) {
        Collection<Accion> actions = null;
        List<Long> ids = getIdList(selectedActionIDs);

        if (ids.size() > 0) {
            actions = new ArrayList<Accion>();
            for (Long id : ids) {
                actions.add(this.accionService.findById(id));
            }
        }

        return actions;
    }

    private List<Long> getIdList(String idsStr) {
        List<Long> listIds = new ArrayList<Long>();
        if (idsStr != null && idsStr.length() > 0) {
            for (String id : idsStr.split(",")) {
                listIds.add(new Long(id.trim()));
            }
        }
        return listIds;
    }

    @Override
    public String save() {
        this.fillEntity();
        genericService.persist(this.entity);
        this.contextHelper.reloadContext();
        return list();
    }

    @Override
    public String destroy() {

        formTitulo = "Eliminar Perfil";
        mensajeAdvertencia = "Atención: El Perfil se inactivará y las relaciones a usuarios se eliminarán.";

        return super.destroy();

    }

    @Override
    public String remove() {
        // Eliminacion Logica SIEMPRE:
        this.getEntity().setActivo(false);

        // Usuario Ultima Modif:
        // this.entity.setUserLastUpdate(this.getSessionUser());

        genericService.update(this.entity);
        return list();
    }

    public void prepare() throws Exception {
        super.prepare();
        availableActions = accionService.getAll();
        mensajeAdvertencia = "";
        if (getRequestId() != 0) {
            this.entity = genericService.findById(Long.valueOf(getRequestId()));
        } else {
            this.entity = new Perfil();
        }
    }

    @Validations
    public Perfil getModel() {
        return entity;
    }

    public void validate() {

        // Trims de los Strings
        this.getEntity().setNombre(this.getEntity().getNombre().trim());
        this.getEntity().setDescripcion(this.getEntity().getDescripcion().trim());

        if (this.getEntity().getNombre().isEmpty()) {
            addFieldError("nombre", "Nombre requerido.");
        }

        if (this.getEntity().getDescripcion().isEmpty()) {
            addFieldError("descripcion", "Descripción requerido.");
        }

        Pattern p = Pattern.compile("[a-zA-Z0-9]*");
        Matcher m = p.matcher(this.getEntity().getNombre());
        if (!m.matches()) {
            addFieldError("nombre", "El nombre puede contener caracteres de la A a la Z y números");
        }

        if (getIdList(getSelectedActionIDs()).size() == 0) {
            addFieldError("selectedActionIDs", "Debe seleccionar al menos una acción.");
        }

        // No se debe repetir el nombre del profile:
        if (((IPerfilService) genericService).isDuplicateName(this.getEntity())) {
            addFieldError("nombre", "El nombre ingresado ya existe.");
        }

    }

    public IAccionService getAccionService() {
        return accionService;
    }

    public void setAccionService(IAccionService accionService) {
        this.accionService = accionService;
    }

    public Collection<Accion> getAvailableActions() {
        return availableActions;
    }

    public void setAvailableActions(Collection<Accion> availableActions) {
        this.availableActions = availableActions;
    }

    public String getSelectedActionIDs() {
        return selectedActionIDs;
    }

    public void setSelectedActionIDs(String selectedActionIDs) {
        this.selectedActionIDs = selectedActionIDs;
    }

    public boolean isHabilitarOKenabled() {
        return habilitarOKenabled;
    }

    public void setHabilitarOKenabled(boolean habilitarOKenabled) {
        this.habilitarOKenabled = habilitarOKenabled;
    }

    public void setFormTitulo(String formTitulo) {
        this.formTitulo = formTitulo;
    }

    public String getFormTitulo() {
        return formTitulo;
    }

    @SkipValidation
    public String listSoloConsulta() {
        setReadOnly(true);
        return list();
    }

    public void setMensajeAdvertencia(String mensajeAdvertencia) {
        this.mensajeAdvertencia = mensajeAdvertencia;
    }

    public String getMensajeAdvertencia() {
        return mensajeAdvertencia;
    }

	@Override
	protected boolean tienePermiso() {
		return false;
	}

}
