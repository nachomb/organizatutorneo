package ar.com.noxit.picadito.web.constantes;

/**
 * Constantes para la implementacion de CRUDs con Struts
 * 
 */
public interface Constantes {

    public static String INPUT = "input";
    public static String LIST = "list";
    public static String LIST_CONSULTA = "listSoloConsulta";
    public static String SAVE = "save";
    public static String UPDATE = "update";
    public static String REMOVE = "remove";
    public static String SUCCESS = "success";
    public static String ERROR = "error";
    public static String SHOW = "show";

}
