/**
 * 
 */
package ar.com.noxit.picadito.web.typeconverters;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.seguridad.dao.IUsuarioDAO;

public class UsuarioTypeConverter extends StrutsTypeConverter {

    private IUsuarioDAO usuarioDAO;

    @Override
    public Object convertFromString(Map map, String[] strings, Class toClass) {
        if (strings.length != 1) {
            super.performFallbackConversion(map, strings, toClass);
        }

        Usuario usuario = null;
        String username = strings[0];

        if ((username != null) && (username.length() > 0)) {
            usuario = this.getUsuarioDAO().getUserByUsername(username);
        }
        return usuario;
    }

    @Override
    public String convertToString(Map map, Object user) {

        if ((user != null))
            return user.toString();

        return null;
    }

    public IUsuarioDAO getUsuarioDAO() {
        return this.usuarioDAO;
    }

    public void setUsuarioDAO(IUsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

}
