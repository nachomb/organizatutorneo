package ar.com.noxit.picadito.web.acciones;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.picadito.dominio.entidades.Resumen;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.servicios.IResumenService;

import com.opensymphony.xwork2.Preparable;

@SuppressWarnings("serial")
public class ResumenAction extends BaseActionModelDrivenPaginado<Resumen>
		implements Preparable {

	private boolean habilitarOKenabled = true;

	private File uploadTapa;
	private Collection<Resumen> resumenesActivos = null;
    private String fechaYHora;
	
	public String getFechaYHora() {
		return fechaYHora;
	}

	public void setFechaYHora(String fechaYHora) {
		this.fechaYHora = fechaYHora;
	}

	public Collection<Resumen> getResumenesActivos() {
		return resumenesActivos;
	}

	public void setResumenesActivos(Collection<Resumen> resumenesActivos) {
		this.resumenesActivos = resumenesActivos;
	}

	public boolean isHabilitarOKenabled() {
		return habilitarOKenabled;
	}

	public void setHabilitarOKenabled(boolean habilitarOKenabled) {
		this.habilitarOKenabled = habilitarOKenabled;
	}

	public File getUploadTapa() {
		return uploadTapa;
	}

	public void setUploadTapa(File uploadTapa) {
		this.uploadTapa = uploadTapa;
	}

	public ResumenAction(IResumenService resumenService){
		this.genericService = resumenService;
        this.setDisplaytagGridID("resumenGrid");
	}
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		if (getRequestId() != 0) {
			this.entity = genericService.findById(Long.valueOf(getRequestId()));
            if (this.entity.getFecha() != null) {
                setFechaYHora(Utils.dateFormat.format(this.entity.getFecha()));
            }
		} else {
			this.entity = new Resumen();
		}

	}

	@Override
	public Resumen getModel() {
		return this.getEntity();
	}


	@Override
	protected boolean tienePermiso() {
		return false;
	}
	
	@Override
	public String save() {
		guardarImagenes();
		cargarFecha();
		return super.save();
	}
	
	@Override
	public String update() {
		guardarImagenes();
		cargarFecha();
		return super.update();
	}

	private void guardarImagenes() {
		try {
			if(uploadTapa != null)
				this.entity.setTapa(Utils.guadarImagen(uploadTapa));
		} catch (IOException e) {
			addActionError(e.getMessage());
		}
	}

	public String resumenesActivos(){
		resumenesActivos = ((IResumenService)this.genericService).getAllActivosOrderByFechaDesc();
		return Constantes.SUCCESS;
	}
	
    private void cargarFecha() {
        try {
            if (fechaYHora != null && fechaYHora.trim().length() != 0) {
                Date fecha = Utils.dateFormat.parse(fechaYHora);
                this.getEntity().setFecha(fecha);
            }
        } catch (ParseException ex) {
            // No hace nada
        }
    }
	
}
