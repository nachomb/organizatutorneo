package ar.com.noxit.picadito.web.acciones;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.picadito.dominio.entidades.Alineacion;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Fecha;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.PuntajePartido;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.servicios.IAlineacionService;
import ar.com.noxit.servicios.IEquipoService;
import ar.com.noxit.servicios.IFechaService;
import ar.com.noxit.servicios.IGolService;
import ar.com.noxit.servicios.IPartidoService;
import ar.com.noxit.servicios.IPuntajePartidoService;
import ar.com.noxit.servicios.ISancionService;
import ar.com.noxit.servicios.ITarjetaService;
import ar.com.noxit.servicios.impl.ITorneoService;

import com.google.common.collect.Lists;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.Validations;

@SuppressWarnings("serial")
public class FechaAction extends BaseActionModelDrivenPaginado<Fecha> implements Preparable, ModelDriven<Fecha> {

    private Long torneoId = 0L;
    private ITorneoService torneoService;
    private ISancionService sancionService;
    private List<Fecha> fechas;
    private List<Torneo> torneosActivos;
	private IPuntajePartidoService puntajePartidoService;
    private IGolService golService;
    private ITarjetaService tarjetaService;
    private IEquipoService equipoService;
    private Long torneoIdNewFecha;
	private Collection<Equipo> equiposPosibles;
	private String partidosNuevos;
	private IPartidoService partidoService;
	private IAlineacionService alineacionService;
	private static Logger logger = Logger.getLogger(FechaAction.class);
    
    public String getPartidosNuevos() {
		return partidosNuevos;
	}

	public void setPartidosNuevos(String partidosNuevos) {
		this.partidosNuevos = partidosNuevos;
	}

	public Collection<Equipo> getEquiposPosibles() {
		return equiposPosibles;
	}

	public void setEquiposPosibles(Collection<Equipo> equiposPosibles) {
		this.equiposPosibles = equiposPosibles;
	}

	public Long getTorneoIdNewFecha() {
		return torneoIdNewFecha;
	}

	public void setTorneoIdNewFecha(Long torneoIdNewFecha) {
		this.torneoIdNewFecha = torneoIdNewFecha;
	}

	public ITarjetaService getTarjetaService() {
		return tarjetaService;
	}

	public void setTarjetaService(ITarjetaService tarjetaService) {
		this.tarjetaService = tarjetaService;
	}

	public IGolService getGolService() {
		return golService;
	}

	public void setGolService(IGolService golService) {
		this.golService = golService;
	}

	public IPuntajePartidoService getPuntajePartidoService() {
		return puntajePartidoService;
	}

	public void setPuntajePartidoService(
			IPuntajePartidoService puntajePartidoService) {
		this.puntajePartidoService = puntajePartidoService;
	}

	public List<Torneo> getTorneosActivos() {
		return torneosActivos;
	}

	public void setTorneosActivos(List<Torneo> torneosActivos) {
		this.torneosActivos = torneosActivos;
	}

	public List<Fecha> getFechas() {
		return fechas;
	}

	public void setFechas(List<Fecha> fechas) {
		this.fechas = fechas;
	}

	public FechaAction(IFechaService fechaService, ISancionService sancionService, 	IEquipoService equipoService, IPartidoService partidoService, IAlineacionService alineacionService) {
        this.genericService = fechaService;
        this.equipoService = equipoService;
        this.setSancionService(sancionService);
        this.partidoService = partidoService;
        this.alineacionService = alineacionService;
        this.setDisplaytagGridID("fechaGrid");
    }

    @Override
    public Collection<Fecha> getEntitiesToList() {
		Usuario usuario = getSessionUser();
		if(!usuario.isAnonimo()){
			if("PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
				this.setRowCount(((IFechaService) this.genericService).getCountByTorneoId(getTorneoId()));
				Torneo torneo = this.torneoService.findById(getTorneoId());
				if(torneo == null){
					torneo = this.entity.getTorneo();
				}
				if(usuario.equals(torneo.getCreador())){
					return ((IFechaService) this.genericService).getAllFechasByTorneoId(getTorneoId());
				}
			}
		}
//		else{
//			this.setRowCount(((IFechaService) this.genericService).getCountByTorneoId(getTorneoId()));
//			return ((IFechaService) this.genericService).getAllFechasByTorneoId(getTorneoId());
//			
//		}
		return Lists.newArrayList();
    }

    @Override
    public void prepare() throws Exception {
        if (getRequestId() != 0) {
            setEntity(getFechaService().findById(getRequestId()));
        } else if (getTorneoId() != 0) {
            setEntity(getFechaService().getLastFechaNoJugadaByTorneoId(getTorneoId()));
        }else{
        	this.entity = new Fecha();
        	Torneo torneo = this.torneoService.findById(torneoIdNewFecha);
        	this.entity.setTorneo(torneo);
    		setEquiposPosibles(torneo.getEquipos());
        }

    }

    @Override
    public String input() throws Exception {
        setMappedRequest("cargar");
        return super.input();
    }

//    private void verificarSanciones() {
//        for (Partido partido : this.entity.getPartidos()) {
//            List<Sancion> sanciones = getSancionService().getSancionesNoCumplidas(partido.getEquipoLocal(),
//                    partido.getEquipoVisitante());
//            for (Sancion sancion : sanciones) {
//                if (!sancion.getPartidoInicio().equals(partido)) {
//                    sancion.agregarPartidoSancionCumplida(partido);
//                    getSancionService().update(sancion);
//                }
//            }
//        }
//    }
    
	@Override
	public String add() {
		Usuario usuario = getSessionUser();
		if(!usuario.isAnonimo() && "PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
			Torneo torneo = this.torneoService.findById(torneoIdNewFecha);
			if(usuario.equals(torneo.getCreador())){
				super.add();
				return "fechaNueva";
			}
		}
		return ERROR;
	}

	@Validations
    public String cargar() throws Exception {
        getFechaService().update(getEntity());
        //verificarSanciones();
        getTorneoService().continuarTorneo(getEntity().getTorneo());
        getTorneoService().actualizarPosiciones(getTorneoId());
        return "cargado";
    }

    @SkipValidation
    public String detalle() {
        setActionMethod("ConsultaTorneo");
        setReadOnly(true);
        return Constantes.SUCCESS;
    }
    
	@Override
	public String save() {
		this.fillEntity();
		super.save();
		return "fechaNuevaCreada";
	}
	
    @Override
	public String update() {
    	fillEntityToUpdate();
    	logger.info("Actualizando Fecha" + this.entity.getNombre() + " del torneo " + this.entity.getTorneo().getNombre());
		super.update();
		logger.info("Fecha actualizada");
		if(getEntity().isJugada()){
			getTorneoService().continuarTorneo(getEntity().getTorneo());
		}
        getTorneoService().actualizarPosiciones(this.entity.getTorneo().getId());
		return "cargaRapidaOk";
	}

	private void fillEntityToUpdate() {
    	List<Long> idEquipos = Utils.stringToLongList(partidosNuevos);
    	Iterator<Partido> itPartidosAnteriores = this.entity.getPartidos().iterator();
    	Partido partidoAnterior = null;
    	for(int i = 0 ; i < idEquipos.size() - 1 ; i+=2){
    		if(itPartidosAnteriores.hasNext()){
    			partidoAnterior = itPartidosAnteriores.next();
    		}
    		Partido partido = partidoService.findById(partidoAnterior.getId());
    		Equipo equipoLocal = equipoService.findById(idEquipos.get(i));
    		Equipo equipoVisitante = equipoService.findById(idEquipos.get(i + 1));
    		boolean huboCambio = false;
    		if(cambioEquipo(equipoLocal, partidoAnterior.getEquipoLocal())){
    			Equipo equipo = equipoService.findById(partido.getEquipoLocal().getId());
    			//borro las tarjetas
    			borrarTarjetas(partido, equipo);
    			//borro los goles
    			borrarGoles(partido, equipo);
    			//borro los puntajes
    			borrarPuntajes(partido, equipo);
    			//cambio el equipo
    			partido.setEquipoLocal(equipoLocal);
    			//borro la alineacion visitante
    			Alineacion alineacionLocal = partidoAnterior.getAlineacionLocal();
    			partido.setAlineacionLocal(null);
				borrarAlineacion(alineacionLocal);
				//reseteo la cantidad de goles
				partido.setGolesLocal(0);
    			huboCambio = true;
    		}
    		if(cambioEquipo(equipoVisitante, partidoAnterior.getEquipoVisitante())){
    			Equipo equipo = equipoService.findById(partido.getEquipoVisitante().getId());
    			//borro las tarjetas
    			borrarTarjetas(partido, equipo);
    			//borro los goles
    			borrarGoles(partido, equipo);
    			//borro los puntajes
    			borrarPuntajes(partido, equipo);
    			//cambio el equipo
    			partido.setEquipoVisitante(equipoVisitante);
    			//borro la alineacion visitante
    			Alineacion alineacionVisitante = partidoAnterior.getAlineacionVisitante();
    			partido.setAlineacionVisitante(null);
				borrarAlineacion(alineacionVisitante);
				//reseteo la cantidad de goles
				partido.setGolesVisitante(0);
    			huboCambio = true;
    		}
    		if(huboCambio){
    			logger.info("Actualizando partido:" + partido + " porque cambiaron los equipos.");
    			this.partidoService.saveOrUpdate(partido);
    			/*tarjetaService.borrarAmarillas(partidoAnterior);
    			tarjetaService.borrarRojas(partidoAnterior);
    			golService.eliminarGoles(partidoAnterior);
    			puntajePartidoService.borrarPuntajes(partidoAnterior);*/
    		}
    			
    	}
    	if(idEquipos.size() % 2 != 0){
    		Equipo equipoLibre = equipoService.findById(idEquipos.get(idEquipos.size() - 1 ));
			this.entity.setEquipoLibre(equipoLibre);
    	}
		
	}

	private void borrarPuntajes(Partido partido, Equipo equipo) {
		if(equipo.getJugadores() != null && !equipo.getJugadores().isEmpty()){
			puntajePartidoService.borrarPuntajes(partido, equipo);
		}
	}

	private void borrarGoles(Partido partido, Equipo equipo) {
		if(equipo.getJugadores() != null && !equipo.getJugadores().isEmpty()){
			golService.eliminarGoles(partido, equipo);
			partido.setGoles(null);
		}
	}

	private void borrarTarjetas(Partido partido, Equipo equipo) {
		if(equipo.getJugadores() != null && !equipo.getJugadores().isEmpty()){
			tarjetaService.borrarTarjetas(partido, equipo);
			partido.setTarjetas(null);
		}
	}

	private void borrarAlineacion(Alineacion alineacion) {
		if(alineacion != null){
			logger.info("Buscando alineacion");
			alineacion = alineacionService.findById(alineacion.getId());
			logger.info("Borrando alineacion con id " + alineacion.getId());
			alineacionService.remove(alineacion);
		}
	}

	private boolean cambioEquipo(Equipo equipoNuevo, Equipo equipoAnterior){
		return !equipoNuevo.equals(equipoAnterior);
	}
	
	private void fillEntity() {
    	this.entity.setTorneo(getTorneoService().findById(getTorneoIdNewFecha()));
    	List<Long> idEquipos = Utils.stringToLongList(partidosNuevos);
    	List<Partido> partidos = new ArrayList<Partido>();
    	for(int i = 0 ; i < idEquipos.size() - 1 ; i+=2){
    		Equipo equipoLocal = equipoService.findById(idEquipos.get(i));
    		Equipo equipoVisitante = equipoService.findById(idEquipos.get(i + 1));
    		Partido partido = new Partido();
    		partido.setEquipoLocal(equipoLocal);
    		partido.setEquipoVisitante(equipoVisitante);
    		partido.setActivo(true);
    		partido.setFecha(this.entity);
			partidos.add(partido);
    	}
    	this.entity.setPartidos(partidos);
    	if(idEquipos.size() % 2 != 0){
    		Equipo equipoLibre = equipoService.findById(idEquipos.get(idEquipos.size() - 1 ));
			this.entity.setEquipoLibre(equipoLibre);
    	}
	}

	@Override
    public void validate() {
//		if(this.entity.getNumero() == null){
//			addFieldError("numero", "Ingrese un numero valido");
//		}
//		if(this.entity.getNombre() == null){
//			addFieldError("nombre", "Ingrese un nombre");
//		}
	}

    @Override
    public Fecha getModel() {
        return this.entity;
    }

    public void setFechaService(IFechaService fechaService) {
        this.genericService = fechaService;
    }

    public IFechaService getFechaService() {
        return (IFechaService) this.genericService;
    }

    public void setTorneoId(Long torneoId) {
        this.torneoId = torneoId;
    }

    public Long getTorneoId() {
        return torneoId;
    }

    public void setTorneoService(ITorneoService torneoService) {
        this.torneoService = torneoService;
    }

    public ITorneoService getTorneoService() {
        return torneoService;
    }

    @Override
    protected boolean tienePermiso() {
        return false;
    }

    public void setSancionService(ISancionService sancionService) {
        this.sancionService = sancionService;
    }

    public ISancionService getSancionService() {
        return sancionService;
    }
    
    public String resultados(){
    	for(Partido p:this.entity.getPartidos()){
    		cargarAlineacionesForm(p);
    	}
    	return "resultados";
    }
    
	@Override
	@SkipValidation
	public String edit() {
		Usuario usuario = getSessionUser();
		if(!usuario.isAnonimo() && "PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
			Fecha fecha = genericService.findById(Long.valueOf(getRequestId()));
			if(usuario.equals(fecha.getTorneo().getCreador())){
				return super.edit();
			}
		}
		return ERROR;
	}
    
    private void cargarAlineacionesForm(Partido partido) {
    	
    	if(partido.getAlineacionLocal() != null){
	        for (Jugador j : partido.getAlineacionLocal().getTitulares()) {
	            PuntajePartido punt = puntajePartidoService.findByJugadorPartido(j, partido);
	            if (punt != null){
	            	j.setPuntajePartido(punt.getPuntaje());
	            	j.setEsFigura(punt.getFigura());
	            	j.setNumero(punt.getNumero());
	            	//TODO:BORRAR
	                j.setCantGolesPartido(golService.countGoles(j,partido).intValue());
	                j.setCantAmarillasPartido(tarjetaService.countTarjetasAmarillas(j,partido).intValue());
	                j.setCantRojasPartido(tarjetaService.countTarjetasRojas(j,partido).intValue());
	                //fin borrar
	            }
	        }
    	
	        for (Jugador j : partido.getAlineacionVisitante().getTitulares()) {
	            PuntajePartido punt = puntajePartidoService.findByJugadorPartido(j, partido);
	            if (punt != null){
	                j.setPuntajePartido(punt.getPuntaje());
	            	j.setEsFigura(punt.getFigura());
	            	j.setNumero(punt.getNumero());
	            	
	            	//TODO:BORRAR
	                j.setCantGolesPartido(golService.countGoles(j,partido).intValue());
	                j.setCantAmarillasPartido(tarjetaService.countTarjetasAmarillas(j,partido).intValue());
	                j.setCantRojasPartido(tarjetaService.countTarjetasRojas(j,partido).intValue());
	                //fin borrar
	                
	            }
	        }
    	}
    }

}
