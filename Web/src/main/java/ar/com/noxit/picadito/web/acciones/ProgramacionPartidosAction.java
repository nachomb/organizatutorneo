package ar.com.noxit.picadito.web.acciones;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.picadito.dominio.entidades.Dummy;
import ar.com.noxit.picadito.dominio.entidades.Fecha;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.servicios.IFechaService;
import ar.com.noxit.servicios.IPartidoService;
import ar.com.noxit.servicios.IProgramacionPartidosService;
import ar.com.noxit.servicios.impl.ITorneoService;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.FabricaRestricciones;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.PartidoDTO;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.Restriccion;

import com.opensymphony.xwork2.Preparable;

public class ProgramacionPartidosAction extends
		BaseActionModelDrivenPaginado<Dummy> implements Preparable {

	private static final long serialVersionUID = 7730191806491790180L;
	private Collection<Torneo> torneosPosibles;
	private Collection<Fecha> fechas = new ArrayList<Fecha>();
	private Collection<Partido> partidos = new ArrayList<Partido>();
	private static Collection<Partido> previsualizacionPartidos = new ArrayList<Partido>();
	private ITorneoService torneoService;
	private IFechaService fechaService;
	private IPartidoService partidoService;
	private IProgramacionPartidosService programacionPartidosService;
	private String torneosSeleccionados;
	private String fechasSeleccionadas;
	private String partidosSeleccionados;
	private String fechaYHora;
	private String cantCanchas = "2";
	private String tiempoPorPartido = "50";
	private List<String> restricciones = new ArrayList<String>();
	private List<String> operadores = new ArrayList<String>();
	private String restriccionesSeleccionadas;
	private FabricaRestricciones fabricaRestricciones = new FabricaRestricciones();
	private Boolean cumpleRestricciones;

	public Boolean getCumpleRestricciones() {
		return cumpleRestricciones;
	}

	public void setCumpleRestricciones(Boolean cumpleRestricciones) {
		this.cumpleRestricciones = cumpleRestricciones;
	}

	public String getRestriccionesSeleccionadas() {
		return restriccionesSeleccionadas;
	}

	public void setRestriccionesSeleccionadas(String restriccionesSeleccionadas) {
		this.restriccionesSeleccionadas = restriccionesSeleccionadas;
	}

	public List<String> getOperadores() {
		return operadores;
	}

	public void setOperadores(List<String> operadores) {
		this.operadores = operadores;
	}

	public List<String> getRestricciones() {
		return restricciones;
	}

	public void setRestricciones(List<String> restricciones) {
		this.restricciones = restricciones;
	}

	public Collection<Partido> getPrevisualizacionPartidos() {
		return previsualizacionPartidos;
	}

	public void setPrevisualizacionPartidos(
			Collection<Partido> previsualizacionPartidos) {
		ProgramacionPartidosAction.previsualizacionPartidos = previsualizacionPartidos;
	}

	public String getFechaYHora() {
		return fechaYHora;
	}

	public void setFechaYHora(String fechaYHora) {
		this.fechaYHora = fechaYHora;
	}

	public String getCantCanchas() {
		return cantCanchas;
	}

	public void setCantCanchas(String cantCanchas) {
		this.cantCanchas = cantCanchas;
	}

	public String getTiempoPorPartido() {
		return tiempoPorPartido;
	}

	public void setTiempoPorPartido(String tiempoPorPartido) {
		this.tiempoPorPartido = tiempoPorPartido;
	}

	public String getPartidosSeleccionados() {
		return partidosSeleccionados;
	}

	public void setPartidosSeleccionados(String partidosSeleccionados) {
		this.partidosSeleccionados = partidosSeleccionados;
	}

	public IFechaService getFechaService() {
		return fechaService;
	}

	public void setFechaService(IFechaService fechaService) {
		this.fechaService = fechaService;
	}

	public Collection<Partido> getPartidos() {
		return partidos;
	}

	public void setPartidos(Collection<Partido> partidos) {
		this.partidos = partidos;
	}

	public String getFechasSeleccionadas() {
		return fechasSeleccionadas;
	}

	public void setFechasSeleccionadas(String fechasSeleccionadas) {
		this.fechasSeleccionadas = fechasSeleccionadas;
	}

	public Collection<Fecha> getFechas() {
		return fechas;
	}

	public void setFechas(Collection<Fecha> fechas) {
		this.fechas = fechas;
	}

	public ITorneoService getTorneoService() {
		return torneoService;
	}

	public void setTorneoService(ITorneoService torneoService) {
		this.torneoService = torneoService;
	}

	public String getTorneosSeleccionados() {
		return torneosSeleccionados;
	}

	public void setTorneosSeleccionados(String torneosSeleccionados) {
		this.torneosSeleccionados = torneosSeleccionados;
	}

	public Collection<Torneo> getTorneosPosibles() {
		return torneosPosibles;
	}

	public void setTorneosPosibles(Collection<Torneo> torneosPosibles) {
		this.torneosPosibles = torneosPosibles;
	}

	public ProgramacionPartidosAction(ITorneoService torneoService,
			IFechaService fechaService, IPartidoService partidoService, IProgramacionPartidosService programacionPartidosService) {
		this.torneoService = torneoService;
		this.fechaService = fechaService;
		this.partidoService = partidoService;
		this.programacionPartidosService = programacionPartidosService;
	}

	@Override
	public Dummy getModel() {
		return null;
	}

	@Override
	protected boolean tienePermiso() {
		return true;
	}

	@Override
	public void prepare() throws Exception {
		this.torneosPosibles = this.torneoService.getTorneosByUsuarioCreador(getSessionUser());
		this.restricciones .add("Horario");
		this.restricciones.add("Cancha");
		this.operadores.add("=");
		this.operadores.add(">=");
		this.operadores.add("<=");
	}

	public String programar() {
		setMappedRequest("publicar");
		return Constantes.SUCCESS;
	}

	public String publicar() throws ParseException {
		for(Partido p: previsualizacionPartidos){
			partidoService.saveOrUpdate(p);
		}
		return Constantes.SUCCESS;
	}

	@SkipValidation
	public String mostrarPartidos() {
		if (!"".equals(fechasSeleccionadas)) {
			List<Long> idList = Utils.stringToLongList(fechasSeleccionadas);
			for (Long id : idList) {
				this.partidos.addAll(fechaService.findById(id).getPartidos());
			}
		}
		return Constantes.SUCCESS;
	}

	@SkipValidation
	public String mostrarFechas() {
		if (!"".equals(torneosSeleccionados)) {
			List<Long> idList = Utils.stringToLongList(torneosSeleccionados);
			for (Long id : idList) {
				this.fechas.addAll(torneoService.findById(id).getFechas());
			}
		}
		return Constantes.SUCCESS;
	}

	@SkipValidation
	public String previsualizarHorarios() throws ParseException {
		previsualizacionPartidos.clear();
		if("".equals(partidosSeleccionados) || partidosSeleccionados == null)
			return Constantes.SUCCESS;
		int cantidadCanchas = Utils.stringToInt(cantCanchas);
		int minutosPorPartido = Utils.stringToInt(tiempoPorPartido);
		Date fechaInicial = null;
		if(!"".equals(fechaYHora))
			fechaInicial = Utils.stringToDate(fechaYHora);
		List<Long> idPartidos = Utils.stringToLongList(partidosSeleccionados);
		Iterator<Long> itPartido = idPartidos.iterator();
		List<PartidoDTO> partidosDTO = new ArrayList<PartidoDTO>();
		while (itPartido.hasNext()) {
			PartidoDTO partido = new PartidoDTO(itPartido.next());
			partidosDTO.add(partido);
		}
		
		List<Restriccion> restricionesParseadas = new ArrayList<Restriccion>();
		if(!restriccionesSeleccionadas.isEmpty()){
			for(String res : restriccionesSeleccionadas.split(";")){
				String[] resSplitted = res.split(",");
				Restriccion restriccion = fabricaRestricciones.crear(resSplitted[0],resSplitted[1],resSplitted[2],resSplitted[3]);
				restricionesParseadas.add(restriccion);
			}
		}
		List<PartidoDTO> posibleProgramacion = programacionPartidosService.obtenerPosibleProgramacion
		(partidosDTO, restricionesParseadas, cantidadCanchas, minutosPorPartido, fechaInicial);
		
		for(PartidoDTO partidoDTO: posibleProgramacion){
			Partido partido = partidoService.findById(partidoDTO.getId());
			partido.setDescripcion(partidoDTO.getDescripcion());
			partido.setFechaPartido(partidoDTO.getFechaPartido());
			previsualizacionPartidos.add(partido);
		}
		
		this.cumpleRestricciones = this.programacionPartidosService.cumpleTodasRestricciones(posibleProgramacion, restricionesParseadas);
		
		return Constantes.SUCCESS;
	}

}
