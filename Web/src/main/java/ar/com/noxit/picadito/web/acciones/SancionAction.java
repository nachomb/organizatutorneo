package ar.com.noxit.picadito.web.acciones;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.interceptor.validation.SkipValidation;

import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Fecha;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Sancion;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.picadito.web.base.BaseActionModelDrivenPaginado;
import ar.com.noxit.picadito.web.constantes.Constantes;
import ar.com.noxit.servicios.IEquipoService;
import ar.com.noxit.servicios.IFechaService;
import ar.com.noxit.servicios.IJugadorService;
import ar.com.noxit.servicios.ISancionService;
import ar.com.noxit.servicios.impl.ITorneoService;

import com.opensymphony.xwork2.Preparable;

@SuppressWarnings("serial")
public class SancionAction extends BaseActionModelDrivenPaginado<Sancion>
		implements Preparable {

	private boolean habilitarOKenabled = true;
	private ITorneoService torneoService;
	private IFechaService fechaService;
	private IJugadorService jugadorService;
	private IEquipoService equipoService;
	private String torneoId;
	private List<Torneo> torneos;
	private String fechaSeleccionada;
	private String equipoSeleccionado;
	private String jugadorSeleccionado;
	private List<Fecha> fechas;
	private List<Jugador> jugadores;
	private List<Equipo> equipos;
	
	public String getEquipoSeleccionado() {
		return equipoSeleccionado;
	}

	public void setEquipoSeleccionado(String equipoSeleccionado) {
		this.equipoSeleccionado = equipoSeleccionado;
	}

	public List<Equipo> getEquipos() {
		return equipos;
	}

	public void setEquipos(List<Equipo> equipos) {
		this.equipos = equipos;
	}

	public String getFechaSeleccionada() {
		return fechaSeleccionada;
	}

	public void setFechaSeleccionada(String fechaSeleccionada) {
		this.fechaSeleccionada = fechaSeleccionada;
	}

	public String getJugadorSeleccionado() {
		return jugadorSeleccionado;
	}

	public void setJugadorSeleccionado(String jugadorSeleccionado) {
		this.jugadorSeleccionado = jugadorSeleccionado;
	}
	
	public List<Torneo> getTorneos() {
		return torneos;
	}

	public void setTorneos(List<Torneo> torneos) {
		this.torneos = torneos;
	}

	public List<Fecha> getFechas() {
		return fechas;
	}

	public void setFechas(List<Fecha> fechas) {
		this.fechas = fechas;
	}

	public List<Jugador> getJugadores() {
		return jugadores;
	}

	public void setJugadores(List<Jugador> jugadores) {
		this.jugadores = jugadores;
	}

	public String getTorneoId() {
		return torneoId;
	}

	public void setTorneoId(String torneoId) {
		this.torneoId = torneoId;
	}

	public boolean isHabilitarOKenabled() {
		return habilitarOKenabled;
	}

	public void setHabilitarOKenabled(boolean habilitarOKenabled) {
		this.habilitarOKenabled = habilitarOKenabled;
	}

	public SancionAction(ISancionService sancionService, ITorneoService torneoService, IFechaService fechaService, IJugadorService jugadorService, IEquipoService equipoService){
        this.genericService = sancionService;
        this.torneoService = torneoService;
        this.fechaService = fechaService;
        this.jugadorService = jugadorService;
        this.equipoService = equipoService;
        this.jugadorSeleccionado = StringUtils.EMPTY;
        this.setDisplaytagGridID("sancionGrid");
	}
	
	@Override
	public Sancion getModel() {
		return this.getEntity();
	}

	@Override
	protected boolean tienePermiso() {
		return true;
	}
	
	@Override
	public void prepare() throws Exception {
		super.prepare();
		if (getRequestId() != 0) {
			this.entity = genericService.findById(Long.valueOf(getRequestId()));
			if(this.entity.getJugadorSancionado() != null){
				this.setJugadorSeleccionado(this.entity.getJugadorSancionado().toString());
			}
			this.fechas = this.fechaService.getAllFechasByTorneoId(entity.getFechaInicio().getTorneo().getId());
			this.equipos = entity.getFechaInicio().getTorneo().getEquipos();
		} else if(getTorneoId() != null){
			this.entity = new Sancion();
			this.fechas = this.fechaService.getAllFechasByTorneoId(Long.valueOf(torneoId));
			this.equipos = this.torneoService.findById(Long.valueOf(torneoId)).getEquipos();
		}else{
			this.entity = new Sancion();
		}
	}

	@Override
	public void validate() {
		if(StringUtils.isBlank(jugadorSeleccionado)){
			addActionError("Seleccione un jugador");
		}
	}

	@Override
	public String save() {
		this.fillEntity();
		return super.save();
	}
	
	@Override
	public String update() {
		this.fillEntity();
		return super.update();
	}

	private void fillEntity() {
		this.entity.setFechaInicio(this.fechaService.findById(Long.valueOf(fechaSeleccionada)));
		this.entity.setJugadorSancionado(this.jugadorService.findById(Long.valueOf(jugadorSeleccionado)));
		this.entity.setTorneo(this.entity.getFechaInicio().getTorneo());
	}
	
	@SkipValidation
	public String mostrarJugadores() {
		if (StringUtils.isNotBlank(equipoSeleccionado)) {
			this.jugadores = new ArrayList<Jugador>(this.equipoService.findById(Long.valueOf(equipoSeleccionado)).getJugadores());
		}
		return Constantes.SUCCESS;
	}
	
	@Override
	@SkipValidation
	public String edit() {
		Usuario usuario = getSessionUser();
		if(!usuario.isAnonimo() && "PUSER".equals(usuario.getPerfiles().iterator().next().getNombre())){
			Sancion sancion = genericService.findById(Long.valueOf(getRequestId()));
			if(usuario.equals(sancion.getTorneo().getCreador())){
				return super.edit();
			}
		}
		return ERROR;
	}	
	
}
