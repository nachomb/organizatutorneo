<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>

<sx:head parseContent="true" />
<script type="text/javascript">
function doSubmit(){
	document.form.submit();
	return true;
}
function mostrarJugadores() {
	dojo.event.topic.publish("mostrarJugadores");
}
</script>

<div class="container">

<div align="center"><br>
<s:if test="id == null">
	<h2>Sancion Nueva</h2>
</s:if>
<s:else>
	<h2>Editar Sancion</h2>
</s:else>
</div>
<s:form id="form" key="form" action="%{actionMethod}" method="POST" theme="simple" cssClass="form-horizontal">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="torneoId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror/>
	<br>
	<br>
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="selectFecha">Fecha</label>
		   <div class="col-lg-4">
			<s:select cssClass="form-control"  name="fechaSeleccionada" label="Fechas" disabled="%{readOnly}"
							list="fechas" listValue="nombre" listKey="id"
							value="%{fechaInicio.id}" required="true" theme="simple"/>
		   </div>
		 </div>
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="selectEquipo">Equipo</label>
		   <div class="col-lg-4">
				<s:select cssClass="form-control" id="equipoSeleccionado"
								key="equipoSeleccionado" label="Equipo" disabled="%{readOnly}"
								list="equipos" listValue="nombre" listKey="id" value="%{jugadorSancionado.equipo.id}"
							    required="true" theme="simple" onchange="javascript:mostrarJugadores();return false;" />
		   </div>
		 </div>
		<s:url id="url_jugadores" action="Sanciones_mostrar_jugadores" />
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="selectJugador">Jugador</label>
		   <div class="col-lg-4">
				<sx:div theme="simple" id="details" href="%{url_jugadores}" disabled="%{readOnly}"
 					listenTopics="mostrarJugadores" formId="form" showLoadingText="cargando jugadores"></sx:div>
		   </div>
		 </div>
		
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="inputCantidadFechas">Cantidad de fechas</label>
		   <div class="col-lg-1">
				<s:textfield key="cantidadFechas" disabled="%{readOnly}"
								label="Cantidad de fechas" maxlength="3" size="3" theme="simple" cssClass="form-control"/>
		   </div>
	 	</div>
		 
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="inputCantidadFechas">Cumplida</label>
		   <div class="col-lg-1">
				<s:checkbox key="cumplida" disabled="%{readOnly}" label="Cumplida" theme="simple" cssClass="form-control"/>
		   </div>
		 </div>
		
		<div class="form-group">
		   <div class="col-lg-offset-2 col-lg-10">
				<button type="button"
					onclick="return doSubmit()" class="btn btn-success">Guardar</button>
				<button type="button"
					onclick="history.back()" class="btn btn-danger">Cancelar</button>
		   </div>
	    </div>
</s:form>
</div>