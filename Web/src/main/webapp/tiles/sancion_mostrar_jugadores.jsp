<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="jugadores != null">

	 <s:select id="jugadorSeleccionado" theme="simple"
	 key="jugadorSeleccionado" listKey="id" listValue="nombreYApellido" 
	 list="jugadores" multiple="false" cssClass="form-control" value="%{jugadorSancionado.id}" />
	 
</s:if>