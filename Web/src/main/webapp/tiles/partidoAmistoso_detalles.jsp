<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<link rel="stylesheet" media="all" type="text/css"
	href="css/jquery-ui-1.8.6.custom.css" />
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/web/struts/optiontransferselect.js"></script>
<script type="text/javascript" language="JavaScript">
	function doSubmit() {
		//seleccionarTodos(true);
		document.form.submit();
		return true;
	}

	function seleccionarTodos(seleccionado) {

		var select = document.getElementById("miembrosSeleccionados");
		var elems = select.getElementsByTagName("option");
		for ( var i = 0; i < elems.length; ++i) {
			elems[i].selected = seleccionado;
		}

	}
	function mostrarJugadores() {
		dojo.event.topic.publish("mostrarJugadores");
	}
	function cambiarEstado() {
		dojo.event.topic.publish("cambiarEstado");
	}
	$(function() {
		$('#fechaYHora').datetimepicker({

			timeOnlyTitle : 'Fecha y Hora',
			timeText : 'horario',
			hourText : 'hora',
			minuteText : 'minutos',
			secondText : 'segundos',
			currentText : 'ahora',
			closeText : 'cerrar',
			dateFormat : 'dd/mm/yy',
			timeFormat : 'hh:mm'
		});
	});

	function editGrid(clave, estado) {
		var selector = 'input[id="clave"][value="' + clave + '"]';
		var msj = $(selector).next().attr('value');

		$.ajax({
			type : "POST",
			url : "Invitacion_editGrid.action",
			data : "requestId=" + clave + "&men=" + msj + "&oper=" + estado ,
			success : function(msg) {
				var c = $(selector).parent().next();
				$('img', c).attr('src', 'imagenes/' + estado + '.png');
				$('#confirmados').text($('#confirmados',msg).text());
				$('#cancelados').text($('#cancelados',msg).text());
				$('#enDuda').text($('#enDuda',msg).text());
				$('#pendientes').text($('#pendientes',msg).text());
			}
		});
	}
</script>

<s:if test="id == null">
	<div align="center"><br>
	<h2>Partido Nuevo</h2>
	<br>
	</div>
</s:if>
<s:else>
	<div align="center"><br>
	<h2>Partido</h2>
	<br>
	<h3>Invitados</h3>
	</div>
</s:else>

<s:form id="demo" key="form" action="%{actionMethod}" method="POST">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
	<br>
	<s:if test="id != null">
		<s:action name="PartidoAmistoso_calcularTotales"/>
		<s:label id="confirmados" key="cantConfirmados" label="Confirmaron" />
		<s:label id="cancelados" key="cantCancelados" label="Cancelaron" />
		<s:label id="enDuda" key="cantEnDuda" label="En duda" />
		<s:label id="pendientes" key="cantPendientes" label="No respondieron" />
		<display:table id="invitacionGrid" name="invitaciones"
			excludedParams="*">
			<s:set name="myrow" value="#attr.invitacionGrid" />
			<display:column title="N�" class="ancho-20"><%=pageContext
									.getAttribute("invitacionGrid_rowNum")%></display:column>
			<display:column property="jugador" title="Jugador" />
			<display:column title="Comentario">
				<s:hidden key="#myrow.id" id="clave" />
				<s:textfield key="#myrow.mensaje" label="Lugar" theme="simple"
					cssStyle="width: 97%;" disabled="#myrow.jugador.id != jugadorSession.id" />
			</display:column>
			<display:column title="Estado">
				<s:if test="#myrow.estado == 'pendiente'">
					<img src="imagenes/pendiente.jpg" alt="pendiente" />
				</s:if>
				<s:elseif test="#myrow.estado == 'confirmado'">
					<img src="imagenes/confirmado.png" alt="confirmado" />
				</s:elseif>
				<s:elseif test="#myrow.estado == 'cancelado'">
					<img src="imagenes/cancelado.png" alt="cancelado" />
				</s:elseif>
				<s:elseif test="#myrow.estado == 'en duda'">
					<img src="imagenes/en duda.png" alt="en duda" />
				</s:elseif>
			</display:column>
			<display:column title="">
				<s:if test="#myrow.jugador.id == jugadorSession.id">
				<a href="javascript:editGrid(<s:property value="#myrow.id"/>,'confirmado');"><img
					src="imagenes/confirmado.png" alt="confirmado" /> </a>
				</s:if>
			</display:column>
			<display:column title="">
				<s:if test="#myrow.jugador.id == jugadorSession.id">
					<a href="javascript:editGrid(<s:property value="#myrow.id"/>,'cancelado');"><img
					src="imagenes/cancelado.png" alt="cancelado" /></a>
				</s:if>
			</display:column>
			<display:column title="">
				<s:if test="#myrow.jugador.id == jugadorSession.id">
					<a href="javascript:editGrid(<s:property value="#myrow.id"/>,'en duda');"><img
					src="imagenes/en duda.png" alt="en duda" /></a>
				</s:if>
			</display:column>
		</display:table>
	</s:if>
	<br>
	<table align="center" width="100%">
		<s:if test="id != null">
			<tr>
				<td><s:label id="id" name="id" key="id" label="Id" /></td>
			</tr>
		</s:if>
		<tr>
			<td><s:textfield label="Fecha y Hora" type="text"
				name="fechaYHora" id="fechaYHora" required="true" disabled="%{readOnly}"/></td>
		</tr>

		<tr>
			<td><s:textfield key="lugar" disabled="%{readOnly}"
				label="Lugar" maxlength="30" required="true" /></td>
		</tr>
		<tr>
			<td><s:textfield key="cantParticipantes" disabled="%{readOnly}"
				label="Cantidad de participantes" maxlength="3" size="3"
				required="true" /></td>
		</tr>
		<s:if test="id == null">
			<s:url id="j_url" action="GrupoMostrarJugadores" />
			<tr>
				<td class="tdLabel"><label class="label"> Jugadores:</label></td>
				<td><sx:div theme="simple" id="details" href="%{j_url}"
					listenTopics="mostrarJugadores" formId="demo" showLoadingText=""></sx:div></td>
			</tr>
		</s:if>
		<s:else>

		</s:else>

		<tr>
			<s:if test="%{habilitarOKenabled}">
				<td><input class="button" type="button"
					onclick="return doSubmit()" value="OK" style="float: right;" /></td>
			</s:if>
			<td><input class="button" type="button"
				onclick="javascript:history.back(1)"
				value="Volver" /></td>
		</tr>
	</table>
</s:form>