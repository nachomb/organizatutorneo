<%-- <%@page import="ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario"%> --%>
<%-- <%@ taglib prefix="s" uri="/struts-tags" %> --%>
<%-- <%@ taglib prefix="authz" uri="http://acegisecurity.org/authz" %> --%>
<%-- <%@page import="org.acegisecurity.context.SecurityContextHolder"%> --%>

 <!-- Main jumbotron for a primary marketing message or call to action -->
 <div class="jumbotron">
   <div class="container">
     <h1>OTorneos.com.ar</h1>
     <p>Te presentamos la plataforma online para mejorar la gesti&oacute;n de tus torneos.</p>
     <p>
     	<a class="btn btn-primary btn-lg" href="CrearUsuario.action" >Crear Usuario</a>
     	<a class="btn btn-success btn-lg" href="Login.action" >Login</a>
     </p>
   </div>
 </div>
<!-- end #content -->
<div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-lg-4">
          <h2>Generaci&oacute;n de Fixture</h2>
          <p>Ida o ida y vuelta. Playoff o todos contra todos.</p>
          <p><a class="btn btn-default" href="Caracteristicas.action">Ver detalle </a></p>
        </div>
        <div class="col-lg-4">
          <h2>C&aacute;lculo de Estad&iacute;sticas</h2>
          <p>Tabla de Posiciones, Goleadores y Tarjetas. </p>
          <p><a class="btn btn-default" href="Caracteristicas.action">Ver detalle</a></p>
       </div>
        <div class="col-lg-4">
          <h2>Compart&iacute; los resultados</h2>
          <p>Tu propia web para compartir las estad&iacute;sticas y resultados de tus torneos.</p>
          <p><a class="btn btn-default" href="Caracteristicas.action">Ver detalle</a></p>
        </div>
      </div>
</div>