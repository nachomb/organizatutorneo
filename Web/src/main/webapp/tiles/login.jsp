<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript">
    $(document).ready(function() {
        $('#j_username').focus();
    });
</script>

<div class="container">

    <h1>Login</h1>
    <s:form id="loginForm" action="/j_acegi_security_check" method="POST" theme="simple" cssClass="form-horizontal">
         <div class="form-group">
		   <label class="col-lg-2 control-label" for="inputEmail">Email</label>
		   <div class="col-lg-4">
        		<s:textfield id="j_username" name="j_username" label="Usuario" cssClass="form-control"/>
		   </div>
		 </div>
        
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="inputPassword">Contrase&ntilde;a</label>
		   <div class="col-lg-4">
        	<s:password id="j_password" name="j_password" label="Contraseņa" cssClass="form-control" />
		   </div>
		 </div>
        
          <div class="form-group">
		    <div class="col-lg-offset-2 col-lg-10">
		      <s:submit cssClass="btn btn-success" value="Login" />
		    </div>
  		 </div>
  </s:form>
   
</div>