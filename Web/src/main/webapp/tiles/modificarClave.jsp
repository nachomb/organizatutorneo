<%@ taglib prefix="s" uri="/struts-tags"%>

	<script type="text/javascript" language="JavaScript">
	function doSubmit(){
		document.form.submit();
		return true;
	}
	</script>


	<div align="center">
		<h2>Cambiar Contrase&ntilde;a</h2>
		<br/>
	</div>

<s:form key="form" action="ModificarClave" method="POST">
	<table align="center" style="width: 400px;">
		<tr>
			<td><s:password key="claveVieja" 
				label="Contraseña actual" maxlength="100" required="true"/></td>
		</tr>
		<tr>
			<td><s:password key="claveNueva" 
				label="Nueva contraseña" maxlength="100" required="true"/></td>
		</tr>
		<tr>
			<td><s:password key="claveNuevaRepetida"
				label="Confirmación nueva contraseña" maxlength="100" required="true"/></td>
		</tr>
		<tr>
			<td><input class="button" type="button" onclick="return doSubmit()" value="OK" style="float:right;" /></td>
		</tr>
	</table>
</s:form>