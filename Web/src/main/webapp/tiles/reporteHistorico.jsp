<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" language="JavaScript">
	function doSubmit() {
		document.form.submit();
		return true;
	}
</script>


<div align="center"><br>
<h2>Reporte Histórico</h2>
<br>
</div>
<tbody>
<s:form key="form" action="%{actionMethod}" method="POST">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
	<br>
	<br>
	<td><s:select id="idEquipo" name="idEquipo"
		key="idEquipo" label="Equipo"
		list="equiposListado" listValue="nombre" listKey="id"
		multiple="false" value="%{equipos.{id}}" required="true" cssClass="noMultiple" />
	</td>
	<td>
	<input class="button" type="button"
				onclick="doSubmit()" value="Reporte" /></td>
	<s:if test="reporteHistorico != null">
			<s:label key="reporteHistorico.equipo" label=""/>
			<tr>
			<td><display:table id="tablaPosicionesGrid" name="reporteHistorico.posiciones"
				sort="list" requestURI="Torneo_reporteHistorico.action">
				<display:column title="Torneo" property="torneo.nombre"
					sortable="true" class="ancho-120">
				</display:column>
				<display:column title="Posicion" class="ancho-60" property="posicion"></display:column>
				<display:column title="PJ" property="partidosJugados"
					sortable="true" class="ancho-60">
				</display:column>
				<display:column title="PG" property="partidosGanados"
					sortable="true" class="ancho-60">
				</display:column>
				<display:column title="PE" property="partidosEmpatados"
					sortable="true" class="ancho-60">
				</display:column>
				<display:column title="PP" property="partidosPerdidos"
					sortable="true" class="ancho-60">
				</display:column>
				<display:column title="GF" property="golesAFavor" sortable="true" class="ancho-60">
				</display:column>
				<display:column title="GC" property="golesEnContra" sortable="true" class="ancho-60">
				</display:column>
				<display:column title="Dif" property="diferencia" sortable="true" class="ancho-60">
				</display:column>
				<display:column title="Pts" property="puntos" sortable="true" class="ancho-60">
				</display:column>
				<display:column title="%" property="porcentajePuntos" sortable="true" class="ancho-60">
				</display:column>
				<display:column title="Cant. Amarillas" property="amarillas" sortable="true" class="ancho-60">
				</display:column>
				<display:column title="Cant. Rojas" property="rojas" sortable="true" class="ancho-60">
				</display:column>
			</display:table></td>
					<tr>
			<td><display:table id="tablaGolesGrid" name="reporteHistorico.reporteJugadores"
				sort="list" requestURI="Torneo_reporteHistorico.action"
				defaultsort="2" defaultorder="descending">
				<display:column title="Jugador" property="jugador"
				  sortable="true" class="ancho-120">
				</display:column>
				<display:column title="PJ" property="cantPartidos"
				 	sortable="true" class="ancho-60">
				</display:column>
				<display:column title="Goles" property="cantGoles"
				    sortable="true" class="ancho-60">
				</display:column>
				<display:column title="Goles por partido" property="golesPromedio"
				 	sortable="true" class="ancho-60" format="{0,number,###.##}" >
				</display:column>
				<display:column title="Veces Figura" property="cantFigura"
				 	sortable="true" class="ancho-60">
				</display:column>
				<display:column title="Puntaje por partido" property="puntajePromedio"
				 	sortable="true" class="ancho-60" format="{0,number,###.##}">
				</display:column> 
				<display:column title="Cant. Amarillas" property="cantAmarillas"
				 	sortable="true" class="ancho-60">
				</display:column>
				<display:column title="Cant. Rojas" property="cantRojas"
				 	sortable="true" class="ancho-60">
				</display:column>
			</display:table></td>
		</tr>
	</s:if>			
				
     
</s:form>
</tbody>
