<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" language="JavaScript">
    function doSubmit() {
        document.form.submit();
        return true;
    }

    function mostrarAdvertenciaInactivo(checkBox) {
        if (!checkBox.checked)
            alert("Atenci�n: De inactivarse el Perfil, las relaciones a usuarios se eliminar�n.");
    }
</script>

<div align="center">
<h2><s:property value="formTitulo" /></h2>
</div>

<s:form key="form" action="%{actionMethod}" method="POST">
    <s:hidden key="requestId" id="requestId" />
    <s:hidden key="actionMethod" value="%{actionMethod}" />
    <s:actionerror />

    <table align="center" width="100)">
        <s:if test="id != null">
            <tr>
                <td><s:label id="id" name="id" key="id" label="Id" /></td>
            </tr>
        </s:if>
        <tr>
            <td><s:textfield key="nombre" disabled="%{readOnly}" label="Nombre" maxlength="100" required="true" /></td>
        </tr>
        <tr>
            <td><s:textfield key="descripcion" disabled="%{readOnly}" label="Descripci�n" maxlength="100"
                required="true" /></td>
        </tr>
        <tr>
            <td><s:checkbox id="enabled" key="activo" disabled="%{readOnly}" label="Activo"
                onclick="mostrarAdvertenciaInactivo(this);" /></td>
        </tr>


        <s:if test="availableActions.size > 0">
            <s:select label="Acciones" name="selectedActionIDs" disabled="%{readOnly}" list="availableActions"
                listKey="id" listValue="nombre" multiple="true" value="%{acciones.{id}}" required="true" />
        </s:if>
        <s:else>
            <tr bordercolor="red" align="center">
                <td style="font-family: Arial; color: red; font-size: 14" align="center">No se encontraron acciones
                </td>
            </tr>
        </s:else>

        <tr>
            <td colspan="3" style="font-family: Arial; color: red; font-size: 14" align="center"><br />
            <s:property value="mensajeAdvertencia" /> <br />
            </td>
        </tr>

        <tr>
            <s:if test="%{habilitarOKenabled}">
                <td><input class="button" type="button" onclick="return doSubmit()" value="OK"
                    style="float: right;" /></td>
            </s:if>
            <td><input class="button" type="button" onclick="location.href='Perfil_list.action'" value="Cancelar" /></td>
        </tr>
    </table>
</s:form>