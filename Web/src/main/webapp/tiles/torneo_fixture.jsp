<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" language="JavaScript">
    function doSubmit() {
        document.form.submit();
        return true;
    }
</script>


<div class="container">
<br>
<h2>Fixture - <s:property value="%{nombre}" /></h2>
<br>
    <s:hidden key="id" />
    <s:hidden key="requestId" />
    <s:hidden key="actionMethod" value="%{actionMethod}" />
    <s:actionerror />
				<div align="center">
					<s:url var="url" action="Fecha_add">
						<s:param name="torneoIdNewFecha" value="%{requestId}"/>
					</s:url>
					<s:a href="%{url}" cssClass="btn btn-success">Nueva Fecha</s:a>
				</div>
			    <br>
			    <br>
                <s:if test="tipoTorneo==\"Todos contra Todos\"" >
                <div>
                    <s:iterator value="fechas" var="fecha">
	                    <div class="panel panel-info">
						  <div class="panel-heading">
						    <h3 class="panel-title"><s:property value="#fecha.numero" /> - <s:property value="#fecha.nombre" /></h3>
						  </div>
						  <div class="panel-body">
	                        	<div align="center">
		                        <ul class="list-group">
								    <s:iterator value="#fecha.partidos" var="partido">
								  			<li class="list-group-item">
								  				<s:url action="Partido_input" var="urlPartido">
								  					<s:param name="requestId" value="#partido.id"/>
								  				</s:url>
								  				<s:a href="%{urlPartido}"><s:property value="#partido.equipoLocal" /> <span class="badge"><s:property value="#partido.golesLocal" /></span> vs <span class="badge"><s:property value="#partido.golesVisitante" /></span> <s:property value="#partido.equipoVisitante" /></s:a>
								  				<span class="label label-default"><s:date name="#partido.fechaPartido" format="dd/MM HH:mm" /></span>
								  				<span class="label label-default"><s:property value="#partido.descripcion" /></span>
								  			</li>
			                        </s:iterator>
								</ul>
		                        </div>
						  </div>
						  <s:if test="#fecha.equipoLibre != null">
		                        <div align="center" class="panel-footer">
										Equipo Libre: <s:property value="#fecha.equipoLibre" />
								</div>
						   </s:if>
						</div>
                    </s:iterator>
                </div>
                </s:if>
                <s:else>
	                <div>
	                    <s:iterator value="fechas" var="fecha">
		                    <div style="float:left; border-style:outset;">
		                        <div><s:property value="#fecha.numero" /> - <s:property value="#fecha.nombre" /></div>
		                        <s:iterator value="#fecha.partidos" var="partido">
		                            <div align="center">
		                                <div style="border-style:outset;"><s:property value="#partido.equipoLocal.nombre" /> <s:property value="#partido.golesLocal" /> </div>
		                                <div style="border-style:outset;"><s:property value="#partido.equipoVisitante.nombre" /> <s:property value="#partido.golesVisitante" /></div>
		                            </div>
		                        </s:iterator>							
		                    </div>
	                    </s:iterator>
	                </div>    
                </s:else>
	            <s:if test="%{!readOnly}">
					<s:url action="Torneo_list" var="urlTorneoList"/>
					<button type="button" onclick="location.href='<s:property value="%{urlTorneoList}" />'" class="btn btn-default">Volver</button>
				</s:if>
</div>
