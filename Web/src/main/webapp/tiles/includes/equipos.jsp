<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

	<script type="text/javascript" language="JavaScript">
		$(document).ready(function() {
		  zebraRows('tbody tr:odd', 'odd');
		  zebraRows('tbody tr:even', 'even');
		});
		
		//used to apply alternating row styles
		function zebraRows(selector, className)
		{
		  $(selector).removeClass(className).addClass(className);
		} 
	</script>

	<div class="equipos">
	<!-- 	<table width="286" border="0" cellspacing="0" cellpadding="0" class="tablaPosiciones">  -->
		<table>
		    <tbody>
		   	<tr>
		    	<td class="tituloEquipo">Equipos</td>
		  	</tr>
		    <s:iterator value="#session.equipos">
		       	<tr>
			    	<td class="equipo" ><s:property /></td>
			  	</tr>
		    </s:iterator>
		  	</tbody>
		</table>
	</div>

	<div id="publicidadGoogle">
	
	<script type="text/javascript">
    var random_number = Math.random();
    if (random_number < .8){
    	google_ad_client = "ca-pub-7023390429459798";
    	/* 205x250 imagen estaticos */
    	google_ad_slot = "6447552373";
    	google_ad_width = 250;
    	google_ad_height = 250;
    } else { 
    	google_ad_client = "ca-pub-7023390429459798";
    	/* 250x250 solo texto */
    	google_ad_slot = "3901801697";
    	google_ad_width = 250;
    	google_ad_height = 250;
    } 
	</script> 
	<script type="text/javascript"     src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
	
	</div>