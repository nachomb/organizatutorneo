<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="authz" uri="http://acegisecurity.org/authz"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="usuario">
<ul>
    <li><s:a href="j_acegi_logout" cssClass="claseEnlace">Login|Salir</s:a></li>
    <li><span>Usuario: <authz:authentication operation="username" /></span></li>
</ul>
</div>
