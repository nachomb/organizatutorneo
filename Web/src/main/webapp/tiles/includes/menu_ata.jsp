<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="authz" uri="http://acegisecurity.org/authz"%>
<%@ page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!-- <div id="menu"> -->
<!-- 	<ul class="menu"> -->
<%--         <li class="itemMenu"><s:a href="Principal.action" cssClass="claseEnlace">Home</s:a></li> --%>
<%--         <s:iterator value="#session.itemsMenu"> --%>
<%--             <li class="itemMenu"><s:url var="url" action="%{accion.menuEntrada}" /> <s:a href="%{url}"> --%>
<%--                 <s:property value="titulo" /> --%>
<%--             </s:a></li> --%>
<%--         </s:iterator> --%>
<!-- 	</ul> -->
<!-- </div> -->



    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">

		<s:if test="%{#session.itemsMenu == null}">
		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <a class="navbar-brand" href="Principal.action">Inicio</a>
		        </div>
		        <div class="navbar-collapse collapse">
		          <ul class="nav navbar-nav">
		            <li><a href="Caracteristicas.action">Caracter&iacute;sticas</a></li>
		            <li><a href="Contacto.action">Contacto</a></li>
<!-- 		            <li class="active"><a href="./Jumbotron Template for Bootstrap_files/Jumbotron Template for Bootstrap.htm">Home</a></li> -->
<!-- 		            <li><a href="http://getbootstrap.com/examples/jumbotron/#contact">Videos</a></li> -->
<!-- 		            <li class="dropdown"> -->
<!-- 		              <a href="./Jumbotron Template for Bootstrap_files/Jumbotron Template for Bootstrap.htm" class="dropdown-toggle" data-toggle="dropdown">Casos <b class="caret"></b></a> -->
<!-- 		              <ul class="dropdown-menu"> -->
<!-- 		                <li><a href="./Jumbotron Template for Bootstrap_files/Jumbotron Template for Bootstrap.htm">Action</a></li> -->
<!-- 		                <li><a href="./Jumbotron Template for Bootstrap_files/Jumbotron Template for Bootstrap.htm">Another action</a></li> -->
<!-- 		                <li><a href="./Jumbotron Template for Bootstrap_files/Jumbotron Template for Bootstrap.htm">Something else here</a></li> -->
<!-- 		                <li class="divider"></li> -->
<!-- 		                <li class="dropdown-header">Nav header</li> -->
<!-- 		                <li><a href="./Jumbotron Template for Bootstrap_files/Jumbotron Template for Bootstrap.htm">Separated link</a></li> -->
<!-- 		                <li><a href="./Jumbotron Template for Bootstrap_files/Jumbotron Template for Bootstrap.htm">One more separated link</a></li> -->
<!-- 		              </ul> -->
<!-- 		            </li> -->
		          </ul>
			          <s:form id="loginForm" action="/j_acegi_security_check" method="POST" theme="simple" cssClass="navbar-form navbar-right">
		            <div class="form-group">
		              <s:textfield id="j_username" name="j_username" label="Usuario" cssClass="form-control" placeholder="Email"/>
		            </div>
		            <div class="form-group">
		              <s:password id="j_password" name="j_password" label="Contraseņa" cssClass="form-control" placeholder="Password"/>
		            </div>
		            <button type="submit" class="btn btn-success">Login</button>
		          </s:form>
		        </div><!--/.navbar-collapse -->
		      
		</s:if>
		<s:else>
			<div class="navbar-header">
		          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <a class="navbar-brand" href="Principal.action">Inicio</a>
		        </div>
		        <div class="navbar-collapse collapse">
		          <ul class="nav navbar-nav">
			         <s:iterator value="#session.itemsMenu">
			            <li><s:url var="url" action="%{accion.menuEntrada}" /> <s:a href="%{url}">
			                <s:property value="titulo" />
			            </s:a></li>
			        </s:iterator>
		        </ul>
		         <a class="btn btn-danger" href="j_acegi_logout">Salir</a>
		        </div><!--/.navbar-collapse -->
		</s:else>


		</div>
    </div>
   