<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(
		function() {
			//$("ul.subnav").parent().append("<span></span>"); //Only shows drop down trigger when js is enabled - Adds empty span tag after ul.subnav
			$("ul.topnav li").click(
					function() { //When trigger is clicked...
						//Following events are applied to the subnav itself (moving subnav up and down)
						$(this).find("ul.subnav").slideDown('fast').show(); //Drop down the subnav on click
						//$(this).find("ul.subnav").css('background-color', 'red'); 
						$(this).hover(function() {
						}, function() {
							$(this).find("ul.subnav").slideUp('slow'); //When the mouse hovers out of the subnav, move it back up
							});
					}
			);
		}
	);
	
</script>

<div id="menu">
<ul class="topnav">
    <s:iterator value="#session.itemsMenuNivel0" var="nivel0">
        <li><span><s:property value="titulo" /></span>
        <ul class="subnav">
            <s:iterator value="#session.itemsMenuNivel1" var="nivel1">
                <s:if test="#nivel0.id == #nivel1.itemMenuPadre.id">
                    <li><s:url var="url" action="%{accion.menuEntrada}" /> <s:a href="%{url}">
                        <s:property value="titulo" />
                    </s:a></li>
                </s:if>
            </s:iterator>
        </ul>
        </li>
    </s:iterator>
</ul>
</div>
