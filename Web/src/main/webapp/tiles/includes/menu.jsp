<%@ taglib prefix="s" uri="/struts-tags" %>

<div id="menu" class="front">

        <ul id="nav">
            <li><a class="menu-item">Informacion</a>
             	<ul class="subsubmenu">
					<li><a href="SobreNosotros.action" title="Sobre Liga Futboleras">Sobre Nosotros</a></li>
					<li><a href="FormatoTorneo.action" title="Formato del Torneo">Formato del Torneo</a></li>
					<li><a href="PreguntasFrecuentes.action" title="Preguntas Frecuentes">Preguntas Frecuentes</a></li>
            	</ul> 
            </li>
            
            <li><a class="menu-item">Estadisticas</a>
           		 <ul class="subsubmenu">
           		    <s:iterator value="#session.torneos" var="torneo">
           		    	<s:set name="itTor" value="#attr.invitacionGrid"/>
           		    	<s:url action="Estadisticas" var="url_torneo" includeParams="none">
           		    		<s:param name="requestId" value="%{#torneo.id}" />
           		    		<s:param name="act" value="true" />
           		    	</s:url>
                		<li><s:a href="%{url_torneo}" title="%{#torneo.nombre}"><s:property /></s:a></li>
                    </s:iterator>
                    <s:url action="Estadisticas" var="url_torneosAnteriores"  includeParams="none">
                    	<s:param name="act" value="false" />
           		    </s:url>
                    <li><s:a href="%{url_torneosAnteriores}" title="Torneos Anteriores">Torneos Anteriores</s:a></li>
               </ul> 
            </li>
            <li><a class="menu-item">Fixture</a>
           		<ul class="subsubmenu">
           		    <s:iterator value="#session.torneos" var="torneo">
           		    	<s:set name="itTor" value="#attr.invitacionGrid"/>
           		    	<s:url action="Fixture" var="url_torneo"  includeParams="none">
           		    		<s:param name="requestId" value="%{#torneo.id}" />
           		    		<s:param name="act" value="true" />
           		    	</s:url>
                		<li><s:a href="%{url_torneo}" title="%{#torneo.nombre}"><s:property /></s:a></li>
                    </s:iterator>
               </ul> 
            </li>
          	<li><a href="Revistas.action" class="menu-item">Resumenes</a> </li>
            <li><a href="Inscripcion.action" class="menu-item">Inscripcion</a></li>
            <li><a href="Contacto.action" class="menu-item">Contacto</a></li>    
        </ul>
	</div>
