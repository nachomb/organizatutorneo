<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="authz" uri="http://acegisecurity.org/authz"%>
<%@ page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">

		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
                  	 <s:url action="ConsultaTorneo" var="urlConsultaTorneo"  >
	            		<s:param name="uid" value="%{#session.uid}" />
	            	</s:url>
		          <s:a cssClass="navbar-brand" href="%{urlConsultaTorneo}">Inicio</s:a>
		        </div>
		        <div class="navbar-collapse collapse">
		          <ul class="nav navbar-nav">
		            <li class="dropdown"> 
		              <a href="" class="dropdown-toggle" data-toggle="dropdown">Torneos <b class="caret"></b></a>
		              <ul class="dropdown-menu">
   				         <s:iterator value="#session.torneos" var="torneo">
			            	<s:url action="DetalleTorneo" var="urlConsultaTorneo"  >
	            				<s:param name="requestId" value="%{#torneo.id}" />
	            			</s:url>
							<li><s:a href="%{urlConsultaTorneo}"><s:property value="nombre" /></s:a></li>
			        	</s:iterator>
		              </ul>
		            </li>
		          </ul>
	              <div class="nav navbar-nav navbar-right">
		          	<a class="btn btn-default" href="CrearUsuario.action">Crear usuario</a>
		            <a class="btn btn-default" href="Login.action" >Login</a>
		    	  </div>
		        </div><!--/.navbar-collapse -->
		      

		</div>
    </div>
   