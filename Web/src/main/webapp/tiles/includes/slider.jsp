<%@ taglib prefix="s" uri="/struts-tags"%>

<link rel="stylesheet" href="css/themes/default/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/themes/pascal/pascal.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/themes/orman/orman.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/style-slider.css" type="text/css" media="screen" />

<script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
 <script type="text/javascript">
 $(window).load(function() {
     $('#slider-publicidad').nivoSlider();
 });
 </script>


<div id="wrapper">

    <div class="slider-wrapper theme-default">
        <div class="ribbon"></div>
        <div id="slider-publicidad" class="nivoSlider">
        	<s:iterator value="#session.publicidadesSlider" var="publicidad">
            		<s:a href="%{link}" target="_blank" >  <img src="<s:url value="%{imagenSlider}" />" alt="" /> </s:a>
   			 </s:iterator>
        </div>
    </div>

</div>
