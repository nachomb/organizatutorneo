<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" language="JavaScript">
	function doSubmit(){
		document.form.submit();
		return true;
	}
	</script>

<s:form key="form" action="%{actionMethod}" method="POST">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
	<br>
	<br>
	<table align="center" width="100%">
		<s:if test="id != null">
			<tr>
				<td><s:label id="id" name="id" key="id" label="Id" /></td>
			</tr>
		</s:if>
		<tr>
			<td><s:textfield key="username" disabled="%{readOnly}"
				label="Usuario" maxlength="30" required="true" /></td>
		</tr>
		<s:if test="requestId != 0">
			<tr>
				<td><s:password key="claveSinEncriptar" disabled="%{readOnly}"
					label="Nueva contrase�a" maxlength="20" required="true" /></td>
			</tr>
			<tr>
				<td><s:password key="claveSinEncriptarRepetida"
					disabled="%{readOnly}" label="Repetir Contrase�a" maxlength="20"
					required="true" /></td>
			</tr>
			<tr>
				<td></td>
				<td><span>(De quedar vac�o se mantendr� la contrase�a
				actual)</span></td>
			</tr>
			<tr>
				<td><s:textfield key="telefonoCelular" disabled="%{readOnly}"
					label="Telefono celular" maxlength="20" required="false" /></td>
			</tr>
		</s:if>
		<s:else>
			<tr>
				<td><s:password key="claveSinEncriptar" disabled="%{readOnly}"
					label="Nueva contrase�a" maxlength="20" required="true" /></td>
			</tr>
			<tr>
				<td><s:password key="claveSinEncriptarRepetida"
					disabled="%{readOnly}" label="Repetir Contrase�a" maxlength="20"
					required="true" /></td>
			</tr>
			<tr>
				<td><s:textfield key="telefonoCelular" disabled="%{readOnly}"
					label="Telefono celular" maxlength="20" required="false" /></td>
			</tr>
		</s:else>
		<tr>
			<td><s:checkbox key="activo" disabled="%{readOnly}"
				label="Activo" /></td>
		</tr>

		<s:if test="availableProfiles.size > 0">
			<s:select label="Perfiles" name="selectedProfileIDs"
				disabled="%{readOnly}" list="availableProfiles" listKey="id"
				listValue="nombre" multiple="true" value="%{perfiles.{id}}"
				required="true" />
		</s:if>
		<s:else>
			<tr bordercolor="red" align="center">
				<td style="font-family: Arial; color: red; font-size: 16"
					align="center">No se encontraron perfiles</td>
			</tr>
		</s:else>

		<tr>
			<s:if test="%{habilitarOKenabled}">
				<td><input class="button" type="button"
					onclick="return doSubmit()" value="OK" style="float: right;" /></td>
			</s:if>
			<td><input class="button" type="button"
				onclick="location.href='Usuario_list.action'" value="Cancelar" /></td>
		</tr>
	</table>
</s:form>