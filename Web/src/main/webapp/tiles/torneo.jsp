<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>

<script type="text/javascript" language="JavaScript">
function doSubmit(){
	document.form.submit();
	return true;
}
function seleccionarTodos(seleccionado) {
	
	var select = document.getElementById("selectedEquipos");
	var elems = select.getElementsByTagName("option");
	for (var i = 0; i < elems.length; ++i) {
		elems[i].selected = seleccionado;
	}

}

</script>

<div class="container">

<s:if test="id == null">
	<div align="center"><br>
	<h2>Torneo Nuevo</h2>
	<br>
	</div>
</s:if>

<s:actionerror/>
<s:actionmessage/>
<s:fielderror/>

<s:form id="form" action="%{actionMethod}" method="POST" theme="simple" cssClass="form-horizontal">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
	<br>
	<br>
	<s:if test="id != null">
	<tr>
		<td><s:label id="id" name="id" key="id" label="Id" /></td>
	</tr>
	</s:if>
	
	<div class="form-group">
	   <label class="col-lg-2 control-label" for="inputNombre">Nombre del Torneo</label>
	   <div class="col-lg-10">
	      		<s:textfield key="nombre" disabled="%{readOnly}"
				label="Nombre" maxlength="30" required="true" cssClass="form-control"/>
	   </div>
	 </div>
	 <div class="form-group">
	   	   <label class="col-lg-2 control-label" for=""></label>
	   <div class="col-lg-10">
				<button type="button" class="btn btn-default" onclick="javascript:seleccionarTodos(true)">Seleccionar
				todos</button>
				<button type="button" class="btn btn-default" onclick="javascript:seleccionarTodos(false)">Seleccionar
				ninguno</button>
	   </div>
	 </div>
	 <div class="form-group">
	   <label class="col-lg-2 control-label" for="equipos">Equipos</label>
	   <div class="col-lg-10">
	 	<s:select id="selectedEquipos" name="selectedEquipos"
				key="selectedEquipos" label="Equipos" disabled="%{readOnly}"
				list="equiposListado" listValue="nombre" listKey="id"
				multiple="true" value="%{equipos.{id}}" required="true" theme="simple" cssClass="form-control" size="10"/>
	   </div>
	 </div>
	 <div class="form-group">
	   <label class="col-lg-2 control-label" for="sedes">Sedes</label>
	   <div class="col-lg-10">
		 <s:select id="sedeSeleccionada" key="sedeSeleccionada" theme="simple" listKey="id" listValue="nombre" 
			 			list="sedes" multiple="false" cssClass="form-control" value="%{sede.id}" headerKey="-1" headerValue="Seleccione"/>
	   </div>
	 </div>
	 <div class="form-group">
	   <label class="col-lg-2 control-label" for="sedes">Tipo de Torneo</label>
	   <div class="col-lg-10">
		<s:select name="tipoTorneo" key="tipoTorneo" headerKey="1" required="true"
						headerValue="Seleccione" list="tiposTorneo" cssClass="form-control" theme="simple"/>
	   </div>
	 </div>
	 <div class="form-group">
	   <label class="col-lg-2 control-label" for="sedes">Puntos por ganar</label>
	   <div class="col-lg-10">
		<s:textfield key="cantPuntosGanador" disabled="%{readOnly}"
						theme="simple" maxlength="3" size="3" cssClass="form-control" />
	   </div>
	 </div>
	 <div class="form-group">
	   <label class="col-lg-2 control-label" for="sedes">Puntos por empatar</label>
	   <div class="col-lg-10">
		<s:textfield key="cantPuntosEmpate" disabled="%{readOnly}"
						 maxlength="3" size="3" cssClass="form-control"/>
	   </div>
	 </div>
	 <div class="form-group">
	   <label class="col-lg-2 control-label" for="sedes">Puntos por presentismo</label>
	   <div class="col-lg-10">
		<s:textfield key="cantPuntosPerdedor" disabled="%{readOnly}"
				 maxlength="3" size="3" cssClass="form-control"/>
	   </div>
	 </div>
	 <div class="form-group">
       <label class="col-lg-2 control-label" for="idaYVuelta">Ida Y Vuelta </label>
	   <div class="col-lg-10">
        <s:checkbox key="idaYVuelta" disabled="%{readOnly}"
				label="Ida Y Vuelta" theme="simple"  />
	   </div>
    </div>
<!-- 	<tr id="torneosAnteriores"> -->
<!-- 		<td class="tdLabel"><label class="label"> Torneos Anteriores:</label></td> -->
<%-- 		<td><s:select id="selectedTorneosAnteriores" name="selectedTorneosAnteriores" --%>
<%-- 			key="selectedTorneosAnteriores" label="Torneos Anteriores" disabled="%{readOnly}" --%>
<%-- 			list="%{torneoService.findActivos()}" listValue="nombre" listKey="id" --%>
<%-- 			multiple="true" theme="simple"/> --%>
<!-- 		</td> -->
<!-- 	</tr> -->
<!-- 		<tr> -->
<%-- 			<td><s:checkbox key="activo" disabled="%{readOnly}" --%>
<%-- 				label="Activo" /></td> --%>
<!-- 		</tr> -->
		<div class="form-group">
		   <div class="col-lg-offset-2 col-lg-10">
				<button type="button"
					onclick="return doSubmit()" class="btn btn-success">Guardar</button>
				<button type="button"
					onclick="history.back()" class="btn btn-danger">Cancelar</button>
		   </div>
	    </div>
	
</s:form>
</div>