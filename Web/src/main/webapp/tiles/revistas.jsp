<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="contenidoSeccion" class="seccionResumenes">
	<h1>Resumenes de la Fecha</h1>
	<div id="separacionTitulos"></div>

	<div class="resumenes">
		<s:iterator value="%{resumenesActivos}" var="resumen" status="statusResumen">		
		 	<s:if test='#statusResumen.first'>
				<div class="tapaResumen">
					<img src="<s:url value="%{tapa}" />" class="imagenesBordeadas clickeable" onclick="window.open('<s:property value="%{link}" />','_blank');" />
					<h1><s:a href="%{link}" target="_blank"><s:property value="%{nombre}" /></s:a></h1>
				</div>
		 	</s:if>
		 	<s:else>
				<div class="tapaResumenMini">
					<img src="<s:url value="%{tapa}" />" class="imagenesBordeadas clickeable" onclick="window.open('<s:property value="%{link}" />','_blank');" />
					<h1><s:a href="%{link}" target="_blank"><s:property value="%{nombre}" /></s:a></h1>
				</div>
			</s:else>

		</s:iterator>
		
		<div class="publicidad google horizontal">					
			<script type="text/javascript"><!--
			google_ad_client = "ca-pub-7023390429459798";
			/* 468 x 60 horizontal */
			google_ad_slot = "4162682164";
			google_ad_width = 468;
			google_ad_height = 60;
			//-->
			</script>
			<script type="text/javascript"
			src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script>
		</div>
		
	</div>
</div>