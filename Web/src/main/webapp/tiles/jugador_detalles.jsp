<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>


<script type="text/javascript" language="JavaScript">
    function doSubmit() {
        document.form.submit();
        return true;
    }
</script>

<h2>Detalles Jugador</h2>

<s:form key="form" action="%{actionMethod}" method="POST">
    <s:hidden key="id" />
    <s:hidden key="requestId" />
    <s:hidden key="actionMethod" value="%{actionMethod}" />

    <table align="center" width="100%">
        <s:if test="id != null">
            <tr>
                <td><s:label id="id" name="id" key="id" label="Id" /></td>
            </tr>
        </s:if>
        <tr>
            <td><s:label key="nombre" disabled="%{readOnly}" label="Nombre" /></td>
        </tr>
        <tr>
            <td><s:label key="apellido" disabled="%{readOnly}" label="Apellido" /></td>
        </tr>
        <tr>
            <td><s:label key="apodo" disabled="%{readOnly}" label="Apodo" /></td>
        </tr>
        <tr>
            <td><s:checkbox key="activo" disabled="%{readOnly}" label="Activo" /></td>
        </tr>
        <tr>
            <td><s:label key="mail" disabled="%{readOnly}" label="Mail" /></td>
        </tr>

        <tr>
            <td><s:label key="equipo" label="Equipo" value="%{equipo.nombre}" /> </td>
        	
        </tr>

        <tr>
        	<td><input class="button" type="button" onclick="history.back()" value="Volver" /></td>
        </tr>
    </table>
</s:form>