<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<div class="container">
	<h2>Fixture - <s:property value="%{torneo.nombre}"/> </h2>
	<s:if test="%{!readOnly}">
		<div align="center">
			<s:url var="url" action="Fecha_add">
				<s:param name="torneoIdNewFecha" value="%{torneoId}"/>
			</s:url>
			<s:a href="%{url}" cssClass="btn btn-success">Nueva Fecha</s:a>
		</div>
		<br>
		<br>
		<display:table class="table table-hover table-striped" id="fechaGrid" name="entities" pagesize="10" requestURI="Fecha_list.action" partialList="true" size="rowCount">
	        <s:set name="myrow" value="#attr.fechaGrid"/>
	        <display:column property="id" title="Id" />
			<display:column property="nombre" title="Nombre" />
		 	
		 	<display:column title="Activo" >
				<s:if test="#myrow.activo">
					<i class="glyphicon glyphicon-ok"></i>
				</s:if>  
			</display:column> 
	        <display:column title="Click para editar" href="Fecha_edit.action" paramId="requestId" paramProperty="id">
	            <i class="glyphicon glyphicon-pencil"></i>
	        </display:column>
<%-- 	        <display:column title="Click para eliminar" href="Fecha_destroy.action" paramId="requestId" paramProperty="id"> --%>
<%-- 	         	<s:if test="#myrow.activo"> --%>
<!-- 	            	<img src="imagenes/borrar.gif" alt="si"/> -->
<%-- 	            </s:if>  --%>
<%-- 	        </display:column> --%>
		</display:table>
		
		<s:url action="Torneo_list" var="urlTorneoList"/>
		<button type="button" onclick="location.href='<s:property value="%{urlTorneoList}" />'" class="btn btn-default">Volver</button>
	</s:if>
	<s:else>
			<div>
            	<s:iterator value="entities" var="fecha">
              	<div class="panel panel-info">
			  		<div class="panel-heading">
			    		<h3 class="panel-title"><s:property value="#fecha.numero" /> - <s:property value="#fecha.nombre" /></h3>
			  		</div>
			  		<div class="panel-body">
                  		<div align="center">
                       	<ul class="list-group">
					    <s:iterator value="#fecha.partidos" var="partido">
					  			<li class="list-group-item">
					  				<a onblur=""><s:property value="#partido.equipoLocal" /> <span class="badge"><s:property value="#partido.golesLocal" /></span> vs <span class="badge"><s:property value="#partido.golesVisitante" /></span> <s:property value="#partido.equipoVisitante" /></a>
					  				<span class="label label-default"><s:date name="#partido.fechaPartido" format="dd/MM HH:mm" /></span>
					  				<span class="label label-default"><s:property value="#partido.descripcion" /></span>
					  			</li>
                        </s:iterator>
					</ul>
                       </div>
			  </div>
			  <s:if test="#fecha.equipoLibre != null">
                       <div align="center" class="panel-footer">
							Equipo Libre: <s:property value="#fecha.equipoLibre" />
					</div>
			   </s:if>
			</div>
                 </s:iterator>
             </div>
        <button type="button" onclick="history.back()" class="btn btn-default">Volver</button>
	</s:else>

</div>