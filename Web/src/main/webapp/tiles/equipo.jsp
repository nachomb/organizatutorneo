<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<style type="text/css">


#lista1 {
	width:275px;
	border:1px solid #0C95C9;
	height:500px;
	float:left;
	margin-right:5px;
	min-height:200px;
	overflow:auto;
}
#lista2 {
	width:275px;
	border:1px solid #0C95C9;
	height:500px;
	float:left;
	margin-right:5px;
	min-height:175px;
	overflow:auto;
}
#filter {
	width:275px;
	margin-bottom:5px;
}
#labelJugadores {
float:right;
}
li.ui-draggable{
	cursor:move;
}


</style>

<script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-personalized-1.6rc6.min.js"></script>
<script type="text/javascript" >
function doSubmit(){
	setJugadoresSel();
	document.form.submit();
	return true;
}
function setJugadoresSel() {
	
	var jugadoresSeleccionados = document.getElementById("jugadoresSel");
	jugadoresSeleccionados.value = '';
	var ul = document.getElementById("lista1");
	var elems = ul.getElementsByTagName("input");
	for (var i = 0; i < elems.length; ++i) {

		var id = elems[i].value;
		var idConComa = id + ((i==elems.length - 1)?'':',');
		jugadoresSeleccionados.value += idConComa;
	}

 }
 /*JQuery drag and drop*/
$(document).ready(function(){
	var $lista1 = $('#lista1'), $lista2 = $('#lista2');
	// lista 1
	$('li',$lista1).draggable({
		revert: 'invalid',		
		helper: 'clone',		
		cursor: 'move'
	});
	$lista1.droppable({
		accept: '#lista2 li',
		drop: function(ev, ui) {
			deleteLista2(ui.draggable);
		}
	});
	/* lista2 */
	$('li',$lista2).draggable({
		revert: 'invalid',
		helper: 'clone',	
		cursor: 'move'
	});
	$lista2.droppable({
		accept: '#lista1 > li',
		drop: function(ev, ui) {
			deleteLista1(ui.draggable);		
		}
	});
	// listas	
	function deleteLista1($item) {
		$item.fadeOut(function() {
			$($item).appendTo($lista2).fadeIn();;
		});
		$item.fadeIn();
	}
	function deleteLista2($item) {
		$item.fadeOut(function() {			
			$item.appendTo($lista1).fadeIn();
		});
	}
	
	
	//default each row to visible
	$('ul#lista2 li').addClass('visible');
	
	
	$('#filter').keyup(function(event) {
		//if esc is pressed or nothing is entered
    if (event.keyCode == 27 || $(this).val() == '') {
			//if esc is pressed we want to clear the value of search box
			$(this).val('');
			
			//we want each row to be visible because if nothing
			//is entered then all rows are matched.
      $('ul#lista2 li').removeClass('visible').show().addClass('visible');
    }

		//if there is text, lets filter
		else {
      filter('ul#lista2 li', $(this).val());
    }
	});
	
});
 
 
//filter results based on query
function filter(selector, query) {
  query	=	$.trim(query); //trim white space
  query = query.replace(/ /gi, '|'); //add OR for regex query

  $(selector).each(function() {
    ($(this).text().search(new RegExp(query, "i")) < 0) ? $(this).hide().removeClass('visible') : $(this).show().addClass('visible');
  });
}
    
 
</script>

<div class="container">
<s:if test="id == null">
	<div align="center"><br>
	<h2>Equipo Nuevo</h2>
	<br>
	</div>
</s:if>
<s:else>
	<div align="center"><br>
	<h2>Editar Equipo</h2>
	</div>
</s:else>

<s:actionerror/>
<s:actionmessage/>
<s:fielderror/>

<s:form key="form" action="%{actionMethod}" method="POST" enctype="multipart/form-data" theme="simple" cssClass="form-horizontal">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:hidden key="jugadoresSeleccionados" id="jugadoresSel"/>
	<s:actionerror/>
	<br>
	<br>
		
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="inputNombre">Nombre</label>
		   <div class="col-lg-4">
		      		<s:textfield key="nombre" disabled="%{readOnly}"
					label="Nombre" maxlength="30" required="true" cssClass="form-control"/>
		   </div>
		 </div>
		
<%-- 		<s:if test="rutaImgEscudo != null"> --%>
<!-- 			<tr> -->
<!-- 				<td class="tdLabel"><label class="label">Escudo Actual:</label> -->
<!-- 				</td> -->
<%-- 				<td><img src="<s:url value="%{rutaImgEscudo}" />" id="escudo"/> --%>
<!-- 				</td> -->
				
<!-- 			</tr> -->
<!-- 			<tr> -->
<%-- 				<td><s:file name="upload" label="Cambiar Escudo" value="%{rutaImgEscudo}"/> --%>
<!-- 				</td> -->
				
<!-- 			</tr> -->
<%-- 		</s:if> --%>
<%-- 		<s:else> --%>
<!-- 			<tr> -->
<%-- 				<td><s:file name="upload" label="Escudo"/> --%>
<!-- 				</td> -->
				
<!-- 			</tr> -->
<%-- 		</s:else> --%>
		

		 <div class="row">
		  <div class="col-xs-2">
		    <label for="jugadoresPosibles" id="labelJugadores">Selecciona los Jugadores</label>
		  </div>
		  <div class="col-xs-3">
		 			<label for="jugadoresSeleccionados">Jugadores Seleccionados</label>
					<ul id="lista1">
					<s:iterator value="jugadoresEquipo" var="j">
		  				<li><s:property value="%{apellido}"/>, <s:property value="%{nombre}"/>
		  				<s:hidden name="hidden" value="%{id}"/>
		  				</li>
					</s:iterator>
				</ul>
		  </div>
		  <div class="col-xs-3">
		        <label for="filter">Filtrar</label> <input type="text" name="filter" value="" id="filter" class="form-control input-sm" />
				<ul id="lista2">
				<s:iterator value="jugadoresPosibles" var="j">
		 				<li><s:property value="%{apellido}"/>, <s:property value="%{nombre}"/> 
		 				<s:hidden name="hidden" value="%{id}"/>
		 				</li>
				</s:iterator>
				</ul>
		  </div>
		</div>
		
		<div class="form-group">
		   <div class="col-lg-offset-2 col-lg-10">
				<button type="button"
					onclick="return doSubmit()" class="btn btn-success">Guardar</button>
				<s:url action="Equipo_list" var="urlEquipoList" />	
				<button type="button"
					onclick="window.location.href='<s:property value="urlEquipoList"/>'" class="btn btn-danger">Cancelar</button>
		   </div>
	    </div>
</s:form>
</div>