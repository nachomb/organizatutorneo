<%@ taglib prefix="s" uri="/struts-tags"%>

<link rel="stylesheet" media="all" type="text/css"
	href="css/jquery-ui-1.8.6.custom.css" />
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript" language="JavaScript">
function doSubmit(){
	document.form.submit();
	return true;
}
</script>
	<div align="center"><br>
	<h2>Importar Jugadores y Equipos</h2>
	<br>
	</div>
<s:form key="form" action="%{actionMethod}" method="POST" enctype="multipart/form-data">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror/>
	<br>
	<br>
	<table align="center" width="100%">
	
		<tr>
			<td>
				<s:file name="excel" label="Excel"/>
			</td>
		</tr>
		
		<tr>
			<s:if test="%{habilitarOKenabled}">
				<td><input class="button" type="button" onclick="return doSubmit()" value="OK" style="float:right;" /></td>
			</s:if>
			<td><input  class="button" type="button" onclick="location.href='Jugador_list.action'" value="Cancelar" /></td>
		</tr>
	</table>
</s:form>
