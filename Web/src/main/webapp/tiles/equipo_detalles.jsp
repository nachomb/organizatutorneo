<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<style type="text/css">


#lista1, #lista2 {
	width:250px;
	border:1px solid #0C95C9;
	height:500px;
	float:left;
	margin-right:5px;
	min-height:200px;
	overflow:auto;
}


</style>

<s:form key="form" action="%{actionMethod}" method="POST" enctype="multipart/form-data">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:hidden key="jugadoresSeleccionados" id="jugadoresSel"/>
	<s:actionerror/>
	<br>
	<br>
	<table align="center" width="100%">
		<s:if test="id != null">
			<tr>
				<td>
					<s:label id="id" name="id" key="id" label="Id"/>
				</td>
			</tr>
		</s:if>
		<tr>
			<td><s:textfield key="nombre" disabled="%{readOnly}"
				label="Nombre" maxlength="30" /></td>
		</tr>
		<s:if test="rutaImgEscudo != null">
			<tr>
				<td class="tdLabel"><label class="label">Escudo:</label>
				</td>
				<td><img src="<s:url value="%{rutaImgEscudo}" />" id="escudo"/>
				</td>
				
			</tr>
		</s:if>
		
		<tr>
			<td>
			<s:checkbox key="activo" disabled="%{readOnly}" label="Activo" disabled="%{readOnly}" />
			</td>
		</tr>

 		<tr class="panel">
			<td>
				<h3>Jugadores</h3>
				<ul id="lista1">
					<s:iterator value="jugadoresEquipo" var="j">
		  				<li><s:property value="%{apellido}"/>, <s:property value="%{nombre}"/> <img id="detalles" src="imagenes/detalles.gif" alt="si" onclick="location.href='DetalleJugador.action?requestId=<s:property value="%{id}"/>'"/>
		  				</li>
					</s:iterator>
				</ul>
			</td>
		</tr>
		
		<tr>
			<td><input  class="button" type="button" onclick="history.back()" value="Volver" /></td>
		</tr>
	</table>
</s:form>
