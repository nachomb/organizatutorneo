<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" language="JavaScript">
	function doSubmit() {
		document.form.submit();
		return true;
	}
</script>


<div align="center"><br>
<h2>Reporte - <s:property value="%{nombre}" /></h2>
<br>
</div>
<tbody>
<s:form key="form" action="%{actionMethod}" method="POST">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
	<br>
	<br>
	<h3>Equipos con mejor delantera	</h3>
	<display:table id="equipoGrid" name="reporte.equiposMasGolesFavor" requestURI="Equipo_list.action" excludedParams="*">
       <s:set name="myrow" value="#attr.equipoGrid"/>
           <display:column property="nombre" title="Nombre" />
           <display:column property="datoReporte.golesAFavor" title="Cantidad de Goles" class="ancho-20"/>
       </display:table>
       <br>
     <h3>Equipos con peor delantera		</h3>
	<display:table id="equipoGrid" name="reporte.equiposMenosGolesFavor" requestURI="Equipo_list.action" excludedParams="*">
       <s:set name="myrow" value="#attr.equipoGrid"/>
           <display:column property="nombre" title="Nombre" />
           <display:column property="datoReporte.golesAFavor" title="Cantidad de Goles" class="ancho-20"/>
     </display:table>  
     <br>
    <h3> Equipos con mejor defensa</h3>
     <display:table id="equipoGrid" name="reporte.equiposMenosGolesEnContra" requestURI="Equipo_list.action" excludedParams="*">
     <s:set name="myrow" value="#attr.equipoGrid"/>
         <display:column property="nombre" title="Nombre" />
         <display:column property="datoReporte.golesEnContra" title="Cantidad de Goles" class="ancho-20"/>
     </display:table>
     <br>
     <h3>Equipos con peor defensa</h3>
     <display:table id="equipoGrid" name="reporte.equiposMasGolesEnContra" requestURI="Equipo_list.action" excludedParams="*">
     <s:set name="myrow" value="#attr.equipoGrid"/>
         <display:column property="nombre" title="Nombre" />
         <display:column property="datoReporte.golesEnContra" title="Cantidad de Goles" class="ancho-20"/>
     </display:table>
	<br>
	 <input class="button" type="button" onclick="history.back()" value="Volver" />

</s:form>
</tbody>
