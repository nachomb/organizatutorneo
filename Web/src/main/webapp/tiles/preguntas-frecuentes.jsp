		<div id="contenidoSeccion">
        
        <h1>Preguntas Frecuentes</h1>
			<h2>Todas las preguntas que necesit&aacute;s sobre Liga Futboleras.			  </h2>
            <div id="separacionTitulos"></div>
			
			<div class="textoGeneral">
			  <h3><strong>&iquest;Qu&eacute; tengo que hacer para anotar a un equipo&#63;</strong></h3>
			  <p>Ten&eacute;s que enviar el &quot;formulario de inscripci&oacute;n&quot; completo con todos los datos de las jugadoras de tu equipo a ligafutboleras@gmail.com</p>
			  <p> Hace Click <a href="Inscripcion.action"> AQUI </a> para Inscribirte.<br />
			    <br />
			  </p>
<p><strong>&iquest;Cu&aacute;nto sale participar del torneo&#63;</strong></p>
			  <p>Para consultar sobre el costo del torneo escribinos a ligafutboleras@gmail.com<br />
			    <br />
			  </p>
<p><strong>&iquest;Cu&aacute;nto dura el torneo&#63;</strong></p>
			  <p>El torneo dura 14 fechas, esto es cuatro meses aproximadamente.<br />
			    <br />
			  </p>
<p><strong>&iquest;Para qu&eacute; sirven los amistosos&#63;</strong></p>
			  <p>Los amistosos sirven para conocer el nivel de juego de los equipos y tambi&eacute;n para ir asimilando el reglamento. Ayuda a los equipos a practicar, organizarse y prepararse de la mejor manera posible para el inicio del torneo.</p>
			  <p><br />
              </p>
			  <p><strong>&iquest;C&oacute;mo hago si no tengo equipo, puedo anotarme igual&#63;</strong></p>
			  <p>Si, pod&eacute;s anotarte igual porque FUTBOLERAS pone a disposici&oacute;n una Bolsa de Jugadoras para ayudar a formar equipos nuevos y/o completar equipos existentes.</p>
			  <p><br />
              </p>
			  <p><strong>&iquest;Qu&eacute; pasa si somos 4 o 5 y no llegamos a formar equipo&#63;</strong></p>
			  <p>Pod&eacute;s anotarte igual y buscar en la BOLSA DE JUGADORAS las integrantes que faltan para completar tu equipo.</p>
			  <p><br />
              </p>
			  <p><strong>&iquest;Cu&aacute;ntas jugadoras puedo anotar en el formulario de inscripci&oacute;n&#63;</strong></p>
			  <p>Pod&eacute;s anotar hasta 15 jugadoras. Ninguna jugadora puede estar inscripta en 2 equipos durante el mismo torneo.</p>
			  <p><br />
              </p>
			  <p><strong> &iquest;Qu&eacute; pasa si una fecha no podemos jugar&#63;</strong></p>
			  <p>Los partidos no se reprograman, si tu equipo no se puede presentar al partido pierde los puntos por 5 a 0.</p>
			  <p><br />
              </p>
			  <p><strong>Y &iquest;si el equipo contrario no juega&#63;</strong></p>
			  <p>Si el equipo contrario no se presenta y se suspende tu partido, tu equipo gana los puntos por 5 a 0. M&aacute;s informaci&oacute;n en Reglamento.</p>
			  <p><br />
              </p>
			  <p> <strong>&iquest;Podemos pedir un horario especial de juego&#63;</strong></p>
			  <p>Los horarios de partidos se publican en Fixture. Una vez publicado no se pueden modificar. M&aacute;s Informaci&oacute;n en el REGLAMENTO DEL TORNEO</p>
			  <p>Los horarios ser&aacute;n rotativos, para que todos los equipos est&eacute;n en igualdad de condiciones.</p>
			  <p><br />
              </p>
			  <p> <strong>&iquest;Con qui&eacute;n tengo que comunicarme para hacer un pedido de horario y reprogramaci&oacute;n&#63;</strong></p>
			  <p>Toda consulta o pedido debe realizarse a trav&eacute;s del correo oficial: ligafutboleras@gmail.com </p>
			  <p><br />
              </p>
			  <p><strong>&iquest;C&oacute;mo me entero a qu&eacute; hora jugamos&#63;</strong></p>
			  <p>Los mi&eacute;rcoles vas a encontrar los horarios en nuestro muro de Facebook y son enviados junto con el resumen de la fecha por mail a cada Delegada de cada equipo.</p>
			  <p><br />
              </p>
			  <p><strong>&iquest;Qu&eacute; d&iacute;a se juega s&aacute;bado o domingo&#63;</strong></p>
			  <p>Se juega los domingos por la tarde.</p>
			  <p><br />
              </p>
			  <p><strong>&iquest;Qu&eacute; indumentaria debo usar&#63;</strong></p>
			  <p>Las jugadoras de cada equipo tienen que presentarse con un juego de remeras absolutamente id&eacute;nticas y con numeraci&oacute;n distinta en todas sus jugadoras. Para aquellos equipos que no cuenten con las camisetas FUTBOLERAS cuenta con camisetas numeradas que pueden ser usadas por el equipo.</p>
			  <p>Se recomienda el uso de canilleras. Por seguridad no est&aacute; permitida la utilizaci&oacute;n de relojes y/o accesorios.</p>
			  <p><br />
              </p>
			  <p><strong>&iquest;Cu&aacute;nto duran los partidos&#63;</strong></p>
			  <p>Los partidos duran 40 minutos, divididos en dos tiempos de 20. El entretiempo dura 2 minutos.</p>
			  <p><br />
              </p>
			  <p><strong> &iquest;Cu&aacute;l es la funci&oacute;n de la Delegada&#63;</strong></p>
			  <p>La Delegada es el medio de contacto oficial entre la Organizaci&oacute;n y el equipo. Es la encargada de comunicar cuando se suspende una fecha, de presentar toda la informaci&oacute;n requerida para la inscripci&oacute;n, abonar en tiempo y forma, y a la vez es la responsable de que las integrantes del equipo conozcan el reglamento.</p>
			  <p><br />
              </p>
			  <p><strong>&iquest;C&oacute;mo es el sistema de juego&#63;</strong></p>
			  <p>Para conocer el sistema de juego ingresa <a href="FormatoTorneo.action">AQUI</a></p>
			  <p><br />
              </p>
			  <p><strong>&iquest;C&oacute;mo funciona el &quot;formulario de inscripci&oacute;n&quot;&#63;</strong></p>
			  <p> El equipo presentar&aacute; antes del inicio del campeonato una Lista de Buena Fe, con opci&oacute;n de agregarle un m&aacute;ximo de 2 (dos) jugadoras por fecha hasta la Fecha Nro. 6 inclusive. </p>
			  <p> Las jugadoras que est&eacute;n en la lista de Buena Fe m&aacute;s las agregadas durante el torneo, no pueden superar la cantidad de 15. </p>
			  <p> Una misma jugadora no puede estar en dos listas de Buena Fe. </p>
			  <p> En el caso que un equipo incluya una jugadora incorrectamente por cualquier motivo que se menciona con anterioridad autom&aacute;ticamente se sanciona a ese equipo con la p&eacute;rdida de puntos. <br />
			    <br />
			    <br />
              </p>
			</div>
              
             </div>