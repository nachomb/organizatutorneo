<%@ taglib prefix="s" uri="/struts-tags"%>

<div id="fb-root"></div>
<script src="http://connect.facebook.net/es_LA/all.js"></script>
<script>
  FB.init({
    appId  : '182811701772156',
    status : true, // check login status
    cookie : true, // enable cookies to allow the server to access the session
    xfbml  : true  // parse XFBML
  });
</script>
	
<div align="center"><br>
<h2>Ficha del Partido</h2>
</div>
<s:form id="demo" key="form" action="%{actionMethod}" method="POST">
	<br>
	<table align="center" width="100%">
		<tr>
			<td><s:textfield label="Fecha y Hora" type="text"
				name="fechaYHora" id="fechaYHora" disabled="%{readOnly}"/></td>
		</tr>
		<tr>
			<td><s:textfield key="lugar" disabled="%{readOnly}"
				label="Lugar" maxlength="30" /></td>
		</tr>
	</table>
	<h3>Comentarios</h3>
	<s:url action="FichaPartidoAmistoso.action" var="urlTag" includeParams="all"/>
	<fb:comments href="<s:property value="#urlTag" />" num_posts="5" width="500"></fb:comments>
</s:form>