<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<script type="text/javascript" src="js/efecto-hover-listados.js"></script>

<div align="center"><br>
<h2>Grupos</h2>
<br>
</div>
<tbody>
<s:form method="POST">
    <br>
    <s:if test="%{!readOnly}">
        <table align="center" width="100%">
            <tr>
                <td>
                <div align="center"><s:a href="Grupo_add.action">Nuevo</s:a></div>
                </td>
            </tr>
        </table>
    </s:if>
    <s:if test="%{!readOnly}">
        <display:table id="grupoGrid" name="entities" pagesize="10" requestURI="Grupo_list.action" partialList="true"
            size="rowCount" excludedParams="*">
            <s:set name="myrow" value="#attr.grupoGrid" />
            <display:column property="id" title="Id" />
            <display:column property="nombre" title="Nombre" />
            <display:column title="Click para editar" href="Grupo_edit.action" paramId="requestId" paramProperty="id">
		            <img src="imagenes/editar.gif" alt="si"/>
		    </display:column>
            <display:column title="Click para eliminar" href="Grupo_destroy.action" paramId="requestId"
                paramProperty="id">

		        <img src="imagenes/borrar.gif" alt="si"/>

            </display:column>
        </display:table>
    </s:if>
    <s:else>
        <display:table id="grupoGrid" name="entities" pagesize="10" requestURI="ConsultaGrupo.action"
            partialList="true" size="rowCount" excludedParams="*">
            <s:set name="myrow" value="#attr.grupoGrid" />
            <display:column property="id" title="Id" />
            <display:column property="nombre" title="Nombre" />
        </display:table>
    </s:else>
</s:form>
</tbody>