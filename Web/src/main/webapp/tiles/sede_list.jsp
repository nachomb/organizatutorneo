<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<div class="container">
		<h2>Sedes</h2>
		<div align="center">
			<s:a href="Sede_add.action" cssClass="btn btn-success">Nuevo</s:a>
		</div>
		<br>
	<br>
	<s:if test="%{!readOnly}">
		<display:table class="table table-hover table-striped" id="sedeGrid" name="entities" pagesize="10" requestURI="Sede_list.action" partialList="true" size="rowCount" excludedParams="*">
	        <s:set name="myrow" value="#attr.sedeGrid"/>
	        <display:column property="id" title="Id" />
			<display:column property="nombre" title="Nombre" />
	        <display:column title="Click para editar" href="Sede_edit.action" paramId="requestId" paramProperty="id">
	             <i class="glyphicon glyphicon-pencil"></i>
	        </display:column>
<%-- 	        <display:column title="Click para eliminar" href="Sede_destroy.action" paramId="requestId" paramProperty="id"> --%>
<!-- 	        	<img src="imagenes/borrar.gif" alt="si"/> -->
<%-- 	        </display:column> --%>
		</display:table>
	</s:if>
	<s:else>
	
	</s:else>
</div>