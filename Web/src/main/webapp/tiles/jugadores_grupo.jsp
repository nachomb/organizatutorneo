<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="jugadoresGrupo != null">
	<s:updownselect list="jugadoresGrupo" key="jugadoresInvitados"
		name="jugadoresInvitados" theme="simple" listKey="usuario" />
</s:if>