<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<script type="text/javascript" src="js/efecto-hover-listados.js"></script>


<div align="center"><br>
<h2>Partidos Amistosos</h2>
<br>
</div>
<tbody>
<s:form method="POST">
    <br>
    <s:if test="%{!readOnly}">
        <table align="center" width="100%">
            <tr>
                <td>
                <div align="center"><s:a href="MisPartidos_addAmistoso.action">Nuevo</s:a></div>
                </td>
            </tr>
        </table>
    </s:if>
    <s:if test="%{!readOnly}">
    <sx:tabbedpanel id="test">
    	<sx:div id="one" label="Pr�ximos" theme="ajax" labelposition="top" >
	        <display:table id="partidoAmistosoGrid" name="entities" pagesize="10" requestURI="MisPartidos_listAmistoso.action" partialList="true"
	            size="rowCount" excludedParams="*">
	            <s:set name="myrow" value="#attr.partidoAmistosoGrid" />
	            <display:column property="id" title="Id" />
	            <display:column property="lugar" title="Lugar" />
	            <display:column property="fecha" title="Fecha y Hora" format="{0,date,dd/MM/yyyy - hh:mm}" />
                
	            <display:column title="Click para editar" href="MisPartidos_editAmistoso.action" paramId="requestId" paramProperty="id">
			            <img src="imagenes/editar.gif" alt="si"/>
			    </display:column>
	            <display:column title="Click para eliminar" href="MisPartidos_destroyAmistoso.action" paramId="requestId"
	                paramProperty="id">
	
			        <img src="imagenes/borrar.gif" alt="si"/>
	
	            </display:column>
	        </display:table>
        </sx:div>
        <sx:div id="two" label="Jugados" theme="ajax">
		  	  <display:table id="partidoAmistosoGrid" name="partidosJugados" pagesize="10" requestURI="MisPartidos_listAmistoso.action" partialList="true"
	            size="partidosJugadosSize" excludedParams="*">
	            <s:set name="myrow" value="#attr.partidoAmistosoGrid" />
	            <display:column property="id" title="Id" />
	            <display:column property="lugar" title="Lugar" />
	            <display:column property="fecha" title="Fecha y Hora" format="{0,date,dd/MM/yyyy - hh:mm}" />
                <display:column title="Cargar Datos" href="MisPartidos_inputAmistoso.action" paramId="requestId" paramProperty="id">
                        <img src="imagenes/editar.gif" alt="si"/>
                </display:column>
	            <display:column title="Ficha del Partido" href="FichaPartidoAmistoso.action" paramId="requestId" paramProperty="id">
			            <img src="imagenes/detalles.gif" alt="si"/>
			    </display:column>
	        </display:table>
		 </sx:div>
    </sx:tabbedpanel>
    </s:if>
    <s:else>
        <display:table id="partidoAmistosoGrid" name="entities" pagesize="10" requestURI="ConsultaPartidoAmistoso.action"
            partialList="true" size="rowCount" excludedParams="*">
            <s:set name="myrow" value="#attr.partidoAmistosoGrid" />
            <display:column property="id" title="Id" />
        </display:table>
    </s:else>
</s:form>
</tbody>