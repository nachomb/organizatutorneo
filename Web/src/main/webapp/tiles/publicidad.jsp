<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" language="JavaScript">
function doSubmit(){
	document.form.submit();
	return true;
}
</script>
<s:if test="id == null">
	<div align="center"><br>
	<h2>Publicidad Nueva</h2>
	<br>
	</div>
</s:if>
<s:form key="form" action="%{actionMethod}" method="POST" enctype="multipart/form-data">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror/>
	<br>
	<br>
	<table align="center" width="100%">
		<s:if test="id != null">
			<tr>
				<td>
					<s:label id="id" name="id" key="id" label="Id"/>
				</td>
			</tr>
		</s:if>
		<tr>
			<td><s:textfield key="nombre" disabled="%{readOnly}"
				label="Nombre" maxlength="30" required="true"/></td>
		</tr>
		
		<s:if test="imagenVertical != null">
			<tr>
				<td class="tdLabel"><label class="label">Imagen Vertical Actual:</label>
				</td>
				<td><img src="<s:url value="%{imagenVertical}" />" id="imagenVertical"/>
				</td>
				
			</tr>
			<tr>
				<td><s:file name="uploadImgVertical" label="Cambiar Imagen Vertical"/>
				</td>
				
			</tr>
		</s:if>
		<s:else>
			<tr>
				<td><s:file name="uploadImgVertical" label="Imagen Vertical"/>
				</td>
				
			</tr>
		</s:else>
		<tr>
			<td>
			<s:checkbox key="activoVertical" disabled="%{readOnly}" label="Activo Vertical" />
			</td>
		</tr>
		
		
		<s:if test="imagenSlider != null">
			<tr>
				<td class="tdLabel"><label class="label">Imagen Slider Actual:</label>
				</td>
				<td><img src="<s:url value="%{imagenSlider}" />" id="imagenSlider"/>
				</td>
				
			</tr>
			<tr>
				<td><s:file name="uploadImgSlider" label="Cambiar Imagen Slider"/>
				</td>
				
			</tr>
		</s:if>
		<s:else>
			<tr>
				<td><s:file name="uploadImgSlider" label="Imagen Slider"/>
				</td>
				
			</tr>
		</s:else>

		<tr>
			<td>
			<s:checkbox key="activoSlider" disabled="%{readOnly}" label="Activo Slider" />
			</td>
		</tr>
		
		<tr>
			<td><s:textfield key="link" disabled="%{readOnly}"
				label="Link" maxlength="70"/></td>
		</tr>
		
		<tr>
			<s:if test="%{habilitarOKenabled}">
				<td><input class="button" type="button" onclick="return doSubmit()" value="OK" style="float:right;" /></td>
			</s:if>
			<td><input  class="button" type="button" onclick="location.href='Publicidad_list.action'" value="Cancelar" /></td>
		</tr>
	</table>
</s:form>
