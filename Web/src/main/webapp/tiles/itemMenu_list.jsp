<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<div align="center">
	<br>
		<h2>Edici�n de Men�</h2>
	<br>
</div>
<s:form method="POST">
	<br>
	<table align="center" width="100%">
		<tr>
			<td>
				<div align="center">
					<s:a href="ItemMenu_add.action">
						Nuevo
					</s:a>
				</div>
			</td>
		</tr>
	</table>
	<display:table id="menuGrid" name="entities" pagesize="25" requestURI="ItemMenu_list.action" partialList="true" size="rowCount" excludedParams="*">
        <display:column property="id" title="Id" />
		<display:column property="titulo" title="Titulo" />
		<display:column property="descripcion" title="Descripci�n" />
		<display:column property="orden" title="Orden" />
		<display:column property="action.name" title="Acci�n" />
		<display:column property="itemMenuPadre.titulo" title="Men� Padre" />
	    <s:if test="%{!readOnly}">
	        <display:column title="Click para eliminar" href="ItemMenu_destroy.action" paramId="requestId" paramProperty="id">
	            Eliminar
	        </display:column>
	        <display:column title="Click para editar" href="ItemMenu_edit.action" paramId="requestId" paramProperty="id">
	            Editar
	        </display:column>
	    </s:if>
	</display:table>
</s:form>