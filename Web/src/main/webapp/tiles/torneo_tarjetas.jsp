<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" language="JavaScript">
	function doSubmit() {
		document.form.submit();
		return true;
	}
</script>



<div class="container">
<h2>Tarjetas - <s:property value="%{nombre}" /></h2>
<br>

	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
		<s:url id="urlTablaPos" action="Torneo_estadisticas"
			includeParams="all"></s:url>
		<s:url id="urlTablaGol" action="Torneo_tablaGoleadores"
			includeParams="all"></s:url>
		<s:url id="urlTablaTarjetas" action="Torneo_tablaTarjetas"
			includeParams="all"></s:url>
		<s:url id="urlTablaSanciones" action="Torneo_tablaSanciones"
			includeParams="all"></s:url>
		<div align="center">
			<s:if test="%{tipoTorneo == 'Todos contra Todos'}">
				<button type="button" onclick="location.href='<s:property value="%{urlTablaPos}" />'" class="btn btn-default">Posiciones</button>
			</s:if>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaGol}" />'" class="btn btn-default">Goleadores</button>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaTarjetas}" />'" class="btn active">Tarjetas</button>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaSanciones}" />'" class="btn btn-default">Sancionados</button>
		</div>
		<br>
		<br>

		<display:table class="table table-hover table-striped" id="tablaTarjetasGrid" name="jugadoresTarjetas"
				sort="list" requestURI="Torneo_tablaTarjetas.action"
				defaultsort="10" defaultorder="descending">
				<s:set name="myrow" value="#attr.tablaTarjetasGrid"/>
				<display:column title="N"><%=pageContext.getAttribute("tablaTarjetasGrid_rowNum")%></display:column>
				<display:column property="jugador" title="Jugador"	defaultorder="ascending" sortable="true"/>
				<display:column title="Equipo" property="jugador.equipo.nombre" sortable="true">
				</display:column>
				<display:column title="Cant. Amarillas" property="cantTarjetasAmarillas"
					sortable="true">
				</display:column>
				<display:column title="Cant. Rojas" property="cantTarjetasRojas"
					sortable="true">
				</display:column>
				<display:column title="Total" property="cantTarjetasTotal"
					sortable="true">
				</display:column>
			</display:table>

		<s:url action="Torneo_list" var="urlTorneoList"/>
		<button type="button" onclick="location.href='<s:property value="%{urlTorneoList}" />'" class="btn btn-default">Volver</button>
</div>