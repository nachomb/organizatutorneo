<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript">
$(function() {
	$('#myModal').on('show.bs.modal', function (e) {
		alert('hola');
	});
});
function showWindow(link){
	window.prompt("Comparti el siguiente link", window.location.origin + link);
}
</script>

<div class="container">
	<h2>Torneos</h2>
	
	<s:if test="%{!readOnly}">
		<div align="center">
			<s:a cssClass="btn btn-success" href="Torneo_add.action">Nuevo</s:a>
           	 <s:url action="ConsultaTorneo" var="urlConsultaTorneo"  >
            	<s:param name="uid" value="%{uid}" />
            </s:url>
			<button type="button" onclick="showWindow('<s:property value="%{urlConsultaTorneo}" />')" class="btn btn-info">Tu Web</button>
		</div>
		<br>
		<div>
		<s:iterator value="actionMessages" var="message">
			<s:property value="#message"/>
			<br>
		</s:iterator>
    	<display:table class="table table-hover table-striped" id="torneoGrid" name="entities" pagesize="10" requestURI="Torneo_list.action" partialList="true" size="rowCount" excludedParams="*">
	        <s:set name="myrow" value="#attr.torneoGrid"/>
	        <display:column property="id" title="Id" />
			<display:column property="nombre" title="Nombre" />
			<display:column title="Activo" >
				<s:if test="#myrow.activo">
					<i class="glyphicon glyphicon-ok"></i>
				</s:if>
			</display:column>
            
            <display:column title="Iniciar" href="Torneo_iniciar.action" paramId="requestId" paramProperty="id">
                <s:if test="!#myrow.estaIniciado()">
                    Generar Fixture
                </s:if>
            </display:column>
            
            <display:column title="Fixture" href="Torneo_fixture.action" paramId="requestId" paramProperty="id">
                <s:if test="#myrow.estaIniciado()">
                    Fixture
                </s:if>
            </display:column>
            
            <display:column title="Cargar Resultados" href="Fecha_list.action" paramId="torneoId" paramProperty="id">
                <s:if test="#myrow.estaIniciado()">
                    Cargar
                </s:if>
            </display:column>
            
            <display:column title="Cargar Sancion" href="Sancion_add.action" paramId="torneoId" paramProperty="id">
                <s:if test="#myrow.estaIniciado()">
                    Cargar
                </s:if>
            </display:column>          
            
            <display:column title="Estadisticas" href="Torneo_estadisticas.action" paramId="requestId" paramProperty="id">
                <s:if test="#myrow.estaIniciado()">
                    Estad&iacute;sticas
                </s:if>
            </display:column>
            
            <display:column title="Actualizar" href="Torneo_actualizar.action" paramId="requestId" paramProperty="id">
                <s:if test="#myrow.estaIniciado()">
                    Actualizar
                </s:if>
            </display:column>
            
<%-- 	        <display:column title="Click para editar" href="Torneo_edit.action" paramId="requestId" paramProperty="id"> --%>
<%-- 	            <s:if test="#myrow.fechas.isEmpty()"> --%>
<!-- 	            	<a href="#myModal" role="button" data-toggle="modal"><i class="glyphicon glyphicon-pencil"></i></a> -->
<%-- 	            </s:if> --%>
<%-- 	        </display:column> --%>
<%-- 	        <display:column title="Click para eliminar" href="Torneo_destroy.action" paramId="requestId" paramProperty="id"> --%>
<%-- 	         	<s:if test="#myrow.activo"> --%>
<!-- 	            	<a href="#myModal" role="button" data-toggle="modal"><i class="glyphicon glyphicon-remove"></i></a> -->
<%-- 	            </s:if>  --%>
<%-- 	        </display:column> --%>
		</display:table>
		</div>
		</s:if>
		<s:else>
		<!-- readOnly -->
		<display:table class="table table-hover table-striped" id="torneoGrid" name="entities" pagesize="10" requestURI="ConsultaTorneo.action" partialList="true" size="rowCount" >
            <s:set var="myrow" value="#attr.torneoGrid"/>
            <display:column property="nombre" title="Nombre" href="DetalleTorneo.action" paramId="requestId" paramProperty="id"/>
            <display:column property="sede.nombre" title="Sede" /> 
            <display:column title="Estado">
                <s:if test="#myrow.estaIniciado()">
                    Iniciado
                </s:if>
                <s:else>
                	En contrucci&iocute;n
                </s:else>
            </display:column>
<%--             <display:column title="Estadisticas" href="Torneo_estadisticas.action" paramId="requestId" paramProperty="id"> --%>
<%--                 <s:if test="#myrow.estaIniciado()"> --%>
<!--                     Estad&iacute;sticas -->
<%--                 </s:if> --%>
<%--             </display:column> --%>
<%--             <display:column title="Detalles" href="DetalleTorneo.action" paramId="requestId" paramProperty="id"> --%>
<!--                 <img src="imagenes/detalles.gif" alt="si"/> -->
<%--              </display:column> --%>
        </display:table>
	</s:else>
	
	<!-- Modal -->
<!--   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> -->
<!--     <div class="modal-dialog"> -->
<!--       <div class="modal-content"> -->
<!--         <div class="modal-header"> -->
<!--           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
<!--           <h4 class="modal-title">Desea eliminar el torneo?</h4> -->
<!--         </div> -->
<!--         <div class="modal-body"> -->
<!--         	   hola -->
<!--         </div> -->
<!--         <div class="modal-footer"> -->
<!--           <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button> -->
<!--           <button type="button" class="btn btn-danger">Borrar</button> -->
<!--         </div> -->
<!--       </div> -->
<!--     </div> -->
<!--   </div> -->
		
<!-- <tbody> -->
<%-- <s:form method="POST"> --%>
<!-- 	<br> -->
<%-- 	<s:if test="%{!readOnly}"> --%>
<!-- 		<table align="center" width="100%"> -->
<!-- 			<tr> -->
<!-- 				<td> -->
<!-- 					<div align="center"> -->
<%-- 						<s:a href="Torneo_add.action">Nuevo</s:a> --%>
<!-- 					</div> -->
<!-- 				</td> -->
<!-- 			</tr> -->
<!-- 		</table> -->
<%-- 		<s:iterator value="actionMessages" var="message"> --%>
<%-- 			<s:property value="#message"/> --%>
<!-- 			<br> -->
<%-- 		</s:iterator> --%>
<%-- 		<display:table id="torneoGrid" name="entities" pagesize="10" requestURI="Torneo_list.action" partialList="true" size="rowCount" excludedParams="*"> --%>
<%-- 	        <s:set name="myrow" value="#attr.torneoGrid"/> --%>
<%-- 	        <display:column property="id" title="Id" /> --%>
<%-- 			<display:column property="nombre" title="Nombre" /> --%>
<%-- 			<display:column title="Activo" > --%>
<%-- 				<s:if test="#myrow.activo"> --%>
<!-- 					<img src="imagenes/check-ok-blue_15x15.png" alt="si"/> -->
<%-- 				</s:if> --%>
<%-- 			</display:column> --%>
            
<%--             <display:column title="Iniciar" href="Torneo_iniciar.action" paramId="requestId" paramProperty="id"> --%>
<%--                 <s:if test="!#myrow.estaIniciado()"> --%>
<!--                     Iniciar -->
<%--                 </s:if> --%>
<%--             </display:column> --%>
            
<%--             <display:column title="Fixture" href="Torneo_fixture.action" paramId="requestId" paramProperty="id"> --%>
<%--                 <s:if test="#myrow.estaIniciado()"> --%>
<!--                     Fixture -->
<%--                 </s:if> --%>
<%--             </display:column> --%>
            
<%--             <display:column title="Cargar Resultados" href="Fecha_list.action" paramId="torneoId" paramProperty="id"> --%>
<%--                 <s:if test="#myrow.estaIniciado()"> --%>
<!--                     Cargar -->
<%--                 </s:if> --%>
<%--             </display:column>          --%>
            
<%--             <display:column title="Estadisticas" href="Torneo_estadisticas.action" paramId="requestId" paramProperty="id"> --%>
<%--                 <s:if test="#myrow.estaIniciado()"> --%>
<!--                     Estadísticas -->
<%--                 </s:if> --%>
<%--             </display:column> --%>
            
<%--             <display:column title="Actualizar" href="Torneo_actualizar.action" paramId="requestId" paramProperty="id"> --%>
<%--                 <s:if test="#myrow.estaIniciado()"> --%>
<!--                     Actualizar -->
<%--                 </s:if> --%>
<%--             </display:column> --%>
             
<%-- 	        <display:column title="Click para editar" href="Torneo_edit.action" paramId="requestId" paramProperty="id"> --%>
<%-- 	            <s:if test="#myrow.fechas.isEmpty()"> --%>
<!-- 	            <img src="imagenes/editar.gif" alt="si"/> -->
<%-- 	            </s:if> --%>
<%-- 	        </display:column> --%>
<%-- 	        <display:column title="Click para eliminar" href="Torneo_destroy.action" paramId="requestId" paramProperty="id"> --%>
<%-- 	         	<s:if test="#myrow.activo"> --%>
<!-- 	            	<img src="imagenes/borrar.gif" alt="si"/> -->
<%-- 	            </s:if>  --%>
<%-- 	        </display:column> --%>
<%-- 		</display:table> --%>
<%-- 	</s:if> --%>
<%-- 	<s:else> --%>
<%-- 		<display:table id="torneoGrid" name="entities" pagesize="10" requestURI="ConsultaTorneo.action" partialList="true" size="rowCount" excludedParams="*"> --%>
<%--             <s:set name="myrow" value="#attr.torneoGrid"/> --%>
<%--             <display:column property="id" title="Id" /> --%>
<%--             <display:column property="nombre" title="Nombre" /> --%>
<%--             <display:column title="Fixture" href="ConsultaFecha.action" paramId="torneoId" paramProperty="id"> --%>
<%--                 <s:if test="#myrow.estaIniciado()"> --%>
<!--                     Ver fixture -->
<%--                 </s:if> --%>
<%--             </display:column> --%>
<%--             <display:column title="Estadisticas" href="Torneo_estadisticas.action" paramId="requestId" paramProperty="id"> --%>
<%--                 <s:if test="#myrow.estaIniciado()"> --%>
<!--                     Estadísticas -->
<%--                 </s:if> --%>
<%--             </display:column> --%>
<%--             <display:column title="Detalles" href="DetalleTorneo.action" paramId="requestId" paramProperty="id"> --%>
<!--                 <img src="imagenes/detalles.gif" alt="si"/> -->
<%--              </display:column> --%>
<%--         </display:table> --%>
<%-- 	</s:else> --%>
	
<!-- 	<tr> -->
<!-- 		<td><input class="button" type="button" onclick="location.href='Exporter_estadisticasHistoricas.action'" value="Exportar Estadisticas Historicas" style="float:right;" /></td> -->
<!-- 	</tr> -->
	
<!-- 	<tr> -->
<!-- 		<td><input class="button" type="button" onclick="location.href='Exporter_estadisticas.action'" value="Exportar Estadisticas" style="float:right;" /></td> -->
<!-- 	</tr> -->
	
<!-- 	<tr> -->
<!-- 		<td><input class="button" type="button" onclick="location.href='Exporter_planilla.action'" value="Exportar Planilla" style="float:right;" /></td> -->
<!-- 	</tr> -->
	
<%-- </s:form> --%>
<!-- </tbody> -->
</div>
