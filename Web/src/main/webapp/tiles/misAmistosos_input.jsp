<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<link rel="stylesheet" media="all" type="text/css"
	href="css/jquery-ui-1.8.6.custom.css" />
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/web/struts/optiontransferselect.js"></script>
<script type="text/javascript" language="JavaScript">

function doSubmit() {
    seleccionarTodos();
    document.form.submit();
    return true;
}

function seleccionarTodos() {
    $("#equipoA option").attr('selected','selected');
    $("#equipoB option").attr('selected','selected');
}


function agregarJugador(idJugadores, idSelected){
    $(idJugadores + ' :selected').each(function(i, selected){
        $(idSelected).append(new Option($(selected).text(), $(selected).val(), false, false));
    });
}

function removerJugador(idJugadorSeleccionados){
    $(idJugadorSeleccionados + ' :selected').remove();
}

function agregarEquipoA(){
    agregarJugador('#invitaciones','#equipoA');
    removerJugador('#invitaciones');
}

function agregarEquipoB(){
    agregarJugador('#invitaciones','#equipoB');
    removerJugador('#invitaciones');
}

function sacarEquipoA(){
    agregarJugador('#equipoA','#invitaciones');
    removerJugador('#equipoA');
}

function sacarEquipoB(){
    agregarJugador('#equipoB','#invitaciones');
    removerJugador('#equipoB');
}

</script>

<div align="center"><br>
    <h2>Cargar Datos de Partido</h2>
    <br>
</div>

<s:form id="demo" key="form" action="%{actionMethod}" method="POST">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
	<br>
	<table align="center" width="100%">
		<s:if test="id != null">
			<tr>
				<td><s:label id="id" name="id" key="id" label="Id" /></td>
			</tr>
		</s:if>
		<tr>
			<td class="tdLabel">
                <label class="label" for="fechaYHora"> Fecha y Hora: </label>
            </td>
            <td>
                <s:label name="fechaYHora" id="fechaYHora" theme="simple" disabled="%{readOnly}"/>
            </td>
		</tr>

        <tr>
            <td><s:label key="lugar" disabled="%{readOnly}" label="Lugar" /></td>
        </tr>
        <tr>
            <td><s:label key="cantParticipantes" disabled="%{readOnly}" label="Cantidad de participantes" /></td>
        </tr>
        <tr>
            <td><s:label cssClass="noMultiple" label="Modo" name="modo" key="modo" /></td>
        </tr>
        
        <tr>
            <td>
                <s:textfield key="golesEquipoA" disabled="%{readOnly}" label="Goles Equipo A" maxlength="3" size="3" />
            </td>
            <td>
                <s:textfield key="golesEquipoB" disabled="%{readOnly}" label="Goles Equipo B" maxlength="3" size="3"  />
            </td>
        </tr>
        
		<tr>
			<td class="tdLabel"><label class="label"> Jugadores:</label></td>
			<td>
                <s:select id="invitaciones" key="invitacionesSelect" list="participantes"  
                        listKey="id" theme="simple" multiple="true">
                </s:select>
			</td>
            <td>
                <input id="botonAgregarEquipoA" type="button" value="Agregar a Equipo A" onclick="agregarEquipoA();" style="display:block;"/>
                <input id="botonAgregarEquipoB" type="button" value="Agregar a Equipo B" onclick="agregarEquipoB();" style="display:block;"/>
            </td>
		</tr>
        
        <tr>
            <td class="tdLabel"><label class="label"> Equipo A:</label></td>
            <td>
                <s:if test="%{alineacionEquipoA != null}">
                <s:select id="equipoA" key="selectedJugadoresEquipoA" list="alineacionEquipoA.titulares" listKey="id" 
                disabled="%{readOnly}" multiple="true" theme="simple" >
                </s:select>
                </s:if>
                <s:else>
                <s:select id="equipoA" key="selectedJugadoresEquipoA" list="equipoANull" listKey="id" 
                disabled="%{readOnly}" multiple="true" theme="simple" >
                </s:select>
                </s:else>
            </td>
            <td>
                <input id="botonSacarEquipoA" type="button" value="Sacar de Equipo A" onclick="sacarEquipoA();" style="display:block;"/>
            </td>
        </tr>


        <tr>
            <td class="tdLabel"><label class="label"> Equipo B:</label></td>
            <td>
                <s:if test="%{alineacionEquipoB != null}">
                <s:select id="equipoB" key="selectedJugadoresEquipoB" list="alineacionEquipoB.titulares" listKey="id" 
                disabled="%{readOnly}" multiple="true" theme="simple" >
                </s:select>
                </s:if>
                <s:else>
                <s:select id="equipoB" key="selectedJugadoresEquipoB" list="equipoBNull" listKey="id" 
                disabled="%{readOnly}" multiple="true" theme="simple" >
                </s:select>
                </s:else>
            </td>
            <td>
                <input id="botonSacarEquipoB" type="button" value="Sacar de Equipo B" onclick="sacarEquipoB();" style="display:block;"/>
            </td>
        </tr>

		<tr>
			<s:if test="%{habilitarOKenabled}">
				<td>
                    <input class="button" type="button" onclick="return doSubmit()" value="OK" style="float: right;" />
                </td>
			</s:if>
			<td>
                <input class="button" type="button" onclick="location.href='MisPartidos_listAmistoso.action'" value="Cancelar" />
            </td>
		</tr>
	</table>
</s:form>