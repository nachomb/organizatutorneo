<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" language="JavaScript">
	function doSubmit() {
		document.form.submit();
		return true;
	}
	function comparar(){
		var checkboxes = document.getElementById('Torneo_graficoGoles').checkbox;
		var select = document.getElementById("Torneo_graficoGoles_selectedJugadores");
		
		for (var x=0; x < checkboxes.length; ++x) {
			 if (checkboxes[x].checked) {
				 select.value += checkboxes[x].id + ',';
			 }
		}
		if(!select.value){
			alert('Debe seleccionar al menos un jugador.');
		}else{
			select.value = select.value.slice(0, -1);	
			document.form.submit();
		}
		
	}
</script>


<div class="container">
<h2>Tabla Goleadores - <s:property value="%{nombre}" /></h2>
<br>

	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="selectedJugadores" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
		<s:url id="urlTablaPos" action="Torneo_estadisticas"
			includeParams="all"></s:url>
		<s:url id="urlTablaGol" action="Torneo_tablaGoleadores"
			includeParams="all"></s:url>
		<s:url id="urlTablaTarjetas" action="Torneo_tablaTarjetas"
			includeParams="all"></s:url>
		<s:url id="urlTablaSanciones" action="Torneo_tablaSanciones"
			includeParams="all"></s:url>
		<div align="center">			
			<s:if test="%{tipoTorneo == 'Todos contra Todos'}">
				<button type="button" onclick="location.href='<s:property value="%{urlTablaPos}" />'" class="btn btn-default">Posiciones</button>
			</s:if>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaGol}" />'" class="btn active">Goleadores</button>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaTarjetas}" />'" class="btn btn-default">Tarjetas</button>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaSanciones}" />'" class="btn btn-default">Sancionados</button>
		</div>
	<br>
	<br>
		<display:table class="table table-hover table-striped" id="tablaGolesGrid" name="tablaGoleadores"
				sort="list" requestURI="Torneo_tablaGoleadores.action"
				defaultsort="4" defaultorder="descending">
				<s:set name="myrow" value="#attr.tablaGolesGrid"/>
				<display:column title="N"><%=pageContext.getAttribute("tablaGolesGrid_rowNum")%></display:column>
				<display:column title="Jugador" property="jugador"
					defaultorder="ascending" sortable="true" >
				</display:column>
				<display:column title="Equipo" property="jugador.equipo.nombre"
					sortable="true">
				</display:column>
				<display:column title="Goles" property="goles"
					defaultorder="descending" sortable="true" >
				</display:column>
<%-- 				<display:column title=""  class="ancho-20"> --%>
<%-- 					<input type="checkbox" id="<s:property value="#myrow.jugador.id"/>" name="checkbox"/> --%>
<%-- 				</display:column> --%>
			</display:table>
			
			<s:url action="Torneo_list" var="urlTorneoList"/>
			<button type="button" onclick="location.href='<s:property value="%{urlTorneoList}" />'" class="btn btn-default">Volver</button>
</div>