<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="error-login">
<h2>Error inesperado</h2>
  <p>
    Por favor reporte este error al administrador del sistema. 
  </p>
  <hr/>
  <h3>Mensaje de error</h3>
    <s:actionerror/>
    <p>
      <s:property value="%{exception.message}"/>
    </p>
    <hr/>
    <h3>Detalles T&eacute;cnicos</h3>
    <pre>
    <p>
      <s:property value="%{exceptionStack}"/>
    </p>
	</pre>
</div>
