<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="partidos != null">

	 <s:select id="partidosSeleccionados"
	 key="partidosSeleccionados" listKey="id" 
	 list="partidos" multiple="true" onchange="cargarPartidosRestricciones()" theme="simple" cssClass="form-control" size="10" />
	 
</s:if>
