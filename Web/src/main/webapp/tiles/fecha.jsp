<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>

<script type="text/javascript" language="JavaScript">
function doSubmit(){
	cargarPartidos();
	document.form.submit();
	return true;
}

function cargarPartidos(){
	var partidos = "";
	$('span.partido:visible').each(function(){
		var aux = "";
		aux += $(this).find('#equipoLocal option:selected').val() + ',';
		aux += $(this).find('#equipoVisitante option:selected').val() + ',';
		partidos += aux;
	});
	var equipoLibre = $('#equipoLibre option:selected').val();
	if(equipoLibre != -1){
		partidos += equipoLibre;
	}
	$('#form_partidosNuevos').val(partidos);
}

</script>

<div class="container">
<h2> Fecha </h2>
<s:form id="form" key="form" action="%{actionMethod}" method="POST" theme="simple" cssClass="form-horizontal" >
	<s:hidden key="id" />
	<s:hidden key="requestId" />
    <s:hidden key="torneoId" />
    <s:hidden key="torneoIdNewFecha" />
    <s:hidden key="partidosNuevos" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror/>
	<br>
	<br>
		 <div class="form-group">
		   <label class="col-lg-2 control-label" for="inputNumero">Numero</label>
		   <div class="col-lg-10">
		      		<s:textfield key="numero" disabled="%{readOnly}"
					label="Numero" maxlength="30" required="true" cssClass="form-control"/>
		   </div>
		 </div>
		 <div class="form-group">
		   <label class="col-lg-2 control-label" for="inputNombre">Nombre</label>
		   <div class="col-lg-10">
		      		<s:textfield key="nombre" disabled="%{readOnly}"
					label="Nombre" maxlength="30" required="true" cssClass="form-control" />
		   </div>
		 </div>

                 <div>
                    <label for="partidos">Partidos:</label>
                    <s:iterator value="partidos" var="partido" status="status" >
                     
                            <div class="row partido">
<!-- 								  <label class="col-lg-2 control-label" for="cantCanchas">Partido</label> -->
								  <div class="col-xs-2">
								  	<s:date name="partidos[%{#status.index}].fechaPartido" format="yyyy-MM-dd" />
								  </div>
								  <div class="col-xs-2">
									<s:select id="equipoLocal" value="%{#partido.equipoLocal.id}" list="%{torneo.equipos}" theme="simple"  listKey="id" listValue="nombre" cssClass="form-control" multiple="false" />
								  </div>
								  <div class="col-xs-1">
									<s:textfield key="partidos[%{#status.index}].golesLocal" value="%{#partido.golesLocal}" theme="simple" size="3" maxlength="3" disabled="%{readOnly}" cssClass="form-control" />
								  </div>
								  <div class="col-xs-1">
									<s:textfield key="partidos[%{#status.index}].golesVisitante" value="%{#partido.golesVisitante}" theme="simple" size="3" maxlength="3" disabled="%{readOnly}" cssClass="form-control"/>
								  </div>
								  <div class="col-xs-2">
									<s:select id="equipoVisitante" value="%{#partido.equipoVisitante.id}" list="%{torneo.equipos}" theme="simple" listKey="id" listValue="nombre" cssClass="form-control" multiple="false"/>
								  </div>
								  <div class="col-xs-2">
								  	<label class="col-lg-2 control-label" for="jugado">Jugado</label><s:checkbox key="partidos[%{#status.index}].jugado" value="%{#partido.jugado}" theme="simple" disabled="%{readOnly}" cssClass="form-control"/>
								  </div>
                                    <s:if test="%{!readOnly}">
                                        <s:url value="Partido_input.action" var="cargarPartido">
                                            <s:param name="requestId" value="#partido.id" />
                                        </s:url>
                                        
                                        <s:if test="%{#partido.jugado}">
                                            <s:a value="%{cargarPartido}" > Modificar </s:a>
                                        </s:if>
                                        <s:else>
                                       		<s:a value="%{cargarPartido}" > Cargar </s:a>
                                        </s:else>
                                    </s:if>
                                    <s:else>
                                        <s:url value="DetallePartido.action" var="detallePartido">
                                            <s:param name="requestId" value="#partido.id" />
                                        </s:url>
                                        <s:a value="%{detallePartido}"> Detalle </s:a>
                                        <s:url value="HistorialPartido.action" var="historialUrl">
                                            <s:param name="requestId" value="#partido.id" />
                                        </s:url>
                                        <s:a value="%{historialUrl}"> Historial </s:a>
                                    </s:else>
							</div>
                     
                     
<%--                                 <span class="partido"> --%>
<%--                                     <s:date name="partidos[%{#status.index}].fechaPartido" format="yyyy-MM-dd" /> --%>
<%-- 									<s:select cssClass="noMultiple" id="equipoLocal" value="%{#partido.equipoLocal.id}" list="%{torneo.equipos}" theme="simple"  listKey="id" listValue="nombre"  /> --%>
<%--                                     <s:textfield key="partidos[%{#status.index}].golesLocal" value="%{#partido.golesLocal}" theme="simple" size="3" maxlength="3" disabled="%{readOnly}" />   --%>
<!--                                     -  -->
<%--                                     <s:textfield key="partidos[%{#status.index}].golesVisitante" value="%{#partido.golesVisitante}" theme="simple" size="3" maxlength="3" disabled="%{readOnly}"/>  --%>
<%-- 									<s:select cssClass="noMultiple" id="equipoVisitante" value="%{#partido.equipoVisitante.id}" list="%{torneo.equipos}" theme="simple" listKey="id" listValue="nombre"  />                                     --%>
<!--                                     -  -->
<%--                                     <s:checkbox key="partidos[%{#status.index}].jugado" value="%{#partido.jugado}" theme="simple" disabled="%{readOnly}"/> --%>
<!--                                     Finalizado -->
<%--                                     <s:if test="%{!readOnly}"> --%>
<%--                                         <s:url value="Partido_input.action" var="cargarPartido"> --%>
<%--                                             <s:param name="requestId" value="#partido.id" /> --%>
<%--                                         </s:url> --%>
                                        
<%--                                         <s:if test="%{#partido.jugado}"> --%>
<%--                                             <s:a value="%{cargarPartido}" > Modificar </s:a> --%>
<%--                                         </s:if> --%>
<%--                                         <s:else> --%>
<%--                                        		<s:a value="%{cargarPartido}" > Cargar </s:a> --%>
<%--                                         </s:else> --%>
<%--                                     </s:if> --%>
<%--                                     <s:else> --%>
<%--                                         <s:url value="DetallePartido.action" var="detallePartido"> --%>
<%--                                             <s:param name="requestId" value="#partido.id" /> --%>
<%--                                         </s:url> --%>
<%--                                         <s:a value="%{detallePartido}"> Detalle </s:a> --%>
<%--                                         <s:url value="HistorialPartido.action" var="historialUrl"> --%>
<%--                                             <s:param name="requestId" value="#partido.id" /> --%>
<%--                                         </s:url> --%>
<%--                                         <s:a value="%{historialUrl}"> Historial </s:a> --%>
<%--                                     </s:else> --%>
<%--                                 </span> --%>
                                </s:iterator>
                                
					     <div class="form-group">
						   <label class="col-lg-2 control-label" for="selectEquipoLibre">Equipo libre</label>
						   <div class="col-lg-2">
								<s:select label="Equipo Libre" value="%{equipoLibre.id}" id="equipoLibre" list="%{torneo.equipos}" listKey="id" listValue="nombre" 
								 headerKey="-1" headerValue="Sin equipo Libre" cssClass="form-control" multiple="false"/>
						   </div>
						 </div>
						
                 </div>
            
           <s:if test="%{!readOnly}">
		    	<div class="form-group">
				   <div class="col-lg-offset-2 col-lg-10">
						<button type="button" onclick="return doSubmit()" class="btn btn-success">Guardar</button>
						<s:url value="Fecha_list.action" var="urlFechaList">
		                	<s:param name="torneoId" value="%{torneo.id}" />
		                </s:url>
						<button type="button" onclick="location.href='<s:property value="%{urlFechaList}" />'" class="btn btn-danger">Cancelar</button>
				   </div>
			    </div>
           </s:if>
           <s:else>
               <td><input  class="button" type="button" onclick="history.back()" value="Volver" style="float: right;" /></td>
           </s:else>
        
</s:form>
</div>