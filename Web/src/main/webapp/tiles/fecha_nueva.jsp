<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>

<script type="text/javascript">
function doSubmit(){
	cargarPartidos();
	document.form.submit();
	return true;
}
function cargarPartidos(){
	var partidos = "";
	$('.partido:visible').each(function(){
		var aux = "";
		aux += $(this).find('#equipoLocal option:selected').val() + ',';
		aux += $(this).find('#equipoVisitante option:selected').val() + ',';
		partidos += aux;
	});
	var equipoLibre = $('#equipoLibre option:selected').val();
	if(equipoLibre != -1){
		partidos += equipoLibre;
	}
	$('#form_partidosNuevos').val(partidos);
}
function agregarPartido(){
	if($('.partido:first').is(':hidden')){
		$('.partido:first').show();
	}else{
		$('.partido:first').clone().insertAfter($('.partido:last'));
	}
	$('.removebtn').click(function () {
		if($('.partido').size() != 1){
	    	$(this).parent().parent().remove();
		  }else{
			  $(this).parent().parent().hide();
		  }
	});
}
$(function() {
	$('.removebtn').click(function () {
		if($('.partido').size() != 1){
	    	$(this).parent().parent().remove();
		  }else{
			  $(this).parent().parent().hide();
		  }
	});
	
});

</script>
<div class="container">
<h2> Fecha Nueva - <s:property value="%{torneo.nombre}"/></h2>
<s:form id="form" key="form" action="%{actionMethod}" method="POST" theme="simple" cssClass="form-horizontal">
	<s:hidden key="id" />
    <s:hidden key="torneoIdNewFecha" />
    <s:hidden key="partidosNuevos" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror/>
	<br>
	<br>
		 <div class="form-group">
		   <label class="col-lg-2 control-label" for="inputNumero">Numero</label>
		   <div class="col-lg-10">
		      		<s:textfield key="numero" disabled="%{readOnly}"
					label="Numero" maxlength="30" required="true" cssClass="form-control"/>
		   </div>
		 </div>
		 <div class="form-group">
		   <label class="col-lg-2 control-label" for="inputNombre">Nombre</label>
		   <div class="col-lg-10">
		      		<s:textfield key="nombre" disabled="%{readOnly}"
					label="Nombre" maxlength="30" required="true" cssClass="form-control" />
		   </div>
		 </div>
		
		<div>
            <label for="partidos">Partidos:</label>
            <div class="row partido">
				<label class="col-lg-2 control-label" for="partido">Partido</label>
	            <div class="col-xs-2">
	            	<s:select id="equipoLocal" list="equiposPosibles" listKey="id" theme="simple" cssClass="form-control" multiple="false"/>
				 </div>
				 <div class="col-xs-2">
				 	<s:select  id="equipoVisitante" list="equiposPosibles" listKey="id" theme="simple" cssClass="form-control" multiple="false"/>
				 </div>
				<div class="col-xs-1">
			  		<a class="removebtn"><i class="glyphicon glyphicon-remove" ></i></a>
			  	</div>
            </div>
            
            <div class="form-group">
			   <div class="col-lg-offset-2 col-lg-10">
			   		<button type="button" class="btn btn-default" onclick="agregarPartido()">Agregar Partido</button>
			   </div>
		 	</div>
        
	        <div class="form-group">
			   <label class="col-lg-2 control-label" for="selectEquipoLibre">Equipo libre</label>
			   <div class="col-lg-2">
					<s:select label="Equipo Libre" value="%{equipoLibre.id}" id="equipoLibre" list="%{torneo.equipos}" listKey="id" listValue="nombre" 
					 headerKey="-1" headerValue="Sin equipo Libre" cssClass="form-control" multiple="false"/>
			   </div>
			 </div>
        </div>
		
		<tr>
            <s:if test="%{!readOnly}">
            	<div class="form-group">
				   <div class="col-lg-offset-2 col-lg-10">
						<button type="button" onclick="return doSubmit()" class="btn btn-success">Guardar</button>
						<button type="button" onclick="history.back()" class="btn btn-danger">Cancelar</button>
				   </div>
			    </div>
            </s:if>
            <s:else>
                <td><input  class="button" type="button" onclick="history.back()" value="Volver" style="float: right;" /></td>
            </s:else>
		</tr>
        
</s:form>

</div>