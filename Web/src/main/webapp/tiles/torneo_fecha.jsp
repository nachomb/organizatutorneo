<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript">
function doSubmit(){
	document.form.submit();
	return true;
}

</script>

<div class="container">
<br>
<h2>Torneo - <s:property value="%{nombre}" /></h2>
<s:form key="form" action="%{actionMethod}" method="POST" theme="simple" cssClass="form-horizontal">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
	<br>
<%-- 			<s:property value="nombre" /> --%>
<%-- 			<s:property value="cantidadFechas" /> --%>

		<s:if test="%{readOnly}">
			<tr>
				<td class="tdLabel"><s:url value="DetalleFecha.action"
					var="ultimaFecha">
					<s:param name="torneoId" value="id" />
				</s:url> <s:a value="%{ultimaFecha}" cssClass="label"> Detalle Fecha Actual </s:a>
				</td>
			</tr>
		</s:if>

		<h3>Equipos:</h3>
		<div class="well">
			<ol>
				<s:iterator value="equipos" var="equipo">
					<li><span><s:property value="#equipo.nombre" /> </span> 
					</li>
				</s:iterator>
			</ol>
		</div>
		

                <div>
                	<h3>Fixture:</h3>
                    <s:iterator value="fechas" var="fecha">
	                    <div class="panel panel-info">
						  <div class="panel-heading">
						    <h3 class="panel-title"><s:property value="#fecha.numero" /> - <s:property value="#fecha.nombre" /></h3>
						  </div>
						  <div class="panel-body">
	                        	<div align="center">
		                        <ul class="list-group">
								    <s:iterator value="#fecha.partidos" var="partido">
								  			<li class="list-group-item">
								  				<a onblur=""><s:property value="#partido.equipoLocal" /> <span class="badge"><s:property value="#partido.golesLocal" /></span> vs <span class="badge"><s:property value="#partido.golesVisitante" /></span> <s:property value="#partido.equipoVisitante" /></a>
								  				<span class="label label-default"><s:date name="#partido.fechaPartido" format="dd/MM HH:mm" /></span>
								  				<span class="label label-default"><s:property value="#partido.descripcion" /></span>
								  			</li>
			                        </s:iterator>
								</ul>
		                        </div>
						  </div>
						  <s:if test="#fecha.equipoLibre != null">
		                        <div align="center" class="panel-footer">
										Equipo Libre: <s:property value="#fecha.equipoLibre" />
								</div>
						   </s:if>
						</div>
                    </s:iterator>
                </div>
		

			<s:if test="%{habilitarOKenabled}">
				<button type="button" onclick="return doSubmit()" class="btn btn-success">Ok</button>
			</s:if>
			<s:if test="%{!readOnly}">
				<button type="button" onclick="location.href='Torneo_list.action'" class="btn btn-default">Volver</button>
				<button type="button" onclick="location.href='Fecha_list.action?torneoId=<s:property value="%{id}"/>'" class="btn btn-default">Editar Fechas</button>
			</s:if>
			<s:else>
				<input class="button" type="button" onclick="location.href='ConsultaTorneo.action'" value="Cancelar" />
			</s:else>

</s:form>
</div>