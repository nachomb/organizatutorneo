<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="fechas != null">

	 <s:select id="fechasSeleccionadas" 
	 key="fechasSeleccionadas" listKey="id" listValue="fechaYTorneo" 
	 list="fechas" multiple="true" onchange="javascript:mostrarPartidos();return false;"  theme="simple" cssClass="form-control" size="10" />
	 
</s:if>