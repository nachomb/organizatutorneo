<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<div class="container">

		<h2>Jugadores</h2>
		<div align="center">
			<s:a cssClass="btn btn-success" href="Jugador_add.action">Nuevo</s:a>
		</div>
		<br>
		
<s:form method="POST" action="%{actionMethod}" theme="simple" cssClass="form-inline">
	<br>
	<s:if test="%{!readOnly}">
	  <div class="form-group">
	    <label class="sr-only" for="Jugador_list_nombreFiltro">Nombre</label>
	    <s:textfield key="nombreFiltro" disabled="%{readOnly}"
				label="Nombre" maxlength="30" cssClass="form-control" placeholder="Nombre"/>
	  </div>
	  <div class="form-group">
	    <label class="sr-only" for="Jugador_list_apellidoFiltro">Apellido</label>
	    <s:textfield key="apellidoFiltro" disabled="%{readOnly}"
				label="Apellido" maxlength="30" cssClass="form-control" placeholder="Apellido" />
	  </div>
	  <button type="submit" class="btn btn-default">Filtrar</button>	
<!-- 		<tr> -->
<%-- 			<td><s:submit value="Filtrar" label="Filtrar" theme="simple"/></td> --%>
<!-- 		</tr> -->
		<br>
		<display:table class="table table-hover table-striped" id="jugadorGrid" name="entities" pagesize="20" requestURI="Jugador_list.action" partialList="true" size="rowCount" excludedParams="*">
	        <s:set name="myrow" value="#attr.jugadorGrid"/>
	        <display:column property="id" title="Id" />
			<display:column property="nombre" title="Nombre" />
			<display:column property="apellido" title="Apellido" />
		 	<display:column property="equipo.nombre" title="Equipo" />
			<display:column property="apodo" title="Apodo" />
		 	
			<display:column title="Activo" >
				<s:if test="#myrow.activo">
					<i class="glyphicon glyphicon-ok"></i>
				</s:if>
			
			</display:column> 
	        <display:column title="Click para editar" href="Jugador_edit.action" paramId="requestId" paramProperty="id">
	            <i class="glyphicon glyphicon-pencil"></i>
	        </display:column>
<%-- 	        <display:column title="Click para eliminar" href="Jugador_destroy.action" paramId="requestId" paramProperty="id"> --%>
<%-- 	         	<s:if test="#myrow.activo"> --%>
<!-- 	            	<img src="imagenes/borrar.gif" alt="si"/> -->
<%-- 	            </s:if>  --%>
<%-- 	        </display:column> --%>
		</display:table>

<!-- 		<button type="button" onclick="location.href='Importer_jugadores.action'" class="btn btn-default">Importar</button> -->
	
	</s:if>
	<s:else>
		<display:table id="jugadorGrid" name="entities" pagesize="10" requestURI="ConsultaJugador.action" partialList="true" size="rowCount" excludedParams="*">
            <s:set name="myrow" value="#attr.jugadorGrid"/>
            <display:column property="id" title="Id" />
            <display:column property="nombre" title="Nombre" />
            <display:column property="apellido" title="Apellido" />
            <display:column property="apodo" title="Apodo" />
            <display:column title="Activo" >
                <s:if test="#myrow.activo">
                    <img src="imagenes/check-ok-blue_15x15.png" alt="si"/>
                </s:if>
            </display:column>
            <display:column title="Detalles" href="DetalleJugador.action" paramId="requestId" paramProperty="id">
                <img src="imagenes/detalles.gif" alt="si"/>
             </display:column>
        </display:table>
	</s:else>
</s:form>
</div>