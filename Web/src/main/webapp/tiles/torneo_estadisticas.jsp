<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript">
	function doSubmit() {
		document.form.submit();
		return true;
	}
</script>



<div class="container">
<h2>Estadisticas - <s:property value="%{nombre}" /></h2>
<br>
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />

		<s:url id="urlTablaPos" action="Torneo_estadisticas"
			includeParams="all"></s:url>
		<s:url id="urlTablaGol" action="Torneo_tablaGoleadores"
			includeParams="all"></s:url>
		<s:url id="urlTablaTarjetas" action="Torneo_tablaTarjetas"
			includeParams="all"></s:url>
		<s:url id="urlTablaSanciones" action="Torneo_tablaSanciones"
			includeParams="all"></s:url>
		<div align="center">
			<s:if test="%{tipoTorneo == 'Todos contra Todos'}">
				<button type="button" onclick="location.href='<s:property value="%{urlTablaPos}" />'" class="btn active">Posiciones</button>
			</s:if>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaGol}" />'" class="btn btn-default">Goleadores</button>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaTarjetas}" />'" class="btn btn-default">Tarjetas</button>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaSanciones}" />'" class="btn btn-default">Sancionados</button>
		</div>
		<br>
		<br>

			<display:table class="table table-hover table-striped" id="tablaPosicionesGrid" name="posiciones" sort="list" requestURI="Torneo_estadisticas.action"
				defaultsort="10" defaultorder="descending" >
				<display:column title="N"><%=pageContext.getAttribute("tablaPosicionesGrid_rowNum")%></display:column>
				<display:column title="Equipo" property="equipo.nombre"
					sortable="true">
				</display:column>
				<display:column title="PJ" property="partidosJugados"
					sortable="true">
				</display:column>
				<display:column title="PG" property="partidosGanados"
					sortable="true">
				</display:column>
				<display:column title="PE" property="partidosEmpatados"
					sortable="true">
				</display:column>
				<display:column title="PP" property="partidosPerdidos"
					sortable="true">
				</display:column>
				<display:column title="GF" property="golesAFavor" sortable="true">
				</display:column>
				<display:column title="GC" property="golesEnContra" sortable="true">
				</display:column>
				<display:column title="Dif" property="diferencia" sortable="true" >
				</display:column>
				<display:column title="Pts" property="puntos" sortable="true" >
				</display:column>
				<display:column title="%" property="porcentajePuntos" sortable="true" >
				</display:column>
			</display:table>
			
			<s:url action="Torneo_list" var="urlTorneoList"/>
			<button type="button" onclick="location.href='<s:property value="%{urlTorneoList}" />'" class="btn btn-default">Volver</button>
<%-- 			<button type="button" onclick="location.href='Torneo_reporte.action?requestId=<s:property value="%{id}"/>'" class="btn btn-default">Reporte </button> --%>
</div>