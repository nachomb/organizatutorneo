<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" language="JavaScript">
	function doSubmit() {
		document.form.submit();
		return true;
	}
</script>


<div align="center"><br>
<h2>Grafico Goles vs. Fechas - <s:property value="%{nombre}" /></h2>
<br>
</div>

<s:form key="form" action="%{actionMethod}" method="POST">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
	<br>
	<br>
	<s:action name="Torneo_calcularMaxGoles"/>
	<img src="https://chart.googleapis.com/chart?cht=lxy&chs=600x250&chxt=x,y&chxr=0,0,<s:property value="%{fechaActualMenosUno}"/>,1|1,0,<s:property value="%{maxGoles}"/>,1&chd=t:<s:property value="%{datosGrafico}"/>&chds=0,<s:property value="%{maxGoles}"/>&chm=<s:property value="%{coloresPuntos}"/>&chg=10,10&chdl=<s:property value="%{leyendaJugadores}"/>&chdlp=b&chco=<s:property value="%{coloresLineas}"/>" />
		<tr>
			<td><input class="button" type="button"
				onclick="history.back()" value="Volver" /></td>
		</tr>
</s:form>
