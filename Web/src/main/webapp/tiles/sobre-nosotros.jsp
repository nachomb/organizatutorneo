<div id="contenidoSeccion">
        
        <h1>Sobre Nosotros</h1>
			<h2>En los &uacute;ltimos a&ntilde;os el F&uacute;tbol Femenino se convirti&oacute; en una actividad que ha mostrado un notable crecimiento en nuestro pa&iacute;s e incluso a nivel mundial.			  </h2>
            <div id="separacionTitulos"></div>
           
            
			<h3>Pudimos observar que el <strong>F&uacute;tbol Femenino</strong> se ha convertido en furor en la mayor&iacute;a de los barrios de la Capital Federal y su expansi�n fue inminente hacia las diferentes zonas del Gran Buenos Aires, especialmente en zona norte, aunque todav&iacute;a es una actividad poco desarrollada en el Oeste del GBA.<br />
			  <br />
Es as&iacute; como surge la <strong>"Liga Futboleras"</strong>, una nueva propuesta deportiva para las mujeres de <strong>Zona Oeste</strong>, convirti�ndose en <strong>El Primer Torneo de F&uacute;tbol 5 Femenino Amateur</strong> de la zona.
			  
			  <strong>Liga Futboleras</strong> es un torneo de <strong>F&uacute;tbol Femenino</strong> el cual pone como principal valor el Fair Play y la buena onda, concibi�ndose (adem&aacute;s de jugar al f&uacute;tbol y competir sanamente) como un lugar de Encuentro, tanto para las chicas amantes del f&uacute;tbol como as&iacute; tambi�n para las que quieran divertirse con sus amigas realizando este apasionante deporte, y disfrutar de un domingo divertido y diferente.<br />
			  <br />
			  
			  As&iacute; que si te gusta el f&uacute;tbol, sos buena onda, tus Domingos son un "Baj�n" o simplemente quer�s divertirte con amigas, sum&aacute;te y form&aacute; parte de nuestro torneo.
			  
              
</div>