<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" language="JavaScript">
function doSubmit(){
	seleccionarTodos(true);
	document.form.submit();
	return true;
}

function seleccionarTodos(seleccionado) {
	
	var select = document.getElementById("miembrosSeleccionados");
	var elems = select.getElementsByTagName("option");
	for (var i = 0; i < elems.length; ++i) {
		elems[i].selected = seleccionado;
	}

}

</script>

<s:if test="id == null">
	<div align="center"><br>
	<h2>Grupo Nuevo</h2>
	<br>
	</div>
</s:if>
<s:form key="form" action="%{actionMethod}" method="POST">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror/>
	<br>
	<br>
	<table align="center" width="100%">
		<s:if test="id != null">
			<tr>
				<td>
					<s:label id="id" name="id" key="id" label="Id"/>
				</td>
			</tr>
		</s:if>
        
        <tr>
            <td>
                <s:label key="creador.usuario.username" label="Creador"  />
            </td>
        </tr>
        
		<tr>
			<td><s:textfield key="nombre" disabled="%{readOnly}"
				label="Nombre" maxlength="30" required="true"/></td>
		</tr>
		 <s:inputtransferselect
		 	  id="miembrosSeleccionados"
		      label="Miembros"
		      name="jugadoresSel"
		      list="jugadoresSel"
		      addLabel="Agregar"
		      allowUpDown="false"
		      leftTitle="Mail"
		      
		      rightTitle="Miembros"
		      removeLabel="Borrar"
		      removeAllLabel="Borrar Todos"
		  />

		
		<tr>
			<s:if test="%{habilitarOKenabled}">
				<td><input class="button" type="button" onclick="return doSubmit()" value="OK" style="float:right;" /></td>
			</s:if>
			<td><input  class="button" type="button" onclick="location.href='Grupo_list.action'" value="Cancelar" /></td>
		</tr>
	</table>
</s:form>