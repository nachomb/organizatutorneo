<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<div class="container">
		<h2>Equipos</h2>
		<div align="center">
			<s:a cssClass="btn btn-success" href="Equipo_add.action">Nuevo</s:a>
		</div>
		<br>
	<div>
	
	<display:table class="table table-hover table-striped" id="equipoGrid" name="entities" pagesize="20" requestURI="Equipo_list.action" partialList="true" size="rowCount" excludedParams="*">
        <s:set name="myrow" value="#attr.equipoGrid"/>
        <display:column property="id" title="Id" />
		<display:column property="nombre" title="Nombre" />
	 	
	 	<display:column title="Activo" >
			<s:if test="#myrow.activo">
				<i class="glyphicon glyphicon-ok"></i>
			</s:if>  
		</display:column> 
	        <display:column title="Click para editar" href="Equipo_edit.action" paramId="requestId" paramProperty="id">
	            <i class="glyphicon glyphicon-pencil"></i>
	        </display:column>
<%-- 	        <display:column title="Click para eliminar" href="Equipo_destroy.action" paramId="requestId" paramProperty="id"> --%>
<%-- 	         	<s:if test="#myrow.activo"> --%>
<!-- 	            	<img src="imagenes/borrar.gif" alt="si"/> -->
<%-- 	            </s:if>  --%>
<%-- 	        </display:column> --%>
	</display:table>
    	
	</div>
</div>
