<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<script type="text/javascript" language="JavaScript">
function doSubmit(){
	var torneoSeleccionado = document.getElementById("requestId");
	document.form.select;
	var select = document.getElementById("select");
	var elems = select.getElementsByTagName("option");
	for (var i = 0; i < elems.length; ++i) {
		if (elems[i].selected) {
			torneoSeleccionado.value = elems[i].value;
		}
	}
	if(torneoSeleccionado.value == 0)
		return false;
	document.form.submit();
	return true;
}
</script>
<script type="text/javascript" src="js/efecto-hover-listados.js"></script>

<div id="contenidoSeccion">
        
        <h1>
			<s:if test="%{act == true}">
				Estad&iacute;sticas
			</s:if>
			<s:else>
				Torneos Anteriores
			</s:else>
        </h1>
        <s:if test="{nombre != null}">
			<h2><s:property value="nombre"/></h2>
		</s:if>	 
		<div id="separacionTitulos"></div>
		 
		<div class="textoGeneral">		
		<p>

		<s:form key="form" action="%{actionMethod}" method="POST" >
				<table class="wwFormTable">
						<tr>
							<td class="tdLabel">
								<label class="label" for="select">Torneo:</label>
							</td>
							<td class="">
							<s:hidden key="requestId" id="requestId" />
							<s:hidden key="act" id="act" />
								<s:select id="select" label="Torneo" disabled="%{readOnly}"
										name="selectedTorneo" key="selectedTorneo" headerKey="0"
									   headerValue="Seleccione" list="torneos" 
									   listValue="nombre"
									   listKey="id" onchange="return doSubmit()" theme="simple"/>
							</td>
							<td>
								<s:if test="%{requestId != 0}"> 
									<s:url action="Fixture" var="url_fixture" includeParams="none">
				                    	<s:param name="requestId" value="%{id}" />
				                    	<s:param name="act" value="%{activo}" />
				           		    </s:url>
									<s:a href="%{url_fixture}" >
									
										<s:if test="%{act == true}">
											Fixture
										</s:if>
										<s:else>
											Resultados
										</s:else>
									
									</s:a>
								</s:if>
							</td>
						</tr>
				</table>
			   	<br/>

				<display:table id="tablaPosicionesGrid" name="posiciones"
						sort="list" requestURI="Estadisticas.action"
						defaultsort="10" defaultorder="descending" 
					     class="tablaPosiciones">
						<display:column title="N�" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"><%=pageContext.getAttribute("tablaPosicionesGrid_rowNum")%></display:column>
						<display:column title="Equipo" property="equipo.nombre"
							 class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
						<display:column title="PJ" property="partidosJugados"
						 class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
						<display:column title="PG" property="partidosGanados"
							 class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
						<display:column title="PE" property="partidosEmpatados"
							 class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
						<display:column title="PP" property="partidosPerdidos"
							 class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
						<display:column title="GF" property="golesAFavor"  class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
						<display:column title="GC" property="golesEnContra" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
						<display:column title="Dif" property="diferencia" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
						<display:column title="Pts" property="puntos" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
						<display:column title="%" property="porcentajePuntos" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
					</display:table>
					
					<display:table id="tablaGolesGrid" name="tablaGoleadores"
						sort="list" requestURI="Estadisticas.action"
						defaultsort="4" defaultorder="descending" class="tablaPosiciones" length="15">
						<s:set name="myrow" value="#attr.tablaGolesGrid"/>
						<display:column title="N�" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"><%=pageContext.getAttribute("tablaGolesGrid_rowNum")%></display:column>
						<display:column title="Jugador" property="jugador.nombreYApellido"
							defaultorder="ascending" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
						<display:column title="Equipo" property="equipo.nombre"
						   class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
						<display:column title="Goles" property="goles"
							defaultorder="descending" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/>
					</display:table>
										
					<display:table id="tablaTarjetasGrid" name="jugadoresTarjetas"
						sort="list" requestURI="Estadisticas.action"
						defaultsort="2" class="tablaPosiciones">
						<s:set name="myrow" value="#attr.tablaTarjetasGrid"/>

						<display:column title="Jugador" class="tablaPosicionesTd" headerClass="tituloTablaPosicion">
							<s:property value="#myrow.jugador.nombreYApellido" />
						</display:column>
						<display:column title="Equipo" property="equipo.nombre"
							 class="tablaPosicionesTd" headerClass="tituloTablaPosicion">
						</display:column>
						<display:column title="Amarillas" property="cantTarjetasAmarillas"
							 class="tablaPosicionesTd" headerClass="tituloTablaPosicion">
						</display:column>
						<display:column title="Rojas" property="cantTarjetasRojas"
							 class="tablaPosicionesTd" headerClass="tituloTablaPosicion">
						</display:column>
						<display:column title="Total" property="cantTarjetasTotal"
						    class="tablaPosicionesTd" headerClass="tituloTablaPosicion">
						</display:column>
					</display:table>
					
<%-- 				 	<display:table id="tablaSancionesGrid" name="sanciones" --%>
<%-- 						sort="list" requestURI="Estadisticas.action" --%>
<%-- 						defaultsort="10" defaultorder="descending"  class="tablaPosiciones" > --%>
<%-- 						<s:set name="myrow" value="#attr.tablaSancionesGrid"/> --%>
<%-- 						<display:column title="N�" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"><%=pageContext.getAttribute("tablaSancionesGrid_rowNum")%></display:column> --%>
<%-- 						<display:column title="Jugador"	property="jugadorSancionado" defaultorder="ascending" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/> --%>
<%-- 						<display:column title="Equipo"	property="jugadorSancionado.equipo" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/> --%>
<%-- 						<display:column title="Cant. Fechas" property="tipo.duracionSancion" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/> --%>
<%-- 						<display:column title="Cumplidas" property="cantFechasCumplidas" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/> --%>
<%-- 						<display:column title="Restan" property="cantFechasRestantes" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/> --%>
<%-- 						<display:column title="Causa" property="tipo.descripcion" class="tablaPosicionesTd" headerClass="tituloTablaPosicion"/> --%>
<%-- 					</display:table>   --%>
		</s:form>		

              </p>
</div>
              
</div>