<%@ taglib prefix="s" uri="/struts-tags"%>

	<script type="text/javascript" language="JavaScript">
	function doSubmit(){
		document.form.submit();
		return true;
	}
	</script>

	<s:form key="form" action="%{actionMethod}" method="POST">
		<s:hidden key="id"/>
		<s:hidden key="requestId" id="requestId" />
		<s:hidden key="actionMethod" value="%{actionMethod}"/>
		<s:actionerror/>
		<br>
		<br>
		<table align="center" width="100%">
			<s:if test="id != null">
				<tr>
					<td>
						<s:label id="id" name="id" key="id" label="Id"/>
					</td>
				</tr>
			</s:if>
			<tr>
				<td><s:textfield name="titulo" key="titulo" readonly="%{readOnly}" label="T�tulo" maxlength="20" required="true"/></td>
			</tr>
			<tr>
				<td><s:textfield name="descripcion" key="descripcion" readonly="%{readOnly}" label="Descripci�n" maxlength="100"/></td>
			</tr>
			<tr>
				<td><s:textfield name="orden" key="orden" readonly="%{readOnly}" label="Orden" maxlength="30" required="true"/></td>
			</tr>
			<tr>
				<td><s:select label="Acci�n" 
							  disabled="%{readOnly}" 
							  headerKey="-1" 
							  headerValue="-- Seleccione... --" 
							  list="actions" 
							  listValue="name" 
							  listKey="id" 
							  key="selectedActionID" 
							  required="true"/></td>
			</tr>
	 		<tr>
				<td><s:select name="selectedItemMenuPadreID" 
							  label="Men� Padre" 
							  disabled="%{readOnly}" 
							  headerKey="-1" 
							  headerValue="-- Seleccione... --" 
							  list="itemsMenu" 
							  listValue="titulo" 
							  listKey="id" 
							  key="selectedItemMenuPadreID" /></td>
			</tr>
			<tr>
			<s:if test="%{habilitarOKenabled}">
				<td><input class="button" type="button" onclick="return doSubmit()" value="OK" style="float:right;" /></td>
			</s:if>
			
			<td><input class="button" type="button" onclick="location.href='ItemMenu_list.action'" value="Cancelar" /></td>
			
			</tr>
		</table>
	</s:form>