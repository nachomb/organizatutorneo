<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>

<sx:head parseContent="true" />
<link rel="stylesheet" media="all" type="text/css" href="css/bootstrap-datetimepicker.min.css" />
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
function doSubmit(){
	document.form.submit();
	return true;
}

function mostrarFechas() {
	dojo.event.topic.publish("mostrarFechas");
}
function mostrarPartidos() {
	dojo.event.topic.publish("mostrarPartidos");
}
function previsualizacion() {
	var restric = "";
	$('.restriccion').each(function(){
		var aux = "";
		aux += $(this).find('#tipoRestriccion option:selected').text() + ',';
		aux += $(this).find('#partidosRestriccion option:selected').val() + ',';
		aux += $(this).find('#operadorRestriccion option:selected').text() + ',';
		aux += $(this).find('#valorRestriccion').val() + ';';
		if($(this).find('#valorRestriccion').val() != ''){
			restric += aux;	
		}
		
	});
	$('#form_restriccionesSeleccionadas').val(restric);
	dojo.event.topic.publish("previsualizacion");
}
function cargarPartidosRestricciones(){
	var options = "";
	$('#partidosSeleccionados option:selected').each(function(i){
		var newOption = "<option value=" + $(this).val() + ">" + $(this).text() + "</option>";
		options += newOption;
	});
	$('.partidosRestriccion').each(function(i){
		$(this).html(options);
	});
}
function agregarRestriccion(){
	$('.restriccion:first').clone().insertAfter($('.restriccion:last'));
	$('.removebtn').click(function () {
		if($('.restriccion').size() != 1){
	    	$(this).parent().parent().remove();
		}
	});
}

</script>


<div class="container">
<h2>Programacion de Horarios de Partidos</h2>
<s:form id="form" key="form" action="%{actionMethod}" method="POST" theme="simple" cssClass="form-horizontal">
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:hidden key="restriccionesSeleccionadas" />
	<s:actionerror/>
	<br>

		 <div class="form-group">
		   <label class="col-lg-2 control-label" for="torneos">Torneos</label>
		   <div class="col-lg-10">
					<s:select label="Torneos" disabled="%{readOnly}" id="torneosSeleccionados"
				key="torneosSeleccionados" listKey="id" listValue="nombre"
				 required="true" list="torneosPosibles" onchange="javascript:mostrarFechas();return false;" 
				 multiple="true" theme="simple" cssClass="form-control" size="10"/>
		   </div>
		 </div>
		
		<s:url id="url_fechas" action="ProgramacionPartidos_mostrar_fechas" />
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="fechas">Fechas</label>
		   <div class="col-lg-10">
				<sx:div theme="simple" id="details" href="%{url_fechas}" disabled="%{readOnly}"
								listenTopics="mostrarFechas" formId="form" showLoadingText="cargando fechas"></sx:div>
		   </div>
		 </div>
		
		<s:url id="url_partidos" action="ProgramacionPartidos_mostrar_partidos" />
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="partidos">Partidos</label>
		   <div class="col-lg-10">
				<sx:div theme="simple" id="details" href="%{url_partidos}" disabled="%{readOnly}"
								listenTopics="mostrarPartidos" formId="form" showLoadingText="cargando partidos"></sx:div>
		   </div>
		 </div>
		
		<div class="form-group">
		 	<label class="col-lg-2 control-label" for="fechaYHora">Fecha y hora del primer partido</label>
	        <div class="input-group date" id="datetimepicker">
	            <s:textfield disabled="%{readOnly}"
				name="fechaYHora" id="fechaYHora" required="true" theme="simple" cssClass="form-control"/>
	            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
	            </span>
	        </div>
        </div>

		<div class="form-group">
		   <label class="col-lg-2 control-label" for="cantCanchas">Cantidad de canchas</label>
		   <div class="col-lg-10">
			   <s:textfield key="cantCanchas" disabled="%{readOnly}"
					 maxlength="3" size="3" required="true" cssClass="form-control" theme="simple"/>
		   </div>
		 </div>
		 
		 <div class="form-group">
		   <label class="col-lg-2 control-label" for="cantCanchas">Tiempo por partido(min)</label>
		   <div class="col-lg-10">
			   <s:textfield key="tiempoPorPartido" disabled="%{readOnly}"
					 maxlength="3" size="3" required="true" cssClass="form-control" theme="simple"/>
		   </div>
		 </div>
        
<!-- 		<tr> -->
<%-- 			<td><s:textfield key="cantCanchas" disabled="%{readOnly}" --%>
<%-- 				label="Cantidad de Canchas" maxlength="3" size="3" --%>
<%-- 				required="true" /></td> --%>
<!-- 		</tr> -->
		
<!-- 		<tr> -->
<%-- 			<td><s:textfield key="tiempoPorPartido" disabled="%{readOnly}" --%>
<%-- 				label="Tiempo por Partido(min)" maxlength="3" size="3" --%>
<%-- 				required="true" /></td> --%>
<!-- 		</tr> -->
		
<!-- 		<tr id="restriccion" class="restriccion"> -->
<!-- 			<td class="tdLabel"><label class="label"> Restriccion:</label></td> -->
<%-- 			<td><s:select theme="simple" id="tipoRestriccion" list="restricciones" cssClass="noMultiple" /></td> --%>
<%-- 			<td><select id="partidosRestriccion" class="noMultiple partidosRestriccion"></select></td> --%>
<%-- 			<td><s:select id="operadorRestriccion" theme="simple" list="operadores" cssClass="noMultiple" style="width:40px;" /></td> --%>
<%-- 			<td><s:textfield id="valorRestriccion" theme="simple" size="6" /></td> --%>
<!-- 		</tr> -->
		
		<div class="row restriccion">
			<label class="col-lg-2 control-label" for="restriccion">Restriccion</label>
		  <div class="col-xs-2">
		  	<s:select theme="simple" id="tipoRestriccion" list="restricciones" cssClass="form-control" multiple="false" />
		  </div>
		  <div class="col-xs-2">
			<select id="partidosRestriccion" class="form-control partidosRestriccion" ></select>
		  </div>
		  <div class="col-xs-1">
			<s:select id="operadorRestriccion" theme="simple" list="operadores" cssClass="form-control" multiple="false" />
		  </div>
		  <div class="col-xs-1">
			<s:textfield id="valorRestriccion" theme="simple" size="6" cssClass="form-control input-sm"/>
		  </div>
		  <div class="col-xs-1">
		  	<a class="removebtn"><i class="glyphicon glyphicon-remove" ></i></a>
		  </div>
		</div>
		
	   <div class="form-group">
		   <div class="col-lg-offset-2 col-lg-10">
		   		<button type="button" class="btn btn-default" onclick="agregarRestriccion()">Agregar Restriccion</button>
		   </div>
	 	</div>
		
		<s:url id="url_prev" action="ProgramacionPartidos_previsualizacion" />
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="partidos">Partidos</label>
		   <div class="col-lg-10">
				<sx:div theme="simple" id="details" href="%{url_prev}" disabled="%{readOnly}"
				listenTopics="previsualizacion" formId="form" showLoadingText="cargando partidos"></sx:div>
		   </div>
		 </div>
		
		<div class="form-group">
		   <div class="col-lg-offset-2 col-lg-10">
		   		<button type="button"
					onclick="previsualizacion()" class="btn btn-default">Previsualizar</button>
				<button type="button"
					onclick="return doSubmit()" class="btn btn-success">Publicar</button>
				<button type="button"
					onclick="location.href='Torneo_list.action'" class="btn btn-danger">Cancelar</button>
		   </div>
	    </div>

</s:form>

</div>
<script type="text/javascript">
 $('#datetimepicker').datetimepicker({
     language: 'es'
 });
</script>