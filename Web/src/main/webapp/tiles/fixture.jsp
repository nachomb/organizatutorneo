<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" language="JavaScript">
function doSubmit(){
	var torneoSeleccionado = document.getElementById("requestId");
	document.form.select;
	var select = document.getElementById("select");
	var elems = select.getElementsByTagName("option");
	for (var i = 0; i < elems.length; ++i) {
		if (elems[i].selected) {
			torneoSeleccionado.value = elems[i].value;
		}
	}
	if(torneoSeleccionado.value == 0)
		return false;
	document.form.submit();
	return true;
}
</script>

<s:bean name="ar.com.noxit.picadito.dominio.entidades.comparator.PartidoComparator" var="partidoComparator" />

<div id="contenidoSeccion">
        
          <s:if test="%{activo == true}"> 	
       	  	<h1>Fixture</h1>
       	  </s:if>
       	  <s:else>
       	  	<h1>Resultados</h1>
       	  </s:else>
       	  <h2><s:property value="nombre" /></h2>
          <div id="separacionTitulos"></div>
            <div class="textoGeneral">
            <p>
			<s:form key="form" action="%{actionMethod}" method="POST">
				
				<table class="wwFormTable">
					<tr>
						<td class="tdLabel">
							<label class="label" for="select">Torneo:</label>
						</td>
						<td class="">
						<s:hidden key="requestId" id="requestId" />
							<s:select id="select" label="Torneo" disabled="%{readOnly}"
									name="selectedTorneo" key="selectedTorneo" headerKey="0"
								   headerValue="Seleccione" list="torneos" 
								   listValue="nombre"
								   listKey="id" onchange="return doSubmit()" theme="simple"/>
						</td>
						<td>
							<s:if test="%{requestId != 0}"> 
								<s:url action="Estadisticas" var="url_torneos" includeParams="none">
			                    	<s:param name="requestId" value="%{id}" />
			                    	<s:param name="act" value="%{activo}" />
			           		    </s:url>
								<s:a href="%{url_torneos}" >
								
									<s:if test="%{act == true}">
										Estad&iacute;sticas
									</s:if>
									<s:else>
										Torneos Anteriores
									</s:else>
								
								</s:a>
							</s:if>
						</td>
					</tr>
				</table>
			   	<br/>
			   	
	             <s:iterator value="fechas" var="fecha" status="statusFecha">
		             <div class="cajaFixture">
		                 <div class="tituloFecha"><s:property value="#fecha.nombre" /></div>
		                 
		                 <s:sort comparator="#partidoComparator" source="#fecha.partidos">
		                 
		                 <s:iterator var="partido" status="statusPartido">
		                 
							 	<div class="cajaPartido <s:if test='#statusPartido.even == true'>even</s:if><s:else>odd</s:else>" >
		                     
		                     	<s:if test="!#partido.jugado && #partido.descripcion != null">
		                           
		                        	<div class="infoPartido fechaPartido"><s:date name="#partido.fechaPartido" format="dd/MM" /></div>
		                        	<div class="infoPartido separador">|</div>
		                        	<div class="infoPartido presentacion"><strong>PRESENTARSE: <s:date name="#partido.fechaPresentacion" format="HH:mm" /></strong></div>
		                        	<div class="infoPartido separador">|</div>
		                        	<div class="infoPartido inicioPartido">Inicio: <s:date name="#partido.fechaPartido" format="HH:mm" /></div>
		                        </s:if>
		                        <s:else>
		                        	<div class="infoPartido inicioPartido">-</div>
		                        	<div class="infoPartido inicioPartido">-</div>
		                        </s:else>
 							  
		                        <div class="cajaEquipo equipoLocal" align="right">
		                        	<s:property value="#partido.equipoLocal" />
		                        </div>
			                        
		                        <div class="cajaResultado">
		                        	<s:if test="#partido.jugado">
		                         		<s:property value="#partido.golesLocal" /> - <s:property value="#partido.golesVisitante" />
		                         	</s:if>
		                         	<s:else>
		                         		-
		                         	</s:else> 
		                         </div>
			                         
	                        	<div class="cajaEquipo equipoVisitante">
	                        		<s:property value="#partido.equipoVisitante" />
	                        	</div>
		                     
		                     </div>
		                 
		                 </s:iterator>
		                 
		                 </s:sort>
		                 
		                 <s:if test="#fecha.equipoLibre != null">
		                  <div align="center">
							Equipo Libre: <s:property value="#fecha.equipoLibre" />
						  </div>
						 </s:if>
		             </div>
		             
		             
	             </s:iterator>
              </s:form>	
			</p>
			</div>

</div>