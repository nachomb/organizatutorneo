<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>

<script type="text/javascript" language="JavaScript">
function doSubmit(){
    document.form.submit();
    return true;
}    
</script>

<s:form key="form" action="%{actionMethod}" method="POST">
    <s:hidden key="id" />
    <s:hidden key="requestId" />
    <s:hidden key="actionMethod" value="%{actionMethod}" />
    <s:actionerror />
    <br>
    <br>
    <table align="center" width="100%">
        <tr>
            <td><s:label id="id" name="id" key="id" label="Id" /></td>
        </tr>
        <tr>
            <td><s:textfield key="nombre" disabled="%{readOnly}" label="Nombre" maxlength="30" required="true" /></td>
        </tr>
        <tr>
            <td><s:textfield key="apellido" disabled="%{readOnly}" label="Apellido" maxlength="20" required="true" /></td>
        </tr>
        <tr>
            <td><s:textfield key="apodo" disabled="%{readOnly}" label="Apodo" maxlength="20" /></td>
        </tr>
        <tr>
            <td><sx:autocompleter list="equiposPosibles" key="selectedEquipo" label="Equipo" disabled="true" /></td>
        </tr>
        <tr>
            <s:if test="%{habilitarOKenabled}">
                <td><input class="button" type="button" onclick="return doSubmit()" value="OK"
                    style="float: right;" /></td>
            </s:if>
            <td><input class="button" type="button" onclick="location.href='MiJugador_miJugadorListado.action'" value="Cancelar" /></td>
        </tr>
    </table>
</s:form>