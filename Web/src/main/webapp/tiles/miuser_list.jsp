<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="js/efecto-hover-listados.js"></script>

<div align="center">
	<br>
		<h2>Mi Usuario</h2>
	<br>
</div>
<tbody>
    <s:form method="POST">
        <br>
        <display:table id="userGrid" name="entities" pagesize="10" requestURI="MiUsuario_miUsuarioListado.action"
            partialList="true" size="rowCount" excludedParams="*">
            <s:set name="myrow" value="#attr.userGrid" />
            <display:column property="id" title="Id" />
            <display:column property="username" title="Nombre" />
            <display:column title="Activo">
                <s:if test="#myrow.activo">
                    <img src="imagenes/check-ok-blue_15x15.png" alt="si" />
                </s:if>
            </display:column>
            <display:column title="Ver Detalle" href="MiUsuario_miUsuarioDetalle.action" paramId="requestId" paramProperty="id">
                    <img src="imagenes/editar.gif" alt="si"/>
            </display:column>
        </display:table>
    </s:form>
</tbody>