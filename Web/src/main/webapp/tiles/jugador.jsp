<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>

<script type="text/javascript">
function doSubmit(){
	document.form.submit();
	return true;
}

</script>

<div class="container">
<s:if test="id == null">
	<div align="center"><br>
	<h2>Jugador Nuevo</h2>
	<br>
	</div>
</s:if>
<s:else>
	<div align="center"><br>
	<h2>Editar Jugador</h2>
	</div>
</s:else>

<s:actionerror/>
<s:actionmessage/>
<s:fielderror/>

<s:form key="form" action="%{actionMethod}" method="POST" theme="simple" cssClass="form-horizontal">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror/>
	<br>
	<br>
<%-- 		<s:if test="id != null"> --%>
<!-- 			<tr> -->
<!-- 				<td> -->
<%-- 					<s:label id="id" name="id" key="id" label="Id"/> --%>
<!-- 				</td> -->
<!-- 			</tr> -->
<%-- 		</s:if> --%>
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="inputNombre">Nombre</label>
		   <div class="col-lg-10">
		      		<s:textfield key="nombre" disabled="%{readOnly}"
					label="Nombre" maxlength="30" required="true" cssClass="form-control"/>
		   </div>
		 </div>
		 <div class="form-group">
		   <label class="col-lg-2 control-label" for="inputApellido">Apellido</label>
		   <div class="col-lg-10">
		      		<s:textfield key="apellido" disabled="%{readOnly}"
					label="Apellido" maxlength="30" required="true" cssClass="form-control"/>
		   </div>
		 </div>
<!-- 		 <div class="form-group"> -->
<!-- 		   <label class="col-lg-2 control-label" for="inputDni">DNI</label> -->
<!-- 		   <div class="col-lg-10"> -->
<%-- 	      		<s:textfield key="dni" disabled="%{readOnly}" --%>
<%-- 				label="DNI" maxlength="30" required="true" cssClass="form-control"/> --%>
<!-- 		   </div> -->
<!-- 		 </div> -->
		 <div class="form-group">
		   <label class="col-lg-2 control-label" for="inputEquipo">Equipo</label>
		   <div class="col-lg-10">
<%-- 			     <sx:autocompleter list="equiposPosibles"  --%>
<%--                  key="selectedEquipo" label="Equipo" cssClass="form-control"  /> --%>
		 		<s:select id="selectedEquipo" key="selectedEquipo" theme="simple" listKey="id" listValue="nombre" 
			 			list="equiposPosibles" multiple="false" cssClass="form-control" value="%{equipo.id}" />
		   </div>
		 </div>
<!-- 		<tr> -->
<!-- 			<td> -->
<%-- 			<s:checkbox key="activo" disabled="%{readOnly}" label="Activo" /> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
		
		<!-- 
		<s:if test="id == null">
			<tr>
				<td>
				<s:checkbox key="crearUsuario" disabled="%{readOnly}" label="Crear Usuario" value="true"/>
				</td>
			</tr>
			<tr>
				<td><s:textfield key="telefonoCelular" disabled="%{readOnly}"
					label="Telefono celular" maxlength="20" javascriptTooltip="Sin guiones y sin espacios" /></td>
			</tr>
		</s:if>
		 -->
		<div class="form-group">
		   <div class="col-lg-offset-2 col-lg-10">
				<button type="button"
					onclick="return doSubmit()" class="btn btn-success">Guardar</button>
					<s:url action="Jugador_list" var="urlJugadorList" />
				<button type="button"
					onclick="window.location.href='<s:property value="urlJugadorList" />'" class="btn btn-danger">Cancelar</button>
		   </div>
	    </div>
</s:form>
</div>