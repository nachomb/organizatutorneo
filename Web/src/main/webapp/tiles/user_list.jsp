<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="js/efecto-hover-listados.js"></script>

<div align="center">
	<br>
		<h2>Usuarios</h2>
	<br>
</div>
<tbody>
<s:form method="POST">
	<br>
	<s:if test="%{!readOnly}">
		<table align="center" width="100%">
			<tr>
				<td>
					<div align="center">
						<s:a href="Usuario_add.action">Nuevo</s:a>
					</div>
				</td>
			</tr>
		</table>
		<display:table id="userGrid" name="entities" pagesize="10" requestURI="Usuario_list.action" partialList="true" size="rowCount" excludedParams="*">
	        <s:set name="myrow" value="#attr.userGrid"/>
	        <display:column property="id" title="Id" />
			<display:column property="username" title="Nombre" />
			<display:column title="Activo" >
				<s:if test="#myrow.activo">
					<img src="imagenes/check-ok-blue_15x15.png" alt="si"/>
				</s:if>
			</display:column>
		        <display:column title="Click para editar" href="Usuario_edit.action" paramId="requestId" paramProperty="id">
		            <img src="imagenes/editar.gif" alt="si"/>
		        </display:column>
		        <display:column title="Click para eliminar" href="Usuario_destroy.action" paramId="requestId" paramProperty="id">
		        	<s:if test="#myrow.activo">
		            	<img src="imagenes/borrar.gif" alt="si"/>
		            </s:if>
		        </display:column>
		</display:table>
	</s:if>
	<s:else>
		<display:table id="userGrid" name="entities" pagesize="10" requestURI="ConsultaUsuario.action" partialList="true" size="rowCount" excludedParams="*">
	        <s:set name="myrow" value="#attr.userGrid"/>
	        <display:column property="id" title="Id" />
			<display:column property="username" title="Nombre" />
			<display:column title="Activo" >
				<s:if test="#myrow.activo">
					<img src="imagenes/check-ok-blue_15x15.png" alt="si"/>
				</s:if>
			</display:column>
		</display:table>
	</s:else>
</s:form>
</tbody>