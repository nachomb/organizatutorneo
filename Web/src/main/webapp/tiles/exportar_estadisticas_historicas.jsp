<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>

<script type="text/javascript" src="/web/struts/optiontransferselect.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="css/jquery-ui-1.8.6.custom.css" />
<script type="text/javascript" src="js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" language="JavaScript">
function doSubmit(){
	document.form.submit();
	return true;
}
function seleccionarTodos(seleccionado) {
	
	var select = document.getElementById("selectedEquipos");
	var elems = select.getElementsByTagName("option");
	for (var i = 0; i < elems.length; ++i) {
		elems[i].selected = seleccionado;
	}

}
</script>



<div align="center"><br>
<h2>Excel Exporter</h2>
<br>
</div>
<s:form id="form" key="form" action="%{actionMethod}" method="POST">
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:hidden key="restriccionesSeleccionadas" />
	<s:actionerror/>
	<br>
	<br>
	<table align="center" width="100%">
	
		<tr>
			<td class="tdLabel"><a href="javascript:seleccionarTodos(true)">Seleccionar
			todos</a></td>
			<td><a href="javascript:seleccionarTodos(false)">Seleccionar
			ninguno</a></td>
		</tr>
		<tr id="equipos" class="equipos">
			<td class="tdLabel"><label class="label"> Equipos:</label></td>
			<td><s:select id="selectedEquipos" name="selectedEquipos"
				key="selectedEquipos" label="Equipos" disabled="%{readOnly}"
				list="equipos" listValue="nombre" listKey="id"
				multiple="true" value="%{equipos.{id}}" required="true" theme="simple"/>
			</td>
		</tr>
		
		<tr>
			<td><input class="button" type="button" onclick="return doSubmit()" value="OK" style="float:right;" /></td>
			<td><input  class="button" type="button" onclick="location.href='Torneo_list.action'" value="Cancelar" /></td>
		</tr>
	</table>
</s:form>