<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>

<script type="text/javascript" src="/web/struts/optiontransferselect.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="css/jquery-ui-1.8.6.custom.css" />
<script type="text/javascript" src="js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<sx:head />

<script type="text/javascript">
function doSubmit(){
	document.form.submit();
	return true;
}

function mostrarFechas() {
	dojo.event.topic.publish("mostrarFechas");
}
function mostrarPartidos() {
	dojo.event.topic.publish("mostrarPartidos");
}
function previsualizacion() {
	var restric = "";
	$('.restriccion').each(function(){
		restric += $(this).find('#tipoRestriccion option:selected').text() + ',';
		restric += $(this).find('#partidosRestriccion option:selected').val() + ',';
		restric += $(this).find('#operadorRestriccion option:selected').text() + ',';
		restric += $(this).find('#valorRestriccion').val() + ';';
	});
	$('#form_restriccionesSeleccionadas').val(restric);
	dojo.event.topic.publish("previsualizacion");
}
function cargarPartidosRestricciones(){
	$('.partidosRestriccion').each(function(i){
		$(this).html($('#partidosSeleccionados').html());
	});
}
function agregarRestriccion(){
	$('.restriccion:first').clone().insertAfter($('.restriccion:last'));
}
function eliminarRestriccion(){
	if($('.restriccion').size() != 1)
		$('.restriccion:last').remove();
}
$(function() {
	$('#fechaYHora').datetimepicker({

		timeOnlyTitle : 'Fecha y Hora',
		timeText : 'horario',
		hourText : 'hora',
		minuteText : 'minutos',
		secondText : 'segundos',
		currentText : 'ahora',
		closeText : 'cerrar',
		dateFormat : 'dd/mm/yy',
		timeFormat : 'hh:mm'
	});
});
</script>



<div align="center"><br>
<h2>Excel Exporter</h2>
<br>
</div>
<s:form id="form" key="form" action="%{actionMethod}" method="POST">
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:hidden key="restriccionesSeleccionadas" />
	<s:actionerror/>
	<br>
	<br>
	<table align="center" width="100%">

		<tr>
			<td><s:select label="Torneos" disabled="%{readOnly}" id="torneosSeleccionados"
				key="torneosSeleccionados" listKey="id" listValue="nombre"
				 required="true" list="torneosPosibles" onchange="javascript:mostrarFechas();return false;" 
				 multiple="true"/></td>
		</tr>
		
		<s:url id="url_fechas" action="ProgramacionPartidos_mostrar_fechas" />
		<tr>
			<td class="tdLabel"><label class="label"> Fechas:</label></td>
			<td><sx:div theme="simple" id="details" href="%{url_fechas}" disabled="%{readOnly}"
				listenTopics="mostrarFechas" formId="form" showLoadingText="cargando fechas"></sx:div></td>
		</tr>
		
		<s:url id="url_partidos" action="ProgramacionPartidos_mostrar_partidos" />
		<tr>
			<td class="tdLabel"><label class="label"> Partidos:</label></td>
			<td><sx:div theme="simple" id="details" href="%{url_partidos}" disabled="%{readOnly}"
				listenTopics="mostrarPartidos" formId="form" showLoadingText="cargando partidos"></sx:div></td>
		</tr>
		
		<tr>
			<td><input class="button" type="button" onclick="return doSubmit()" value="OK" style="float:right;" /></td>
			<td><input  class="button" type="button" onclick="location.href='Torneo_list.action'" value="Cancelar" /></td>
		</tr>
	</table>
</s:form>