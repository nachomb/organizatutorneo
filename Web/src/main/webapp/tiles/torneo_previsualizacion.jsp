<%@ taglib prefix="s" uri="/struts-tags"%>		            

<s:iterator value="previsualizacionFechas" var="fecha" status="statusFecha">
 <div class="cajaFixture">
     <div class="tituloFecha"><s:property value="#fecha.nombre" /></div>
     
     <s:iterator value="#fecha.partidos" var="partido" status="statusPartido">
          
			<div class="cajaPartido <s:if test='#statusPartido.even == true'>even</s:if><s:else>odd</s:else>" >
            	<s:property value="#partido.equipoLocal" />
				vs
           		<s:property value="#partido.equipoVisitante" />
           	</div>
         
         </div>
     
     </s:iterator>
     
     <s:if test="#fecha.equipoLibre != null">
             <div align="center">
		Equipo Libre: <s:property value="#fecha.equipoLibre" />
	</div>
	</s:if>
	</div>
</s:iterator>

