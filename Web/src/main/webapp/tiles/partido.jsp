<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<link rel="stylesheet" media="all" type="text/css" href="css/bootstrap-datetimepicker.min.css" />
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" >
function doSubmit(){
	if(cambioEquipos()){
		document.form.submit();
		return true;
	}
	if(!cargarFigura()){
		alert("Debe elegir la figura del partido");
		return false;
	}
	cargarInputHidden("puntajesLocales");
	cargarInputHidden("puntajesVisitantes");
	cargarMapHidden("numerosLocales");
	cargarMapHidden("numerosVisitantes");
	cargarMapHidden("golesLocales");
	cargarMapHidden("golesVisitantes");
	cargarMapHidden("amarillasLocales");
	cargarMapHidden("amarillasVisitantes");
	cargarMapHidden("rojasLocales");
	cargarMapHidden("rojasVisitantes");
	cargarLocales();
	cargarVisitantes();
	document.form.submit();
	return true;
}

function cargarInputHidden(id){
	var selector = 'input[id="' + id + '"]';
	var input = document.getElementById("Partido_cargar_" + id);
	input.value = '';
	$(selector).each(function(index){
		var value = ($(this).val()=='')? '0':$(this).val();
		input.value += value + ',';
	});
	if(input.value){
		input.value = input.value.slice(0, -1);	
	}
}

function cargarMapHidden(id){
	var selector = 'input[id="' + id + '"]';
	var input = document.getElementById("Partido_cargar_" + id);
	input.value = '';
	$(selector).each(function(index){
		var value = ($(this).val()=='')? '0':$(this).val();
		if(value != '0'){
			var key = $(this).prev().val();
			input.value +=  key + ',' +  value + ',';
		}

	});
	if(input.value){
		input.value = input.value.slice(0, -1);	
	}
}

function cargarFigura(){
	var figura = document.getElementById("Partido_cargar_figura");
	figura.value = '';
	var fig = false;
	$('input[name=radioFigura]:checked').each(function(index){
		figura.value = $(this).attr('id');
		fig = true;
	});
	return fig;
}

function cargarLocales(){
	var selectTitularesLocales = document.getElementById("Partido_cargar_selectedTitularesLocales");
	selectTitularesLocales.value = '';
	$('input[id=titularLocal]:checked').each(function(index){
		selectTitularesLocales.value += $(this).attr('name') + ',';
	});
	if(selectTitularesLocales.value){
		selectTitularesLocales.value = selectTitularesLocales.value.slice(0, -1);	
	}
	var selectSuplentesLocales = document.getElementById("Partido_cargar_selectedSuplentesLocales");
	selectSuplentesLocales.value = '';
	$('input[id=suplenteLocal]:checked').each(function(index){
		selectSuplentesLocales.value += $(this).attr('name') + ',';
	});
	if(selectSuplentesLocales.value){
		selectSuplentesLocales.value = selectSuplentesLocales.value.slice(0, -1);	
	}
	
	
}

function cargarVisitantes(){
	var selectTitularesVisitantes = document.getElementById("Partido_cargar_selectedTitularesVisitantes");
	selectTitularesVisitantes.value = '';
	$('input[id=titularVisitante]:checked').each(function(index){
		selectTitularesVisitantes.value += $(this).attr('name') + ',';
	});
	if(selectTitularesVisitantes.value){
		selectTitularesVisitantes.value = selectTitularesVisitantes.value.slice(0, -1);	
	}
	var selectSuplentesVisitantes = document.getElementById("Partido_cargar_selectedSuplentesVisitantes");
	selectSuplentesVisitantes.value = '';
	$('input[id=suplenteVisitante]:checked').each(function(index){
		selectSuplentesVisitantes.value += $(this).attr('name') + ',';
	});
	if(selectSuplentesVisitantes.value){
		selectSuplentesVisitantes.value = selectSuplentesVisitantes.value.slice(0, -1);	
	}
}

$(document).ready(function(){
    $('input[id="numerosLocales"]').change(function() {
    	var clave = $(this).prev().val();
    	var numero = $(this).val();
    	var titLocal = $('input[id="titularLocal"][name="'+clave+'"]');
    	var noJug = $('input[id="noJugoLocal"][name="'+clave+'"]');
    	if(!numero){
    		titLocal.removeAttr('checked');
    		noJug.attr('checked', 'checked');
    	}else{
        	noJug.removeAttr('checked');
        	titLocal.attr('checked', 'checked');
    	}
    });
    $('input[id="numerosVisitantes"]').change(function() {
    	var clave = $(this).prev().val();
    	var numero = $(this).val();
    	var titLocal = $('input[id="titularVisitante"][name="'+clave+'"]');
    	var noJug = $('input[id="noJugoVisitante"][name="'+clave+'"]');
    	if(!numero){
    		titLocal.removeAttr('checked');
    		noJug.attr('checked', 'checked');
    	}else{
        	noJug.removeAttr('checked');
        	titLocal.attr('checked', 'checked');
    	}

    });
    
    setFunctionChangeGoles("golesLocales", "Partido_cargar_golesLocal");
    setFunctionChangeGoles("golesVisitantes", "Partido_cargar_golesVisitante");
    
    //setTimeout(setEquipos,1000);
    

});
var equipoLocal;
var equipoVisitante;
function setEquipos(){
	equipoLocal = $('input.dojoComboBox.noMultiple').first().val();
	equipoVisitante = $('input.dojoComboBox.noMultiple').last().val();
}

function cambioEquipos(){
	return (equipoLocal != $('input.dojoComboBox.noMultiple').first().val()) || (equipoVisitante != $('input.dojoComboBox.noMultiple').last().val())
	
}

function setFunctionChangeGoles(id, idAModificar){
    $('input#'+id).change(function() {
    	var cantGoles = 0;
    	$('input#'+id).each(function(index){
    		var value = ($(this).val()=='')? 0: parseInt($(this).val());
    		cantGoles += value;
    	});
    	$('input#'+idAModificar).val(cantGoles);
    });
	
}

function mostrarLeyenda(){
    $('#divLeyenda').show();
}
function ocultarLeyenda(){
    $('#divLeyenda').hide();
}
$(function() {
	var offset = 0;
	
	offset = setTabindex("numerosLocales", offset);
	offset += setTabindex("numerosVisitantes", offset);
	offset += setTabindex("golesLocales", offset);
	offset += setTabindex("golesVisitantes", offset);
	
});
function setTabindex(id, offset){
	$('input#'+id).each(function(index) {
		$(this).attr('tabindex',offset+index+1);
	});
	return $('input#'+id).length;
}

</script>

<div class="container">
<h2>Partido</h2>
<s:form key="form" action="%{actionMethod}" method="POST" theme="simple" cssClass="form-horizontal">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:hidden key="selectedTitularesLocales" />
	<s:hidden key="selectedTitularesVisitantes" />
	<s:hidden key="selectedSuplentesLocales" />
	<s:hidden key="selectedSuplentesVisitantes" />
	<s:hidden key="puntajesLocales" />
	<s:hidden key="puntajesVisitantes" />
	<s:hidden key="figura" />
	<s:hidden key="golesLocales" />
	<s:hidden key="golesVisitantes" />
	<s:hidden key="amarillasLocales" />
	<s:hidden key="amarillasVisitantes" />
	<s:hidden key="rojasLocales" />
	<s:hidden key="rojasVisitantes" />
	<s:hidden key="numerosLocales" />
	<s:hidden key="numerosVisitantes" />
	<s:actionerror />
	<br>
	<br>
		<s:if test="%{!readOnly}">
		<div class="form-group">
		 	<label class="col-lg-2 control-label" for="fechaYHora">Fecha y hora del partido</label>
	        <div class="input-group date" id="datetimepicker">
	            <s:textfield disabled="%{readOnly}"
				name="fechaYHora" id="fechaYHora" required="true" theme="simple" cssClass="form-control"/>
	            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
	            </span>
	        </div>
        </div>
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="descripcion">Descripcion</label>
		   <div class="col-lg-10">
			   <s:textfield key="descripcion" disabled="%{readOnly}" required="true" cssClass="form-control" theme="simple"/>
		   </div>
		 </div>
		<div>
			<div class="row partido">
				<label class="col-lg-2 control-label" for="cantCanchas"></label>
			  <div class="col-xs-2">
				<s:select id="equipoLocal" value="%{equipoLocal.id}" list="%{fecha.torneo.equipos}" theme="simple"  listKey="id" listValue="nombre" cssClass="form-control" multiple="false" />
			  </div>
			  <div class="col-xs-1">
				<s:textfield key="golesLocal" theme="simple" size="3" maxlength="3" disabled="%{readOnly}" cssClass="form-control" />
			  </div>
			  <div class="col-xs-1">
				<s:textfield key="golesVisitante" theme="simple" size="3" maxlength="3" disabled="%{readOnly}" cssClass="form-control"/>
			  </div>
			  <div class="col-xs-2">
				<s:select id="equipoVisitante" value="%{equipoVisitante.id}" list="%{fecha.torneo.equipos}" theme="simple" listKey="id" listValue="nombre" cssClass="form-control" multiple="false"/>
			  </div>
			  <div class="col-xs-2">
			  	<label class="col-lg-2 control-label" for="jugado">Jugado</label><s:checkbox key="jugado" theme="simple" disabled="%{readOnly}" cssClass="form-control"/>
			  </div>
			</div>
		</div>
		
		<br>
				<display:table class="table table-hover table-striped" id="jugadorGrid" name="equipoLocal.jugadores" defaultsort="1" defaultorder="descending">
					<s:set name="myrow" value="#attr.jugadorGrid"/>
					<display:column property="activo" class="invisible" headerClass="invisible" />
					<display:column title="N" >
			        	<s:hidden key="#myrow.id" id="clave" />
			        	<s:textfield id="numerosLocales" theme="simple" disabled="%{readOnly}"  key="#myrow.numero" cssStyle="width: 97%;"/>
		        	</display:column>
					<display:column property="nombre" title="Nombre" class="col-xs-2" />
					<display:column property="apellido" title="Apellido" class="col-xs-2" />
					<display:column title="T/S/NJ" class="col-xs-1">
                        <s:if test="#myrow.estaSuspendido">
                            <span>Suspendido</span>
                        </s:if>
                        <s:else>
						<s:if test="#myrow.esTitular" >
							<input type="radio" id="titularLocal" name="<s:property value="#myrow.id"/>" <s:if test="%{readOnly}">disabled</s:if> checked />
						</s:if>
						<s:else>
							<input type="radio" id="titularLocal" name="<s:property value="#myrow.id"/>" <s:if test="%{readOnly}">disabled</s:if> />
						</s:else>
						<s:if test="#myrow.esSuplente" >
							<input type="radio" id="suplenteLocal" name="<s:property value="#myrow.id"/>" <s:if test="%{readOnly}">disabled</s:if> checked />
						</s:if>
						<s:else>
							<input type="radio" id="suplenteLocal" name="<s:property value="#myrow.id"/>" <s:if test="%{readOnly}">disabled</s:if> />
						</s:else>
						<s:if test="!#myrow.esTitular && !#myrow.esSuplente" >
							<input type="radio" id="noJugoLocal" name="<s:property value="#myrow.id"/>" <s:if test="%{readOnly}">disabled</s:if> checked/>
						</s:if>
						<s:else>
							<input type="radio" id="noJugoLocal" name="<s:property value="#myrow.id"/>" <s:if test="%{readOnly}">disabled</s:if> />
						</s:else>
                        </s:else>
                        
					</display:column>
					<display:column title="Fig" class="ancho-20">
						<s:if test="#myrow.esFigura" >
			        		<input type="radio" id="<s:property value="#myrow.id"/>" name="radioFigura" <s:if test="%{readOnly}">disabled</s:if> checked/>
			        	</s:if>
						<s:else>
							<input type="radio" id="<s:property value="#myrow.id"/>" name="radioFigura" <s:if test="%{readOnly}">disabled</s:if> />
						</s:else>
		        	</display:column>
		        	<display:column title="G" class="ancho-20">
			        	<s:hidden key="#myrow.id" id="clave" />
			        	<s:textfield id="golesLocales" theme="simple" disabled="%{readOnly}" cssStyle="width: 97%;" key="#myrow.cantGolesPartido"/>
		        	</display:column>
		        	<display:column title="A" class="ancho-20">
			        	<s:hidden key="#myrow.id" id="clave" />
			        	<s:textfield id="amarillasLocales" theme="simple" disabled="%{readOnly}" cssStyle="width: 97%;" key="#myrow.cantAmarillasPartido"/>
		        	</display:column>
		        	<display:column title="R" class="ancho-20">
			        	<s:hidden key="#myrow.id" id="clave" />
			        	<s:textfield id="rojasLocales" theme="simple" disabled="%{readOnly}" cssStyle="width: 97%;" key="#myrow.cantRojasPartido"/>
		        	</display:column>
					<display:column title="Puntaje" class="ancho-20">
			        	<s:hidden key="#myrow.id" id="clave" />
			        	<s:textfield id="puntajesLocales" theme="simple" disabled="%{readOnly}" cssStyle="width: 97%;" key="#myrow.puntajePartido"/>
		        	</display:column>
				</display:table>
                 <br>   
                    <display:table  class="table table-hover table-striped" id="jugadorGrid" name="equipoVisitante.jugadores" defaultsort="1" defaultorder="descending">
						<s:set name="myrow" value="#attr.jugadorGrid"/>
						<display:column property="activo" class="invisible" headerClass="invisible" />
						<display:column title="N" >
				        	<s:hidden key="#myrow.id" id="clave" />
				        	<s:textfield id="numerosVisitantes" theme="simple" disabled="%{readOnly}" key="#myrow.numero" cssStyle="width: 97%;"/>
		        		</display:column>
						<display:column property="nombre" title="Nombre" class="col-xs-2"/>
						<display:column property="apellido" title="Apellido" class="col-xs-2"/>
						<display:column title="T/S/NJ" class="col-xs-1">
                          <s:if test="#myrow.estaSuspendido">
                            <span>Suspendido</span>
                        </s:if>
                        <s:else>
						<s:if test="#myrow.esTitular" >
							<input type="radio" id="titularVisitante" name="<s:property value="#myrow.id"/>"<s:if test="%{readOnly}">disabled</s:if> checked/>
						</s:if>
						<s:else>
							<input type="radio" id="titularVisitante" name="<s:property value="#myrow.id"/>" <s:if test="%{readOnly}">disabled</s:if> />
						</s:else>
						<s:if test="#myrow.esSuplente" >
							<input type="radio" id="suplenteVisitante" name="<s:property value="#myrow.id"/>" <s:if test="%{readOnly}">disabled</s:if> checked />
						</s:if>
						<s:else>
							<input type="radio" id="suplenteVisitante" name="<s:property value="#myrow.id"/>" <s:if test="%{readOnly}">disabled</s:if> />
						</s:else>						
						<s:if test="!#myrow.esTitular && !#myrow.esSuplente" >	
							<input type="radio" id="noJugoVisitante" name="<s:property value="#myrow.id"/>" <s:if test="%{readOnly}">disabled</s:if> checked />
						</s:if>
						<s:else>
							<input type="radio" id="noJugoVisitante" name="<s:property value="#myrow.id"/>" <s:if test="%{readOnly}">disabled</s:if> />
						</s:else>
                        </s:else>
						</display:column>
						
						<display:column title="Fig" class="ancho-20">
						<s:if test="#myrow.esFigura" >
			        		<input type="radio" id="<s:property value="#myrow.id"/>" name="radioFigura" <s:if test="%{readOnly}">disabled</s:if> checked/>
			        	</s:if>
						<s:else>
							<input type="radio" id="<s:property value="#myrow.id"/>" name="radioFigura" <s:if test="%{readOnly}">disabled</s:if> />
						</s:else>
		        		</display:column>
		        		<display:column title="G" class="ancho-20">
			        		<s:hidden key="#myrow.id" id="clave" />
			        		<s:textfield id="golesVisitantes" theme="simple" disabled="%{readOnly}" cssStyle="width: 97%;" key="#myrow.cantGolesPartido"/>
		        		</display:column>
		        		<display:column title="A" class="ancho-20">
				        	<s:hidden key="#myrow.id" id="clave" />
				        	<s:textfield id="amarillasVisitantes" theme="simple" disabled="%{readOnly}" cssStyle="width: 97%;" key="#myrow.cantAmarillasPartido"/>
			        	</display:column>
			        	<display:column title="R" class="ancho-20">
				        	<s:hidden key="#myrow.id" id="clave" />
				        	<s:textfield id="rojasVisitantes" theme="simple" disabled="%{readOnly}" cssStyle="width: 97%;" key="#myrow.cantRojasPartido"/>
			        	</display:column>
						<display:column title="Puntaje" class="ancho-20">
				        	<s:hidden key="#myrow.id" id="clave" />
				        	<s:textfield id="puntajesVisitantes" theme="simple" disabled="%{readOnly}" cssStyle="width: 97%;" key="#myrow.puntajePartido" />
		        		</display:column>
					</display:table>
        </s:if>
        <s:else>
        		<!--read only  -->
        		
        <tr>
			<td class="tdLabel">
            <s:label key="equipoLocal" disabled="%{readOnly}" theme="simple" />
			<s:textfield key="golesLocal" disabled="%{readOnly}" theme="simple" maxlength="3" size="3" />
            <span> - </span>
            <s:textfield key="golesVisitante" disabled="%{readOnly}" theme="simple" maxlength="3" size="3" />
            <s:label key="equipoVisitante" disabled="%{readOnly}" theme="simple" />
			</td>
		</tr>
        <tr>
            <td>
        			<display:table id="jugadorGrid" name="alineacionLocal.titulares" >
						<s:set name="myrow" value="#attr.jugadorGrid"/>
						<display:column title="Jugadoras" >
							<s:property value="#myrow" />
						</display:column>
	                    <display:column property="cantGolesPartido" title="G" />
	                    <display:column property="cantAmarillasPartido" title="A" />
	                    <display:column property="cantRojasPartido" title="R" />
	                    <display:column title="Figura">
	                    	<s:if test="#myrow.esFigura">
	                    		figura
	                    	</s:if>
	                    </display:column>
        			</display:table>
     			</td>
     			<td>
        			<display:table id="jugadorGrid" name="alineacionVisitante.titulares" >
						<s:set name="myrow" value="#attr.jugadorGrid"/>
						<display:column title="Jugadoras" >
							<s:property value="#myrow" />
						</display:column>
	                    <display:column property="cantGolesPartido" title="Goles" />
	                    <display:column property="cantAmarillasPartido" title="A" />
	                    <display:column property="cantRojasPartido" title="R" />
	                    <display:column title="Figura">
	                    	<s:if test="#myrow.esFigura">
	                    		figura
	                    	</s:if>
	                    </display:column>
        			</display:table>
                </td>
            </tr>
        
        </s:else>
		<tr>
            <s:if test="%{!readOnly}">
            	<div class="form-group">
				   <div class="col-lg-offset-2 col-lg-10">
						<button type="button" onclick="return doSubmit()" class="btn btn-success">Guardar</button>
		                <s:url value="Fecha_edit.action" var="urlFechaEdit">
		                	<s:param name="requestId" value="%{fecha.id}" />
		                </s:url>
						<button type="button" onclick="location.href='<s:property value="%{urlFechaEdit}" />'" class="btn btn-danger">Cancelar</button>
				   </div>
			    </div>
            </s:if>
            <s:else>
				<td><input  class="button" type="button" onclick="history.back()" value="Volver" style="float: right;" /></td>
            </s:else>
		</tr>

</s:form>
</div>
<script type="text/javascript">
 $('#datetimepicker').datetimepicker({
     language: 'es'
 });
</script>