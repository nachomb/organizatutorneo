<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript">
function doSubmit(){
	document.form.submit();
	return true;
}
</script>
<div class="container">
<s:if test="id == null">
	<div align="center"><br>
	<h2>Sede Nueva</h2>
	<br>
	</div>
</s:if>
<s:else>
	<div align="center"><br>
	<h2>Editar Sede</h2>
	</div>
</s:else>
<s:form theme="simple" cssClass="form-horizontal" key="form" action="%{actionMethod}" method="POST" enctype="multipart/form-data">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror/>
	<br>
	<br>
<%-- 		<s:if test="id != null"> --%>
<!-- 			<tr> -->
<!-- 				<td> -->
<%-- 					<s:label id="id" name="id" key="id" label="Id"/> --%>
<!-- 				</td> -->
<!-- 			</tr> -->
<%-- 		</s:if> --%>
		<div class="form-group">
		   <label class="col-lg-2 control-label" for="inputNombre">Nombre</label>
		   <div class="col-lg-10">
		      		<s:textfield key="nombre" disabled="%{readOnly}"
					label="Nombre" maxlength="30" required="true" cssClass="form-control"/>
		   </div>
		 </div>
<!-- 		<tr> -->
<!-- 			<td> -->
<%-- 			<s:checkbox key="activo" disabled="%{readOnly}" label="activo" /> --%>
<!-- 			</td> -->
<!-- 		</tr> -->

		<div class="form-group">
	       <label class="col-lg-2 control-label" for="buttons"></label>
		   <div class="col-lg-10">
				<button type="button"
					onclick="return doSubmit()" class="btn btn-success">Guardar</button>
				<s:url action="Sede_list" var="urlSedeList" />
				<button type="button"
					onclick="window.location.href='<s:property value="urlSedeList"/>'" class="btn btn-danger">Cancelar</button>
		   </div>
	    </div>
</s:form>
</div>