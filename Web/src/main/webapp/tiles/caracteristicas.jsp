<div class="container">
	<div class="well">
		<h2>Caracter&iacute;sticas de la Aplicaci&oacute;n</h2>
	</div>
      <div class="panel panel-info">
		  <div class="panel-heading">
		    <h3 class="panel-title">Organizador del Torneo</h3>
		  </div>
		  <div class="panel-body">
			<ul class="list-group">
				<li class="list-group-item">Importar <b>Jugadores y Equipos</b></li>
				<li class="list-group-item">Crear <b>Torneos</b></li>
				<li class="list-group-item">Generar el <b>Fixture</b></li>
				<li class="list-group-item">Cargar <b>Resultados</b> de los Partidos (Goles, figura, puntaje, tarjetas amarillas y rojas)</li>
				<li class="list-group-item">Calcular el <b>Horario</b> de los Partidos</li>
			</ul>
	      </div>
	  	</div>
	  	<div class="panel panel-info">
		  <div class="panel-heading">
		    <h3 class="panel-title">Jugadores</h3>
		  </div>
		  <div class="panel-body">
			<ul class="list-group">
				<li class="list-group-item">Consultar las <b>Tablas de Posiciones, Goleadores y Tarjetas</b></li>
				<li class="list-group-item">Consultar el <b>Fixture</b></li>
				<li class="list-group-item">Consultar los <b>Resultados de los Partidos</b></li>
				<li class="list-group-item">Consultar el horario de los <b>pr&oacute;ximos Partidos</b></li>
			</ul>
	      </div>
	  	</div>
</div>