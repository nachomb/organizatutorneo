<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" language="JavaScript">
	function doSubmit() {
		document.form.submit();
		return true;
	}
</script>


<div align="center"><br>
<h2>Historial</h2>
<br>
</div>

<s:form key="form" action="%{actionMethod}" method="POST">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
	<br>
	<br>
	<s:action name="Torneo_calcularMaxGoles"/>
	<img src="https://chart.googleapis.com/chart?cht=p3&chs=800x250&chd=t:<s:property value="%{datosGrafico}"/>&chl=<s:property value="%{leyendaGrafico}"/>&chdlp=b" />
		<tr>
			<td><input class="button" type="button"
				onclick="history.back()" value="Volver" /></td>
		</tr>
</s:form>
