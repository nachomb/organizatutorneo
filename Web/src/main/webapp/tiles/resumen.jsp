<%@ taglib prefix="s" uri="/struts-tags"%>

<link rel="stylesheet" media="all" type="text/css"
	href="css/jquery-ui-1.8.6.custom.css" />
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript" language="JavaScript">
function doSubmit(){
	document.form.submit();
	return true;
}
$(function() {
	$('#fechaYHora').datetimepicker({

		timeOnlyTitle : 'Fecha y Hora',
		timeText : 'horario',
		hourText : 'hora',
		minuteText : 'minutos',
		secondText : 'segundos',
		currentText : 'ahora',
		closeText : 'cerrar',
		dateFormat : 'dd/mm/yy',
		timeFormat : 'hh:mm'
	});
});
</script>
<s:if test="id == null">
	<div align="center"><br>
	<h2>Resumen Nueva</h2>
	<br>
	</div>
</s:if>
<s:form key="form" action="%{actionMethod}" method="POST" enctype="multipart/form-data">
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror/>
	<br>
	<br>
	<table align="center" width="100%">
		<s:if test="id != null">
			<tr>
				<td>
					<s:label id="id" name="id" key="id" label="Id"/>
				</td>
			</tr>
		</s:if>
		<tr>
			<td><s:textfield key="nombre" disabled="%{readOnly}"
				label="Nombre" maxlength="30" required="true"/></td>
		</tr>
		
		<tr>
			<td class="tdLabel">Fecha:</td>
			<td><s:textfield name="fechaYHora" id="fechaYHora" theme="simple" disabled="%{readOnly}"/></td>

		</tr>
		
		<s:if test="tapa != null">
			<tr>
				<td class="tdLabel"><label class="label">Tapa Actual:</label>
				</td>
				<td><img src="<s:url value="%{tapa}" />" id="tapa"/>
				</td>
				
			</tr>
			<tr>
				<td><s:file name="uploadTapa" label="Cambiar Tapa"/>
				</td>
				
			</tr>
		</s:if>
		<s:else>
			<tr>
				<td><s:file name="uploadTapa" label="Tapa"/>
				</td>
				
			</tr>
		</s:else>
		
		<tr>
			<td><s:textfield key="link" disabled="%{readOnly}"
				label="Link" maxlength="70"/></td>
		</tr>
		
		<tr>
			<td>
			<s:checkbox key="activo" disabled="%{readOnly}" label="Activo" />
			</td>
		</tr>
		
		<tr>
			<s:if test="%{habilitarOKenabled}">
				<td><input class="button" type="button" onclick="return doSubmit()" value="OK" style="float:right;" /></td>
			</s:if>
			<td><input  class="button" type="button" onclick="location.href='Resumen_list.action'" value="Cancelar" /></td>
		</tr>
	</table>
</s:form>
