<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<script type="text/javascript">

$(function() {
	if(localStorage){
		if(localStorage.getItem('activeTab') === null){
			setActiveTab('posiciones');
		}
		var selector = "#" + localStorage.getItem('activeTab');
		$('ul.nav').children('li').attr('class','');
		$('div.tab-pane').removeClass('active');
		$('li'+selector+'2').addClass('active');
		$('div'+selector).addClass('active');
		
	}
});

function setActiveTab(param){
	if(localStorage){
		localStorage.setItem('activeTab', param);
	}
}

</script>

<div class="container">
<br>
<h2>Torneo - <s:property value="%{nombre}" /></h2>
<br>
	<!-- Nav tabs -->
	<ul class="nav nav-tabs">
	<!-- 	<ul class="nav nav-pills"> -->
	  <li class="active" id="posiciones2"><a href="#posiciones" data-toggle="tab" onclick="setActiveTab('posiciones')">Posiciones</a></li>
	  <li id="goleadores2"><a href="#goleadores" data-toggle="tab" onclick="setActiveTab('goleadores')">Goleadores</a></li>
	  <li id="tarjetas2"><a href="#tarjetas" data-toggle="tab" onclick="setActiveTab('tarjetas')">Tarjetas</a></li>
	  <li id="sancionados2"><a href="#sancionados" data-toggle="tab" onclick="setActiveTab('sancionados')">Sancionados</a></li>
	  <li id="fixture2"><a href="#fixture" data-toggle="tab" onclick="setActiveTab('fixture')">Fixture/Resultados</a></li>
	</ul>
	
	<!-- Tab panes -->
	<div class="tab-content">
	  <div class="tab-pane active" id="posiciones">
			<display:table class="table table-hover table-striped" id="tablaPosicionesGrid" name="posiciones" sort="list" requestURI="DetalleTorneo.action"
				defaultsort="10" defaultorder="descending">
				<display:column title="N"><%=pageContext.getAttribute("tablaPosicionesGrid_rowNum")%></display:column>
				<display:column title="Equipo" property="equipo.nombre"
					sortable="true">
				</display:column>
				<display:column title="PJ" property="partidosJugados"
					sortable="true">
				</display:column>
				<display:column title="PG" property="partidosGanados"
					sortable="true">
				</display:column>
				<display:column title="PE" property="partidosEmpatados"
					sortable="true">
				</display:column>
				<display:column title="PP" property="partidosPerdidos"
					sortable="true">
				</display:column>
				<display:column title="GF" property="golesAFavor" sortable="true">
				</display:column>
				<display:column title="GC" property="golesEnContra" sortable="true">
				</display:column>
				<display:column title="Dif" property="diferencia"  sortable="true">
				</display:column>
				<display:column title="Pts" property="puntos" sortable="true" >
				</display:column>
				<display:column title="%" property="porcentajePuntos" sortable="true">
				</display:column>
			</display:table>
	  
	  </div>
	  <div class="tab-pane" id="goleadores">
				<display:table class="table table-hover table-striped" uid="tablaGolesGrid" name="tablaGoleadores"
				sort="list" requestURI="DetalleTorneo.action"
				defaultsort="4" defaultorder="descending" pagesize="10" >
				<s:set var="myrow" value="#attr.tablaGolesGrid"/>
				<display:column title="N"><%=pageContext.getAttribute("tablaGolesGrid_rowNum")%></display:column>
				<display:column title="Jugador" property="jugador"
					defaultorder="ascending" sortable="true">
				</display:column>
				<display:column title="Equipo" property="jugador.equipo.nombre"
					sortable="true">
				</display:column>
				<display:column title="Goles" property="goles"
					defaultorder="descending"  sortable="true">
				</display:column>
			</display:table>
	  </div>
	  <div class="tab-pane" id="tarjetas">
			<display:table class="table table-hover table-striped" uid="tablaTarjetasGrid" name="jugadoresTarjetas"
				sort="list" requestURI="DetalleTorneo.action"
				defaultsort="10" defaultorder="descending" pagesize="10">
				<s:set var="myrow" value="#attr.tablaTarjetasGrid"/>
				<display:column title="N"><%=pageContext.getAttribute("tablaTarjetasGrid_rowNum")%></display:column>
				<display:column property="jugador" title="Jugador"	defaultorder="ascending" sortable="true"/>
				<display:column title="Equipo" property="jugador.equipo.nombre" sortable="true">
				</display:column>
				<display:column title="Cant. Amarillas" property="cantTarjetasAmarillas"
					sortable="true">
				</display:column>
				<display:column title="Cant. Rojas" property="cantTarjetasRojas"
					sortable="true">
				</display:column>
				<display:column title="Total" property="cantTarjetasTotal"
					sortable="true">
				</display:column>
			</display:table>
	  </div>
	  <div class="tab-pane" id="sancionados">
				<display:table class="table table-hover table-striped" uid="tablaSancionesGrid" name="sanciones"
				sort="list" requestURI="DetalleTorneo.action" 
				defaultsort="4" defaultorder="descending">
				<s:set name="myrow" value="#attr.tablaSancionesGrid"/>
				<display:column title="N" ><%=pageContext.getAttribute("tablaSancionesGrid_rowNum")%></display:column>
				<display:column title="Jugador"	property="jugadorSancionado.nombreYApellido" defaultorder="ascending"  sortable="true"/>
				<display:column title="Equipo"	property="jugadorSancionado.equipo" sortable="true"/>
				<display:column title="Cant. Fechas" property="cantidadFechas"  sortable="true"/>
				<display:column title="Fecha Inicio" property="fechaInicio.nombre" sortable="true"/>
				<display:column title="Cumplida" sortable="true">
		         	<s:if test="#myrow.cumplida">
		            	SI
		            </s:if>
		            <s:else>
		            	NO
		            </s:else>
	        	</display:column>
	        </display:table>
	  </div>
	  <div class="tab-pane" id="fixture">
	  		<s:iterator value="fechas" var="fecha">
	                    <div class="panel panel-info">
						  <div class="panel-heading">
						    <h3 class="panel-title"><s:property value="#fecha.numero" /> - <s:property value="#fecha.nombre" /></h3>
						  </div>
						  <div class="panel-body">
	                        	<div align="center">
		                        <ul class="list-group">
								    <s:iterator value="#fecha.partidos" var="partido">
								  			<li class="list-group-item">
								  				<a onblur=""><s:property value="#partido.equipoLocal" /> <span class="badge"><s:property value="#partido.golesLocal" /></span> vs <span class="badge"><s:property value="#partido.golesVisitante" /></span> <s:property value="#partido.equipoVisitante" /></a>
								  				<span class="label label-default"><s:date name="#partido.fechaPartido" format="dd/MM HH:mm" /></span>
								  				<span class="label label-default"><s:property value="#partido.descripcion" /></span>
								  			</li>
			                        </s:iterator>
								</ul>
		                        </div>
						  </div>
						  <s:if test="#fecha.equipoLibre != null">
		                        <div align="center" class="panel-footer">
										Equipo Libre: <s:property value="#fecha.equipoLibre" />
								</div>
						   </s:if>
						</div>
                    </s:iterator>
	  </div>
	</div>
	
	<s:url action="ConsultaTorneo" var="urlConsultaTorneo">
		<s:param name="uid" value="creador.id"/>
	</s:url>
	<button type="button" onclick="location.href='<s:property value="%{urlConsultaTorneo}" />'" class="btn btn-default">Volver</button>
</div>