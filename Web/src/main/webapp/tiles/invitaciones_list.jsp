<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
<!-- 
<head>
<sj:head jqueryui="true" jquerytheme="redmond" />
</head>
<s:url var="editurl" action="Invitacion_editGrid"/>
 -->
 
<script type="text/javascript" src="js/efecto-hover-listados.js"></script>
<script type="text/javascript" language="JavaScript">
function editGrid(clave,estado){
	var selector = 'input[id="clave"][value="'+clave+'"]';
	var msj = $(selector).next().attr('value');
	
	$.ajax({
         type: "POST",
         url: "Invitacion_editGrid.action",
         data: "requestId=" + clave + "&men=" + msj + "&oper=" + estado + "&dummy=d",
         success: function(msg){
        	 var c = $(selector).parent().next();
        	 $('img',c).attr('src','imagenes/' + estado + '.png');
         }
    });
}

</script>
<div align="center"><br>
<h2>Invitaciones Recibidas</h2>
<br>
</div>
<tbody>
<s:form method="POST" action="%{actionMethod}">
    <br>
    
    <s:if test="%{!readOnly}">
	 	<display:table id="invitacionGrid" name="entities" excludedParams="*" pagesize="10" requestURI="MisInvitaciones.action">
		        <s:set name="myrow" value="#attr.invitacionGrid"/>
		        <display:column property="partidoAmistoso.lugar" title="Lugar" />
		        <display:column property="partidoAmistoso.fecha" title="Fecha y Hora" format="{0,date,dd/MM/yyyy - hh:mm}" />
		        <display:column title="Comentario">
		        	<s:hidden key="#myrow.id" id="clave" />
		        	<s:textfield  key="#myrow.mensaje" label="Lugar" theme="simple" cssStyle="width: 97%;" />
		        </display:column>
				<display:column title="Estado">
					<s:if test="#myrow.estado == 'pendiente'">
		            	<img src="imagenes/pendiente.jpg" alt="pendiente"/>
		            </s:if>
		            <s:elseif test="#myrow.estado == 'confirmado'">
		            	<img src="imagenes/confirmado.png" alt="confirmado"/>
		            </s:elseif>
		            <s:elseif test="#myrow.estado == 'cancelado'">
		            	<img src="imagenes/cancelado.png" alt="cancelado"/>
		            </s:elseif>
		            <s:elseif test="#myrow.estado == 'en duda'">
		            	<img src="imagenes/en duda.png" alt="en duda"/>
		            </s:elseif>    
				</display:column>
		        <display:column title="">
					<a href="javascript:editGrid(<s:property value="#myrow.id"/>,'confirmado');"><img src="imagenes/confirmado.png" alt="confirmado" /> </a>
		        </display:column>
		        <display:column title="" >
					<a href="javascript:editGrid(<s:property value="#myrow.id"/>,'cancelado');"><img src="imagenes/cancelado.png" alt="cancelado"/></a>
		        </display:column>
		        <display:column title="" >
					<a href="javascript:editGrid(<s:property value="#myrow.id"/>,'en duda');"><img src="imagenes/en duda.png" alt="en duda"/></a>
		        </display:column>
		        <display:column title="" >
		        	 <s:url id="detalle_url" action="DetallePartidoAmistoso" >
    					<s:param name="requestId"><s:property value="#myrow.partidoAmistoso.id"/></s:param>
					</s:url>
					 <s:a href="%{detalle_url}" ><img src="imagenes/detalles.gif" alt="si"/></s:a>
		        </display:column>
			</display:table>
    </s:if>
    <s:else>

    </s:else>
</s:form>
</tbody>