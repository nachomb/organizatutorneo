<%@ taglib prefix="s" uri="/struts-tags"%>


	<script type="text/javascript" language="JavaScript">
	function isInt(x) {
		var y = parseInt(x);
		if (isNaN(y)) 
			return false;
			
		return ( (x==y) && (x.toString()==y.toString()) );
	} 
	
	function doSubmit(){
		descripcion = document.form.descripcion.value;
		cantidad = document.form.cantidad.value;
		errors = "";
		if(cantidad == "")
			errors += "Se debe especificar la cantidad de Dummys\n";
		else
			if(!isInt(cantidad))
				errors += '"' + document.form.cantidad.value + '"' + " no es una cantidad valida \n";
			else if (cantidad >= 100 || cantidad <= 0)
				errors += " Cantidad debe ser mayor a 0 y menor a 100 \n";
				
		if(descripcion == "")
			errors += "Se debe especificar una descripcion del Dummy\n"
		
		if(errors == ""){
			document.form.submit();
			return true;
		}
		else{
			alert(errors);
			return false;
		}
		
	}
	</script>


	<s:form key="form" action="%{actionMethod}" method="POST">
		<s:hidden key="id"/>
		<s:hidden key="actionMethod" value="%{actionMethod}"/>
		<br>
		<br>
		<table align="center" style="width: 400px;">
			<tr>
				<td>Id:</td><td><s:property value="id" /></td>
			</tr>
			<tr>
				<td><s:textfield key="descripcion" readonly="%{readOnly}" label="Descripción" maxlength="100"/></td>
			</tr>
			<tr>
				<td><s:textfield key="cantidad" readonly="%{readOnly}" label="Cantidad" maxlength="100"/></td>
			</tr>
			<tr>
				<td><input type="button" onclick="return doSubmit()" value="OK" style="float:right;" /></td>
				<td><input type="button" onclick="javascript:history.back(1)" value="Volver" /></td>		
			</tr>
		</table>
	</s:form>
