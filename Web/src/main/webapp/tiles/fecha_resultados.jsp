<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" media="all" type="text/css"
	href="css/jquery-ui-1.8.6.custom.css" />
<!-- script type="text/javascript" src="js/jquery-1.4.4.min.js"></script -->
<script type="text/javascript" src="js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript" language="JavaScript">
function doSubmit(){
	if(!cargarFigura()){
		alert("Debe elegir la figura del partido");
		return false;
	}
	cargarPuntajes();
	cargarLocales();
	cargarVisitantes();
    cargarHiddens();
	document.form.submit();
	return true;
}

function cargarFigura(){
	var figura = document.getElementById("Partido_cargar_figura");
	figura.value = '';
	var fig = false;
	$('input[name=radioFigura]:checked').each(function(index){
		figura.value = $(this).attr('id');
		fig = true;
	});
	return fig;
}

function cargarPuntajes(){
	var selector = 'input[id="puntajeLocales"]';
	var puntajesLocales = document.getElementById("Partido_cargar_puntajesLocales");
	puntajesLocales.value = '';
	$(selector).each(function(index){
		var p = ($(this).attr('value')=='')? '0':$(this).attr('value');
		puntajesLocales.value += p + ',';
	});
	if(puntajesLocales.value){
		puntajesLocales.value = puntajesLocales.value.slice(0, -1);	
	}
	var selector = 'input[id="puntajeVisitantes"]';
	var puntajesVisitantes = document.getElementById("Partido_cargar_puntajesVisitantes");
	puntajesVisitantes.value = '';
	$(selector).each(function(index){
		var p = ($(this).attr('value')=='')? '0':$(this).attr('value');
		puntajesVisitantes.value += p + ',';
	});
	if(puntajesVisitantes.value){
		puntajesVisitantes.value = puntajesVisitantes.value.slice(0, -1);	
	}
	
}

function cargarLocales(){
	var selectTitularesLocales = document.getElementById("Partido_cargar_selectedTitularesLocales");
	selectTitularesLocales.value = '';
	$('input[id=titularLocal]:checked').each(function(index){
		selectTitularesLocales.value += $(this).attr('name') + ',';
	});
	if(selectTitularesLocales.value){
		selectTitularesLocales.value = selectTitularesLocales.value.slice(0, -1);	
	}
	var selectSuplentesLocales = document.getElementById("Partido_cargar_selectedSuplentesLocales");
	selectSuplentesLocales.value = '';
	$('input[id=suplenteLocal]:checked').each(function(index){
		selectSuplentesLocales.value += $(this).attr('name') + ',';
	});
	if(selectSuplentesLocales.value){
		selectSuplentesLocales.value = selectSuplentesLocales.value.slice(0, -1);	
	}
	
	
}

function cargarVisitantes(){
	var selectTitularesVisitantes = document.getElementById("Partido_cargar_selectedTitularesVisitantes");
	selectTitularesVisitantes.value = '';
	$('input[id=titularVisitante]:checked').each(function(index){
		selectTitularesVisitantes.value += $(this).attr('name') + ',';
	});
	if(selectTitularesVisitantes.value){
		selectTitularesVisitantes.value = selectTitularesVisitantes.value.slice(0, -1);	
	}
	var selectSuplentesVisitantes = document.getElementById("Partido_cargar_selectedSuplentesVisitantes");
	selectSuplentesVisitantes.value = '';
	$('input[id=suplenteVisitante]:checked').each(function(index){
		selectSuplentesVisitantes.value += $(this).attr('name') + ',';
	});
	if(selectSuplentesVisitantes.value){
		selectSuplentesVisitantes.value = selectSuplentesVisitantes.value.slice(0, -1);	
	}
}


$(document).ready(function(){
    cargarHiddensASelects();
});

function agregarJugador(idJugadores, idSelected){
    $(idJugadores + ' :selected').each(function(i, selected){
        $(idSelected).append(new Option($(selected).text(), $(selected).val(), false, false));
    });
}

function agregarJugadorLocal(){
    agregarJugador('#goleadoresLocales','#selectedGoleadoresLocales');
}

function agregarJugadorVisitante() {
    agregarJugador('#goleadoresVisitantes','#selectedGoleadoresVisitantes');
}

function agregarAmarillaLocal(){
    agregarJugador('#goleadoresLocales','#selectedAmarillasLocales');
}

function agregarAmarillaVisitante() {
    agregarJugador('#goleadoresVisitantes','#selectedAmarillasVisitantes');
}

function removerJugador(idJugadorSeleccionados){
    $(idJugadorSeleccionados + ' :selected').remove();
}

function removerJugadorLocal(){
    removerJugador('#selectedGoleadoresLocales');
}

function removerJugadorVisitante(){
    removerJugador('#selectedGoleadoresVisitantes');
}

function removerAmarillaLocal(){
    removerJugador('#selectedAmarillasLocales');
}

function removerAmarillaVisitante(){
    removerJugador('#selectedAmarillasVisitantes');
}

function agregarRojaLocal(){
    agregarJugador('#goleadoresLocales','#selectedRojasLocales');
}

function agregarRojaVisitante() {
    agregarJugador('#goleadoresVisitantes','#selectedRojasVisitantes');
}

function removerRojaLocal(){
    removerJugador('#selectedRojasLocales');
}

function removerRojaVisitante(){
    removerJugador('#selectedRojasVisitantes');
}

function cargarHiddens(){
    cargarHiddensGoleadores();
    cargarHiddensTarjetas();
}

function cargarHiddensGoleadores(){
    $('#selectedGoleadoresLocalesHidden').val('');
    $('#selectedGoleadoresLocales option').each(function(i,element){
        $('#selectedGoleadoresLocalesHidden').val($('#selectedGoleadoresLocalesHidden').val() + $(element).val() + ',');
    });

    $('#selectedGoleadoresVisitantesHidden').val('');
    $('#selectedGoleadoresVisitantes option').each(function(i,element){
        $('#selectedGoleadoresVisitantesHidden').val($('#selectedGoleadoresVisitantesHidden').val() + $(element).val() + ',');
    });
}

function cargarHiddensTarjetas(){
    cargarTarjetasAmarillas();
    cargarTarjetasRojas();
    
}

function cargarTarjetasAmarillas(){
    $('#selectedAmarillasLocalesHidden').val('');
    $('#selectedAmarillasLocales option').each(function(i,element){
        $('#selectedAmarillasLocalesHidden').val($('#selectedAmarillasLocalesHidden').val() + $(element).val() + ',');
    });

    $('#selectedAmarillasVisitantesHidden').val('');
    $('#selectedAmarillasVisitantes option').each(function(i,element){
        $('#selectedAmarillasVisitantesHidden').val($('#selectedAmarillasVisitantesHidden').val() + $(element).val() + ',');
    });
}

function cargarTarjetasRojas(){
    $('#selectedRojasLocalesHidden').val('');
    $('#selectedRojasLocales option').each(function(i,element){
        $('#selectedRojasLocalesHidden').val($('#selectedRojasLocalesHidden').val() + $(element).val() + ',');
    });

    $('#selectedRojasVisitantesHidden').val('');
    $('#selectedRojasVisitantes option').each(function(i,element){
        $('#selectedRojasVisitantesHidden').val($('#selectedRojasVisitantesHidden').val() + $(element).val() + ',');
    });
}

function cargarHiddensASelects(){
    hiddenASelect('#selectedGoleadoresLocalesHidden', '#goleadoresLocales', '#selectedGoleadoresLocales');
    hiddenASelect('#selectedGoleadoresVisitantesHidden', '#goleadoresVisitantes', '#selectedGoleadoresVisitantes');

    hiddenASelect('#selectedAmarillasLocalesHidden', '#goleadoresLocales', '#selectedAmarillasLocales');
    hiddenASelect('#selectedAmarillasVisitantesHidden', '#goleadoresVisitantes', '#selectedAmarillasVisitantes');
    hiddenASelect('#selectedRojasLocalesHidden', '#goleadoresLocales', '#selectedRojasLocales');
    hiddenASelect('#selectedRojasVisitantesHidden', '#goleadoresVisitantes', '#selectedRojasVisitantes');
}

function hiddenASelect(hidden, origen, destino){
    var items=$(hidden).val().split(',');
    $.each(items,function(j,item){
        $(origen + ' option').each(function(i,element){
            if ($(element).val() == item){
                $(destino).append(new Option($(element).text(), $(element).val(), false, false));
            }
        });
    });
}
$(function() {
	$('#fechaYHora').datetimepicker({

		timeOnlyTitle : 'Fecha y Hora',
		timeText : 'horario',
		hourText : 'hora',
		minuteText : 'minutos',
		secondText : 'segundos',
		currentText : 'ahora',
		closeText : 'cerrar',
		dateFormat : 'dd/mm/yy',
		timeFormat : 'hh:mm'
	});
});

function mostrarLeyenda(){
    $('#divLeyenda').show();
}
function ocultarLeyenda(){
    $('#divLeyenda').hide();
}
</script>

<h2>Partido</h2>
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:actionerror />

    <s:iterator value="partidos" var="partido" status="status" >
        <tr>
			<td>
			<s:property value="%{#partido.equipoLocal}"/> (<s:label value="%{#partido.golesLocal}" theme="simple" />) - <s:property value="%{#partido.equipoVisitante}"/> (<s:label value="%{#partido.golesVisitante}" theme="simple" />)
            	
            </td>
            <td></td>  
            <td> 
			</td>
		</tr>
		<br/>
		<tr>
			<td>
            	
            </td>
            <td></td>  
            <td>
            	
			</td>
		</tr>
         <tr>
            <td> 
        			<display:table id="jugadorGrid" name="partido.alineacionLocal.titulares" >
						<s:set name="myrow" value="#attr.jugadorGrid"/>
						<display:column title="Jugadoras" >
							<s:property value="#myrow" />
						</display:column>
	                    <display:column property="cantGolesPartido" title="Goles" />
	                    <display:column property="cantAmarillasPartido" title="A" />
	                    <display:column property="cantRojasPartido" title="R" />
	                    <display:column title="Figura">
	                    	<s:if test="#myrow.esFigura">
	                    		figura
	                    	</s:if>
	                    </display:column>
        			</display:table>
     		 <!--	</td>
     			<td>-->
        			<display:table id="jugadorGrid" name="partido.alineacionVisitante.titulares" >
						<s:set name="myrow" value="#attr.jugadorGrid"/>
						<display:column title="Jugadoras" >
							<s:property value="#myrow" />
						</display:column>
	                    <display:column property="cantGolesPartido" title="Goles" />
	                    <display:column property="cantAmarillasPartido" title="A" />
	                    <display:column property="cantRojasPartido" title="R" />
	                    <display:column title="Figura">
	                    	<s:if test="#myrow.esFigura">
	                    		figura
	                    	</s:if>
	                    </display:column>
        			</display:table>
                  </td>
            </tr>
        <br/>
        </s:iterator>
        
		<tr>
            <s:if test="%{!readOnly}">
                <td><input class="button" type="button" onclick="return doSubmit()" value="OK" style="float: right;" /></td>
			    <td><input class="button" type="button" onclick="location.href='Torneo_list.action'" value="Cancelar" /></td>
            </s:if>
            <s:else>
				<td><input  class="button" type="button" onclick="history.back()" value="Volver" style="float: right;" /></td>
            </s:else>
		</tr>
