<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>

	<div align="center">
		<br>
			<h2>Dummies</h2>
		<br>
	</div>
		<s:form method="POST">
			<br>
			<table align="center" style="width: 400px">
				<tr>
					<td>
						<div align="center">
							<s:a href="Dummy_add.action">Nuevo</s:a>
						</div>
					</td>
				</tr>
			</table>
			<table align="center" border="1" style="width: 400px">
		
				<s:if test="entities.size > 0">
						<tr>
							<td style="font-family: Arial; font-weight: bold;font-size: 14px;">
								Id
							</td>
							<td style="font-family: Arial; font-weight: bold;font-size: 14px;">
								Cantidad
							</td>
							<td style="font-family: Arial; font-weight: bold;font-size: 14px;">
								Descripción
							</td>
						</tr>
						<s:iterator value="entities">
							<tr>
								<td>
									<s:property value="id" />
								</td>
								<td>
									<s:property value="cantidad" />
								</td>
								<td>
									<s:property value="descripcion" />
								</td>
								<td style= "width: 10%">
									<s:url action="Dummy_destroy.action" id="remove">
										<s:param name="requestId" value="id" />
									</s:url>
									<s:a href="%{remove}">Eliminar</s:a>
								</td>
								<td style= "width: 10%">
									<s:url action="Dummy_edit.action" id="edit">
										<s:param name="requestId" value="id" />
									</s:url>
									<s:a href="%{edit}">Editar</s:a>
								</td>
							</tr>
						</s:iterator>
					</s:if>
					<s:else>
						<tr bordercolor="red" align="center">
							<td style="font-family: Arial; color: red;font-size: 16 " align="center">
								No se encontraron registros para mostrar
							</td>
						</tr>
					</s:else>
					</table>
			</s:form>
