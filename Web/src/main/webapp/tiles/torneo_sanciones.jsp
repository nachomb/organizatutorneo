<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" language="JavaScript">
	function doSubmit() {
		document.form.submit();
		return true;
	}
</script>



<div class="container">
<h2>Sanciones - <s:property value="%{nombre}" /></h2>
<br>
	<s:hidden key="id" />
	<s:hidden key="requestId" />
	<s:hidden key="actionMethod" value="%{actionMethod}" />
	<s:actionerror />
		<s:url id="urlTablaPos" action="Torneo_estadisticas"
			includeParams="all"></s:url>
		<s:url id="urlTablaGol" action="Torneo_tablaGoleadores"
			includeParams="all"></s:url>
		<s:url id="urlTablaTarjetas" action="Torneo_tablaTarjetas"
			includeParams="all"></s:url>
		<s:url id="urlTablaSanciones" action="Torneo_tablaSanciones"
			includeParams="all"></s:url>
			<div align="center">
			<s:if test="%{tipoTorneo == 'Todos contra Todos'}">
				<button type="button" onclick="location.href='<s:property value="%{urlTablaPos}" />'" class="btn btn-default">Posiciones</button>
			</s:if>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaGol}" />'" class="btn btn-default">Goleadores</button>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaTarjetas}" />'" class="btn btn-default">Tarjetas</button>
			<button type="button" onclick="location.href='<s:property value="%{urlTablaSanciones}" />'" class="btn active">Sancionados</button>
			</div>
			<br>
			<div align="center">
				<s:url action="Sancion_add" var="urlNuevaSancion">
					<s:param name="torneoId" value="%{requestId}"/>
				</s:url>
				<button type="button" onclick="location.href='<s:property value="%{urlNuevaSancion}" />'" class="btn btn-success">Nueva Sancion</button>
			</div>
			<br>
			<br>
			<display:table class="table table-hover table-striped" id="tablaSancionesGrid" name="sanciones"
				sort="list" requestURI="Torneo_tablaSanciones.action"
				defaultsort="4" defaultorder="descending">
				<s:set name="myrow" value="#attr.tablaSancionesGrid"/>
				<display:column title="N" ><%=pageContext.getAttribute("tablaSancionesGrid_rowNum")%></display:column>
				<display:column title="Jugador"	property="jugadorSancionado.nombreYApellido" defaultorder="ascending" sortable="true" />
				<display:column title="Equipo"	property="jugadorSancionado.equipo" sortable="true"/>
				<display:column title="Cant. Fechas" property="cantidadFechas" sortable="true" />
				<display:column title="Fecha Inicio" property="fechaInicio.nombre" sortable="true"/>
				<display:column title="Cumplida" sortable="true">
		         	<s:if test="#myrow.cumplida">
		            	SI
		            </s:if>
		            <s:else>
		            	NO
		            </s:else>
	        	</display:column>
		        <display:column title="Click para editar" href="Sancion_edit.action" paramId="requestId" paramProperty="id" class="ancho-20">
		             <i class="glyphicon glyphicon-pencil"></i>
		        </display:column>
<%-- 				<display:column title="Restan" property="cantFechasRestantes" sortable="true" class="ancho-60"/> --%>
<%-- 				<display:column title="Causa" property="tipo.descripcion" sortable="true" class="ancho-60"/ --%>
<%-- 				<display:column title="Cumplidas" property="cantFechasCumplidas" sortable="true" class="ancho-60"/> --%>
			</display:table>

			<s:url action="Torneo_list" var="urlTorneoList"/>
			<button type="button" onclick="location.href='<s:property value="%{urlTorneoList}" />'" class="btn btn-default">Volver</button>
</div>