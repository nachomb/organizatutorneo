<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="partidos != null">
	<div class="well">
		<s:iterator value="previsualizacionPartidos" var="partido">
			Partido: <strong><s:property value="equipoLocal"/> vs. <s:property value="equipoVisitante"/></strong> - Horario: 
			<s:date name="fechaPartido" format="HH:mm"/> - 
			<s:property value="descripcion"/>
			<br/>
		</s:iterator>
		<br/>
		<s:if test="cumpleRestricciones">
			<strong>Se cumplen con todas las Restricciones.</strong>
		</s:if>
		<s:else>
			<strong>No se cumplen todas las Restricciones.</strong>
		</s:else>
	</div>
</s:if>