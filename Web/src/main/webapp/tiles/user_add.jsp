<%@ taglib prefix="s" uri="/struts-tags" %>
 
<div class="container">
		<s:actionerror cssClass="has-error" theme="simple"/>
		<s:actionmessage/>
		<s:fielderror cssClass="has-error" theme="simple"/>

	  <h1>Crear Usuario</h1>
	  <s:form key="form" action="%{actionMethod}" method="POST" cssClass="form-horizontal" theme="simple">
			  <s:hidden key="activo" value="true" />
			  <s:hidden name="selectedProfileIDs" value="1" /> 
			  <s:hidden key="crearJugador" value="false" />
			  
			<div class="form-group">
			   <label class="col-lg-2 control-label" for="GuardarUsuarioConJugador_username">Email</label>
			   <div class="col-lg-4">
	     			<s:textfield key="username" disabled="%{readOnly}" 
							label="Usuario" maxlength="30" required="true" cssClass="form-control input-xlarge" /> 
			 	</div>
  			</div>
		    <div class="form-group">
		    	<label class="col-lg-2 control-label" for="GuardarUsuarioConJugador_claveSinEncriptar">Contrase&ntilde;a</label>
			    <div class="col-lg-4">
			      	 <s:password key="claveSinEncriptar" disabled="%{readOnly}"
 						                 maxlength="20" required="true" cssClass="form-control input-xlarge"/> 
			    </div>
		  	</div>
		    <div class="form-group">
		    	<label class="col-lg-2 control-label" for="GuardarUsuarioConJugador_claveSinEncriptarRepetida">Repetir Contrase&ntilde;a</label>
			    <div class="col-lg-4">
			    	<s:password key="claveSinEncriptarRepetida" disabled="%{readOnly}"
 						               maxlength="20" required="true" cssClass="form-control input-xlarge"/> 
			    </div>
  			</div>

			<div class="form-group">
			    <div class="col-lg-offset-2 col-lg-10">
			      <s:submit value="Registrarme" cssClass="btn btn-primary"/>
			  </div>
			</div>

	</s:form>
	
</div>

