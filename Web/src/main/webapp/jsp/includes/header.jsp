<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="authz" uri="http://acegisecurity.org/authz" %>


<html>
	<head>
		<title>JRaptor Base Application</title>
		<link rel="stylesheet" href="././css/styles.css" type="text/css" />
	</head>
	<body>
		<div class="pagina">
			<div class="cabeza-menu">
				<div class="logout"><a href="j_acegi_logout">Logout</a></div>
			</div>
			<div class="cabeza">
				<div class="cabeza-titulo">JRaptor Base Application</div>
			</div>
			<div class="cuerpo">