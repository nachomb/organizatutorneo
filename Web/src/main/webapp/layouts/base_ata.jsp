<!DOCTYPE html>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="Organiza Tu Torneo, Software de Administracion de Torneos de Futbol" />
		<meta name="keywords" content="Software,Aplicacion,Torneo,Futbol,Fixture,Estadisticas,Tabla de Posiciones,Tabla de Goleadores,Tabla de Tarjetas,Estadisticas Historicas,Crear Equipos,Calcular Horarios,Campeonatos,Organizar Campeonatos,Golf,Tenis,PES" />
	    <meta name="author" content="">
	    <link rel="shortcut icon" href="imagenes/favicon.ico"/>
	
	    <title>OTorneos.com.ar - Plataforma online de gestion de torneos</title>
	
	    <!-- Bootstrap core CSS -->
	    <link href="http://getbootstrap.com/dist/css/bootstrap.css" rel="stylesheet">
	
	    <!-- Custom styles for this template -->
	    <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">
	
	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="../../assets/js/html5shiv.js"></script>
	      <script src="../../assets/js/respond.min.js"></script>
	    <![endif]-->
	    <script src="js/jquery-2.0.3.min.js"></script>
    	<script src="bootstrap/js/bootstrap.min.js"></script>
    	<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-46841701-1', 'otorneos.com.ar');
		  ga('send', 'pageview');
		
		</script>
	</head>

  <body style="">

    <tiles:insertAttribute name='menu' />

    <tiles:insertAttribute name='body' />

    <div class="container">
      <hr>

      <footer>
        <p>Copyright (c) 2013 OTorneos.com.ar. Todos los derechos reservados.</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

  

</body></html>
