<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@page pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="title" content="Liga Futboleras" />
<meta name="description" content="Liga Futboleras, Primer Torneo de Futbol Femenino en Zona Oeste" />
<meta name="keywords" content="Liga Futboleras,Torneo,Campeonato,Futbol Femenino,Futbol 5,Zona Oeste,Castelar,Estadisticas,Fixture,Premios Semanales,Resumenes,Divisiones con Ascensos y Descensos" />
<meta name="revisit" content="7 days" />
<meta name="distribution" content="Global" />
<meta name="robots" content="All" />
<!-- customizacion -->
<title>Titulo</title>

<link rel="shortcut icon" href="imagenes/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="css/estilos-1.1.css" />
<link rel="stylesheet" type="text/css" href="include/menu/menu-1.0.css" />

<script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>

<script type="text/javascript">
function mainmenu(){
//$(" #nav ul ").css({display: "none"});
$(" #nav li").hover(function(){
	$(this).find('ul:first:hidden').css({visibility: "visible",display: "none"}).slideDown(400);
	},function(){
		$(this).find('ul:first').slideUp(400);
	});
}
$(document).ready(function(){
	mainmenu();
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29215788-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>

<div id="wide-header">
  	<div id="header">
  	  <div id="logo_search">
              <a href="Principal.action" id="sprite_logo" class="spritesCommon logo" title="Liga Futboleras">
              </a>
              <tiles:insertAttribute name='slider' /> 
              <div id="headerDerecho">
              	<!-- customizacion -->
	             <a id="sprite_facebookMini" class="spritesCommon logosFbTw" href="http://www.facebook.com/liga.futboleras" target="_blank"></a>
	             <a id="sprite_twitterMini" class="spritesCommon logosFbTw" href="http://twitter.com/LigaFutboleras" target="_blank"> <img src="imagenes/tw-mini.png" width="16" height="16" /></a>
             </div>
      	</div>
		
		<tiles:insertAttribute name='menu' />

        <div id="contenido_general">
        <div style="height:15px;"></div>  
        
        <!-- CONTENIDO CENTRAL -->
        
        <div id="contenidoCentral">
           	<div id="contenidoIzquierdo">
				<tiles:insertAttribute name='body' /> 
         	</div> <!-- FIN CONTENIDO IZQUIERDO -->
        
	        <div id="contenidoDerecho">
		        <div>
					<tiles:insertAttribute name='cont_derecho' />
		        </div>
	        </div><!-- FIN CONTENIDO DERECHO -->

  		</div><!-- FIN CONTENIDO CENTRAL -->
</div>
<tiles:insertAttribute name='footer' />
</div>
</div>

</body>
</html>