package ar.com.noxit.servicios.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.PartidoDTO;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.Restriccion;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.RestriccionCancha;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.RestriccionHorario;

import com.google.common.collect.Lists;

public class ProgramacionPartidosServiceIntegrationTest {

	private ProgramacionPartidosService target;
	private List<PartidoDTO> partidos;
	private List<Restriccion> restricciones;
	private int cantCanchas;
	private int duracionPartido;
	private Date fechaInicial;
	
	@Before
	public void setUp(){
		target = new ProgramacionPartidosService();
		partidos = Lists.newArrayList();
		restricciones = Lists.newArrayList();
		fechaInicial = new GregorianCalendar(2013, 6, 1, 16, 0).getTime();
		cantCanchas = 2;
		duracionPartido = 50;
		for(int i = 0 ; i < 14 ; ++i){
			PartidoDTO partido = crearPartido(new Long(i));
			partidos.add(partido);
			restricciones.add(crearRestriccionHoraria(i, partido));
		}
		restricciones.add(new RestriccionCancha("=", partidos.get(4), "2"));
		restricciones.add(new RestriccionCancha("=", partidos.get(5), "2"));
	}
	
	private Restriccion crearRestriccionHoraria(int i, PartidoDTO partido) {
		int turno = i / cantCanchas;
		String horario = Utils.formatDateTime(Utils.addMinutes(fechaInicial, turno*duracionPartido), "HH:mm");
		Restriccion restriccion = new RestriccionHorario("=", partido, horario);
		System.out.println(restriccion);
		return restriccion;
	}

	private PartidoDTO crearPartido(Long id) {
		PartidoDTO partido = new PartidoDTO(id);
		return partido;
	}

	@Test
	public void obtenerPosibleProgramacion_muchasRestricciones_programacionOk(){
		Collections.shuffle(partidos);
		List<PartidoDTO> programacion = this.target.obtenerPosibleProgramacion(partidos, restricciones, cantCanchas, duracionPartido, fechaInicial);
		assertNotNull(programacion);
		
		for(PartidoDTO part : programacion){
			Date fechaPartido = part.getFechaPartido();
			Long id = part.getId();
			int turno = id.intValue() / cantCanchas;
			Date fechaEsperada = Utils.addMinutes(fechaInicial, turno*duracionPartido);
			System.out.println(part);
			assertEquals(fechaEsperada, fechaPartido);
			assertNotNull(part.getDescripcion());
			//assertEquals("Cancha " + ((id % cantCanchas) + 1), part.getDescripcion());
		}
		
	}
	
	
}
