package ar.com.noxit.servicios.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.uncommons.watchmaker.framework.FitnessEvaluator;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.FuncionDeAptitud;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.PartidoDTO;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.Restriccion;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.RestriccionCancha;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.RestriccionHorario;

public class ProgramacionPartidosServiceTest {

	private List<Restriccion> restricciones;
	private List<PartidoDTO> partidos;
	private long id = 1;
	private ProgramacionPartidosService target = new ProgramacionPartidosService();
	private FitnessEvaluator<List<PartidoDTO>> fitnessEvaluator;
	private int cantCanchas = 2;
	private int duracionPartido = 50;

	@Before
	public void setUp() {
		id = 1;
		restricciones = new ArrayList<Restriccion>();
		partidos = new ArrayList<PartidoDTO>();
	}

	private void crearContexto() {
		PartidoDTO p = crearPartido();
		partidos.add(p);
		restricciones.add(new RestriccionHorario("=", p, "18:30"));
		restricciones.add(new RestriccionCancha("=", p, "1"));
		p = crearPartido();
		partidos.add(p);
		restricciones.add(new RestriccionHorario(">=", p, "18:30"));
		p = crearPartido();
		partidos.add(p);
		restricciones.add(new RestriccionHorario(">=", p, "17:40"));
		restricciones.add(new RestriccionHorario("<=", p, "19:20"));
		p = crearPartido();
		partidos.add(p);
		restricciones.add(new RestriccionHorario(">=", p, "19:20"));
		p = crearPartido();
		partidos.add(p);
		restricciones.add(new RestriccionHorario(">=", p, "19:20"));
		p = crearPartido();
		partidos.add(p);
		restricciones.add(new RestriccionHorario("=", p, "21:00"));
		crearPartidos(7);
		fitnessEvaluator = new FuncionDeAptitud(restricciones);
	}

	private void crearPartidos(int cantidad) {
		for (int i = 1; i <= cantidad; ++i) {
			partidos.add(crearPartido());
		}
	}

	private PartidoDTO crearPartido() {
		return new PartidoDTO(id++);
	}

	@Test
	public void completarFecha_ok(){
		 crearPartidos(9);
		 Date fechaInicial = crearFechaInicial();
		 partidos.get(0).setFechaPartido(Utils.addMinutes(fechaInicial, duracionPartido));
		 partidos.get(1).setFechaPartido(Utils.addMinutes(fechaInicial, duracionPartido*2));
		 partidos.get(2).setFechaPartido(Utils.addMinutes(fechaInicial, duracionPartido*3));
		 this.target.completarFecha(partidos, fechaInicial, cantCanchas, duracionPartido);
		 for(PartidoDTO partido : partidos){
			 assertNotNull(partido.getFechaPartido());
			 System.out.println(partido);
		 }
		
	}
	
	 @Test
	 public void contarPartidosMismoHorario_ok(){
		 crearPartidos(9);
		 Date fechaInicial = crearFechaInicial();
		 int i = 0;
		 for(PartidoDTO partido : partidos){
			 if(i <= 2){
				 // 0, 1, 2
				 partido.setFechaPartido(fechaInicial);
			 }else if(i <= 3){
				 //3
				 partido.setFechaPartido(Utils.addMinutes(fechaInicial, 20));
			 }else if(i <= 7){
				 //4, 5, 6, 7
				 partido.setFechaPartido(Utils.addMinutes(fechaInicial, 40));
			 }else if(i <= 10){
				 //8
				 partido.setFechaPartido(Utils.addMinutes(fechaInicial, 60));
			 }
			 ++i;
		 }
		 List<Integer> partidosMismoHorario = target.contarPartidosMismoHorario(partidos, fechaInicial);
		 assertEquals(4, partidosMismoHorario.size());
		 assertEquals(3, partidosMismoHorario.get(0).intValue());
		 assertEquals(1, partidosMismoHorario.get(1).intValue());
		 assertEquals(4, partidosMismoHorario.get(2).intValue());
		 assertEquals(1, partidosMismoHorario.get(3).intValue());
	 }
	
	@Test
	public void test() {
		crearContexto();
		Date fechaInicial = crearFechaInicial();
		System.out.println(fechaInicial);
		
		List<PartidoDTO> candidato = target
				.obtenerPosibleProgramacion(partidos, restricciones, 2, 50,
						fechaInicial);
		
		double fitness = fitnessEvaluator.getFitness(candidato, null);
		assertEquals(100, fitness, 0.1);
		
		assertEquals(true, target.cumpleTodasRestricciones(candidato, restricciones));
	}

	private Date crearFechaInicial() {
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 16);
		calendar.set(Calendar.MINUTE, 0);
		Date fechaInicial = calendar.getTime();
		return fechaInicial;
	}
	

	
}
