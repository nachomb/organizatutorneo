package ar.com.noxit.servicios.impl.algoritmosgeneticos;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

public class RestriccionHorarioTest {

	private Restriccion restriccion;
	private PartidoDTO p = new PartidoDTO(1L);

	@Before
	public void setUp() {

	}

	@Test
	public void cumple_mayorIgual_verdadero() {
		restriccion = new RestriccionHorario(">=", p, "16:00");

		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 16);
		calendar.set(Calendar.MINUTE, 20);
		Date date = calendar.getTime();
		p.setFechaPartido(date);

		boolean esValida = restriccion.esValida(p);
		assertEquals(true, esValida);
		assertEquals(date, p.getFechaPartido());
	}

	@Test
	public void cumple_menorIgual_verdadero() {
		restriccion = new RestriccionHorario("<=", p, "16:00");

		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 15);
		calendar.set(Calendar.MINUTE, 59);
		p.setFechaPartido(calendar.getTime());

		boolean esValida = restriccion.esValida(p);
		assertEquals(true, esValida);
	}

	@Test
	public void cumple_igual_verdadero() {
		restriccion = new RestriccionHorario("=", p, "16:00");

		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 16);
		calendar.set(Calendar.MINUTE, 0);
		p.setFechaPartido(calendar.getTime());

		boolean esValida = restriccion.esValida(p);
		assertEquals(true, esValida);
	}

}
