package ar.com.noxit.servicios.dao.impl;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Dummy;
import ar.com.noxit.servicios.dao.IDummyDAO;

@Transactional
public class DummyDAO extends AbstractDAO<Dummy> implements IDummyDAO {

}
