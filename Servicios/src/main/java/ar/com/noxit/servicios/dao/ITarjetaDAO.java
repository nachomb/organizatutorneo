package ar.com.noxit.servicios.dao;

import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.Tarjeta;
import ar.com.noxit.picadito.dominio.entidades.Torneo;

public interface ITarjetaDAO extends IGenericDAO<Tarjeta, Long> {

    void persistList(List<Tarjeta> tarjetas);

    List<Tarjeta> getTarjetasByJugadorYPartido(Jugador jugador, Partido partido);

    List<Tarjeta> getTarjetasByJugadorYPartidoYTipo(Jugador jugador, Partido partido, String tipoTarjeta);

    Long getCountTarjetasByJugadorYPartidoYTipo(Jugador jugador, Partido partido, String tipoTarjeta);

	Long getCountTarjetasByJugadorYTipo(Jugador jugador, String tipo);

    Boolean existeTarjeta(Partido partido, Jugador jugador, String tipo);

    void eliminarTarjeta(Partido partido, String tipoAmarilla);

	Long countTarjetas(Torneo torneo, Equipo equipo, String tipo);

	List<Tarjeta> findByTorneo(Torneo torneo);

	void borrarTarjetas(Partido partido, Equipo equipo);

}
