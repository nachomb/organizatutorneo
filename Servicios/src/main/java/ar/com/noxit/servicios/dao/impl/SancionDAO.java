package ar.com.noxit.servicios.dao.impl;

import java.util.List;

import org.hibernate.Query;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.Sancion;
import ar.com.noxit.picadito.dominio.entidades.Tarjeta;
import ar.com.noxit.picadito.dominio.entidades.TipoSancion;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.servicios.dao.ISancionDAO;

public class SancionDAO extends AbstractDAO<Sancion> implements ISancionDAO {

    @Override
    public List<TipoSancion> existeSancion(int cantidadTarjetas, String tipoTarjeta) {
        // Se utiliza un distinct para que se genere solo un tipo de sancion
        String queryString = " select distinct ts from TipoSancion ts ";
        if (tipoTarjeta != null && tipoTarjeta.trim().equalsIgnoreCase(Tarjeta.TIPO_AMARILLA))
            queryString += " where ts.cantidadAmarillas <= :amarillas ";
        else if (tipoTarjeta != null && tipoTarjeta.trim().equalsIgnoreCase(Tarjeta.TIPO_ROJA))
            queryString += " where ts.cantidadRojas <= :rojas ";
        else
            return null;

        Query query = getSession().createQuery(queryString);
        if (tipoTarjeta != null && tipoTarjeta.trim().equalsIgnoreCase(Tarjeta.TIPO_AMARILLA))
            query.setParameter("amarillas", cantidadTarjetas);
        else if (tipoTarjeta != null && tipoTarjeta.trim().equalsIgnoreCase(Tarjeta.TIPO_ROJA))
            query.setParameter("rojas", cantidadTarjetas);

        return query.list();
    }

    @Override
    public List<TipoSancion> existeSancion(int cantidadAmarillas, int cantidadRojas) {
        // Se utiliza un distinct para que se genere solo un tipo de sancion
        // Se busca por igual estricto para que no se superpongan sanciones
        String queryString = " select distinct ts from TipoSancion ts where ts.cantidadAmarillas = :amarillas "
                + " and ts.cantidadRojas = :rojas ";

        Query query = getSession().createQuery(queryString);
        query.setParameter("amarillas", cantidadAmarillas);
        query.setParameter("rojas", cantidadRojas);

        return query.list();
    }

    @Override
    public boolean existeSancion(Partido partido, Jugador jugador, TipoSancion tipoSancion) {
        String queryString = " from Sancion s where s.partidoInicio = :partido and s.jugadorSancionado = :jugador and s.tipo = :tipo ";

        Query query = getSession().createQuery(queryString);
        query.setParameter("partido", partido);
        query.setParameter("jugador", jugador);
        query.setParameter("tipo", tipoSancion);

        return !query.list().isEmpty();
    }

    @Override
    public List<Sancion> getSancionesPorPartido(Partido partido) {
        String queryString = " from Sancion s where s.partidoInicio = :partido and s.cumplida = false ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("partido", partido);
        return query.list();
    }

    @Override
    public List<Sancion> getSancionesPorPartido(Equipo equipoLocal, Equipo equipoVisitante) {
        String queryString = " from Sancion s where s.cumplida = false and (s.jugadorSancionado in (:locales) "
                + " or s.jugadorSancionado in (:visitantes)) ";
        Query query = getSession().createQuery(queryString);
        query.setParameterList("locales", equipoLocal.getJugadores());
        query.setParameterList("visitantes", equipoVisitante.getJugadores());
        return query.list();
    }

	@Override
	public List<Sancion> findByTorneoNoCumplidas(Torneo torneo) {
        String queryString = " from Sancion s where s.fechaInicio.torneo = :torneo and s.cumplida = false ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("torneo", torneo);
        return query.list();
	}

	@Override
	public List<Sancion> findByTorneo(Torneo torneo) {
        String queryString = " from Sancion s where s.fechaInicio.torneo = :torneo ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("torneo", torneo);
        return query.list();
	}

}
