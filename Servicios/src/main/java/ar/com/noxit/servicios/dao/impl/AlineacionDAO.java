package ar.com.noxit.servicios.dao.impl;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Alineacion;
import ar.com.noxit.servicios.dao.IAlineacionDAO;

public class AlineacionDAO extends AbstractDAO<Alineacion> implements IAlineacionDAO {

}
