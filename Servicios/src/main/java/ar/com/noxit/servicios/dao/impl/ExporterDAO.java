package ar.com.noxit.servicios.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class ExporterDAO {

	private Connection connect = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	private ComboPooledDataSource dataSource;
	
	public ComboPooledDataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(ComboPooledDataSource dataSource) {
		this.dataSource = dataSource;
	}

	public ExporterDAO(){
	}
	
	public void conectar() throws Exception{
		connect = dataSource.getConnection();
	}

	public void readDataBase() throws Exception {
		try {

			// Statements allow to issue SQL queries to the database
			statement = connect.createStatement();
			// Result set get the result of the SQL query
			resultSet = statement.executeQuery("select * from equipo");
			// writeResultSet(resultSet);

			String sql = "select * from partido";
			connect.prepareCall(sql);

			PreparedStatement ps = connect
					.prepareStatement("SELECT distinct id_partido, eLoc.nombre as nombreLocal,eVis.nombre as nombreVisitante,golesLocal, golesVisitante "
							+ "FROM partido p "
							+ "inner join equipo eLoc on eLoc.id_equipo = p.equipo_local_fk "
							+ "inner join equipo eVis on eVis.id_equipo = p.equipo_visitante_fk "
							+ "where id_fecha = ?");
			ps.setLong(1, 322);
			ResultSet resultSet2 = ps.executeQuery();
			writeResultSet(resultSet2);

		} catch (Exception e) {
			throw e;
		} finally {
			close();
		}

	}

	private void writeMetaData(ResultSet resultSet) throws SQLException {
		// Now get some metadata from the database
		// Result set get the result of the SQL query

		System.out.println("The columns in the table are: ");

		System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
		for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
			System.out.println("Column " + i + " "
					+ resultSet.getMetaData().getColumnName(i));
		}
	}

	private void writeResultSet(ResultSet resultSet) throws SQLException {
		// ResultSet is initially before the first data set
		while (resultSet.next()) {
			// It is possible to get the columns via name
			// also possible to get the columns via the column number
			// which starts at 1
			// e.g. resultSet.getSTring(2);
			String nombreLocal = resultSet.getString("nombreLocal");
			System.out.println("nombreLocal: " + nombreLocal);
			String nombreVisitante = resultSet.getString("nombreVisitante");
			System.out.println("nombreVisitante: " + nombreVisitante);
			String golesLocal = resultSet.getString("golesLocal");
			System.out.println("golesLocal: " + golesLocal);
			String golesVisitante = resultSet.getString("golesVisitante");
			System.out.println("golesVisitante: " + golesVisitante);
		}
	}

	// You need to close the resultSet
	private void close() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (statement != null) {
				statement.close();
			}

			if (connect != null) {
				connect.close();
			}
		} catch (Exception e) {

		}
	}

	public ResultSet getResults(Long idFecha) throws Exception {
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT distinct id_partido, eLoc.nombre as nombreLocal,eVis.nombre as nombreVisitante,golesLocal, golesVisitante ");
		builder.append("FROM partido p ");
		builder.append("inner join equipo eLoc on eLoc.id_equipo = p.equipo_local_fk ");
		builder.append("inner join equipo eVis on eVis.id_equipo = p.equipo_visitante_fk ");
		builder.append("where id_fecha = ?");
		PreparedStatement ps = connect
				.prepareStatement(builder.toString());
		ps.setLong(1, idFecha);
		return ps.executeQuery();

	}
	
	public ResultSet getIdsPartido(Long idFecha) throws Exception{
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT id_partido ");
		builder.append("FROM partido ");
		builder.append("WHERE id_fecha = ?");
		PreparedStatement ps = connect.prepareStatement(builder.toString());
		ps.setLong(1, idFecha);
		return ps.executeQuery();
	}

	public ResultSet getPlayersLocales(int idPartido) throws SQLException {
		String alineacion = "alineacion_local";
		PreparedStatement ps = connect.prepareStatement(getQueryPlayers(alineacion));
		ps.setLong(1, idPartido);
		return ps.executeQuery();
	}
	
	public ResultSet getPlayersVisitantes(int idPartido) throws SQLException {
		String alineacion = "alineacion_visitante";
		PreparedStatement ps = connect.prepareStatement(getQueryPlayers(alineacion));
		ps.setLong(1, idPartido);
		return ps.executeQuery();
	}

	private String getQueryPlayers(String alineacion) {
		StringBuilder builder = new StringBuilder();
		builder.append("select punt.numero, concat_ws(' ', j.nombre,j.apellido), count(distinct id_gol) as goles, count(distinct tAma.id_tarjeta) as tarjetasAmarillas, count(distinct tRoj.id_tarjeta) as tarjetasRojas, punt.figura, j.id_jugador ");
		builder.append("from partido p ");
		builder.append("inner join alineacion_titulares alLoc on alLoc.id_alineacion = p.");
		builder.append(alineacion);
		builder.append(" ");
		builder.append("inner join jugador j on j.id_jugador = alLoc.id_jugador ");
		builder.append("left outer join gol g on g.jugador_fk = j.id_jugador and g.partido_fk = id_partido ");
		builder.append("left outer join tarjeta tAma on tAma.jugador_fk = j.id_jugador and tAma.partido_fk = id_partido and tAma.tipo = 'AMARILLA' ");
		builder.append("left outer join tarjeta tRoj on tRoj.jugador_fk = j.id_jugador and tRoj.partido_fk = id_partido and tRoj.tipo = 'ROJA' ");
		builder.append("inner join puntaje_partido punt on punt.jugador_fk = j.id_jugador and punt.partido_fk = id_partido ");
		builder.append("where id_partido = ? ");
		builder.append("group by j.id_jugador ");
		builder.append("order by j.activo desc, punt.numero ");
		return builder.toString();
	}

	public ResultSet getPosiciones(Long idTorneo) throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT e.nombre, p.partidosJugados, p.partidosGanados, p.partidosEmpatados, p.partidosPerdidos, ");
		builder.append("p.golesAFavor, p.golesEnContra, p.golesAFavor - p.golesEnContra as diferencia, p.puntos ");
		builder.append("FROM posicion_torneo p ");
		builder.append("inner join equipo e on e.id_equipo = p.equipo_fk ");
		builder.append("where torneo_fk = ? ");
		builder.append("order by p.puntos desc, diferencia desc, p.golesAFavor desc");
		PreparedStatement ps = connect.prepareStatement(builder.toString());
		ps.setLong(1, idTorneo);
		return ps.executeQuery();
	}

	public ResultSet getGoleadores(Long idTorneo) throws SQLException{
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT concat_ws(' ', j.nombre,j.apellido),e.nombre,p.goles ");
		builder.append("FROM posicion_goleador p ");
		builder.append("inner join jugador j on j.id_jugador = p.jugador_fk ");
		builder.append("inner join equipo e on e.id_equipo = j.equipo_fk ");
		builder.append("WHERE torneo_fk = ? ");
		builder.append("order by goles desc ");
		builder.append("limit 10");
		PreparedStatement ps = connect.prepareStatement(builder.toString());
		ps.setLong(1, idTorneo);
		return ps.executeQuery();
	}
	
	public ResultSet getTarjetas(Long idTorneo) throws SQLException{
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT concat_ws(' ', j.nombre,j.apellido), e.nombre, c.tarjetas_amarillas, c.tarjetas_rojas ");
		builder.append("FROM cantidad_tarjetas c ");
		builder.append("inner join jugador j on j.id_jugador = c.jugador_fk ");
		builder.append("inner join equipo e on e.id_equipo = j.equipo_fk ");
		builder.append("where torneo_fk = ? ");
		builder.append("order by e.nombre");
		PreparedStatement ps = connect.prepareStatement(builder.toString());
		ps.setLong(1, idTorneo);
		return ps.executeQuery();
	}
	
	
}