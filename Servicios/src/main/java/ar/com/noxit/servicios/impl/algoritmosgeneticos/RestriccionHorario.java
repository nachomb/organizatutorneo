package ar.com.noxit.servicios.impl.algoritmosgeneticos;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class RestriccionHorario extends Restriccion {

	private Calendar calendar;

	public RestriccionHorario(String operador, PartidoDTO partido, String valor) {
		super(operador, partido, valor);
		calendar = new GregorianCalendar();
	}

	@Override
	protected boolean esValida(PartidoDTO partido) {
		Date fechaPartido = partido.getFechaPartido();
		Date fechaRestriccion = getFechaRestriccion(fechaPartido);
		if ("<=".equals(operador)) {
			return fechaPartido.before(fechaRestriccion)
					|| fechaPartido.equals(fechaRestriccion);
		} else if (">=".equals(operador)) {
			return fechaPartido.after(fechaRestriccion)
					|| fechaPartido.equals(fechaRestriccion);
		} else if ("=".equals(operador)) {
			return fechaPartido.equals(fechaRestriccion);
		}
		return false;
	}

	private Date getFechaRestriccion(Date fecha) {
		calendar.setTime(fecha);
		calendar.set(Calendar.HOUR_OF_DAY,
				Integer.parseInt(valor.substring(0, 2)));
		calendar.set(Calendar.MINUTE, Integer.parseInt(valor.substring(3)));
		Date fechaRestriccion = calendar.getTime();
		return fechaRestriccion;
	}

	@Override
	protected void aplicar(PartidoDTO partidoDTO, Date fechaInicial) {
		if("=".equals(operador)){
			Date fechaRestriccion = getFechaRestriccion(fechaInicial);
			partidoDTO.setFechaPartido(fechaRestriccion);
		}
	}

}
