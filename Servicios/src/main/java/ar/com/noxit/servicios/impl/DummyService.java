package ar.com.noxit.servicios.impl;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Dummy;
import ar.com.noxit.servicios.IDummyService;
import ar.com.noxit.servicios.dao.IDummyDAO;

@Transactional
public class DummyService extends AbstractService<Dummy> implements IDummyService {

    public DummyService(IDummyDAO dao) {
        this.genericDAO = dao;
    }

    public IDummyDAO getAgenteDAO() {
        return (IDummyDAO) this.genericDAO;
    }

    public void setAgenteDAO(IDummyDAO agenteDAO) {
        this.genericDAO = agenteDAO;
    }

}
