package ar.com.noxit.servicios.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.PartidoAmistoso;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.dao.IPartidoAmistosoDAO;

public class PartidoAmistosoDAO extends AbstractDAO<PartidoAmistoso> implements IPartidoAmistosoDAO {

    @SuppressWarnings("unchecked")
    @Override
    public List<PartidoAmistoso> getPartidosByUsuario(Usuario usuario, Jugador jugador) {
        String  queryString = "select p from " + this.getEntity().getSimpleName() + " p "
                    + " where p.id in ( "
                    + " select p1.id from " + this.getEntity().getSimpleName() + " p1 "
                    + " where p1.creador = :usuario and p1.fecha > :fechaActual "
                    + " ) or p.id in ( "
                    + " select p2.id from " + this.getEntity().getSimpleName() + " p2 "
                    + " inner join p2.alineacionEquipoA.titulares ea "
                    + " where ea is not null and :jugador in (ea) and p2.fecha > :fechaActual "
                    + " ) or p.id in ("
                    + " select p3.id from " + this.getEntity().getSimpleName() + " p3 "
                    + " inner join p3.alineacionEquipoB.titulares eb "
                    + " where eb is not null and :jugador in (eb) and p3.fecha > :fechaActual "
                    + " ) order by p.fecha desc ";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("usuario", usuario);
        query.setParameter("jugador", jugador);
        query.setParameter("fechaActual", new Date());
        return (List<PartidoAmistoso>) this.doList(query);
    }

    @Override
    public Integer getCountPartidosByUsuario(Usuario usuario, Jugador jugador) {
        String  queryString = "select count(p) from " + this.getEntity().getSimpleName() + " p "
                    + " where p.id in ( "
                    + " select p1.id from " + this.getEntity().getSimpleName() + " p1 "
                    + " where p1.creador = :usuario and p1.fecha > :fechaActual "
                    + " ) or p.id in ( "
                    + " select p2.id from " + this.getEntity().getSimpleName() + " p2 "
                    + " inner join p2.alineacionEquipoA.titulares ea "
                    + " where ea is not null and :jugador in (ea) and p2.fecha > :fechaActual "
                    + " ) or p.id in ("
                    + " select p3.id from " + this.getEntity().getSimpleName() + " p3 "
                    + " inner join p3.alineacionEquipoB.titulares eb "
                    + " where eb is not null and :jugador in (eb) and p3.fecha > :fechaActual "
                    + " ) order by p.fecha desc ";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("usuario", usuario);
        query.setParameter("jugador", jugador);
        query.setParameter("fechaActual", new Date());
        Long uniqueResult = (Long) query.uniqueResult();
        return (uniqueResult != null) ? uniqueResult.intValue() : 0;
    }

    @Override
    public List<PartidoAmistoso> getPartidosJugadosByUsuario(Usuario usuario, Jugador jugador) {
        String  queryString = "select p from " + this.getEntity().getSimpleName() + " p "
                    + " where p.id in ( "
                    + " select p1.id from " + this.getEntity().getSimpleName() + " p1 "
                    + " where p1.creador = :usuario and p1.fecha <= :fechaActual "
                    + " ) or p.id in ( "
                    + " select p2.id from " + this.getEntity().getSimpleName() + " p2 "
                    + " inner join p2.alineacionEquipoA.titulares ea "
                    + " where ea is not null and :jugador in (ea) and p2.fecha <= :fechaActual "
                    + " ) or p.id in ("
                    + " select p3.id from " + this.getEntity().getSimpleName() + " p3 "
                    + " inner join p3.alineacionEquipoB.titulares eb "
                    + " where eb is not null and :jugador in (eb) and p3.fecha <= :fechaActual "
                    + " ) order by p.fecha desc ";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("usuario", usuario);
        query.setParameter("jugador", jugador);
        query.setParameter("fechaActual", new Date());
        return (List<PartidoAmistoso>) this.doList(query);
    }

    @Override
    public Integer getCountPartidosJugadosByUsuario(Usuario usuario, Jugador jugador) {
        String  queryString = "select count(p) from " + this.getEntity().getSimpleName() + " p "
                + " where p.id in ( "
                + " select p1.id from " + this.getEntity().getSimpleName() + " p1 "
                + " where p1.creador = :usuario and p1.fecha <= :fechaActual "
                + " ) or p.id in ( "
                + " select p2.id from " + this.getEntity().getSimpleName() + " p2 "
                + " inner join p2.alineacionEquipoA.titulares ea "
                + " where ea is not null and :jugador in (ea) and p2.fecha <= :fechaActual "
                + " ) or p.id in ("
                + " select p3.id from " + this.getEntity().getSimpleName() + " p3 "
                + " inner join p3.alineacionEquipoB.titulares eb "
                + " where eb is not null and :jugador in (eb) and p3.fecha <= :fechaActual "
                + " ) order by p.fecha desc ";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("usuario", usuario);
        query.setParameter("jugador", jugador);
        query.setParameter("fechaActual", new Date());
        Long uniqueResult = (Long) query.uniqueResult();
        return (uniqueResult != null) ? uniqueResult.intValue() : 0;
    }

    @Override
    public List<PartidoAmistoso> getAllPartidosJugados() {
        String queryString = "select p from " + this.getEntity().getSimpleName() + " p "
                + " where  fecha <= :fechaActual order by fecha desc";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("fechaActual", new Date());
        return (List<PartidoAmistoso>) this.doList(query);
    }

    @Override
    public Integer getCountAllPartidosJugados() {
        String queryString = "select count(p) from " + this.getEntity().getSimpleName() + " p "
                + " where  fecha <= :fechaActual order by fecha desc";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("fechaActual", new Date());
        Long uniqueResult = (Long) query.uniqueResult();
        return (uniqueResult != null) ? uniqueResult.intValue() : 0;
    }

    @Override
    public List<PartidoAmistoso> getAllPartidosNoJugados() {
        String queryString = "select p from " + this.getEntity().getSimpleName() + " p "
                + " where  fecha > :fechaActual order by fecha desc";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("fechaActual", new Date());
        return (List<PartidoAmistoso>) this.doList(query);
    }

    @Override
    public Integer getCountAllPartidosNoJugados() {
        String queryString = "select count(p) from " + this.getEntity().getSimpleName() + " p "
                + " where  fecha > :fechaActual order by fecha desc";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("fechaActual", new Date());
        Long uniqueResult = (Long) query.uniqueResult();
        return (uniqueResult != null) ? uniqueResult.intValue() : 0;
    }
    
}
