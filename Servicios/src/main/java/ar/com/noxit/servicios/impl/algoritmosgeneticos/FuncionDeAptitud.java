package ar.com.noxit.servicios.impl.algoritmosgeneticos;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uncommons.watchmaker.framework.FitnessEvaluator;

public class FuncionDeAptitud implements FitnessEvaluator<List<PartidoDTO>> {

	private static final long serialVersionUID = -2508142600605113673L;
	private List<Restriccion> restricciones;
	private static final Logger logger = LoggerFactory
			.getLogger(FuncionDeAptitud.class);

	public FuncionDeAptitud(List<Restriccion> restricciones) {
		super();
		this.restricciones = restricciones;
	}

	@Override
	public double getFitness(List<PartidoDTO> candidate,
			List<? extends List<PartidoDTO>> population) {

		double aptitud = 100;

		for (PartidoDTO partidoDTO : candidate) {
			for (Restriccion restriccion : restricciones) {
				if (!restriccion.cumple(partidoDTO)) {
					aptitud -= 10;
				}
			}
		}
		if (aptitud < 0)
			return 0;

		return aptitud;
	}

	@Override
	public boolean isNatural() {
		return true;
	}

}
