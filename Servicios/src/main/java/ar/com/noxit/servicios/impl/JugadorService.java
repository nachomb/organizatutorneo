package ar.com.noxit.servicios.impl;

import java.util.Collection;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.IJugadorService;
import ar.com.noxit.servicios.dao.IJugadorDAO;

public class JugadorService extends AbstractService<Jugador> implements
		IJugadorService {

	public JugadorService(IJugadorDAO jugadorDAO) {
		this.genericDAO = jugadorDAO;
	}

	public IJugadorDAO getJugadorDAO() {
		return (IJugadorDAO) this.genericDAO;
	}

	public void setJugadorDAO(IJugadorDAO jugadorDAO) {
		this.genericDAO = jugadorDAO;
	}

	@Override
	public Collection<Jugador> getAllJugadoresPosibles() {
		return getJugadorDAO().getAllJugadoresPosibles();
	}

	@Override
	@Transactional(readOnly=true)
	public Jugador getjugadorByUsuario(Usuario usuario) {
		return getJugadorDAO().getjugadorByUsuario(usuario);
	}

	@Override
	@Transactional(readOnly=true)
	public Jugador getjugadorByNombreUsuario(String usuario) {
		return getJugadorDAO().getjugadorByNombreUsuario(usuario);
	}

	@Override
	public Collection<Jugador> getJugadorLike(Jugador jugador) {
		return getJugadorDAO().getJugadorLike(jugador);
	}

	@Override
	public Jugador getJugadorByDni(String dni) {
		return getJugadorDAO().getJugadorByDni(dni);
	}

	@Override
	public Collection<Jugador> getJugadoresByUsuarioCreador(Usuario usuario) {
		return getJugadorDAO().getJugadoresByUsuarioCreador(usuario);
	}

	@Override
	public Collection<Jugador> getJugadoresByUsuarioCreadorSinEquipo(
			Usuario usuario) {
		return getJugadorDAO().getJugadoresByUsuarioCreadorSinEquipo(usuario);
	}

}
