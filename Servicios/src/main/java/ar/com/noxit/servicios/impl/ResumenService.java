package ar.com.noxit.servicios.impl;

import java.util.Collection;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Resumen;
import ar.com.noxit.servicios.IResumenService;
import ar.com.noxit.servicios.dao.IResumenDAO;

public class ResumenService extends AbstractService<Resumen> implements IResumenService{

	public ResumenService(IResumenDAO resumenDAO){
		this.genericDAO = resumenDAO;
	}

	@Override
	public Collection<Resumen> getAllActivosOrderByFechaDesc() {
		return ((IResumenDAO)this.genericDAO).getAllActivosOrderByFechaDesc();
	}
	
}
