package ar.com.noxit.servicios;

import java.util.List;
import java.util.Map;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Alineacion;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.PuntajePartido;

public interface IPuntajePartidoService extends IGenericService<PuntajePartido> {

	void generarPuntajes(Map<Long, Long> numJugadores, List<Long> puntJugadores, Jugador figura,
			Partido partido, Equipo equipo, Alineacion alineacion);

	PuntajePartido findByJugadorPartido(Jugador j, Partido entity);

	Integer sumaPuntajes(Jugador j);

	Integer countFigura(Jugador j);
	
	void borrarPuntajes(Partido partido);
	
	void borrarPuntajes(Partido partido, Equipo equipo);

}
