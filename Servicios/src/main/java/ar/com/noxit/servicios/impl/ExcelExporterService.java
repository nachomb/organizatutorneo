package ar.com.noxit.servicios.impl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import ar.com.noxit.base.utils.ExcelWritter;
import ar.com.noxit.base.utils.Mail;
import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.base.utils.container.Matriz;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.PosicionGoleador;
import ar.com.noxit.picadito.dominio.entidades.PosicionTorneo;
import ar.com.noxit.picadito.dominio.entidades.ReporteHistorico;
import ar.com.noxit.picadito.dominio.entidades.ReporteJugador;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.picadito.dominio.entidades.comparator.PartidoComparator;
import ar.com.noxit.picadito.dominio.entidades.comparator.PosicionGoleadorComparator;
import ar.com.noxit.picadito.dominio.entidades.comparator.PosicionTorneoComparator;
import ar.com.noxit.servicios.IExcelExporterService;
import ar.com.noxit.servicios.dao.IPartidoDAO;
import ar.com.noxit.servicios.dao.ITorneoDAO;
import ar.com.noxit.servicios.dao.impl.ExporterDAO;

public class ExcelExporterService implements IExcelExporterService {

	private static final String NOMBRE_ARCHIVO_PLANILLAS = "Planillas.xls";
	private static final String NOMBRE_ARCHIVO_TEMPLATE_PLANILLAS = "TemplatePlanillas.xls";
	private static final String NOMBRE_ARCHIVO_ESTADISTICAS = "Estadisticas.xls";
	private static final String NOMBRE_ARCHIVO_TEMPLATE_ESTADISTICAS = "TemplateEstadisticas.xls";
	private static final String NOMBRE_ARCHIVO_ESTADISTICAS_HISTORICAS = "EstadisticasHistoricas.xlsx";
	private static final String NOMBRE_ARCHIVO_TEMPLATE_ESTADISTICAS_HISTORICAS = "TemplateEstadisticasHistoricas.xlsx";
	private Mail mailService;
	private ExporterDAO exporterDAO;
	private ITorneoDAO torneoDAO;
	private IPartidoDAO partidoDAO;
	private ITorneoService torneoService;
	private static Logger logger = Logger.getLogger(ExcelExporterService.class);

	public ExcelExporterService(ExporterDAO exporterDAO, ITorneoDAO torneoDAO, Mail mailService, 
			IPartidoDAO partidoDAO, ITorneoService torneoService) {
		this.exporterDAO = exporterDAO;
		this.torneoDAO = torneoDAO;
		this.mailService = mailService;
		this.partidoDAO = partidoDAO;
		this.torneoService = torneoService;
	}

	public ExporterDAO getExporterDAO() {
		return exporterDAO;
	}

	public void setExporterDAO(ExporterDAO exporterDAO) {
		this.exporterDAO = exporterDAO;
	}

	public Mail getMailService() {
		return mailService;
	}

	public void setMailService(Mail mailService) {
		this.mailService = mailService;
	}

	public String exportarEstadisticas(List<Long> idTorneos, List<Long> idFechas)
			throws Exception {
		List<PosicionTorneo> posicionesGenerales = new ArrayList<PosicionTorneo>();
		List<PosicionGoleador> posicionesGoleadoresGenerales = new ArrayList<PosicionGoleador>();
		Iterator<Long> itTorneos = idTorneos.iterator();
		Iterator<Long> itFechas = idFechas.iterator();
		ExcelWritter writter = new ExcelWritter(NOMBRE_ARCHIVO_TEMPLATE_ESTADISTICAS);
		writter.inicializar();

		exporterDAO.conectar();

		escribirFichasDePartidos(itFechas, writter);
		
		escribirTablas(posicionesGenerales, posicionesGoleadoresGenerales,
				itTorneos, writter);
		
		escribirTablasGenerales(posicionesGenerales,
				posicionesGoleadoresGenerales, writter);
		
		
		final int filaInicial = 2;
		final int columnaInicial = 1;
		final int filaEntreGrillas = 14;
		int cont = 0;
		int filaAEscribir = filaInicial;
		for(Long idTorneo : idTorneos){
			Torneo torneo = this.torneoDAO.findById(idTorneo);
			Matriz matrizEquipos = new Matriz();
			Matriz matrizResultados = new Matriz();
			List<String> filaResultadoEquipo;

			for(Equipo equipo: torneo.getEquipos()){
				ArrayList<String> fila = new ArrayList<String>();
				fila.add(equipo.getNombre());
				matrizEquipos.agregarFila(fila);
				
				filaResultadoEquipo = new ArrayList<String>();
				Iterator<Equipo> itEquipo = torneo.getEquipos().iterator();
				while(itEquipo.hasNext()){
					Equipo equipo2 = itEquipo.next();
					List<Partido> partidosFinalizados = this.partidoDAO.getAllFinalizados(equipo, equipo2);
					boolean agregoResultado = false;
					
					for(Partido partido : partidosFinalizados){
						if(partido.getEquipoLocal().equals(equipo) && partido.getFecha().getTorneo().equals(torneo)){
							filaResultadoEquipo.add(partido.getGolesLocal() + "-" + partido.getGolesVisitante());
							agregoResultado = true;
						}
					}
					if(!agregoResultado){
						filaResultadoEquipo.add("");
					}
				}
				matrizResultados.agregarFila(filaResultadoEquipo);
			}
			writter.escribirMatriz(matrizEquipos, 6, filaAEscribir, columnaInicial);
			matrizEquipos.trasponer();
			writter.escribirMatriz(matrizEquipos, 6, filaAEscribir - 1, columnaInicial + 1);
			writter.escribirMatriz(matrizResultados, 6, filaAEscribir, columnaInicial + 1);
			++cont;
			filaAEscribir = filaInicial + cont*filaEntreGrillas;
		}
		
		writter.save(NOMBRE_ARCHIVO_ESTADISTICAS);

		//mailService.enviarMailConAdjunto(mail, NOMBRE_ARCHIVO_ESTADISTICAS, NOMBRE_ARCHIVO_ESTADISTICAS);
		return NOMBRE_ARCHIVO_ESTADISTICAS;
	}

	private void escribirTablasGenerales(
			List<PosicionTorneo> posicionesGenerales,
			List<PosicionGoleador> posicionesGoleadoresGenerales,
			ExcelWritter writter) {
		Collections.sort(posicionesGenerales, new PosicionTorneoComparator());
		Matriz matrizPosiciones = new Matriz();
		int pos = 1;
		for(PosicionTorneo posicion : posicionesGenerales){
			posicion.setPosicion(pos++);
			matrizPosiciones.agregarFila(posicionTorneoToStringList(posicion));
		}
		writter.escribirMatriz(matrizPosiciones, 4, 4, 1);
		
		Collections.sort(posicionesGoleadoresGenerales, new PosicionGoleadorComparator());
		if(!posicionesGoleadoresGenerales.isEmpty()){
			if(posicionesGoleadoresGenerales.size() > 20){
				posicionesGoleadoresGenerales = posicionesGoleadoresGenerales.subList(0, 20);
		    }
			Matriz matrizPosGoleadores = new Matriz();
			pos = 1;
			for(PosicionGoleador posGol : posicionesGoleadoresGenerales){
				if(posGol.getJugador()!= null && posGol.getJugador().getActivo()){
					posGol.setPosicion(pos++);
					matrizPosGoleadores.agregarFila(posicionGoleadorToStringList(posGol));
				}
			}
			writter.escribirMatriz(matrizPosGoleadores, 5, 4, 1);
		}
	}

	private void escribirTablas(List<PosicionTorneo> posicionesGenerales,
			List<PosicionGoleador> posicionesGoleadoresGenerales,
			Iterator<Long> itTorneos, ExcelWritter writter) throws SQLException {
		while(itTorneos.hasNext()){
			Long idTorneo = itTorneos.next();
			
			ResultSet posiciones = exporterDAO.getPosiciones(idTorneo);
			writter.writePosiciones(posiciones);
			posiciones.close();
	
			ResultSet goleadores = exporterDAO.getGoleadores(idTorneo);
			writter.writeGoleadores(goleadores);
			goleadores.close();
	
			ResultSet tarjetas = exporterDAO.getTarjetas(idTorneo);
			writter.writeTarjetas(tarjetas);
			tarjetas.close();
			
			Torneo torneo = torneoDAO.findById(idTorneo);
			posicionesGenerales.addAll(torneo.getPosiciones());
			posicionesGoleadoresGenerales.addAll(torneo.getTablaGoleadores());
		}
	}

	private void escribirFichasDePartidos(Iterator<Long> itFechas, ExcelWritter writter)
			throws Exception, SQLException, IOException {
		while(itFechas.hasNext()){
			Long idFecha = itFechas.next();
		
			ResultSet results = exporterDAO.getResults(idFecha);
			writter.writeResults(results);
			results.close();
	
			ResultSet idsPartido = exporterDAO.getIdsPartido(idFecha);
			while (idsPartido.next()) {
				ResultSet players = exporterDAO.getPlayersLocales(idsPartido.getInt(1));
				writter.writePlayersLocales(players);
				players = exporterDAO.getPlayersVisitantes(idsPartido.getInt(1));
				writter.writePlayersVisitantes(players);
				players.close();
			}
			idsPartido.close();

		}
	}

	private List<String> posicionGoleadorToStringList(PosicionGoleador posGol) {
		List<String> stringList = new ArrayList<String>();
		stringList.add(posGol.getPosicion().toString());
		stringList.add(posGol.getJugador().toString());
		logger.debug("[posicionGoleadorToStringList] Jugador: " +  posGol.getJugador().toString());
		if(posGol.getJugador().getEquipo() != null){
			stringList.add(posGol.getJugador().getEquipo().toString());
		}else{
			logger.warn("[posicionGoleadorToStringList] Jugador: " + posGol.getJugador().toString() + " sin equipo." );
		}
		stringList.add(posGol.getGoles().toString());
		return stringList;
	}

	private List<String> posicionTorneoToStringList(PosicionTorneo posicion) {
		List<String> stringList = new ArrayList<String>();
		stringList.add(posicion.getPosicion().toString());
		stringList.add(posicion.getEquipo().getNombre());
		stringList.add(posicion.getPartidosJugados().toString());
		stringList.add(posicion.getPartidosGanados().toString());
		stringList.add(posicion.getPartidosEmpatados().toString());
		stringList.add(posicion.getPartidosPerdidos().toString());
		stringList.add(posicion.getGolesAFavor().toString());
		stringList.add(posicion.getGolesEnContra().toString());
		stringList.add(posicion.getDiferencia().toString());
		stringList.add(posicion.getPuntos().toString());
		stringList.add(posicion.getPorcentajePuntos().toString());
		return stringList;
	}

	private static final int rowInicialPartido = 0;
	private static final int rowInicialEquipos = 3;
	private static final int culumnInicialPartidos = 0;
	private static final int columnInicialEquipos = 2;
	private static final int rowsBetweenPartidos = 32;
	private static final int columnsBetweenEquipos = 10;
	private static final int sheet = 0;
	
	private static final int rowIncialJugadorPlanilla = 2;
	private static final int rowsBetweenEquipos = 29;
	private static final int rowIncialEquipoPlanilla = 0;
	private static final int columnEquipoPlanilla = 2;
	private static final int sheetPlanillas = 2;


	@Override
	public String exportarPlanilla(List<Partido> partidos) throws Exception {

		Collections.sort(partidos, new PartidoComparator());
		
		ExcelWritter writter = new ExcelWritter(NOMBRE_ARCHIVO_TEMPLATE_PLANILLAS);
		int row = rowInicialPartido;
		int column = culumnInicialPartidos;
		int rowJugadorPlanilla = rowIncialJugadorPlanilla;
		int rowEquipoPlanilla = rowIncialEquipoPlanilla;
		
		for (Partido partido : partidos) {
			writter.writeCell(row, column, sheet, partido.toString());
			row += rowsBetweenPartidos;
		}
		
		row = rowInicialEquipos;
		column = columnInicialEquipos;
		int i = 0;
		int j = 0;
		for (Partido partido : partidos) {
			column = columnInicialEquipos;
			row = rowInicialEquipos + i*rowsBetweenPartidos;
			rowJugadorPlanilla = rowIncialJugadorPlanilla + j*rowsBetweenEquipos;
			
			rowEquipoPlanilla = rowIncialEquipoPlanilla + j*rowsBetweenEquipos;
			writter.writeCell(rowEquipoPlanilla, columnEquipoPlanilla, sheetPlanillas, partido.getEquipoLocal().toString());
			
			for(Jugador jugador : partido.getEquipoLocal().getJugadores()){
				if(jugador.getActivo()){
					writter.writeCell(row, column, sheet, jugador.getNombre());
					writter.writeCell(row, column + 1, sheet, jugador.getApellido());
					writter.writeCell(rowJugadorPlanilla, 0, sheetPlanillas, jugador.getNombre());
					writter.writeCell(rowJugadorPlanilla, 1, sheetPlanillas, jugador.getApellido());
					++row;
					++rowJugadorPlanilla;
				}
			}
			row = rowInicialEquipos + i*rowsBetweenPartidos;
			column += columnsBetweenEquipos;
			
			rowJugadorPlanilla = rowIncialJugadorPlanilla + (j+1)*rowsBetweenEquipos;
			rowEquipoPlanilla = rowIncialEquipoPlanilla + (j+1)*rowsBetweenEquipos;
			writter.writeCell(rowEquipoPlanilla, columnEquipoPlanilla, sheetPlanillas, partido.getEquipoVisitante().toString());
			
			for(Jugador jugador : partido.getEquipoVisitante().getJugadores()){
				if(jugador.getActivo()){
					writter.writeCell(row, column, sheet, jugador.getNombre());
					writter.writeCell(row, column + 1, sheet, jugador.getApellido());
					writter.writeCell(rowJugadorPlanilla, 0, sheetPlanillas, jugador.getNombre());
					writter.writeCell(rowJugadorPlanilla, 1, sheetPlanillas, jugador.getApellido());
					++row;
					++rowJugadorPlanilla;
				}
			}
			++i;
			j = j + 2;
		}
		
		writter.save(NOMBRE_ARCHIVO_PLANILLAS);
		
		return NOMBRE_ARCHIVO_PLANILLAS;
		
		//mailService.enviarMailConAdjunto(mail, "Planillas de Partido", NOMBRE_ARCHIVO_PLANILLAS);
	}

	private static final int rowInicialEstadisticasHistoricas = 0;
	private static final int culumnInicialEstadisticasHistoricas = 1;
	private static final int rowsBetweenReportes = 24;
	
	@Override
	public String exportarEstadisticasHistoricas(
			List<Equipo> equipos) throws Exception {
		
		logger.info("Exportando Estadisticas Historicas");
		
		ExcelWritter writter = new ExcelWritter(NOMBRE_ARCHIVO_TEMPLATE_ESTADISTICAS_HISTORICAS);
		int row = rowInicialEstadisticasHistoricas;
		int column = culumnInicialEstadisticasHistoricas;

		int i = 0;
		for(Equipo equipo : equipos){
			ReporteHistorico reporteHistorico = torneoService.getReporteHistorico(equipo);
			
			writter.writeCell(row, column, 0, reporteHistorico.getEquipo().getNombre());
			
			Matriz matrizPosiciones = new Matriz();
			for(PosicionTorneo posicionTorneo : reporteHistorico.getPosiciones()){
				matrizPosiciones.agregarFila(this.posicionTorneoToFilaEstHis(posicionTorneo));
			}
			writter.escribirMatriz(matrizPosiciones, 0, row + 3, column);
			
			Matriz matrizJugadores = new Matriz();
			
			if(reporteHistorico.getReporteJugadores().size() > 14){
				reporteHistorico.setReporteJugadores(reporteHistorico.getReporteJugadores().subList(0, 13));
			}
			
			for(ReporteJugador repJugador : reporteHistorico.getReporteJugadores()){
				matrizJugadores.agregarFila(this.reporteJugadorToFila(repJugador));
			}
			writter.escribirMatriz(matrizJugadores, 0, row + 9, column);
			++i;
			row = rowInicialEstadisticasHistoricas + i*rowsBetweenReportes;
			
		}
		//writter.setCellTypeNumeric(3, 2, 6, 11, 0);
		writter.save(NOMBRE_ARCHIVO_ESTADISTICAS_HISTORICAS);
		//logger.info("Enviando Mail con estadisticas historicas");
		//mailService.enviarMailConAdjunto(mail, "Estadisticas Historicas", NOMBRE_ARCHIVO_ESTADISTICAS_HISTORICAS);
		//logger.info("Fin Estadisticas Historicas");
		return NOMBRE_ARCHIVO_ESTADISTICAS_HISTORICAS;
	}

	private List<String> reporteJugadorToFila(ReporteJugador repJugador) {
		List<String> list = new ArrayList<String>();
		list.add(repJugador.getJugador().toString());
		list.add(repJugador.getCantPartidos().toString());
		list.add("");
		list.add(repJugador.getCantGoles().toString());
		list.add("");
		list.add(Utils.formatDouble(repJugador.getGolesPromedio()).toString());
		list.add("");
		list.add(repJugador.getCantFigura().toString());
		list.add("");
		list.add(repJugador.getCantAmarillas().toString());
		list.add(repJugador.getCantRojas().toString());
		return list;
	}

	private List<String> posicionTorneoToFilaEstHis(
			PosicionTorneo posicionTorneo) {
		List<String> list = new ArrayList<String>();
		list.add(posicionTorneo.getTorneo().getNombre());
		list.add(posicionTorneo.getPosicion().toString());
		list.add(posicionTorneo.getPartidosJugados().toString());
		list.add(posicionTorneo.getPartidosGanados().toString());
		list.add(posicionTorneo.getPartidosEmpatados().toString());
		list.add(posicionTorneo.getPartidosPerdidos().toString());
		list.add(posicionTorneo.getGolesAFavor().toString());
		list.add(posicionTorneo.getGolesEnContra().toString());
		list.add(posicionTorneo.getDiferencia().toString());
		list.add(posicionTorneo.getPuntos().toString());
		list.add(posicionTorneo.getPorcentajePuntos().toString());
		return list;
	}
	
	

}
