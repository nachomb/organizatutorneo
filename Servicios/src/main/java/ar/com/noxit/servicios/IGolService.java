package ar.com.noxit.servicios;

import java.util.Map;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Gol;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;

public interface IGolService extends IGenericService<Gol> {

	void eliminarGoles(Partido partido);
	
    void generarGoles(Map<Long, Long> goleadores, Partido partido);

	Long countGoles(Jugador j);

	Long countGoles(Jugador j, Partido entity);

	void eliminarGoles(Partido partido, Equipo equipo);

}
