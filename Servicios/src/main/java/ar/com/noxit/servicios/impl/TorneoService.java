package ar.com.noxit.servicios.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.DatoReporte;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.PosicionTorneo;
import ar.com.noxit.picadito.dominio.entidades.Reporte;
import ar.com.noxit.picadito.dominio.entidades.ReporteHistorico;
import ar.com.noxit.picadito.dominio.entidades.ReporteJugador;
import ar.com.noxit.picadito.dominio.entidades.Sancion;
import ar.com.noxit.picadito.dominio.entidades.Tarjeta;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.picadito.dominio.entidades.comparator.ReporteJugadorComparator;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.IGolService;
import ar.com.noxit.servicios.IPartidoService;
import ar.com.noxit.servicios.IPuntajePartidoService;
import ar.com.noxit.servicios.ISancionService;
import ar.com.noxit.servicios.ITarjetaService;
import ar.com.noxit.servicios.dao.ITorneoDAO;

public class TorneoService extends AbstractService<Torneo> implements
		ITorneoService {

	private ITarjetaService tarjetaService;
	private ISancionService sancionService;
	private IGolService golService;
	private IPartidoService partidoService;
	private IPuntajePartidoService puntajePartidoService;

	public TorneoService(ITorneoDAO torneoDAO,ITarjetaService tarjetaService,
			IGolService golService,IPartidoService partidoService,IPuntajePartidoService puntajePartidoService,ISancionService sancionService) {
		this.genericDAO = torneoDAO;
		this.tarjetaService = tarjetaService;
		this.golService = golService;
		this.partidoService = partidoService;
		this.puntajePartidoService = puntajePartidoService;
		this.sancionService = sancionService;
	}

	public ITorneoDAO getTorneoDAO() {
		return (ITorneoDAO) this.genericDAO;
	}

	public void setTorneoDAO(ITorneoDAO torneoDAO) {
		this.genericDAO = torneoDAO;
	}

	@Override
	public void iniciarTorneo(Torneo torneo) {
		Torneo torn = getTorneoDAO().merge(torneo);
		torn.calcularFecha();
		torn.inicializarPosiciones();
		getTorneoDAO().update(torn);
	}

	@Override
	public void continuarTorneo(Torneo torneo) {
		if (torneo.estaIniciado()) {
			Torneo torn = getTorneoDAO().merge(torneo);
			torn.calcularSiguienteFecha();
			getTorneoDAO().update(torn);
		}
	}

	@Override
	public List<Torneo> getTorneosByUsuario(Usuario usuario) {
		return getTorneoDAO().getTorneosByUsuario(usuario);
	}

	@Override
	public void actualizarPosiciones(Long torneoId) {
		// TODO: arreglar esto que esta medio feo
		Torneo torneo = getTorneoDAO().findById(torneoId);
		if ("Todos contra Todos".equals(torneo.getTipoTorneo())){
			getTorneoDAO().actualizarPosiciones(torneoId);
		}else{
			getTorneoDAO().actualizarGoleadoresYTarjetas(torneoId);
		}
			
	}

	@Override
	public Torneo calcularTablaGoleadores(Torneo torneo) {
		Torneo tor = getTorneoDAO().merge(torneo);
		tor.calcularTablaGoleadores();
		return tor;
	}

	@Override
	public String getDatosGrafico(Torneo torneo, Jugador jugador) {
		String datosGrafico = "0,";
		int fechaActual = torneo.getFechaActual();
		for (int i = 1; i < fechaActual; ++i) {
			datosGrafico += torneo.getCantGoles(jugador, i);
			if (i < fechaActual - 1) {
				datosGrafico += ",";
			}
		}
		return datosGrafico;
	}

	@Override
	public String getFechas(Torneo torneo) {
		String fechas = "";
		int fechaActual = torneo.getFechaActual();
		for (int i = 1; i <= fechaActual; ++i) {
			fechas += i;
			if (i < fechaActual) {
				fechas += ",";
			}
		}
		return fechas;
	}

	@Override
	public Reporte generarReporte(Torneo torneo) {
		Reporte reporte = new Reporte();
		List<PosicionTorneo> posiciones = torneo.getPosiciones();
		List<Equipo> equipos = new ArrayList<Equipo>();
		for (PosicionTorneo pos : posiciones) {
			Equipo equipo = pos.getEquipo();
			DatoReporte dato = new DatoReporte();
			dato.setGolesAFavor(pos.getGolesAFavor());
			dato.setGolesEnContra(pos.getGolesEnContra());
			dato.setPorcentaje(pos.getPorcentajePuntos());
			//dato.setTarjetasAmarillas(tarjetaService.countTarjetasAmarillas(torneo,equipo));
			//dato.setTarjetasRojas(tarjetaService.countTarjetasRojas(torneo,equipo));
			equipo.setDatoReporte(dato);
			equipos.add(equipo);
		}
		reporte.calcular(equipos);
		return reporte;
	}

	@Override
	public ReporteHistorico getReporteHistorico(Equipo equipo) {
		
		ReporteHistorico reporteHistorico = new ReporteHistorico();
		reporteHistorico.setEquipo(equipo);
		List<PosicionTorneo> posiciones = new ArrayList<PosicionTorneo>();
		List<ReporteJugador> jugadores = new ArrayList<ReporteJugador>();
		Collection<Torneo> torneos = getTorneoDAO().getAllByEquipo(equipo);
		
		for(Torneo torneo:torneos){
			List<PosicionTorneo> posicionesTorneo = torneo.getPosiciones();
			int i = 1;
			for(PosicionTorneo pos: posicionesTorneo){
				if(equipo.equals(pos.getEquipo())){
					pos.setAmarillas(tarjetaService.countTarjetasAmarillas(torneo, equipo).intValue());
					pos.setRojas(tarjetaService.countTarjetasRojas(torneo, equipo).intValue());
					pos.setPosicion(i);
					posiciones.add(pos);
					break;
				}
				++i;
			}

		}
		
		for(Jugador j:equipo.getJugadores() ){
			ReporteJugador repJug = new ReporteJugador();
			repJug.setJugador(j);
			repJug.setCantAmarillas(tarjetaService.countTarjetasAmarillas(j).intValue());
			repJug.setCantRojas(tarjetaService.countTarjetasRojas(j).intValue());
			repJug.setCantGoles(golService.countGoles(j).intValue());
			repJug.setCantPartidos(partidoService.countPartidosJugados(j).intValue());
			repJug.setCantFigura(puntajePartidoService.countFigura(j).intValue());
			if(!repJug.getCantPartidos().equals(0)){
				Float golesPromedio = repJug.getCantGoles().floatValue() / repJug.getCantPartidos().floatValue();
				repJug.setGolesPromedio(golesPromedio);
				Float puntajePromedio = puntajePartidoService.sumaPuntajes(j) / repJug.getCantPartidos().floatValue();
				repJug.setPuntajePromedio(puntajePromedio);
			}else{
				repJug.setGolesPromedio(0F);
				repJug.setPuntajePromedio(0F);
			}
			jugadores.add(repJug);
		}
		Collections.sort(jugadores, new ReporteJugadorComparator());
		reporteHistorico.setPosiciones(posiciones);
		reporteHistorico.setReporteJugadores(jugadores);
		reporteHistorico.setEquipo(equipo);
		return reporteHistorico;
	}

	@Override
	public Torneo calcularTarjetas(Torneo torneo) {
		Torneo tor = getTorneoDAO().merge(torneo);
		List<Tarjeta> tarjetas = tarjetaService.findByTorneo(torneo);
		List<Jugador> jugadoresTarjeta = new ArrayList<Jugador>();
		Iterator<Tarjeta> it = tarjetas.iterator();
		while(it.hasNext()){
			Tarjeta tarjeta = it.next();
			Jugador jugador = tarjeta.getJugador();
			if(!jugadoresTarjeta.contains(jugador)){
				jugadoresTarjeta.add(jugador);	
			}
			if(tarjeta.getTipo().equals(Tarjeta.TIPO_AMARILLA)){
				jugador.incrementarAmarilla();
			}else{
				jugador.incrementarRoja();
			}
		}
		
		//torneo.setJugadoresTarjetas(jugadoresTarjeta);

		return tor;
	}
	
	@Override
	public Torneo calcularSanciones(Torneo torneo) {
		Torneo tor = getTorneoDAO().merge(torneo);
		List<Sancion> sanciones = sancionService.findByTorneo(torneo);
		torneo.setSanciones(sanciones);

		return tor;
	}

	@Override
	public List<Torneo> findActivos() {
		return getTorneoDAO().findActivos();
	}

	@Override
	public List<Torneo> findNoActivos() {
		return getTorneoDAO().findNoActivos();
	}

	@Override
	public List<Torneo> getTorneosByUsuarioCreador(Usuario usuario) {
		return getTorneoDAO().getTorneosByUsuarioCreador(usuario);
	}


}
