package ar.com.noxit.servicios.impl.algoritmosgeneticos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.uncommons.watchmaker.framework.CandidateFactory;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import ar.com.noxit.base.utils.Utils;

public class PartidosFactory extends AbstractCandidateFactory<List<PartidoDTO>> implements
		CandidateFactory<List<PartidoDTO>> {
	
	private List<PartidoDTO> partidos;
	private int cantCanchas;
	private int duracionPartidos;
	private Date fechaInicial;
	
	
	public PartidosFactory(List<PartidoDTO> partidos, int cantCanchas, int duracionPartidos, Date fechaInicial) {
		this.partidos = partidos;
		this.cantCanchas = cantCanchas;
		this.duracionPartidos = duracionPartidos;
		this.fechaInicial = fechaInicial;
	}

	@Override
	public List<List<PartidoDTO>> generateInitialPopulation(int populationSize,
			Collection<List<PartidoDTO>> seedCandidates, Random rng) {
		
		List<List<PartidoDTO>> initialPopulation = new ArrayList<List<PartidoDTO>>();
		
		initialPopulation.addAll(seedCandidates);
		if(rng == null){
			return initialPopulation.subList(0, populationSize);
		}
		
		while(populationSize != initialPopulation.size()){
			List<PartidoDTO> candidato = generateRandomCandidate(rng);
			initialPopulation.add(candidato);
		}
		
		return initialPopulation;
	}

	@Override
	public List<PartidoDTO> generateRandomCandidate(Random rng) {
		Date fechaPartido = fechaInicial;
		Collections.shuffle(partidos, rng);
		Iterator<PartidoDTO> itPartido = this.partidos.iterator();
		List<PartidoDTO> candidato = new ArrayList<PartidoDTO>(); 
		
		while (itPartido.hasNext()) {
			for (int i = 1; i <= cantCanchas && itPartido.hasNext(); ++i) {
				PartidoDTO partido = itPartido.next();
				PartidoDTO partidoNuevo = new PartidoDTO(partido.getId());
				partidoNuevo.setFechaPartido(fechaPartido);
				partidoNuevo.setNumeroCancha(i);
				candidato.add(partidoNuevo);
			}
			fechaPartido = Utils.addMinutes(fechaPartido, duracionPartidos);
		}
		return candidato;
	}

}
