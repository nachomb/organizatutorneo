package ar.com.noxit.servicios.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.ResultadoImportacion;
import ar.com.noxit.servicios.IEquipoService;
import ar.com.noxit.servicios.IImporterService;
import ar.com.noxit.servicios.IJugadorService;

public class ImporterService implements IImporterService {

	private IJugadorService jugadorService;
	private IEquipoService equipoService;
	private static Logger logger = Logger.getLogger(ImporterService.class);

	public ImporterService(IJugadorService jugadorService,
			IEquipoService equipoService) {
		this.jugadorService = jugadorService;
		this.equipoService = equipoService;
	}

	@Override
	public ResultadoImportacion importarJugadoresYEquipos(File file) throws IOException {
		FileInputStream fis = null;
		List<List<HSSFCell>> sheetData = new ArrayList<List<HSSFCell>>();
		try {
			fis = new FileInputStream(file);
			HSSFWorkbook workbook = new HSSFWorkbook(fis);

			HSSFSheet sheet = workbook.getSheetAt(0);

			//
			// When we have a sheet object in hand we can iterator on
			// each sheet's rows and on each row's cells. We store the
			// data read on an ArrayList so that we can printed the
			// content of the excel to the console.
			//
			Iterator<Row> rows = sheet.rowIterator();
			while (rows.hasNext()) {
				HSSFRow row = (HSSFRow) rows.next();
				Iterator<Cell> cells = row.cellIterator();

				List<HSSFCell> data = new ArrayList<HSSFCell>();
				while (cells.hasNext()) {
					HSSFCell cell = (HSSFCell) cells.next();
					data.add(cell);
				}

				sheetData.add(data);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				fis.close();
			}
		}
		return importar(sheetData);
	}

	protected ResultadoImportacion importar(List<List<HSSFCell>> sheetData) {

		int cantEquiposCreados = 0;
		int cantJugadoresCreados = 0;
		
		for (int i = 1; i < sheetData.size(); i++) {
			List<HSSFCell> list = sheetData.get(i);
			Jugador jugador = new Jugador();
			Equipo equipo = new Equipo();
			logger.debug("Leyendo fila: " + i);
			for (int j = 0; j < list.size(); j++) {
				HSSFCell cell = list.get(j);
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					logger.debug("Leyendo columna:"  + j +  " dato: " + cell.getRichStringCellValue());
					if(j == 0){
						String nombreEquipo = cell.getRichStringCellValue().getString();
						Equipo equipoBd = equipoService.getEquipoPorNombre(nombreEquipo);
						if (equipoBd != null){
							jugador.setEquipo(equipoBd);
						}else{
							equipo.setNombre(nombreEquipo);
							equipo.setActivo(true);
							//logger.info("Creando Equipo: " + cell.getRichStringCellValue());
							equipoService.persist(equipo);
							jugador.setEquipo(equipo);
							++cantEquiposCreados;
						}
					}else if(j == 1){
						jugador.setApellido(cell.getRichStringCellValue().getString());
					}else if(j == 2){
						jugador.setNombre(cell.getRichStringCellValue().getString());
					}
					
				}
				
				if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
					if(j == 3){
						jugador.setDni(String.valueOf(Math.round(cell.getNumericCellValue())));
					}
				}
				
			}
			if(jugador.getNombre() != null && jugador.getApellido() != null && jugador.getDni() != null){
				Jugador jugadorBd = jugadorService.getJugadorByDni(jugador.getDni());
				if(jugadorBd == null){
					//logger.info("Persistiendo jugador " + jugador + " del equipo " + jugador.getEquipo());
					jugadorService.persist(jugador);
					++cantJugadoresCreados;
				}
			}
		}
		
		return new ResultadoImportacion(cantEquiposCreados, cantJugadoresCreados);
	}

}
