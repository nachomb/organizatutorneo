package ar.com.noxit.servicios.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.PuntajePartido;
import ar.com.noxit.servicios.dao.IPuntajePartidoDAO;

public class PuntajePartidoDAO extends AbstractDAO<PuntajePartido> implements IPuntajePartidoDAO {

    @Override
    public void persistList(List<PuntajePartido> puntajes) {
        getHibernateTemplate().saveOrUpdateAll(puntajes);
    }

	@Override
	public PuntajePartido findByJugadorPartido(Jugador j, Partido partido) {
        Criteria criteria = this.getSession().createCriteria(PuntajePartido.class);
        criteria.add(Restrictions.eq("jugador", j))
        .add(Restrictions.eq("partido", partido));
        criteria.setMaxResults(1);
        return (PuntajePartido) criteria.uniqueResult();
	}

	@Override
	public Integer sumaPuntajes(Jugador j) {
		Criteria criteria = this.getSession().createCriteria(PuntajePartido.class);
		criteria.add(Restrictions.eq("jugador", j))
		.setProjection(Projections.sum("puntaje"));
		
		List list = criteria.list();
		if(list == null || list.isEmpty() || list.get(0) == null)
			return 0;
        return ((Integer) list.get(0)).intValue();
		
	}

	@Override
	public Integer countFigura(Jugador j) {
		Criteria criteria = this.getSession().createCriteria(PuntajePartido.class);
		criteria.add(Restrictions.eq("jugador", j))
		.setProjection(Projections.count("jugador"))
		.add(Restrictions.eq("figura", true));
		if(criteria.list() == null || criteria.list().isEmpty())
			return 0;
        return ((Integer) criteria.list().get(0));
	}

	@Override
	public void borrarPuntajes(Partido partido) {
        String queryString = " delete from Partido p where p.id = :idPartido ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("idPartido", partido.getId());
        query.executeUpdate();
	}

	@Override
	public void borrarPuntajes(Partido partido, Equipo equipo) {
        String queryString = " delete from PuntajePartido p where p.partido = :partido and jugador in (:jugadores)";
        Query query = getSession().createQuery(queryString);
        query.setParameter("partido", partido);
        query.setParameterList("jugadores", equipo.getJugadores());
        Integer cantidad = query.executeUpdate();
		logger.info("Se borraron " + cantidad + " de registros de la tabla puntaje_partido del partido " + partido);
	}
	
}
