package ar.com.noxit.servicios.impl.algoritmosgeneticos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;

public class OperadorEvolucionario implements
		EvolutionaryOperator<List<PartidoDTO>> {

	@Override
	public List<List<PartidoDTO>> apply(
			List<List<PartidoDTO>> selectedCandidates, Random rng) {
		
		List<List<PartidoDTO>> candidatosEvolucionados = new ArrayList<List<PartidoDTO>>();
		
		Iterator<List<PartidoDTO>> itCandidates = selectedCandidates.iterator();
		while(itCandidates.hasNext()){
			List<PartidoDTO> candidatoSeleccionado = itCandidates.next();
			List<PartidoDTO> candidatoEvolucionado = new ArrayList<PartidoDTO>(candidatoSeleccionado);
			modificar(candidatoEvolucionado,rng);
			candidatosEvolucionados.add(candidatoEvolucionado);
		}
		
		
		return candidatosEvolucionados;
	}

	private void modificar(List<PartidoDTO> candidatoEvolucionado, Random rng) {
		int pos1 = Math.abs(rng.nextInt() % candidatoEvolucionado.size());
		PartidoDTO partidoDTO1 = candidatoEvolucionado.get(pos1);
		int pos2 = Math.abs(rng.nextInt() % candidatoEvolucionado.size());
		PartidoDTO partidoDTO2 = candidatoEvolucionado.get(pos2);
		swapIds(partidoDTO1, partidoDTO2);
		candidatoEvolucionado.set(pos1, partidoDTO1);
		candidatoEvolucionado.set(pos2, partidoDTO2);
	}

	private void swapIds(PartidoDTO partidoDTO1, PartidoDTO partidoDTO2) {
		Long idAux = partidoDTO1.getId();
		partidoDTO1.setId(partidoDTO2.getId());
		partidoDTO2.setId(idAux);
	}

}
