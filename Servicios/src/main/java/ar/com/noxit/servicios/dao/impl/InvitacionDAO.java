package ar.com.noxit.servicios.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Invitacion;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.dao.IInvitacionDAO;

public class InvitacionDAO extends AbstractDAO<Invitacion> implements IInvitacionDAO {

    @SuppressWarnings("unchecked")
    @Override
    public List<Invitacion> findByUsuario(Usuario usuario) {
        String queryString = "select i from " + this.getEntity().getSimpleName() + " i "
        + " where i.jugador.usuario = :usuario" +
        " and i.partidoAmistoso.fecha > :fechaActual";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("usuario", usuario);
        query.setParameter("fechaActual", new Date());
        return (List<Invitacion>) this.doList(query);
    }

    @Override
    public Invitacion getInvitacionByIdPartidoYNumero(String numero, Long idPartido) {
        String queryString = "from " + this.getEntity().getSimpleName() + " i "
                + " where i.jugador.usuario.telefonoCelular = :numero and i.partidoAmistoso.id = :idPartido ";
        return (Invitacion) getSession().createQuery(queryString).setParameter("numero", numero)
                .setParameter("idPartido", idPartido).uniqueResult();

    }

    @Override
    public List<Invitacion> getAllInvitacionesAEnviar() {
        String queryString = "from " + this.getEntity().getSimpleName() + " i " + " where i.estado = :pendiente ";
        return (List<Invitacion>) getSession().createQuery(queryString)
                .setParameter("pendiente", Invitacion.ESTADO_PENDIENTE).list();
    }

}
