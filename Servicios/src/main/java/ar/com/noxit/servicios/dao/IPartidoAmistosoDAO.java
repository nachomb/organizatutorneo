package ar.com.noxit.servicios.dao;

import java.util.Collection;
import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.PartidoAmistoso;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IPartidoAmistosoDAO extends IGenericDAO<PartidoAmistoso, Long> {

	public List<PartidoAmistoso> getPartidosByUsuario(Usuario usuario, Jugador jugador);

	public List<PartidoAmistoso> getPartidosJugadosByUsuario(Usuario usuario, Jugador jugador);

	public Integer getCountPartidosByUsuario(Usuario usuario, Jugador jugador);

    public Integer getCountPartidosJugadosByUsuario(Usuario usuario, Jugador jugador);
	
    public List<PartidoAmistoso> getAllPartidosJugados();

    public Integer getCountAllPartidosJugados();

    public Collection<PartidoAmistoso> getAllPartidosNoJugados();

    public Integer getCountAllPartidosNoJugados();

}
