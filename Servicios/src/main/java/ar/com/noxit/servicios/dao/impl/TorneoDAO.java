package ar.com.noxit.servicios.dao.impl;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.dao.ITorneoDAO;

@SuppressWarnings("unchecked")
public class TorneoDAO extends AbstractDAO<Torneo> implements ITorneoDAO {

	@Override
    @Transactional(readOnly = true)
    public List<Torneo> getAllPaginado() {
        return (List<Torneo>) this.doList(getQueryAllOrderByActivo());
    }

	private Query getQueryAllOrderByActivo() {
        Query query = this.getSession().createQuery("from " + this.getEntity().getSimpleName() + " order by activo desc, id");
        return query;
	}

    @Override
    public List<Torneo> getTorneosByUsuario(Usuario usuario) {
        String queryString = "select t from " + this.getEntity().getSimpleName() + " t "
        + " inner join t.equipos e "
        + " inner join e.jugadores j "
        + " inner join j.usuario u "
        + " where u.id = :usuarioId "
        + " order by t.activo desc, t.id";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("usuarioId", usuario.getId());
        return (List<Torneo>) this.doList(query);
    }

	@Override
	public void actualizarPosiciones(Long torneoId) {
		Query query = this.getSession().createSQLQuery("call actualizar_posiciones(?)");
		query.setLong(0, torneoId);
		query.executeUpdate();
		
		actualizarGoleadoresYTarjetas(torneoId);
		
	}

	@Override
	public void actualizarGoleadoresYTarjetas(Long torneoId) {
		Query queryGoleadores = this.getSession().createSQLQuery("call actualizar_goleadores(?)");
		queryGoleadores.setLong(0, torneoId);
		queryGoleadores.executeUpdate();
		
		Query queryTarjetas = this.getSession().createSQLQuery("call actualizar_tarjetas(?)");
		queryTarjetas.setLong(0, torneoId);
		queryTarjetas.executeUpdate();
	}

	@Override
	public Collection<Torneo> getAllByEquipo(Equipo equipo) {
		String queryString = "select t from " + this.getEntity().getSimpleName() + " t "
        + " inner join t.equipos e "
        + " where e.id = :equipoId"; 
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("equipoId", equipo.getId());
        return (Collection<Torneo>) this.doList(query);
	}

	@Override
	public List<Torneo> findActivos() {
		Criteria criteria = getSession().createCriteria(Torneo.class);
		criteria.add(Restrictions.eq("activo", true));
		return criteria.list();
	}

	@Override
	public List<Torneo> findNoActivos() {
		Criteria criteria = getSession().createCriteria(Torneo.class);
		criteria.add(Restrictions.eq("activo", false));
		return criteria.list();

	}

	@Override
	public List<Torneo> getTorneosByUsuarioCreador(Usuario usuario) {
		Criteria criteria = getSession().createCriteria(Torneo.class);
		criteria.add(Restrictions.eq("creador", usuario));
		return criteria.list();
	}


}
