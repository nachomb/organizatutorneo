package ar.com.noxit.servicios.dao;

import java.util.Collection;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IJugadorDAO extends IGenericDAO<Jugador, Long> {

	Collection<Jugador> getAllJugadoresPosibles();

	Jugador getjugadorByUsuario(Usuario usuario);

	Jugador getjugadorByNombreUsuario(String nombreUsuario);

	Collection<Jugador> getJugadorLike(Jugador jugador);

	Jugador getJugadorByDni(String dni);

	Collection<Jugador> getJugadoresByUsuarioCreador(Usuario usuario);

	Collection<Jugador> getJugadoresByUsuarioCreadorSinEquipo(Usuario usuario);
}
