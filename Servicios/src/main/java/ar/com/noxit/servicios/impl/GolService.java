package ar.com.noxit.servicios.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Gol;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.servicios.IGolService;
import ar.com.noxit.servicios.dao.IGolDAO;
import ar.com.noxit.servicios.dao.IJugadorDAO;

public class GolService extends AbstractService<Gol> implements IGolService {

    private IJugadorDAO jugadorDAO;

    public GolService(IGolDAO golDAO, IJugadorDAO jugadorDAO) {
        this.genericDAO = golDAO;
        this.jugadorDAO = jugadorDAO;
    }

    public IGolDAO getGolDAO() {
        return (IGolDAO) this.genericDAO;
    }

    public void setGolDAO(IGolDAO golDAO) {
        this.genericDAO = golDAO;
    }

    @Override
    public void generarGoles(Map<Long, Long> goleadores, Partido partido) {
        // Elimino goles para no duplicar y luego los vuelvo a cargar
        List<Gol> goles = new ArrayList<Gol>();
        Iterator<Long> itJug = goleadores.keySet().iterator();
        
        while(itJug.hasNext()){
        	Long idJugador = itJug.next();
        	Jugador jugador = this.jugadorDAO.findById(idJugador);
        	Long cantGoles = goleadores.get(idJugador);
        	for(int i = 1; i <= cantGoles ; ++i){
        		goles.add(new Gol(jugador, partido, jugador.getEquipo()));
        	}
        }
        
        getGolDAO().persistList(goles);
    }
    
    public void setJugadorDAO(IJugadorDAO jugadorDAO) {
        this.jugadorDAO = jugadorDAO;
    }

    public IJugadorDAO getJugadorDAO() {
        return jugadorDAO;
    }

	@Override
	public Long countGoles(Jugador j) {
		return getGolDAO().countGoles(j);
	}

	@Override
	public Long countGoles(Jugador j, Partido partido) {
		return getGolDAO().countGoles(j,partido);
	}

	@Override
	public void eliminarGoles(Partido partido) {
		getGolDAO().eliminarGoles(partido);
		
	}

	@Override
	public void eliminarGoles(Partido partido, Equipo equipo) {
		getGolDAO().eliminarGoles(partido, equipo);
		
	}

}
