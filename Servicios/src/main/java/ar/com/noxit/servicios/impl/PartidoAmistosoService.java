package ar.com.noxit.servicios.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Invitacion;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.PartidoAmistoso;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.IPartidoAmistosoService;
import ar.com.noxit.servicios.dao.IPartidoAmistosoDAO;

public class PartidoAmistosoService extends AbstractService<PartidoAmistoso> implements IPartidoAmistosoService {

    public PartidoAmistosoService(IPartidoAmistosoDAO partidoAmistosoDAO) {
        this.genericDAO = partidoAmistosoDAO;
    }

    public IPartidoAmistosoDAO getPartidoAmistosoDAO() {
        return (IPartidoAmistosoDAO) this.genericDAO;
    }

    public void setPartidoAmistosoDAO(IPartidoAmistosoDAO partidoAmistosoDAO) {
        this.genericDAO = partidoAmistosoDAO;
    }

    @Override
    public List<PartidoAmistoso> getPartidosByUsuario(Usuario usuario, Jugador jugador) {
        return getPartidoAmistosoDAO().getPartidosByUsuario(usuario, jugador);
    }

    @Override
    public List<PartidoAmistoso> getPartidosJugadosByUsuario(Usuario usuario, Jugador jugador) {
        return getPartidoAmistosoDAO().getPartidosJugadosByUsuario(usuario, jugador);
    }

    @Override
    public List<PartidoAmistoso> getAllPartidosJugados() {
        return getPartidoAmistosoDAO().getAllPartidosJugados();
    }

    @Override
    public Integer getCountAllPartidosJugados() {
        return getPartidoAmistosoDAO().getCountAllPartidosJugados();
    }

    @Override
    public void cargarAlineaciones(PartidoAmistoso partidoAmistoso, List<Long> idsJugadoresEquipoA,
            List<Long> idsJugadoresEquipoB) {
        getPartidoAmistosoDAO().merge(partidoAmistoso);
        List<Jugador> equipoA = new ArrayList<Jugador>();
        List<Jugador> equipoB = new ArrayList<Jugador>();
        for (Invitacion inv : partidoAmistoso.getInvitaciones()) {
            Jugador jug = inv.getJugador();
            if (idsJugadoresEquipoA.contains(jug.getId())) {
                equipoA.add(jug);
            } else if (idsJugadoresEquipoB.contains(jug.getId())) {
                equipoB.add(jug);
            }
        }
        partidoAmistoso.asignarAlineaciones(equipoA, equipoB);
        getPartidoAmistosoDAO().update(partidoAmistoso);

    }

    @Override
    public Integer getCountPartidosByUsuario(Usuario usuario, Jugador jugador) {
        return getPartidoAmistosoDAO().getCountPartidosByUsuario(usuario, jugador);
    }

    @Override
    public Integer getCountPartidosJugadosByUsuario(Usuario usuario, Jugador jugador) {
        return getPartidoAmistosoDAO().getCountPartidosJugadosByUsuario(usuario, jugador);
    }

    @Override
    public Collection<PartidoAmistoso> getAllPartidosNoJugados() {
        return getPartidoAmistosoDAO().getAllPartidosNoJugados();
    }

    @Override
    public Integer getCountAllPartidosNoJugados() {
        return getPartidoAmistosoDAO().getCountAllPartidosNoJugados();
    }

}
