package ar.com.noxit.servicios;

import java.util.Date;
import java.util.List;

import ar.com.noxit.servicios.impl.algoritmosgeneticos.PartidoDTO;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.Restriccion;

public interface IProgramacionPartidosService {

	List<PartidoDTO> obtenerPosibleProgramacion(List<PartidoDTO> partidos, 
			List<Restriccion> restricciones, int cantCanchas, int duracionPartido,
			Date fechaInicial);
	
	Boolean cumpleTodasRestricciones(List<PartidoDTO> partidos, List<Restriccion> restricciones);
}
