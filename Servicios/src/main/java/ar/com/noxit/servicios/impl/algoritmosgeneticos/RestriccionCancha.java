package ar.com.noxit.servicios.impl.algoritmosgeneticos;

import java.util.Date;

public class RestriccionCancha extends Restriccion {

	public RestriccionCancha(String operador, PartidoDTO partido, String valor) {
		super(operador, partido, valor);
	}

	@Override
	protected boolean esValida(PartidoDTO part){
		return part.getNumeroCancha().equals(Integer.parseInt(valor));
	}

	@Override
	protected void aplicar(PartidoDTO partidoDTO, Date fechaInicial) {
		partidoDTO.setNumeroCancha(Integer.parseInt(valor));
	}

}
