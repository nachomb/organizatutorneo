package ar.com.noxit.servicios.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Alineacion;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.PuntajePartido;
import ar.com.noxit.servicios.IPuntajePartidoService;
import ar.com.noxit.servicios.dao.IPuntajePartidoDAO;

public class PuntajePartidoService extends AbstractService<PuntajePartido> implements IPuntajePartidoService {

	public PuntajePartidoService(IPuntajePartidoDAO puntajePartidoDAO){
		this.genericDAO = puntajePartidoDAO;
	}

	@Override
	public void generarPuntajes(Map<Long, Long> numJugadores, List<Long> puntJugadores, Jugador figura, Partido partido, Equipo equipo, Alineacion alineacion) {
		Iterator<Jugador> itJugadores = equipo.getJugadores().iterator();
        List<PuntajePartido> puntajes = new ArrayList<PuntajePartido>();
        //Iterator<Long> itNumeros = numJugadores.keySet().iterator();
        
        for (Long punt : puntJugadores) {
        	Jugador jugador = itJugadores.next();
        	Long numero = numJugadores.get(jugador.getId()) != null ? numJugadores.get(jugador.getId()) : 0;
        	if(alineacion.getTitulares().contains(jugador) || alineacion.getSuplentes().contains(jugador)){
        		PuntajePartido puntajePartido = findByJugadorPartido(jugador, partido);
        		Boolean esFigura = figura.equals(jugador);
        		if(puntajePartido == null){
        			puntajePartido = new PuntajePartido(jugador, partido, punt.intValue(), esFigura, numero.intValue());
        		}else{
        			puntajePartido.setPuntaje(punt.intValue());
        			puntajePartido.setFigura(esFigura);
        			puntajePartido.setNumero(numero.intValue());
        		}
        		puntajes.add(puntajePartido);
        	}
        }
        getPuntajePartidoDAO().persistList(puntajes);
		
	}

	@Override
	public PuntajePartido findByJugadorPartido(Jugador j, Partido partido) {
		return getPuntajePartidoDAO().findByJugadorPartido(j,partido);
	}

	@Override
	public Integer sumaPuntajes(Jugador j) {
		return getPuntajePartidoDAO().sumaPuntajes(j);
	}

	@Override
	public Integer countFigura(Jugador j) {
		return getPuntajePartidoDAO().countFigura(j);
	}

	private IPuntajePartidoDAO getPuntajePartidoDAO() {
		return (IPuntajePartidoDAO)this.genericDAO;
	}

	@Override
	public void borrarPuntajes(Partido partido) {
		this.getPuntajePartidoDAO().borrarPuntajes(partido);
		
	}

	@Override
	public void borrarPuntajes(Partido partido, Equipo equipo) {
		this.getPuntajePartidoDAO().borrarPuntajes(partido, equipo);
		
	}
	
}
