package ar.com.noxit.servicios;

import java.util.List;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;

public interface IPartidoService extends IGenericService<Partido> {

    void cargarAlineaciones(Partido partido, List<Long> idsTitularesLocales, List<Long> idsTitularesVisitantes, List<Long> idsSuplentesLocales, List<Long> idsSuplentesVisitantes);

    List<String> getDatosGraficoHistorial(Partido partido);

	Long countPartidosJugados(Jugador j);
    
}
