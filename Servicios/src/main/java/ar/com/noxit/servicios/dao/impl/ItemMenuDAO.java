package ar.com.noxit.servicios.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.ItemMenu;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Accion;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.servicios.dao.IItemMenuDAO;

public class ItemMenuDAO extends AbstractDAO<ItemMenu> implements IItemMenuDAO {

    @Override
    public List<ItemMenu> findAllItemsMenuByPadre(Collection<Perfil> perfiles, ItemMenu itemMenu) {
        String query = "";
        List<ItemMenu> items = null;
        List<Accion> acciones = new ArrayList<Accion>();
        Iterator<Perfil> it = perfiles.iterator();

        while (it.hasNext()) {
            Perfil p = it.next();
            acciones.addAll(p.getAcciones());
        }
        
        if (acciones.isEmpty()) { // Nunca va a ser null pero puede estar vacia
            return items;
        }

        if (itemMenu == null) {
            query = "from ItemMenu im where im.itemMenuPadre is null and im.accion in (:acciones) order by im.orden asc";
            items = this.getHibernateTemplate().findByNamedParam(query, "acciones", acciones);
            return items;
        }

        query = "from ItemMenu im where im.itemMenuPadre = :padre and im.accion in (:acciones) order by im.orden asc";
        String[] paramNames = { "padre", "acciones" };
        Object[] values = { itemMenu, acciones };
        items = this.getHibernateTemplate().findByNamedParam(query, paramNames, values);
        return items;

    }

}
