package ar.com.noxit.servicios.dao;

import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Gol;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;


public interface IGolDAO extends IGenericDAO<Gol, Long> {

    void persistList(List<Gol> goles);

	Long countGoles(Jugador j);

    void eliminarGoles(Partido partido);

    Long countGoles(Jugador j, Partido partido);

	void eliminarGoles(Partido partido, Equipo equipo);

}
