package ar.com.noxit.servicios.impl.algoritmosgeneticos;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class PartidoDTO {

	private Long id;
	private Date fechaPartido;
	private Integer numeroCancha;
	
	public Integer getNumeroCancha() {
		return numeroCancha;
	}
	public void setNumeroCancha(Integer numeroCancha) {
		this.numeroCancha = numeroCancha;
	}
	public PartidoDTO(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescripcion() {
		StringBuilder builder = new StringBuilder();
		builder.append("Cancha ").append(numeroCancha);
		return builder.toString();
	}
	
	public Date getFechaPartido() {
		return fechaPartido;
	}
	public void setFechaPartido(Date fechaPartido) {
		this.fechaPartido = fechaPartido;
	}
	
	public boolean equals(Object obj) {
		PartidoDTO otro = (PartidoDTO) obj;
		return this.id.equals(otro.getId());
	}
	
	@Override
	public String toString() {
	    return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
}
