package ar.com.noxit.servicios.impl.algoritmosgeneticos;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uncommons.watchmaker.framework.EvolutionObserver;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.PopulationData;

public class ObservadorDeEvolucion implements EvolutionObserver<List<PartidoDTO>> {

    private static final Logger logger = LoggerFactory.getLogger(ObservadorDeEvolucion.class);
    private FitnessEvaluator<List<PartidoDTO>> fitnessEvaluator;
    
	public ObservadorDeEvolucion(FitnessEvaluator<List<PartidoDTO>> fitnessEvaluator) {
		super();
		this.fitnessEvaluator = fitnessEvaluator;
	}


	@Override
	public void populationUpdate(PopulationData<? extends List<PartidoDTO>> data) {
		double fitness = this.fitnessEvaluator.getFitness(data.getBestCandidate(), null);
		logger.info("Generacion numero: " + data.getGenerationNumber());
		logger.info("Tiempo: " + data.getElapsedTime());
		logger.info("Aptitud Calculada por el fittnessEvaluator: " + fitness);
	}

}
