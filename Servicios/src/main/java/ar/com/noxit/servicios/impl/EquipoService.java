package ar.com.noxit.servicios.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.IEquipoService;
import ar.com.noxit.servicios.dao.IEquipoDAO;

public class EquipoService extends AbstractService<Equipo> implements IEquipoService {

    public EquipoService(IEquipoDAO equipoDAO) {
        this.genericDAO = equipoDAO;
    }

    public IEquipoDAO getEquipoDAO() {
        return (IEquipoDAO) this.genericDAO;
    }

    public void setEquipoDAO(IEquipoDAO equipoDAO) {
        this.genericDAO = equipoDAO;
    }

    public Equipo getEquipoPorNombre(String nombre) {
        return getEquipoDAO().getEquipoPorNombre(nombre);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean esEquipoDuplicado(Equipo entity) {
        Equipo equipo = ((IEquipoDAO) this.genericDAO).getEquipoPorNombre(entity.getNombre());
        Long id = entity.getId();
        if (id == null && equipo != null) {
            return true;
        } else if (id != null && equipo != null) {
            return !equipo.getId().equals(id);
        }

        return false;

    }

    @Override
    public List<Equipo> getEquiposByUsuario(Usuario usuario) {
        return getEquipoDAO().getEquiposByUsuario(usuario);
    }

	@Override
	public List<Equipo> getAllActivos() {
		return getEquipoDAO().getAllActivos();
	}

	@Override
	public Collection<Equipo> getAllOrderByActivoYNombre() {
		return getEquipoDAO().getAllOrderByActivoYNombre();
	}

	@Override
	public List<Equipo> getAllActivosOrderByNombre() {
		return getEquipoDAO().getAllActivosOrderByNombre();
	}

	@Override
	public List<Equipo> getEquiposByUsuarioCreador(Usuario usuario) {
		return getEquipoDAO().getEquiposByUsuarioCreador(usuario);
	}
}
