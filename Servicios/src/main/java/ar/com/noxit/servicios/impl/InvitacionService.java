package ar.com.noxit.servicios.impl;

import java.util.List;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Invitacion;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.IInvitacionService;
import ar.com.noxit.servicios.dao.IInvitacionDAO;

public class InvitacionService extends AbstractService<Invitacion> implements IInvitacionService {

    public InvitacionService(IInvitacionDAO invitacionDAO) {
        this.genericDAO = invitacionDAO;
    }

    @Override
    public void cambiarEstadoAPendiente(Invitacion invitacion) {
        invitacion.cambiarEstadoAPendiente();
        this.genericDAO.update(invitacion);

    }

    @Override
    public void cambiarEstadoAConfirmado(Invitacion invitacion) {
        invitacion.cambiarEstadoAConfirmado();

    }

    @Override
    public void cambiarEstadoACancelado(Invitacion invitacion) {
        invitacion.cambiarEstadoACancelado();
    }

    @Override
    public void cambiarEstadoADuda(Invitacion invitacion) {
        invitacion.cambiarEstadoADuda();
    }

    @Override
    public void cambiarEstadoAEnviado(Invitacion invitacion) {
        invitacion.cambiarEstadoAEnviado();
    }

    @Override
    public List<Invitacion> findByUsuario(Usuario usuario) {
        return ((IInvitacionDAO) this.genericDAO).findByUsuario(usuario);
    }

    @Override
    public Invitacion getInvitacionByIdPartidoYNumero(String numero, Long idPartido) {
        return ((IInvitacionDAO) this.genericDAO).getInvitacionByIdPartidoYNumero(numero, idPartido);
    }

    @Override
    public List<Invitacion> getAllInvitacionesAEnviar() {
        return ((IInvitacionDAO) this.genericDAO).getAllInvitacionesAEnviar();
    }

}
