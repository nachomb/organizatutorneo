package ar.com.noxit.servicios;

import java.util.List;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Publicidad;

public interface IPublicidadService extends IGenericService<Publicidad> {

	public List<Publicidad> getPublicidadesVerticalesActivas();

	public List<Publicidad> getPublicidadesSliderActivas();
}
