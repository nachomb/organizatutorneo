package ar.com.noxit.servicios;

import java.util.List;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Invitacion;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IInvitacionService extends IGenericService<Invitacion> {

    public void cambiarEstadoAPendiente(Invitacion invitacion);

    public void cambiarEstadoAConfirmado(Invitacion invitacion);

    public void cambiarEstadoACancelado(Invitacion invitacion);

    public void cambiarEstadoADuda(Invitacion invitacion);

    public void cambiarEstadoAEnviado(Invitacion invitacion);

    public List<Invitacion> findByUsuario(Usuario usuario);

    public Invitacion getInvitacionByIdPartidoYNumero(String numero, Long idPartido);

    public List<Invitacion> getAllInvitacionesAEnviar();

}
