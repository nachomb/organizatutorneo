package ar.com.noxit.servicios.impl;

import java.util.List;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Reporte;
import ar.com.noxit.picadito.dominio.entidades.ReporteHistorico;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface ITorneoService extends IGenericService<Torneo> {

    void iniciarTorneo(Torneo torneo);

	void continuarTorneo(Torneo entity);

    List<Torneo> getTorneosByUsuario(Usuario usuario);
    
    List<Torneo> getTorneosByUsuarioCreador(Usuario usuario);

	void actualizarPosiciones(Long torneoId);

	Torneo calcularTablaGoleadores(Torneo torneo);

	String getDatosGrafico(Torneo torneo, Jugador j);

	String getFechas(Torneo torneo);

	Reporte generarReporte(Torneo entity);

	ReporteHistorico getReporteHistorico(Equipo equipo);

	Torneo calcularTarjetas(Torneo entity);

	Torneo calcularSanciones(Torneo entity);

	List<Torneo> findActivos();
	
	List<Torneo> findNoActivos();

}
