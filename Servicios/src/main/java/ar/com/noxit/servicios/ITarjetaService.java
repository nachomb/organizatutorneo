package ar.com.noxit.servicios;

import java.util.List;
import java.util.Map;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.Tarjeta;
import ar.com.noxit.picadito.dominio.entidades.Torneo;

public interface ITarjetaService extends IGenericService<Tarjeta> {
	
	void borrarAmarillas(Partido partido);
	
	void borrarRojas(Partido partido);

    void generarAmarillas(Map<Long, Long> jugadoresTarjetas, Partido partido);

    void generarRojas(Map<Long, Long> jugadoresTarjetas, Partido partido);
    
    Long countTarjetasAmarillas(Torneo torneo, Equipo equipo);
    
    Long countTarjetasRojas(Torneo torneo, Equipo equipo);

    Long countTarjetasAmarillas(Jugador jugador);
    
    Long countTarjetasRojas(Jugador jugador);

    Boolean existeTarjeta(Partido partido, Jugador jugador, String tipo);

    Long countTarjetasAmarillas(Jugador j, Partido entity);

	Long countTarjetasRojas(Jugador j, Partido entity);

	List<Tarjeta> findByTorneo(Torneo torneo);

	void borrarTarjetas(Partido partido, Equipo equipo);

}
