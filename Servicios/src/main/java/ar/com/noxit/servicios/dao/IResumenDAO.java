package ar.com.noxit.servicios.dao;

import java.util.Collection;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Resumen;

public interface IResumenDAO extends IGenericDAO<Resumen, Long>{

	Collection<Resumen> getAllActivosOrderByFechaDesc();

}
