package ar.com.noxit.servicios.impl;

import java.util.Collection;
import java.util.List;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.ItemMenu;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.servicios.IItemMenuService;
import ar.com.noxit.servicios.dao.IItemMenuDAO;

public class ItemMenuService extends AbstractService<ItemMenu> implements IItemMenuService {

    public ItemMenuService(IItemMenuDAO itemMenuDAO) {
        this.genericDAO = itemMenuDAO;
    }

    public IItemMenuDAO getItemMenuDAO() {
        return (IItemMenuDAO) this.genericDAO;
    }

    public void setItemMenuDAO(IItemMenuDAO itemMenuDAO) {
        this.genericDAO = itemMenuDAO;
    }

    @Override
    public List<ItemMenu> findAllItemsMenuByPadre(Collection<Perfil> perfiles, ItemMenu itemMenu) {
        return getItemMenuDAO().findAllItemsMenuByPadre(perfiles, itemMenu);
    }

}
