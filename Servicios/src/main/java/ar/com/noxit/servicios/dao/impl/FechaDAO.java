package ar.com.noxit.servicios.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Fecha;
import ar.com.noxit.servicios.dao.IFechaDAO;

public class FechaDAO extends AbstractDAO<Fecha> implements IFechaDAO {

    @Override
    public Fecha getLastFechaNoJugadaByTorneoId(Long torneoId) {
        String sql = "select f from Fecha as f inner join f.torneo as t where t.id = :torneoId order by f.numero asc ";
        List<Fecha> fechas = (List<Fecha>) getSession().createQuery(sql).setParameter("torneoId", torneoId).list();
        Fecha fec = null;
        for (Fecha fecha : fechas) {
            fec = fecha;
            if (!fecha.isJugada()) {
                break;
            }
        }
        return fec;
    }

	@Override
	public List<Fecha> getAllFechasByTorneoId(Long torneoId) {
		String sql = "select f from Fecha as f inner join f.torneo as t where t.id = :torneoId order by f.numero asc ";
        return (List<Fecha>) getSession().createQuery(sql).setParameter("torneoId", torneoId).list();
        
	}

	@Override
	public Integer getCountByTorneoId(Long torneoId) {
		Criteria criteria = this.getSession().createCriteria(Fecha.class);
		criteria.add(Restrictions.eq("torneo.id", torneoId));
		criteria.setProjection(Projections.rowCount());
		return (Integer)criteria.list().get(0);
	}
	

	
	
}
