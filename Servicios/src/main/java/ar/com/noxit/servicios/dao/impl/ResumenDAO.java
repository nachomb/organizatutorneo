package ar.com.noxit.servicios.dao.impl;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Resumen;
import ar.com.noxit.servicios.dao.IResumenDAO;

public class ResumenDAO extends AbstractDAO<Resumen> implements IResumenDAO {

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Resumen> getAllActivosOrderByFechaDesc() {
		Criteria criteria = getSession().createCriteria(Resumen.class);
		criteria.add(Restrictions.eq("activo", true))
		.addOrder(Order.desc("fecha"));
		return criteria.list();
	}

}
