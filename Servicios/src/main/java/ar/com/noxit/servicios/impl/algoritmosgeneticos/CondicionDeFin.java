package ar.com.noxit.servicios.impl.algoritmosgeneticos;

import java.util.List;

import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.PopulationData;
import org.uncommons.watchmaker.framework.TerminationCondition;

public class CondicionDeFin implements TerminationCondition {

	FitnessEvaluator<List<PartidoDTO>> fitnessEvaluator;
	
	public CondicionDeFin(FitnessEvaluator<List<PartidoDTO>> fitnessEvaluator) {
		this.fitnessEvaluator = fitnessEvaluator;
	}
	
	@Override
	public boolean shouldTerminate(PopulationData<?> populationData) {
		List<PartidoDTO> mejorCandidato = (List<PartidoDTO>) populationData.getBestCandidate();
		return this.fitnessEvaluator.getFitness(mejorCandidato, null) == 100;
	}

}
