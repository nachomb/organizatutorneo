package ar.com.noxit.servicios;

import java.util.List;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Sede;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface ISedeService extends IGenericService<Sede> {
	
	List<Sede> getSedesByUsuarioCreador(Usuario usuario);
	
}
