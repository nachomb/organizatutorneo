package ar.com.noxit.servicios.dao.impl;

import java.util.List;

import org.hibernate.Query;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.Tarjeta;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.servicios.dao.ITarjetaDAO;

public class TarjetaDAO extends AbstractDAO<Tarjeta> implements ITarjetaDAO {

	@Override
	public Long getCountTarjetasByJugadorYTipo(Jugador jugador, String tipo) {
		String queryString = " select count(tar) from Tarjeta tar where tar.jugador = :jugador "
				+ " and tar.tipo = :tipo ";
		Query query = getSession().createQuery(queryString);
		query.setParameter("jugador", jugador);
		query.setParameter("tipo", tipo);
		return (Long) query.uniqueResult();
	}

    @Override
    public void persistList(List<Tarjeta> tarjetas) {
        getHibernateTemplate().saveOrUpdateAll(tarjetas);
    }

    @Override
    public List<Tarjeta> getTarjetasByJugadorYPartido(Jugador jugador, Partido partido) {
        String queryString = " from Tarjeta tar where tar.partido = :partido and tar.jugador = :jugador ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("partido", partido);
        query.setParameter("jugador", jugador);
        return (List<Tarjeta>) query.list();
    }

    @Override
    public List<Tarjeta> getTarjetasByJugadorYPartidoYTipo(Jugador jugador, Partido partido, String tipoTarjeta) {
        String queryString = " from Tarjeta tar where tar.partido = :partido and tar.jugador = :jugador and tar.tipo = :tipo ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("partido", partido);
        query.setParameter("jugador", jugador);
        query.setParameter("tipo", tipoTarjeta);
        return (List<Tarjeta>) query.list();
    }

    @Override
    public Long getCountTarjetasByJugadorYPartidoYTipo(Jugador jugador, Partido partido, String tipoTarjeta) {
        String queryString = " select count(tar) from Tarjeta tar where tar.partido = :partido and tar.jugador = :jugador "
                + " and tar.tipo = :tipo ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("partido", partido);
        query.setParameter("jugador", jugador);
        query.setParameter("tipo", tipoTarjeta);
        return (Long) query.uniqueResult();
    }

    @Override
    public Boolean existeTarjeta(Partido partido, Jugador jugador, String tipo) {
        String queryString = "  from Tarjeta tar where tar.partido = :partido and tar.jugador = :jugador "
                + " and tar.tipo = :tipo ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("partido", partido);
        query.setParameter("jugador", jugador);
        query.setParameter("tipo", tipo);
        return !query.list().isEmpty();
    }

    @Override
    public void eliminarTarjeta(Partido partido, String tipo) {
        String queryString = " delete from Tarjeta tar where tar.partido = :partido and tar.tipo = :tipo ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("partido", partido);
        query.setParameter("tipo", tipo);
        query.executeUpdate();
    }

	@Override
	public Long countTarjetas(Torneo torneo, Equipo equipo, String tipo) {
		String queryString = " select count(tar) from Tarjeta tar " 
			+	" inner join tar.partido p "
		    +   " inner join p.fecha f "
		    +   " inner join f.torneo t "
			+	" where tar.tipo = :tipo "
			+ " and t = :torneo " 
			+ " and tar.jugador.equipo = :equipo";
		Query query = getSession().createQuery(queryString);
		query.setParameter("tipo", tipo);
		query.setParameter("torneo", torneo);
		query.setParameter("equipo", equipo);
		return (Long) query.uniqueResult();
	}

	@Override
	public List<Tarjeta> findByTorneo(Torneo torneo) {
		
        String queryString = " from Tarjeta tar where tar.partido.fecha.torneo = :torneo or tar.partido.fecha.torneo.id = 43 or tar.partido.fecha.torneo.id = 44 ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("torneo", torneo);
       // query.setParameter("jugador", jugador);
       // query.setParameter("tipo", tipoTarjeta);
        return (List<Tarjeta>) query.list();
	}

	@Override
	public void borrarTarjetas(Partido partido, Equipo equipo) {
        String queryString = " delete from Tarjeta tar where tar.partido = :partido and tar.equipo = :equipo ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("partido", partido);
        query.setParameter("equipo", equipo);
        int cantidad = query.executeUpdate();
		logger.info("Se borraron " + cantidad + " de registros de la tabla tarjetas del partido " + partido);
	}

}
