package ar.com.noxit.servicios;

import java.util.Collection;
import java.util.List;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.PartidoAmistoso;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IPartidoAmistosoService extends IGenericService<PartidoAmistoso> {

    List<PartidoAmistoso> getPartidosByUsuario(Usuario usuario, Jugador jugador);

    List<PartidoAmistoso> getPartidosJugadosByUsuario(Usuario usuario, Jugador jugador);

    List<PartidoAmistoso> getAllPartidosJugados();

    void cargarAlineaciones(PartidoAmistoso entity, List<Long> idsJugadoresEquipoA, List<Long> idsJugadoresEquipoB);

    Integer getCountAllPartidosJugados();

    Integer getCountPartidosByUsuario(Usuario usuario, Jugador jugador);

    Integer getCountPartidosJugadosByUsuario(Usuario usuario, Jugador jugador);

    Collection<PartidoAmistoso> getAllPartidosNoJugados();

    Integer getCountAllPartidosNoJugados();

}
