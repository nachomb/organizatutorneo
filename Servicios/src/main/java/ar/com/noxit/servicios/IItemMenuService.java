package ar.com.noxit.servicios;

import java.util.Collection;
import java.util.List;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.ItemMenu;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;

public interface IItemMenuService extends IGenericService<ItemMenu> {

    List<ItemMenu> findAllItemsMenuByPadre(Collection<Perfil> perfiles, ItemMenu itemMenu);

}
