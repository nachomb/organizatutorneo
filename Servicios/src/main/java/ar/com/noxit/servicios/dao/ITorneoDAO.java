package ar.com.noxit.servicios.dao;

import java.util.Collection;
import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface ITorneoDAO extends IGenericDAO<Torneo, Long> {

    List<Torneo> getTorneosByUsuario(Usuario usuario);

	void actualizarPosiciones(Long torneoId);

	void actualizarGoleadoresYTarjetas(Long torneoId);
	
	Collection<Torneo> getAllByEquipo(Equipo equipo);

	List<Torneo> findActivos();

	List<Torneo> findNoActivos();

	List<Torneo> getTorneosByUsuarioCreador(Usuario usuario);


}
