package ar.com.noxit.servicios.dao.impl;

import java.util.List;

import org.hibernate.Query;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Gol;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.servicios.dao.IGolDAO;

public class GolDAO extends AbstractDAO<Gol> implements IGolDAO {

	@Override
	public void persistList(List<Gol> goles) {
		getHibernateTemplate().saveOrUpdateAll(goles);
	}

	@Override
	public Long countGoles(Jugador jugador) {
		String queryString = " select count(*) from Gol g where g.goleador = :jugador ";
		Query query = getSession().createQuery(queryString);
		query.setParameter("jugador", jugador);
		return (Long) query.uniqueResult();
	}

    @Override
    public void eliminarGoles(Partido partido) {
        String queryString = " delete from Gol g where g.partido = :partido ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("partido", partido);
        query.executeUpdate();
    }

	@Override
	public Long countGoles(Jugador jugador, Partido partido) {
		String queryString = " select count(*) from Gol g where g.goleador = :jugador and partido = :partido ";
		Query query = getSession().createQuery(queryString);
		query.setParameter("jugador", jugador);
		query.setParameter("partido", partido);
		return (Long) query.uniqueResult();
	}

	@Override
	public void eliminarGoles(Partido partido, Equipo equipo) {
        String queryString = " delete from Gol g where g.partido = :partido and g.equipo = :equipo ";
        Query query = getSession().createQuery(queryString);
        query.setParameter("partido", partido);
        query.setParameter("equipo", equipo);
        Integer cantidad = query.executeUpdate();
		logger.info("Se borraron " + cantidad + " de registros de la tabla gol del partido " + partido);
	}
}
