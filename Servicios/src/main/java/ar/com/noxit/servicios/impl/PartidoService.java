package ar.com.noxit.servicios.impl;

import java.util.ArrayList;
import java.util.List;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.servicios.IPartidoService;
import ar.com.noxit.servicios.dao.IJugadorDAO;
import ar.com.noxit.servicios.dao.IPartidoDAO;

public class PartidoService extends AbstractService<Partido> implements IPartidoService {

    private IJugadorDAO jugadorDAO;

    public PartidoService(IPartidoDAO partidoDAO, IJugadorDAO jugadorDAO) {
        this.genericDAO = partidoDAO;
        this.jugadorDAO = jugadorDAO;
    }

    public void setPartidoDAO(IPartidoDAO partidoDAO) {
        this.genericDAO = partidoDAO;
    }

    public IPartidoDAO getPartidoDAO() {
        return (IPartidoDAO) genericDAO;
    }

    public void setPartidoDAO(IJugadorDAO jugadorDAO) {
        this.jugadorDAO = jugadorDAO;
    }

    public IJugadorDAO getJugadorDAO() {
        return jugadorDAO;
    }

    @Override
    public void cargarAlineaciones(Partido partido, List<Long> idsTitularesLocales, List<Long> idsTitularesVisitantes, List<Long> idsSuplentesLocales, List<Long> idsSuplentesVisitantes) {
        getPartidoDAO().merge(partido);
        List<Jugador> titLocales = new ArrayList<Jugador>();
        List<Jugador> supLocales = new ArrayList<Jugador>();
        List<Jugador> titVisitantes = new ArrayList<Jugador>();
        List<Jugador> supVisitantes = new ArrayList<Jugador>();
        for (Jugador jug : partido.getEquipoLocal().getJugadores()) {
            if (idsTitularesLocales.contains(jug.getId())) {
                titLocales.add(jug);
            } else if(idsSuplentesLocales.contains(jug.getId())){
                supLocales.add(jug);
            }
        }
        for (Jugador jug : partido.getEquipoVisitante().getJugadores()) {
            if (idsTitularesVisitantes.contains(jug.getId())) {
                titVisitantes.add(jug);
            } else if(idsSuplentesVisitantes.contains(jug.getId())){
                supVisitantes.add(jug);
            }
        }
        partido.asignarAlineaciones(titLocales, supLocales, titVisitantes, supVisitantes);
        getPartidoDAO().update(partido);
    }

	@Override
	public List<String> getDatosGraficoHistorial(Partido partido) {
		List<Partido> partidos = ((IPartidoDAO)this.genericDAO).getAllFinalizados(partido.getEquipoLocal(),partido.getEquipoVisitante());
		List<String> datosGrafico = new ArrayList<String>();
		int cantGanados = 0;
		int cantEmpatados = 0;
		int cantPerdidos = 0;
		for(Partido p:partidos){
			Equipo ganador = p.getGanador();
			if(ganador == null){
				++cantEmpatados;
			}else if(partido.getEquipoLocal().equals(ganador)){
				++cantGanados;
			}else{
				++cantPerdidos;
			}
		}
		datosGrafico.add(String.valueOf(cantGanados));
		datosGrafico.add(String.valueOf(cantEmpatados));
		datosGrafico.add(String.valueOf(cantPerdidos));
		return datosGrafico;
	}

	@Override
	public Long countPartidosJugados(Jugador j) {
		return getPartidoDAO().countPartidosJugados(j);
	}

}
