package ar.com.noxit.servicios.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Sede;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

@Transactional
public interface ISedeDAO extends IGenericDAO<Sede, Long> {

	List<Sede> getSedesByUsuarioCreador(Usuario usario);
	
}
