package ar.com.noxit.servicios.dao;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Dummy;

@Transactional
public interface IDummyDAO extends IGenericDAO<Dummy, Long> {

}
