package ar.com.noxit.servicios.impl;

import java.util.List;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Publicidad;
import ar.com.noxit.servicios.IPublicidadService;
import ar.com.noxit.servicios.dao.IPublicidadDAO;

public class PublicidadService extends AbstractService<Publicidad> implements IPublicidadService {

    public PublicidadService(IPublicidadDAO dao) {
        this.genericDAO = dao;
    }

	@Override
	public List<Publicidad> getPublicidadesVerticalesActivas() {
		return ((IPublicidadDAO) this.genericDAO).getPublicidadesVerticalesActivas();
	}

	@Override
	public List<Publicidad> getPublicidadesSliderActivas() {
		return ((IPublicidadDAO) this.genericDAO).getPublicidadesSliderActivas();
	}
    
    

}
