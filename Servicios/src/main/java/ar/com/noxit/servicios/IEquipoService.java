package ar.com.noxit.servicios;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IEquipoService extends IGenericService<Equipo> {

    Equipo getEquipoPorNombre(String nombre);

    @Transactional(readOnly = true)
    boolean esEquipoDuplicado(Equipo entity);

    List<Equipo> getEquiposByUsuario(Usuario usuario);
    
    List<Equipo> getEquiposByUsuarioCreador(Usuario usuario);
    
    List<Equipo> getAllActivos();
    
    List<Equipo> getAllActivosOrderByNombre();

    Collection<Equipo> getAllOrderByActivoYNombre();

}
