package ar.com.noxit.servicios.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Grupo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.IGrupoService;
import ar.com.noxit.servicios.dao.IGrupoDAO;

public class GrupoService extends AbstractService<Grupo> implements IGrupoService {

    public GrupoService(IGrupoDAO grupoDAO) {
        this.genericDAO = grupoDAO;
    }

    public IGrupoDAO getGrupoDAO() {
        return (IGrupoDAO) this.genericDAO;
    }

    public void setGrupoDAO(IGrupoDAO grupoDAO) {
        this.genericDAO = grupoDAO;
    }

    @Override
    @Transactional(readOnly=true)
    public List<Grupo> getGruposByUsuario(Usuario usuario) {
        return getGrupoDAO().getGruposByUsuario(usuario);
    }

    @Override
    @Transactional(readOnly=true)
    public Grupo getGrupoPorNombreYCreador(String nombre, Jugador jugador) {
        return getGrupoDAO().getGrupoPorNombreYCreador(nombre, jugador);
    }

    @Override
    @Transactional(readOnly=true)
    public boolean existeGrupoParaJugador(Jugador jugador, Grupo grupo) {
        Grupo grp = getGrupoDAO().getGrupoPorNombreYCreador(grupo.getNombre(),jugador);
        if (grupo.getId() == null)
            return grp != null;
        else if (grp == null)
            return false;
        else
            return grp.getId() != grupo.getId();
    }
}
