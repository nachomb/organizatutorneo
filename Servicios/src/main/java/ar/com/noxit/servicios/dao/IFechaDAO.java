package ar.com.noxit.servicios.dao;

import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Fecha;

public interface IFechaDAO extends IGenericDAO<Fecha, Long> {

    public Fecha getLastFechaNoJugadaByTorneoId(Long torneoId);
    
    public List<Fecha> getAllFechasByTorneoId(Long torneoId);

	public Integer getCountByTorneoId(Long torneoId);

}
