package ar.com.noxit.servicios;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Grupo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IGrupoService extends IGenericService<Grupo> {

    @Transactional(readOnly=true)
    List<Grupo> getGruposByUsuario(Usuario usuario);

    @Transactional(readOnly=true)
	Grupo getGrupoPorNombreYCreador(String nombre, Jugador jugador);

	@Transactional(readOnly=true)
    boolean existeGrupoParaJugador(Jugador jugador, Grupo grupo);

}
