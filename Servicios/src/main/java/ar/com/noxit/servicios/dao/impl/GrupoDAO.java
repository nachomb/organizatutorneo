package ar.com.noxit.servicios.dao.impl;

import java.util.List;

import org.hibernate.Query;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Grupo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.dao.IGrupoDAO;

public class GrupoDAO extends AbstractDAO<Grupo> implements IGrupoDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Grupo> getGruposByUsuario(Usuario usuario) {
        String queryString = "select g from " + this.getEntity().getSimpleName() + " g "
        + " inner join g.creador c "
        + " inner join c.usuario u "
        + " where u.id = :usuarioId ";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("usuarioId", usuario.getId());
        return (List<Grupo>) this.doList(query);
	}

	@Override
	public Grupo getGrupoPorNombreYCreador(String nombre, Jugador jugador) {
        String queryString = "select g from " + this.getEntity().getSimpleName() + " g "
        + " where g.creador.id = :jugadorId and g.nombre = :nombreGrupo";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("jugadorId", jugador.getId());
        query.setParameter("nombreGrupo", nombre);
		return (Grupo) query.uniqueResult();
	}


}
