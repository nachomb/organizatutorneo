package ar.com.noxit.servicios.dao;

import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;

public interface IPartidoDAO extends IGenericDAO<Partido, Long> {

	List<Partido> getAllFinalizados(Equipo equipo1,Equipo equipo2);

	Long countPartidosJugados(Jugador j);
	
}
