package ar.com.noxit.servicios.dao;

import java.util.Collection;
import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IEquipoDAO extends IGenericDAO<Equipo, Long> {

    Equipo getEquipoPorNombre(String nombre);

    List<Equipo> getEquiposByUsuario(Usuario usuario);

	List<Equipo> getAllActivos();

	Collection<Equipo> getAllOrderByActivoYNombre();

	List<Equipo> getAllActivosOrderByNombre();

	List<Equipo> getEquiposByUsuarioCreador(Usuario usuario);

}
