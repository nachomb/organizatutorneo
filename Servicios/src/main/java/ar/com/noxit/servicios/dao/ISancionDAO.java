package ar.com.noxit.servicios.dao;

import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.Sancion;
import ar.com.noxit.picadito.dominio.entidades.TipoSancion;
import ar.com.noxit.picadito.dominio.entidades.Torneo;

public interface ISancionDAO extends IGenericDAO<Sancion, Long> {

    List<TipoSancion> existeSancion(int cantidadTarjetas, String tipoTarjeta);

    List<TipoSancion> existeSancion(int cantidadAmarillas, int cantidadRojas);

    boolean existeSancion(Partido partido, Jugador jugador, TipoSancion tipoSancion);

    List<Sancion> getSancionesPorPartido(Partido partido);

    List<Sancion> getSancionesPorPartido(Equipo equipoLocal, Equipo equipoVisitante);

	List<Sancion> findByTorneoNoCumplidas(Torneo torneo);

	List<Sancion> findByTorneo(Torneo torneo);

}
