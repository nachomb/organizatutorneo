package ar.com.noxit.servicios.dao;

import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Invitacion;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IInvitacionDAO extends IGenericDAO<Invitacion, Long> {

	public List<Invitacion> findByUsuario(Usuario usuario);

    public Invitacion getInvitacionByIdPartidoYNumero(String numero, Long idPartido);

    public List<Invitacion> getAllInvitacionesAEnviar();

}
