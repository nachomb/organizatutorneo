package ar.com.noxit.servicios;

import java.util.List;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Fecha;

public interface IFechaService extends IGenericService<Fecha> {

    public Fecha getLastFechaNoJugadaByTorneoId(Long torneoId);
    
    public List<Fecha> getAllFechasByTorneoId(Long torneoId);
    
	public Integer getCountByTorneoId(Long torneoId);


}
