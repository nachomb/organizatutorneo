package ar.com.noxit.servicios.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Sede;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.dao.ISedeDAO;

public class SedeDAO extends AbstractDAO<Sede> implements ISedeDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Sede> getSedesByUsuarioCreador(Usuario usuario) {
        Criteria criteria = getSession().createCriteria(Sede.class);
        criteria.add(Restrictions.eq("creador", usuario));
		return criteria.list();
	}

}
