package ar.com.noxit.servicios.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Publicidad;
import ar.com.noxit.servicios.dao.IPublicidadDAO;

public class PublicidadDAO extends AbstractDAO<Publicidad> implements IPublicidadDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Publicidad> getPublicidadesVerticalesActivas() {
		Criteria criteria = getSession().createCriteria(Publicidad.class);
		criteria.add(Restrictions.isNotNull("imagenVertical"))
		.add(Restrictions.eq("activoVertical", true));
		return criteria.list();
	}

	@Override
	public List<Publicidad> getPublicidadesSliderActivas() {
		Criteria criteria = getSession().createCriteria(Publicidad.class);
		criteria.add(Restrictions.isNotNull("imagenSlider"))
		.add(Restrictions.eq("activoSlider", true));
		return criteria.list();
	}

}
