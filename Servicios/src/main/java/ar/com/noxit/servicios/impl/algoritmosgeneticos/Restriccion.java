package ar.com.noxit.servicios.impl.algoritmosgeneticos;

import java.util.Date;
import java.util.List;


public abstract class Restriccion {

	protected String operador;
	protected PartidoDTO partido;
	protected String valor;
	
	public Restriccion(String operador, PartidoDTO partido, String valor) {
		this.operador = operador;
		this.partido = partido;
		this.valor = valor;
	}

	public boolean cumple(PartidoDTO part){
		if(!this.partido.equals(part)){
			return true;
		}
		return esValida(part);
	}
	
	public void aplicar(List<PartidoDTO> partidos, Date fechaInicial){
		for(PartidoDTO part: partidos){
			if(part.equals(partido)){
				aplicar(part, fechaInicial);
				break;
			}
			
		}
	}
	
	protected abstract boolean esValida(PartidoDTO partidoDTO);
	
	protected abstract void aplicar(PartidoDTO partidoDTO, Date fechaInicial);
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Operador: ").append(operador).append(" ");
		builder.append("Partido id: ").append(partido.getId()).append(" ");
		builder.append("Valor: ").append(valor);
	    return builder.toString();
	}
}
