package ar.com.noxit.servicios;

import java.util.Collection;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IJugadorService extends IGenericService<Jugador> {

    Collection<Jugador> getAllJugadoresPosibles();

    @Transactional(readOnly=true)
    Jugador getjugadorByUsuario(Usuario usuario);
    
    @Transactional(readOnly=true)
    Jugador getjugadorByNombreUsuario(String usuario);
    
    Collection<Jugador> getJugadorLike(Jugador jugador);

    Jugador getJugadorByDni(String dni);
	
    Collection<Jugador> getJugadoresByUsuarioCreador(Usuario usuario);
    
    Collection<Jugador> getJugadoresByUsuarioCreadorSinEquipo(Usuario usuario);

}
