package ar.com.noxit.servicios.dao.impl;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.dao.IJugadorDAO;

public class JugadorDAO extends AbstractDAO<Jugador> implements IJugadorDAO {

    @SuppressWarnings("unchecked")
    @Override
    public Collection<Jugador> getAllJugadoresPosibles() {
        DetachedCriteria criteria = DetachedCriteria.forClass(Jugador.class);
        criteria.add(Restrictions.eq("activo", true)).add(Restrictions.isNull("equipo"))
                .addOrder(Order.asc("apellido")).addOrder(Order.asc("nombre"));
        return getHibernateTemplate().findByCriteria(criteria);
    }

    @Override
    public Jugador getjugadorByUsuario(Usuario usuario) {
        String queryString = " select j from Jugador j " 
            + " inner join j.usuario u " 
            + " where u.id = :usuarioId ";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("usuarioId", usuario.getId());
        return (Jugador) query.uniqueResult();
    }

	@Override
	public Jugador getjugadorByNombreUsuario(String nombreUsuario) {
        String queryString = " select j from Jugador j " 
            + " inner join j.usuario u " 
            + " where u.username = :nombreUsuario ";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("nombreUsuario", nombreUsuario);
        return (Jugador) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Jugador> getJugadorLike(Jugador jugador) {
		Criteria criteria = this.getSession().createCriteria(Jugador.class);
		criteria.add(Restrictions.like("nombre", "%" + jugador.getNombre() +"%"));
		criteria.add(Restrictions.like("apellido", "%"+jugador.getApellido()+"%"));
		criteria.add(Restrictions.eq("creador", jugador.getCreador() ));
		return criteria.list();
	}

	@Override
	public Jugador getJugadorByDni(String dni) {
        String queryString = " select j from Jugador j where j.dni = :dni ";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("dni", dni);
        return (Jugador) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Jugador> getJugadoresByUsuarioCreador(Usuario usuario) {
		Criteria criteria = this.getSession().createCriteria(Jugador.class);
		criteria.add(Restrictions.eq("creador", usuario ));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Jugador> getJugadoresByUsuarioCreadorSinEquipo(
			Usuario usuario) {
		Criteria criteria = this.getSession().createCriteria(Jugador.class);
		criteria.add(Restrictions.eq("creador", usuario )).add(Restrictions.eq("activo", true)).add(Restrictions.isNull("equipo"))
        .addOrder(Order.asc("apellido")).addOrder(Order.asc("nombre"));
		return criteria.list();
	}
}
