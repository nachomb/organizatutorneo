package ar.com.noxit.servicios.dao;

import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.PuntajePartido;

public interface IPuntajePartidoDAO extends IGenericDAO<PuntajePartido, Long> {

	void persistList(List<PuntajePartido> puntajes);

	PuntajePartido findByJugadorPartido(Jugador j, Partido partido);

	Integer sumaPuntajes(Jugador j);

	Integer countFigura(Jugador j);

	void borrarPuntajes(Partido partido);

	void borrarPuntajes(Partido partido, Equipo equipo);

}
