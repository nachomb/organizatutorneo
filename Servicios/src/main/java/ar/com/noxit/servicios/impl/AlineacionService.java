package ar.com.noxit.servicios.impl;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Alineacion;
import ar.com.noxit.servicios.IAlineacionService;
import ar.com.noxit.servicios.dao.IAlineacionDAO;

public class AlineacionService extends AbstractService<Alineacion> implements IAlineacionService {

	public AlineacionService(IAlineacionDAO alineacionDAO){
		this.genericDAO = alineacionDAO;
	}
}
