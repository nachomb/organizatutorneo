package ar.com.noxit.servicios.dao.impl;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.dao.IEquipoDAO;

public class EquipoDAO extends AbstractDAO<Equipo> implements IEquipoDAO {

    @Override
    public Equipo getEquipoPorNombre(String nombre) {
        Criteria criteria = getSession().createCriteria(Equipo.class);
        criteria.add(Restrictions.eq("nombre", nombre));
        return (Equipo) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<Equipo> getEquiposByUsuario(Usuario usuario) {
        String queryString = " select e from Equipo e "
            + " inner join e.jugadores j "
            + " inner join j.usuario u "
            + " where u.id = :usuarioId "
            + " order by e.nombre ";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("usuarioId", usuario.getId());
        return (List<Equipo>) doList(query); 
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<Equipo> getAllActivos() {
        Criteria criteria = getSession().createCriteria(Equipo.class);
        criteria.add(Restrictions.eq("activo", true));
        return (List<Equipo>)criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Equipo> getAllOrderByActivoYNombre() {
        Criteria criteria = getSession().createCriteria(Equipo.class);
        criteria.addOrder(Order.desc("activo"));
        criteria.addOrder(Order.asc("nombre"));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Equipo> getAllActivosOrderByNombre() {
        Criteria criteria = getSession().createCriteria(Equipo.class);
        criteria.add(Restrictions.eq("activo", true));
        criteria.addOrder(Order.asc("nombre"));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Equipo> getEquiposByUsuarioCreador(Usuario usuario) {
        Criteria criteria = getSession().createCriteria(Equipo.class);
        criteria.add(Restrictions.eq("creador", usuario));
		return criteria.list();
	}

}
