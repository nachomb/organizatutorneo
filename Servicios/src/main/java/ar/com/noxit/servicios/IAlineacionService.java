package ar.com.noxit.servicios;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Alineacion;

public interface IAlineacionService extends IGenericService<Alineacion>{

}
