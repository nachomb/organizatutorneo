package ar.com.noxit.servicios;

import java.io.File;
import java.io.IOException;

import ar.com.noxit.picadito.dominio.entidades.ResultadoImportacion;

public interface IImporterService {

	public ResultadoImportacion importarJugadoresYEquipos(File file) throws IOException;
	
}
