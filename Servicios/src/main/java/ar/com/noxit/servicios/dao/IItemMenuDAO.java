package ar.com.noxit.servicios.dao;

import java.util.Collection;
import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.ItemMenu;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;

public interface IItemMenuDAO extends IGenericDAO<ItemMenu, Long> {

    List<ItemMenu> findAllItemsMenuByPadre(Collection<Perfil> perfiles, ItemMenu itemMenu);

}
