package ar.com.noxit.servicios.dao;

import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Grupo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IGrupoDAO extends IGenericDAO<Grupo, Long> {

	List<Grupo> getGruposByUsuario(Usuario usuario);

	Grupo getGrupoPorNombreYCreador(String nombre, Jugador jugador);

}
