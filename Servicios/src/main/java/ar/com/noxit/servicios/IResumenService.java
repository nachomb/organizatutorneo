package ar.com.noxit.servicios;

import java.util.Collection;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Resumen;

public interface IResumenService extends IGenericService<Resumen>{

	Collection<Resumen> getAllActivosOrderByFechaDesc();

}
