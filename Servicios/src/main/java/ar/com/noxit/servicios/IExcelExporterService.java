package ar.com.noxit.servicios;

import java.util.List;

import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Partido;

public interface IExcelExporterService {

	public String exportarEstadisticas(List<Long> idTorneos, List<Long> idFechas) throws Exception;

	public String exportarPlanilla(List<Partido> partidos)
			throws Exception;

	public String exportarEstadisticasHistoricas(List<Equipo> equipos) throws Exception;

}
