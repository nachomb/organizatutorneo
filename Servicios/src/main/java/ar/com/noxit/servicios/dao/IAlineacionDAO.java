package ar.com.noxit.servicios.dao;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Alineacion;

public interface IAlineacionDAO extends IGenericDAO<Alineacion, Long>{

}
