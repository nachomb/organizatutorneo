package ar.com.noxit.servicios.dao;

import java.util.List;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.Publicidad;

public interface IPublicidadDAO extends IGenericDAO<Publicidad, Long> {

	public List<Publicidad> getPublicidadesVerticalesActivas();
	
	public List<Publicidad> getPublicidadesSliderActivas();
	
}
