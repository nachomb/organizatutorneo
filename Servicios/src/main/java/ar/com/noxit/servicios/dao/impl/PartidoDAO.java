package ar.com.noxit.servicios.dao.impl;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.Query;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.servicios.dao.IPartidoDAO;

public class PartidoDAO extends AbstractDAO<Partido> implements IPartidoDAO {

	@Override
	public List<Partido> getAllFinalizados(Equipo equipo1, Equipo equipo2) {
        String queryString = "select p from " + this.getEntity().getSimpleName() + " p "
        + " where ((p.equipoLocal = :equipo1 and p.equipoVisitante = :equipo2) " 
        + "or (p.equipoLocal = :equipo2 and p.equipoVisitante = :equipo1)) and p.jugado = true";
        Query query = this.getSession().createQuery(queryString);
        query.setParameter("equipo1", equipo1);
        query.setParameter("equipo2", equipo2);
        return (List<Partido>) this.doList(query);
	}

	@Override
	public Long countPartidosJugados(Jugador j) {
		String queryString = "select  (select count(*) from alineacion_suplentes alsup" +
				" where alsup.id_jugador = " + j.getId() + ") + " +
				" (select count(*) from alineacion_titulares altit " +
				"where altit.id_jugador = " + j.getId() + ")";
		BigInteger count = (BigInteger)getSession().createSQLQuery(queryString).uniqueResult();
		return count.longValue();
	}

}
