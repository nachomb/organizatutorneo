package ar.com.noxit.servicios.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.Tarjeta;
import ar.com.noxit.picadito.dominio.entidades.TarjetaAmarilla;
import ar.com.noxit.picadito.dominio.entidades.TarjetaRoja;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.servicios.ITarjetaService;
import ar.com.noxit.servicios.dao.IJugadorDAO;
import ar.com.noxit.servicios.dao.ITarjetaDAO;

public class TarjetaService extends AbstractService<Tarjeta> implements ITarjetaService {

    private IJugadorDAO jugadorDAO;

    public TarjetaService(ITarjetaDAO tarjetaDAO, IJugadorDAO jugadorDAO) {
        this.genericDAO = tarjetaDAO;
        this.jugadorDAO = jugadorDAO;
    }

    public ITarjetaDAO getTarjetaDAO() {
        return (ITarjetaDAO) this.genericDAO;
    }

    public void setTarjetaDAO(ITarjetaDAO tarjetaDAO) {
        this.genericDAO = tarjetaDAO;
    }

    @Override
    public void generarAmarillas(Map<Long, Long> jugadoresTarjetas, Partido partido) {
        
        List<Tarjeta> amarillas = new ArrayList<Tarjeta>();

        Iterator<Long> itJugadores = jugadoresTarjetas.keySet().iterator();
        
        while(itJugadores.hasNext()){
        	Long idJugador = itJugadores.next();
			Jugador jug = this.jugadorDAO.findById(idJugador);
			Long cantTarjetas = jugadoresTarjetas.get(idJugador);
        	for(int i = 1; i <= cantTarjetas ; ++i){
        		amarillas.add(new TarjetaAmarilla(jug, partido, jug.getEquipo()));
        	}
        }
        
        getTarjetaDAO().persistList(amarillas);
    }

    @Override
    public void generarRojas(Map<Long, Long> jugadoresTarjetas, Partido partido) {
        
        List<Tarjeta> rojas = new ArrayList<Tarjeta>();

        Iterator<Long> itJugadores = jugadoresTarjetas.keySet().iterator();
        
        while(itJugadores.hasNext()){
        	Long idJugador = itJugadores.next();
			Jugador jug = this.jugadorDAO.findById(idJugador);
			Long cantTarjetas = jugadoresTarjetas.get(idJugador);
        	for(int i = 1; i <= cantTarjetas ; ++i){
        		rojas.add(new TarjetaRoja(jug, partido, jug.getEquipo()));
        	}
        }
        
        getTarjetaDAO().persistList(rojas);
    }

    public void setJugadorDAO(IJugadorDAO jugadorDAO) {
        this.jugadorDAO = jugadorDAO;
    }

    public IJugadorDAO getJugadorDAO() {
        return jugadorDAO;
    }

    @Override
    public Long countTarjetasAmarillas(Torneo torneo, Equipo equipo) {
        return getTarjetaDAO().countTarjetas(torneo,equipo, Tarjeta.TIPO_AMARILLA);
    }

    @Override
    public Long countTarjetasRojas(Torneo torneo, Equipo equipo) {
        return getTarjetaDAO().countTarjetas(torneo,equipo, Tarjeta.TIPO_ROJA);
    }

    @Override
    public Boolean existeTarjeta(Partido partido, Jugador jugador, String tipo) {
        return this.getTarjetaDAO().existeTarjeta(partido, jugador, tipo);
    }

	@Override
	public Long countTarjetasAmarillas(Jugador jugador) {
		return getTarjetaDAO().getCountTarjetasByJugadorYTipo(jugador, Tarjeta.TIPO_AMARILLA);
	}

	@Override
	public Long countTarjetasRojas(Jugador jugador) {
		return getTarjetaDAO().getCountTarjetasByJugadorYTipo(jugador, Tarjeta.TIPO_ROJA);
	}

	@Override
	public Long countTarjetasAmarillas(Jugador jugador, Partido partido) {
		return getTarjetaDAO().getCountTarjetasByJugadorYPartidoYTipo(jugador, partido, Tarjeta.TIPO_AMARILLA);
	}

	@Override
	public Long countTarjetasRojas(Jugador jugador, Partido partido) {
		return getTarjetaDAO().getCountTarjetasByJugadorYPartidoYTipo(jugador, partido, Tarjeta.TIPO_ROJA);
	}
	
	@Override
	public List<Tarjeta> findByTorneo(Torneo torneo) {
		return getTarjetaDAO().findByTorneo(torneo);
	}

	@Override
	public void borrarAmarillas(Partido partido) {
		getTarjetaDAO().eliminarTarjeta(partido, Tarjeta.TIPO_AMARILLA);
		
	}

	@Override
	public void borrarRojas(Partido partido) {
		getTarjetaDAO().eliminarTarjeta(partido, Tarjeta.TIPO_ROJA);
	}

	@Override
	public void borrarTarjetas(Partido partido, Equipo equipo) {
		getTarjetaDAO().borrarTarjetas(partido, equipo);
		
	}

}
