package ar.com.noxit.servicios.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.watchmaker.framework.CandidateFactory;
import org.uncommons.watchmaker.framework.EvolutionEngine;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.GenerationalEvolutionEngine;
import org.uncommons.watchmaker.framework.SelectionStrategy;
import org.uncommons.watchmaker.framework.TerminationCondition;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.termination.ElapsedTime;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import ar.com.noxit.base.utils.Utils;
import ar.com.noxit.servicios.IProgramacionPartidosService;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.CondicionDeFin;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.FuncionDeAptitud;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.ObservadorDeEvolucion;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.OperadorEvolucionario;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.PartidoDTO;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.PartidosFactory;
import ar.com.noxit.servicios.impl.algoritmosgeneticos.Restriccion;

public class ProgramacionPartidosService implements
		IProgramacionPartidosService {

    private static final Logger logger = LoggerFactory.getLogger(ProgramacionPartidosService.class);
	
	@Override
	public List<PartidoDTO> obtenerPosibleProgramacion(List<PartidoDTO> partidos,
			List<Restriccion> restricciones, int cantCanchas, int duracionPartido,
			Date fechaInicial) {

		Collection<List<PartidoDTO>> candidatoInicial = crearCandidatoInicial(partidos, restricciones, fechaInicial, cantCanchas, duracionPartido);
		
		CandidateFactory<List<PartidoDTO>> candidateFactory = 
			new PartidosFactory(partidos, cantCanchas, duracionPartido, fechaInicial);
		EvolutionaryOperator<List<PartidoDTO>> evolutionaryOperator = new OperadorEvolucionario();
		FitnessEvaluator<List<PartidoDTO>> fitnessEvaluator = new FuncionDeAptitud(restricciones);
		SelectionStrategy<Object> selectionStrategy = new RouletteWheelSelection();
		Random rng = new MersenneTwisterRNG();
		
		EvolutionEngine<List<PartidoDTO>> engine = new GenerationalEvolutionEngine<List<PartidoDTO>>(candidateFactory ,
	                                              evolutionaryOperator,
	                                              fitnessEvaluator,
	                                              selectionStrategy,
	                                              rng);
		
		TerminationCondition[] terminationsConditions = new TerminationCondition[2];
		terminationsConditions[0] = new ElapsedTime(20000);
		terminationsConditions[1] = new CondicionDeFin(fitnessEvaluator);
		
		engine.addEvolutionObserver(new ObservadorDeEvolucion(fitnessEvaluator));
		
		List<PartidoDTO> result = engine.evolve(10, 0, candidatoInicial, terminationsConditions);
		
		ordenarPorFecha(result);
		
		return result;
	}

	private Collection<List<PartidoDTO>> crearCandidatoInicial(List<PartidoDTO> partidos,
			List<Restriccion> restricciones, Date fechaInicial, int cantCanchas, int duracionPartido) {
		
		Collection<List<PartidoDTO>> candidatoInicial = Lists.newArrayList();
		
		for(Restriccion restriccion : restricciones){
			restriccion.aplicar(partidos, fechaInicial);
		}
		
		//validar horarios
		
		//completar la fecha de los partidos que no tengan fecha
		completarFecha(partidos, fechaInicial, cantCanchas, duracionPartido);
		//ordernar por fecha
		ordenarPorFecha(partidos);
		//llenar la descripcion de los partidos que no tengan descripcion
		completarNumeroDeCancha(partidos, fechaInicial, cantCanchas, duracionPartido);
		//validar solucion
		//boolean esValida = validarSolucion(partidos, restricciones, fechaInicial, cantCanchas);
		
		logger.info("Candidato Inicial: ");
		for(PartidoDTO partido : partidos){
			logger.info(partido.toString());
		}
		candidatoInicial.add(partidos);
		
		return candidatoInicial;
	}

	private void completarNumeroDeCancha(List<PartidoDTO> partidos, Date fechaInicial, int cantCanchas, int duracionPartido) {
		List<PartidoDTO> partidosConCancha = Lists.newArrayList();
		List<PartidoDTO> partidosSinCancha = Lists.newArrayList();
		for(PartidoDTO partido : partidos){
			if(partido.getNumeroCancha() == null){
				partidosSinCancha.add(partido);
			}else{
				partidosConCancha.add(partido);
			}
		}
		if(partidosSinCancha.isEmpty()){
			return;
		}
		List<Date> turnos = calcularTurnos(fechaInicial, duracionPartido, partidos.size());
		Map<Date, List<Integer>> canchasDisponiblesPorTurno = calcularCanchasDisponiblesPorTurno(turnos, partidosConCancha, cantCanchas);
		
		Iterator<PartidoDTO> partidosSinCanchaIterator = partidosSinCancha.iterator();
		while(partidosSinCanchaIterator.hasNext()){
			PartidoDTO partidoSinCancha = partidosSinCanchaIterator.next();
			List<Integer> canchasDisponibles = canchasDisponiblesPorTurno.get(partidoSinCancha.getFechaPartido());
			int i = 0;
			while(partidosSinCanchaIterator.hasNext() && i < canchasDisponibles.size()){
				 partidoSinCancha.setNumeroCancha(canchasDisponibles.get(i));
				 ++i;
				 if(i != canchasDisponibles.size()){
					 partidoSinCancha = partidosSinCanchaIterator.next();
				 }
			}
			if(!partidosSinCanchaIterator.hasNext() && i < canchasDisponibles.size()){
				partidoSinCancha.setNumeroCancha(canchasDisponibles.get(i));
			}else if(!partidosSinCanchaIterator.hasNext() && i == canchasDisponibles.size()){
				//TODO
				//buscar cancha disponible
			}
			
		}
	}

	private Map<Date, List<Integer>> calcularCanchasDisponiblesPorTurno(List<Date> turnos,
			List<PartidoDTO> partidos,int cantCanchas) {
		
		List<Integer> canchasDisponibles = Lists.newArrayList();
		List<PartidoDTO> partidosPorTurno = Lists.newArrayList();
		Map<Date, List<Integer>> resultado = Maps.newLinkedHashMap();
		for(Date turno: turnos){
			for(PartidoDTO partido : partidos){
				if(partido.getFechaPartido().equals(turno)){
					partidosPorTurno.add(partido);
				}
			}
			for(int i = 1 ; i <= cantCanchas; ++i){
				boolean canchaDisponible = true;
				for(PartidoDTO p : partidosPorTurno){
					if(p.getNumeroCancha().equals(i)){
						canchaDisponible = false;
					}
				}
				if(canchaDisponible){
					canchasDisponibles.add(i);
				}
			}
			
			resultado.put(turno, canchasDisponibles);
			canchasDisponibles = Lists.newArrayList();
			partidosPorTurno = Lists.newArrayList();
		}
		
		return resultado;
		
	}

	private void ordenarPorFecha(List<PartidoDTO> partidos) {
		Collections.sort(partidos, new Comparator<PartidoDTO>(){
			@Override
			public int compare(PartidoDTO p1, PartidoDTO p2) {
				return p1.getFechaPartido().compareTo(p2.getFechaPartido());
			}
			
		});
	}

	protected void completarFecha(List<PartidoDTO> partidos, Date fechaInicial,
			int cantCanchas, int duracionPartido) {
		List<PartidoDTO> partidosSinFecha = Lists.newArrayList();
		List<PartidoDTO> partidosConFecha = Lists.newArrayList();
		for(PartidoDTO partido : partidos){
			if(partido.getFechaPartido() == null){
				partidosSinFecha.add(partido);
			}else{
				partidosConFecha.add(partido);
			}
		}
		if(partidosSinFecha.isEmpty()){
			return;
		}
		List<Date> turnos = calcularTurnos(fechaInicial, duracionPartido, partidos.size());
		Map<Date,Integer> cantPartidosPorTurno = calcularCantidadDePartidosPorTurno(turnos, partidosConFecha);
		Iterator<PartidoDTO> partidosSinFechaIterator = partidosSinFecha.iterator();
		Iterator<Entry<Date, Integer>> cantPartidosPorTurnoIterator = cantPartidosPorTurno.entrySet().iterator();
		while(partidosSinFechaIterator.hasNext()){
			PartidoDTO partidoSinFecha = partidosSinFechaIterator.next();
			Date horarioDisponible = null;
			int cantCanchasDisponibles = 0;
			while(horarioDisponible == null){
				Entry<Date, Integer> entry = cantPartidosPorTurnoIterator.next();
				int cantPartidos = entry.getValue().intValue();
				if(cantPartidos < cantCanchas){
					horarioDisponible = entry.getKey();
					cantCanchasDisponibles = cantCanchas - cantPartidos;
				}
			}
			int i = 0;
			while(partidosSinFechaIterator.hasNext() && i < cantCanchasDisponibles){
				partidoSinFecha.setFechaPartido(horarioDisponible);
				 ++i;
				 if(i != cantCanchasDisponibles){
					 partidoSinFecha = partidosSinFechaIterator.next();
				 }
			}
			if(!partidosSinFechaIterator.hasNext() && i < cantCanchasDisponibles){
				partidoSinFecha.setFechaPartido(horarioDisponible);
			}else if(!partidosSinFechaIterator.hasNext() && i == cantCanchasDisponibles){
				//TODO
				//buscar horario disponible
			}
			
		}
		
	}

	private Map<Date, Integer> calcularCantidadDePartidosPorTurno(
			List<Date> turnos, List<PartidoDTO> partidosConFecha) {
		int cont = 0;
		Map<Date, Integer> resultado = Maps.newLinkedHashMap();
		for(Date turno: turnos){
			for(PartidoDTO partido : partidosConFecha){
				if(partido.getFechaPartido().equals(turno)){
					++cont;
				}
			}
			resultado.put(turno, cont);
			cont = 0;
		}
		
		return resultado;
	}

	private List<Date> calcularTurnos(Date fechaInicial, int duracionPartido, int cantidadPartidos) {
		List<Date> turnos = Lists.newArrayList();
		for(int i = 0 ; i < cantidadPartidos ; ++i){
			Date fecha = Utils.addMinutes(fechaInicial, duracionPartido*i);
			turnos.add(fecha);
		}
		return turnos;
	}

	private boolean validarSolucion(List<PartidoDTO> partidos,
			List<Restriccion> restricciones, Date fechaInicial, int cantCanchas) {
		boolean esValida = true;
		List<Integer> partidosMismoHorario = contarPartidosMismoHorario(partidos, fechaInicial);
		
		
		return esValida;
	}

	protected List<Integer> contarPartidosMismoHorario(List<PartidoDTO> partidos, Date fechaInicial) {
		List<Integer> resultado = Lists.newArrayList();
		int cantPartidos = 1;
		Iterator<PartidoDTO> iterator = partidos.iterator();
		Iterator<PartidoDTO> iteratorProxPartido = partidos.iterator();
		PartidoDTO proxPartido = iteratorProxPartido.next();
		while(iteratorProxPartido.hasNext()){
			PartidoDTO partido = iterator.next();
			proxPartido = iteratorProxPartido.next();
			Date fechaActual = partido.getFechaPartido();
			Date proxFecha = proxPartido.getFechaPartido();
			if(fechaActual.equals(proxFecha)){
				++cantPartidos;
			}else{
				fechaActual = partido.getFechaPartido();
				resultado.add(cantPartidos);
				cantPartidos = 1;

			}
		}
		resultado.add(cantPartidos);
		return resultado;
	}

	@Override
	public Boolean cumpleTodasRestricciones(List<PartidoDTO> partidos,
			List<Restriccion> restricciones) {
		FitnessEvaluator<List<PartidoDTO>> fitnessEvaluator = new FuncionDeAptitud(restricciones);
		return fitnessEvaluator.getFitness(partidos, null) == 100;
	}

}
