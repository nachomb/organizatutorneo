package ar.com.noxit.servicios;

import java.util.List;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.Sancion;
import ar.com.noxit.picadito.dominio.entidades.Torneo;

public interface ISancionService extends IGenericService<Sancion> {

    void generarSanciones(Partido partido);

    List<Sancion> getSancionesPorPartido(Partido partido);

    List<Sancion> getSancionesNoCumplidas(Equipo equipoLocal, Equipo equipoVisitante);

	List<Sancion> findByTorneoNoCumplidas(Torneo torneo);
	
	List<Sancion> findByTorneo(Torneo torneo);

}
