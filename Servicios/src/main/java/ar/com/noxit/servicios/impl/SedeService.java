package ar.com.noxit.servicios.impl;

import java.util.List;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Sede;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.servicios.ISedeService;
import ar.com.noxit.servicios.dao.ISedeDAO;

public class SedeService extends AbstractService<Sede> implements ISedeService {

    public SedeService(ISedeDAO dao) {
        this.genericDAO = dao;
    }

	@Override
	public List<Sede> getSedesByUsuarioCreador(Usuario usuario) {
		return ((ISedeDAO)this.genericDAO).getSedesByUsuarioCreador(usuario);
	}
	
}
