package ar.com.noxit.servicios.impl.algoritmosgeneticos;

public class FabricaRestricciones {

	public Restriccion crear(String nombre, String idPartido, String operador,
			String valor) {
		if ("Horario".equals(nombre)) {
			return new RestriccionHorario(operador, new PartidoDTO(
					Long.parseLong(idPartido)), valor);
		} else if ("Cancha".equals(nombre)) {
			return new RestriccionCancha(operador, new PartidoDTO(
					Long.parseLong(idPartido)), valor);
		}
		return null;
	}

}
