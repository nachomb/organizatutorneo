package ar.com.noxit.servicios;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.Dummy;

@Transactional
public interface IDummyService extends IGenericService<Dummy> {

}
