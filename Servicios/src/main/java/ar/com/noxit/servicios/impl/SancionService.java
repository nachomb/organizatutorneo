package ar.com.noxit.servicios.impl;

import java.util.Collection;
import java.util.List;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Equipo;
import ar.com.noxit.picadito.dominio.entidades.Jugador;
import ar.com.noxit.picadito.dominio.entidades.Partido;
import ar.com.noxit.picadito.dominio.entidades.Sancion;
import ar.com.noxit.picadito.dominio.entidades.Tarjeta;
import ar.com.noxit.picadito.dominio.entidades.TipoSancion;
import ar.com.noxit.picadito.dominio.entidades.Torneo;
import ar.com.noxit.servicios.ISancionService;
import ar.com.noxit.servicios.dao.ISancionDAO;
import ar.com.noxit.servicios.dao.ITarjetaDAO;

public class SancionService extends AbstractService<Sancion> implements ISancionService {

    private ITarjetaDAO tarjetaDAO;

    public SancionService(ISancionDAO sancionDAO, ITarjetaDAO tarjetaDAO) {
        this.genericDAO = sancionDAO;
        this.tarjetaDAO = tarjetaDAO;
    }

    public ISancionDAO getSancionDAO() {
        return (ISancionDAO) this.genericDAO;
    }

    public void setSancionDAO(ISancionDAO sancionDAO) {
        this.genericDAO = sancionDAO;
    }

    @Override
    public void generarSanciones(Partido partido) {
        Collection<Jugador> jugadoresLocales = partido.getEquipoLocal().getJugadores();
        Collection<Jugador> jugadoresVisitantes = partido.getEquipoVisitante().getJugadores();
        generarSanciones(jugadoresLocales, partido);
        generarSanciones(jugadoresVisitantes, partido);
    }

    private void generarSanciones(Collection<Jugador> jugadores, Partido partido) {
        ITarjetaDAO dao = getTarjetaDAO();
        for (Jugador jugador : jugadores) {
            Long amarillas = dao.getCountTarjetasByJugadorYPartidoYTipo(jugador, partido, Tarjeta.TIPO_AMARILLA);
            Long rojas = dao.getCountTarjetasByJugadorYPartidoYTipo(jugador, partido, Tarjeta.TIPO_ROJA);
            List<TipoSancion> sanciones = getSancionDAO().existeSancion(amarillas.intValue(), rojas.intValue());
            generarSanciones(jugador, partido, sanciones);
        }
    }

    private void generarSanciones(Jugador jugador, Partido partido, List<TipoSancion> sanciones) {
        for (TipoSancion tipoSancion : sanciones) {
            if (!getSancionDAO().existeSancion(partido, jugador, tipoSancion)){
                Sancion sancion = new Sancion();
                jugador.agregarSancion(sancion);
                this.getSancionDAO().persist(sancion);
            }
        }
    }

    public void setTarjetaDAO(ITarjetaDAO tarjetaDAO) {
        this.tarjetaDAO = tarjetaDAO;
    }

    public ITarjetaDAO getTarjetaDAO() {
        return tarjetaDAO;
    }

    @Override
    public List<Sancion> getSancionesPorPartido(Partido partido){
        return getSancionDAO().getSancionesPorPartido(partido);
    }

    @Override
    public List<Sancion> getSancionesNoCumplidas(Equipo equipoLocal, Equipo equipoVisitante) {
        return getSancionDAO().getSancionesPorPartido(equipoLocal,equipoVisitante);
    }

	@Override
	public List<Sancion> findByTorneoNoCumplidas(Torneo torneo) {
		return getSancionDAO().findByTorneoNoCumplidas(torneo);
	}

	@Override
	public List<Sancion> findByTorneo(Torneo torneo) {
		return getSancionDAO().findByTorneo(torneo);
	}

}
