package ar.com.noxit.servicios.impl;

import java.util.List;

import org.apache.commons.lang.Validate;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.Fecha;
import ar.com.noxit.servicios.IFechaService;
import ar.com.noxit.servicios.dao.IFechaDAO;

public class FechaService extends AbstractService<Fecha> implements IFechaService {

    public FechaService(IFechaDAO fechaDAO) {
        this.genericDAO = fechaDAO;
    }

    @Override
    public Fecha getLastFechaNoJugadaByTorneoId(Long torneoId) {
        Validate.notNull(torneoId, "TorneoId en servicio de fecha nulo.");
        return getFechaDAO().getLastFechaNoJugadaByTorneoId(torneoId);
    }
    
    @Override
    public List<Fecha> getAllFechasByTorneoId(Long torneoId) {
    	return getFechaDAO().getAllFechasByTorneoId(torneoId);
    }

    public void setFechaDAO(IFechaDAO fechaDAO) {
        this.genericDAO = fechaDAO;
    }

    public IFechaDAO getFechaDAO() {
        return (IFechaDAO) this.genericDAO;
    }

	@Override
	public Integer getCountByTorneoId(Long torneoId) {
		return getFechaDAO().getCountByTorneoId(torneoId);
	}


}
