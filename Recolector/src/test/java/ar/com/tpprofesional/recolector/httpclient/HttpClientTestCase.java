package ar.com.tpprofesional.recolector.httpclient;

import static org.junit.Assert.assertNotNull;
import static junit.framework.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Test;

public class HttpClientTestCase {

    @Test
    public void clienteTest() throws ClientProtocolException, IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet("http://localhost/");
        HttpResponse response = httpclient.execute(httpget);
        HttpEntity entity = response.getEntity();
        assertNotNull(entity);
    }

    @Test
    public void contenidoClienteTest() throws ClientProtocolException, IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet("http://localhost/");
        HttpResponse response = httpclient.execute(httpget);
        HttpEntity entity = response.getEntity();
        assertNotNull(entity);

        if (entity != null) {
            Header contentType = entity.getContentType();
            InputStream instream = entity.getContent();
            int l;
            byte[] tmp = new byte[2048];
            while ((l = instream.read(tmp)) != -1) {
            }
            assertEquals("text/html",contentType.getValue());
        }
    }
}
