﻿Pasos a seguir para levantar la aplicación: (debería estar en una página de la wiki)

1 - Crear una base de datos con los siguientes datos: (MySql)
	- Nombre de base: picadito
	- Usuario: root
	- Clave: root
Las tablas las crea hibernate asique no hay problemas.
2 - Bajar eclipse (j2ee helios)
3 - Bajar plugin maven para eclipse 
	- help -> install new software
	- sitio:http://m2eclipse.sonatype.org/sites/m2e/
4 - Bajar extras de plugin maven para eclipse: wtp y extras
	- sitio: http://m2eclipse.sonatype.org/sites/m2e-extras
	- http://m2eclipse.sonatype.org/sites/m2e-webby/
	- posta:  http://download.eclipse.org/m2e-wtp/releases/juno/
Con este plugin te setea automaticamente la variable en el classpath de eclipse M2_REPO, sino hacerla apuntar a /home/usuario/.m2/repository en linux o c:\Documents and Settings\Usuario\.m2\repository en win (o por ahi XD)
5 - En linux instalar maven, en win con solo bajar los binarios y agregarlos al path basta para poder usarlo.
6 - Bajar binarios del tomcat 6, no hace falta el servidor corriendo en la pc, se monta en eclipse para correr desde ahi.
7 - Con el plugin de maven levantar los proyectos al eclipse.
8 - Una vez levantados con el plugin, debe estar activado el manejo de dependencias de maven, si no es asi, click derecho sobre el proyecto -> maven -> enable dependency management (a todos los proyectos)
9 - Para el proyecyo web, si no lo tomo como un war o proyecto web, hacer lo siguiente:
	- click derecho -> propiedades
	- Project Facets -> activar ... (si no estan ya activas)
	- Setear las facetas:
		- Dynamic Web Module 2.4 para arriba
		- Java 1.6
		- Javascript 1.0
	- En Project References setear referencia a todos los proyectos
	- Java Build Path -> Pestaña Projects -> Add -> Select All
	- Java Build Path -> Pestaña Order and Export: subir al tope los proyectos recien seleccionados, luego seleccionar todos y guadar
10 - En la vista Servers hay que crear un servidor, generalmente la vista Servers no esta, para verla: Window -> Show View -> Others -> Buscar ahi Servers y con eso te la agrega a la perspectiva
11 - Crear un nuevo servidor, Apache Tomcat 6 apuntando a los binarios bajados
12 - Publicar la aplicacion, agregandola
13 - Darle run o debug al servidor