package ar.com.noxit.seguridad.services;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IUsuarioService extends IGenericService<Usuario> {

    /**
     * Codifica el rawPassword pasado por parmetro y se lo setea al user.
     * 
     * @param usuario
     *            al cual se quiere setear el rawPassword codificado
     * @param rawPassword
     *            es el password a codificar
     */
    public void encodeAndSetPassword(Usuario usuario, String rawPassword);

    /**
     * Codifica el rawPassword pasado por parmetro lo retorna.
     * 
     * @param rawPassword
     *            a codificar
     * @return password codificado
     */
    public String encodePassword(String rawPassword);

    @Transactional(readOnly = true)
    public boolean isDuplicateUsername(Usuario entity);

    @Transactional(readOnly = true)
    public boolean isDuplicateUsername(String username);

    @Transactional(readOnly = true)
    public boolean isDuplicateUsername(String username, Long id);

    public List<Usuario> getAllActivos();
    
    @Transactional(readOnly = true)
    public Usuario getByUsername(String username);
    
}