package ar.com.noxit.seguridad.services.impl;

import org.acegisecurity.userdetails.UserDetails;
import org.acegisecurity.userdetails.UserDetailsService;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.springframework.dao.DataAccessException;

import ar.com.noxit.seguridad.dao.IUsuarioDAO;

/**
 * Service que carga un usuario a partir de su username para que pueda ser usado por Acegi.
 * 
 */
public class DetallesUsuarioService implements UserDetailsService {

    private IUsuarioDAO usuarioDAO;

    /**
     * Metodo que indica como obtener el usuario
     */
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        UserDetails user = null;

        try {
            user = usuarioDAO.getUserByUsername(username);
        } catch (Exception e) {
            e.printStackTrace();
            throw new UsernameNotFoundException(e.getCause().getMessage(), e.getCause());
        }
        return user;
    }

    public IUsuarioDAO getUsuarioDAO() {
        return usuarioDAO;
    }

    public void setUsuarioDAO(IUsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }
}
