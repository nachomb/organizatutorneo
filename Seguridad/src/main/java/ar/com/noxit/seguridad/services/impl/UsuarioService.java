package ar.com.noxit.seguridad.services.impl;

import java.util.List;

import org.acegisecurity.providers.encoding.Md5PasswordEncoder;
import org.acegisecurity.userdetails.User;
import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.seguridad.dao.IUsuarioDAO;
import ar.com.noxit.seguridad.services.IUsuarioService;

@Transactional
public class UsuarioService extends AbstractService<Usuario> implements IUsuarioService {

    public UsuarioService(IUsuarioDAO usuarioDAO) {
        this.genericDAO = usuarioDAO;
    }

    public void setUsuarioDAO(IUsuarioDAO usuarioDAO) {
        this.genericDAO = usuarioDAO;
    }

    public UsuarioService() {
    }

    public void encodeAndSetPassword(Usuario usuario, String rawPassword) {
        usuario.setPassword(encodePassword(rawPassword));
    }

    /**
     * Encripta el password indicado por parmetro. Formato de enconde es MD5.
     * 
     * @param password
     *            contrasea a ser encodeada a MD5.
     * @return MD5 correspondiente al password indicado por parmetro.
     */
    public String encodePassword(String password) {
        Md5PasswordEncoder md5Encoder = new Md5PasswordEncoder();
        return md5Encoder.encodePassword(password, null);
    }

    @Transactional(readOnly = true)
    public boolean isDuplicateUsername(Usuario entity) {
        return isDuplicateUsername(entity.getUsername(), entity.getId());
    }

    @Transactional(readOnly = true)
    public boolean isDuplicateUsername(String username) {
        Usuario usuario = ((IUsuarioDAO) genericDAO).getUserByUsername(username);
        if (usuario != null) {
            return true;
        }
        return false;
    }

    @Transactional(readOnly = true)
    public boolean isDuplicateUsername(String username, Long id) {
        Usuario usuario = ((IUsuarioDAO) genericDAO).getUserByUsername(username);
        if (id == null && usuario != null) {
            return true;
        } else if (id != null && usuario != null) {
            return !usuario.getId().equals(id);
        }

        return false;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Usuario> getAllActivos() {
        return ((IUsuarioDAO) genericDAO).getAllActivos();
    }

    @Override
    public Usuario getByUsername(String username) {
        return ((IUsuarioDAO) genericDAO).getUserByUsername(username);
    }

}
