package ar.com.noxit.seguridad.filters;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.acegisecurity.Authentication;
import org.acegisecurity.providers.anonymous.AnonymousAuthenticationToken;
import org.acegisecurity.providers.anonymous.AnonymousProcessingFilter;
import org.acegisecurity.ui.AuthenticationDetailsSource;
import org.acegisecurity.ui.AuthenticationDetailsSourceImpl;
import org.acegisecurity.userdetails.memory.UserAttribute;
import org.springframework.util.Assert;

import ar.com.noxit.picadito.dominio.entidades.seguridad.UsuarioAnonimo;

public class UsuarioAnonimoProcessingFilter extends AnonymousProcessingFilter {

    private AuthenticationDetailsSource authenticationDetailsSource = new AuthenticationDetailsSourceImpl();

    @Override
    protected Authentication createAuthentication(ServletRequest request) {
        Assert.isInstanceOf(HttpServletRequest.class, request,
                "ServletRequest must be an instance of HttpServletRequest");

        UserAttribute atributos = getUserAttribute();

        AnonymousAuthenticationToken auth = new AnonymousAuthenticationToken(getKey(), UsuarioAnonimo.getInstancia(),
                atributos.getAuthorities());

        Object details = authenticationDetailsSource.buildDetails((HttpServletRequest) request);
        auth.setDetails(details);

        return auth;
    }

}
