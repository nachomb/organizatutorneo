package ar.com.noxit.seguridad.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.seguridad.dao.IUsuarioDAO;

@Transactional
public class UsuarioDAO extends AbstractDAO<Usuario> implements IUsuarioDAO {

    @SuppressWarnings("unchecked")
    public Usuario getUserByUsername(String username) {
        Usuario user = null;

        String queryString = "from Usuario u where u.username=?";

        List<Usuario> usuarios = this.getHibernateTemplate().find(queryString, username);

        if (!usuarios.isEmpty())
            user = (Usuario) usuarios.iterator().next();

        return user;
    }

    @Override
    protected Query getQueryAll() {
        Query query = this.getSession().createQuery("from " + this.getEntity().getSimpleName() + " order by username");
        return query;
    }

    @Override
    public List<Usuario> getAllActivos() {
        String queryString = "from Usuario u where u.activo = true ";
        return getSession().createQuery(queryString).list();
    }

}
