package ar.com.noxit.seguridad.services.impl;

import java.util.Collection;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.seguridad.dao.IPerfilDAO;
import ar.com.noxit.seguridad.services.IPerfilService;

@Transactional
public class PerfilService extends AbstractService<Perfil> implements IPerfilService {

    @Override
    public void update(Perfil perfil) {
        // Si el Profile esta inactivo, NO debe tener relaciones con usuarios:
        if (!perfil.isActivo()) {
            perfil.setUsuarios(null);
        }
        super.update(perfil);
    }

    public PerfilService(IPerfilDAO perfilDAO) {
        this.genericDAO = perfilDAO;
    }

    public void setPerfilDAO(IPerfilDAO perfilDAO) {
        this.genericDAO = perfilDAO;
    }

    public PerfilService() {
    }

    @Override
    public Collection<Usuario> findUsersWithProfile(Perfil perfil) {
        return ((IPerfilDAO) this.genericDAO).findUsersWithProfile(perfil);
    }

    public Perfil findByName(String nombre) {
        return ((IPerfilDAO) this.genericDAO).getProfileByName(nombre);
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public boolean isDuplicateName(Perfil entity) {

        Perfil prof = ((IPerfilDAO) genericDAO).getProfileByName(entity.getNombre());
        if (prof != null) {
            return (!prof.getId().equals(entity.getId()));
        }

        return false;

    }

    @Override
    public Collection<Perfil> getAllActivos() {
        return ((IPerfilDAO) this.genericDAO).getAllActivos();
    }

}
