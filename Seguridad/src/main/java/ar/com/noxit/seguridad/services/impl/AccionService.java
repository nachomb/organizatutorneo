package ar.com.noxit.seguridad.services.impl;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.AbstractService;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Accion;
import ar.com.noxit.seguridad.dao.IAccionDAO;
import ar.com.noxit.seguridad.services.IAccionService;

@Transactional
public class AccionService extends AbstractService<Accion> implements IAccionService {

    public AccionService(IAccionDAO accionDAO) {
        this.genericDAO = accionDAO;
    }

    public void setAccionDAO(IAccionDAO accionDAO) {
        this.genericDAO = accionDAO;
    }

    public IAccionDAO getAccionDAO() {
        return (IAccionDAO) this.genericDAO;
    }

    public AccionService() {
    }

    @Override
    @Transactional(readOnly = true)
    public Accion findByEntity(String entity) {
        return this.getAccionDAO().findByEntity(entity);
    }
}
