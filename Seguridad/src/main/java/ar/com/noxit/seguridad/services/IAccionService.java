package ar.com.noxit.seguridad.services;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Accion;

public interface IAccionService extends IGenericService<Accion> {

    @Transactional(readOnly = true)
    public Accion findByEntity(String entity);

}