package ar.com.noxit.seguridad.filters;

import java.util.Iterator;

import org.acegisecurity.ConfigAttributeDefinition;
import org.acegisecurity.SecurityConfig;
import org.acegisecurity.intercept.web.AbstractFilterInvocationDefinitionSource;
import org.acegisecurity.intercept.web.PathBasedFilterInvocationDefinitionMap;

import ar.com.noxit.picadito.dominio.entidades.seguridad.Accion;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.seguridad.constantes.SeguridadConstantes;
import ar.com.noxit.seguridad.dao.IAccionDAO;
import ar.com.noxit.seguridad.dao.IPerfilDAO;

/**
 * Proxy de la clase <code>PathBasedFilterInvocationDefinitionMap</code> Levanta los datos de las actions y sus
 * perfiles.
 * 
 */
public class PathBasedFilterInvocationDefinitionMapProxy extends AbstractFilterInvocationDefinitionSource {

    private PathBasedFilterInvocationDefinitionMap delegado;

    private IAccionDAO accionDAO;

    private IPerfilDAO perfilDAO;

    /**
     * Carga todas las acciones de tipo page (url) y los perfiles que pueden acceder a ellas.
     */
    public void refresh() {

        delegado = new PathBasedFilterInvocationDefinitionMap();

        delegado.setConvertUrlToLowercaseBeforeComparison(true);

        Iterator<Accion> itAcciones = accionDAO.getAll().iterator();

        // Por cada accion del tipo page, busco los perfiles la contenga y los agrego al COnfigAttributeDefinition
        while (itAcciones.hasNext()) {
            Accion accion = itAcciones.next();

            // Verifico que la accion sea del tipo page
            if (accion.getTipoEntidad().getNombre().equals(SeguridadConstantes.ACTION_PAGE)) {

                ConfigAttributeDefinition cad = new ConfigAttributeDefinition();

                Iterator<Perfil> itPerfiles = perfilDAO.getProfilesByAction(accion).iterator();

                // Itero los perfiles y los agrego al ConfigAttributeDefinition
                while (itPerfiles.hasNext()) {
                    String nombrePerfil = itPerfiles.next().getNombre();

                    cad.addConfigAttribute(new SecurityConfig(nombrePerfil
                            .equals(SeguridadConstantes.PROFILE_ANONYMOUS) ? nombrePerfil
                            : SeguridadConstantes.PROFILE_PREFIX + nombrePerfil));
                }

                delegado.addSecureUrl(accion.getEntidad(), cad);
            }
        }
    }

    public ConfigAttributeDefinition lookupAttributes(String url) {
        return delegado.lookupAttributes(url);
    }

    public Iterator getConfigAttributeDefinitions() {
        return delegado.getConfigAttributeDefinitions();
    }

    public IAccionDAO getAccionDAO() {
        return accionDAO;
    }

    public void setAccionDAO(IAccionDAO accionDAO) {
        this.accionDAO = accionDAO;
    }

    public IPerfilDAO getPerfilDAO() {
        return perfilDAO;
    }

    public void setPerfilDAO(IPerfilDAO perfilDAO) {
        this.perfilDAO = perfilDAO;
    }
}
