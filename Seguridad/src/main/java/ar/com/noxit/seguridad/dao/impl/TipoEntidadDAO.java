package ar.com.noxit.seguridad.dao.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import ar.com.noxit.picadito.dominio.entidades.seguridad.TipoEntidad;
import ar.com.noxit.seguridad.dao.ITipoEntidadDAO;

/**
 * Implementacin de la interfaz EntityType utilizando HibernateDaoSupport
 * 
 */
public class TipoEntidadDAO extends HibernateDaoSupport implements ITipoEntidadDAO {

    @SuppressWarnings("unchecked")
    @Override
    public TipoEntidad getTipoEntidadByName(String nombre) {
        TipoEntidad tipoEntidad = null;

        String queryString = "from TipoEntidad e where e.nombre = ?";

        List<TipoEntidad> tiposEntidad = this.getHibernateTemplate().find(queryString, nombre);

        if (!tiposEntidad.isEmpty())
            tipoEntidad = tiposEntidad.iterator().next();

        return tipoEntidad;
    }

    public TipoEntidad load(Long id) {
        return (TipoEntidad) this.getHibernateTemplate().load(TipoEntidad.class, id);
    }

    public TipoEntidad merge(TipoEntidad tipoEntidad) {
        return (TipoEntidad) this.getHibernateTemplate().merge(tipoEntidad);
    }

    public void persist(TipoEntidad tipoEntidad) {
        this.getHibernateTemplate().persist(tipoEntidad);
    }

    public void remove(TipoEntidad tipoEntidad) {
        this.getHibernateTemplate().delete(tipoEntidad);
    }

    public void update(TipoEntidad tipoEntidad) {
        this.getHibernateTemplate().update(tipoEntidad);
    }

    @SuppressWarnings("unchecked")
    public Collection<TipoEntidad> get(TipoEntidad tipoEntidad) {
        return this.getHibernateTemplate().findByExample(tipoEntidad);
    }

    @SuppressWarnings("unchecked")
    public Collection<TipoEntidad> findAll() {
        return this.getHibernateTemplate().loadAll(TipoEntidad.class);
    }

}
