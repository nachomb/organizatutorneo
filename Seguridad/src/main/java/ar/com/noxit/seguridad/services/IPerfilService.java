package ar.com.noxit.seguridad.services;

import java.util.Collection;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.IGenericService;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

public interface IPerfilService extends IGenericService<Perfil> {

    Collection<Usuario> findUsersWithProfile(Perfil entity);

    Perfil findByName(String name);

    @Transactional(readOnly = true)
    boolean isDuplicateName(Perfil entity);

    @Transactional(readOnly = true)
    public Collection<Perfil> getAllActivos();

}
