package ar.com.noxit.seguridad.dao;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Accion;

/**
 * Interfaz para el manejo de acciones
 * 
 */
public interface IAccionDAO extends IGenericDAO<Accion, Long> {

    /**
     * Obtiene una accion a traves del name indicado
     * 
     * @param name
     *            nombre de la accion buscada
     * @return accion que se desea buscar. Si no lo encuentra devuelve null
     */
    @Transactional(readOnly = true)
    public Accion getAccionByName(String name);

    @Transactional(readOnly = true)
    public Accion findByEntity(String entity);

}
