package ar.com.noxit.seguridad.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Accion;
import ar.com.noxit.seguridad.dao.IAccionDAO;

/**
 * Implementacin de la interfaz ActionDAO utilizando HibernateDaoSupport
 * 
 */
public class AccionDAO extends AbstractDAO<Accion> implements IAccionDAO {

    @SuppressWarnings("unchecked")
    @Override
    public Accion getAccionByName(String nombre) {
        Accion accion = null;

        String queryString = "from Accion a where a.nombre = ?";

        List<Accion> acciones = this.getHibernateTemplate().find(queryString, nombre);

        if (!acciones.isEmpty())
            accion = acciones.iterator().next();

        return accion;
    }

    @Override
    @Transactional(readOnly = true)
    public Accion findByEntity(String entity) {
        Criteria criteria = this.getSession().createCriteria(Accion.class);
        criteria.add(Restrictions.ilike("entidad", entity));

        return (Accion) criteria.uniqueResult();
    }

}
