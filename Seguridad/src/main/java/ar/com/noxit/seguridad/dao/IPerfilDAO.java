package ar.com.noxit.seguridad.dao;

import java.util.Collection;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Accion;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

/**
 * Intefaz del DAO de perfiles
 * 
 */
public interface IPerfilDAO extends IGenericDAO<Perfil, Long> {

    /**
     * Obtiene el perfil segun el nombre indicado
     * 
     * @param name
     *            nombre del perfil buscado
     * @return perfil cuyo nombre es el indicado. En caso de no existir, devuelve null
     */
    @Transactional(readOnly = true)
    public Perfil getProfileByName(String name);

    /**
     * Obtiene los perfiles que se asocian a un mismo action
     * 
     * @param action
     *            : accin por la que se quieren obtener los perfiles
     * @return retorna una coleccin de perfiles asociados a un action
     */
    @Transactional(readOnly = true)
    public Collection<Perfil> getProfilesByAction(Accion accion);

    /**
     * Obtiene los usuarios asociados al perfil
     * 
     * @param profile
     *            nombre del perfil
     * @return coleccin de usuarios asociados al perfil
     */
    public Collection<Usuario> findUsersWithProfile(Perfil perfil);

    /**
     * Obtiene todos los Profiles activos (enabled)
     * 
     * @return
     */
    @Transactional(readOnly = true)
    public Collection<Perfil> getAllActivos();

}
