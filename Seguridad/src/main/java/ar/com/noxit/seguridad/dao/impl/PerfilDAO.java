package ar.com.noxit.seguridad.dao.impl;

import java.util.Collection;
import java.util.List;

import org.hibernate.Query;

import ar.com.noxit.daos.dao.generico.AbstractDAO;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Accion;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Perfil;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;
import ar.com.noxit.seguridad.dao.IPerfilDAO;

public class PerfilDAO extends AbstractDAO<Perfil> implements IPerfilDAO {

    @SuppressWarnings("unchecked")
    public Perfil getProfileByName(String name) {
        Perfil perfil = null;

        String queryString = "from Perfil p where p.nombre = ?";

        List<Perfil> perfiles = this.getHibernateTemplate().find(queryString, name);

        if (!perfiles.isEmpty())
            perfil = perfiles.iterator().next();

        return perfil;
    }

    @SuppressWarnings("unchecked")
    public Collection<Perfil> getProfilesByAction(Accion accion) {
        String hql = "select p from Perfil p inner join p.acciones a where a.id = :id";

        Query query = getSession().createQuery(hql);
        query.setParameter("id", accion.getId());

        return query.list();
    }

    @SuppressWarnings("unchecked")
    public Collection<Usuario> findUsersWithProfile(Perfil perfil) {
        Long perfilID = perfil.getId();

        String hql = "select u from Usuario u inner join u.perfiles p where p.id = :id";
        Query query = getSession().createQuery(hql);
        query.setParameter("id", perfilID);

        return query.list();
    }

    @Override
    public Collection<Perfil> getAllActivos() {
        return this.getHibernateTemplate().find("from Perfil where activo = true order by id");
    }

    @Override
    protected Query getQueryAll() {
        Query query = this.getSession().createQuery("from " + this.getEntity().getSimpleName() + " order by nombre");
        return query;
    }

}
