package ar.com.noxit.seguridad.dao;

import java.util.Collection;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.picadito.dominio.entidades.seguridad.TipoEntidad;

/**
 * Interfaz para el manejo de EntityTypes
 * 
 * @author dbraithwaite
 */
public interface ITipoEntidadDAO {

    /**
     * Obtiene el tipo de entity segun el name indicado
     * 
     * @param name
     *            nombre que representa al tipo de entity
     * @return el tipo de entity
     */
    @Transactional(readOnly = true)
    public TipoEntidad getTipoEntidadByName(String nombre);

    /**
     * Obtiene el tipo de entity cuyo identificador es el indicado
     * 
     * @param id
     *            identificador nico de la accin
     * @return retorna el tipo de entity cuyo identificador es el indicado
     */
    @Transactional(readOnly = true)
    public TipoEntidad load(Long id);

    /**
     * Persiste el tipo de entity indicado
     * 
     * @param tipoEntidad
     *            tipo de entity que se quiere persistir
     */
    public void persist(TipoEntidad tipoEntidad);

    /**
     * Actualiza el tipo de entity indicado
     * 
     * @param tipoEntidad
     *            tipo de entity que se quiere actualizar
     */
    public void update(TipoEntidad tipoEntidad);

    /**
     * Persiste o actualiza el tipo de entity indicado
     * 
     * @param tipoEntidad
     *            tipo de entity que se quiere persistir o actualizar
     * @return retorna el tipo de entity con un nuevo identificador en caso de haber sido persistida
     */
    public TipoEntidad merge(TipoEntidad tipoEntidad);

    /**
     * Remueve el tipo de entity indicado
     * 
     * @param tipoEntidad
     *            tipo de entity que se desea remover
     */
    public void remove(TipoEntidad tipoEntidad);

    /**
     * Obtiene una coleccion de entidades que se asocian al ejemplo
     * 
     * @param businessObject
     *            entidad ejemplo
     * @return retorna una coleccion de entidades
     */
    @Transactional(readOnly = true)
    public Collection<TipoEntidad> get(TipoEntidad tipoEntidad);

    /**
     * Obtiene todas las entities type
     * 
     * @return retorna una coleccin con todas las entity types
     */
    @Transactional(readOnly = true)
    public Collection<TipoEntidad> findAll();
}
