package ar.com.noxit.seguridad.helpers;

import ar.com.noxit.seguridad.filters.PathBasedFilterInvocationDefinitionMapProxy;

public class ContextHelper {
    private PathBasedFilterInvocationDefinitionMapProxy accionesPerfilMap;

    public PathBasedFilterInvocationDefinitionMapProxy getAccionesPerfilMap() {
        return accionesPerfilMap;
    }

    public void setAccionesPerfilMap(PathBasedFilterInvocationDefinitionMapProxy accionesPerfilMap) {
        this.accionesPerfilMap = accionesPerfilMap;
    }

    public void reloadContext() {
        this.getAccionesPerfilMap().refresh();
    }
}
