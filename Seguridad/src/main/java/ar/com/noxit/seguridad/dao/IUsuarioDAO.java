package ar.com.noxit.seguridad.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ar.com.noxit.daos.dao.generico.IGenericDAO;
import ar.com.noxit.picadito.dominio.entidades.seguridad.Usuario;

/**
 * Interfaz para el DAO de usuarios
 * 
 */
@Transactional
public interface IUsuarioDAO extends IGenericDAO<Usuario, Long> {

    /**
     * Obtiene un usuario a traves del username indicado
     * 
     * @param username
     *            nombre del usuario buscado
     * @return usuario que se desea buscar. Si no lo encuentra devuelve null
     */
    @Transactional(readOnly = true)
    public Usuario getUserByUsername(String username);

    public List<Usuario> getAllActivos();

}