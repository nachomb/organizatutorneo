package ar.com.noxit.seguridad.constantes;

/**
 * Constantes de seguridad
 * 
 * @author lucas
 */

public interface SeguridadConstantes {

    // Nombre del tipo de accion page. Asi debe figurar en la base.
    public static final String ACTION_PAGE = "page";

    // Nombre del tipo de accion item_menu. Asi debe figurar en la base.
    public static final String ACTION_ITEM_MENU = "item_menu";

    // Prefijo de los perfiles
    public static final String PROFILE_PREFIX = "PROFILE_";

    // Nombre del perfil anonimo
    public static final String PROFILE_ANONYMOUS = "IS_AUTHENTICATED_ANONYMOUSLY";
}
